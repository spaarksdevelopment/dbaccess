﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.Interfaces
{
    public interface ILogging
    {
        void LogInfoMessage(string message);

        void LogErrorMessage(string message, Exception exception);

        void LogDebugMessage(string message, Exception exception);

        void LogFatalMessage(string message, Exception exception);

        void LogInfoMessageVerboseOnly(string message);
    }
}
