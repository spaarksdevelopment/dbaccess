﻿using HRFeedMasterDataFileImport.DAL;
using HRFeedMasterDataFileImport.DAL.Abstract;
using HRFeedMasterDataFileImport.Helpers;
using HRFeedMasterDataFileImport.Interfaces;
using System;

namespace HRFeedMasterDataFileImport.HRPersonImport
{
    public class HRPersonCopier
    {
        private ILogging _Logger;
        private IHRPersonImportRepo _HRPersonImportRepo;
 
        private HRPersonImportValidator _HRPersonImportValidator;
        
        public HRPersonCopier(ILogging logger, IHRPersonImportRepo repo)
        {
            _Logger = logger;
            _HRPersonImportRepo = repo;
            _HRPersonImportValidator = new HRPersonImportValidator(repo);
        }

        public bool ImportFromMasterDataToHRPerson(string HRID)
        {
            MasterDataImport masterDataImportRow = _HRPersonImportRepo.GetMasterData(HRID);
            string divPath = _HRPersonImportRepo.GetDivPath(masterDataImportRow.GR_DIVISION_CODE);

            HRPersonImportValidationFlags errorFlags = _HRPersonImportValidator.ValidateMasterDataRow(masterDataImportRow, divPath);
            if (!_HRPersonImportValidator.AreThereAnyErrors(errorFlags))
            {
                bool success = CopyMasterDataToHRPerson(masterDataImportRow, divPath);
                success = UpdateMPUser(success, masterDataImportRow);
                if (success)
                return true;
            }
            
            _HRPersonImportRepo.RemoveEntryFromMasterData(HRID);

            LogInvalidRow(masterDataImportRow, errorFlags);
            
            return false;
        }

        private bool UpdateMPUser(bool success, MasterDataImport masterDataImportRow)
        {
            if (!success)
                return false;

            if (!ValidToCopyToMPUser(masterDataImportRow))
                return true;

            try
            {
                _HRPersonImportRepo.UpdateOrInsertMPUserFromHRPerson(masterDataImportRow.HR_ID);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("Failed to update mpuser for HR ID:{0} - {1}", masterDataImportRow.HR_ID, ex.Message);
                _Logger.LogErrorMessage(errorMessage, ex);
            }
            return false;
        }

        public bool ValidToCopyToMPUser(MasterDataImport masterDataImportRow)
        {
            string countryName = GetCountryName(masterDataImportRow.LOCATION_COUNTRY);

            if (countryName == "Denmark")
                return false;

            string emailAddress = masterDataImportRow.EMPLOYEE_EMAIL_ID.Replace(" ","");

            if (string.IsNullOrWhiteSpace(emailAddress))
                return false;

            if (masterDataImportRow.EMPLOYEE_STATUS != "A")
                return false;

            return true;
        }

        private bool CopyMasterDataToHRPerson(MasterDataImport masterDataImportRow, string divPath)
        {
            int dbpeopleID = HRPersonImportHelpers.GetDBPeopleID(masterDataImportRow.HR_ID);
            HRPerson hrperson = _HRPersonImportRepo.GetHRPerson(dbpeopleID);
            hrperson = CopyFieldsFromMasterDataToHRPerson(hrperson, masterDataImportRow, divPath);

            return UpdateHRPerson(hrperson);
        }

        public bool UpdateHRPerson(HRPerson hrperson)
        {
            try
            {
                _HRPersonImportRepo.UpdateOrInsertHRPerson(hrperson);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("Failed to update hrperson for ID:{0} - {1}", hrperson.dbPeopleID, ex.Message);
                _Logger.LogErrorMessage(errorMessage, ex);
            }
            return false;
        }

        public HRPerson CopyFieldsFromMasterDataToHRPerson(HRPerson hrperson, MasterDataImport masterDataImportRow, string divPath)
        {
            if (hrperson == null)
                hrperson = new HRPerson();

            bool isExternal = HRPersonImportHelpers.GetIsExternal(masterDataImportRow.ORGANIZATIONAL_RELATIONSHIP);
            string hrClassification = GetClassification(masterDataImportRow.EMPLOYEE_CLASS, isExternal);
            int dbpeopleID = HRPersonImportHelpers.GetDBPeopleID(masterDataImportRow.HR_ID);
            string countryname = GetCountryName(masterDataImportRow.LOCATION_COUNTRY);

            hrperson.dbPeopleID = dbpeopleID;
            hrperson.Forename = masterDataImportRow.LEGAL_FIRST_NAME;
            hrperson.Surname = GetSurname(masterDataImportRow.LEGAL_LAST_NAME);
            hrperson.PreferredFirstName = masterDataImportRow.PREFERRED_FIRST_NAME;
            hrperson.PreferredLastName = masterDataImportRow.PREFERRED_LAST_NAME;
            hrperson.DBDirID = 0;
            hrperson.EmailAddress = masterDataImportRow.EMPLOYEE_EMAIL_ID;
            hrperson.Telephone = null;
            hrperson.Class = hrClassification;
            hrperson.UBR = masterDataImportRow.GR_DIVISION_CODE;
            hrperson.CostCentre = masterDataImportRow.COST_CENTER;
            hrperson.CostCentreName = masterDataImportRow.COST_CENTER_DESCR;
            hrperson.LegalEntityID = masterDataImportRow.LEGAL_ENTITY;
            hrperson.LegalEntity = null;
            hrperson.DivPath = divPath;
            hrperson.ManagerName = null;
            hrperson.CityName = masterDataImportRow.LOCATION_CITY;
            hrperson.CountryName = countryname;
            hrperson.Status = masterDataImportRow.EMPLOYEE_STATUS;
            hrperson.VendorID = 0;
            hrperson.VendorName = null;
            hrperson.IsExternal = isExternal;
            hrperson.Enabled = true;
            hrperson.OfficerId = masterDataImportRow.CORPORATE_TITLE;
            //Note - don't want to set revoke flag. This should be null for a new person,
            //or remain as true if they've already been revoked 

            return hrperson;
        }

        public string GetSurname(string surname)
        {
            if (string.IsNullOrWhiteSpace(surname))
                return surname;

            return surname.Replace("(E)", string.Empty);
        }

        public string GetCountryName(string countryISO)
        {
            return _HRPersonImportRepo.GetCountryNameFromISO(countryISO);
        }

        public string GetClassification(string hrClassification, bool isExternal)
        {
            if (string.IsNullOrWhiteSpace(hrClassification) || !isExternal)
                return null;

            return "Class_" + hrClassification;
        }

        public void LogInvalidRow(MasterDataImport masterDataImportRow, HRPersonImportValidationFlags errorFlags)
        {
            _HRPersonImportRepo.AddEntryToBadData(masterDataImportRow, errorFlags);

            AddInvalidFieldsToErrorLog(masterDataImportRow, errorFlags);
        }

        private void AddInvalidFieldsToErrorLog(MasterDataImport masterDataImportRow, HRPersonImportValidationFlags errorFlags)
        {
            if (HRPersonImportHelpers.GetStringConfigValue("VerboseLogging", "0") == "1")
            {
                string message = _HRPersonImportValidator.GetErrorMessage(errorFlags, masterDataImportRow.HR_ID);
                _Logger.LogInfoMessageVerboseOnly(message);
            }
        }
    }
}
