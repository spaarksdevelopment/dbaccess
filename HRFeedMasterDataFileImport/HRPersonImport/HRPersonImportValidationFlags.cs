﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.HRPersonImport
{
    [Flags]
    public enum HRPersonImportValidationFlags
    {
        InvalidHRID = 1,
        InvalidFirstName = 2,
        InvalidLastName = 4,
        InvalidEmail = 8,
        InvalidClass = 16,
        InvalidUBR = 32,
        InvalidCostCentre = 64,
        InvalidCostCentreName = 128,
        InvalidLegalEntity = 256,
        InvalidDivPath = 512,
        InvalidCityName = 1024,
        InvalidCountryName = 2048,
        InvalidStatus = 4096,
        InvalidOfficerID = 8192,
        InvalidPreferredFirstName = 16384,
        InvalidPreferredLastName = 32768
    }
}
