﻿using HRFeedMasterDataFileImport.DAL;
using HRFeedMasterDataFileImport.DAL.Abstract;
using HRFeedMasterDataFileImport.Helpers;
using HRFeedMasterDataFileImport.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace HRFeedMasterDataFileImport.HRPersonImport
{
    public class HRPersonImportValidator
    {
        private IHRPersonImportRepo _HRPersonImportRepo;

        private int _MaxCityNameLength = 50;
        private int _MaxLegalEntityIDLength = 5;
        private List<string> _Classes = new List<string>();

        public HRPersonImportValidator(IHRPersonImportRepo repo)
        {
            _HRPersonImportRepo = repo;
            _Classes = repo.GetClasses();
        }

        public string GetErrorMessage(HRPersonImportValidationFlags errorFlags, string hrID)
        {
            StringBuilder builder = new StringBuilder();

            foreach (HRPersonImportValidationFlags value in Enum.GetValues(errorFlags.GetType()))
                if (errorFlags.HasFlag(value))
                    AppendMessageForFlag(builder, value);

            string message = string.Format("{0} for HR ID:{1}", builder.ToString(), hrID);

            return message;
        }

        public void AppendMessageForFlag(StringBuilder builder, HRPersonImportValidationFlags flag)
        {
            string message = flag.ToString();
            message = message.Replace("Invalid", "Invalid ");
            message += ";";
            builder.Append(message);
        }

        public HRPersonImportValidationFlags ValidateMasterDataRow(MasterDataImport masterDataImportRow, string divPath)
        {
            HRPersonImportValidationFlags errors = new HRPersonImportValidationFlags();

            if (!HRPersonImportHelpers.IsHRIDValid(masterDataImportRow.HR_ID))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidHRID);

            if (!IsNameValid(masterDataImportRow.LEGAL_FIRST_NAME))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidFirstName);

            if (!IsNameValid(masterDataImportRow.LEGAL_LAST_NAME))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidLastName);

            if (!IsNameNullOrValid(masterDataImportRow.PREFERRED_FIRST_NAME))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidPreferredFirstName);

            if (!IsNameNullOrValid(masterDataImportRow.PREFERRED_LAST_NAME))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidPreferredLastName);

            if (!IsEmailValid(masterDataImportRow.EMPLOYEE_EMAIL_ID))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidEmail);

            if (!IsClassValid(masterDataImportRow.EMPLOYEE_CLASS))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidClass);

            if (!IsUBRValid(masterDataImportRow.GR_DIVISION_CODE))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidUBR);

            if (!IsCostCentreValid(masterDataImportRow.COST_CENTER))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCostCentre);

            if (!IsCostCentreNameValid(masterDataImportRow.COST_CENTER_DESCR))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCostCentreName);

            if (!IsLegalEntityIDValid(masterDataImportRow.LEGAL_ENTITY))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidLegalEntity);

            if (!IsDivPathValid(divPath))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidDivPath);

            if (!IsCityNameValid(masterDataImportRow.LOCATION_CITY))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCityName);

            if (!IsCountryNameValid(masterDataImportRow.LOCATION_COUNTRY))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCountryName);

            if (!IsStatusValid(masterDataImportRow.EMPLOYEE_STATUS))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidStatus);

            if (!IsOfficerIDValid(masterDataImportRow.CORPORATE_TITLE))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidOfficerID);


            return errors;
        }

        public void SetErrorMessage(ref HRPersonImportValidationFlags allErrors, HRPersonImportValidationFlags thisError)
        {
            allErrors = allErrors |= thisError;
        }

        public bool AreThereAnyErrors(HRPersonImportValidationFlags allErrors)
        {
            return (allErrors) != 0;
        }

        public bool IsNameNullOrValid(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return true;

            return IsNameValid(name);
        }

        public bool IsNameValid(string name)
        {
            return !HRPersonImportHelpers.IsRegexMatch(name, GetRegexFromConfig("RegExValidName"));
        }

        public bool IsEmailValid(string emailAddress)
        {
            return string.IsNullOrWhiteSpace(emailAddress) ||
                HRPersonImportHelpers.IsRegexMatch(emailAddress,
                GetRegexFromConfig("RegExValidEmail"));
        }

        public bool IsClassValid(string personClass)
        {
            return true;
            //bool isExternal = HRPersonImportHelpers.GetIsExternal(HR_ID);

            //if (!isExternal)
            //    return true;

            //if (string.IsNullOrWhiteSpace(personClass))
            //    return true;

            //return (DoesClassExist(personClass));
        }

        public bool IsUBRValid(string ubr)
        {
            return true;
            //if (string.IsNullOrWhiteSpace(ubr))
            //    return false;

            //return HRPersonImportHelpers.IsRegexMatch(ubr, GetRegexFromConfig("RegExValidUBR"));
        }

        public bool IsCostCentreValid(string costCentre)
        {
            return true;
        }

        public bool IsCostCentreNameValid(string costCentreName)
        {
            return true;
        }

        public bool IsLegalEntityIDValid(string legalEntityID)
        {
            if (IsStringNullOrTooLong(legalEntityID, _MaxLegalEntityIDLength))
                return false;

            return true;
        }

        public bool IsDivPathValid(string divPath)
        {
            return true;
            //if (string.IsNullOrWhiteSpace(divPath))
            //    return false;

            //return HRPersonImportHelpers.IsRegexMatch(divPath, GetRegexFromConfig("RegExValidDivPath"));
        }

        public bool IsCityNameValid(string cityName)
        {
            if (IsStringTooLong(cityName, _MaxCityNameLength))
                return false;

            return true;
        }

        private bool IsStringTooLong(string input, int maxLength)
        {
            if (input == null)
                return false;

            return input.Length > maxLength;
        }

        private bool IsStringNullOrTooLong(string input, int maxLength)
        {
            return string.IsNullOrWhiteSpace(input) || input.Length > maxLength;
        }

        public bool IsCountryNameValid(string isoAlpha3Code)
        {
            return _HRPersonImportRepo.IsCountryInDBAccess(isoAlpha3Code);
        }

        public bool IsStatusValid(string status)
        {
            if (string.IsNullOrWhiteSpace(status))
                return false;

            return HRPersonImportHelpers.IsRegexMatch(status, GetRegexFromConfig("RegExValidStatus"));
        }

        public bool IsOfficerIDValid(string officerID)
        {
            if (string.IsNullOrWhiteSpace(officerID))
                return true;

            return HRPersonImportHelpers.IsRegexMatch(officerID, GetRegexFromConfig("RegExValidOfficerID"));
        }

        public string GetRegexFromConfig(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey].Replace("#8217;", "'");
        }
    }
}
