﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace HRFeedMasterDataFileImport.Helpers
{
    public static class HRPersonImportHelpers
    {
        private const int MaxLengthDBPeopleID = 7;

        public static bool GetIsExternal(string organizationalRelationship)
        {
            return organizationalRelationship != "EMP";
        }

        public static int GetDBPeopleID(string hrID)
        {
            return Convert.ToInt32(hrID);
        }

        public static bool IsHRIDValid(string hrID)
        {
            return hrID.Length <= MaxLengthDBPeopleID && IsRegexMatch(hrID, @"^\d{7,}$");
        }

        public static bool IsRegexMatch(string input, string regex)
        {
            Match match = Regex.Match(input, regex, RegexOptions.IgnoreCase);
            return match.Success;
        }

        public static bool GetBooleanConfigValue(string configValueName)
        {
            string value = ConfigurationManager.AppSettings[configValueName];
            if (string.IsNullOrWhiteSpace(value))
                return false;

            bool result = false;

            Boolean.TryParse(value, out result);

            return result;
        }

        public static string GetStringConfigValue(string configValueName, string defaultValue)
        {
            string value = ConfigurationManager.AppSettings[configValueName];
            if (string.IsNullOrWhiteSpace(value))
                return defaultValue;

            return value;
        }
    }
}
