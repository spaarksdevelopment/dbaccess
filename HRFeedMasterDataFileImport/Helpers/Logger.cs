﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using HRFeedMasterDataFileImport.Interfaces;
using System.Configuration;

namespace HRFeedMasterDataFileImport.Helpers
{
    public class Logger : ILogging
    {
        private ILog _Log = null;

        private bool LogVerbose
        {
            get
            {
                bool toReturn = false;
                bool workingCheck = false;

                string configValue = ConfigurationManager.AppSettings["VerboseLogging"];

                if (!String.IsNullOrEmpty(configValue) && Boolean.TryParse(configValue, out workingCheck))
                    toReturn = workingCheck;

                return toReturn;
            }
        }

        public Logger()
        {
            XmlConfigurator.Configure();
            _Log = LogManager.GetLogger(typeof(Logger));
        }

        public void LogInfoMessageVerboseOnly(string message)
        {
            if (LogVerbose)
            {
                LogInfoMessage(message);
            }
        }

        public void LogInfoMessage(string message)
        {
            _Log.Info(message);
        }

        public void LogErrorMessage(string message, Exception exception)
        {
            _Log.Error(message, exception);
        }

        public void LogDebugMessage(string message, Exception exception)
        {
            _Log.Debug(message, exception);
        }

        public void LogFatalMessage(string message, Exception exception)
        {
            _Log.Fatal(message, exception);
        }
    }
}
