﻿using HRFeedMasterDataFileImport.DAL.Abstract;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HRFeedMasterDataFileImport.Helpers;

namespace HRFeedMasterDataFileImport.DAL.Concrete
{
    public class HRPersonImportRepo : IHRPersonImportRepo
    {
        private const int StandardUserSecurityGroupID = 1;

        public MasterDataImport GetMasterData(string HRID)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                return context.MasterDataImports.Where(a => a.HR_ID == HRID).FirstOrDefault();
            }
        }

        private bool DoesCountryExist(string isoAlpha3Code, dbAccess_LatestSchemaEntities context)
        {
            bool isoExists = context.ISO_Countries.Any(a => a.ISOALPHA3code == isoAlpha3Code);
            if (!isoExists)
                return false;

            string countryName = context.ISO_Countries.Where(a => a.ISOALPHA3code == isoAlpha3Code).First().Countryorareaname;

            return context.LocationCountries.Any(a => a.Name == countryName);
        }

        public bool IsCountryInDBAccess(string isoAlpha3Code)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                return DoesCountryExist(isoAlpha3Code, context);
            }
        }

        public string GetDivPath(string ubrCode)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                string result = context.Database.SqlQuery<string>
                    ("SELECT [dbo].[ComputeUBRPath] (@p0)", ubrCode).First();
                return result;
            }
        }

        public List<string> GetClasses()
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                return context.HRClassifications.Select(a=>a.Classification).OrderBy(a=>a).ToList();
            }
        }

        public void AddEntryToBadData(MasterDataImport masterDataImportRow, HRPersonImport.HRPersonImportValidationFlags errorFlags)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImportBadData badData = new MasterDataImportBadData();
                badData.ErrorFlags = (int)errorFlags;
                badData.HR_ID = masterDataImportRow.HR_ID;
                badData.PREFIX = masterDataImportRow.PREFIX;
                badData.NAME_TITLE = masterDataImportRow.NAME_TITLE;
                badData.LEGAL_FIRST_NAME = masterDataImportRow.LEGAL_FIRST_NAME;
                badData.LEGAL_MIDDLE_NAME = masterDataImportRow.LEGAL_MIDDLE_NAME;
                badData.LEGAL_LAST_NAME = masterDataImportRow.LEGAL_LAST_NAME;
                badData.PREFERRED_FIRST_NAME = masterDataImportRow.PREFERRED_FIRST_NAME;
                badData.PREFERRED_LAST_NAME = masterDataImportRow.PREFERRED_LAST_NAME;
                badData.PREFERRED_NAME = masterDataImportRow.PREFERRED_NAME;
                badData.EMPLOYEE_EMAIL_ID = masterDataImportRow.EMPLOYEE_EMAIL_ID;
                badData.EMPLOYEE_CLASS = masterDataImportRow.EMPLOYEE_CLASS;
                badData.EMPLOYEE_STATUS = masterDataImportRow.EMPLOYEE_STATUS;
                badData.ORGANIZATIONAL_RELATIONSHIP = masterDataImportRow.ORGANIZATIONAL_RELATIONSHIP;
                badData.CORPORATE_TITLE = masterDataImportRow.CORPORATE_TITLE;
                badData.POSITION_NUMBER = masterDataImportRow.POSITION_NUMBER;
                badData.JOB_CODE = masterDataImportRow.JOB_CODE;
                badData.TELEPHONE_NO = masterDataImportRow.TELEPHONE_NO;
                badData.LOCATION_ID = masterDataImportRow.LOCATION_ID;
                badData.LOCATION_COUNTRY = masterDataImportRow.LOCATION_COUNTRY;
                badData.LOCATION_CITY = masterDataImportRow.LOCATION_CITY;
                badData.COST_CENTER = masterDataImportRow.COST_CENTER;
                badData.COST_CENTER_DESCR = masterDataImportRow.COST_CENTER_DESCR;
                badData.LEGAL_ENTITY = masterDataImportRow.LEGAL_ENTITY;
                badData.GR_DIVISION_CODE = masterDataImportRow.GR_DIVISION_CODE;
                badData.UBR_PRODUCT_CODE = masterDataImportRow.UBR_PRODUCT_CODE;
                badData.UBR_PRODUCT_DESCR = masterDataImportRow.UBR_PRODUCT_DESCR;
                badData.UBR_SUB_PRODUCT_CODE = masterDataImportRow.UBR_SUB_PRODUCT_CODE;
                badData.UBR_SUB_PRODUCT_DESCR = masterDataImportRow.UBR_SUB_PRODUCT_DESCR;
                badData.EFFECTIVE_DATE_JOB_START = masterDataImportRow.EFFECTIVE_DATE_JOB_START;
                badData.EFFECTIVE_DATE_JOB_END = masterDataImportRow.EFFECTIVE_DATE_JOB_END;
                badData.CREATE_ACCESS_DATE_TIMESTAMP = masterDataImportRow.CREATE_ACCESS_DATE_TIMESTAMP;
                badData.REVOKE_ACCESS_DATE_TIMESTAMP = masterDataImportRow.REVOKE_ACCESS_DATE_TIMESTAMP;
                badData.MANAGER_HR_ID = masterDataImportRow.MANAGER_HR_ID;
                badData.MANAGER_EMAIL_ADDRESS = masterDataImportRow.MANAGER_EMAIL_ADDRESS;
                badData.BADGE_ID = masterDataImportRow.BADGE_ID;
                badData.AMR_ID = masterDataImportRow.AMR_ID;
                badData.EXTERNAL_PROVIDER = masterDataImportRow.EXTERNAL_PROVIDER;
                badData.BUILDING = masterDataImportRow.BUILDING;
                badData.FLOOR = masterDataImportRow.FLOOR;
                badData.PAO_INDICATOR = masterDataImportRow.PAO_INDICATOR;
                badData.DB_SITE_INDICATOR = masterDataImportRow.DB_SITE_INDICATOR;

                context.MasterDataImportBadDatas.Add(badData);
                context.SaveChanges();
            }
        }

        public void RemoveEntryFromMasterData(string HRID)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                var rowToDelete = context.MasterDataImports.Where(a => a.HR_ID == HRID).FirstOrDefault();
                if (rowToDelete == null)
                    return;

                context.MasterDataImports.Remove(rowToDelete);
                context.SaveChanges();
            }
        }

        public HRPerson GetHRPerson(int dbPeopleID)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                return context.HRPersons.Where(a => a.dbPeopleID == dbPeopleID).SingleOrDefault();
            }
        }

        public string GetCountryNameFromISO(string countryISO)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                if (!DoesCountryExist(countryISO, context))
                    return null;

                string countryName = context.ISO_Countries.Where(a => a.ISOALPHA3code == countryISO).First().Countryorareaname;

                return countryName;
            }
        }

        public void UpdateOrInsertHRPerson(HRPerson hrperson)
        {
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                bool exists = context.HRPersons.Any(a => a.dbPeopleID == hrperson.dbPeopleID);
                context.Entry(hrperson).State = exists ?
                                    EntityState.Modified :
                                    EntityState.Added;

                context.SaveChanges();
            }
        }

        public void UpdateOrInsertMPUserFromHRPerson(string hrID)
        {
            int dbpeopleID = HRPersonImportHelpers.GetDBPeopleID(hrID);

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                bool exists = context.mp_User.Any(a => a.dbPeopleID == dbpeopleID);

                if (exists)
                    UpdateMPUserFromHRPerson(dbpeopleID, context);
                else
                    InsertMPUserFromHRPerson(dbpeopleID, context);

                context.SaveChanges();
            }
        }

        public void InsertMPUserFromHRPerson(int dbpeopleID, dbAccess_LatestSchemaEntities context)
        {
            HRPerson hrPerson = context.HRPersons.Where(a => a.dbPeopleID == dbpeopleID).First();
            mp_User mpUser = mp_User.InsertFromHRPerson(hrPerson);
            context.mp_User.Add(mpUser);

            this.UpdateOrInsertMPSecurityGroupUser(dbpeopleID, context);
        }

        public void UpdateMPUserFromHRPerson(int dbpeopleID, dbAccess_LatestSchemaEntities context)
        {
            mp_User mpUser = context.mp_User.Where(a => a.dbPeopleID == dbpeopleID).First();
            HRPerson hrPerson = context.HRPersons.Where(a => a.dbPeopleID == dbpeopleID).First();
            mpUser.CopyFromHRPerson(hrPerson);

            this.UpdateOrInsertMPSecurityGroupUser(dbpeopleID, context);
        }

        public void UpdateOrInsertMPSecurityGroupUser(int dbpeopleID, dbAccess_LatestSchemaEntities context)
        {
            bool exists = context.mp_SecurityGroupUser.Any(a => a.dbPeopleID == dbpeopleID && a.SecurityGroupId == StandardUserSecurityGroupID);

            if (!exists)
                InsertMPSecurityGroupUser(dbpeopleID, context);
        }

        public void InsertMPSecurityGroupUser(int dbpeopleID, dbAccess_LatestSchemaEntities context)
        {
            mp_SecurityGroupUser securityGroupUser = new mp_SecurityGroupUser();
            securityGroupUser.dbPeopleID = dbpeopleID;
            securityGroupUser.SecurityGroupId = StandardUserSecurityGroupID;
            context.mp_SecurityGroupUser.Add(securityGroupUser);
        }
    }
}
