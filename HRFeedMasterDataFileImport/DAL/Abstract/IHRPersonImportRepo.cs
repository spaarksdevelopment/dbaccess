﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.DAL.Abstract
{
    public interface IHRPersonImportRepo
    {
        MasterDataImport GetMasterData(string HRID);
        
        bool IsCountryInDBAccess(string isoAlpha3Code);

        string GetDivPath(string ubrCode);

        List<string> GetClasses();

        void AddEntryToBadData(MasterDataImport masterDataImportRow, HRPersonImport.HRPersonImportValidationFlags errorFlags);

        void RemoveEntryFromMasterData(string HRID);

        HRPerson GetHRPerson(int dbPeopleID);

        string GetCountryNameFromISO(string countryISO);

        void UpdateOrInsertHRPerson(HRPerson hrperson);

        void UpdateOrInsertMPUserFromHRPerson(string hrID);

        void InsertMPUserFromHRPerson(int dbpeopleID, dbAccess_LatestSchemaEntities context);

        void UpdateMPUserFromHRPerson(int dbpeopleID, dbAccess_LatestSchemaEntities context);

        void UpdateOrInsertMPSecurityGroupUser(int dbpeopleID, dbAccess_LatestSchemaEntities context);

        void InsertMPSecurityGroupUser(int dbpeopleID, dbAccess_LatestSchemaEntities context);
    }
}
