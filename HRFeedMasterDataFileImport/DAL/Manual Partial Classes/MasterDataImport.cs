﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.DAL
{
    public partial class MasterDataImport
    {
        public override bool Equals(object obj)
        {
            MasterDataImport other = (MasterDataImport)obj;
            return this.HR_ID == other.HR_ID &&
                   this.PREFIX == other.PREFIX &&
                   this.NAME_TITLE == other.NAME_TITLE &&
                   this.LEGAL_FIRST_NAME == other.LEGAL_FIRST_NAME &&
                   this.LEGAL_MIDDLE_NAME == other.LEGAL_MIDDLE_NAME &&
                   this.LEGAL_LAST_NAME == other.LEGAL_LAST_NAME &&
                   this.PREFERRED_FIRST_NAME == other.PREFERRED_FIRST_NAME &&
                   this.PREFERRED_LAST_NAME == other.PREFERRED_LAST_NAME &&
                   this.PREFERRED_NAME == PREFERRED_NAME &&
                   this.EMPLOYEE_EMAIL_ID == other.EMPLOYEE_EMAIL_ID &&
                   this.EMPLOYEE_CLASS == other.EMPLOYEE_CLASS &&
                   this.EMPLOYEE_STATUS == other.EMPLOYEE_STATUS &&
                   this.ORGANIZATIONAL_RELATIONSHIP == other.ORGANIZATIONAL_RELATIONSHIP &&
                   this.CORPORATE_TITLE == other.CORPORATE_TITLE &&
                   this.POSITION_NUMBER == other.POSITION_NUMBER &&
                   this.JOB_CODE == other.JOB_CODE &&
                   this.TELEPHONE_NO == other.TELEPHONE_NO &&
                   this.LOCATION_ID == other.LOCATION_ID &&
                   this.LOCATION_COUNTRY == other.LOCATION_COUNTRY &&
                   this.LOCATION_CITY == other.LOCATION_CITY &&
                   this.COST_CENTER == other.COST_CENTER &&
                   this.COST_CENTER_DESCR == other.COST_CENTER_DESCR &&
                   this.LEGAL_ENTITY == other.LEGAL_ENTITY &&
                   this.GR_DIVISION_CODE == other.GR_DIVISION_CODE &&
                   this.UBR_PRODUCT_CODE == other.UBR_PRODUCT_CODE &&
                   this.UBR_PRODUCT_DESCR == other.UBR_PRODUCT_DESCR &&
                   this.UBR_SUB_PRODUCT_CODE == other.UBR_SUB_PRODUCT_CODE &&
                   this.UBR_SUB_PRODUCT_DESCR == other.UBR_SUB_PRODUCT_DESCR &&
                   this.EFFECTIVE_DATE_JOB_START == other.EFFECTIVE_DATE_JOB_START &&
                   this.EFFECTIVE_DATE_JOB_END == other.EFFECTIVE_DATE_JOB_END &&
                   this.AMR_ID == other.AMR_ID &&
                   this.CREATE_ACCESS_DATE_TIMESTAMP == other.CREATE_ACCESS_DATE_TIMESTAMP &&
                   this.REVOKE_ACCESS_DATE_TIMESTAMP == other.REVOKE_ACCESS_DATE_TIMESTAMP &&
                   this.MANAGER_HR_ID == other.MANAGER_HR_ID &&
                   this.MANAGER_EMAIL_ADDRESS == other.MANAGER_EMAIL_ADDRESS &&
                   this.BADGE_ID == other.BADGE_ID &&
                   this.EXTERNAL_PROVIDER == other.EXTERNAL_PROVIDER &&
                   this.BUILDING == other.BUILDING &&
                   this.FLOOR == other.FLOOR &&
                   this.PAO_INDICATOR == other.PAO_INDICATOR &&
                   this.DB_SITE_INDICATOR == other.DB_SITE_INDICATOR;
        }

        public override int GetHashCode()
        {
            return 
            new
            {
                HR_ID = this.HR_ID,
                PREFIX = this.PREFIX,
                NAME_TITLE = this.NAME_TITLE,
                LEGAL_FIRST_NAME = this.LEGAL_FIRST_NAME,
                LEGAL_MIDDLE_NAME = this.LEGAL_MIDDLE_NAME,
                LEGAL_LAST_NAME = this.LEGAL_LAST_NAME,
                PREFERRED_FIRST_NAME = this.PREFERRED_FIRST_NAME,
                PREFERRED_LAST_NAME = this.PREFERRED_LAST_NAME,
                PREFERRED_NAME = this.PREFERRED_NAME,
                EMPLOYEE_EMAIL_ID = this.EMPLOYEE_EMAIL_ID,
                EMPLOYEE_CLASS = this.EMPLOYEE_CLASS,
                EMPLOYEE_STATUS = this.EMPLOYEE_STATUS,
                ORGANIZATIONAL_RELATIONSHIP = this.ORGANIZATIONAL_RELATIONSHIP,
                CORPORATE_TITLE = this.CORPORATE_TITLE,
                POSITION_NUMBER = this.POSITION_NUMBER,
                JOB_CODE = this.JOB_CODE,
                TELEPHONE_NO = this.TELEPHONE_NO,
                LOCATION_ID = this.LOCATION_ID,
                LOCATION_COUNTRY = this.LOCATION_COUNTRY,
                LOCATION_CITY = this.LOCATION_CITY,
                COST_CENTER = this.COST_CENTER,
                COST_CENTER_DESCR = this.COST_CENTER_DESCR,
                LEGAL_ENTITY = this.LEGAL_ENTITY,
                GR_DIVISION_CODE = this.GR_DIVISION_CODE,
                UBR_PRODUCT_CODE = this.UBR_PRODUCT_CODE,
                UBR_PRODUCT_DESCR = this.UBR_PRODUCT_DESCR,
                UBR_SUB_PRODUCT_CODE = this.UBR_SUB_PRODUCT_CODE,
                UBR_SUB_PRODUCT_DESCR = this.UBR_SUB_PRODUCT_DESCR,
                EFFECTIVE_DATE_JOB_START = this.EFFECTIVE_DATE_JOB_START,
                EFFECTIVE_DATE_JOB_END = this.EFFECTIVE_DATE_JOB_END,
                AMR_ID = this.AMR_ID,
                CREATE_ACCESS_DATE_TIMESTAMP = this.CREATE_ACCESS_DATE_TIMESTAMP,
                REVOKE_ACCESS_DATE_TIMESTAMP = this.REVOKE_ACCESS_DATE_TIMESTAMP,
                MANAGER_HR_ID = this.MANAGER_HR_ID,
                MANAGER_EMAIL_ADDRESS = this.MANAGER_EMAIL_ADDRESS,
                BADGE_ID = this.BADGE_ID,
                EXTERNAL_PROVIDER = this.EXTERNAL_PROVIDER,
                BUILDING = this.BUILDING,
                FLOOR = this.FLOOR,
                PAO_INDICATOR = this.PAO_INDICATOR,
                DB_SITE_INDICATOR = this.DB_SITE_INDICATOR
            }.GetHashCode();
        }
    }
}
