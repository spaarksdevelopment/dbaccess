﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.DAL
{
    public partial class mp_SecurityGroupUser
    {
        public override bool Equals(object obj)
        {
            mp_SecurityGroupUser other = (mp_SecurityGroupUser)obj;
            return
                this.SecurityGroupId == other.SecurityGroupId &&
                this.dbPeopleID == other.dbPeopleID;
        }

        public override int GetHashCode()
        {
            return
            new
            {
                SecurityGroupId = this.SecurityGroupId,
                dbPeopleID = this.dbPeopleID
            }.GetHashCode();
        }
    }
}
