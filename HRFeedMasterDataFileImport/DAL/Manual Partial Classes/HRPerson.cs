﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.DAL
{
    public partial class HRPerson
    {
        public override bool Equals(object obj)
        {
            HRPerson other = (HRPerson)obj;
            return
                    this.dbPeopleID == other.dbPeopleID &&
                    this.Forename == other.Forename &&
                    this.Surname == other.Surname &&
                    this.DBDirID == other.DBDirID &&
                    this.EmailAddress == other.EmailAddress &&
                    this.Telephone == other.Telephone &&
                    this.Class == other.Class &&
                    this.UBR == other.UBR &&
                    this.CostCentre == other.CostCentre &&
                    this.CostCentreName == other.CostCentreName &&
                    this.LegalEntityID == other.LegalEntityID &&
                    this.LegalEntity == other.LegalEntity &&
                    this.DivPath == other.DivPath &&
                    this.ManagerName == other.ManagerName &&
                    this.CityName == other.CityName &&
                    this.CountryName == other.CountryName &&
                    this.Status == other.Status &&
                    this.VendorID == other.VendorID &&
                    this.VendorName == other.VendorName &&
                    this.IsExternal == other.IsExternal &&
                    this.Enabled == other.Enabled &&
                    this.OfficerId == other.OfficerId &&
                    this.Revoked == other.Revoked;
        }

        public override int GetHashCode()
        {
            return
            new
            {
                dbPeopleID = this.dbPeopleID,
                Forename = this.Forename,
                Surname = this.Surname,
                DBDirID = this.DBDirID,
                EmailAddress = this.EmailAddress,
                Telephone = this.Telephone,
                Class = this.Class,
                UBR = this.UBR,
                CostCentre = this.CostCentre,
                CostCentreName = this.CostCentreName,
                LegalEntityID = this.LegalEntityID,
                LegalEntity = this.LegalEntity,
                DivPath = this.DivPath,
                ManagerName = this.ManagerName,
                CityName = this.CityName,
                CountryName = this.CountryName,
                Status = this.Status,
                VendorID = this.VendorID,
                VendorName = this.VendorName,
                IsExternal = this.IsExternal,
                Enabled = this.Enabled,
                OfficerId = this.OfficerId,
                Revoked = this.Revoked
            }.GetHashCode();
        }
    }
}
