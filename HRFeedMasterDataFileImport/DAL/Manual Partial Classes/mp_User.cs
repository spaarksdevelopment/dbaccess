﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedMasterDataFileImport.DAL
{
    public partial class mp_User : ICloneable
    {
        public override bool Equals(object obj)
        {
            mp_User other = (mp_User)obj;
            return
                this.Id == other.Id &&
                this.EmailAddress == other.EmailAddress &&
                this.Forename == other.Forename &&
                this.Surname == other.Surname &&
                this.Telephone == other.Telephone &&
                this.CostCentre == other.CostCentre &&
                this.UBR == other.UBR &&
                this.Enabled == other.Enabled &&
                this.VisitCount == other.VisitCount &&
                this.VendorID == other.VendorID &&
                this.dbPeopleID == other.dbPeopleID &&
                this.Revoked == other.Revoked;
        }

        public override int GetHashCode()
        {
            return
            new
            {
                Id = this.Id,
                EmailAddress = this.EmailAddress,
                Forename = this.Forename,
                Surname = this.Surname,
                Telephone = this.Telephone,
                CostCentre = this.CostCentre,
                UBR = this.UBR,
                Enabled = this.Enabled,
                CreatedDateTime = this.CreatedDateTime,
                CreatedByUserId = this.CreatedByUserId,
                VisitCount = this.VisitCount,
                VendorID = this.VendorID,
                dbPeopleID = this.dbPeopleID,
                Revoked = this.Revoked
            }.GetHashCode();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void CopyFromHRPerson(HRPerson hrPerson)
        {
            mp_User.GetFieldsFromHRPerson(this, hrPerson);
        }

        public static mp_User InsertFromHRPerson(HRPerson hrPerson)
        {
            mp_User other = new mp_User();
            mp_User.GetFieldsFromHRPerson(other, hrPerson);
            
            other.CreatedDateTime = DateTime.Now;
            other.CreatedByUserId = null;
            other.VisitCount = 0;
            return other;
        }

        private static void GetFieldsFromHRPerson(mp_User other, HRPerson hrPerson)
        {
            other.Id = hrPerson.dbPeopleID;
            other.EmailAddress = hrPerson.EmailAddress;
            other.Forename = hrPerson.Forename;
            other.Surname = hrPerson.Surname;
            other.Telephone = hrPerson.Telephone;
            other.CostCentre = hrPerson.CostCentre;
            other.UBR = hrPerson.UBR;
            other.Enabled = hrPerson.Enabled;
            other.VendorID = hrPerson.VendorID;
            other.dbPeopleID = hrPerson.dbPeopleID;
            other.Revoked = hrPerson.Revoked;
        }
    }
}
