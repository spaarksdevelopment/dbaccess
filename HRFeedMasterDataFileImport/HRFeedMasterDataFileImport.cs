﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using HRFeedMasterDataFileImport.Helpers;

namespace HRFeedMasterDataFileImport
{
    public partial class HRFeedMasterDataFileImport : ServiceBase
    {
        private Timer _Timer;
        private FileParser _FileParser;
        private Logger _Logger;

        public HRFeedMasterDataFileImport()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _Logger = new Logger();
                _FileParser = new FileParser(_Logger);
                _Logger.LogInfoMessage("HRFeed MasterData Import Started");
                this.EventLog.WriteEntry("HRFeed MasterData Import Started");

                _Timer = new Timer(new TimerCallback(TimerImportProc));

                _Timer.Change(0, (Convert.ToInt32(ConfigurationManager.AppSettings["IntervalInMinutes"]) * 1000 * 60));
            }
            catch (Exception ex)
            {
                _Logger.LogFatalMessage("Error starting HRFeed MasterData Import Service", ex);

                if (_Timer != null)
                    _Timer.Dispose();

                this.Stop();
            }
        }

        private void TimerImportProc(object state)
        {
            ProcessInputTick();
        }

        private void ProcessInputTick()
        {
            string timingInput = ConfigurationManager.AppSettings["TimingRuns"];

            String[] timingInputSplit = timingInput.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            string theCurrentTime = DateTime.Now.ToString("HH:mm");

            foreach (string item in timingInputSplit)
            {
                if (item == theCurrentTime)
                {
                    _FileParser.CheckForFiles();
                    break;
                }
            }
        }

        protected override void OnStop()
        {
            _Logger.LogInfoMessage("HRFeed MasterData Import Stopped");            
            this.EventLog.WriteEntry("HRFeed MasterData Import Stopped");

            if (_Timer != null)
                _Timer.Dispose();

            _FileParser = null;
        }
    }
}
