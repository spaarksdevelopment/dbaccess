﻿using Aduvo.Common.UtilityManager;
using HRFeedMasterDataFileImport.DAL;
using HRFeedMasterDataFileImport.DAL.Concrete;
using HRFeedMasterDataFileImport.Helpers;
using HRFeedMasterDataFileImport.HRPersonImport;
using HRFeedMasterDataFileImport.Interfaces;
using Net.Code.Csv;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Text;

namespace HRFeedMasterDataFileImport
{
    public class FileParser
    {
        private ILogging _Logger;
        private const int _NumberOfColumnsInCSV = 41;
        private HRPersonCopier _HRPersonCopier;

        public FileParser(ILogging logger)
        {
            _Logger = logger;
            HRPersonImportRepo repo = new HRPersonImportRepo();
            _HRPersonCopier = new HRPersonCopier(_Logger, repo);
        }

        public void CheckForFiles()
        {
            try
            {
                string sourcePath = ConfigurationManager.AppSettings["PickupLocation"];
                string fileRegexMatch = ConfigurationManager.AppSettings["FileRegexMatch"];
                fileRegexMatch = String.Format(fileRegexMatch, DateTime.Now.ToString("yyyyMMdd"));

                string[] theFiles = GetFilesForProcessing(sourcePath, fileRegexMatch);

                if (theFiles.Length > 0)
                    ProcessFiles(sourcePath, theFiles);
                else
                    _Logger.LogInfoMessage("No file found matching the '" + fileRegexMatch + "' expression at:" + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                _Logger.LogErrorMessage("Error in file check", ex);
            }
        }

        private void ProcessFiles(string sourcePath, string[] theFiles)
        {
            using (dbAccess_LatestSchemaEntities dataConn = new dbAccess_LatestSchemaEntities())
            {
                dataConn.Configuration.AutoDetectChangesEnabled = false;

                dataConn.spr_ImportMasterDataFile_Prep();

                foreach (string item in theFiles)
                    ProcessFile(sourcePath, dataConn, item);
            }
        }

        private void ProcessFile(string sourcePath, dbAccess_LatestSchemaEntities dataConn, string item)
        {
            int processedSuccessfullyCount = 0, processedTotalCount = 0;
            string fileName = item.Replace(sourcePath, "");

            LogStartOfFileProcessing(fileName);

            try
            {
                using (IDataReader csv = item.ReadFileAsCsv(Encoding.UTF8))
                {
                    Dictionary<string, int> ordinalLookup = new Dictionary<string, int>();

                    GetCSVHeaders(csv, ordinalLookup);
                    
                    while (csv.Read())
                    {
                        processedSuccessfullyCount = ProcessRowFromCSV(dataConn, processedSuccessfullyCount, csv, ordinalLookup);
                        processedTotalCount++;
                    }
                }

                ProcessFileComplete(dataConn, processedSuccessfullyCount, processedTotalCount, fileName);
            }
            catch (Exception ex)
            {
                LogFileProcessingFailed(item, ex);
            }
        }

        private void LogFileProcessingFailed(string item, Exception ex)
        {
            _Logger.LogFatalMessage("Error in MasterDataFile Import", ex);
            _Logger.LogErrorMessage(item + " - " + ex.Message, ex);
        }

        private void LogStartOfFileProcessing(string fileName)
        {
            _Logger.LogInfoMessage("Start Processing: " + fileName);
            _Logger.LogInfoMessageVerboseOnly("Processing import file " + fileName);
        }

        private int ProcessRowFromCSV(dbAccess_LatestSchemaEntities dataConn, int processedSuccessfullyCount, IDataReader csv, Dictionary<string, int> ordinalLookup)
        {
            MasterDataImport inputData = GetMasterDataImportFromCSV(csv, ordinalLookup);
            bool importFromCSVSuccess = SaveRowToMasterData(inputData, dataConn);

            bool copiedToHRPersonSuccess = false;
            if (importFromCSVSuccess)
                copiedToHRPersonSuccess = _HRPersonCopier.ImportFromMasterDataToHRPerson(inputData.HR_ID);

            if (copiedToHRPersonSuccess)
                processedSuccessfullyCount++;

            return processedSuccessfullyCount;
        }
        
        private void ProcessFileComplete(dbAccess_LatestSchemaEntities dataConn, int processedSuccessfullyCount, int processedTotalCount, string fileName)
        {
            dataConn.spr_ImportMasterDataFile();
            SendFileProcessedCount(processedSuccessfullyCount, processedTotalCount, fileName);
            _Logger.LogInfoMessage("Processed import file " + fileName);
        }

        private static void GetCSVHeaders(IDataReader csv, Dictionary<string, int> ordinalLookup)
        {
            csv.Read();

            for (int i = 0; i < _NumberOfColumnsInCSV; i++)
                ordinalLookup.Add(csv[i].ToString(), i);
        }

        private string[] GetFilesForProcessing(string sourcePath, string fileRegexMatch)
        {
            string[] filesToProcess = Directory.GetFiles(sourcePath, fileRegexMatch, SearchOption.TopDirectoryOnly);
            if (HRPersonImportHelpers.GetBooleanConfigValue("ProcessFirstFileOnly") && filesToProcess.Length > 0)
                return new string[]{filesToProcess[0]};

            return Directory.GetFiles(sourcePath, fileRegexMatch, SearchOption.TopDirectoryOnly);
        }
        
        protected void SendFileProcessedCount(int processedCount, int processedTotalCount, string fileName)
        {
            string Subject = "DBAccess HR Masterdata File Import Completed";
            StringBuilder messageBody = GetProcessedFileEmailBody(processedCount, processedTotalCount, fileName, ref Subject);

            _Logger.LogInfoMessage(messageBody.ToString());

            try
            {
                string recipientAddresses = HRPersonImportHelpers.GetStringConfigValue("MessageRecipient", "martin.steel@db.com");
                string senderEmailAddress = HRPersonImportHelpers.GetStringConfigValue("SenderEmailAddress", "business.spaars@db.com");
                string senderName = HRPersonImportHelpers.GetStringConfigValue("SenderName", "HRFeedMasterDataFileImporter"); 

                MailHelper.SafeSendEmail(recipientAddresses.Split(','), Subject, messageBody.ToString(), true, senderEmailAddress, senderName);
            }
            catch (Exception ex)
            {
                _Logger.LogErrorMessage("Error in SendFileProcessedCount", ex);
            }
        }

        private StringBuilder GetProcessedFileEmailBody(int processedCount, int processedTotalCount, string fileName, ref string Subject)
        {
            StringBuilder messageBody = new StringBuilder();

            messageBody.Append("<font face=arial size=2>DBAccess HRFeed Masterfile Import</font>");
            messageBody.Append(String.Format("<br><br><font face=arial size=2>The DBAccess HR Masterdata File Import has successfully processed file {0}</font>", fileName));

            if (processedCount != processedTotalCount)
            {
                Subject += " (with Errors)";
                messageBody.Append(String.Format("<br><br><font face=arial size=2>{0} record(s) of {1} were processed</font>", processedCount, processedTotalCount));
            }
            else
                messageBody.Append(String.Format("<br><br><font face=arial size=2>All {0} record(s) were processed</font>", processedTotalCount));

            messageBody.Append("<font face=arial size=2><br><br>Regards,</font>");
            messageBody.Append("<font face=arial size=2><br>The DBAccess Team</font>");
            messageBody.Append("<font face=arial size=2><br><br>PLEASE NOTE THAT THIS IS A SYSTEM GENERATED EMAIL</font>");
            return messageBody;
        }

        private bool SaveRowToMasterData(MasterDataImport inputData, dbAccess_LatestSchemaEntities dataConn)
        {
            if(!CheckHRID(inputData.HR_ID))
                return false;
           
            string badgeID = GetBadgeID(inputData.BADGE_ID);
            inputData.BADGE_ID = badgeID;

            try
            {
                dataConn.MasterDataImports.Add(inputData);
                dataConn.SaveChanges();

                return true;
            }
            catch (DbEntityValidationException castException)
            {
                HandleCastExceptionCreatingMasterDataImportRow(dataConn, inputData, castException);
            }
            catch (Exception ex)
            {
                string errorMessage = GetGenericExceptionErrorMessage(inputData.HR_ID);
                HandleExceptionCreatingMasterDataImportRow(dataConn, inputData, ex, errorMessage);
            }

            return false;
        }

        private bool CheckHRID(string hrID)
        {
            _Logger.LogInfoMessageVerboseOnly("HR_ID is: " + hrID);

            if(HRPersonImportHelpers.IsHRIDValid(hrID))
                return true;

            LogErrorWithHRID(hrID);
            return false;            
        }

        private void LogErrorWithHRID(string hrID)
        {
            _Logger.LogErrorMessage(String.Format("Invalid HRID ({0})", hrID), null);
        }

        public MasterDataImport GetMasterDataImportFromCSV(IDataReader csv, Dictionary<string, int> ordinalLookup)
        {
            MasterDataImport csvRow = new MasterDataImport();
            csvRow.HR_ID = GetCSVValue(csv, ordinalLookup, "HR ID");
            csvRow.PREFIX = GetCSVValue(csv, ordinalLookup, "PREFIX");
            csvRow.NAME_TITLE = GetCSVValue(csv, ordinalLookup, "NAME TITLE");
            csvRow.LEGAL_FIRST_NAME = GetCSVValue(csv, ordinalLookup, "LEGAL FIRST NAME");
            csvRow.LEGAL_MIDDLE_NAME = GetCSVValue(csv, ordinalLookup, "LEGAL MIDDLE NAME");
            csvRow.LEGAL_LAST_NAME = GetCSVValue(csv, ordinalLookup, "LEGAL LAST NAME");
            csvRow.PREFERRED_FIRST_NAME = GetCSVValue(csv, ordinalLookup, "PREFERRED FIRST NAME");
            csvRow.PREFERRED_LAST_NAME = GetCSVValue(csv, ordinalLookup, "PREFERRED LAST NAME");
            csvRow.PREFERRED_NAME = GetCSVValue(csv, ordinalLookup, "PREFERRED NAME");
            csvRow.EMPLOYEE_EMAIL_ID = GetCSVValue(csv, ordinalLookup, "EMPLOYEE EMAIL ID");
            csvRow.EMPLOYEE_CLASS = GetCSVValue(csv, ordinalLookup, "EMPLOYEE CLASS");
            csvRow.EMPLOYEE_STATUS = GetCSVValue(csv, ordinalLookup, "EMPLOYEE STATUS");
            csvRow.ORGANIZATIONAL_RELATIONSHIP = GetCSVValue(csv, ordinalLookup, "ORGANIZATIONAL RELATIONSHIP");
            csvRow.CORPORATE_TITLE = GetCSVValue(csv, ordinalLookup, "CORPORATE TITLE");
            csvRow.POSITION_NUMBER = GetCSVValue(csv, ordinalLookup, "POSITION NUMBER");
            csvRow.JOB_CODE = GetCSVValue(csv, ordinalLookup, "JOB CODE");
            csvRow.TELEPHONE_NO = GetCSVValue(csv, ordinalLookup, "TELEPHONE NO");
            csvRow.LOCATION_ID = GetCSVValue(csv, ordinalLookup, "LOCATION ID");
            csvRow.LOCATION_COUNTRY = GetCSVValue(csv, ordinalLookup, "LOCATION COUNTRY");
            csvRow.LOCATION_CITY = GetCSVValue(csv, ordinalLookup, "LOCATION CITY");
            csvRow.COST_CENTER = GetCSVValue(csv, ordinalLookup, "COST CENTER");
            csvRow.COST_CENTER_DESCR = GetCSVValue(csv, ordinalLookup, "COST CENTER DESCR");
            csvRow.LEGAL_ENTITY = GetCSVValue(csv, ordinalLookup, "LEGAL ENTITY");
            csvRow.GR_DIVISION_CODE = GetCSVValue(csv, ordinalLookup, "GR DIVISION CODE");
            csvRow.UBR_PRODUCT_CODE = GetCSVValue(csv, ordinalLookup, "UBR PRODUCT CODE");
            csvRow.UBR_PRODUCT_DESCR = GetCSVValue(csv, ordinalLookup, "UBR PRODUCT DESCR");
            csvRow.UBR_SUB_PRODUCT_CODE = GetCSVValue(csv, ordinalLookup, "UBR SUB PRODUCT CODE");
            csvRow.UBR_SUB_PRODUCT_DESCR = GetCSVValue(csv, ordinalLookup, "UBR SUB PRODUCT DESCR");
            csvRow.EFFECTIVE_DATE_JOB_START = GetDateTimeFieldFromCSV(csv, ordinalLookup, "EFFECTIVE DATE JOB START");
            csvRow.EFFECTIVE_DATE_JOB_END = GetDateTimeFieldFromCSV(csv, ordinalLookup, "EFFECTIVE DATE JOB END");
            csvRow.AMR_ID = GetCSVValue(csv, ordinalLookup, "AMR ID");
            csvRow.CREATE_ACCESS_DATE_TIMESTAMP = GetDateTimeFieldFromCSV(csv, ordinalLookup, "CREATE ACCESS DATE TIMESTAMP");
            csvRow.REVOKE_ACCESS_DATE_TIMESTAMP = GetDateTimeFieldFromCSV(csv, ordinalLookup, "REVOKE ACCESS DATE TIMESTAMP");
            csvRow.MANAGER_HR_ID = GetCSVValue(csv, ordinalLookup, "MANAGER HR ID");
            csvRow.MANAGER_EMAIL_ADDRESS = GetCSVValue(csv, ordinalLookup, "MANAGER EMAIL ADDRESS");
            csvRow.BADGE_ID = GetCSVValue(csv, ordinalLookup, "BADGE ID");
            csvRow.EXTERNAL_PROVIDER = GetCSVValue(csv, ordinalLookup, "EXTERNAL PROVIDER");
            csvRow.BUILDING = GetCSVValue(csv, ordinalLookup, "BUILDING");
            csvRow.FLOOR = GetCSVValue(csv, ordinalLookup, "FLOOR");
            csvRow.PAO_INDICATOR = GetCSVValue(csv, ordinalLookup, "PAO INDICATOR");
            csvRow.DB_SITE_INDICATOR = GetCSVValue(csv, ordinalLookup, "DB SITE INDICATOR");

            return csvRow;
        }

        private DateTime? GetDateTimeFieldFromCSV(IDataReader csv, Dictionary<string, int> ordinalLookup, string fieldName)
        {
            string dateString = GetCSVValue(csv, ordinalLookup, fieldName);
            return SetDateTimeField(dateString);
        } 

        private string GetCSVValue(IDataReader csv, Dictionary<string, int> ordinalLookup, string fieldName)
        {
            if (csv.IsDBNull(ordinalLookup[fieldName]))
                return string.Empty;

            return csv[ordinalLookup[fieldName]].ToString();
        }

        private string GetGenericExceptionErrorMessage(string hrID)
        {
            return String.Format("Error in HRFeedMasterData Import ({0})", hrID);
        }

        private string GetCastExceptionErrorMessage(StringBuilder errorBuilder)
        {
            return "Error in processrow - " + errorBuilder.ToString();
        }

        private void HandleCastExceptionCreatingMasterDataImportRow(dbAccess_LatestSchemaEntities dataConn, MasterDataImport inputData,
                                                                                DbEntityValidationException castCheck)
        {
            StringBuilder builder = new StringBuilder("Error for HRID: " + inputData.HR_ID + " Badge ID is " + inputData.BADGE_ID + ":-");

            foreach (DbEntityValidationResult item in castCheck.EntityValidationErrors)
                foreach (DbValidationError errorItem in item.ValidationErrors)
                    builder.Append("{ " + errorItem.ErrorMessage + " }");

            string errorMessage = GetCastExceptionErrorMessage(builder);

            HandleExceptionCreatingMasterDataImportRow(dataConn, inputData, castCheck, errorMessage);
        }

        private void HandleExceptionCreatingMasterDataImportRow(dbAccess_LatestSchemaEntities dataConn, MasterDataImport inputData, 
                                                                            Exception ex, string errorMessage)
        { 
            _Logger.LogErrorMessage(errorMessage, ex);
            dataConn.MasterDataImports.Remove(inputData);
        }

        private string GetBadgeID(string badgeID)
        {
            if(badgeID==null)
                return string.Empty;

            return badgeID;
        }

        public Nullable<DateTime> SetDateTimeField(string fieldString)
        {
            if(string.IsNullOrWhiteSpace(fieldString))
                return null;

            DateTime parsedDateTime;

            CultureInfo USCultureInfo = new CultureInfo("en-US");
            DateTime.TryParse(fieldString, USCultureInfo, DateTimeStyles.None, out parsedDateTime);

            return parsedDateTime;
        }
    }
}
