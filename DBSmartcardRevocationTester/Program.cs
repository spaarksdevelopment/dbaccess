﻿using HRFeedMasterDataFileImport;
using HRFeedMasterDataFileImport.Helpers;

namespace DBSmartcardRevocationTester
{
	class Program
	{
		static void Main(string[] args)
		{
            Logger logger = new Logger();
            FileParser fileParser = new FileParser(logger);
            fileParser.CheckForFiles();
		}
	}
}
