﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace DBSmartcardRevocationTester
{
	public class ClientCertificates
	{
		private static int CERT_STORE_PROV_SYSTEM = 10;
		//private static int CERT_SYSTEM_STORE_CURRENT_USER = (1 << 16);
		private static int CERT_SYSTEM_STORE_LOCAL_MACHINE = (2 << 16);

		[DllImport("CRYPT32", EntryPoint = "CertOpenStore", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr CertOpenStore(
			int storeProvider, int encodingType,
			int hcryptProv, int flags, string pvPara);

		[DllImport("CRYPT32", EntryPoint = "CertEnumCertificatesInStore", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr CertEnumCertificatesInStore(
			IntPtr storeProvider,
			IntPtr prevCertContext);

		[DllImport("CRYPT32", EntryPoint = "CertCloseStore", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool CertCloseStore(
			IntPtr storeProvider,
			int flags);

		X509CertificateCollection m_ClientCertificates;

		public ClientCertificates()
		{
			m_ClientCertificates = new X509CertificateCollection();
		}

		public X509CertificateCollection ClientCertificatesList { get { return m_ClientCertificates; } }

		public int Init()
		{
			IntPtr storeHandle;
			storeHandle = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, 0, CERT_SYSTEM_STORE_LOCAL_MACHINE, "MY");
			IntPtr currentCertContext;
			currentCertContext = CertEnumCertificatesInStore(storeHandle, (IntPtr)0);
			int i = 0;
			while (currentCertContext != (IntPtr)0)
			{
				m_ClientCertificates.Insert(i++, new X509Certificate(currentCertContext));
				currentCertContext = CertEnumCertificatesInStore(storeHandle, currentCertContext);
			}
			CertCloseStore(storeHandle, 0);

			return m_ClientCertificates.Count;
		}

		public X509Certificate this[int index]
		{
			get
			{
				// Check the index limits.
				if (index < 0 || index > m_ClientCertificates.Count)
					return null;
				else
					return m_ClientCertificates[index];
			}
		}

	}
}
