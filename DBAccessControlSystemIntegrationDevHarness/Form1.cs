﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DBAccessControlSystemInterface;
using System.Globalization;

namespace DBAccessControlSystemIntegrationDevHarness
{
    public partial class Form1 : Form
    {
        string selectedDatabase = "informix";
        AccessControlSystemType selectedControlSystem;

        public Form1()
        {
            InitializeComponent();
            setUpComboBox();
        }


        public void setUpComboBox()
        {
            cbControlSystems.DataSource = Enum.GetValues(typeof(AccessControlSystemType));
            cbControlSystems.SelectedItem = AccessControlSystemType.PicturePerfect;
            selectedControlSystem = (AccessControlSystemType)cbControlSystems.SelectedItem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbCategories.DataSource = FactoryManager.GetAllCategories(selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "CategoryName";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbCategories.DataSource = FactoryManager.GetBadgesByPersonId(selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "MiFare";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lbCategories.DataSource = FactoryManager.GetAllAccess(selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "CategoryName";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lbCategories.DataSource = FactoryManager.GetNewCategories(selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "CategoryName";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (txtPersonID.Text == "") return;

            lbCategories.DataSource = FactoryManager.GetAccessByPersonId(Convert.ToInt32(txtPersonID.Text), selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "CategoryName";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (txtAddAccessPersonId.Text == "" || txtAddAccessCategoryId.Text == "" || txtAddAccessStartDate.Text == "" || txtAddAccessEndDate.Text == "") return;

            lblAddAccessResult.Text = FactoryManager.AddAcess(txtAddAccessPersonId.Text,
                                            txtAddAccessCategoryId.Text, 
                                            Convert.ToDateTime(txtAddAccessStartDate.Text),
                                            Convert.ToDateTime(txtAddAccessEndDate.Text),
                                            selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem).ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (txtRemoveAccessPersonId.Text == "" || txtRemoveAccessCategoryId.Text == "") return;

            lblAddAccessResult.Text = FactoryManager.RemoveAcess(txtRemoveAccessPersonId.Text, txtRemoveAccessCategoryId.Text, selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem).ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            lbCategories.DataSource = FactoryManager.GetAllBadges(selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem);
            lbCategories.DisplayMember = "MiFare";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (txtAddBadgePersonId.Text == "" || txtAddBadgeMiFareId.Text == "") return;

            lblAddAccessResult.Text = FactoryManager.AddBadge(Convert.ToInt32(txtAddBadgePersonId.Text), txtAddBadgeMiFareId.Text, selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem).ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (txtActivateBadgeMiFareId.Text == "") return;

            lblAddAccessResult.Text = FactoryManager.ActivateBadge(txtActivateBadgeMiFareId.Text, selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem).ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (dtExtend.Text == "" || txtExtendBadgeMiFareId.Text == "") return;

            CultureInfo cultEnGb = new CultureInfo("en-GB");

            lblAddAccessResult.Text = FactoryManager.ExtendBadge(txtExtendBadgeMiFareId.Text, Convert.ToDateTime(dtExtend.Text, cultEnGb), selectedDatabase, (AccessControlSystemType)cbControlSystems.SelectedItem).ToString();
        }

        private void rbInformix_CheckedChanged(object sender, EventArgs e)
        {
            selectedDatabase = "informix";
        }

        private void rbSqlServer_CheckedChanged(object sender, EventArgs e)
        {
            selectedDatabase = "sql";
        }

    }


    class FactoryManager
    {
        public static bool RemoveAcess(string personID, string categoryId, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.RemoveAccess(personID, categoryId);
        }

        public static bool AddAcess(string personID, string categoryId, DateTime startDate, DateTime endDate, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.AddAccess(personID, categoryId, startDate, endDate);
        }

        public static List<AccessInstance> GetAccessByPersonId(int personId, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.GetAccessByPersonId(personId);
        }

        public static List<AccessInstance> GetAllAccess(string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.GetAllAccess();
        }

        public static List<CategoryInstance> GetNewCategories(string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            DateTime dt = new DateTime(2011, 01, 01);
            return ac.GetNewCategories(dt);
        }

        public static List<CategoryInstance> GetAllCategories(string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.GetAllCategories();
        }

        public static List<BadgeInstance> GetBadgesByPersonId(string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.GetBadgesForPerson("C9021862", true);
        }

        public static List<BadgeInstance> GetAllBadges(string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.GetAllBadges(true);
        }

        public static bool AddBadge(int personId, string miFareId, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.CreateBadge(personId, miFareId);
        }

        public static bool ActivateBadge(string miFareId, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.ActivateBadge(miFareId);
        }

        public static bool ExtendBadge(string miFareId, DateTime expiryDate, string db, AccessControlSystemType cType)
        {
            ControlSystemFactory f = new ControlSystemFactory();
            IAccessControl ac = f.CreateAccessControl(cType, ControlSystemMode.Test, db);
            return ac.ExtendBadge(miFareId, expiryDate);
        }
    }
}
