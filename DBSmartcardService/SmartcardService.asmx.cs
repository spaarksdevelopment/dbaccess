﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Fake;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using DBAccess.BLL.Concrete;

namespace DBSmartcardService
{
	/// <summary>
	/// Summary description for Service1
	/// </summary>
	[WebService(Namespace = "http://www.spaarks.com/DBSmartcard")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class SmartcardService : System.Web.Services.WebService
	{
		/// <summary>
		/// Returns the dbPeopleID of the smartcard owner or an empty string if no owner
		/// </summary>
		/// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
		[WebMethod]
		public string GetSmartcardOwner(string smartcardIdentifier)
		{
            string[] certDetails;

            if (!CertificateContainsSubjectNames("GetSmartcardOwner", smartcardIdentifier, out certDetails))
                return "Error";

            SmartcardServiceBusinessLogic smartcardServiceBusinessLogic = new SmartcardServiceBusinessLogic();
            return smartcardServiceBusinessLogic.GetSmartcardOwner(smartcardIdentifier, certDetails);
		}

		/// <summary>
		/// Returns the status of the badge associated with the given smartcard identifier
		/// </summary>
		/// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
		[WebMethod]
		public string GetSmartcardStatus(string smartcardIdentifier)
		{
            string[] certDetails;

            if (!CertificateContainsSubjectNames("GetSmartcardStatus", smartcardIdentifier, out certDetails))
                return "Error";

            return Director.GetSmartcardStatus(smartcardIdentifier, certDetails);
		}

		/// <summary>
		/// Returns the PUK (Personal Unblocking Key) for the given smartcard or an empty string if smartcard is not known
		/// The PUK is associated with a vendor and will change every few years.
		/// The PUK is stored against the smartcard in the db when the smartcard is issued
		/// </summary>
		/// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
		[WebMethod]
		public string GetKeyIdentifier(string smartcardIdentifier)
		{
            string[] certDetails;

            if (!CertificateContainsSubjectNames("GetKeyIdentifier", smartcardIdentifier, out certDetails))
                return "Error";

			return Director.GetKeyIdentifier(smartcardIdentifier, certDetails);
		}

        /// <summary>
        /// Returns false if the request does not contain a client certificate, or if the Subject Alternative Name of the certificate
        /// does not contain any of the comma delimited strings in config param "ClientSubjectNames"
        /// </summary>
        private bool CertificateContainsSubjectNames(string method, string smartcardIdentifier, out string[] certDetails)
        {
			certDetails = new string[3];

			try
			{
				bool checkClientCertificate = Convert.ToBoolean(ConfigurationManager.AppSettings["CheckClientCertificate"]);
				if (!checkClientCertificate)
				{
					certDetails[0] = "Ignored";
					certDetails[1] = "Ignored";
					certDetails[2] = "Ignored";

					return true;
				}

				if (Context.Request.ClientCertificate.Certificate.Length == 0)
				{
					string errorMessage = "No client certificate has been provided";

					Director.AddSmartcardServiceAudit(method, smartcardIdentifier, "Error", certDetails, errorMessage);

					return false;
				}

				X509Certificate2 cert = new X509Certificate2(Context.Request.ClientCertificate.Certificate);

				certDetails[0] = cert.SubjectName.Name;
                certDetails[1] = cert.SerialNumber;
                certDetails[2] = cert.IssuerName.Name;

				string subjectNames = Convert.ToString(ConfigurationManager.AppSettings["ClientSubjectNames"]);
				subjectNames = subjectNames.TrimEnd(',');

				List<string> names = subjectNames.Split(',').ToList();

				foreach (string name in names)
					if (cert.SubjectName.Name.Contains(name))
						return true;

				string errorMessage2 = "The client certificate does not contain any of the required subject names";

				Director.AddSmartcardServiceAudit(method, smartcardIdentifier, "Error", certDetails, errorMessage2);
			}
			catch (Exception ex)
			{
				string errorMessageException = "There was an error when trying to read certificate details;";
				if (ex.InnerException != null)
					errorMessageException += ex.InnerException.Message;

				Director.AddSmartcardServiceAudit(method, smartcardIdentifier, "Error", certDetails, errorMessageException);
			}

            return false;
        }
	}
}