﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spaarks.Common.UtilityManager.WindowsService;
using System.Diagnostics;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Data;
using System.Net;
using System.Configuration;
using System.Data.SqlClient;
using Spaarks.Common.UtilityManager;


namespace DBAccessGenericBatchEmail
{
	public class ProcessActions : IProcessServiceActions
	{
		private EventLog _EventLog;
		private int _EmailsProcessed = 0;
		private string _AppName;

		public void ProcessServiceActions(EventLog log, string appName)
		{
			_EventLog = log;
			_AppName = appName;

			try
			{
				_EmailsProcessed = 0;
				SendBatchEmails();
			}
			catch (Exception ex)
			{
				string errorMessage = "There was an error sending email" + ex.Message;
				WriteEventToWindowsLog(_AppName, errorMessage, EventLogEntryType.Error);
				LogHelper.HandleException(ex);
			}
		}

		private void SendBatchEmails()
		{
			try
			{
                string message = string.Empty;

				List<BatchEmailPeople> batchEmailPeople = Director.GetOutStandingEmailLog().ToList();
				if (batchEmailPeople != null)
				{
					Director director = new Director();
					bool bResult = false;
					foreach (var batch in batchEmailPeople)
					{
						try
						{
							bResult = director.SendEmail(batch);
						}
						catch(Exception e)
						{
							WindowsLogging.LogError(e);
                            LogHelper.HandleException(e);
						}

						if (!bResult)
						{
                            message = GetFailedMessage(batch);
                            LogException(message);
							continue;
						}
						_EmailsProcessed++;

					}
				}

                message = GetProcessedActionsMessage();
                LogException(message);
			}
			catch(Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}

        private string GetFailedMessage(BatchEmailPeople batch)
        {
            string toAddress = GetToAddressFromBatchEmailPeople(batch);
            return string.Format("Failed on {0} person", toAddress);
        }

        private string GetToAddressFromBatchEmailPeople(BatchEmailPeople batch)
        {
            if (batch == null || batch.To == null)
                return string.Empty;

            return batch.To.ToString();
        }

        private string GetProcessedActionsMessage()
        {
            return string.Format("Processed {0} actions", _EmailsProcessed);
        }

        private void LogException(string message)
        {
            WriteEventToWindowsLog(_AppName, message, EventLogEntryType.Information);
            Exception completedException = new Exception(message);
            LogHelper.HandleException(completedException);
        }


		public void WriteEventToWindowsLog(string app, string logEntry, EventLogEntryType type)
		{
			if (!System.Diagnostics.EventLog.SourceExists(app))
				System.Diagnostics.EventLog.CreateEventSource(app, "Application");

			EventLog spaarksServiceEventLog = new EventLog();
			spaarksServiceEventLog.Source = app;
			spaarksServiceEventLog.WriteEntry(logEntry, type);
		}
	}
}
