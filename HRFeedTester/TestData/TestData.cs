﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedTester.TestData
{
    public class TestObject
    {
        public int IntValue { get; set; }
        public string StringValue { get; set; }
        public DateTime DateTimeValue { get; set; }
        public DateTime? DateTimeNullableValue { get; set; }
    }
}
