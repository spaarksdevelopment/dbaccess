﻿using HRFeedJMSQueueHandler.DAL;
using HRFeedMasterDataFileImport.DAL;
using HRFeedMasterDataFileImport.HRPersonImport;
using System;
using System.Collections.Generic;

namespace HRFeedTester.TestData
{
    public static class HRPersonImportTestData
    {
        public static MasterDataImport GetMasterDataImportFakeData()
        {
            MasterDataImport data = new MasterDataImport();
            data.HR_ID = "8062652";
            data.PREFIX = "";
            data.NAME_TITLE = "";
            data.LEGAL_FIRST_NAME = "SIT FN 93";
            data.LEGAL_MIDDLE_NAME = "";
            data.LEGAL_LAST_NAME = "SIT LN 93";
            data.PREFERRED_FIRST_NAME = "SIT";
            data.PREFERRED_LAST_NAME = "93";
            data.PREFERRED_NAME = "SIT LN 93,SIT FN 93";
            data.EMPLOYEE_EMAIL_ID = "sit.fn.ln.93@db.com";
            data.EMPLOYEE_CLASS = "A";
            data.ORGANIZATIONAL_RELATIONSHIP = "EMP";
            data.EMPLOYEE_STATUS = "A";
            data.CORPORATE_TITLE = "AS";
            data.POSITION_NUMBER = "80067852";
            data.JOB_CODE = "000000";
            data.TELEPHONE_NO = "";
            data.LOCATION_ID = "CHN00001";
            data.LOCATION_COUNTRY = "CHN";
            data.LOCATION_CITY = "Shanghai";
            data.COST_CENTER = "0912407011";
            data.COST_CENTER_DESCR = "Trade Risk Service - Sales";
            data.LEGAL_ENTITY = "0912";
            data.GR_DIVISION_CODE = "G_6097";
            data.UBR_PRODUCT_CODE = "G_8488";
            data.UBR_PRODUCT_DESCR = "Trade Finance & CM Corporates";
            data.UBR_SUB_PRODUCT_CODE = "G_2991";
            data.UBR_SUB_PRODUCT_DESCR = "Trade & Risk Sales";
            data.MANAGER_HR_ID = "8062142";
            data.MANAGER_EMAIL_ADDRESS = "sarah-may.reyes@db.com";
            data.BADGE_ID = "";
            data.EXTERNAL_PROVIDER = "";
            data.BUILDING = "";
            data.FLOOR = "";
            data.PAO_INDICATOR = "";
            data.DB_SITE_INDICATOR = "";

            data.EFFECTIVE_DATE_JOB_START = new DateTime(2014,12,31);
            data.EFFECTIVE_DATE_JOB_END = new DateTime(2020, 01, 01);

            return data;
        }

        public static MasterDataImport GetMasterDataImportFakeDataEmptyPreferredFirstAndPreferredLast()
        {
            MasterDataImport data = GetMasterDataImportFakeData();
            data.PREFERRED_FIRST_NAME = "";
            data.PREFERRED_LAST_NAME = "";

            return data;
        }

        public static string GetDivPathFakeData()
        {
            return "G_4596/G_3120/G_8488/G_2991/G_6097/";
        }

        public static List<string> GetClasses()
        {
            return new List<string>()
            {
                "Class_5",
                "Class_4",
                "Class_X",
                "Class_W",
                "Class_V",
                "Class_U",
                "Class_2",
                "Class_1",
                "Class_3"
            };
        }

        public static string GetAllErrorMessage()
        {
            return
                "Invalid HRID;Invalid FirstName;Invalid LastName;Invalid Email;Invalid Class;Invalid UBR;Invalid CostCentre;"
                    + "Invalid CostCentreName;Invalid LegalEntity;Invalid DivPath;Invalid CityName;Invalid CountryName;Invalid Status;Invalid OfficerID; for HR ID:C1234567";
        }

        public static HRPersonImportValidationFlags GetAllFlags()
        {
            HRPersonImportValidationFlags flags = new HRPersonImportValidationFlags();
            flags |= HRPersonImportValidationFlags.InvalidCityName;
            flags |= HRPersonImportValidationFlags.InvalidClass;
            flags |= HRPersonImportValidationFlags.InvalidCostCentre;
            flags |= HRPersonImportValidationFlags.InvalidCostCentreName;
            flags |= HRPersonImportValidationFlags.InvalidCountryName;
            flags |= HRPersonImportValidationFlags.InvalidDivPath;
            flags |= HRPersonImportValidationFlags.InvalidEmail;
            flags |= HRPersonImportValidationFlags.InvalidFirstName;
            flags |= HRPersonImportValidationFlags.InvalidHRID;
            flags |= HRPersonImportValidationFlags.InvalidLastName;
            flags |= HRPersonImportValidationFlags.InvalidLegalEntity;
            flags |= HRPersonImportValidationFlags.InvalidOfficerID;
            flags |= HRPersonImportValidationFlags.InvalidStatus;
            flags |= HRPersonImportValidationFlags.InvalidUBR;
            return flags;
        }

        public static string GetCityNameErrorMessage()
        {
            return "Invalid CityName; for HR ID:C1234567";
        }

        public static string GetHRIDANDEmailErrorMessage()
        {
            return "Invalid HRID;Invalid Email; for HR ID:C1234567";
        }

        public static HRFeedMessage GetHRFeedMessageFakeDate()
        {
            HRFeedMessage data = new HRFeedMessage();

            data.HRID = "8062652";
            data.FirstName = "SIT FN 93";
            data.MiddleName = "";
            data.LastName = "SIT LN 93";
            data.PreferredFirstName = "SIT";
            data.PreferredLastName = "93";
            data.DBEmailAddress = "sit.fn.ln.93@db.com";
            data.Organisational_Relationship = "EMP";
            data.CorporateTitle = "AS";
            data.LocationCountry = "CHN";
            data.LocationCity = "Shanghai";
            data.CostCenter = "0912407011";            
            data.LegalEntity = "0912";            
            data.UBR_CODE = "G_8488";           
            data.ManagerHRID = "8062142";
            data.CorporateTitle = "AS";
            data.PhysicalAccessOnly = "Y";
            data.DBSiteIndicator = "Y";
            data.AMRID = "123";
            data.EP_Code = "Y";
            data.TriggerAction = "ADD";
            data.TriggerType = "JOIN";

            return data;
        }
    }
}
