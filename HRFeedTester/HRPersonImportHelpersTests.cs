﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRFeedMasterDataFileImport.Helpers;

namespace HRFeedTester
{
    [TestClass]
    public class HRPersonImportHelpersTests
    {
        [TestMethod]
        public void When_OrganizationalRelationship_Is_CWR_GetIsExternal_ReturnsTrue()
        {
            bool actual = HRPersonImportHelpers.GetIsExternal("CWR");
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void When_OrganizationalRelationship_Is_POI_GetIsExternal_ReturnsTrue()
        {
            bool actual = HRPersonImportHelpers.GetIsExternal("POI");
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void When_OrganizationalRelationship_Is_EMP_GetIsExternal_ReturnsFalse()
        {
            bool actual = HRPersonImportHelpers.GetIsExternal("EMP");
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void GetDBPeopleID()
        {
            string hrID = "1234567";
            int expected = 1234567;
            int actual = HRPersonImportHelpers.GetDBPeopleID(hrID);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsHRIDValid_InvalidChars()
        {
            bool result = HRPersonImportHelpers.IsHRIDValid("D1234567");
            Assert.IsFalse(result);
            result = HRPersonImportHelpers.IsHRIDValid("1234567C");
            Assert.IsFalse(result);
            result = HRPersonImportHelpers.IsHRIDValid("1234C567");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsHRIDValid_TooLong()
        {
            bool result = HRPersonImportHelpers.IsHRIDValid("12345678");
            Assert.IsFalse(result);
            result = HRPersonImportHelpers.IsHRIDValid(string.Empty);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsHRIDValid_TooShort()
        {
            bool result = HRPersonImportHelpers.IsHRIDValid("123456");
            Assert.IsFalse(result);
            result = HRPersonImportHelpers.IsHRIDValid(string.Empty);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsHRIDInvalid_Valid()
        {
            bool result = HRPersonImportHelpers.IsHRIDValid("C1234567");
            Assert.IsFalse(result);
            result = HRPersonImportHelpers.IsHRIDValid("1234567");
            Assert.IsTrue(result);
        }
    }
}
