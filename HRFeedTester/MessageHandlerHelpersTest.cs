﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using HRFeedJMSQueueHandler;
using HRFeedJMSQueueHandler.Interfaces;
using System.Xml;
using System.Xml.Schema;
using HRFeedJMSQueueHandler.Enums;
using HRFeedJMSQueueHandler.Helpers;
using HRFeedTester.TestData;
using HRFeedJMSQueueHandler.DAL;

namespace HRFeedTester
{

    [TestClass]
    public class MessageHandlerHelpersTest
    {
        #region XMLFieldName

        [TestMethod]
        public void GetXMLFieldTest_TransactionID()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.TransactionId);
            Assert.AreEqual(result, "TRANSACTION_ID");
        }

        [TestMethod]
        public void GetXMLFieldTest_MessageTimestamp()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.MessageTimestamp);
            Assert.AreEqual(result, "TRANSACTION_TIMESTAMP");
        }

        [TestMethod]
        public void GetXMLFieldTest_TriggerType()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.TriggerType);
            Assert.AreEqual(result, "TRIGGER_TYPE");
        }

        [TestMethod]
        public void GetXMLFieldTest_TriggerAction()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.TriggerAction);
            Assert.AreEqual(result, "TRIGGER_ACTION");
        }

        [TestMethod]
        public void GetXMLFieldTest_PriorityFlag()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.PriorityFlag);
            Assert.AreEqual(result, "PRIORITY_FLAG");
        }

        [TestMethod]
        public void GetXMLFieldTest_HRID()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.HRID);
            Assert.AreEqual(result, "HR_ID");
        }

        [TestMethod]
        public void GetXMLFieldTest_FirstName()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.FirstName);
            Assert.AreEqual(result, "LEGAL_FIRST_NAME");
        }

        [TestMethod]
        public void GetXMLFieldTest_MiddleName()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.MiddleName);
            Assert.AreEqual(result, "LEGAL_MIDDLE_NAME");
        }

        [TestMethod]
        public void GetXMLFieldTest_LastName()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.LastName);
            Assert.AreEqual(result, "LEGAL_LAST_NAME");
        }

        [TestMethod]
        public void GetXMLFieldTest_CreateAccessTimestamp()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.CreateAccessTimestamp);
            Assert.AreEqual(result, "CREATE_ACCESS_DATE_TIMESTAMP");
        }

        [TestMethod]
        public void GetXMLFieldTest_Organisational_Relationship()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.Organisational_Relationship);
            Assert.AreEqual(result, "ORGANIZATIONAL_RELATIONSHIP");
        }

        [TestMethod]
        public void GetXMLFieldTest_CostCenter()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.CostCenter);
            Assert.AreEqual(result, "COST_CENTER");
        }

        [TestMethod]
        public void GetXMLFieldTest_LegalEntity()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.LegalEntity);
            Assert.AreEqual(result, "LEGAL_ENTITY");
        }

        [TestMethod]
        public void GetXMLFieldTest_UBR_CODE()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.UBR_CODE);
            Assert.AreEqual(result, "UBR_CODE");
        }

        [TestMethod]
        public void GetXMLFieldTest_LocationId()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.LocationId);
            Assert.AreEqual(result, "LOCATION_ID");
        }

        [TestMethod]
        public void GetXMLFieldTest_LocationCountry()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.LocationCountry);
            Assert.AreEqual(result, "LOCATION_COUNTRY");
        }

        [TestMethod]
        public void GetXMLFieldTest_LocationCity()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.LocationCity);
            Assert.AreEqual(result, "LOCATION_CITY");
        }

        [TestMethod]
        public void GetXMLFieldTest_ManagerHRID()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.ManagerHRID);
            Assert.AreEqual(result, "MANAGER_HR_ID");
        }

        [TestMethod]
        public void GetXMLFieldTest_ReactivationFlag()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.ReactivationFlag);
            Assert.AreEqual(result, "REACTIVATION_FLAG");
        }

        [TestMethod]
        public void GetXMLFieldTest_PreferredFirstName()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.PreferredFirstName);
            Assert.AreEqual("PREFERRED_FIRST_NAME", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_PreferredLastName()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.PreferredLastName);
            Assert.AreEqual("PREFERRED_LAST_NAME", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_AMRID()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.AMRID);
            Assert.AreEqual("AMR_ID", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_EPCode()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.EP_Code);
            Assert.AreEqual("EP_CODE", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_DBSiteIndicator()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.DBSiteIndicator);
            Assert.AreEqual("DB_SITE_INDICATOR", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_DBEmailAddress()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.DBEmailAddress);
            Assert.AreEqual("DB_EMAIL_ADDRESS", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_PhysicalAccessOnly()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.PhysicalAccessOnly);
            Assert.AreEqual("PHYSICAL_ACCESS_ONLY", result);
        }

        [TestMethod]
        public void GetXMLFieldTest_CorporateTitle()
        {
            string result = MessageHandlerHelpers.GetXMLFieldName(HRFeedMessageField.CorporateTitle);
            Assert.AreEqual("CORPORATE_TITLE", result);
        }

        [TestMethod]
        [ExpectedException((typeof(ApplicationException)))]
        public void GetXMLFieldTest_UnknownEnum()
        {
            MessageHandlerHelpers.GetXMLFieldName((HRFeedMessageField)(-1));
        }

        #endregion

        #region GetHRFeedMessage

        [TestMethod]
        public void GetHRFeedMessage()//(string inputXML, XmlNamespaceManager nsmgr, XmlNode rootNode)
        {
            string inputXML = GetXMLFromFile();

            XmlDocument theDocument = new XmlDocument();
            theDocument.LoadXml(inputXML);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
            nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

            XmlNode rootNode = theDocument.ChildNodes[1];

            HRFeedMessage message = MessageHandlerHelpers.GetHRFeedMessage(inputXML, nsmgr, rootNode);

            Assert.AreEqual(message.TransactionId, 106);

            DateTime messageTimestamp = Convert.ToDateTime("2013-11-20T11:52:00.000+01:00");
            Assert.AreEqual(message.MessageTimestamp, messageTimestamp);
            Assert.AreEqual(message.TriggerType, "JOIN");
            Assert.AreEqual(message.TriggerAction, "ADD");
            Assert.AreEqual(message.PriorityFlag, 10);
            Assert.AreEqual(message.HRID, "8000067");
            Assert.AreEqual(message.FirstName, "Solomon");
            Assert.AreEqual(message.LastName, "Quetuket");
            
            //Removed for now as this doesn't work in the winter!
            //DateTime createAccessTimestamp = Convert.ToDateTime("1900-01-01T00:01:00.000+01:00");
            //Assert.AreEqual(message.CreateAccessTimestamp, createAccessTimestamp);
            
            Assert.AreEqual(message.Organisational_Relationship, "EMP");
            Assert.AreEqual(message.CostCenter, "9731124324");
            Assert.AreEqual(message.LegalEntity, "9731");
            Assert.AreEqual(message.UBR_CODE, "G_7740");
            Assert.AreEqual(message.LocationId, "USA00020");
            Assert.AreEqual(message.LocationCountry, "USA");
            Assert.AreEqual(message.LocationCity, "Jacksonville");
            Assert.AreEqual(message.ManagerHRID, "6009178");
            Assert.AreEqual(message.ReactivationFlag, "Y");
            Assert.AreEqual(message.PreferredFirstName, "Gray");
            Assert.AreEqual(message.PreferredLastName, "Jack");
            Assert.AreEqual(message.AMRID, "1");
            Assert.AreEqual(message.DBEmailAddress, "solomon@yopmail.com");
            Assert.AreEqual(message.PhysicalAccessOnly, "N");
            Assert.AreEqual(message.DBSiteIndicator, "Y");
            Assert.AreEqual(message.EP_Code, "MAN");
            Assert.AreEqual(message.CorporateTitle, "MD");
        }

        #endregion

        #region SetHRFeedValueString

        [TestMethod]
        public void SetHRFeedValueStringTest()
        {
            string inputXML = GetXMLFromFile();

            XmlDocument theDocument = new XmlDocument();
            theDocument.LoadXml(inputXML);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
            nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

            XmlNode rootNode = theDocument.ChildNodes[1];

            HRFeedMessage message = new HRFeedMessage();

            MessageHandlerHelpers.SetHRFeedValueString(message, HRFeedMessageField.MessageTimestamp, rootNode, nsmgr);

            DateTime expected = Convert.ToDateTime("2013-11-20T11:52:00.000+01:00");
            Assert.AreEqual(expected, message.MessageTimestamp);
        }

        #endregion

        #region GetXMLValue
        [TestMethod]
        public void GetXMLValueLocationCountryTest()
        {
            string inputXML = GetXMLFromFile();

            XmlDocument theDocument = new XmlDocument();
            theDocument.LoadXml(inputXML);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
            nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

            XmlNode rootNode = theDocument.ChildNodes[1];

            string result = (string)MessageHandlerHelpers.GetXMLValue(rootNode, HRFeedMessageField.LocationCountry, nsmgr);
            Assert.AreEqual(result, "USA");
        }

        [TestMethod]
        public void GetXMLValueMessageTimestampTest()
        {
            string inputXML = GetXMLFromFile();

            XmlDocument theDocument = new XmlDocument();
            theDocument.LoadXml(inputXML);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
            nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

            XmlNode rootNode = theDocument.ChildNodes[1];

            string result = (string)MessageHandlerHelpers.GetXMLValue(rootNode, HRFeedMessageField.MessageTimestamp, nsmgr);
            Assert.AreEqual(result, "2013-11-20T11:52:00.000+01:00");
        }

        private static string GetXMLFromFile()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            StreamReader validTextStreamReader = new StreamReader(assembly.GetManifestResourceStream("HRFeedTester.XML.SampleMessageJoiner.xml"));
            return validTextStreamReader.ReadToEnd();
        }
        #endregion

        #region GenericMethods
        [TestMethod]
        public void GenericMethods_SetPropertyString()
        {
            TestObject testObject = new TestObject();

            MessageHandlerHelpers.SetProperty(testObject, "StringValue", "Test");
            Assert.AreEqual(testObject.StringValue, "Test");
        }

        [TestMethod]
        public void GenericMethods_SetPropertyInt()
        {
            TestObject testObject2 = new TestObject();
            string intString = "5";
            MessageHandlerHelpers.SetProperty(testObject2, "IntValue", intString);
            Assert.AreEqual(testObject2.IntValue, 5);
        }

        [TestMethod]
        public void GenericMethods_SetPropertyDateTime()
        {
            TestObject testObject = new TestObject();
            DateTime date = new DateTime(2014, 05, 31, 5, 30, 10);
            string dateString = date.ToString();
            MessageHandlerHelpers.SetProperty(testObject, "DateTimeValue", dateString);

            Assert.AreEqual(testObject.DateTimeValue, date);
        }

        [TestMethod]
        public void GenericMethods_SetPropertyDateTimeNullable()
        {
            TestObject testObject = new TestObject();
            DateTime? date = new DateTime(2014, 05, 31, 5, 30, 10);
            string dateString = date.ToString();
            MessageHandlerHelpers.SetProperty(testObject, "DateTimeNullableValue", dateString);

            Assert.AreEqual(testObject.DateTimeNullableValue, date);
        }

        [TestMethod]
        public void GenericMethods_ConvertToString1()
        {
            String Stringnum = MessageHandlerHelpers.ConvertToString(TestEnum.Enum1);
            Assert.AreEqual("Enum1", Stringnum);
        }

        [TestMethod]
        public void GenericMethods_ConvertToString2()
        {
            String Stringnum = MessageHandlerHelpers.ConvertToString(TestEnum.Enum2);
            Assert.AreEqual("Enum2", Stringnum);
        }

        #endregion

        #region GenerateAndSendAcknowledgementMessage

        [TestMethod]
        public void GenerateAcknowledgementMessageTest()
        {
            DateTime testDate = DateTime.Now;
            TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(testDate);

            string testDateString = testDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + utcOffset.ToString("'+'hh':'mm");
            string expectedXML = String.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?><svcAdminAcknowledgementRequest xmlns=""http://xmlns.oracle.com/svcAdminAcknowledgement""><TRANSACTION_ID>1</TRANSACTION_ID><ACKNOWLEDGEMENT_TIMESTAMP>{0}</ACKNOWLEDGEMENT_TIMESTAMP></svcAdminAcknowledgementRequest>", testDateString);

            string actualXML = MessageHandlerHelpers.GenerateAcknowledgementXML(1, testDate); 
            Assert.AreEqual(expectedXML, actualXML);
        }
        #endregion
    }
}
