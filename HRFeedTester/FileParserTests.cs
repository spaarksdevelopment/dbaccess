﻿using HRFeedMasterDataFileImport;
using HRFeedMasterDataFileImport.Helpers;
using HRFeedMasterDataFileImport.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using HRFeedMasterDataFileImport.DAL;

namespace HRFeedTester
{
    [TestClass]
    public class FileParserTests
    {
        private FileParser _Parser;

         #region Data

        private Dictionary<string, int> _OrdinalLookup =
            new Dictionary<string, int>()
            {
                {"HR ID", 0},
                {"PREFIX", 1},
                {"NAME TITLE", 2},
                {"LEGAL FIRST NAME", 3},
                {"PREFERRED FIRST NAME", 4},
                {"LEGAL MIDDLE NAME", 5},
                {"LEGAL LAST NAME", 6},
                {"PREFERRED LAST NAME", 7},
                {"PREFERRED NAME", 8},
                {"EMPLOYEE EMAIL ID", 9},
                {"EMPLOYEE CLASS", 10},
                {"EMPLOYEE STATUS", 11},
                {"ORGANIZATIONAL RELATIONSHIP", 12},
                {"CORPORATE TITLE", 13},
                {"POSITION NUMBER", 14},
                {"JOB CODE", 15},
                {"TELEPHONE NO", 16},
                {"LOCATION ID", 17},
                {"LOCATION COUNTRY", 18},
                {"LOCATION CITY", 19},
                {"COST CENTER", 20},
                {"COST CENTER DESCR", 21},
                {"LEGAL ENTITY", 22},
                {"GR DIVISION CODE", 23},
                {"UBR PRODUCT CODE", 24},
                {"UBR PRODUCT DESCR", 25},
                {"UBR SUB PRODUCT CODE", 26},
                {"UBR SUB PRODUCT DESCR", 27},
                {"EFFECTIVE DATE JOB START", 28},
                {"EFFECTIVE DATE JOB END", 29},
                {"AMR ID", 30},
                {"CREATE ACCESS DATE TIMESTAMP", 31},
                {"REVOKE ACCESS DATE TIMESTAMP", 32},
                {"MANAGER HR ID", 33},
                {"MANAGER EMAIL ADDRESS", 34},
                {"BADGE ID", 35},
                {"EXTERNAL PROVIDER", 36},
                {"BUILDING", 37},
                {"FLOOR", 38},
                {"PAO INDICATOR", 39},
                {"DB SITE INDICATOR", 40}
            };

        private Mock<IDataReader> GetMockDataReader()
        {
            var mockDataReader = new Mock<IDataReader>();

            //HR ID
            mockDataReader.Setup(x => x[0]).Returns("8000000");
            //PREFIX	
            mockDataReader.Setup(x => x[1]).Returns("DR");
            //NAME TITLE	
            mockDataReader.Setup(x => x[2]).Returns("Dr.");
            //LEGAL FIRST NAME	
            mockDataReader.Setup(x => x[3]).Returns("Candy");
             
            //PREFERRED FIRST NAME	
            mockDataReader.Setup(x => x[4]).Returns("Candy");
            
            //LEGAL MIDDLE NAME	
            mockDataReader.Setup(x => x[5]).Returns("William");
            
            //LEGAL LAST NAME	
            mockDataReader.Setup(x => x[6]).Returns("Winter");
            
            //PREFEERED LAST NAME	
            mockDataReader.Setup(x => x[7]).Returns("Winter");
            
            //PREFERRED NAME	 
            mockDataReader.Setup(x => x[8]).Returns("Winter,Candy ");
            
            //EMPLOYEE EMAIL ID	
            mockDataReader.Setup(x => x[9]).Returns("candy.winter@db.com");
            
            //EMPLOYEE CLASS	
            mockDataReader.Setup(x => x[10]).Returns("A");
            
            //EMPLOYEE STATUS	
            mockDataReader.Setup(x => x[11]).Returns("A");
            
            //ORGANIZATIONAL RELATIONSHIP
	        mockDataReader.Setup(x => x[12]).Returns("EMP");
            
            //CORPORATE TITLE	
            mockDataReader.Setup(x => x[13]).Returns("VP");
            
            //POSITION NUMBER	
            mockDataReader.Setup(x => x[14]).Returns("8000003");
            
            //JOB CODE 	
            mockDataReader.Setup(x => x[15]).Returns("A25015");
            
            //TELEPHONE NO 	
            mockDataReader.Setup(x => x[16]).Returns("+1 123 4567890");
            
            //LOCATION ID	
            mockDataReader.Setup(x => x[17]).Returns("USA00020");
            
            //LOCATION COUNTRY	
            mockDataReader.Setup(x => x[18]).Returns("DEU");
            
            //LOCATION CITY	
            mockDataReader.Setup(x => x[19]).Returns("New York");
            
            //COST CENTER	
            mockDataReader.Setup(x => x[20]).Returns("6301122436");
            
            //COST CENTER DESCR	
            mockDataReader.Setup(x => x[21]).Returns("Jax Loan Ops");
            
            //LEGAL ENTITY	
            mockDataReader.Setup(x => x[22]).Returns("6301");
            
            //GR DIVISION CODE	
            mockDataReader.Setup(x => x[23]).Returns("G_6411");
            
            //UBR PRODUCT CODE	
            mockDataReader.Setup(x => x[24]).Returns("G_8401");
            
            //UBR PRODUCT DESCR	
            mockDataReader.Setup(x => x[25]).Returns("SOM GBS");
            
            //UBR SUB PRODUCT CODE	
            mockDataReader.Setup(x => x[26]).Returns("G_7543");
            
            //UBR  SUB PRODUCT DESCR	
            mockDataReader.Setup(x => x[27]).Returns("SOM GBS Ops");
            
            //EFFECTIVE DATE JOB START	
            mockDataReader.Setup(x => x[28]).Returns("01/01/2013");
            
            //EFFECTIVE DATE JOB END	
            mockDataReader.Setup(x => x[29]).Returns("01/01/2013");
            
            //AMR ID	
            mockDataReader.Setup(x => x[30]).Returns("0");

            //CREATE ACCESS DATE TIMESTAMP	
            mockDataReader.Setup(x => x[31]).Returns("09/01/2013 13:00:00 ");

            //REVOKE ACCESS DATE TIMESTAMP	
            mockDataReader.Setup(x => x[32]).Returns("09/01/2013 13:00:00 ");

            //MANAGER HR ID	
            mockDataReader.Setup(x => x[33]).Returns("6210766");

            //MANAGER EMAIL ADDRESS	
            mockDataReader.Setup(x => x[34]).Returns("wie.dong@db.com");

            //BADGE ID	
            mockDataReader.Setup(x => x[35]).Returns("C9000000");

            //EXTERNAL PROVIDER	
            mockDataReader.Setup(x => x[36]).Returns("GERS");

            //BUILDING	
            mockDataReader.Setup(x => x[37]).Returns("ORQ");

            //FLOOR	
            mockDataReader.Setup(x => x[38]).Returns("14");

            //PHYSICAL ACCESS ONLY	
            mockDataReader.Setup(x => x[39]).Returns("N");
            
            //DB SITE INDICATOR	
            mockDataReader.Setup(x => x[40]).Returns("N");
            
            return mockDataReader;
        }
        #endregion

        [TestInitialize]
        public void Setup()
        {
            Mock<ILogging> logMock = new Mock<ILogging>();
            _Parser = new FileParser(logMock.Object);
        }

        [TestMethod]
        public void SetDateTimeField_Null()
        { 
            DateTime? parseDate = _Parser.SetDateTimeField(null);
            Assert.IsNull(parseDate);
        }

        [TestMethod]
        public void SetDateTimeField_NotNull()
        {
            string fieldToParse = "2014-01-25";

            DateTime? parseDate = _Parser.SetDateTimeField(fieldToParse);

            DateTime expected = new DateTime(2014, 1, 25);
            
            Assert.AreEqual(expected, parseDate);
        }

        [TestMethod]
        public void  SetDateTimeField_MMDDYYYY()
        {
            string fieldToParse = "01/25/2014";

            DateTime? parseDate = _Parser.SetDateTimeField(fieldToParse);

            DateTime expected = new DateTime(2014, 1, 25);

            Assert.AreEqual(expected, parseDate);
        }

        [TestMethod]
        public void SetDateTimeField_MMDDYYYY_HHMM()
        {
            string fieldToParse = "01/25/2014 10:30:23";

            DateTime? parseDate = _Parser.SetDateTimeField(fieldToParse);

            DateTime expected = new DateTime(2014, 1, 25, 10, 30, 23);

            Assert.AreEqual(expected, parseDate);
        }

        [TestMethod]
        public void GetStringConfigValueTest_Valid()
        {
            string expected = "Value1";
            string actual = HRPersonImportHelpers.GetStringConfigValue("Test", "default");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetStringConfigValueTest_Missing()
        {
            string expected = "default";
            string actual = HRPersonImportHelpers.GetStringConfigValue("TestNotThere", "default");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void When_GivenValidCSVRow_GetMasterDataImportFromCSV_PopulatesAllfields()
        {
            var mockDataReader = GetMockDataReader();

            MasterDataImport result = _Parser.GetMasterDataImportFromCSV(mockDataReader.Object, _OrdinalLookup);
            Assert.AreEqual(result.HR_ID, "8000000");
            Assert.AreEqual(result.PREFIX, "DR");
            Assert.AreEqual("Dr.", result.NAME_TITLE);
            Assert.AreEqual("Candy", result.LEGAL_FIRST_NAME);
            Assert.AreEqual("Candy", result.PREFERRED_FIRST_NAME);
            Assert.AreEqual("William", result.LEGAL_MIDDLE_NAME);
            Assert.AreEqual("Winter", result.LEGAL_LAST_NAME);
            Assert.AreEqual("Winter", result.PREFERRED_LAST_NAME);
            Assert.AreEqual("Winter,Candy ", result.PREFERRED_NAME);
            Assert.AreEqual("candy.winter@db.com", result.EMPLOYEE_EMAIL_ID);
            Assert.AreEqual("A", result.EMPLOYEE_CLASS);
            Assert.AreEqual("A", result.EMPLOYEE_STATUS);
            Assert.AreEqual("EMP", result.ORGANIZATIONAL_RELATIONSHIP);
            Assert.AreEqual("VP", result.CORPORATE_TITLE);
            Assert.AreEqual("8000003", result.POSITION_NUMBER);
            Assert.AreEqual("A25015", result.JOB_CODE);
            Assert.AreEqual("+1 123 4567890", result.TELEPHONE_NO);
            Assert.AreEqual("USA00020", result.LOCATION_ID);
            Assert.AreEqual("DEU", result.LOCATION_COUNTRY);
            Assert.AreEqual("New York", result.LOCATION_CITY);
            Assert.AreEqual("6301122436", result.COST_CENTER);
            Assert.AreEqual("Jax Loan Ops", result.COST_CENTER_DESCR);
            Assert.AreEqual("6301", result.LEGAL_ENTITY);
            Assert.AreEqual("G_6411", result.GR_DIVISION_CODE);
            Assert.AreEqual("G_8401", result.UBR_PRODUCT_CODE);
            Assert.AreEqual("SOM GBS", result.UBR_PRODUCT_DESCR);
            Assert.AreEqual("G_7543", result.UBR_SUB_PRODUCT_CODE);
            Assert.AreEqual("SOM GBS Ops", result.UBR_SUB_PRODUCT_DESCR);
            Assert.AreEqual(DateTime.Parse("01/01/2013"), result.EFFECTIVE_DATE_JOB_START);
            Assert.AreEqual(DateTime.Parse("01/01/2013"), result.EFFECTIVE_DATE_JOB_END);
            Assert.AreEqual("0", result.AMR_ID);
            Assert.AreEqual(DateTime.Parse("2013-09-01 13:00:00 "), result.CREATE_ACCESS_DATE_TIMESTAMP);
            Assert.AreEqual(DateTime.Parse("2013-09-01 13:00:00 "), result.REVOKE_ACCESS_DATE_TIMESTAMP);
            Assert.AreEqual("6210766", result.MANAGER_HR_ID);
            Assert.AreEqual("wie.dong@db.com", result.MANAGER_EMAIL_ADDRESS);
            Assert.AreEqual("C9000000", result.BADGE_ID);
            Assert.AreEqual("GERS", result.EXTERNAL_PROVIDER);
            Assert.AreEqual("ORQ", result.BUILDING);
            Assert.AreEqual("14", result.FLOOR);
            Assert.AreEqual("N", result.PAO_INDICATOR);
            Assert.AreEqual("N", result.DB_SITE_INDICATOR);
        }
        
        
        //TEST AddEntryToBadData (HRPersonImportRepo)


        //TEST IsNameValid with Preferred Name ie. NULL is valid
    }
}
