﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using HRFeedMasterDataFileImport.DAL;
using System.Data.SqlClient;
using System.Data.Entity;

namespace RepositoryIntegrationTests.Helpers
{
	public static class RepositoryTestHelperMethods
	{
		public static void ExecuteScripts(string source)
		{ 
			string[] commands = source.Split(new [] {';'},StringSplitOptions.RemoveEmptyEntries);

            using (dbAccess_LatestSchemaEntities context = new dbAccess_LatestSchemaEntities())
            {
                foreach (string command in commands)
                {
                    if (string.IsNullOrEmpty(command))
                        break;

                    if (!string.IsNullOrEmpty(command.Trim()))
                        context.Database.ExecuteSqlCommand(command, new object[0]);
                }
            }
		}

        public static int ExecuteCommand(DbContext context, string command, params object[] parameters)
        {

            return context.Database.ExecuteSqlCommand(command, parameters);
        }

        public static IEnumerable<T> ExecuteQuery<T>(DbContext context, string command, params object[] parameters)
        {

            return context.Database.SqlQuery<T>(command, parameters);
        }

        public static object ExecuteSQLFunction(string functionName, Dictionary<string, object> funcParams)
		{
			object returnValue = null;

			SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TestDB"].ConnectionString);
			SqlCommand cmd = new SqlCommand();
			SqlDataReader dr = null;
			cmd.Connection = conn;

			string stringParams = string.Empty;
			foreach (KeyValuePair<string, object> kv in funcParams)
			{
				stringParams += "@" + kv.Key + ",";
			}

			if (stringParams == string.Empty)
				cmd.CommandText = string.Format("select dbo.{0} as functionresult", functionName);
			else
			{
				stringParams=stringParams.Substring(0,stringParams.Length-1);
                cmd.CommandText = string.Format("select dbo.{0}({1}) as functionresult", functionName, stringParams);
			}

			cmd.CommandType = CommandType.Text;

			foreach (KeyValuePair<string,object> kv in funcParams)
			{
				cmd.Parameters.AddWithValue(kv.Key, kv.Value);
			}

			try
			{
				conn.Open();
				dr = cmd.ExecuteReader();
				if (dr.HasRows)
				{
					dr.Read();

					if (dr["functionResult"] == DBNull.Value)
						returnValue = null;
					else
						returnValue = dr["functionResult"];
				}
			}
			catch (SqlException ex)
			{
				throw ex;
			}
			finally
			{
				conn.Close();
				conn.Dispose();
				cmd.Dispose();
				if (dr != null)
				{
					dr.Close();
				}
			}

			return returnValue;
		}
	}
}
