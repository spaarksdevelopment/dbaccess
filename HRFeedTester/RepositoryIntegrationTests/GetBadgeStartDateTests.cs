﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests
{
    [TestClass]
    public class GetBadgeStartDateTests
    {
        [TestInitialize]
        public void Setup()
        { 
            RepositoryTestHelperMethods.ExecuteScripts(HRFeedTester.Properties.Resources.GetBadgeStartDateTestsSetup);
        }

        //[TestMethod]
        //public void GetBadgeStartDateCreateAccessDateInPast()
        //{
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("dbPeopleID", 8112877);

        //    Object result = RepositoryTestHelperMethods.ExecuteSQLFunction("GetStartDateForBadgeRequest", functionParams);

        //    Assert.IsNull(result);
        //}

        //[TestMethod]
        //public void GetBadgeStartDateNotInMasterData()
        //{
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("dbPeopleID", 1234567);

        //    Object result = RepositoryTestHelperMethods.ExecuteSQLFunction("GetStartDateForBadgeRequest", functionParams);

        //    Assert.IsNull(result);
        //}

        //Disabled this test as it only works for badge dates in the future
        //Need to amend the data accordingly
        //[TestMethod]
        //public void GetBadgeStartDateCreateAccessDateInFuture()
        //{
        //    Dictionary<string,object> functionParams = new Dictionary<string,object>();
        //    functionParams.Add("dbPeopleID", 8062652);

        //    object objectResult = RepositoryTestHelperMethods.ExecuteSQLFunction("GetStartDateForBadgeRequest", functionParams);
        //    DateTime result = (DateTime)objectResult;
        
        //    DateTime expected = new DateTime(2014,12,31,0,1,0);
        //    Assert.AreEqual(result, expected);
        //}

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(HRFeedTester.Properties.Resources.GetBadgeStartDateTestsTeardown);
        }
    }
}
