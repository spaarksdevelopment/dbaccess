﻿using System;
using System.Collections.Generic;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestCaseExtensions;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class UnsuspendMessageProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            CleanUpAndCreateDBObjects();
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "UnsuspendMessageProcessing", "InsertingNewUnsuspendMessageForSuspendedPersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewUnsuspendMessageForSuspendedPersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "suspended D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended MD with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "suspended other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "suspended NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "suspended NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewUnsuspendMessageForSuspendedPersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
            int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'Y', DateTime.Now.AddDays(-5), "JOIN", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("GenerateUnsuspendAccessRequests");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount,
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);

            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
