﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HRFeedMasterDataFileImport.DAL;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RepositoryIntegrationTests.Helpers;
using HRFeedJMSQueueHandler.DAL;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    public class MessageProcessingTestBase 
    {
        public void InsertHRFeedMessage(string forename, string surname, char reactivationFlag, DateTime terminationDate, string triggerType, string triggerAction, DateTime revokeDate)
        {
            const string accessAreaName = "Unit Test Madrid AccessArea";
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                var result = RepositoryTestHelperMethods.ExecuteCommand(context, "UnitTestInsertHRFeedMessage {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7} ", triggerType, triggerAction, forename, surname, accessAreaName, reactivationFlag, terminationDate, revokeDate);
            }
        }

        public void InsertHRFeedMessage(string forename, string surname, char reactivationFlag, DateTime terminationDate, string triggerType, string triggerAction)
        {
            InsertHRFeedMessage(forename, surname, reactivationFlag, terminationDate, triggerType, triggerAction, DateTime.Today.AddDays(-1));
        }

        public void InsertHRFeedMessage(string hrID, string forename, string surname, string prefForename, string prefSurname, string email, string UBR_Code, string costCenter, string triggerType, string triggerAction, string reactivationFlag)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                var msg = new HRFeedMessage();
                msg.HRID = hrID;
                msg.TransactionId = context.HRFeedMessages.Max(m => m.TransactionId) + 1;
                msg.FirstName = forename;
                msg.LastName = surname;
                msg.PreferredFirstName = prefForename;
                msg.PreferredLastName = prefSurname;
                msg.Processed = false;
                msg.TriggerType = triggerType;
                msg.TriggerAction = triggerAction;
                msg.ReactivationFlag = reactivationFlag;
                msg.LocationCountry = "USA";
                msg.MessageTimestamp = DateTime.Now;
                msg.LegalEntity = "1234";
                msg.DBEmailAddress = email;
                msg.UBR_CODE = UBR_Code;
                msg.CostCenter = costCenter;

                context.Entry(msg).State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public string AssessOutcome(int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount, int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, 
            int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, 
            int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            var resultBuilder = new StringBuilder();

            const string emailsCounterSqlCommand = "select count(1) from EmailLog where [To] = ";
            const string tasksCounterSqlCommand = "select count(distinct taskId) from TasksUsers join HRPerson on TasksUsers.dbPeopleID = HRPerson.dbPeopleID where EmailAddress = ";

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                //TODO: check outcome; 

                var actualSuperPassOfficerEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, emailsCounterSqlCommand + "'Faith.Test@db.com'").Single();
                var actualHomeCountryPassOfficerEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, emailsCounterSqlCommand + "'Bridgette.Test@db.com'").Single();
                var actualAccessAreaPassOfficerEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    emailsCounterSqlCommand + "'Roman.Test@db.com'").Single();
                var actualNonAccessAreaPassOfficerInHomeCountryEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    emailsCounterSqlCommand + "'Juan.Test@db.com'").Single();
                var actualInactivePassOfficerEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    emailsCounterSqlCommand + "'Kristi.Test@db.com'").Single();
                var actualEmailDisabledPassOfficerEmailCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                   emailsCounterSqlCommand + "'Muller.Test@db.com'").Single();
                
                var actualSuperPassOfficerTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, tasksCounterSqlCommand + "'Faith.Test@db.com'").Single();
                var actualHomeCountryPassOfficerTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, tasksCounterSqlCommand + "'Bridgette.Test@db.com'").Single();
                var actualAccessAreaPassOfficerTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    tasksCounterSqlCommand + "'Roman.Test@db.com'").Single();
                var actualNonAccessAreaPassOfficerInHomeCountryTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                        tasksCounterSqlCommand + "'Juan.Test@db.com'").Single();
                var actualInactivePassOfficerTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    tasksCounterSqlCommand + "'Kristi.Test@db.com'").Single();
                var actualEmailDisabledPassOfficerTaskCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context,
                    tasksCounterSqlCommand + "'Muller.Test@db.com'").Single();


                if (superPassOfficerTaskCount != actualSuperPassOfficerTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Super Pass Officer task count: expected<{0}>, actual<{1}>.\r\n",
                        superPassOfficerTaskCount, actualSuperPassOfficerTaskCount);

                if (homeCountryPassOfficerTaskCount != actualHomeCountryPassOfficerTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Home Country Pass Officer task count: expected<{0}>, actual<{1}>.\r\n",
                        homeCountryPassOfficerTaskCount, actualHomeCountryPassOfficerTaskCount);

                if (accessAreaPassOfficerTaskCount != actualAccessAreaPassOfficerTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Access Area Pass Officer task count: expected<{0}>, actual<{1}>.\r\n",
                        accessAreaPassOfficerTaskCount, actualAccessAreaPassOfficerTaskCount);

                if(nonAccessAreaPassOfficerInHomeCountryTaskCount != actualNonAccessAreaPassOfficerInHomeCountryTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Non Access Area PassOfficer In Home Country Task Count: expected<{0}>, actual<{1}>.\r\n",
                        nonAccessAreaPassOfficerInHomeCountryTaskCount, actualNonAccessAreaPassOfficerInHomeCountryTaskCount);

                if (inactivePassOfficerTaskCount != actualInactivePassOfficerTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Inactive Pass Officer task count: expected<{0}>, actual<{1}>.\r\n",
                        inactivePassOfficerTaskCount, actualInactivePassOfficerTaskCount);

                if (superPassOfficerEmailCount != actualSuperPassOfficerEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Super Pass Officer email count: expected<{0}>, actual<{1}>.\r\n",
                        superPassOfficerEmailCount, actualSuperPassOfficerEmailCount);

                if (homeCountryPassOfficerEmailCount != actualHomeCountryPassOfficerEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Home Country Pass Officer email count: expected<{0}>, actual<{1}>.\r\n",
                        homeCountryPassOfficerEmailCount, actualHomeCountryPassOfficerEmailCount);

                if (accessAreaPassOfficerEmailCount != actualAccessAreaPassOfficerEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Access Area Pass Officer email count: expected<{0}>, actual<{1}>.\r\n",
                        accessAreaPassOfficerEmailCount, actualAccessAreaPassOfficerEmailCount);

                if (nonAccessAreaPassOfficerInHomeCountryEmailCount != actualNonAccessAreaPassOfficerInHomeCountryEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Non Access Area Pass Officer in home country email count: expected<{0}>, actual<{1}>.\r\n",
                        nonAccessAreaPassOfficerInHomeCountryEmailCount, actualNonAccessAreaPassOfficerInHomeCountryEmailCount);

                if (inactivePassOfficerEmailCount != actualInactivePassOfficerEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Inactive Pass Officer email count: expected<{0}>, actual<{1}>.\r\n",
                        inactivePassOfficerEmailCount, actualInactivePassOfficerEmailCount);

                if (emailDisabledPassOfficerTaskCount != actualEmailDisabledPassOfficerTaskCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Email Disabled Pass Officer task count: expected<{0}>, actual<{1}>.\r\n",
                        emailDisabledPassOfficerTaskCount, actualEmailDisabledPassOfficerTaskCount);

                if (emailDisabledPassOfficerEmailCount != actualEmailDisabledPassOfficerEmailCount)
                    resultBuilder.AppendFormat(
                        "Incorrect Email Disabled Pass Officer email count: expected<{0}>, actual<{1}>.\r\n",
                        emailDisabledPassOfficerEmailCount, actualEmailDisabledPassOfficerEmailCount);
            }

            return resultBuilder.ToString();
        }

        protected void CleanUpAndCreateDBObjects()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingCleanupSetup);
            RepositoryTestHelperMethods.ExecuteScripts(
                Resources.CreateLocationAndPassofficesMessageProcessingTestsSetup);
            RepositoryTestHelperMethods.ExecuteScripts(
                Resources.CreatePersonsWithActiveAccessMessageProcessingTestsSetup);
            RepositoryTestHelperMethods.ExecuteScripts(
                Resources.CreatePersonsWithRevokedAccessMessageProcessingTestsSetup);
            RepositoryTestHelperMethods.ExecuteScripts(
                Resources.CreatePersonsWithSuspendAccessMessageProcessingTestsSetup);
            using (var context = new dbAccess_LatestSchemaEntities())
            {
                var result = RepositoryTestHelperMethods.ExecuteCommand(context,
                    Resources.sp_UnitTestInsertHRFeedMessage);
            }
        }
    }
}