﻿using System;
using System.Collections.Generic;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestCaseExtensions;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class RevokeMessageProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            CleanUpAndCreateDBObjects();
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "RevokeMessageProcessing", "InsertingNewRevokeMessageForActivePersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewRevokeMessageForActivePersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "active D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewRevokeMessageForActivePersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
            int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-1), "LVER", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedRevoke");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, emailDisabledPassOfficerTaskCount, 
                superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "RevokeMessageProcessing", "InsertingNewRevokeMessageForRevokedPersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewRevokeMessageForRevokedPersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        public void InsertingNewRevokeMessageForRevokedPersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount, 
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount,
            int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-1), "LVER", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedRevoke");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, 
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, inactivePassOfficerEmailCount, 
                emailDisabledPassOfficerEmailCount);

            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "RevokeMessageProcessing", "InsertNewRevokeMessageHavingTerminationDateBeforeRevokeDateForActivePerson", DataAccessMethod.Sequential)]
        public void InsertNewRevokeMessageHavingTerminationDateBeforeRevokeDateForActivePerson()
        {

        }

        [TestCase("Unit Test", "active D with access", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active D without access", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active MD with access", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active MD without access", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active D with access building pass office", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active MD with access building pass office", 8, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active other with access", 6, 2, 2, 2, 0, 0, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active other without access", 4, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 0)]
        [TestCase("Unit Test", "active other with access building pass office", 4, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 0)]
        [TestCase("Unit Test", "active NULL with access", 6, 2, 2, 2, 0, 0, 2, 2, 2, 2, 0, 0)]
        [TestCase("Unit Test", "active NULL without access", 4, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 0)]
        public void InsertNewRevokeMessageHavingTerminationDateBeforeRevokeDateForActivePerson(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
           int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
           int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-1), "LVER", "ADD", DateTime.Today.AddDays(20));

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedRevoke");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, emailDisabledPassOfficerTaskCount,
                superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
