﻿using System;
using System.Collections.Generic;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestCaseExtensions;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class ReactivateMessageProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            CleanUpAndCreateDBObjects();
        }
                
        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "ReactivateMessageProcessing", "InsertingNewReactivateMessageForRevokedPersonForWithinWeekProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewReactivateMessageForRevokedPersonForWithinWeekProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewReactivateMessageForRevokedPersonForWithinWeekProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
            int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'Y', DateTime.Now.AddDays(-5), "JOIN", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedReactivate");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount,
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);

            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "ReactivateMessageProcessing", "InsertingNewReactivateMessageForRevokedPersonForMoreThanWeekAndLessThanMonthProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewReactivateMessageForRevokedPersonForMoreThanWeekAndLessThanMonthProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D without access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other without access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building po 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access 8 - 30 ago", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        public void InsertingNewReactivateMessageForRevokedPersonForMoreThanWeekAndLessThanMonthProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
           int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
           int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'Y', DateTime.Now.AddDays(-5), "JOIN", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedReactivate");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount,
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);

            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
