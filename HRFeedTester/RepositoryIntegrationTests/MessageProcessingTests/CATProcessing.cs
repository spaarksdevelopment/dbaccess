﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RepositoryIntegrationTests.Helpers;
using HRFeedTester.Properties;
using MSTestCaseExtensions;
using HRFeedJMSQueueHandler.DAL;
using System.Text;
using System.Data.Entity;
using System.Linq;
using HRFeedJMSQueueHandler.CAT;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class CATProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            CleanUp();
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingCleanupSetup);
            RepositoryTestHelperMethods.ExecuteScripts(Resources.CreateLocationAndPassofficesMessageProcessingTestsSetup);
            RepositoryTestHelperMethods.ExecuteScripts(Resources.CATProcessingTestsSetup);
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "CATProcessing", "InsertCAT_ADD", DataAccessMethod.Sequential)]
        public void InsertCAT_ADD()
        {
        }
                
        [TestCase("9007712", "Unit Test", "Existing HRPerson with security group", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678")]
        [TestCase("8547763", "Unit Test", "Existing HRPerson and mpUser", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678")]
        [TestCase("8547762", "Unit Test", "Existing HRPerson", "UT", "NewPerson", "", "G_9696", "12345678")]
        [TestCase("8547761", "Unit Test", "New HRPerson", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678")]
        public void InsertCAT_ADD(string hrID, string forename, string surname, string prefForename, string prefSurname, string email, string UBR_Code, string costCenter)
        {
            InsertHRFeedMessage(hrID, forename, surname, prefForename, prefSurname, email, UBR_Code, costCenter, "JOIN", "ADD", "N");

            CATProcessor catProcessor = new CATProcessor();
            catProcessor.ProcessTriggers();

            ProcessOutcomeCAT_ADDorUPD(hrID, forename, surname, prefForename, prefSurname, email, UBR_Code, costCenter);
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "CATProcessing", "InsertCAT_UPD", DataAccessMethod.Sequential)]
        public void InsertCAT_UPD()
        {
        }

        [TestCase("9007712", "Unit Test", "Existing HRPerson with security group", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678", false)]
        [TestCase("8547763", "Unit Test", "Existing HRPerson and mpUser", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678", false)]
        [TestCase("8547762", "Unit Test", "Existing HRPerson", "UT", "NewPerson", "", "G_9696", "12345678", false)]
        [TestCase("8547761", "Unit Test", "New HRPerson", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678", true)]
        public void InsertCAT_UPD(string hrID, string forename, string surname, string prefForename, string prefSurname, string email, string UBR_Code, string costCenter, bool isNewUser = false)
        {
            InsertHRFeedMessage(hrID, forename, surname, prefForename, prefSurname, email, UBR_Code, costCenter, "JOIN", "UPD", "N");

            CATProcessor catProcessor = new CATProcessor();
            catProcessor.ProcessTriggers();

            using (var context = new DbAccess_ImprovementsEntities())
            {
                int dbPplID = int.Parse(hrID);
                var hrPerson = context.HRPersons.Where(p => p.dbPeopleID == dbPplID).FirstOrDefault();

                ProcessOutcomeCAT_ADDorUPD(hrID, forename, surname, prefForename, prefSurname, email, UBR_Code, costCenter);
            }
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "CATProcessing", "InsertCAT_DEL", DataAccessMethod.Sequential)]
        public void InsertCAT_DEL()
        {
        }

        [TestCase("1234568", "Unit Test", "Person with actioned requests", "", "", "unittest@db.com", "G_9667", "123232323", 1, 1)]
        [TestCase("1234569", "Unit Test", "Person with pending requests", "", "", "unittest@db.com", "G_9667", "123232323", 0, 0)]
        [TestCase("9007712", "Unit Test", "Existing HRPerson with security group", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678", 0, 0)]
        [TestCase("8547763", "Unit Test", "Existing HRPerson and mpUser", "UT", "NewPerson", "unittest@db.com", "G_9696", "12345678", 0, 0)]
        [TestCase("8547762", "Unit Test", "Existing HRPerson", "UT", "NewPerson", "", "G_9696", "12345678", 0, 0)]
        public void InsertCAT_DEL(string hrID, string forename, string surname, string prefForename, string prefSurname, string email, string UBR_Code, string costCenter, int revokeRequestCount, int offboardTriggerCount)
        {
            InsertHRFeedMessage(hrID, forename, surname, prefForename, prefSurname, email, UBR_Code, costCenter, "JOIN", "DEL", "N");

            CATProcessor catProcessor = new CATProcessor();
            catProcessor.ProcessTriggers();

            ProcessOutcomeCAT_DEL(hrID, revokeRequestCount, offboardTriggerCount);
        }

        private void ProcessOutcomeCAT_ADDorUPD(string hrID, string forename, string surname, string prefForename, string prefSurname, string email, string UBR_Code, string costCenter)
        {
            var errors = new StringBuilder();

            using (var context = new DbAccess_ImprovementsEntities())
            {
                int dbPplID = int.Parse(hrID);
                var hrPerson = context.HRPersons.Where(p => p.dbPeopleID == dbPplID).FirstOrDefault();

                Assert.IsNotNull(hrPerson);
                Assert.AreEqual(hrID, hrPerson.dbPeopleID.ToString());
                Assert.AreEqual(forename, hrPerson.Forename);
                Assert.AreEqual(surname, hrPerson.Surname);
                Assert.AreEqual(prefForename, hrPerson.PreferredFirstName);
                Assert.AreEqual(prefSurname, hrPerson.PreferredLastName);
                Assert.IsNull(hrPerson.Revoked);
                Assert.AreEqual("A", hrPerson.Class);
                Assert.AreEqual("A", hrPerson.Status);
                if (!string.IsNullOrWhiteSpace(email))
                    Assert.AreEqual(hrPerson.EmailAddress, email);

                var mp_user = context.mp_User.Where(u => u.dbPeopleID == hrPerson.dbPeopleID).FirstOrDefault();
                if (mp_user != null)
                {
                    Assert.AreEqual(mp_user.Forename, forename);
                    Assert.AreEqual(mp_user.Surname, surname);
                    Assert.IsNull(mp_user.Revoked);

                    if (!string.IsNullOrWhiteSpace(email))
                        Assert.AreEqual(mp_user.EmailAddress, email);
                }
            }
        }

        private void ProcessOutcomeCAT_DEL(string hrID, int revokeRequestCount, int offboardTriggerCount)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                int dbPplID = int.Parse(hrID);
                var hrPerson = context.HRPersons.Where(p => p.dbPeopleID == dbPplID).FirstOrDefault();

                Assert.IsTrue((bool)hrPerson.Revoked);

                var mp_user = context.mp_User.Where(u => u.dbPeopleID == hrPerson.dbPeopleID).FirstOrDefault();
                if (mp_user != null)
                    Assert.IsTrue((bool)mp_user.Revoked);

                string pendingPassOrAccReqCountQuery = "select count(distinct r.RequestID) from AccessRequest r inner join AccessRequestPerson p on r.RequestPersonID = p.RequestPersonID"
                                                + " where r.RequestTypeID in (1, 2) and r.RequestStatusID in (1, 2, 3) and p.dbPeopleID = " + hrID;

                string pendingRevokeRequestCountQuery = "select count(distinct r.RequestID) from AccessRequest r inner join AccessRequestPerson p on r.RequestPersonID = p.RequestPersonID"
                                                + " where r.RequestTypeID in (16) and r.RequestStatusID in (1, 2, 3) and p.dbPeopleID = " + hrID;

                string pendingOffBoardTriggerCountQuery = "select count(*) from CATDeleteOffboard where processed=0 and dbPeopleId= " + hrID;

                var pendingReqCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, pendingPassOrAccReqCountQuery).Single();
                var pendingRevokeRequestCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, pendingRevokeRequestCountQuery).Single();
                var pendingOffboardTriggerCount = RepositoryTestHelperMethods.ExecuteQuery<int>(context, pendingOffBoardTriggerCountQuery).Single();

                Assert.AreEqual(0, pendingReqCount);
                Assert.AreEqual(revokeRequestCount, pendingRevokeRequestCount);
                Assert.AreEqual(offboardTriggerCount, pendingOffboardTriggerCount);
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.CATProcessingTestsTearDown);
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
