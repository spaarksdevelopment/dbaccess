﻿using System;
using System.Collections.Generic;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestCaseExtensions;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class SuspendMessageProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            CleanUpAndCreateDBObjects();
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "SuspendMessageProcessing", "InsertingNewSuspendMessageForActivePersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewSuspendMessageForActivePersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "active D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewSuspendMessageForActivePersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount,
            int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-1), "SUSP", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("GenerateSuspendAccessRequests");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, emailDisabledPassOfficerTaskCount,
                superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "SuspendMessageProcessing", "InsertingNewSuspendMessageForSuspendedPersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewSuspendMessageForSuspendedPersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "suspended D with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended D without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended MD with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended MD without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended D with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended MD with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended other with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended other without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended other with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended NULL with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "suspended NULL without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        public void InsertingNewSuspendMessageForSuspendedPersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, int accessAreaPassOfficerTaskCount,
            int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount,
            int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-1), "SUSP", "ADD");

            RepositoryTestHelperMethods.ExecuteScripts("HRFeedRevoke");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, inactivePassOfficerEmailCount,
                emailDisabledPassOfficerEmailCount);

            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
