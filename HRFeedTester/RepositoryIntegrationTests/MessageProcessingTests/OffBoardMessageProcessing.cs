﻿using System;
using System.Collections.Generic;
using HRFeedTester.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestCaseExtensions;
using RepositoryIntegrationTests.Helpers;

namespace HRFeedTester.RepositoryIntegrationTests.MessageProcessingTests
{
    [TestClass]
    public class OffBoardMessageProcessing : MessageProcessingTestBase
    {
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Setup()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
            CleanUpAndCreateDBObjects();
        }
        
        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "OffBoardMessageProcessing", "InsertingNewOffboardMessageForActivePersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewOffboardMessageForActivePersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "active D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "active NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewOffboardMessageForActivePersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount, 
            int accessAreaPassOfficerTaskCount, int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount, 
            int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-8), "LVER", "ADD", DateTime.Now.AddDays(-8));

            RepositoryTestHelperMethods.ExecuteScripts("FourtyEightHourOffboarding");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, 
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, 
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }
                
        [TestCategory("Integration")]
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "OffBoardMessageProcessing", "InsertingNewOffboardMessageForRevokedPersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewOffboardMessageForRevokedPersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked D without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access",  4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office", 4, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building pass office", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access", 3, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access", 2, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0)]
        public void InsertingNewOffboardMessageForRevokedPersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount,
            int accessAreaPassOfficerTaskCount, int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount,
            int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(7), "LVER", "ADD", DateTime.Now.AddDays(-8));

            RepositoryTestHelperMethods.ExecuteScripts("FourtyEightHourOffboarding");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount, 
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount, 
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "OffBoardMessageProcessing", "InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForActivePersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForActivePersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        public void InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForActivePersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount,
            int accessAreaPassOfficerTaskCount, int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount,
            int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(-3), "LVER", "ADD", DateTime.Now.AddDays(-3));

            RepositoryTestHelperMethods.ExecuteScripts("FourtyEightHourOffboarding");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount,
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "OffBoardMessageProcessing", "InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForRevokedPersonProducesCorrectOutcome", DataAccessMethod.Sequential)]
        public void InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForRevokedPersonProducesCorrectOutcome()
        {
        }

        [TestCase("Unit Test", "revoked D with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked D with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked MD with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked other with access building pass office", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL with access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [TestCase("Unit Test", "revoked NULL without access", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        public void InsertingNewOffboardMessageHavingRevokeDateWithinLast7DaysForRevokedPersonProducesCorrectOutcome(string forename, string surname, int superPassOfficerTaskCount, int homeCountryPassOfficerTaskCount,
            int accessAreaPassOfficerTaskCount, int nonAccessAreaPassOfficerInHomeCountryTaskCount, int inactivePassOfficerTaskCount, int emailDisabledPassOfficerTaskCount, int superPassOfficerEmailCount,
            int homeCountryPassOfficerEmailCount, int accessAreaPassOfficerEmailCount, int nonAccessAreaPassOfficerInHomeCountryEmailCount, int inactivePassOfficerEmailCount, int emailDisabledPassOfficerEmailCount)
        {
            InsertHRFeedMessage(forename, surname, 'N', DateTime.Now.AddDays(7), "LVER", "ADD", DateTime.Now.AddDays(-3));

            RepositoryTestHelperMethods.ExecuteScripts("FourtyEightHourOffboarding");

            var failureMessage = AssessOutcome(superPassOfficerTaskCount, homeCountryPassOfficerTaskCount, accessAreaPassOfficerTaskCount, nonAccessAreaPassOfficerInHomeCountryTaskCount, inactivePassOfficerTaskCount,
                emailDisabledPassOfficerTaskCount, superPassOfficerEmailCount, homeCountryPassOfficerEmailCount, accessAreaPassOfficerEmailCount, nonAccessAreaPassOfficerInHomeCountryEmailCount,
                inactivePassOfficerEmailCount, emailDisabledPassOfficerEmailCount);
            Assert.IsTrue(string.IsNullOrWhiteSpace(failureMessage), failureMessage);
        }

        [TestCleanup]
        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(Resources.MessageProcessingTestsTeardown);
        }
    }
}
