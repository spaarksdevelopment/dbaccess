﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RepositoryIntegrationTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedTester.RepositoryIntegrationTests
{
    [TestClass]
    public class CreateChangeStartDateRepoTests
    {
        private DateTime _SummerDate = new DateTime(2014, 6, 25, 0, 1, 0);
        private DateTime _WinterDate = new DateTime(2014, 2, 25, 0, 1, 0);

        private DateTime GetDateTimeInUKFromTimeInAnotherTimeZone(DateTime otherDateTime, string timeZoneID)
        {
            TimeZoneInfo ukTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            TimeZoneInfo otherTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);

            TimeSpan otherUTCOffset = otherTimeZoneInfo.GetUtcOffset(otherDateTime);
            DateTimeOffset otherOffset = new DateTimeOffset(otherDateTime, otherUTCOffset);
            DateTimeOffset ukOffset = TimeZoneInfo.ConvertTime(otherOffset, ukTimeZoneInfo);

            return ukOffset.DateTime;
        }

        private bool DatesWithinTwoHours(DateTime first, DateTime second)
        {
            TimeSpan difference = first.Subtract(second);
            TimeSpan absoluteDifference = difference.Duration();
            
            TimeSpan sixhours = new TimeSpan(2, 0, 0);

            return absoluteDifference < sixhours;
        }

        //[TestMethod]
        //public void ConvertDateToUK_Summer_German()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_SummerDate, "Central European Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _SummerDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "Germany");
        //    functionParams.Add("CityName", "Berlin");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void ConvertDateToUK_Summer_Singapore()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_SummerDate, "Singapore Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _SummerDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "Singapore");
        //    functionParams.Add("CityName", "Singapore");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void ConvertDateToUK_Summer_US()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_SummerDate, "US Eastern Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _SummerDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void ConvertDateToUK_Winter_German()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "Central European Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "Germany");
        //    functionParams.Add("CityName", "Berlin");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void ConvertDateToUK_Winter_Singapore()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "Singapore Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "Singapore");
        //    functionParams.Add("CityName", "Singapore");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void ConvertDateToUK_Winter_US()
        //{
        //    DateTime expected = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");
        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("LocalDate", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    DateTime? actual = (DateTime?)RepositoryTestHelperMethods.ExecuteSQLFunction("GetUKDate", functionParams);

        //    bool result = DatesWithinTwoHours(expected, actual.Value);
        //    Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void DoWeNeedToGenerateChangeStartDateTasks_FirstDateMoreThanSixHoursBefore()
        //{
        //    //US Time is 00:01 in master data. Convert to UK (+5 hours) to simulate date in the trigger
        //    DateTime ukDateFromUSTime = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");

        //    //Adjust the date to pass the test
        //    ukDateFromUSTime = ukDateFromUSTime.Subtract(new TimeSpan(10, 0, 0));

        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("CreateAccessDateTrigger", ukDateFromUSTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CreateAccessDateMasterData", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    bool? actual = (bool?)RepositoryTestHelperMethods.ExecuteSQLFunction("DoWeNeedToGenerateChangeStartDateTasks", functionParams);

        //    Assert.IsTrue(actual.Value);
        //}

        //[TestMethod]
        //public void DoWeNeedToGenerateChangeStartDateTasks_FirstDateMoreThanSixHoursAfter()
        //{
        //    //US Time is 00:01 in master data. Convert to UK (+5 hours) to simulate date in the trigger
        //    DateTime ukDateFromUSTime = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");

        //    //Adjust the date to pass the test
        //    ukDateFromUSTime = ukDateFromUSTime.Add(new TimeSpan(10, 0, 0));

        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("CreateAccessDateTrigger", ukDateFromUSTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CreateAccessDateMasterData", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    bool? actual = (bool?)RepositoryTestHelperMethods.ExecuteSQLFunction("DoWeNeedToGenerateChangeStartDateTasks", functionParams);

        //    Assert.IsTrue(actual.Value);
        //}

        //[TestMethod]
        //public void DoWeNeedToGenerateChangeStartDateTasks_FirstDateOneHoursBefore()
        //{
        //    //US Time is 00:01 in master data. Convert to UK (+5 hours) to simulate date in the trigger
        //    DateTime ukDateFromUSTime = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");

        //    //Adjust the date to pass the test
        //    ukDateFromUSTime = ukDateFromUSTime.Subtract(new TimeSpan(1, 0, 0));

        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("CreateAccessDateTrigger", ukDateFromUSTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CreateAccessDateMasterData", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    bool? actual = (bool?)RepositoryTestHelperMethods.ExecuteSQLFunction("DoWeNeedToGenerateChangeStartDateTasks", functionParams);

        //    Assert.IsFalse(actual.Value);
        //}

        //[TestMethod]
        //public void DoWeNeedToGenerateChangeStartDateTasks_FirstDateOneHoursAfter()
        //{
        //    //US Time is 00:01 in master data. Convert to UK (+5 hours) to simulate date in the trigger
        //    DateTime ukDateFromUSTime = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");

        //    //Adjust the date to pass the test
        //    ukDateFromUSTime = ukDateFromUSTime.Add(new TimeSpan(1, 0, 0));

        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("CreateAccessDateTrigger", ukDateFromUSTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CreateAccessDateMasterData", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    bool? actual = (bool?)RepositoryTestHelperMethods.ExecuteSQLFunction("DoWeNeedToGenerateChangeStartDateTasks", functionParams);

        //    Assert.IsFalse(actual.Value);
        //}

        //[TestMethod]
        //public void DoWeNeedToGenerateChangeStartDateTasks_DatesEqual()
        //{
        //    //US Time is 00:01 in master data. Convert to UK (+5 hours) to simulate date in the trigger
        //    DateTime ukDateFromUSTime = GetDateTimeInUKFromTimeInAnotherTimeZone(_WinterDate, "US Eastern Standard Time");

        //    Dictionary<string, object> functionParams = new Dictionary<string, object>();
        //    functionParams.Add("CreateAccessDateTrigger", ukDateFromUSTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CreateAccessDateMasterData", _WinterDate.ToString("yyyy-MM-dd HH:mm:ss"));
        //    functionParams.Add("CountryName", "United States Of America");
        //    functionParams.Add("CityName", "New York");

        //    bool? actual = (bool?)RepositoryTestHelperMethods.ExecuteSQLFunction("DoWeNeedToGenerateChangeStartDateTasks", functionParams);

        //    Assert.IsFalse(actual.Value);
        //}
    }
}
