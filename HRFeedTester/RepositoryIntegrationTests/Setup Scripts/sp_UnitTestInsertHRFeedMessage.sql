Create Procedure UnitTestInsertHRFeedMessage
(
	@TriggerType nvarchar(4)
	,@TriggerAction nvarchar(3)
	,@FirstName nvarchar(60)
	,@LastName nvarchar(60)
	,@AccessAreaName nvarchar(50)
	,@ReactivationFlag nvarchar(1)
	,@TerminationDate datetime2
	,@RevokeDate datetime2
	)

As
Begin
DECLARE @DBPeopleID INT

SELECT @DBPeopleID=dbPeopleID FROM HRPerson WHERE Forename = @FirstName and Surname = @LastName

INSERT INTO HRFeedMessage
           ([TransactionId]
           ,[MessageTimestamp]
           ,[TriggerType]
           ,[TriggerAction]
           ,[PriorityFlag]
           ,[HRID]
           ,[FirstName]
           ,[LastName]
           ,[Organisational_Relationship]
           ,[LocationId]
           ,[LocationCountry]
           ,[LocationCity]
           ,[ManagerHRID]
           ,[ReactivationFlag]
           ,[TerminationDate]
		   ,[Processed]
		   ,[RevokeProcessed]
		   ,[ReactivateProcessed]
		   ,[RevokeAccessDateTimestamp]
           )
     VALUES
           ((SELECT MAX(TransactionId) + 1 FROM HRFeedMessage)
           ,GETDATE()
           ,@TriggerType
           ,@TriggerAction
           ,10
           ,@DBPeopleID
           ,@FirstName
           ,@LastName
           ,'EMP'
           ,(SELECT AccessAreaID FROM AccessArea WHERE Name = @AccessAreaName)
           ,(SELECT CostCentreCode FROM LocationCountry WHERE CountryID = 
				(SELECT CountryID FROM LocationCity WHERE CityID = 
					(SELECT CityID FROM LocationBuilding WHERE BuildingID = 
						(SELECT BuildingID FROM AccessArea WHERE Name = @AccessAreaName))))
           ,(SELECT Name FROM LocationCity WHERE CityID = 
				(SELECT CityID FROM LocationBuilding WHERE BuildingID = 
					(SELECT BuildingID FROM AccessArea WHERE Name = @AccessAreaName)))
           ,22313
           ,@ReactivationFlag
           ,@TerminationDate
		   ,0
		   ,0
		   ,0
		   ,@RevokeDate 
           );

INSERT INTO MasterData (HR_ID,dbPeopleID, FIRST_NAME) VALUES (@DBPeopleID, @DBPeopleID, @FirstName); 

END

