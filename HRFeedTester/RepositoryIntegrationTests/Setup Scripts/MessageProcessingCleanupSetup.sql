IF OBJECT_ID('UnitTestInsertHRFeedMessage','P') IS NOT NULL DROP PROCEDURE UnitTestInsertHRFeedMessage;
update HRFeedMessage set Processed = 1 where Processed = 0; 
update CATDeleteOffboard set Processed = 1 where Processed = 0
delete from HRFeedMessage where FirstName='Unit Test';
delete from MasterData where FIRST_NAME = 'Unit Test';
update LocationPassOffice set Enabled = 0; 
delete from HRPersonAccessArea where dbPeopleID in (select dbPeopleID from HRPerson where Forename = 'Unit Test');
delete from LocationPassOfficeUser where PassOfficeID in (select [PassOfficeID] from [dbo].[LocationPassOffice] where [Code] like 'Unit Test%');
delete from AccessRequestMaster where Comment like 'Unit Test%';
delete from AccessRequestPerson where dbPeopleID in (select dbPeopleID from HRPerson where Forename = 'Unit Test');
delete from AccessRequest where PassOfficeID in (select PassOfficeID from LocationPassOffice where [Code] like 'Unit Test%');
delete from LocationPassOffice where Code like 'Unit Test%';
delete from AccessArea where Name like 'Unit Test%';
delete from LocationBuilding where Name like 'Unit Test%';
delete from LocationCity where Code like 'Unit Test%';
delete from HRPerson where Forename = 'Unit Test' OR Surname = 'Test';
delete from mp_User where Forename = 'Unit Test';
delete from EmailLog where body like '% Unit Test %';
delete from hrperson where dbpeopleid in (1234571,1234570,1234569,1234568,1234567);