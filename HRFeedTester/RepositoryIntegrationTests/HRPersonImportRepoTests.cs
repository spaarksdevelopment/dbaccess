﻿using HRFeedMasterDataFileImport.DAL;
using HRFeedMasterDataFileImport.DAL.Abstract;
using HRFeedMasterDataFileImport.DAL.Concrete;
using HRFeedMasterDataFileImport.HRPersonImport;
using HRFeedTester.TestData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RepositoryIntegrationTests.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace HRFeedTester.RepositoryIntegrationTests
{
    [TestClass]
    public class HRPersonImportRepoTests
    {
        IHRPersonImportRepo _Repo;
        
        [TestInitialize]
        public void Setup()
        {
            _Repo = new HRPersonImportRepo();
            RepositoryTestHelperMethods.ExecuteScripts(HRFeedTester.Properties.Resources.HRPersonRepoTestsSetup);
        }

        [TestMethod]
        public void GetMasterDataTest()
        {
            string HRID = "8062652";

            MasterDataImport actual = _Repo.GetMasterData(HRID);

            var expected = HRPersonImportTestData.GetMasterDataImportFakeData();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsCountryInDBAccess_Exists()
        {
            bool countryExists = _Repo.IsCountryInDBAccess("GBR");
            Assert.IsTrue(countryExists);
        }

        [TestMethod]
        public void IsCountryInDBAccess_DoesntExist()
        {
            bool countryExists = _Repo.IsCountryInDBAccess("AFG");
            Assert.IsFalse(countryExists);
        }

        [TestMethod]
        public void IsCityInDBAccess_DoesntExist()
        {
            bool cityExists = _Repo.IsCountryInDBAccess("Stirling");
            Assert.IsFalse(cityExists);
        }

        //TODO Data is wrong, so comment out for now (no longer used in DBAccess anyway)
        //[TestMethod]
        //public void GetDivPath()
        //{
        //    string ubrCode = "G_2802";
        //    string actualDivPath = _Repo.GetDivPath(ubrCode);
        //    string expectedDivPath = "G_2216/G_6811/G_7489/G_7122/G_2792/G_2802/";
        //    Assert.AreEqual(expectedDivPath, actualDivPath);
        //}

        [TestMethod]
        public void GetClasses()
        {
            List<string> actual = _Repo.GetClasses();

            List<string> expected = HRPersonImportTestData.GetClasses();

            bool result = Enumerable.SequenceEqual(expected.OrderBy(t => t), actual.OrderBy(t => t));

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void AddEntryToBadData()
        {
            HRPersonImportValidationFlags flags = HRPersonImportValidationFlags.InvalidCityName | HRPersonImportValidationFlags.InvalidClass;

            MasterDataImport masterDataImportRow = HRPersonImportTestData.GetMasterDataImportFakeData();
            _Repo.AddEntryToBadData(masterDataImportRow, flags);

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImportBadData badData = context.MasterDataImportBadDatas.Where(a => a.HR_ID == masterDataImportRow.HR_ID).First();
                Assert.AreEqual((int)flags, badData.ErrorFlags);
                Assert.AreEqual(masterDataImportRow.HR_ID, badData.HR_ID);

                context.MasterDataImportBadDatas.Remove(badData);
                context.SaveChanges();
            }
        }

        [TestMethod]
        public void AddEntryToBadDataNewFlags()
        {
            HRPersonImportValidationFlags flags = HRPersonImportValidationFlags.InvalidPreferredFirstName | HRPersonImportValidationFlags.InvalidPreferredLastName;

            MasterDataImport masterDataImportRow = HRPersonImportTestData.GetMasterDataImportFakeData();
            _Repo.AddEntryToBadData(masterDataImportRow, flags);

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImportBadData badData = context.MasterDataImportBadDatas.Where(a => a.HR_ID == masterDataImportRow.HR_ID).First();
                Assert.AreEqual((int)flags, badData.ErrorFlags);
                Assert.AreEqual(masterDataImportRow.HR_ID, badData.HR_ID);

                context.MasterDataImportBadDatas.Remove(badData);
                context.SaveChanges();
            }
        }

        [TestMethod]
        public void GetCountryNameFromISO_CountryExists()
        {
            string country = _Repo.GetCountryNameFromISO("CHN");
            Assert.AreEqual("China", country);
        }

        [TestMethod]
        public void GetCountryNameFromISO_ISOExistsCountryDoesntExist()
        {
            string country = _Repo.GetCountryNameFromISO("AFG");
            Assert.IsNull(country);
        }

        [TestMethod]
        public void GetCountryNameFromISO_ISODoesntExist()
        {
            string country = _Repo.GetCountryNameFromISO("XHG");
            Assert.IsNull(country);
        }

        [TestMethod]
        public void RemoveEntryFromMasterData_Exists()
        {
            string hrIDToRemove = "8062652";

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImport actual = context.MasterDataImports.Where(a => a.HR_ID == hrIDToRemove).FirstOrDefault();
                Assert.IsNotNull(actual);
            }

            _Repo.RemoveEntryFromMasterData(hrIDToRemove);

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImport actual = context.MasterDataImports.Where(a => a.HR_ID == hrIDToRemove).FirstOrDefault();
                Assert.IsNull(actual);
            }
        }

        [TestMethod]
        public void RemoveEntryFromMasterData_DoesntExist()
        {
            string hrIDToRemove = "0";

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImport actual = context.MasterDataImports.Where(a => a.HR_ID == hrIDToRemove).FirstOrDefault();
                Assert.IsNull(actual);
            }

            _Repo.RemoveEntryFromMasterData(hrIDToRemove);

            using (var context = new dbAccess_LatestSchemaEntities())
            {
                MasterDataImport actual = context.MasterDataImports.Where(a => a.HR_ID == hrIDToRemove).FirstOrDefault();
                Assert.IsNull(actual);
            }
        }

        public void Teardown()
        {
            RepositoryTestHelperMethods.ExecuteScripts(HRFeedTester.Properties.Resources.HRPersonRepoTestsTeardown);
        }
    }
}
