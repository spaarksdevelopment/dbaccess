﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRFeedJMSQueueHandler.CAT;
using Moq;
using HRFeedJMSQueueHandler.DAL;
using HRFeedTester.TestData;
using System.Text;

namespace HRFeedTester
{
    [TestClass]
    public class HRPersonValidatorTests
    {
        private HRPersonValidator _hrPersonValidator;
        private Mock<ICATRepository> _catRepo;

        [TestInitialize]
        public void HRPersonValidatorTestsInit()
        {
            _catRepo = new Mock<ICATRepository>();
            _hrPersonValidator = new HRPersonValidator(_catRepo.Object);
        }

        [TestMethod]
        public void IsNameValid_InvalidChars()
        {
            bool result = _hrPersonValidator.IsNameValid("Bob#");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob[");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob]");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob=");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("+Bob");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob^");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob&");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob$");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob!");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsNameValid("Bob*");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNameValid_ValidChars()
        {
            bool result = _hrPersonValidator.IsNameValid("Bob-");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob'");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob ");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid(" Bob");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob.");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob,");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob?");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob(");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob)");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bobü");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bobä");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bobö");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bobß");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Bob67");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameValid_NormalNames()
        {
            bool result = _hrPersonValidator.IsNameValid("Schuster-Brommenschenkel");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Sommer");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Weisenfeld-Dünnebier");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Schuster-Brommenschenkel");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Grämmel");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Bölinger");
            Assert.IsTrue(result);

            result = _hrPersonValidator.IsNameValid("Prölß");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameValid_UnusualNames()
        {
            bool result = _hrPersonValidator.IsNameValid("Harris-(Sturt)");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Schmidt1");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Hillingsh?er");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameValid("Doerr,-Dr.");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameNullOrValid()
        {
            bool result = _hrPersonValidator.IsNameNullOrValid("");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameNullOrValid(null);
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsNameNullOrValid(" ");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsUBRValid_Valid()
        {
            bool result = _hrPersonValidator.IsUBRValid("G_1234");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsUBRValid_Null()
        {
            bool result = _hrPersonValidator.IsUBRValid(null);
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsUBRValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCityNameValid_Invalid()
        {
            StringBuilder b = new StringBuilder();
            b.Append("0123456789");//10
            b.Append("0123456789");//20
            b.Append("0123456789");//30
            b.Append("0123456789");//40
            b.Append("0123456789");//50
            b.Append("0");//1
            bool result = _hrPersonValidator.IsCityNameValid(b.ToString());

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCityNameValid_TooLong()
        {
            StringBuilder builder = new StringBuilder("0123456789"); //10
            builder.Append("0123456789"); //20
            builder.Append("0123456789"); //30
            builder.Append("0123456789"); //40
            builder.Append("0123456789"); //50
            builder.Append("0"); //51

            string cityName = builder.ToString();

            bool result = _hrPersonValidator.IsCityNameValid(cityName);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCityNameValid_Null()
        {
            bool result = _hrPersonValidator.IsCityNameValid(null);
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsCityNameValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Invalid()
        {
            _catRepo.Setup(a => a.IsCountryInDBAccess("XX4")).Returns(false);
            bool result = _hrPersonValidator.IsCountryNameValid("XX4");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Valid()
        {
            _catRepo.Setup(a => a.IsCountryInDBAccess("GBR")).Returns(true);
            bool result = _hrPersonValidator.IsCountryNameValid("GBR");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Null()
        {
            bool result = _hrPersonValidator.IsCountryNameValid(null);
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsCountryNameValid(string.Empty);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Null()
        {
            bool result = _hrPersonValidator.IsOfficerIDValid(null);
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Valid()
        {
            bool result = _hrPersonValidator.IsOfficerIDValid("VP");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("AS");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("AVP");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("AYST");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("D");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("MD");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("NA");
            Assert.IsTrue(result);
            result = _hrPersonValidator.IsOfficerIDValid("NC");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Invalid()
        {
            bool result = _hrPersonValidator.IsOfficerIDValid("VPAS");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsOfficerIDValid("1");
            Assert.IsFalse(result);
            result = _hrPersonValidator.IsOfficerIDValid("DMDNC");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsEmailValid_Valid()
        {
            string email = "martin.steel@db.com";
            bool result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsTrue(result);

            email = "petra.kiefer-buedinger@db.com";
            result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsEmailValid_InValid()
        {
            string email = "martin.steel@db";
            bool result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsFalse(result);

            email = "petra.kiefer-buedingerdb.com";
            result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsFalse(result);

            email = "12martin.steel@db";
            result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsEmailValid_Null()
        {
            string email = null;
            bool result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsTrue(result);

            email = string.Empty;
            result = _hrPersonValidator.IsEmailValid(email);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsValidHRID_Valid()
        {
            string hrID = "1234567";
            bool result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsTrue(result);

            hrID = "0234567";
            result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsValidHRID_InValid()
        {
            string hrID = "12345678";
            bool result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsFalse(result);

            hrID = "123456";
            result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsFalse(result);

            hrID = "C1234567";
            result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsFalse(result);

            hrID = "HRIDInValid";
            result = _hrPersonValidator.IsHRIDValid(hrID);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_FakeDataNoError()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);
            
            HRPersonImportValidationFlags errorFlags =  _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual(0, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidHRID()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.HRID = "12345";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidHRID, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_NamewithSpecialChars()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.FirstName = "Ricky,";
            data.LastName = "Ponting45";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual(0, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidFisrtName()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.FirstName = "Wijitha=";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidFirstName, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_NullOrEmptyPreferredName()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.PreferredFirstName = null;
            data.PreferredLastName = string.Empty;

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual(0, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidPreferrdName()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.FirstName = "wiji=";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidFirstName, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_NullDBEmail()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.DBEmailAddress = null;

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual(0, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_EmptyDBEmail()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.DBEmailAddress = string.Empty;

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual(0, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidEmail()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.DBEmailAddress = "ricky.gmail.com";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidEmail , (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidCorporateTitle()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.CorporateTitle = "UAT";

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidOfficerID, (int)errorFlags);
        }

        [TestMethod]
        public void ValidateHRFeedMessageRow_InvalidCountry()
        {
            HRFeedMessage data = HRPersonImportTestData.GetHRFeedMessageFakeDate();
            _catRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(false);

            HRPersonImportValidationFlags errorFlags = _hrPersonValidator.ValidateHRFeedMessageRow(data);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidCountryName, (int)errorFlags);
        }
    }
}
