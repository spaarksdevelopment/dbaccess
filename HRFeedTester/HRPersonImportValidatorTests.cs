﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using Moq;
using HRFeedMasterDataFileImport.HRPersonImport;
using HRFeedMasterDataFileImport.DAL.Abstract;
using HRFeedMasterDataFileImport.DAL;
using HRFeedTester.TestData;

namespace HRFeedTester
{
    [TestClass]
    public class HRPersonImportValidatorTests
    {
        private HRPersonImportValidator _HRPersonImportValidator;
        private Mock<IHRPersonImportRepo> _IHRPersonImportRepo;

        [TestInitialize]
        public void HRPersonImportTestsInit()
        {
            _IHRPersonImportRepo = new Mock<IHRPersonImportRepo>();
            _IHRPersonImportRepo.Setup(a => a.GetClasses()).Returns(HRPersonImportTestData.GetClasses());

            _HRPersonImportValidator = new HRPersonImportValidator(_IHRPersonImportRepo.Object);
        }

        [TestMethod]
        public void IsNameValid_InvalidChars()
        {
            bool result = _HRPersonImportValidator.IsNameValid("Bob#");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob[");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob]");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob=");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("+Bob");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob^");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob&");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob$");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob!");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsNameValid("Bob*");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNameValid_ValidChars()
        {
            bool result = _HRPersonImportValidator.IsNameValid("Bob-");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob'");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob ");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid(" Bob");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob.");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob,");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob?");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob(");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob)");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bobü");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bobä");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bobö");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bobß");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Bob67");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameValid_NormalNames()
        {
            bool result = _HRPersonImportValidator.IsNameValid("Schuster-Brommenschenkel");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Sommer");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Weisenfeld-Dünnebier");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Schuster-Brommenschenkel");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Grämmel");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Bölinger");
            Assert.IsTrue(result);

            result = _HRPersonImportValidator.IsNameValid("Prölß");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameValid_UnusualNames()
        {
            bool result = _HRPersonImportValidator.IsNameValid("Harris-(Sturt)");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Schmidt1");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Hillingsh?er");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameValid("Doerr,-Dr.");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNameNullOrValid()
        {
            bool result = _HRPersonImportValidator.IsNameNullOrValid("");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameNullOrValid(null);
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsNameNullOrValid(" ");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsUBRValid_Valid()
        {
            bool result = _HRPersonImportValidator.IsUBRValid("G_1234");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsUBRValid_Null()
        {
            bool result = _HRPersonImportValidator.IsUBRValid(null);
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsUBRValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCityNameValid_Invalid()
        {
            StringBuilder b = new StringBuilder();
            b.Append("0123456789");//10
            b.Append("0123456789");//20
            b.Append("0123456789");//30
            b.Append("0123456789");//40
            b.Append("0123456789");//50
            b.Append("0");//1
            bool result = _HRPersonImportValidator.IsCityNameValid(b.ToString());

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCityNameValid_TooLong()
        {
            StringBuilder builder = new StringBuilder("0123456789"); //10
            builder.Append("0123456789"); //20
            builder.Append("0123456789"); //30
            builder.Append("0123456789"); //40
            builder.Append("0123456789"); //50
            builder.Append("0"); //51

            string cityName = builder.ToString();

            bool result = _HRPersonImportValidator.IsCityNameValid(cityName);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCityNameValid_Null()
        {
            bool result = _HRPersonImportValidator.IsCityNameValid(null);
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsCityNameValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Invalid()
        {
            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("XX4")).Returns(false);
            bool result = _HRPersonImportValidator.IsCountryNameValid("XX4");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Valid()
        {
            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("GBR")).Returns(true);
            bool result = _HRPersonImportValidator.IsCountryNameValid("GBR");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsCountryNameValid_Null()
        {
            bool result = _HRPersonImportValidator.IsCountryNameValid(null);
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsCountryNameValid(string.Empty);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsStatusValid_Null()
        {
            bool result = _HRPersonImportValidator.IsStatusValid(null);
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsStatusValid(string.Empty);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsStatusValid_Valid()
        {
            bool result = _HRPersonImportValidator.IsStatusValid("A");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("D");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("I");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("L");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("P");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("R");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsStatusValid("T");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsStatusValid_Invalid()
        {
            bool result = _HRPersonImportValidator.IsStatusValid("S");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsStatusValid("1");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsStatusValid("AD");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Null()
        {
            bool result = _HRPersonImportValidator.IsOfficerIDValid(null);
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Valid()
        {
            bool result = _HRPersonImportValidator.IsOfficerIDValid("VP");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("AS");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("AVP");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("AYST");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("D");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("MD");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("NA");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("NC");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsOfficerIDValid_Invalid()
        {
            bool result = _HRPersonImportValidator.IsOfficerIDValid("VPAS");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("1");
            Assert.IsFalse(result);
            result = _HRPersonImportValidator.IsOfficerIDValid("DMDNC");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsDivPathValid_Valid()
        {
            bool result = _HRPersonImportValidator.IsDivPathValid("G_0183/G_2530/G_2538////////");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsDivPathValid("G_0183/G_2530/G_2447/G_3066/G_4514//////");
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsDivPathValid("G_0183/G_4596/G_2482/G_6284/G_6289/G_2898/G_5914/G_6303/G_1059/G_8293/G_8227/G_1228/G_1231");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsDivPathValid_Null()
        {
            bool result = _HRPersonImportValidator.IsDivPathValid(null);
            Assert.IsTrue(result);
            result = _HRPersonImportValidator.IsDivPathValid(string.Empty);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SetFlag_NoFlagsSet()
        {
            HRPersonImportValidationFlags flags = new HRPersonImportValidationFlags();

            bool result = _HRPersonImportValidator.AreThereAnyErrors(flags);
            Assert.IsFalse(result);

            _HRPersonImportValidator.SetErrorMessage(ref flags, HRPersonImportValidationFlags.InvalidCityName);
            result = _HRPersonImportValidator.AreThereAnyErrors(flags);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsEmailValid_Valid()
        {
            string email = "martin.steel@db.com";
            bool result = _HRPersonImportValidator.IsEmailValid(email);
            Assert.IsTrue(result);

            email = "petra.kiefer-buedinger@db.com";
            result = _HRPersonImportValidator.IsEmailValid(email);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsEmailValid_Null()
        {
            string email = null;
            bool result = _HRPersonImportValidator.IsEmailValid(email);
            Assert.IsTrue(result);

            email = string.Empty;
            result = _HRPersonImportValidator.IsEmailValid(email);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsClassValid_Null()
        {
            string personClass = null;
            bool result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);

            personClass = string.Empty;
            result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsClassValid_Valid()
        {
            string personClass = "X";
            bool result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);

            personClass = "1";
            result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsClassValid_Invalid()
        {
            string personClass = "AZ";
            bool result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);

            personClass = "*";
            result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);

            personClass = "A";
            result = _HRPersonImportValidator.IsClassValid(personClass);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataNoErrors()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual(0, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataEmptyPreferredLastNameAndFirstName()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeDataEmptyPreferredFirstAndPreferredLast();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual(0, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadCountry()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(false);

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidCountryName, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadEmail()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.EMPLOYEE_EMAIL_ID = "bob@smith@something.db.com";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidEmail, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadHRID()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.HR_ID = "D1234567";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidHRID, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadName()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.LEGAL_FIRST_NAME = "&&&&";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidFirstName, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadLastName()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.LEGAL_LAST_NAME = "&&&&";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidLastName, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadClassInternal()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.EMPLOYEE_CLASS = "*";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual(0, (int)errors);
        }
 
        [TestMethod]
        public void ValidateMasterData_FakeDataBadStatus()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.EMPLOYEE_STATUS = "%";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidStatus, (int)errors);
        }

        [TestMethod]
        public void ValidateMasterData_FakeDataBadOfficerID()
        {
            MasterDataImport data = HRPersonImportTestData.GetMasterDataImportFakeData();
            string divpath = HRPersonImportTestData.GetDivPathFakeData();

            _IHRPersonImportRepo.Setup(a => a.IsCountryInDBAccess("CHN")).Returns(true);

            data.CORPORATE_TITLE = "UMN";

            HRPersonImportValidationFlags errors = _HRPersonImportValidator.ValidateMasterDataRow(data, divpath);

            Assert.AreEqual((int)HRPersonImportValidationFlags.InvalidOfficerID, (int)errors);
        }

        [TestMethod]
        public void GetErrorMessage_AllErrors()
        {
            HRPersonImportValidationFlags flags = HRPersonImportTestData.GetAllFlags();
            string allErrorMessage = HRPersonImportTestData.GetAllErrorMessage();

            string hrID = "C1234567";
            string actualMessage = _HRPersonImportValidator.GetErrorMessage(flags, hrID);
            Assert.AreEqual(allErrorMessage, actualMessage);
        }

        [TestMethod]
        public void GetErrorMessages_SingleErrors()
        {
            HRPersonImportValidationFlags flags = new HRPersonImportValidationFlags();
            flags |= HRPersonImportValidationFlags.InvalidCityName;
            string singleErrorMessage = HRPersonImportTestData.GetCityNameErrorMessage();

            string hrID = "C1234567"; 
            string actualMessage = _HRPersonImportValidator.GetErrorMessage(flags, hrID);
            Assert.AreEqual(singleErrorMessage, actualMessage);
        }

        [TestMethod]
        public void GetErrorMessages_TwoErrors()
        {
            HRPersonImportValidationFlags flags = new HRPersonImportValidationFlags();
            flags |= HRPersonImportValidationFlags.InvalidHRID;
            flags |= HRPersonImportValidationFlags.InvalidEmail;

            string twoErrorsMessage = HRPersonImportTestData.GetHRIDANDEmailErrorMessage();
            string hrID = "C1234567"; 
            string actualMessage = _HRPersonImportValidator.GetErrorMessage(flags, hrID);
            Assert.AreEqual(twoErrorsMessage, actualMessage);
        }
    }
}
