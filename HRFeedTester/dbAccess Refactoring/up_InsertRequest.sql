USE [DbAccess_LatestSchema]
GO
/****** Object:  StoredProcedure [dbo].[up_InsertRequest]    Script Date: 4/11/2014 3:51:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================= 
-- Author:        <AIS> 
-- Create date: <09/02/2011> 
-- Description:   <Insert the Request> 
-- History:
--     11/04/2014 (CM) - Remove old code which fills in BusinessAreaID and DivisionID
-- ============================================= 

ALTER PROCEDURE [dbo].[up_InsertRequest] 
( 
      @CreatedByUserID Int 
      ,@RequestTypeID Int 
      ,@AccessAreaID Int 
      ,@RequestPersonID Int 
      ,@StartDate DateTime 
      ,@EndDate DateTime 
      ,@BadgeID Int 
      ,@DivisionID Int 
      ,@iPassOfficeID Int 
      ,@BusinessAreaID Int 
      ,@RequestID Int Output 
      ,@ExternalSystemRequestID Int = null 
      ,@ExternalSystemID Int = null 
      ,@ClassificationID Int 
) 
AS 
Begin 
                
          --If PassOfficeID is null 
          --        Calculate PassOfficeID 
          --                If AccessArea is not null then use that country as the pass office 
          --                Otherwise use the home pass office 
          --                        --find out the person's home pass office 
                                        --declare @homePassOfficeID int 
                                        --select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp 
                                        --inner join LocationCountry lc on lc.CountryID = arp.CountryID 
                                        --where arp.RequestPersonID = @RequestPersonID 
          --        Set @iPassOfficeID 
          IF @iPassOfficeID IS NULL 
          BEGIN 
                  IF @AccessAreaID IS NOT NULL 
                  BEGIN 
                          SELECT  @iPassOfficeID=LCC.PassOfficeID 
                          FROM dbo.AccessArea 
                          INNER JOIN dbo.LocationBuilding LB ON AccessArea.BuildingID = LB.BuildingID 
                          INNER JOIN dbo.LocationCity  LC  ON LB.CityID  =LC.CityID 
                          INNER JOIN dbo.LocationCountry LCC  ON LC.CountryID =LCC.COuntryID 
						  WHERE dbo.AccessArea.AccessAreaID=@AccessAreaID
                  END                   
                  ELSE 
                  BEGIN 
                        select @iPassOfficeID = PassOfficeID
						from 
						AccessRequestPerson arp 
						inner join LocationCountry lc on lc.CountryID = arp.CountryID 
						where arp.RequestPersonID = @RequestPersonID 
                  END 
          END 


      Set NoCount On; 

      Set @RequestID = 0; 

      Declare @dtNow DateTime2 = SysDateTime(); 
      Declare @bNewTransaction Bit = (Case @@TranCount When 0 Then 1 Else 0 End); 

      Begin Try 
            --If (1 = @bNewTransaction) Begin Transaction; 

            Insert Into 
                  AccessRequest 
            ( 
                  RequestTypeID 
                  ,AccessAreaID 
                  ,RequestPersonID 
                  ,StartDate 
                  ,EndDate 
                  ,RequestStatusID 
                  ,Approved 
                  ,ApprovedDate 
                  ,BadgeID 
                  ,DivisionID 
                  ,PassOfficeID 
                  ,BusinessAreaID 
                  ,CreatedByID 
                  ,Created 
                  ,ExternalSystemRequestID 
                  ,ExternalSystemID 
                  ,Classification 
            ) 
            Values 
            ( 
                  @RequestTypeID 
                  ,@AccessAreaID 
                  ,@RequestPersonID 
                  ,@StartDate 
                  ,@EndDate 
                  ,1 
                  ,null 
                  ,null 
                  ,@BadgeID 
                  ,@DivisionID 
                  ,@iPassOfficeID 
                  ,@BusinessAreaID 
                  ,@CreatedByUserID 
                  ,@dtNow 
                  ,@ExternalSystemRequestID 
                  ,@ExternalSystemID 
                  ,@ClassificationID 
            ); 

            Set @RequestID = Scope_Identity(); 

            --If (1 = @bNewTransaction) Commit Transaction; 

      End Try 
      Begin Catch 

            If (1 = @bNewTransaction) Rollback Transaction; 

            -- raise an error with the details of the exception 

            Declare @iState Int; 
            Declare @iSeverity Int; 
            Declare @sMessage NVarChar(4000); 

            Select 
                  @iState = Error_State (), 
                  @sMessage = Error_Message (), 
                  @iSeverity = Error_Severity () 

            RaisError (@sMessage, @iSeverity, @iState); 

      End Catch 
End; 



