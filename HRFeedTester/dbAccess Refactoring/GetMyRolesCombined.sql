USE [DbAccess_LatestSchema]
GO
/****** Object:  StoredProcedure [dbo].[GetMyRolesCombined]    Script Date: 4/11/2014 3:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		MCS
-- Create date: 05.10.2012
-- Description:	Combines all my roles into one query
-- History:
--     11/04/2014 (CM) - Removed Business Area Approver roles
-- =============================================

ALTER PROCEDURE [dbo].[GetMyRolesCombined]
	@dbPeopleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    DECLARE @languageID int
    SET @languageID=ISNULL((SELECT LanguageID FROM mp_User WHERE dbpeopleID=@dbPeopleID),1)

	--Access Approver Roles
	SELECT 					
					ISNULL(CASE @languageID WHEN 1 THEN vw_MyRoles.RoleName  WHEN 2 THEN vw_MyRoles.RoleGermanName WHEN 3 THEN vw_MyRoles.RoleItalyName END,'') AS RoleName,
					vw_MyRoles.RoleID,
					vw_MyRoles.DateAccepted,
					MAX(RecertificationRoles.DateRecertified) AS DateRecertified,
					'' AS HRClassificationID,
					ISNULL(vw_MyRoles.DivisionName,'') As DivisionName,
					ISNULL(vw_AccessArea.RegionName,'') As RegionName,
					ISNULL(vw_AccessArea.CountryName,'') AS CountryName,
					ISNULL(vw_AccessArea.CityName,'') AS CityName,
					ISNULL(vw_AccessArea.BuildingName,'') AS BuildingName,
					ISNULL(vw_AccessArea.LandlordID, '') AS LandlordID,
					ISNULL(vw_AccessArea.FloorName,'') AS FloorName,
					ISNULL(vw_AccessArea.AccessAreaName,'') AS AccessAreaName,
					ISNULL(hrc.Classification,'') As Classification,
					ISNULL(CASE @languageID WHEN 1 THEN vw_AccessArea.AccessAreaTypeName WHEN 2 THEN vw_AccessArea.AccessAreaTypeGermanName WHEN 3 THEN vw_AccessArea.AccessAreaTypeItalyName END,'') AS AccessAreaType,
					vw_MyRoles.CorporateDivisionAccessAreaID,
					vw_MyRoles.CorporateDivisionAccessAreaApproverId,
					'' AS DivisionRoleUserID,
					'' VendorName,
					'' MSPID,
					'' PassOfficeName,
					'' PassOfficeEmail,
					'' PassOfficeUserID			
	FROM
					vw_MyRoles	
					LEFT OUTER JOIN vw_AccessArea ON vw_MyRoles.AccessAreaID = vw_AccessArea.AccessAreaID
					LEFT OUTER JOIN	RecertificationRoles ON vw_MyRoles.CorporateDivisionAccessAreaApproverId = 	RecertificationRoles.ParentId AND vw_MyRoles.mp_SecurityGroupID = RecertificationRoles.RoleId
					LEFT JOIN dbo.CorporateDivisionAccessAreaApprover cdaa on vw_MyRoles.CorporateDivisionAccessAreaApproverId = cdaa.CorporateDivisionAccessAreaApproverId
					LEFT OUTER JOIN HRClassification hrc ON cdaa.Classification = hrc.HRClassificationID
	WHERE     
					vw_MyRoles.dbPeopleID = @dbPeopleID
					--set the value of role id's to get the 
					AND	
					vw_MyRoles.mp_SecurityGroupID In (5,8)
	Group By 
					CASE @languageID WHEN 1 THEN vw_AccessArea.AccessAreaTypeName WHEN 2 THEN vw_AccessArea.AccessAreaTypeGermanName WHEN 3 THEN vw_AccessArea.AccessAreaTypeItalyName END,
					vw_AccessArea.AccessAreaName,
					FloorName,
					BuildingName,
					CityName,
					vw_AccessArea.CountryName,
					RegionName,
					vw_MyRoles.DivisionName,
					CASE @languageID WHEN 1 THEN vw_MyRoles.RoleName  WHEN 2 THEN vw_MyRoles.RoleGermanName WHEN 3 THEN vw_MyRoles.RoleItalyName END,
					LandlordID,
					vw_MyRoles.DateAccepted,
					hrc.Classification,
					vw_MyRoles.CorporateDivisionAccessAreaID,
					hrc.HRClassificationID,
					vw_MyRoles.CorporateDivisionAccessAreaApproverId,
					vw_MyRoles.RoleID
	UNION 
	--Division Roles		
	SELECT 					
					ISNULL(CASE @languageID WHEN 1 THEN vw_MyRoles.RoleName  WHEN 2 THEN vw_MyRoles.RoleGermanName WHEN 3 THEN vw_MyRoles.RoleItalyName END,'') AS RoleName,
					vw_MyRoles.RoleID,
					vw_MyRoles.DateAccepted,
					MAX(RecertificationRoles.DateRecertified) AS DateRecertified,
					'' AS HRClassificationID,
					ISNULL(vw_MyRoles.DivisionName,'') As DivisionName,
					lr.Name RegionName,
					ISNULL(vw_MyRoles.CountryName,'') AS CountryName,
					'' AS CityName,
					'' AS BuildingName,
					'' AS LandlordID,
					'' AS FloorName,
					'' AS AccessAreaName,
					'' As Classification,
					'' AS AccessAreaType,
					'' AS CorporateDivisionAccessAreaID,
					'' AS CorporateDivisionAccessAreaApproverId,
					vw_MyRoles.DivisionRoleUserID,
					'' VendorName,
					'' MSPID,
					'' PassOfficeName,
				    '' PassOfficeEmail,
					'' PassOfficeUserID		
	FROM
					vw_MyRoles						
					LEFT OUTER JOIN	RecertificationRoles ON vw_MyRoles.DivisionRoleUserID = RecertificationRoles.ParentId AND vw_MyRoles.mp_SecurityGroupID = RecertificationRoles.RoleId
					JOIN dbo.LocationCountry lc ON vw_MyRoles.CountryID = lc.CountryID
					JOIN dbo.LocationRegion lr ON lc.RegionID=lr.RegionID
	WHERE     
					vw_MyRoles.dbPeopleID = @dbPeopleID
					--set the value of role id's to get the 
					AND	vw_MyRoles.mp_SecurityGroupID IN (3,7)
	Group By 
					CASE @languageID WHEN 1 THEN vw_MyRoles.RoleName  WHEN 2 THEN vw_MyRoles.RoleGermanName WHEN 3 THEN vw_MyRoles.RoleItalyName END,
					vw_MyRoles.DivisionName,
					lr.Name,
					vw_MyRoles.CountryName,
					vw_MyRoles.DateAccepted,
					vw_MyRoles.DivisionRoleUserID,
					vw_MyRoles.RoleID
	UNION				
	SELECT
					ISNULL(CASE @languageID WHEN 1 THEN mpsg.RoleName  WHEN 2 THEN mpsg.GermanName WHEN 3 THEN mpsg.ItalyName END,'') AS RoleName,
					mpsg.ID AS RoleID,
					mpru.DateAccepted,
					NULL DateRecertified,
					'' AS HRClassificationID,
					'' DivisionName,	
					lr.Name RegionName,
					lcu.Name CountryName,		
					'' CityName,
					'' BuildingName,
					'' LandlordID,
					'' FloorName,
					'' AccessAreaName,
					'' Classification,
					'' AccessAreaType,
					'' CorporateDivisionAccessAreaID,
					'' CorporateDivisionAccessAreaApproverId,
					'' DivisionRoleUserID,
					hrvd.VendorName,
					mpru.MSPId,
					'' PassOfficeName,
				    '' PassOfficeEmail,
					'' PassOfficeUserID
	FROM
					MSPRoleUser AS mpru
					INNER JOIN		HRVendorCountry as hrvdc ON mpru.VendorCountryID = hrvdc.VendorCountryID
					INNER JOIN      HRVendor as hrvd ON hrvd.VendorId = hrvdc.VendorID
					INNER JOIN		LocationCountry AS lcu ON lcu.CountryID = hrvdc.CountryID
					INNER JOIN		dbo.LocationRegion lr ON lcu.RegionID=lr.RegionID
					INNER JOIN		HRPerson AS hrp ON hrp.dbPeopleID = mpru.dbPeopleID
					INNER JOIN      mp_SecurityGroupUser AS mspgu ON mspgu.dbPeopleID = hrp.dbPeopleID
					INNER JOIN      mp_SecurityGroup as mpsg ON mspgu.SecurityGroupID=mpsg.Id

	WHERE
					mpru.dbPeopleID = @dbPeopleID
					AND		mpsg.Id = 9
	UNION
	SELECT 
				    mpsgu.RoleName,
				    mpsgu.ID AS RoleID,
				    [DateAccepted],
				    [DateLastCertified] DateRecertified,
					'' AS HRClassificationID,
					'' DivisionName,	
					'' RegionName,
					'' CountryName,		
					'' CityName,
					'' BuildingName,
					'' LandlordID,
					'' FloorName,
					'' AccessAreaName,
					'' Classification,
					'' AccessAreaType,
					'' CorporateDivisionAccessAreaID,
					'' CorporateDivisionAccessAreaApproverId,
					'' DivisionRoleUserID,
					'' VendorName,
					'' MSPId,
				    lpo.Name AS PassOfficeName,
				    lpo.Email AS PassOfficeEmail,
					lpu.PassOfficeUserID
	FROM 
					[LocationPassOfficeUser] as lpu
					INNER JOIN  [LocationPassOffice] AS lpo ON lpo.PassOfficeID = lpu.PassOfficeID
					INNER JOIN  mp_SecurityGroup AS mpsgu ON mpsgu.Id = lpu.mp_SecurityGroupID
	WHERE
					mpsgu.Id = 6
					AND  lpu.dbPeopleID = @dbPeopleID
					AND  lpu.[IsEnabled] = 1
END