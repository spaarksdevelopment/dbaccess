USE [DbAccess_LatestSchema]
GO
/****** Object:  StoredProcedure [dbo].[up_InsertRequestPerson]    Script Date: 4/11/2014 3:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Authors:			AIS, SC
-- Created:			09/02/2011
-- Description:
--		Submits a master request, and generates tasks
-- History:
--		24/06/2011 (SC) - major update to fix the way that the business area is calculated
--						- also reworked the code to be laid out more clearly and use try/catch/raiserror
--      11/04/2014 (CM) - removed code to fill out BusinessArea fields
-- =============================================
ALTER PROCEDURE [dbo].[up_InsertRequestPerson]
(
	@CreatedByUserID Int,
	@RequestMasterID Int,
	@dbPeopleID Int,
	@RequestPersonID Int Output,
	@bCheckBusinessArea Bit
)
As
Begin

	Set NoCount On;

	Set @RequestPersonID = 0;

	Set @bCheckBusinessArea = IsNull (@bCheckBusinessArea, 0);

	Declare @dtNow DateTime2 = SysDateTime();

	Declare @iCount Int =
	(
		Select
			Count (1)
		From
			AccessRequestPerson
		Where
			dbPeopleID = @dbPeopleID
			And
			RequestMasterID = @RequestMasterID
	);

	If (0 != @iCount)
	Begin

		-- the person is already on this request

		Set @RequestPersonID =
		(
			Select
				RequestPersonID
			From
				AccessRequestPerson
			Where
				dbPeopleID = @dbPeopleID
				And
				RequestMasterID = @RequestMasterID
		);

		Set @RequestPersonID = -1;
		Return;

	End

	Begin Try

		Insert Into
			AccessRequestPerson
		(
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorID
			,VendorName
			,RequestMasterID
			,CreatedByID
			,Created
		)
		Select
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorId
			,VendorName
			,@RequestMasterID
			,@CreatedByUserID
			,@dtNow
		From
			HRPerson
		Where
			dbPeopleID = @dbPeopleID;

		Set @RequestPersonID = Scope_Identity();

	End Try
	Begin Catch

			Declare @iState Int;
			Declare @iSeverity Int;
			Declare @sMessage NVarChar(4000);

			Select
			@iState = Error_State (),
			@sMessage = Error_Message (),
			@iSeverity = Error_Severity ()

			PRINT CAST(@iState as varchar(100)) + cAST(@sMessage as varchar(1000)) + cAST(@iSeverity as varchar(100))

		Set @RequestPersonID = 0;
	End Catch

End;


