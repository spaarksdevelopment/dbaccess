﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessControlSystemInterface
{
    public interface IVisitorControl
    {
        string InstanceName { get; }

        bool RegisterVisitor(string VisitorName, string VisitorCompany, DateTime StartDate, DateTime EndDate);
    }
}
