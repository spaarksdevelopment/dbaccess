﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DBAccessControlSystemInterface.VisitorControl
{
    class LobbyWorks : IVisitorControl, IDisposable
    {
        protected string instanceName;
        protected SqlConnection conn;


        public LobbyWorks(string InstanceName)
        {
            instanceName = InstanceName;

            switch (instanceName)
            {
                case "test":
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONN"].ConnectionString);
                    break;
            }

            conn.Open();
        }

        #region IDisposable Members

        public void Dispose()
        {
            conn.Close();
        }

        #endregion

        #region IVisitorControl Members

        public string InstanceName
        {
            get
            {
                return instanceName;
            }            
        }

        public bool RegisterVisitor(string VisitorName, string VisitorCompany, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
