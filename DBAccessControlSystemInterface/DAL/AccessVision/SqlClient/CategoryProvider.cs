using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DBAccessControlSystemInterface
{
    public abstract class CategoryProvider : DataAccess
    {
        //private static CategoryProvider _instance = null;
        
        //public static CategoryProvider Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //            _instance = (CategoryProvider)Activator.CreateInstance(Type.GetType(Globals.Settings.Category.ProviderType));
                    
        //        return _instance;
        //    }
        //}
    
        public CategoryProvider()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["CONN"].ConnectionString;
            //this.EnableCaching = Globals.Settings.Category.EnableCaching;
            //this.CacheDuration = Globals.Settings.Category.CacheDuration;
        }

        #region methods for i_category
        // Methods that work with i_category
        public abstract List<CategoryInstance> GetNewCategories(DateTime addedSince, SqlConnection cn);
        public abstract List<CategoryInstance> Geti_category(SqlConnection cn);
        public abstract int Geti_categoryCount();
        public abstract CategoryInstance Geti_categoryByID(int id);
        public abstract bool Inserti_category(CategoryInstance i_category);
        public abstract bool Updatei_category(CategoryInstance i_category);
        public abstract bool Deletei_category(int id);

        protected virtual CategoryInstance Geti_categoryFromReader(IDataReader reader)
        {
            CategoryInstance c = new CategoryInstance();

            c.CategoryID = DataAccess.CastTo<string>(reader["id"].ToString());
            c.CategoryName = DataAccess.CastTo<string>(reader["description"]);
            c.Type = CategoryType.Business;

            return c;// new CategoryInstance(DataAccess.CastTo<int>(reader["id"]), DataAccess.CastTo<string>(reader["description"]), DataAccess.CastTo<DateTime?>(reader["last_updated_from_av"]));
        }

        protected virtual List<CategoryInstance> Geti_categoryCollectionFromReader(IDataReader reader)
        {
            List<CategoryInstance> i_category = new List<CategoryInstance>();
        
            while (reader.Read())
            {
                i_category.Add(Geti_categoryFromReader(reader));
            }

            return i_category;
        }
        #endregion
    }
}