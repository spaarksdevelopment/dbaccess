using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DBAccessControlSystemInterface
{
    public abstract class BadgeProvider : DataAccess
    {
        //private static BadgeProvider _instance = null;
        
        //public static BadgeProvider Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //            _instance = (BadgeProvider)Activator.CreateInstance(Type.GetType(Globals.Settings.Badge.ProviderType));
                    
        //        return _instance;
        //    }
        //}
    
        public BadgeProvider()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["CONN"].ConnectionString;
        }

        #region methods for i_badge
        // Methods that work with i_badge
        public abstract List<BadgeInstance> GetAllBadges(bool includeDisabled, SqlConnection cn);
        public abstract List<BadgeInstance> Geti_badge(string personId, bool includeDisabled, SqlConnection cn);
        public abstract int Geti_badgeCount();
        public abstract BadgeInstance Geti_badgeByID(int id);
        public abstract bool Inserti_badge(int personId, string miFare, SqlConnection cn);
        public abstract bool Updatei_badge(BadgeInstance i_badge);
        public abstract bool Deletei_badge(int id);
        public abstract bool ActivateBadge(string miFare, SqlConnection cn);
        public abstract bool ExtendBadge(string miFare, DateTime expiryDate, SqlConnection cn);

        protected virtual BadgeInstance Geti_badgeFromReader(IDataReader reader)
        {
            BadgeInstance b = new BadgeInstance();

            b.PersonID = DataAccess.CastTo<string>(reader["person_id"].ToString());
            b.MiFare = DataAccess.CastTo<string>(reader["bid"].ToString());
            b.ExpiryDate = CastTo<DateTime?>(reader["badge_expiry_date"]);

            return b;

            //return new BadgeInstance(
            //    DataAccess.CastTo<int>(reader["id"]), 
            //    DataAccess.CastTo<string>(reader["bid"]), 
            //    DataAccess.CastTo<DateTime?>(reader["badge_expiry_date"]), 
            //    DataAccess.CastTo<DateTime>(reader["last_updated_from_av"]), 
            //    DataAccess.CastTo<int>(reader["status"]), 
            //    DataAccess.CastTo<bool>(reader["deleted"]), 
            //    DataAccess.CastTo<int?>(reader["person_id"]), 
            //    DataAccess.CastTo<DateTime?>(reader["modify_date"]), 
            //    DataAccess.CastTo<DateTime?>(reader["last_accessed_date"]),
            //    DataAccess.CastTo<DateTime?>(reader["issue_date"]));
        }

        protected virtual List<BadgeInstance> Geti_badgeCollectionFromReader(IDataReader reader)
        {
            List<BadgeInstance> i_badge = new List<BadgeInstance>();
        
            while (reader.Read())
            {
                i_badge.Add(Geti_badgeFromReader(reader));
            }

            return i_badge;
        }
        #endregion
    }
}