using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IBM.Data.Informix;
using System.Configuration;

namespace DBAccessControlSystemInterface.AccessControl
{
    public class SqlCategoryProvider : CategoryProvider
    {
        #region methods for i_category
        // ************************************************************
        //  Methods that deal with i_category
        // ************************************************************

        /// <summary>
        /// Returns a collection of all i_category last updated after added since
        /// </summary>
        /// <param name="addedSince"></param>
        /// <returns></returns>
        public override List<CategoryInstance> GetNewCategories(DateTime addedSince, SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("SELECT id,description,last_updated_from_av FROM i_category WHERE last_updated_from_av > '" + addedSince + "'", cn);
            return this.Geti_categoryCollectionFromReader(ExecuteReader(cmd));
        }

        /// <summary>
        /// Returns a collection of all i_category
        /// </summary>
        public override List<CategoryInstance> Geti_category(SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("SELECT id,description,last_updated_from_av FROM i_category", cn);
            return this.Geti_categoryCollectionFromReader(ExecuteReader(cmd));

        }

        /// <summary>
        /// Returns the total number of i_category
        /// </summary>
        public override int Geti_categoryCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM i_category", cn);
                cn.Open();
                return (int)this.ExecuteScalar(cmd);
            }
        }
        
        /// <summary>
        /// Returns a collection with all i_category filtered by id
        /// </summary>
        public override CategoryInstance Geti_categoryByID(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM i_category WHERE id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = this.ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    return this.Geti_categoryFromReader(reader);
                }
                else
                {
                    CategoryInstance v = new CategoryInstance();
                    return v;
                }
            }
        }

        /// <summary>
        /// Insert a i_category
        /// </summary>
        public override bool Inserti_category(CategoryInstance i_category)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO i_category (description, last_updated_from_av) VALUES (@description, @last_updated_from_av)", cn);        
                cmd.Parameters.Add("@description", SqlDbType.Char).Value = i_category.CategoryName;
                cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = (object)i_category.Type ?? DBNull.Value;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }

        /// <summary>
        /// Update a i_category
        /// </summary>
        public override bool Updatei_category(CategoryInstance i_category)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE i_category SET description = @description, last_updated_from_av = @last_updated_from_av WHERE id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = i_category.CategoryID;
                cmd.Parameters.Add("@description", SqlDbType.Char).Value = i_category.CategoryName;
                cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = (object)i_category.Type ?? DBNull.Value;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }

        /// <summary>
        /// Delete a i_category
        /// </summary>
        public override bool Deletei_category(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM i_category WHERE id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }
        #endregion
    }
}