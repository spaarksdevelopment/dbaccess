using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DBAccessControlSystemInterface
{
    public abstract class AccessProvider : DataAccess
    {
        //private static AccessProvider _instance = null;
        
        //public static AccessProvider Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //            _instance = (AccessProvider)Activator.CreateInstance(Type.GetType(Globals.Settings.Access.ProviderType));
                    
        //        return _instance;
        //    }
        //}
    
        public AccessProvider()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["CONN"].ConnectionString;
        }

        #region methods for access
        // Methods that work with access
        public abstract List<AccessInstance> Geti_person_category(SqlConnection cn);
        public abstract int Geti_person_categoryCount();
        public abstract List<AccessInstance> Geti_person_categoryByID(int id, SqlConnection cn);
        public abstract bool Inserti_person_category(string personID, string categoryId, DateTime startDate, DateTime endDate, SqlConnection cn);
        public abstract bool Updatei_person_category(AccessInstance i_person_category);
        public abstract bool Deletei_person_category(string personId, string categoryId, SqlConnection cn);

        protected virtual AccessInstance Geti_person_categoryFromReader(IDataReader reader)
        {
            AccessInstance a = new AccessInstance();

            a.CategoryID = DataAccess.CastTo<string>(reader["CategoryId"].ToString());
            a.CategoryName = DataAccess.CastTo<string>(reader["CategoryName"]);
            a.ExpiryDate = CastTo<DateTime?>(reader["ExpiryDate"]);
            a.PersonID = CastTo<string>(reader["PersonId"].ToString());
            a.Type = CategoryType.Business;

            return a;

            //return new AccessInstance(
            //    DataAccess.CastTo<int>(reader["id"]), 
            //    DataAccess.CastTo<int?>(reader["person_id"]), 
            //    DataAccess.CastTo<int?>(reader["category_id"]), 
            //    DataAccess.CastTo<int?>(reader["slot_number"]), 
            //    DataAccess.CastTo<DateTime>(reader["last_updated_from_av"]), 
            //    DataAccess.CastTo<int?>(reader["request_id"]), 
            //    DataAccess.CastTo<bool>(reader["deleted"]), 
            //    DataAccess.CastTo<DateTime?>(reader["enddate"]), 
            //    DataAccess.CastTo<int?>(reader["floorauthoriserid"]), 
            //    DataAccess.CastTo<string>(reader["floorauthoriser"]), 
            //    DataAccess.CastTo<string>(reader["floorauthoriseremail"]), 
            //    DataAccess.CastTo<string>(reader["badge_id_var"]), 
            //    DataAccess.CastTo<string>(reader["justification"]), 
            //    DataAccess.CastTo<string>(reader["justificationtext"]), 
            //    DataAccess.CastTo<int?>(reader["line_manager_1_id"]), 
            //    DataAccess.CastTo<string>(reader["line_manager_1"]), 
            //    DataAccess.CastTo<string>(reader["line_manager_1_email"]), 
            //    DataAccess.CastTo<int?>(reader["line_manager_2_id"]), 
            //    DataAccess.CastTo<string>(reader["line_manager_2"]), 
            //    DataAccess.CastTo<string>(reader["line_manager_2_email"]));
        }

        protected virtual List<AccessInstance> Geti_person_categoryCollectionFromReader(IDataReader reader)
        {
            List<AccessInstance> i_person_category = new List<AccessInstance>();
        
            while (reader.Read())
            {
                i_person_category.Add(Geti_person_categoryFromReader(reader));
            }

            return i_person_category;
        }
        #endregion
    }
}