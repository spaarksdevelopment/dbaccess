using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using IBM.Data.Informix;

namespace DBAccessControlSystemInterface.AccessControl
{
    public class SqlAccessProvider : AccessProvider
    {
        #region methods for access
        // ************************************************************
        //  Methods that deal with access
        // ************************************************************

        /// <summary>
        /// Returns a collection with all access
        /// </summary>
        public override List<AccessInstance> Geti_person_category(SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
                                " from i_person_category pc" +
                                " join i_person p on pc.person_id = p.[id]" +
                                " join i_category c on pc.category_id = c.[id]", (SqlConnection)cn);
            return this.Geti_person_categoryCollectionFromReader(ExecuteReader((DbCommand)cmd));

            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
            //                                    " from i_person_category pc" +
            //                                    " join i_person p on pc.person_id = p.[id]" +
            //                                    " join i_category c on pc.category_id = c.[id]", (SqlConnection)cn);
            //    return this.Geti_person_categoryCollectionFromReader(ExecuteReader((DbCommand)cmd));
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
            //                    " from i_person_category pc" +
            //                    " join i_person p on pc.person_id = p.[id]" +
            //                    " join i_category c on pc.category_id = c.[id]", (IfxConnection)cn);

            //    return this.Geti_person_categoryCollectionFromReader(ExecuteReader((DbCommand)cmd));
            //}
            //else
            //    return null;

        }

        /// <summary>
        /// Returns the total number of access
        /// </summary>
        public override int Geti_person_categoryCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM i_person_category", cn);
                cn.Open();
                return (int)this.ExecuteScalar(cmd);
            }
        }
        
        /// <summary>
        /// Returns a collection with all access filtered by person id
        /// </summary>
        public override List<AccessInstance> Geti_person_categoryByID(int id, SqlConnection cn)
        {

            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{

            //    SqlCommand cmd = new SqlCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
            //                    " from i_person_category pc" +
            //                    " join i_person p on pc.person_id = p.[id]" +
            //                    " join i_category c on pc.category_id = c.[id] WHERE person_id = @id", (SqlConnection)cn);
            //    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            //    return this.Geti_person_categoryCollectionFromReader(ExecuteReader((DbCommand)cmd));
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
            //                    " from i_person_category pc" +
            //                    " join i_person p on pc.person_id = p.[id]" +
            //                    " join i_category c on pc.category_id = c.[id] WHERE person_id = @id", (IfxConnection)cn);
            //    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            //    return this.Geti_person_categoryCollectionFromReader(ExecuteReader((DbCommand)cmd));
            //}
            //else
            //    return null;

            SqlCommand cmd = new SqlCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
                            " from i_person_category pc" +
                            " join i_person p on pc.person_id = p.[id]" +
                            " join i_category c on pc.category_id = c.[id] WHERE person_id = @id", cn);

            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            return this.Geti_person_categoryCollectionFromReader(ExecuteReader(cmd));

        }

        /// <summary>
        /// Insert a access
        /// </summary>
        public override bool Inserti_person_category(string personID, string categoryId, DateTime startDate, DateTime endDate, SqlConnection cn)
        {
            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{

            //    SqlCommand cmd = new SqlCommand("INSERT INTO i_person_category (person_id, category_id,last_updated_from_av,enddate ) " +
            //         " VALUES ((SELECT [id] FROM i_person WHERE employee = '" + personID + "'), @category_id, @last_updated_from_av, @enddate);", (SqlConnection)cn);
            //    cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = (object)categoryId ?? DBNull.Value;
            //    cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = startDate;
            //    cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = (object)endDate ?? DBNull.Value;

            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("INSERT INTO i_person_category (person_id, category_id,last_updated_from_av,enddate ) " +
            //         " VALUES ((SELECT [id] FROM i_person WHERE employee = '" + personID + "'), @category_id, @last_updated_from_av, @enddate);", (IfxConnection)cn);
            //    cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = (object)categoryId ?? DBNull.Value;
            //    cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = startDate;
            //    cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = (object)endDate ?? DBNull.Value;

            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else
            //    return false;


            SqlCommand cmd = new SqlCommand("INSERT INTO i_person_category (person_id, category_id,last_updated_from_av,enddate ) " +
                 " VALUES ((SELECT [id] FROM i_person WHERE employee = '" + personID + "'), @category_id, @last_updated_from_av, @enddate);", cn);

            cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = (object)categoryId ?? DBNull.Value;
            cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = startDate;
            cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = (object)endDate ?? DBNull.Value;

            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }

        /// <summary>
        /// Update a access
        /// </summary>
        public override bool Updatei_person_category(AccessInstance i_person_category)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE i_person_category SET person_id = @person_id, category_id = @category_id, slot_number = @slot_number, last_updated_from_av = @last_updated_from_av, request_id = @request_id, deleted = @deleted, enddate = @enddate, floorauthoriserid = @floorauthoriserid, floorauthoriser = @floorauthoriser, floorauthoriseremail = @floorauthoriseremail, badge_id_var = @badge_id_var, justification = @justification, justificationtext = @justificationtext, line_manager_1_id = @line_manager_1_id, line_manager_1 = @line_manager_1, line_manager_1_email = @line_manager_1_email, line_manager_2_id = @line_manager_2_id, line_manager_2 = @line_manager_2, line_manager_2_email = @line_manager_2_email WHERE id = @id", cn);
                //cmd.Parameters.Add("@id", SqlDbType.Int).Value = access.id;
                //cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = (object)access.person_id ?? DBNull.Value;
                //cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = (object)access.category_id ?? DBNull.Value;
                //cmd.Parameters.Add("@slot_number", SqlDbType.Int).Value = (object)access.slot_number ?? DBNull.Value;
                //cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = access.last_updated_from_av;
                //cmd.Parameters.Add("@request_id", SqlDbType.Int).Value = (object)access.request_id ?? DBNull.Value;
                //cmd.Parameters.Add("@deleted", SqlDbType.Bit).Value = access.deleted;
                //cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = (object)access.enddate ?? DBNull.Value;
                //cmd.Parameters.Add("@floorauthoriserid", SqlDbType.Int).Value = (object)access.floorauthoriserid ?? DBNull.Value;
                //cmd.Parameters.Add("@floorauthoriser", SqlDbType.VarChar).Value = (object)access.floorauthoriser ?? DBNull.Value;
                //cmd.Parameters.Add("@floorauthoriseremail", SqlDbType.VarChar).Value = (object)access.floorauthoriseremail ?? DBNull.Value;
                //cmd.Parameters.Add("@badge_id_var", SqlDbType.Char).Value = (object)access.badge_id_var ?? DBNull.Value;
                //cmd.Parameters.Add("@justification", SqlDbType.VarChar).Value = (object)access.justification ?? DBNull.Value;
                //cmd.Parameters.Add("@justificationtext", SqlDbType.NVarChar).Value = (object)access.justificationtext ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_1_id", SqlDbType.Int).Value = (object)access.line_manager_1_id ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_1", SqlDbType.VarChar).Value = (object)access.line_manager_1 ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_1_email", SqlDbType.VarChar).Value = (object)access.line_manager_1_email ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_2_id", SqlDbType.Int).Value = (object)access.line_manager_2_id ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_2", SqlDbType.VarChar).Value = (object)access.line_manager_2 ?? DBNull.Value;
                //cmd.Parameters.Add("@line_manager_2_email", SqlDbType.VarChar).Value = (object)access.line_manager_2_email ?? DBNull.Value;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }

        /// <summary>
        /// Delete a access
        /// </summary>
        public override bool Deletei_person_category(string personId, string categoryId, SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM i_person_category WHERE person_id = @personID and category_id = @categoryId", cn);
            cmd.Parameters.Add("@personID", SqlDbType.Int).Value = personId;
            cmd.Parameters.Add("@categoryId", SqlDbType.Int).Value = categoryId;

            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }
        #endregion
    }
}