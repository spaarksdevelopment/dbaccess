using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IBM.Data.Informix;

namespace DBAccessControlSystemInterface.AccessControl
{
    public class SqlBadgeProvider : BadgeProvider
    {
        #region methods for i_badge
        // ************************************************************
        //  Methods that deal with i_badge
        // ************************************************************

        public override bool ExtendBadge(string miFare, DateTime expiryDate, SqlConnection cn)
        {
            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("UPDATE i_badge SET badge_expiry_date = '" + expiryDate + "' WHERE bid='" + miFare + "'", (SqlConnection)cn);
            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("UPDATE i_badge SET badge_expiry_date = '" + expiryDate + "' WHERE bid='" + miFare + "'", (IfxConnection)cn);
            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else
            //    return false;


            SqlCommand cmd = new SqlCommand("UPDATE i_badge SET badge_expiry_date = '" + expiryDate + "' WHERE bid='" + miFare + "'", cn);


            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;

        }

        /// <summary>
        /// Custom method.
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="miFare"></param>
        /// <returns></returns>
        public override bool ActivateBadge(string miFare, SqlConnection cn)
        {
            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("UPDATE i_badge SET status = 1 WHERE bid='" + miFare + "'", (SqlConnection)cn);
            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("UPDATE i_badge SET status = 1 WHERE bid='" + miFare + "'", (IfxConnection)cn);
            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else
            //    return false;

           // using (SqlConnection cn = new SqlConnection(this.ConnectionString))
           // {
            SqlCommand cmd = new SqlCommand("UPDATE i_badge SET status = 1 WHERE bid='" + miFare + "'", cn);

            //cn.Open();
            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
           // }
        }

        /// <summary>
        /// custom method
        /// </summary>
        /// <returns></returns>
        public override List<BadgeInstance> GetAllBadges(bool includeDisabled, SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("select * from i_badge", (SqlConnection)cn);

            return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));

            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("select * from i_badge", (SqlConnection)cn);

            //    return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("select * from i_badge", (IfxConnection)cn);

            //    return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
            //}
            //else
            //    return null;

                //SqlCommand cmd = new SqlCommand("select * from i_badge", cn);

                //return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));

        }

        /// <summary>
        /// Returns a collection of badges by person id
        /// </summary>
        public override List<BadgeInstance> Geti_badge(string personId, bool includeDisabled, SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", cn);

            return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));

            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", (SqlConnection)cn);

            //    return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", (IfxConnection)cn);

            //    return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
            //}
            //else
            //    return null;
        }

        /// <summary>
        /// Returns the total number of i_badge
        /// </summary>
        public override int Geti_badgeCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM i_badge", cn);
                cn.Open();
                return (int)this.ExecuteScalar(cmd);
            }
        }
        
        /// <summary>
        /// Returns a collection with all i_badge filtered by id
        /// </summary>
        public override BadgeInstance Geti_badgeByID(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM i_badge WHERE id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = this.ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    return this.Geti_badgeFromReader(reader);
                }
                else
                {
                    BadgeInstance v = new BadgeInstance();
                    return v;
                }
            }
        }

        /// <summary>
        /// Insert a i_badge
        /// </summary>
        public override bool Inserti_badge(int personId, string miFare, SqlConnection cn)
        {
            //Type type = cn.GetType();

            //if (type == typeof(SqlConnection))
            //{
            //    SqlCommand cmd = new SqlCommand("INSERT INTO i_badge (bid, person_id, status) VALUES (@bid, @person_id, 1)", (SqlConnection)cn);
            //    cmd.Parameters.Add("@bid", SqlDbType.Char).Value = miFare;
            //    cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = (object)personId ?? DBNull.Value;
            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else if (type == typeof(IfxConnection))
            //{
            //    IfxCommand cmd = new IfxCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", (IfxConnection)cn);
            //    cmd.Parameters.Add("@bid", SqlDbType.Char).Value = miFare;
            //    cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = (object)personId ?? DBNull.Value;

            //    int ret = this.ExecuteNonQuery(cmd);
            //    return ret == 1;
            //}
            //else
            //    return false;

            // using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            // {
            //TODO: ac: need to get proper values for the last parm here, status.
            SqlCommand cmd = new SqlCommand("INSERT INTO i_badge (bid, person_id, status) VALUES (@bid, @person_id, 1)", cn);
            cmd.Parameters.Add("@bid", SqlDbType.Char).Value = miFare;
            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = (object)personId ?? DBNull.Value;

            //   cn.Open();
            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
            //}
        }

        /// <summary>
        /// Update a i_badge
        /// </summary>
        public override bool Updatei_badge(BadgeInstance i_badge)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE i_badge SET bid = @bid, badge_expiry_date = @badge_expiry_date, last_updated_from_av = @last_updated_from_av, status = @status, deleted = @deleted, person_id = @person_id, modify_date = @modify_date, last_accessed_date = @last_accessed_date, issue_date = @issue_date WHERE id = @id", cn);
                //cmd.Parameters.Add("@id", SqlDbType.Int).Value = i_badge.id;
                //cmd.Parameters.Add("@bid", SqlDbType.Char).Value = i_badge.bid;
                //cmd.Parameters.Add("@badge_expiry_date", SqlDbType.DateTime).Value = (object)i_badge.badge_expiry_date ?? DBNull.Value;
                //cmd.Parameters.Add("@last_updated_from_av", SqlDbType.DateTime).Value = i_badge.last_updated_from_av;
                //cmd.Parameters.Add("@status", SqlDbType.Int).Value = i_badge.status;
                //cmd.Parameters.Add("@deleted", SqlDbType.Bit).Value = i_badge.deleted;
                //cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = (object)i_badge.person_id ?? DBNull.Value;
                //cmd.Parameters.Add("@modify_date", SqlDbType.DateTime).Value = (object)i_badge.modify_date ?? DBNull.Value;
                //cmd.Parameters.Add("@last_accessed_date", SqlDbType.DateTime).Value = (object)i_badge.last_accessed_date ?? DBNull.Value;
                //cmd.Parameters.Add("@issue_date", SqlDbType.DateTime).Value = (object)i_badge.issue_date ?? DBNull.Value;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }

        /// <summary>
        /// Delete a i_badge
        /// </summary>
        public override bool Deletei_badge(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM i_badge WHERE id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }
        #endregion
    }
}