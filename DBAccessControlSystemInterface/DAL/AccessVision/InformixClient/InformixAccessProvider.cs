﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    class InformixAccessProvider : AccessProvider
    {
        public override List<AccessInstance> Geti_person_category(IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
                            " from i_person_category pc" +
                            " join i_person p on pc.person_id = p.[id]" +
                            " join i_category c on pc.category_id = c.[id]", (IfxConnection)cn);

            return this.Geti_person_categoryCollectionFromReader(ExecuteReader(cmd));
        }

        public override List<AccessInstance> Geti_person_categoryByID(int id, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("select top 100 employee as [PersoniD],enddate AS [ExpiryDate],category_id as [CategoryId],[description] as [CategoryName]" +
                            " from i_person_category pc" +
                            " join i_person p on pc.person_id = p.[id]" +
                            " join i_category c on pc.category_id = c.[id] WHERE person_id = @id", (IfxConnection)cn);
            cmd.Parameters.Add("@id", IfxType.Int8).Value = id;

            return this.Geti_person_categoryCollectionFromReader(ExecuteReader(cmd));
        }

        public override bool Inserti_person_category(string personID, string categoryId, DateTime startDate, DateTime endDate, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("INSERT INTO i_person_category (person_id, category_id,last_updated_from_av,enddate ) " +
                     " VALUES ((SELECT [id] FROM i_person WHERE employee = '" + personID + "'), @category_id, @last_updated_from_av, @enddate);", (IfxConnection)cn);
            cmd.Parameters.Add("@category_id", IfxType.Int8).Value = (object)categoryId ?? DBNull.Value;
            cmd.Parameters.Add("@last_updated_from_av", IfxType.DateTime).Value = startDate;
            cmd.Parameters.Add("@enddate", IfxType.DateTime).Value = (object)endDate ?? DBNull.Value;

            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }

        public override bool Deletei_person_category(string personId, string categoryId, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("DELETE FROM i_person_category WHERE person_id = @personID and category_id = @categoryId", cn);
            cmd.Parameters.Add("@personID", IfxType.Int8).Value = personId;
            cmd.Parameters.Add("@categoryId", IfxType.Int8).Value = categoryId;

            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }
    }
}
