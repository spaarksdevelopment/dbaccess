﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;
using System.Data;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    public abstract class BadgeProvider : DataAccess
    {
        public abstract List<BadgeInstance> GetAllBadges(bool includeDisabled, IfxConnection cn);
        public abstract List<BadgeInstance> Geti_badge(string personId, bool includeDisabled, IfxConnection cn);
        //public abstract int Geti_badgeCount();
        //public abstract BadgeInstance Geti_badgeByID(int id);
        public abstract bool Inserti_badge(int personId, string miFare, IfxConnection cn);
        public abstract bool Updatei_badge(BadgeInstance i_badge);
        public abstract bool Deletei_badge(int id);
        public abstract bool ActivateBadge(string miFare, IfxConnection cn);
        public abstract bool ExtendBadge(string miFare, DateTime expiryDate, IfxConnection cn);

        protected virtual BadgeInstance Geti_badgeFromReader(IDataReader reader)
        {
            BadgeInstance b = new BadgeInstance();

            b.PersonID = DataAccess.CastTo<string>(reader["person_id"].ToString());
            b.MiFare = DataAccess.CastTo<string>(reader["bid"].ToString());
            b.ExpiryDate = CastTo<DateTime?>(reader["badge_expiry_date"]);

            return b;
        }

        protected virtual List<BadgeInstance> Geti_badgeCollectionFromReader(IDataReader reader)
        {
            List<BadgeInstance> i_badge = new List<BadgeInstance>();

            while (reader.Read())
            {
                i_badge.Add(Geti_badgeFromReader(reader));
            }

            return i_badge;
        }
    }
}
