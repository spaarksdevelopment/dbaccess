﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    class InformixBadgeProvider : BadgeProvider
    {
        public override bool ExtendBadge(string miFare, DateTime expiryDate, IfxConnection cn)
        {

            IfxCommand cmd = new IfxCommand("UPDATE i_badge SET badge_expiry_date = '" + expiryDate + "' WHERE bid='" + miFare + "'", cn);
            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }

        public override bool ActivateBadge(string miFare, IfxConnection cn)
        {

            IfxCommand cmd = new IfxCommand("UPDATE i_badge SET status = 1 WHERE bid='" + miFare + "'", cn);
            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }

        public override List<BadgeInstance> GetAllBadges(bool includeDisabled, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("select * from i_badge", (IfxConnection)cn);
            return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
        }

        public override List<BadgeInstance> Geti_badge(string personId, bool includeDisabled, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", cn);

            return this.Geti_badgeCollectionFromReader(ExecuteReader(cmd));
        }

        public override bool Inserti_badge(int personId, string miFare, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("select * from i_badge where person_id in (select id from i_person where employee = '" + personId + "')", (IfxConnection)cn);
            cmd.Parameters.Add("@bid", IfxType.Char).Value = miFare;
            cmd.Parameters.Add("@person_id", IfxType.Int8).Value = (object)personId ?? DBNull.Value;

            int ret = this.ExecuteNonQuery(cmd);
            return ret == 1;
        }

        public override bool Deletei_badge(int id)
        {
            using (IfxConnection cn = new IfxConnection(this.ConnectionString))
            {
                IfxCommand cmd = new IfxCommand("DELETE FROM i_badge WHERE id = @id", cn);
                cmd.Parameters.Add("@id", IfxType.Int8).Value = id;
                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }

        public override bool Updatei_badge(BadgeInstance i_badge)
        {
            using (IfxConnection cn = new IfxConnection(this.ConnectionString))
            {
                IfxCommand cmd = new IfxCommand("UPDATE i_badge SET bid = @bid, badge_expiry_date = @badge_expiry_date, last_updated_from_av = @last_updated_from_av, status = @status, deleted = @deleted, person_id = @person_id, modify_date = @modify_date, last_accessed_date = @last_accessed_date, issue_date = @issue_date WHERE id = @id", cn);

                cn.Open();
                int ret = this.ExecuteNonQuery(cmd);
                return ret == 1;
            }
        }
    }
}
