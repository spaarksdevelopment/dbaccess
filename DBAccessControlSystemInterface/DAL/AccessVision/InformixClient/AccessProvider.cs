﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;
using System.Data;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    public abstract class AccessProvider : DataAccess
    {
        public abstract List<AccessInstance> Geti_person_category(IfxConnection cn);
        //public abstract int Geti_person_categoryCount();
        public abstract List<AccessInstance> Geti_person_categoryByID(int id, IfxConnection cn);
        public abstract bool Inserti_person_category(string personID, string categoryId, DateTime startDate, DateTime endDate, IfxConnection cn);
        //public abstract bool Updatei_person_category(AccessInstance i_person_category);
        public abstract bool Deletei_person_category(string personId, string categoryId, IfxConnection cn);

        protected virtual AccessInstance Geti_person_categoryFromReader(IDataReader reader)
        {
            AccessInstance a = new AccessInstance();

            a.CategoryID = DataAccess.CastTo<string>(reader["CategoryId"].ToString());
            a.CategoryName = DataAccess.CastTo<string>(reader["CategoryName"]);
            a.ExpiryDate = CastTo<DateTime?>(reader["ExpiryDate"]);
            a.PersonID = CastTo<string>(reader["PersonId"].ToString());
            a.Type = CategoryType.Business;

            return a;
        }

        protected virtual List<AccessInstance> Geti_person_categoryCollectionFromReader(IDataReader reader)
        {
            List<AccessInstance> i_person_category = new List<AccessInstance>();

            while (reader.Read())
            {
                i_person_category.Add(Geti_person_categoryFromReader(reader));
            }

            return i_person_category;
        }
    }
}
