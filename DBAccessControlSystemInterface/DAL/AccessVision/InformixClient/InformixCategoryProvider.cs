﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    public class InformixCategoryProvider : CategoryProvider
    {
        public override List<CategoryInstance> GetNewCategories(DateTime addedSince, IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("SELECT id,description,last_updated_from_av FROM i_category WHERE last_updated_from_av > '" + addedSince + "'", cn);
            return this.Geti_categoryCollectionFromReader(ExecuteReader(cmd));
        }

        public override List<CategoryInstance> Geti_category(IfxConnection cn)
        {
            IfxCommand cmd = new IfxCommand("SELECT id,description,last_updated_from_av FROM i_category", (IfxConnection)cn);
            return this.Geti_categoryCollectionFromReader(ExecuteReader(cmd));
        }
    }
}
