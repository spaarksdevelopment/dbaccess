﻿using System;
using System.Collections.Generic;
using IBM.Data.Informix;
using System.Data;

namespace DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient
{
    public abstract class CategoryProvider : DataAccess
    {

        public abstract List<CategoryInstance> GetNewCategories(DateTime addedSince, IfxConnection cn);
        public abstract List<CategoryInstance> Geti_category(IfxConnection cn);

        protected virtual CategoryInstance Geti_categoryFromReader(IDataReader reader)
        {
            CategoryInstance c = new CategoryInstance();

            c.CategoryID = DataAccess.CastTo<string>(reader["id"].ToString());
            c.CategoryName = DataAccess.CastTo<string>(reader["description"]);
            c.Type = CategoryType.Business;

            return c;
        }

        protected virtual List<CategoryInstance> Geti_categoryCollectionFromReader(IDataReader reader)
        {
            List<CategoryInstance> i_category = new List<CategoryInstance>();

            while (reader.Read())
            {
                i_category.Add(Geti_categoryFromReader(reader));
            }

            return i_category;
        }
    }
}
