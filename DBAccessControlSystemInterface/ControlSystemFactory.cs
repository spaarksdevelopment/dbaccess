﻿
namespace DBAccessControlSystemInterface
{
    public class ControlSystemFactory
    {
        public IAccessControl CreateAccessControl(AccessControlSystemType type, ControlSystemMode mode, string instanceName)
        {
            switch (type)
            {
                case AccessControlSystemType.PicturePerfect:
                    if (mode == ControlSystemMode.Live)
                        return null;
                    else
                        return new AccessControl.AccessVisionSql(instanceName);
                case AccessControlSystemType.Honeywell:
                    if (mode == ControlSystemMode.Live)
                        return null;
                    else
                        return new AccessControl.AccessVisionSql(instanceName);
                case AccessControlSystemType.Siemens:
                    if (mode == ControlSystemMode.Live)
                        return null;
                    else
                        return new AccessControl.AccessVisionSql(instanceName);
                default:
                    return null;
            }
        }

        public IVisitorControl CreateVisitorControl(VisitorControlSystemType type, ControlSystemMode mode, string instanceName)
        {
            switch (type)
            {
                case VisitorControlSystemType.LobbyWorks:
                    return new VisitorControl.LobbyWorks(instanceName);
                default:
                    return null;
            }
        }

    }
           
}
