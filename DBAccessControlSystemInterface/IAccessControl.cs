﻿using System;
using System.Collections.Generic;

namespace DBAccessControlSystemInterface
{
    public interface IAccessControl
    {
        string InstanceName { get; }

        // Dump of categories table into tbc...done
        List<CategoryInstance> GetAllCategories();

        // Dump of categories table by date into tbc...done
        List<CategoryInstance> GetNewCategories(DateTime AddedSince);

        // Called when need to get all access for all people in intial setup...done
        List<AccessInstance> GetAllAccess();

        // Does what it says on the tin!...done
        List<AccessInstance> GetAccessByPersonId(int PersonID);

        // From ControlSystemActions table, passes in the dbpeopleid, need to writes into access, via i_person table [for accessvisionsql]...done
        bool AddAccess(string PersonID, string CategoryId, DateTime StartDate, DateTime EndDate);

        // From ControlSystemActions table, removes from  access, via i_person table [for accessvisionsql]...done
        bool RemoveAccess(string PersonID, string CategoryId);

        // Dump of i_badge table into ...tbc...done
        List<BadgeInstance> GetAllBadges(bool IncludeDisabled);

        // Dump of i_badge table into ...tbc...done
        List<BadgeInstance> GetBadgesForPerson(string PersonID, bool IncludeDisabled);

        // Populate the i_badge table...done - but needs proper value for the status parm.
        bool CreateBadge(int PersonID, string MiFare);
        
        // Update status column against bid column...done
        bool ActivateBadge(string MiFare);

        // Update expiry date against bid column...needs date formatted to us.
        bool ExtendBadge(string MiFare, DateTime ExpiryDate);
    }
}
