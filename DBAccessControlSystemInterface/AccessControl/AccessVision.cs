﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IBM.Data.Informix;
using DBAccessControlSystemInterface.DAL.AccessVision.InformixCLient;

namespace DBAccessControlSystemInterface.AccessControl
{
    class AccessVisionSql : IAccessControl, IDisposable
    {
        protected string instanceName;
        protected SqlConnection connSql;
        protected IfxConnection connInformix;

        public AccessVisionSql(string InstanceName)
        {
            instanceName = InstanceName;

            switch (instanceName)
            {
                case "sql":
                    // SQL SERVER CONNECTION.
                    connSql = new SqlConnection();
                    connSql.ConnectionString = ConfigurationManager.ConnectionStrings["CONN"].ConnectionString;
                    connSql.Open();
                    break;
                case "informix":
                    // INFORMIX CONNECTION.
                    connInformix = new IfxConnection(); 
                    connInformix.ConnectionString = ConfigurationManager.ConnectionStrings["CONN_INFORMIX"].ConnectionString;
                    connInformix.Open();
                    break;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (connSql.State == ConnectionState.Open)
                connSql.Close();

            if (connInformix.State == ConnectionState.Open)
                connInformix.Close();
        }

        #endregion

        #region IAccessControl Members

        public string InstanceName
        {
            get { throw new NotImplementedException(); }
        }

        public List<CategoryInstance> GetAllCategories()
        {
            switch (instanceName)
            {
                case "sql":
                    SqlCategoryProvider sp = new SqlCategoryProvider();
                    return sp.Geti_category(connSql);
                case "informix":
                    InformixCategoryProvider ip = new InformixCategoryProvider();
                    return ip.Geti_category(connInformix);
                default:
                    return null;
            }
        }

        public List<CategoryInstance> GetNewCategories(DateTime AddedSince)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlCategoryProvider sp = new SqlCategoryProvider();
                    return sp.GetNewCategories(AddedSince, connSql);
                case "informix":
                    InformixCategoryProvider ip = new InformixCategoryProvider();
                    return ip.GetNewCategories(AddedSince, connInformix);
                default:
                    return null;
            }
            //SqlCategoryProvider sp = new SqlCategoryProvider();
            //return sp.GetNewCategories(AddedSince, connSql);
        }

        public List<AccessInstance> GetAllAccess()
        {
            switch (instanceName)
            {
                case "sql":
                    SqlAccessProvider sp = new SqlAccessProvider();
                    return sp.Geti_person_category(connSql);
                case "informix":
                    InformixAccessProvider ip = new InformixAccessProvider();
                    return ip.Geti_person_category(connInformix);
                default:
                    return null;
            }
            //SqlAccessProvider sp = new SqlAccessProvider();
            //return sp.Geti_person_category(connSql);
        }

        public List<AccessInstance> GetAccessByPersonId(int personID)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlAccessProvider sp = new SqlAccessProvider();
                    return sp.Geti_person_categoryByID(personID, connSql);
                case "informix":
                    InformixAccessProvider ip = new InformixAccessProvider();
                    return ip.Geti_person_categoryByID(personID, connInformix);
                default:
                    return null;
            }
            //SqlAccessProvider sp = new SqlAccessProvider();
            //return sp.Geti_person_categoryByID(personID, connSql);
        }

        public bool AddAccess(string personID, string categoryId, DateTime startDate, DateTime endDate)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlAccessProvider sp = new SqlAccessProvider();
                    return sp.Inserti_person_category(personID, categoryId, startDate, endDate, connSql);
                case "informix":
                    InformixAccessProvider ip = new InformixAccessProvider();
                    return ip.Inserti_person_category(personID, categoryId, startDate, endDate, connInformix);
                default:
                    return false;
            }
            //SqlAccessProvider sp = new SqlAccessProvider();
            //return sp.Inserti_person_category(personID, categoryId, startDate, endDate, connSql);
        }

        public bool RemoveAccess(string personID, string categoryId)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlAccessProvider sp = new SqlAccessProvider();
                    return sp.Deletei_person_category(personID, categoryId, connSql);
                case "informix":
                    InformixAccessProvider ip = new InformixAccessProvider();
                    return ip.Deletei_person_category(personID, categoryId, connInformix);
                default:
                    return false;
            }
            //SqlAccessProvider sp = new SqlAccessProvider();
            //return sp.Deletei_person_category(personID, categoryId, connSql);
        }

        public List<BadgeInstance> GetAllBadges(bool IncludeDisabled)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlBadgeProvider sp = new SqlBadgeProvider();
                    return sp.GetAllBadges(IncludeDisabled, connSql);
                case "informix":
                    InformixBadgeProvider ip = new InformixBadgeProvider();
                    return ip.GetAllBadges(IncludeDisabled, connInformix);
                default:
                    return null;
            }
            //SqlBadgeProvider sp = new SqlBadgeProvider();
            //return sp.GetAllBadges(IncludeDisabled, connSql);
        }

        public List<BadgeInstance> GetBadgesForPerson(string personID, bool includeDisabled)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlBadgeProvider sp = new SqlBadgeProvider();
                    return sp.Geti_badge(personID, includeDisabled, connSql);
                case "informix":
                    InformixBadgeProvider ip = new InformixBadgeProvider();
                    return ip.Geti_badge(personID, includeDisabled, connInformix);
                default:
                    return null;
            }
            //SqlBadgeProvider sp = new SqlBadgeProvider();
            //return sp.Geti_badge(personID, includeDisabled, connSql);
        }

        public bool CreateBadge(int personId, string miFare)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlBadgeProvider sp = new SqlBadgeProvider();
                    return sp.Inserti_badge(personId, miFare, connSql);
                case "informix":
                    InformixBadgeProvider ip = new InformixBadgeProvider();
                    return ip.Inserti_badge(personId, miFare, connInformix);
                default:
                    return false;
            }
            //SqlBadgeProvider sp = new SqlBadgeProvider();
            //return sp.Inserti_badge(personId, miFare, connSql);
        }

        public bool ActivateBadge(string miFare)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlBadgeProvider sp = new SqlBadgeProvider();
                    return sp.ActivateBadge(miFare, connSql);
                case "informix":
                    InformixBadgeProvider ip = new InformixBadgeProvider();
                    return ip.ActivateBadge(miFare, connInformix);
                default:
                    return false;
            }
            //SqlBadgeProvider sp = new SqlBadgeProvider();
            //return sp.ActivateBadge(miFare, connSql);
        }

        public bool ExtendBadge(string miFare, DateTime expiryDate)
        {
            switch (instanceName)
            {
                case "sql":
                    SqlBadgeProvider sp = new SqlBadgeProvider();
                    return sp.ExtendBadge(miFare, expiryDate, connSql);
                case "informix":
                    InformixBadgeProvider ip = new InformixBadgeProvider();
                    return ip.ExtendBadge(miFare, expiryDate, connInformix);
                default:
                    return false;
            }
            //SqlBadgeProvider sp = new SqlBadgeProvider();
            //return sp.ExtendBadge(miFare, expiryDate, connSql);
        }

        #endregion
    }
}
