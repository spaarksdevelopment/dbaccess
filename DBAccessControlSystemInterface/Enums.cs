﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessControlSystemInterface
{
    public enum AccessControlSystemType
    {
        PicturePerfect, Honeywell, Siemens
    };

    public enum VisitorControlSystemType
    {
        LobbyWorks
    };

    public enum ControlSystemMode
    {
        Test, Live
    };

    public enum BadgeStatus
    {
        Active, Inactive, Disabled
    };

    public enum CategoryType
    {
        Business, Service
    }
}
