﻿using System;

namespace DBAccessControlSystemInterface
{
    public struct AccessInstance
    {
        // dbpeopleid from dbaccess
        public string PersonID { get; set; }

        public DateTime? ExpiryDate { get; set; }
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
        public CategoryType Type { get; set; }
    }

    public struct CategoryInstance
    {
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
        public CategoryType Type { get; set; }
    }

    public struct BadgeInstance
    {
        // dbpeopleid from dbaccess
        public string PersonID { get; set; }

        public string MiFare { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public BadgeStatus Status { get; set; }
    }
}
