﻿using System.Collections.Generic;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IBadgeRequestManager
    {
        List<GetBadgeJustificationReasons_Result> GetBadgeJustificationReasons(int? languageID);
        void UpdateBadgeJustification(int requestMasterID, int? justificationID);
        List<vw_BadgeRequest> GetBadgeRequests(int requestMasterID);

        bool UpdateMasterRequestDeliveryDetail(int? requestMasterID, int? deliveryOption, int? passOfficeID,
            string contactEmail, string contactTelephoneNumber);

        bool HasPendingBadgeRequest(int dbPeopleID, List<int> badgesToReplace);

        int CreateBadgeRequest(int userID, int requestPersonID, int? badgeID, int? externalSystemRequestID = null,
            int? externalSystemID = null);

        bool HasDraftBadgeRequest(int dbPeopleID);
        bool HasActiveBadgeRequest(int dbPeopleID);

        void SetCardType(int requestMasterId, bool isSmartCard, string justficationReason);
    }
}
