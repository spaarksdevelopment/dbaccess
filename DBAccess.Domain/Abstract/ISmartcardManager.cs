﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;

namespace DBAccess.Domain.Abstract
{
    public interface ISmartcardManager
    {
        HRPersonBadge GetPersonBadgeFromMifare(string mifareNumber);
        SmartcardNumber GetSmartcardNumber(string smartcardIdentifier);

        void AddSmartcardServiceAudit(string method, string smartcardIdentifier, string returnValue,
            string[] certDetails, string errorMessage);
    }
}
