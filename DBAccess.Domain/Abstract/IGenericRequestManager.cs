﻿using System;
using System.Collections.Generic;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IGenericRequestManager
    {
        bool SubmitRequestMaster(int userID, int? requestMasterID);

        void UpdateMasterRequestComment(int requestMasterID, string comment);

        bool DoesAccessRequestMasterExist(int? requestMasterID);

        List<vw_AccessRequestPerson> GetAccessRequestPersons(int requestMasterID);

        int CreateDivisionRoleUserRequest(int currentUserID, RequestType requestType, int requestPersonID, int? divisionID, int? businessAreaID = null);

        int CreateAccessRequest(int currentUserID, int requestPersonID, int accessAreaID, int? classificationID,
            DateTime? startDate, DateTime? endDate, int? externalSystemRequestID, int? externalSystemID);

        int CreateRequest(int currentUserID, RequestType requestType, int? accessAreaID, int requestPersonID,
            int? classificationID, DateTime? startDate, DateTime? endDate, int? badgeID, int? divisionID, int? passOfficeID, int? businessAreaID,
            int? externalSystemRequestID = null, int? externalSystemID = null);

        int CreateRequestMasterAndRequestPerson(int userDBPeopleID, int requestPersonDBPeopleID, ref int requestMasterID);

        int ProcessUploadedUsers(int userID, string users, int masterID, int requestType);

        void DeleteRequest(int requestID);
        void DeleteRequestPerson(int requestPersonID);
        void DeleteRequestMaster(int requestMasterID);

        List<int> GetRequestIDs(int requestMasterID);

        List<int> GetDraftAccessAreaRoleUserRequestsForCurrentUser(int corporateDivisionAccessAreaID, int currentUserID);

        List<int> GetDraftDivisionRequestsForCurrentUser(int divisionID, int currentUserID);

        int GetNumberOfRequestPersons(int requestMasterID);

        vw_AccessRequest GetViewAccessRequest(int corporateDivisionAccessAreaID, int personToDeleteDBPeopleID, RoleType roleType);
        RequestType? GetRequestTypeFromRoleType(RoleType roleType);

        void DeleteAccessAreaRoleUser(int corporateDivisionAccessAreaID, RoleType roleType, int personToDeleteDBPeopleID);
    }
}
