﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IAccessRequestManager
    {
        int? GetDivisionIDFromAccessAreaID(int accessAreaID);
        List<CorporateDivisionAccessAreaApprover> GetAccessAreaApproversWhoAreInTheOffice(int accessAreaID);
        int? GetNumberOfApprovalsRequiredForAccessArea(int accessAreaID);

        bool CheckAccessAreaExistsForRequest(int requestMasterID, int accessAreaID);

        List<vw_AccessRequestApprover> GetAccessRequestApprovers(int RequestMasterId);

        List<vw_AccessApproverDetails> GetAccessApproverDetails(int RequestPersonID);
    }
}
