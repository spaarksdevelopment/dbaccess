﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IDivisionManager
    {
        int GetNumberOfDivisionAdministratorsForDivision(int divisionID);
        void DeleteDivisionRoleUser(int divisionID, int roleID, int personID);
        List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID);
        CorporateDivision GetDivision(int divisionID);
        bool AddNewDivisionRoleUsers(int divisionID, List<DivisionRoleUser> newCorporateDivisionRoleUsers, int dbPeopleID);
        int CreateEditDivision(UBRDivision division, bool isNew, int currentUserID, int requestMasterIDDA,
            int requestMasterIDDO, bool isSubdivision, int? subdivisionParentDivisionID);

        List<vw_AccessRequest> GetViewAccessRequest(int divisionID, int personToDeleteDBpeopleID, RoleType roleType);
    }
}
