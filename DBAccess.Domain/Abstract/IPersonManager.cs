﻿using DBAccess.Model.DAL;

namespace DBAccess.Domain.Abstract
{
    public interface IPersonManager
    {
        LocationPassOffice GetUserPassOffice(int dbPeopleID);
        int GetPersonLanguageID(int? dbPeopleID, string emailAddress);
        HRPerson GetPerson(int dbPeopleID);
    }
}
