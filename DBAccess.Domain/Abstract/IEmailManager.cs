﻿using System;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IEmailManager
    {
        int InsertBatchEmailLog(string to, string cc, string subject, string body, bool isSent, Guid guid,
            int? emailTypeId);

        int InsertEmailLog(string to, string from, string subject, string body, bool isSent, Guid guid,
            int? emailTypeId);

        int InsertEmailLog(string to, string from, string cc, string subject, string body, bool isSent, Guid guid,
            int? emailTypeId);

        EmailTemplateShort GetEmailTemplate(EmailType templateType, int? languageID);
    }
}
