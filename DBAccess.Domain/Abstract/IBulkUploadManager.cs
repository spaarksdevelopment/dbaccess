﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;
using DBAccess.Model.Models;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Abstract
{
    public interface IBulkUploadManager
    {
        List<XLImportEntity> GetImportEntities();

        List<XLImportSheetColumnMapping> GetSheetColumnMappings();

        int CreateXLImportFile(string FileName, int userID);

        bool CreateStageTableRecord(string stageTable, string tableColumns, string tableRows);

        void ClearStagingTables();

        List<ImportHistory> GetBulkUploadHistory();

        List<XLImportValidateFile_Result> ValidateData(int fileID);

        bool ImportAllData(int fileID, ref string error);

        List<XLImportGetSummary_Result> GetImportSummary(int fileID);

        bool RollbackFile(int fileID, ref string errorMsg);

        List<ValidationRuleDisplayModel> GetValidationRules();

        List<ValidationErrorDisplayModel> GetValidationErrors(int fileID);

        bool IsValidFile(int fileID);
    }
}
