﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;

namespace DBAccess.Domain.Abstract
{
    public interface ILocationManager
    {
        LocationCountry GetCountry(int countryID);
    }
}
