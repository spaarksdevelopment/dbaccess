﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;

namespace DBAccess.Domain.Concrete
{
    public class LocationManager : ILocationManager
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;

        //TODO Refactor out with DI
        public LocationManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        internal LocationManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public LocationCountry GetCountry(int countryID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.LocationCountries.SingleOrDefault(a => a.CountryID == countryID);
            }
        }
    }
}
