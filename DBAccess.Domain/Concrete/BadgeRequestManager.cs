﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;
using System.Configuration;

namespace DBAccess.Domain.Concrete
{
    public class BadgeRequestManager : IBadgeRequestManager
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;
        
        public BadgeRequestManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        //TODO Refactor out with DI
        internal BadgeRequestManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public List<GetBadgeJustificationReasons_Result> GetBadgeJustificationReasons(int? languageID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.GetBadgeJustificationReasons(languageID).ToList();
            }
        }

        public void UpdateBadgeJustification(int requestMasterID, int? justificationID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                //Need badges to filter out new badge requests, which don't need justification
                List<int> requestPeople = GetRequestPersonIDsWithReplacementBadges(requestMasterID, container);

                if (requestPeople != null && requestPeople.Count > 0)
                {
                    List<AccessRequest> accessRequests = GetAccessRequestsForRequestPersonIDs(requestPeople, container);

                    foreach (AccessRequest request in accessRequests)
                        request.BadgeJustificationReasonID = justificationID;

                    container.SaveChanges();
                }
            }
        }

        public List<vw_BadgeRequest> GetBadgeRequests(int requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.vw_BadgeRequest.Where(a => a.RequestMasterID == requestMasterID).ToList();
            }
        }

        public bool UpdateMasterRequestDeliveryDetail(int? requestMasterID, int? deliveryOption, int? passOfficeID, string contactEmail, string contactTelephoneNumber)
        {
            //there can be only one (Highlander) delivery detail per request master
            //either find existing and update, or delete first and insert new
            using (var context = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));
                context.InsertAccessRequestDeliveryDetail(requestMasterID, (short?)deliveryOption, passOfficeID, contactEmail, contactTelephoneNumber, result);
                return (int)result.Value == 0;
            }
        }

        public bool HasPendingBadgeRequest(int dbPeopleID, List<int> badgesToReplace)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                if (AreThereBadgesInTheList(badgesToReplace))
                {
                    return badgesToReplace.Select(item => GetNumberOfPendingRequestsForThisBadge(dbPeopleID, container, item))
                            .Any(pendingRequestCount => pendingRequestCount > 0);
                }

                return false;
            }
        }

        public int CreateBadgeRequest(int createdByUserID, int requestPersonID, int? badgeID, int? externalSystemRequestID = null,
            int? externalSystemID = null)
        {
            using (var context = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter requestIDObjectParameter = new ObjectParameter("RequestID", typeof(int));
                context.InsertRequest(createdByUserID, (int)RequestType.NewPass, null, requestPersonID, null, null, badgeID, null, null, null, requestIDObjectParameter,
                    externalSystemRequestID, externalSystemID, null);
                return (int)requestIDObjectParameter.Value;
            }
        }

        private static bool AreThereBadgesInTheList(List<int> badgesToReplace)
        {
            return badgesToReplace != null && badgesToReplace.Count > 0;
        }

        private static int GetNumberOfPendingRequestsForThisBadge(int dbPeopleID, IDBAccessContainer container, int item)
        {
            string submittedRequestStatus = AccessRequestStatus.Submitted.ToString();

            return container.vw_AccessRequest.Count(a => a.dbPeopleID == dbPeopleID
                                                         &&
                                                         (a.RequestTypeID ==
                                                          (int)RequestType.NewPass ||
                                                          a.RequestTypeID ==
                                                          (int)RequestType.StopPass)
                                                         && a.RequestStatus == submittedRequestStatus &&
                                                         a.BadgeID == item);
        }

        public bool HasDraftBadgeRequest(int dbPeopleID)
        {
            string draftRequestStatus = AccessRequestStatus.Draft.ToString();

            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var requestCountQuery =
                    container.vw_AccessRequest.Where(
                        a =>
                            a.dbPeopleID == dbPeopleID &&
                            (a.RequestTypeID == (int)RequestType.NewPass ||
                             a.RequestTypeID == (int)RequestType.StopPass) &&
                            a.RequestStatus == draftRequestStatus);

                return requestCountQuery.Any();
            }
        }

        public bool HasActiveBadgeRequest(int dbPeopleID)
        {
            string draftRequestStatus = AccessRequestStatus.Draft.ToString();
            string submittedRequestStatus = AccessRequestStatus.Submitted.ToString();

            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var requestCountQuery =
                    container.vw_AccessRequest.Where(
                        a =>
                            a.dbPeopleID == dbPeopleID &&
                            (a.RequestTypeID == (int) RequestType.NewPass ||
                             a.RequestTypeID == (int) RequestType.StopPass) &&
                            a.RequestStatus == draftRequestStatus &&
                            a.RequestStatus == submittedRequestStatus
                    );

                return requestCountQuery.Any();
            }
        }

        public void SetCardType(int requestMasterId, bool isSmartCard, string justficationReason)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                int accessAreaId = int.Parse(ConfigurationManager.AppSettings["SmartCard_AccessAreaID"]);
                int divisionId = int.Parse(ConfigurationManager.AppSettings["SmartCard_DivisionID"]);

                var requests = container.AccessRequests.Where(r => r.AccessRequestPerson.RequestMasterID == requestMasterId).ToList();
               
                requests.ForEach(r =>
                {
                    r.AccessAreaID = accessAreaId;
                    r.DivisionID = divisionId;
                    r.IsSmartCard = isSmartCard;
                    r.SmartCardJustification = justficationReason;
                });

                container.SaveChanges();
            }
        }

        #region Helpers
        internal List<AccessRequest> GetAccessRequestsForRequestPersonIDs(List<int> requestPeople, IDBAccessContainer container)
        {
            List<AccessRequest> accessRequests = container.AccessRequests.Where(a => requestPeople.Contains(a.RequestPersonID)).ToList();
            return accessRequests;
        }

        internal List<int> GetRequestPersonIDsWithReplacementBadges(int requestMasterID, IDBAccessContainer container)
        {
            return container.vw_BadgeRequest.Where(a => a.Issue == "R" && a.RequestMasterID == requestMasterID)
                .Select(b => b.RequestPersonID)
                .ToList();
        }
        #endregion
    }
}
