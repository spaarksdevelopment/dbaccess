﻿using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Concrete
{
    public class DivisionManager : IDivisionManager 
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;

        public DivisionManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        internal DivisionManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public int GetNumberOfDivisionAdministratorsForDivision(int divisionID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.CorporateDivisionRoleUsers.Count(a => a.DivisionID == divisionID 
                                        && a.mp_SecurityGroupID == (int)RoleType.DivisionAdministrator);
            }
        }

        public void DeleteDivisionRoleUser(int divisionID, int roleID, int dbPeopleID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var roleUserToDelete = container.CorporateDivisionRoleUsers.SingleOrDefault(a => a.DivisionID == divisionID && a.mp_SecurityGroupID == roleID && a.dbPeopleID == dbPeopleID);
                if (roleUserToDelete != null)
                {
                    container.CorporateDivisionRoleUsers.Remove(roleUserToDelete);
                    container.SaveChanges();
                }
            }
        }

        public List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.GetDivisionRoleUsers(divisionID).ToList();
            }
        }

        public CorporateDivision GetDivision(int divisionID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.CorporateDivisions.SingleOrDefault(a => a.DivisionID == divisionID);
            }
        }

        public bool AddNewDivisionRoleUsers(int divisionID, List<DivisionRoleUser> newCorporateDivisionRoleUsers, int dbPeopleID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                AddNewCorporateDivisionRoleUsers(divisionID, newCorporateDivisionRoleUsers, container);
                container.SaveChanges();

                return true;
            }
        }

        public int CreateEditDivision(UBRDivision division, bool isNew, int currentUserID, int requestMasterIDDA, int requestMasterIDDO, bool isSubdivision, int? subdivisionParentDivisionID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));
                container.CreateEditDivision(Convert.ToInt32(division.Id), division.CountryID, division.Name, division.UBRCode, division.Enabled,
                    division.RecertPeriod, isNew, currentUserID, requestMasterIDDA, requestMasterIDDO, isSubdivision, subdivisionParentDivisionID, result);

                return Convert.ToInt32(result.Value);
            }
        }

        public List<vw_AccessRequest> GetViewAccessRequest(int divisionID, int personToDeleteDBpeopleID, RoleType roleType)
        {
            RequestType requestType;
            if(roleType==RoleType.DivisionAdministrator)
                requestType=RequestType.DivisionAdministratorRole;
            else if (roleType == RoleType.DivisionOwner)
                requestType = RequestType.DivisionOwnerRole;
            else return null;

            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.vw_AccessRequest.Where(
                    a =>
                        (a.DivisionID == divisionID || a.DivisionID.Equals(null)) &&
                        a.RequestTypeID == (int) requestType
                        && a.dbPeopleID == personToDeleteDBpeopleID
                        && (a.RequestStatusID==(int)AccessRequestStatus.Draft|| a.RequestStatusID==(int)AccessRequestStatus.Submitted) ).ToList();
            }
        }

        internal void AddNewCorporateDivisionRoleUsers(int divisionID, List<DivisionRoleUser> newCorporateDivisionRoleUsers, IDBAccessContainer container)
        {
            IEnumerable<int> databaseCorporateDivisionRoleUserIDs = GetDivisionRoleUsers(divisionID).Select(a => a.DBPeopleID);
            foreach (var divisionRoleUser in newCorporateDivisionRoleUsers.Where(divisionRoleUser => !databaseCorporateDivisionRoleUserIDs.Contains(divisionRoleUser.DBPeopleID)))
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));
                container.InsertDivisionRoleUser(divisionID, divisionRoleUser.DBPeopleID, divisionRoleUser.SecurityGroupID, result);
            }
        }
    }
}
