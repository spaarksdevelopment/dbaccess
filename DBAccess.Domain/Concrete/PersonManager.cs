﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;

namespace DBAccess.Domain.Concrete
{
    public class PersonManager : IPersonManager
    {
        private readonly IDBAccessContainerFactory _ContextFactory;

        //TODO Refactor out with DI
        public PersonManager()
        {
            _ContextFactory = new DBAccessContainerFactory();
        }

        internal PersonManager(IDBAccessContainerFactory contextFactory)
        {
            _ContextFactory = contextFactory;
        }

        public LocationPassOffice GetUserPassOffice(int dbPeopleID)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                return container.GetUserPassOffices(dbPeopleID).FirstOrDefault();
            }
        }

        public int GetPersonLanguageID(int? dbPeopleID, string emailAddress)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                ObjectParameter languageID = new ObjectParameter("languageID", typeof(int));

                container.GetPersonLanguage(dbPeopleID, emailAddress, languageID);

                return Convert.ToInt32(languageID.Value);
            }
        }

        public HRPerson GetPerson(int dbPeopleID)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                return container.HRPersons.SingleOrDefault(a => a.dbPeopleID == dbPeopleID);
            }
        }
    }
}
