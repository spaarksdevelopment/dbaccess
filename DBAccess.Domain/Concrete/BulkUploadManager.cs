﻿using System;
using System.Data.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;
using DBAccess.Model.Models;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Concrete
{
    public class BulkUploadManager : IBulkUploadManager
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;

        public BulkUploadManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        internal BulkUploadManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public List<XLImportEntity> GetImportEntities()
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportEntities.OrderBy(e => e.SortOrder).ToList();
            }
        }

        public List<XLImportSheetColumnMapping> GetSheetColumnMappings()
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportSheetColumnMappings.ToList();
            }
        }

        public int CreateXLImportFile(string fileName, int userID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                XLImportFile file = new XLImportFile();
                file.Name = fileName;
                file.ImportedBy = userID;
                file.ImportedDate = DateTime.Now;
                file.Processed = false;
                file.ValidationPassed = false;

                container.XLImportFiles.Add(file);

                container.SaveChanges();

                return file.ID;
            }
        }

        public bool CreateStageTableRecord(string stageTable, string tableColumns, string tableRows)
        {
            try
            {
                using (var container = _ContainerFactory.GetDBAccessContainer())
                {
                    ObjectParameter isSuccess = new ObjectParameter("Success", typeof(bool));
                    container.XLImportInsertStageTableRecords(stageTable, tableColumns, tableRows, isSuccess);

                    return (bool)isSuccess.Value;
                }
            }
            catch 
            {
                return false;
            }
        }

        public void ClearStagingTables()
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                container.XLImportClearStagingTables();
            }
        }

        public List<ImportHistory> GetBulkUploadHistory()
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportFiles.Where(f => f.Processed == true)
                                .Select(f => new ImportHistory
                                {
                                    ID = f.ID,
                                    Name = f.Name,
                                    ImportedBy = f.ImportedBy,
                                    ImportedByName = f.HRPerson.Forename + " " +  f.HRPerson.Surname,
                                    ImportedDate = f.ImportedDate
                                }).OrderByDescending(f => f.ImportedDate).ToList();
            }
        }

        public List<XLImportValidateFile_Result> ValidateData(int fileID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportValidateFile(fileID).ToList();
            }
        }

        public bool ImportAllData(int fileID, ref string error)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter errorMessage = new ObjectParameter("ErrorMessage", typeof(string));
                container.XLImportCopyAllData(fileID, errorMessage);

                error = errorMessage.Value.ToString();

                return string.IsNullOrWhiteSpace(error);
            }
        }

        public List<XLImportGetSummary_Result> GetImportSummary(int fileID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportGetSummary(fileID).ToList();
            }
        }

        public bool RollbackFile(int fileID, ref string errorMsg)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter errorMessage = new ObjectParameter("ErrorMessage", typeof(string));
                container.XLImportRollbackFile(fileID, errorMessage);

                errorMsg = errorMessage.Value.ToString();

                return string.IsNullOrWhiteSpace(errorMsg);
            }
        }

        public List<ValidationRuleDisplayModel> GetValidationRules()
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportValidationRules.Where(vr => vr.Enabled == true).OrderBy(vr => vr.XLImportEntity.SortOrder).ThenBy(vr => vr.ValidationType)
                                    .Select(vr => new ValidationRuleDisplayModel
                                    {
                                        SheetName = vr.XLImportEntity.SheetName,
                                        ValidationType = vr.ValidationType,
                                        BusinessRule = vr.BusinessRule
                                    }).ToList();
            }
        }

        public List<ValidationErrorDisplayModel> GetValidationErrors(int fileID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportValidationErrors.Where(ve => ve.FileID == fileID).OrderBy(ve => ve.XLImportEntity.SortOrder).ThenBy(ve => ve.ErrorType)
                                    .Select(ve => new ValidationErrorDisplayModel
                                    {
                                        Entity = ve.XLImportEntity.Entity,
                                        SheetName = ve.XLImportEntity.SheetName,
                                        ErrorType = ve.ErrorType,
                                        ErrorMessage = ve.ErrorMessage,
                                        RowNumbers = ve.RowNumbers
                                    }).ToList();
            }
        }

        public bool IsValidFile(int fileID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.XLImportFiles.Where(f => f.ID == fileID).FirstOrDefault().ValidationPassed.GetValueOrDefault(false);
            }
        }
    }
}
