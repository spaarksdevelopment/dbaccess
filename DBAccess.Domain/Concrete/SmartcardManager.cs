﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;

namespace DBAccess.Domain.Concrete
{
    public class SmartcardManager : ISmartcardManager
    {
        private readonly IDBAccessContainerFactory _ContextFactory;

        //TODO Refactor out with DI
        public SmartcardManager()
        {
            _ContextFactory = new DBAccessContainerFactory();
        }

        internal SmartcardManager(IDBAccessContainerFactory contextFactory)
        {
            _ContextFactory = contextFactory;
        }

        public HRPersonBadge GetPersonBadgeFromMifare(string mifareNumber)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                string paddedMifare = mifareNumber.PadLeft(12, '0');
                HRPersonBadge badge =
                    container.HRPersonBadges.SingleOrDefault(a => a.MiFareNumber == mifareNumber || a.MiFareNumber == paddedMifare);
                return badge;
            }
        }

        public SmartcardNumber GetSmartcardNumber(string smartcardIdentifier)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                return container.SmartcardNumbers.SingleOrDefault(a => a.SerialNumber == smartcardIdentifier);
            }
        }

        public void AddSmartcardServiceAudit(string method, string smartcardIdentifier, string returnValue, string[] certDetails, string errorMessage)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                SmartcardServiceAudit audit = new SmartcardServiceAudit
                {
                    DateMethodCalled = DateTime.Now,
                    MethodCalled = method,
                    SmartcardIdentifier = smartcardIdentifier,
                    ReturnedValue = returnValue,
                    CertSubjectName = certDetails[0],
                    CertSerialNumber = certDetails[1],
                    CertIssuerName = certDetails[2],
                    ErrorMessage = errorMessage
                };
                container.SmartcardServiceAudits.Add(audit);
                container.SaveChanges();
            }
        }
    }
}
