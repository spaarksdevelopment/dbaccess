﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Concrete
{
    public class GenericRequestManager : IGenericRequestManager
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;

        //TODO Refactor out with DI
        public GenericRequestManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        internal GenericRequestManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public bool SubmitRequestMaster(int userID, int? requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter successObjectParameter = new ObjectParameter("Success", typeof(bool));
                container.SubmitRequestMaster(userID, requestMasterID, successObjectParameter);
                
                return (bool)successObjectParameter.Value;
            }
        }

        public void UpdateMasterRequestComment(int requestMasterID, string comment)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                AccessRequestMaster accessRequestMaster = container.AccessRequestMasters.Find(requestMasterID);

                if (accessRequestMaster == null)
                    throw new Exception("Request Master could not be updated");

                accessRequestMaster.Comment = comment;

                container.SaveChanges();
            }
        }

        public bool DoesAccessRequestMasterExist(int? requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                if (requestMasterID == null)
                    return false;

                return container.AccessRequestMasters.Any(a => a.RequestMasterID == requestMasterID);
            }
        }

        public List<vw_AccessRequestPerson> GetAccessRequestPersons(int requestMasterID)
        {
            using (var context = _ContainerFactory.GetDBAccessContainer())
            {
                return context.vw_AccessRequestPerson.Where(a => a.RequestMasterID == requestMasterID).ToList();
            }
        }

        public int CreateRequestMasterAndRequestPerson(int userDBPeopleID, int requestPersonDBPeopleID, ref int requestMasterID)
        {
            //check if Request master was supplied, and create it if not
            if (requestMasterID == 0)
                requestMasterID = CreateRequestMaster(userDBPeopleID);

            //create the request person
            int requestPersonID = CreateRequestPerson(userDBPeopleID, requestMasterID, requestPersonDBPeopleID, false);

            return requestPersonID;
        }

        public int ProcessUploadedUsers(int userID, string users, int masterID, int requestType)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter requestMasterIDObjectParameter = new ObjectParameter("RequestMasterId", typeof(int));
                container.ProcessUploadedUsers(userID, users, masterID, requestType, requestMasterIDObjectParameter);
                return (int)requestMasterIDObjectParameter.Value;
            }
        }

        public int CreateDivisionRoleUserRequest(int currentUserID, RequestType requestType, int requestPersonID, int? divisionID, int? businessAreaID = null)
        {
            return CreateRequest(currentUserID, requestType, null, requestPersonID, null, null, null, null, divisionID, null, businessAreaID);
        }

        public int CreateAccessRequest(int currentUserID, int accessAreaID, int requestPersonID, int? classificationID, DateTime? startDate,
            DateTime? endDate, int? externalSystemRequestID, int? externalSystemID)
        {
            return CreateRequest(currentUserID, RequestType.Access, accessAreaID, requestPersonID,
                classificationID, startDate, endDate, null, null, null, null, externalSystemRequestID, externalSystemID);
        }

        public int CreateRequest(int currentUserID, RequestType requestType, int? accessAreaID, int requestPersonID, int? classificationID, DateTime? startDate, 
            DateTime? endDate, int? badgeID, int? divisionID, int? passOfficeID, int? businessAreaID, int? externalSystemRequestID = null, int? externalSystemID = null)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter requestID = new ObjectParameter("RequestID", typeof(int));
                container.InsertRequest(currentUserID, (int)requestType, accessAreaID, requestPersonID, startDate, 
                    endDate, badgeID, divisionID, passOfficeID, businessAreaID, requestID, externalSystemRequestID, externalSystemID, classificationID);

                return (int)requestID.Value;
            }
        }

        public void DeleteRequestPerson(int requestPersonID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var requestPerson = container.AccessRequestPersons.FirstOrDefault(a => a.RequestPersonID == requestPersonID);
                if (requestPerson == null)
                    return;

                container.AccessRequestPersons.Remove(requestPerson);
                container.SaveChanges();
            }
        }

        public void DeleteRequest(int requestID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var request = container.AccessRequests.FirstOrDefault(a => a.RequestID == requestID);
                if (request == null)
                    return;

                container.AccessRequests.Remove(request);
                container.SaveChanges();
            }
        }

        public List<int> GetRequestIDs(int requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                List<int> requestPersonIDs = container.AccessRequestPersons.Where(a=>a.RequestMasterID==requestMasterID).Select(b => b.RequestPersonID).ToList();
                if (!requestPersonIDs.Any())
                    return null;

                return container.AccessRequests.Where(a => requestPersonIDs.Contains(a.RequestPersonID)).Select(b=>b.RequestID).ToList();
            }
        }

        public List<int> GetDraftDivisionRequestsForCurrentUser(int divisionID, int currentUserID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                List<int> requestPersonIDs = container.AccessRequests.Where(a => a.DivisionID == divisionID && a.CreatedByID == currentUserID
                                                                            && a.RequestStatusID == (int)AccessRequestStatus.Draft).Select(b => b.RequestPersonID).ToList();
                if (!requestPersonIDs.Any())
                    return new List<int>();

                return container.AccessRequestPersons.Where(a => requestPersonIDs.Contains(a.RequestPersonID)).Select(b => b.RequestMasterID).ToList();
            }
        }

        public List<int> GetDraftAccessAreaRoleUserRequestsForCurrentUser(int corporateDivisionAccessAreaID, int currentUserID) 
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                int accessAreaID =
                    container.CorporateDivisionAccessAreas.Where(
                        a => a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID)
                        .Select(b => b.AccessAreaID)
                        .FirstOrDefault();

                if(accessAreaID<=0)
                    return new List<int>();

                List<int> requestPersonIDs = container.AccessRequests.Where(a => a.AccessAreaID == accessAreaID && a.CreatedByID == currentUserID
                                                                            && a.RequestStatusID == (int)AccessRequestStatus.Draft && 
                                                                            (a.RequestTypeID == (int)RequestType.AccessAreaApproverRole ||
                                                                            a.RequestTypeID == (int)RequestType.AccessAreaRecertifierRole)
                                                                            ).Select(b => b.RequestPersonID).ToList();
                if (!requestPersonIDs.Any())
                    return new List<int>();

                return container.AccessRequestPersons.Where(a => requestPersonIDs.Contains(a.RequestPersonID)).Select(b => b.RequestMasterID).Distinct().ToList();
            }
        }

        public void DeleteRequestMaster(int requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var requestMaster = container.AccessRequestMasters.FirstOrDefault(a => a.RequestMasterID == requestMasterID);
                if (requestMaster == null)
                    return;

                container.AccessRequestMasters.Remove(requestMaster);
                container.SaveChanges();
            }
        }

        public int GetNumberOfRequestPersons(int requestMasterID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                return container.AccessRequestPersons.Count(a => a.RequestMasterID == requestMasterID);
            }
        }

        public vw_AccessRequest GetViewAccessRequest(int corporateDivisionAccessAreaID, int personToDeleteDBPeopleID, RoleType roleType)
        {
            RequestType? requestType = GetRequestTypeFromRoleType(roleType);
            if (requestType == null)
                return null;

            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var corporateDivisionAccessArea =
                    container.CorporateDivisionAccessAreas.FirstOrDefault(
                        a => a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID);

                if (corporateDivisionAccessArea == null)
                    return null;

                int accessAreaID = corporateDivisionAccessArea.AccessAreaID;
                if (accessAreaID == 0)
                    return null;

                return container.vw_AccessRequest.FirstOrDefault(
                    a =>
                        a.AccessAreaID == accessAreaID &&
                        a.RequestTypeID == (int)requestType
                        && a.dbPeopleID == personToDeleteDBPeopleID
                        && (a.RequestStatusID == (int)AccessRequestStatus.Draft || a.RequestStatusID == (int)AccessRequestStatus.Submitted));
            }
        }

        public void DeleteAccessAreaRoleUser(int corporateDivisionAccessAreaID, RoleType roleType, int personToDeleteDBPeopleID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var accessAreaRoleToDelete =
                    container.CorporateDivisionAccessAreaApprovers.SingleOrDefault(
                        a =>
                            a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID &&
                            a.mp_SecurityGroupID == (int)roleType && a.dbPeopleID == personToDeleteDBPeopleID);


                if (accessAreaRoleToDelete == null) return;

                container.CorporateDivisionAccessAreaApprovers.Remove(accessAreaRoleToDelete);
                container.SaveChanges();
            }
        }

        public RequestType? GetRequestTypeFromRoleType(RoleType roleType)
        {
            switch (roleType)
            {
                case RoleType.AccessApprover:
                    return RequestType.AccessAreaApproverRole;
                case RoleType.AccessRecertifier:
                    return RequestType.AccessAreaRecertifierRole;
            }
            return null;
        }

        internal int CreateRequestMaster(int userID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter requestMasterID = new ObjectParameter("RequestMasterID", typeof(int));
                container.InsertRequestMaster(userID, requestMasterID);
                return (int)requestMasterID.Value;
            }
        }

        internal int CreateRequestPerson(int userID, int requestMasterID, int requestPersonDBPeopleID, Boolean bCheckBusinessArea)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                ObjectParameter requestPersonID = new ObjectParameter("requestPersonID", typeof(int));
                container.InsertRequestPerson(userID, requestMasterID, requestPersonDBPeopleID, requestPersonID, bCheckBusinessArea);
                return (int)requestPersonID.Value;
            }
        }
    }
}
