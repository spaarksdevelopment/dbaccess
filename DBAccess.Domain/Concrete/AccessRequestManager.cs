﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using DBAccess.Domain.Abstract;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Concrete
{
    public class AccessRequestManager : IAccessRequestManager
    {
        private readonly IDBAccessContainerFactory _ContainerFactory;

        //TODO Refactor out with DI
        public AccessRequestManager()
        {
            _ContainerFactory = new DBAccessContainerFactory();
        }

        internal AccessRequestManager(IDBAccessContainerFactory containerFactory)
        {
            _ContainerFactory = containerFactory;
        }

        public int? GetDivisionIDFromAccessAreaID(int accessAreaID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var corporateDivisionAccessArea =
                    container.CorporateDivisionAccessAreas.FirstOrDefault(a => a.AccessAreaID == accessAreaID);

                if (corporateDivisionAccessArea == null)
                    return null;

                return corporateDivisionAccessArea.DivisionID;
            }
        }

        public List<CorporateDivisionAccessAreaApprover> GetAccessAreaApproversWhoAreInTheOffice(int accessAreaID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var corporateDivisionAccessArea =
                    container.CorporateDivisionAccessAreas.FirstOrDefault(a => a.AccessAreaID == accessAreaID);

                if (corporateDivisionAccessArea == null)
                    return null;

                //Assume that only FTE and any unspecified EXT Class option is used for adding approvers (ie all approvers are stored with Classification of null)
                List<CorporateDivisionAccessAreaApprover> approvers = container.CorporateDivisionAccessAreaApprovers.Where
                        (a =>
                            a.CorporateDivisionAccessAreaID == corporateDivisionAccessArea.CorporateDivisionAccessAreaID &&
                            a.Classification.Equals(null)).ToList();

                return RemoveOutOfOfficePeople(approvers);
            }
        }

        public List<CorporateDivisionAccessAreaApprover> RemoveOutOfOfficePeople(List<CorporateDivisionAccessAreaApprover> approvers)
        {
            return approvers.Where(a => !IsPersonOutOfOffice(a.dbPeopleID)).ToList();
        }

        internal bool IsPersonOutOfOffice(int? dbpeopleID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                if (dbpeopleID == null)
                    return false;

                ObjectParameter resultObjectParameter = new ObjectParameter("Result", typeof(bool));
                container.IsPersonOutOfOfficeTodayWrapper(dbpeopleID, resultObjectParameter);

                return (bool)resultObjectParameter.Value;
            }
        }

        public int? GetNumberOfApprovalsRequiredForAccessArea(int accessAreaID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var corporateDivisionAccessArea =
                    container.CorporateDivisionAccessAreas.FirstOrDefault(a => a.AccessAreaID == accessAreaID);

                if (corporateDivisionAccessArea == null)
                    return null;

                return corporateDivisionAccessArea.NumberOfApprovals;
            }
        }

        public bool CheckAccessAreaExistsForRequest(int requestMasterID, int accessAreaID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var listAccessAreaRequests =
                    container.vw_AccessAreaRequest.Where(a => a.RequestMasterID == requestMasterID
                                                              && a.AccessAreaID == accessAreaID).Distinct().ToList();

                return listAccessAreaRequests.Count > 0;
            }
        }

        public List<vw_AccessRequestApprover> GetAccessRequestApprovers(int RequestMasterId)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var q = from p in container.vw_AccessRequestApprover
                        where p.RequestMasterID == RequestMasterId
                        select p;
                List<vw_AccessRequestApprover> approvers = q.ToList();
                return approvers;
            }
        }

        public List<vw_AccessApproverDetails> GetAccessApproverDetails(int RequestPersonID)
        {
            using (var container = _ContainerFactory.GetDBAccessContainer())
            {
                var q = from p in container.vw_AccessApproverDetails
                        where p.RequestPersonID == RequestPersonID
                        select p;
                List<vw_AccessApproverDetails> listrp = q.ToList();
                return listrp;
            }
        }
    }
}
