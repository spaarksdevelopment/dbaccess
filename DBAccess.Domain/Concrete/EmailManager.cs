﻿using System;
using System.Data.Objects;
using System.Linq;
using DBAccess.Domain.Abstract;
using DBAccess.Model;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.Domain.Concrete
{
    public class EmailManager : IEmailManager
    {
        private readonly IDBAccessContainerFactory _ContextFactory;

        //TODO Refactor out with DI
        public EmailManager()
        {
            _ContextFactory = new DBAccessContainerFactory();
        }

        internal EmailManager(IDBAccessContainerFactory contextFactory)
        {
            _ContextFactory = contextFactory;
        }

        public int InsertBatchEmailLog(string to, string cc, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
        {
            string from = ConfigurationParameters.GetStringParameter("SenderEmailAddress");
            return InsertEmailLog(to, from, cc, subject, body, isSent, guid, emailTypeId);
        }

        public int InsertEmailLog(string to, string from, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
        {
            return InsertEmailLog(to, from, null, subject, body, isSent, guid, emailTypeId);
        }

        public int InsertEmailLog(string to, string from, string cc, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
        {
            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                ObjectParameter resultObjectParameter = new ObjectParameter("Result", typeof(int));
                container.InsertEmailLog(to, from, cc, subject, body, isSent, guid, emailTypeId, resultObjectParameter);

                return (int)resultObjectParameter.Value;
            }
        }

        public EmailTemplateShort GetEmailTemplate(EmailType templateType, int? languageID)
        {
            EmailTemplateShort emailTemplate;

            using (var container = _ContextFactory.GetDBAccessContainer())
            {
                emailTemplate = container.GetEmailTemplateForLanguage((int)templateType, languageID).ToList().First();
            }

            return emailTemplate;
        }
    }
}
