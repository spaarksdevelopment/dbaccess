﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.BLL.Abstract
{
    public interface IDivisionService
    {
        List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID);
        bool AreThereEnoughDivisionAdministratorsToDelete(int divisionID , int roleID);
        bool DeleteDivisionRoleUser(int divisionID, int roleID , int personID);
        RoleType GetRoleFromString(string roleString);
        AddDivisionRoleUserResult CanAddDivisionRoleUser(RoleType role, int requestPersonDBPeopleID, List<DivisionRoleUser> existingDivisionRoleUsers);

        bool CreateNewRequestForDivisionRoleUser(int requestPersonDBPeopleID, RequestType requestType, ref int requestMasterID, int? divisionID, int? businessAreaID);

        RequestType GetRequestTypeFromRole(RoleType roleType);
        CorporateDivision GetDivision(int divisionID);
        bool IsSubdivision(CorporateDivision division);
        bool AddNewDivisionRoleUsers(Int32 iDivisionID, List<DivisionRoleUser> listDivisionRoleUsers, int dbPeopleID);

        int CreateEditDivision(UBRDivision division, bool isNew, int requestMasterIDDA, int requestMasterIDDO,
            bool isSubdivision, int? subdivisionParentDivisionID);

        List<DivisionRoleUser> AddNewDivisionRoleUserToList(List<DivisionRoleUser> listDivisionRoleUsers, DivisionRoleUser newPerson);

        DivisionRoleUser CreateNewDivisionRoleUser(int requestPersonDBPeopleID, int currentUserDBPeopleID, RoleType roleType);

        bool RemoveDraftAndPendingDivisionRoleRequestForDeletedPerson(int divisionID, RoleType roleType, int personToDeleteDBPeopleID);

        bool ShouldCheckNumberOfAdministrators(RoleType roleType, int divisionID);
    }
}
