﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.BLL.Abstract
{
    public interface ISmartcardService
    {
        /// <summary>
        /// Returns the dbPeopleID of the smartcard owner or an empty string if no owner
        /// </summary>
        /// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
        string GetSmartcardOwner(string smartcardIdentifier, string[] certDetails);

        //public string GetSmartcardStatus(string smartcardIdentifier, string[] certDetails)
        //{

        //}

        //public string GetKeyIdentifier(string smartcardIdentifier, string[] certDetails)
        //{

        //}
    }
}
