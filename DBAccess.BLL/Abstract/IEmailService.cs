﻿namespace DBAccess.BLL.Abstract
{
    public interface IEmailService
    {
        void SendEmailToPersonThatRequestedBadge(string userForename, string userEmail);
        void SendEmailToPeopleOnBadgeRequest(int requestMasterID);
        void SendEmailToSmartCardApprovers(int requestMasterId);
    }
}
