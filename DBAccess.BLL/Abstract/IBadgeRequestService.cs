﻿using System.Collections.Generic;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;

namespace DBAccess.BLL.Abstract
{
    public interface IBadgeRequestService
    {
        FailureReason FailureReason { get; set; }

        List<GetBadgeJustificationReasons_Result> GetBadgeJustificationReasons(int? languageID);
        bool UpdateBadgeJustification(int requestMasterID, int? justificationID);
        List<vw_BadgeRequest> GetBadgeRequests(int requestMasterId);
        bool SubmitBadgeRequest(int? requestMasterID, DeliveryOption deliveryOption, string additionalInfo,
            string comment, string userForename, string userEmail, bool isSmartCard, string smartCardJustification);

        bool CreateNewBadgeRequestForPerson(int requestPersonDBPeopleID, List<int> badgesToReplace, ref int requestMasterID,
            int? externalSystemRequestID = null, int? externalSystemID = null);

        bool HasPendingBadgeRequest(int dbPeopleID, List<int> badgesToReplace);

        bool HasDraftBadgeRequest(int dbPeopleID);
        bool HasActiveBadgeRequest(int dbPeopleID);

        int ProcessUploadedUsers(string users, int masterId, int requestType, out int numberNotProcessed);

        int GetDefaultPassOffice(List<vw_BadgeRequest> badgeRequests);
    }
}
