﻿using System;
using System.Data;
using DBAccess.Model.Enums;
using DBAccess.Model.DAL;
using DBAccess.Model.Models;
using System.Collections.Generic;

namespace DBAccess.BLL.Abstract
{
    public interface IBulkUploadService
    {
        List<ImportHistory> GetBulkUploadHistory();
                
        bool StageData(DataSet excelData, string filename, ref int importFileID, ref string strErrors);
                
        bool ValidateData(int fileID, ref List<XLImportValidateFile_Result> listErrors);

        bool ImportData(int fileID, ref string errorMsg);

        List<XLImportGetSummary_Result> GetImportSummary(int fileID);

        bool RollbackFile(int fileID, ref string errorMsg);

        List<ValidationRuleDisplayModel> GetValidationRules();

        List<ValidationErrorDisplayModel> GetValidationErrors(int fileID);
    }
}
