﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.DAL;

namespace DBAccess.BLL.Abstract
{
    public interface IPersonService
    {
        HRPerson GetPerson(int dbPeopleID);
    }
}
