﻿using System;
using DBAccess.Model.Enums;

namespace DBAccess.BLL.Abstract
{
    public interface IAccessRequestService
    {
        FailureReason AccessRequestFailureReason { get; set; }

        bool CreateAccessAreaRequest(int accessAreaId, int requestMasterId,
            DateTime? startDate, DateTime? endDate, int? externalSystemRequestId = null,
            int? externalSystemId = null);

        bool CreateNewRequestForPerson(int dbPeopleID, ref int requestMasterID);

        int ProcessUploadedUsers(string users, int masterId, int requestType);

        bool CheckAccessAreaExistsForRequest(int requestMasterId, int accessAreaId);
    }
}
