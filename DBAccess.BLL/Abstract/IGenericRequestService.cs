﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.Model.Enums;

namespace DBAccess.BLL.Abstract
{
    public interface IGenericRequestService
    {
        bool DeleteRequest(int requestID);
        bool IsRequestMasterIDValid(int? requestMasterID);
        void RemoveDraftDivisionRequests(int? requestMasterIDDA, int? requestMasterIDDO);

        void RemoveDraftDivisionRequestsForCurrentUser(int? divisionID, int currentUserID);

        void RemoveDraftAccessAreaRoleUserRequestsForCurrentUser(int corporateDivisionAccessAreaID, int currentUserID);

        void DeleteAccessAreaRoleUser(int corporateDivisionAccessAreaID, RoleType roleType, int currentUserID);

        bool RemoveDraftAndPendingAccessAreaRoleRequestForDeletedPerson(int corporateDivisionAccessAreaID, RoleType roleType, int personToDeleteDBPeopleID);
    }
}
