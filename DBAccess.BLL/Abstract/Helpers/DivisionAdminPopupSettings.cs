﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.BLL.Abstract.Helpers
{
    public abstract class DivisionAdminPopupSettings
    {
        public abstract string PopupTitle { get; }
        public abstract string ButtonEditDivisionText { get; }
        public abstract string ButtonAddDivisionAdministratorText { get; }
        public abstract string ButtonAddDivisionOwnerText { get; }

        public abstract bool IsLiteralAddDivisionNameInfoVisible { get; }
        public abstract bool IsLiteralAddSubdivisionNameInfoVisible { get; }
        public abstract bool IsLiteralEditDivisionNameInfoVisible { get; }
        public abstract bool IsLiteralEditSubdivisionNameInfoVisible { get; }
        public abstract bool IsLiteralAddDivisionEnabledVisible { get; }
        public abstract bool IsLiteralSubdivisionEnabledInfoVisible { get; }
        public abstract bool IsLiteralSelectCountryInfoVisible { get; }
        public abstract bool IsLiteralSelectCountrySubdivisionInfoVisible { get; }
        public abstract bool IsLiteralSelectRecertVisible { get; }
        public abstract bool IsLiteralSelectRecertSubdivisionInfoVisible { get; }

        public abstract bool ShowDateAcceptedColumn { get; }
        public abstract bool ShowDateRecertifyColumn { get; }
        public abstract bool ShouldSetDefaultDivisionID { get; }

        public abstract bool ShouldSetDefaultDivisionName { get; }
        public abstract bool ShouldCheckForDuplicateCountry { get; }
    }
}
