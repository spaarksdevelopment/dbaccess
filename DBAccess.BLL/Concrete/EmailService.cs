﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;
using System.Linq;

namespace DBAccess.BLL.Concrete
{
    public class EmailService : IEmailService
    {
        private readonly IEmailManager _IEmailManager;
        private readonly IGenericRequestManager _IGenericRequestManager;
        private readonly IPersonManager _PersonManager;
        private readonly IAccessRequestManager _accessRequestManager;

        public EmailService()
        {
            _IEmailManager = new EmailManager();
            _IGenericRequestManager = new GenericRequestManager();
            _PersonManager = new PersonManager();
            _accessRequestManager = new AccessRequestManager();
        }

        internal EmailService(IEmailManager emailManager, IGenericRequestManager genericRequestManager, IPersonManager personManager)
        {
            _IEmailManager = emailManager;
            _IGenericRequestManager = new GenericRequestManager();
            _PersonManager = personManager;
        }

        public void SendEmailToPersonThatRequestedBadge(string userForename, string userEmail)
        {
            int languageID = _PersonManager.GetPersonLanguageID(null, userEmail); 
            EmailTemplateShort emailTemplateShort = _IEmailManager.GetEmailTemplate(EmailType.CreatePassRequest, languageID);
            Guid guid = Guid.NewGuid();

            emailTemplateShort.Body = emailTemplateShort.Body.Replace("[$$NAME$$]", userForename);

            _IEmailManager.InsertBatchEmailLog(userEmail, string.Empty, emailTemplateShort.Subject, 
                                                            emailTemplateShort.Body, false, guid, emailTemplateShort.Type);
        }

        public void SendEmailToPeopleOnBadgeRequest(int requestMasterID)
        {
            List<vw_AccessRequestPerson> emailRecipients = _IGenericRequestManager.GetAccessRequestPersons(Helper.SafeInt(requestMasterID));
            
            foreach (vw_AccessRequestPerson recipient in emailRecipients)
            {
                Guid guid = Guid.NewGuid();

                int languageID = _PersonManager.GetPersonLanguageID(recipient.dbPeopleID, recipient.EmailAddress);

                EmailTemplateShort emailTemplateShort = _IEmailManager.GetEmailTemplate(EmailType.PassToRequestee, languageID);

                if (string.IsNullOrEmpty(recipient.EmailAddress)) continue;

                emailTemplateShort.Body = emailTemplateShort.Body.Replace("[$$NAME$$]", recipient.Forename);
                _IEmailManager.InsertBatchEmailLog(recipient.EmailAddress, string.Empty, emailTemplateShort.Subject,
                                                            emailTemplateShort.Body, false, guid, emailTemplateShort.Type);
            }
        }

        public void SendEmailToSmartCardApprovers(int requestMasterId)
        {
            var approvers = _accessRequestManager.GetAccessRequestApprovers(requestMasterId);

            foreach (vw_AccessRequestApprover approver in approvers)
            {
                int languageID = _PersonManager.GetPersonLanguageID(approver.dbPeopleID, approver.EmailAddress);

                // Now email all relevant approvers (ie people who are set as approvers in task users)
                EmailTemplateShort emailTemplate = _IEmailManager.GetEmailTemplate(EmailType.SmartCardApproval, languageID);

                if (string.IsNullOrEmpty(approver.EmailAddress)) continue;

                var applicant = _accessRequestManager.GetAccessApproverDetails(Convert.ToInt32(approver.RequestPersonID))
                                                    .Where(a => a.RequestID == approver.RequestID)
                                                    .ToList();
                if (applicant.Count > 0)
                {
                    var applicantDetails = applicant[0];

                    emailTemplate.Body = emailTemplate.Body.Replace("[$$NAME$$]", approver.RequesterName);
                    emailTemplate.Body = emailTemplate.Body.Replace("[$$RequestId$$]", approver.RequestMasterID.ToString());
                    emailTemplate.Body = emailTemplate.Body.Replace("[$$ApplicantName$$]", applicantDetails.ApplicantName);
                    emailTemplate.Body = emailTemplate.Body.Replace("[$$CountryName$$]", applicantDetails.CountryName);
                    emailTemplate.Body = emailTemplate.Body.Replace("[$$UbrCode$$]", applicantDetails.UbrCode);
                    emailTemplate.Body = emailTemplate.Body.Replace("[$$UbrName$$]", applicantDetails.UbrName);

                    Guid guid = Guid.NewGuid();
                    _IEmailManager.InsertBatchEmailLog(approver.EmailAddress, string.Empty, emailTemplate.Subject,
                                                                emailTemplate.Body, false, guid, emailTemplate.Type);
                }
            }
        }
    }
}
