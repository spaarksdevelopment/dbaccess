﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;

namespace DBAccess.BLL.Concrete
{
    public class GenericRequestService : IGenericRequestService
    {
        private readonly IGenericRequestManager _GenericRequestManager;

        public GenericRequestService()
        {
            _GenericRequestManager = new GenericRequestManager();
        }

        public GenericRequestService(IGenericRequestManager genericRequestManager)
        {
            _GenericRequestManager = genericRequestManager;
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }

        public bool DeleteRequest(int requestID)
        {
            try
            {
                _GenericRequestManager.DeleteRequest(requestID);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public void RemoveDraftDivisionRequests(int? requestMasterIDDA, int? requestMasterIDDO)
        {
            CancelRequest(requestMasterIDDA);
            CancelRequest(requestMasterIDDO);
        }

        public void RemoveDraftDivisionRequestsForCurrentUser(int? divisionID, int currentUserID)
        {
            if (!divisionID.HasValue)
                return;

            List<int> requestMasterIDsToDelete = _GenericRequestManager.GetDraftDivisionRequestsForCurrentUser(divisionID.Value, currentUserID);

            foreach (var requestMasterID in requestMasterIDsToDelete)
                CancelRequest(requestMasterID);
        }

        public void RemoveDraftAccessAreaRoleUserRequestsForCurrentUser(int corporateDivisionAccessAreaID, int currentUserID)
        {
            List<int> requestMasterIDsToDelete = _GenericRequestManager.GetDraftAccessAreaRoleUserRequestsForCurrentUser(corporateDivisionAccessAreaID, currentUserID);
        
            foreach (var eachRequestMasterID in requestMasterIDsToDelete)
                CancelRequest(eachRequestMasterID);
        }

        public void DeleteAccessAreaRoleUser(int corporateDivisionAccessAreaID, RoleType roleType, int personToDeleteDBPeopleID)
        {
            _GenericRequestManager.DeleteAccessAreaRoleUser(corporateDivisionAccessAreaID, roleType, personToDeleteDBPeopleID);
        }

        public bool RemoveDraftAndPendingAccessAreaRoleRequestForDeletedPerson(int corporateDivisionAccessAreaID, RoleType roleType, int personToDeleteDBPeopleID)
        {
            vw_AccessRequest viewAccessRequest = _GenericRequestManager.GetViewAccessRequest(corporateDivisionAccessAreaID, personToDeleteDBPeopleID, roleType);

            if (viewAccessRequest == null)
                return false;

            _GenericRequestManager.DeleteRequestPerson(viewAccessRequest.RequestPersonID);

            if (viewAccessRequest.RequestMasterID == null)
                return false;

            int numberOfRequests = _GenericRequestManager.GetNumberOfRequestPersons(viewAccessRequest.RequestMasterID.Value);

            if (numberOfRequests == 0)
                _GenericRequestManager.DeleteRequestMaster(viewAccessRequest.RequestMasterID.Value);

            return numberOfRequests == 0;
        }

        internal void CancelRequest(int? requestMasterID)
        {
            if (!IsRequestMasterIDValid(requestMasterID))
                return;

            _GenericRequestManager.DeleteRequestMaster(requestMasterID.Value);
        }

        public bool IsRequestMasterIDValid(int? requestMasterID)
        {
            if (!requestMasterID.HasValue)
                return false;

            return requestMasterID > 0;
        }
    }
}
