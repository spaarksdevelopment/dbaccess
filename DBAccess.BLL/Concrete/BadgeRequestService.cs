﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;
using System.Collections;

namespace DBAccess.BLL.Concrete
{
    public class BadgeRequestService : IBadgeRequestService
    {
        private readonly IBadgeRequestManager _BadgeRequestManager;
        private readonly IPersonManager _PersonManager;
        private readonly IGenericRequestManager _GenericRequestManager;
        private readonly IEmailService _EmailService;
        private readonly IAccessRequestManager _accessRequestManager;

        private readonly int _CurrentUserID;

        public FailureReason FailureReason { get; set; }

        public BadgeRequestService(int currentUserID)
        {
            _CurrentUserID = currentUserID;
            _BadgeRequestManager = new BadgeRequestManager();
            _PersonManager = new PersonManager();
            _GenericRequestManager = new GenericRequestManager();
            _EmailService = new EmailService();
            _accessRequestManager = new AccessRequestManager();
        }

        //TODO Refactor out with DI. This is required for testing
        public BadgeRequestService(int currentUserID, IBadgeRequestManager manager,
            IPersonManager personManager, IGenericRequestManager genericRequestManager, IEmailService emailService)
        {
            _CurrentUserID = currentUserID;
            _BadgeRequestManager = manager;
            _PersonManager = personManager;
            _GenericRequestManager = genericRequestManager;
            _EmailService = emailService;
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }

        public List<GetBadgeJustificationReasons_Result> GetBadgeJustificationReasons(int? languageId)
		{
			try
			{
                return _BadgeRequestManager.GetBadgeJustificationReasons(languageId);
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return null;
			}
		}

        public bool UpdateBadgeJustification(int requestMasterID, int? justificationID)
        {
            try
            {
                _BadgeRequestManager.UpdateBadgeJustification(requestMasterID, justificationID);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public List<vw_BadgeRequest> GetBadgeRequests(int requestMasterId)
        {
            try
            {
                return _BadgeRequestManager.GetBadgeRequests(requestMasterId);
            }
            catch (Exception ex)
            {
                HandleException(ex);

                return null;
            }
        }

        public bool CreateNewBadgeRequestForPerson(int requestPersonDBPeopleID, List<int> badgesToReplace, ref int requestMasterID,
            int? externalSystemRequestID = null, int? externalSystemID = null)
        {
            if (!CanBadgeRequestsBeCreated(requestPersonDBPeopleID, badgesToReplace, requestMasterID))
                return false;

            try
            {
                var requestPersonId = _GenericRequestManager.CreateRequestMasterAndRequestPerson(_CurrentUserID, requestPersonDBPeopleID, ref requestMasterID);

                CreateBadgeRequests(_CurrentUserID, badgesToReplace, requestPersonId, externalSystemRequestID, externalSystemID);

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public bool SubmitBadgeRequest(int? requestMasterID, DeliveryOption deliveryOption, string additionalInfo, 
                                                                string comment, string userForename, string userEmail, bool isSmartCard, string smartCardJustification)
        {
            bool success;
            int requestMasterIDNotNull = Helper.SafeInt(requestMasterID);

            if (!_GenericRequestManager.DoesAccessRequestMasterExist(requestMasterIDNotNull))
            {
                FailureReason = FailureReason.InvalidRequestMasterID;
                return false;
            }

            try
            {
                if (isSmartCard)
                    _BadgeRequestManager.SetCardType(requestMasterIDNotNull, true, smartCardJustification);

                success = CommitBadgeRequest(requestMasterID, deliveryOption, additionalInfo, comment, requestMasterIDNotNull);
            }
            catch (Exception ex)
            {
                success = false;
                HandleException(ex);
            }

            if (!success) return false;

            SendBadgeRequestEmails(userForename, userEmail, requestMasterIDNotNull);

            if (isSmartCard)
                SendSmartCardApproverNotificationEmails(requestMasterIDNotNull);

            return true;
        }

        public bool HasDraftBadgeRequest(int dbPeopleID)
        {
            return _BadgeRequestManager.HasDraftBadgeRequest(dbPeopleID);
        }

        public bool HasActiveBadgeRequest(int dbPeopleID)
        {
            return _BadgeRequestManager.HasActiveBadgeRequest(dbPeopleID);
        }

        public bool HasPendingBadgeRequest(int dbPeopleID, List<int> badgesToReplace)
        {
            return _BadgeRequestManager.HasPendingBadgeRequest(dbPeopleID, badgesToReplace);
        }

        public int ProcessUploadedUsers(string users, int masterID, int requestType, out int numberNotProcessed)
        {
            numberNotProcessed = GetNumberOfUsersWithPendingAccessRequests(ref users);

            if (string.IsNullOrEmpty(users))
                return 0;

            try
            {
                return _GenericRequestManager.ProcessUploadedUsers(_CurrentUserID, users, masterID, requestType); ;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return -1;
        }

        public int GetDefaultPassOffice(List<vw_BadgeRequest> badgeRequests)
        {
            int defaultPassOfficeId = 0;
            if (badgeRequests != null && badgeRequests.Count > 0)
            {
                bool firstRecord = true;

                foreach (vw_BadgeRequest badgeReq in badgeRequests)
                {
                    int passOfficeId = badgeReq.CountryDefaultPassOffice == null ? 0 : badgeReq.CountryDefaultPassOffice.Value;

                    if (firstRecord)
                    {
                        defaultPassOfficeId = passOfficeId;
                        firstRecord = false;
                    }
                    else if (defaultPassOfficeId != passOfficeId)
                    {
                        defaultPassOfficeId = 0;
                        break;
                    }
                }
            }

            return defaultPassOfficeId;
        }

        #region internal methods
        internal int GetNumberOfUsersWithPendingAccessRequests(ref string users)
        {
            int numberPeopleWithPendingRequests = 0;

            List<int> dbPeopleIDs = DelimitedStringToList(users, ',');

            int initialLength = dbPeopleIDs.Count;

            dbPeopleIDs.RemoveAll(_BadgeRequestManager.HasActiveBadgeRequest);
            numberPeopleWithPendingRequests = initialLength - dbPeopleIDs.Count;

            users = GetDelimitedStringFromList(dbPeopleIDs);

            return numberPeopleWithPendingRequests;
        }

        internal List<int> DelimitedStringToList(string serializedList, char delimiter)
        {
            serializedList = serializedList.TrimEnd(delimiter);

            string[] listItmes = serializedList.Split(delimiter);
            List<int> list = new List<int>();

            foreach (string item in listItmes)
            {
                list.Add(Convert.ToInt32(item.Replace("C", "")));
            }
            return list;
        }

        internal string GetDelimitedStringFromList(List<int> listOfInts)
        {
            if (listOfInts.Count == 0)
                return string.Empty;

            return Helper.DelimitList(listOfInts, ",");
        }

        internal virtual bool CanBadgeRequestsBeCreated(int dbPeopleID, List<int> badgesToReplace, int requestMasterID)
        {
            if (this.HasPendingBadgeRequest(dbPeopleID, badgesToReplace))
            {
                FailureReason = FailureReason.PersonAlreadyOnRequest;
                return false;
            }

            if (IsRequestMasterValid(requestMasterID))
            {
                FailureReason = FailureReason.InvalidRequestMasterID;
                return false;
            }

            return true;
        }

        internal void CreateBadgeRequests(int createdBydbPeopleID, List<int> badgesToReplace, int requestPersonId, 
            int? externalSystemRequestID = null, int? externalSystemID = null)
        {
            int requestID = 0;

            if (HasBadgesToReplace(badgesToReplace))
            {
                foreach (var badgeID in badgesToReplace)
                    requestID = _BadgeRequestManager.CreateBadgeRequest(createdBydbPeopleID, requestPersonId, badgeID, externalSystemRequestID, externalSystemID);
            }
            else
                requestID = _BadgeRequestManager.CreateBadgeRequest(createdBydbPeopleID, requestPersonId, null, externalSystemRequestID, externalSystemID);

            if (requestID == 0)
                throw new Exception("The Request could not be created");
        }

        internal bool HasBadgesToReplace(List<int> badgesToReplace)
        {
            return (badgesToReplace != null) && (badgesToReplace.Count > 0);
        }

        internal void SendBadgeRequestEmails(string userForename, string userEmail, int requestMasterIDNotNull)
        {
            _EmailService.SendEmailToPersonThatRequestedBadge(userForename, userEmail);
            _EmailService.SendEmailToPeopleOnBadgeRequest(requestMasterIDNotNull);
        }

        internal void SendSmartCardApproverNotificationEmails(int requestMasterId)
        {
            _EmailService.SendEmailToSmartCardApprovers(requestMasterId);            
        }

        internal bool CommitBadgeRequest(int? requestMasterID, DeliveryOption deliveryOption, string additionalInfo,
            string comment, int requestMasterIDNotNull)
        {
            UpdateRequestDeliveryOption(deliveryOption, additionalInfo, requestMasterIDNotNull);

            if (!string.IsNullOrWhiteSpace(comment))
                _GenericRequestManager.UpdateMasterRequestComment(requestMasterIDNotNull, comment);

            return _GenericRequestManager.SubmitRequestMaster(_CurrentUserID, requestMasterID);
        }

        internal bool IsRequestMasterValid(int requestMasterID)
        {
            return requestMasterID > 0 && !_GenericRequestManager.DoesAccessRequestMasterExist(requestMasterID);
        }

        internal bool UpdateRequestDeliveryOption(DeliveryOption deliveryOption, string additionalInfo, int? requestMasterID)
        {
            if(deliveryOption==DeliveryOption.Collect)
                return UpdateDeliveryOptionCollect(additionalInfo, requestMasterID);

            return UpdateDeliveryOptionContact(additionalInfo, requestMasterID);
        }

        internal bool UpdateDeliveryOptionCollect(string additionalInfo, int? requestMasterID)
        {
            int passOfficeId = Convert.ToInt32(additionalInfo);
            int deliveryOptionID = Convert.ToInt32(DeliveryOption.Collect);

            return _BadgeRequestManager.UpdateMasterRequestDeliveryDetail(requestMasterID, deliveryOptionID, passOfficeId, null, null);
        }

        internal bool UpdateDeliveryOptionContact(string additionalInfo, int? requestMasterID)
        {
            int deliveryOptionID = Convert.ToInt32(DeliveryOption.Contact);

            string[] contactInfo = GetContactInfo(additionalInfo);
            string contactEmail = GetContactEmail(contactInfo);
            string contactTelephoneNumber = GetContactTelephoneNumber(contactInfo);

            int? passOfficeID = GetPassOfficeID();

            return _BadgeRequestManager.UpdateMasterRequestDeliveryDetail(requestMasterID, deliveryOptionID, passOfficeID, contactEmail, contactTelephoneNumber);
        }

        internal int? GetPassOfficeID()
        {
            LocationPassOffice passOffice = _PersonManager.GetUserPassOffice(_CurrentUserID);

            int? passOfficeID = null;
            
            if (passOffice != null)
                passOfficeID = passOffice.PassOfficeID;

            return passOfficeID;
        }

        internal string GetContactTelephoneNumber(IList<string> contactInfo)
        {
            return contactInfo[0];
        }

        internal string GetContactEmail(string[] contactInfo)
        {
            return contactInfo[1];
        }

        internal string[] GetContactInfo(string additionalInfo)
        {
            string[] contactInfo = additionalInfo.Split(',');
            return contactInfo;
        }
        #endregion internal methods
    }
}
