﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using Spaarks.Common.UtilityManager;

namespace DBAccess.BLL.Concrete
{
    public class PersonService : IPersonService
    {
        private readonly IPersonManager _PersonManager;

        private readonly int _CurrentUserID;

        public PersonService(int currentUserID)
        {
            _CurrentUserID = currentUserID;
            _PersonManager = new PersonManager();
        }

        public HRPerson GetPerson(int dbPeopleID)
        {
            return _PersonManager.GetPerson(dbPeopleID);
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }
    }
}
