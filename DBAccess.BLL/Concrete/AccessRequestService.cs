﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;

namespace DBAccess.BLL.Concrete
{
    public class AccessRequestService : IAccessRequestService
    {
        private readonly IAccessRequestManager _AccessRequestManager;
        private readonly IGenericRequestManager _GenericRequestManager;
        private readonly int _CurrentUserID;

        public FailureReason AccessRequestFailureReason { get; set; }

        //TODO - Use DI
        public AccessRequestService(int currentUserID)
        {
            _CurrentUserID = currentUserID;
            _AccessRequestManager = new AccessRequestManager();
            _GenericRequestManager = new GenericRequestManager();
        }

        //Only used for testing
        internal AccessRequestService(int currentUserID, IAccessRequestManager accessRequestManager, 
                                                        IGenericRequestManager genericRequestManager)
        {
            _CurrentUserID = currentUserID;
            _AccessRequestManager = accessRequestManager;
            _GenericRequestManager = genericRequestManager;
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }

        //TODO - Needs further work to consider different timezones
        internal DateTime? CorrectDateForDaylightSavings(DateTime? theDate)
        {
            if (!theDate.HasValue) return null;

            if (theDate.Value.TimeOfDay.Hours == 23)
                return theDate.Value.AddHours(1);

            if (theDate.Value.TimeOfDay.Hours == 1)
                return theDate.Value.AddHours(-1);

            return theDate;
        }

        public bool CreateNewRequestForPerson(int dbPeopleID, ref int requestMasterID)
        {
            if (requestMasterID > 0 && !_GenericRequestManager.DoesAccessRequestMasterExist(requestMasterID))
            {
                AccessRequestFailureReason = FailureReason.InvalidRequestMasterID;
                return false;
            }

            try
            {
                var requestPersonID = _GenericRequestManager.CreateRequestMasterAndRequestPerson(_CurrentUserID, dbPeopleID, ref requestMasterID);

                if (requestPersonID == 0) throw new Exception("The Request Person could not be created");

                if (requestPersonID == -1)
                {
                    AccessRequestFailureReason = FailureReason.PersonAlreadyOnRequest;
                    return false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                AccessRequestFailureReason = FailureReason.Exception;
                return false;
            }
            return true;
        }
        
        public bool CreateAccessAreaRequest(int accessAreaId, int requestMasterID,
            DateTime? startDate, DateTime? endDate, int? externalSystemRequestId = null,
            int? externalSystemId = null)
        {
            startDate = CorrectDateForDaylightSavings(startDate);
            endDate = CorrectDateForDaylightSavings(endDate);

            if (!_GenericRequestManager.DoesAccessRequestMasterExist(requestMasterID))
            {
                AccessRequestFailureReason = FailureReason.InvalidRequestMasterID;
                return false;
            }

            startDate = SetDefaultStartDate(startDate);

            var requestPersonList = _GenericRequestManager.GetAccessRequestPersons(requestMasterID);

            try
            {
                return CreateAccessRequestForEachPerson(accessAreaId, startDate, endDate, externalSystemRequestId, externalSystemId, requestPersonList);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                AccessRequestFailureReason = FailureReason.Exception;
                return false;
            }
        }

        public int ProcessUploadedUsers(string users, int masterId, int requestType)
        {
            try
            {
                return _GenericRequestManager.ProcessUploadedUsers(_CurrentUserID, users, masterId, requestType);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return -1;
        }

        public bool CheckAccessAreaExistsForRequest(int requestMasterId, int accessAreaId)
        {
            try
            {
                return _AccessRequestManager.CheckAccessAreaExistsForRequest(requestMasterId, accessAreaId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return true;
            }
        }

        internal bool DoesAccessAreaHaveEnoughValidApprovers(int accessAreaID)
        {
            try
            {
                int? divisionID = _AccessRequestManager.GetDivisionIDFromAccessAreaID(accessAreaID);

                if (divisionID == null)
                    return false;

                var corporateDivisionAccessAreaApprovers = _AccessRequestManager.GetAccessAreaApproversWhoAreInTheOffice(accessAreaID);
                
                int? numberOfApprovalsRequired = _AccessRequestManager.GetNumberOfApprovalsRequiredForAccessArea(accessAreaID);

                if (numberOfApprovalsRequired == null)
                    return false;

                int numberOfValidApprovers = GetNumberOfValidApprovers(corporateDivisionAccessAreaApprovers);

                return numberOfValidApprovers >= numberOfApprovalsRequired;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                AccessRequestFailureReason = FailureReason.Exception;
                return false;
            }
        }

        internal int GetNumberOfValidApprovers(List<CorporateDivisionAccessAreaApprover> corporateDivisionAccessAreaApprovers)
        {
            if (corporateDivisionAccessAreaApprovers == null)
                return 0;

            return corporateDivisionAccessAreaApprovers.Count(a => a.mp_SecurityGroupID == (int)RoleType.AccessApprover && a.Enabled);
        }

        internal DateTime? SetDefaultStartDate(DateTime? startDate)
        {
            if (!startDate.HasValue) startDate = DateTime.Today;
            return startDate;
        }

        internal virtual bool CreateAccessRequestForEachPerson(int accessAreaID, DateTime? startDate,
            DateTime? endDate, int? externalSystemRequestID, int? externalSystemID, List<vw_AccessRequestPerson> requestPersonList)
        {
            bool success = false;

            if (requestPersonList == null)
                return false;

            foreach (var requestPerson in requestPersonList)
                success = CreateAccessRequestForPerson(accessAreaID, startDate, endDate, externalSystemRequestID, externalSystemID, requestPerson);
 
            return success;
        }

        internal virtual bool CreateAccessRequestForPerson(int accessAreaID, DateTime? startDate, DateTime? endDate,
            int? externalSystemRequestID, int? externalSystemID, vw_AccessRequestPerson requestPerson)
        {
            var requestID = _GenericRequestManager.CreateAccessRequest(_CurrentUserID, accessAreaID, requestPerson.RequestPersonID,
                requestPerson.HRClassificationID, startDate, endDate, externalSystemRequestID, externalSystemID);

            if (requestID == 0)
            {
                AccessRequestFailureReason = FailureReason.Exception;
                throw new Exception("The request could not be created");
            }

            if (DoesAccessAreaHaveEnoughValidApprovers(accessAreaID))
                return true;

            _GenericRequestManager.DeleteRequest(requestID);

            AccessRequestFailureReason = FailureReason.NotEnoughApproversAvailable;

            return false;
        }
    }
}
