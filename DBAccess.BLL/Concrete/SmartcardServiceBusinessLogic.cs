﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;

namespace DBAccess.BLL.Concrete
{
    public class SmartcardServiceBusinessLogic : ISmartcardService
    {
        private readonly ISmartcardManager _SmartcardManager;
        private readonly IPersonManager _PersonManager;

        private readonly int DBPeopleIDLength = 7;
        private readonly string ExternalDBPeopleIDFirstCharacter = "8";

        //TODO - Use DI
        public SmartcardServiceBusinessLogic()
        {
            _SmartcardManager = new SmartcardManager();
            _PersonManager = new PersonManager();
        }

        internal SmartcardServiceBusinessLogic(ISmartcardManager smartcardManager, IPersonManager personManager)
        {
            _SmartcardManager = smartcardManager;
            _PersonManager = personManager;
        }

        public string GetSmartcardOwner(string smartcardIdentifier, string[] certDetails)
        {
            SmartcardNumber number = _SmartcardManager.GetSmartcardNumber(smartcardIdentifier);

            string dbPeopleID = string.Empty;

            if (number != null)
            {
                HRPersonBadge badge = _SmartcardManager.GetPersonBadgeFromMifare(number.Mifare);

                if (badge != null)
                {
                    HRPerson person = _PersonManager.GetPerson(badge.dbPeopleID);

                    if (person != null)
                    {
                        dbPeopleID = badge.dbPeopleID.ToString();

                        if (!IsValidDBPeopleID(person))
                            return "Error";
                    }
                }
            }

            _SmartcardManager.AddSmartcardServiceAudit("GetSmartcardOwner", smartcardIdentifier, dbPeopleID, certDetails, null);

            return dbPeopleID;
        }

        internal bool IsValidDBPeopleID(HRPerson person)
        {
            return person.dbPeopleID.ToString().Length == DBPeopleIDLength;
        }
    }
}
