﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Models;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;

namespace DBAccess.BLL.Concrete
{
    public class BulkUploadService : IBulkUploadService
    {
        private readonly IBulkUploadManager _BulkUploadManager;
        private readonly int _CurrentUserID;

        public BulkUploadService(int currentUserID)
        {
            _CurrentUserID = currentUserID;
            _BulkUploadManager = new BulkUploadManager();
        }

        //Only used for testing
        internal BulkUploadService(int currentUserID, IBulkUploadManager bulkUploadManager)
        {
            _CurrentUserID = currentUserID;
            _BulkUploadManager = bulkUploadManager;
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }

        private bool IsValidExcelTemplate(DataSet excelData, List<XLImportEntity> listEntities, List<XLImportSheetColumnMapping> listAllColumnMappings, ref string errorMessage)
        {
            StringBuilder sbMissingSheets = new StringBuilder();
            StringBuilder sbAllMissingColumns = new StringBuilder();

            foreach (var entity in listEntities)
            {
                if (!SheetExists(excelData, entity.SheetName))
                {
                    sbMissingSheets.Append(entity.SheetName + ", ");
                }
                else
                {
                    string missingColumns = CheckForMissingColumns(entity, excelData, listAllColumnMappings);

                    if(!string.IsNullOrEmpty(missingColumns))
                        sbAllMissingColumns.AppendLine("- " + missingColumns + " column(s) are required in " + entity.SheetName + " sheet");
                }
            }

            errorMessage = ConstructValidationError(sbMissingSheets, sbAllMissingColumns);

            return string.IsNullOrWhiteSpace(errorMessage);
        }

        private bool SheetExists(DataSet execlData, string sheetName)
        {
            return execlData.Tables.Contains(sheetName);
        }

        private string CheckForMissingColumns(XLImportEntity entity, DataSet execlData, List<XLImportSheetColumnMapping> listAllColumnMappings)
        {
            StringBuilder sbMissingColumns = new StringBuilder();

            var entityColumnMapings = listAllColumnMappings.Where(c => c.EntityID == entity.ID);
            foreach (var columnMapping in entityColumnMapings)
            {
                if (!execlData.Tables[entity.SheetName].Columns.Contains(columnMapping.SheetColumn))
                    sbMissingColumns.Append(columnMapping.SheetColumn + ", ");
            }

            return sbMissingColumns.ToString().Trim(' ', ',');
        }

        private string ConstructValidationError(StringBuilder sbMissingSheets, StringBuilder sbAllMissingColumns)
        {
            if (sbMissingSheets.Length > 0 || sbAllMissingColumns.Length > 0)
            {
                string missingSheetsError = string.Empty;
                if (sbMissingSheets.Length > 0)
                    missingSheetsError = "- " + sbMissingSheets.ToString().Trim(' ', ',') + " sheet(s) are required in the Excel template";

                return string.Format("{0}\n{1}", missingSheetsError, sbAllMissingColumns.ToString()).Trim(' ', '\n');
            }

            return string.Empty;
        }            

        private bool IsEmptySheet(List<XLImportEntity> listEntities, DataSet excelData)
        {
            foreach (var entity in listEntities)
            {
                DataRow firstRow = excelData.Tables[entity.SheetName].Rows[0];
                if (!firstRow.ItemArray.All(e => e == null || e.ToString().Trim() == string.Empty))
                    return false;
            }

            return true;
        }

        private bool StageSingleSheet(int fileID, XLImportEntity entity, DataTable excelTable, List<XLImportSheetColumnMapping> entityColumnMapings, ref string errorMessage)
        {
            List<string> listDataRows = new List<string>();
            StringBuilder sbDataRow = new StringBuilder();
            int rowIndex = 2;
           
            foreach (DataRow row in excelTable.Rows)
            {
                //When an empty row found, finish iterating the table
                if (row.ItemArray.All(e => e == null || e.ToString().Trim() == string.Empty))
                    break;

                sbDataRow.Clear();

                //Iterate all the columns and construct the data row to be inserted
                foreach (var map in entityColumnMapings)
                {
                    sbDataRow.Append("'" + row[map.SheetColumn].ToString().Replace("'", "''") + "',");
                }

                sbDataRow.Append(fileID + ",");
                sbDataRow.Append(rowIndex);

                listDataRows.Add("(" + sbDataRow.ToString().TrimEnd(',') + ")");

                rowIndex++;
            }
           
            if (listDataRows.Count > 0)
            {
                string stagingTableColumns = string.Join(", ", entityColumnMapings.Select(c => c.StagingTableColumn)) + ", FileID, RowNumber"; 
                string stagingTableData = string.Join(", ", listDataRows);

                bool success = _BulkUploadManager.CreateStageTableRecord(entity.StagingTable, stagingTableColumns, stagingTableData);

                if (!success)
                {
                    errorMessage = "Error while coping " + entity.SheetName + " to staging table";
                    return false;
                }
            }
            return true;
        }

        public bool StageData(DataSet excelData, string filename, ref int importFileID, ref string errorMessage)
        {
            List<XLImportEntity> listEntities = _BulkUploadManager.GetImportEntities();
            List<XLImportSheetColumnMapping> listAllColumnMappings = _BulkUploadManager.GetSheetColumnMappings();

            //Check whether excel template is valid before moving further
            if (!IsValidExcelTemplate(excelData, listEntities, listAllColumnMappings, ref errorMessage))
                return false;

            //Clear all the staging tables before each upload
            _BulkUploadManager.ClearStagingTables();

            importFileID = _BulkUploadManager.CreateXLImportFile(filename, _CurrentUserID);

            if (importFileID <= 0)
                return false;

            if (IsEmptySheet(listEntities, excelData))
            {
                errorMessage = "File does not contain any data";
                return false;
            }

            foreach (var entity in listEntities)
            {
                var entityColumnMapings = listAllColumnMappings.Where(c => c.EntityID == entity.ID).ToList();

                if (!StageSingleSheet(importFileID, entity, excelData.Tables[entity.SheetName], entityColumnMapings, ref errorMessage))
                    return false;
            }

            return true;
        }
               
        public bool ValidateData(int fileID, ref List<XLImportValidateFile_Result> listErrors)
        {
            listErrors = _BulkUploadManager.ValidateData(fileID);
            return _BulkUploadManager.IsValidFile(fileID);
        }
               
        public bool ImportData(int fileID, ref string errorMsg)
        {
            return _BulkUploadManager.ImportAllData(fileID, ref errorMsg);
        }

        public List<ImportHistory> GetBulkUploadHistory()
        {
            return _BulkUploadManager.GetBulkUploadHistory();
        }

        public List<XLImportGetSummary_Result> GetImportSummary(int fileID)
        {
            return _BulkUploadManager.GetImportSummary(fileID);
        }

        public bool RollbackFile(int fileID, ref string errorMsg)
        {
            return _BulkUploadManager.RollbackFile(fileID, ref errorMsg);
        }

        public List<ValidationRuleDisplayModel> GetValidationRules() 
        {
            return _BulkUploadManager.GetValidationRules();
        }

        public List<ValidationErrorDisplayModel> GetValidationErrors(int fileID)
        {
            return _BulkUploadManager.GetValidationErrors(fileID);
        }
    }
}
