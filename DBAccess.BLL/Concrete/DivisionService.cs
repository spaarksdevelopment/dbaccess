﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Domain.Abstract;
using DBAccess.Model;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Spaarks.Common.UtilityManager;

namespace DBAccess.BLL.Concrete
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionManager _DivisionManager;
        private readonly IPersonManager _PersonManager;
        private readonly IGenericRequestManager _GenericRequestManager;

        private readonly int _CurrentUserID;

        public FailureReason DivisionRoleUserRequestFailureReason { get; set; }

        //TODO - Use DI
        public DivisionService(int currentUserID)
        {
            _CurrentUserID = currentUserID;
            _DivisionManager = new DivisionManager();
            _PersonManager = new PersonManager();
            _GenericRequestManager = new GenericRequestManager();
        }

        internal DivisionService(int currentUserID, IDivisionManager divisionManager, IPersonManager personManager, IGenericRequestManager genericRequestManager)
        {
            _CurrentUserID = currentUserID;
            _DivisionManager = divisionManager;
            _PersonManager = personManager;
            _GenericRequestManager = genericRequestManager;
        }

        protected virtual void HandleException(Exception ex)
        {
            LogHelper.HandleException(ex);
        }

        public List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID)
        {
            if (divisionID == 0)
                return null;

            return _DivisionManager.GetDivisionRoleUsers(divisionID);
        }
        
        public bool AreThereEnoughDivisionAdministratorsToDelete(int divisionID, int roleID)
        {
            try
            {
                int? minimumNumberOfDivisionAdministrators = GetMinimumNumberOfDivisionAdministrators();

                return minimumNumberOfDivisionAdministrators <
                       _DivisionManager.GetNumberOfDivisionAdministratorsForDivision(divisionID);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public bool DeleteDivisionRoleUser(int divisionID, int roleID, int dbPeopleID)
        {
            try
            {
                _DivisionManager.DeleteDivisionRoleUser(divisionID, roleID, dbPeopleID);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }

            return true;
        }

        public bool RemoveDraftAndPendingDivisionRoleRequestForDeletedPerson(int divisionID, RoleType roleType, int personToDeleteDBPeopleID)
        {   
            List<vw_AccessRequest> viewAccessRequests =
                _DivisionManager.GetViewAccessRequest(divisionID, personToDeleteDBPeopleID, roleType);
            
            if (viewAccessRequests==null || viewAccessRequests.Count==0)
                return false;

            int numberOfRequests = 0;

            foreach (vw_AccessRequest viewAccessRequest in viewAccessRequests)
            {
                _GenericRequestManager.DeleteRequestPerson(viewAccessRequest.RequestPersonID);

                if (viewAccessRequest.RequestMasterID == null)
                    return false;

                numberOfRequests =
                    _GenericRequestManager.GetNumberOfRequestPersons(viewAccessRequest.RequestMasterID.Value);

                if (numberOfRequests == 0)
                    _GenericRequestManager.DeleteRequestMaster(viewAccessRequest.RequestMasterID.Value);
            }
            return numberOfRequests == 0;
        }

        public RoleType GetRoleFromString(string roleString)
        {
            switch (roleString)
            {
                case "DA":
                    return RoleType.DivisionAdministrator;
                case "DO":
                    return RoleType.DivisionOwner;
                default:
                    throw new ApplicationException("Unknown role type");
            }
        }

        public AddDivisionRoleUserResult CanAddDivisionRoleUser(RoleType role, int requestPersonDBPeopleID, List<DivisionRoleUser> existingDivisionRoleUsers)
        {
            HRPerson person = _PersonManager.GetPerson(requestPersonDBPeopleID);
            if (person == null)
                return GetErrorCodeForMissingPerson(role);

            if(IsPersonDisabled(person))
                return AddDivisionRoleUserResult.PersonIsDisabled;

            if(PersonHasNoEmail(person))
                return AddDivisionRoleUserResult.PersonHasNoEmail;

            if (role == RoleType.DivisionAdministrator)
                return CanAddDivisionAdministrator(existingDivisionRoleUsers, requestPersonDBPeopleID);

            return CanAddDivisionOwner(existingDivisionRoleUsers);
        }

        public bool CreateNewRequestForDivisionRoleUser(int requestPersonDBPeopleID, RequestType requestType, ref int requestMasterID, int? divisionID, int? businessAreaID)
        {
            try
            {
                var requestPersonID = _GenericRequestManager.CreateRequestMasterAndRequestPerson(_CurrentUserID, requestPersonDBPeopleID, ref requestMasterID);

                //check if person is disabled
                if (requestPersonID == -3)
                {
                    DivisionRoleUserRequestFailureReason = FailureReason.RequestPersonInActive;
                    return false;
                }

                if (divisionID == 0)
                    divisionID = null;

                int requestID = _GenericRequestManager.CreateDivisionRoleUserRequest(_CurrentUserID, requestType, requestPersonID, divisionID, businessAreaID);

                if (requestID == 0)
                {
                    DivisionRoleUserRequestFailureReason = FailureReason.InvalidRequestID;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                DivisionRoleUserRequestFailureReason = FailureReason.Exception;
                HandleException(ex);
                return false;
            }
        }

        public RequestType GetRequestTypeFromRole(RoleType roleType)
        {
            return roleType==RoleType.DivisionAdministrator? RequestType.DivisionAdministratorRole : RequestType.DivisionOwnerRole;
        }

        public CorporateDivision GetDivision(int divisionID)
        {
            return _DivisionManager.GetDivision(divisionID);
        }

        public bool IsSubdivision(CorporateDivision division)
        {
            if (division != null && division.IsSubdivision != null)
                return (bool)division.IsSubdivision;

            return false;
        }

        public bool AddNewDivisionRoleUsers(Int32 iDivisionID, List<DivisionRoleUser> listDivisionRoleUsers, int dbPeopleID)
        {
            try
            {
                return _DivisionManager.AddNewDivisionRoleUsers(iDivisionID, listDivisionRoleUsers, dbPeopleID);
            }
            catch (Exception ex)
            {
                DivisionRoleUserRequestFailureReason = FailureReason.Exception;
                HandleException(ex);
                return false;
            }
        }

        public int CreateEditDivision(UBRDivision division, bool isNew, int requestMasterIDDA, int requestMasterIDDO, bool isSubdivision, int? subdivisionParentDivisionID)
        {
            try
            {
                return _DivisionManager.CreateEditDivision(division, isNew, _CurrentUserID, requestMasterIDDA, requestMasterIDDO, isSubdivision, subdivisionParentDivisionID);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return 0;
            }
        }

        public List<DivisionRoleUser> AddNewDivisionRoleUserToList(List<DivisionRoleUser> listDivisionRoleUsers, DivisionRoleUser newPerson)
        {
            listDivisionRoleUsers.Add(newPerson);
            return listDivisionRoleUsers.OrderBy(a => a.SecurityGroupID).ThenBy(a => a.Surname).ThenBy(a => a.Forename).ToList();
        }

        public DivisionRoleUser CreateNewDivisionRoleUser(int requestPersonDBPeopleID, int currentUserDBPeopleID, RoleType roleType)
        {
            IPersonService personService = new PersonService(currentUserDBPeopleID);
            HRPerson person = personService.GetPerson(requestPersonDBPeopleID);

            DivisionRoleUser newDivisionRoleUser = new DivisionRoleUser();
            newDivisionRoleUser.DBPeopleID = requestPersonDBPeopleID;
            newDivisionRoleUser.EmailAddress = person.EmailAddress;
            newDivisionRoleUser.Forename = person.Forename;
            newDivisionRoleUser.Surname = person.Surname;
            newDivisionRoleUser.SecurityGroupID = (int)roleType;
            newDivisionRoleUser.SecurityGroup = roleType == RoleType.DivisionAdministrator ? "Divisional Administrator" : "Divisional Owner";

            return newDivisionRoleUser;
        }

        public bool ShouldCheckNumberOfAdministrators(RoleType roleType, int divisionID)
        {
            if (divisionID == 0)
                return false;

            return roleType==RoleType.DivisionAdministrator;
        }

        internal AddDivisionRoleUserResult CanAddDivisionAdministrator(List<DivisionRoleUser> existingDivisionRoleUsers, int requestPersonDBPeopleID)
        {
            if (IsPersonAlreadyADivisionAdministrator(existingDivisionRoleUsers, requestPersonDBPeopleID))
                return AddDivisionRoleUserResult.PersonAlreadyDivisionAdministratorForThisDivision;

            if (!CanWeAddMoreDivisionAdministratorsToDivision(existingDivisionRoleUsers))
                return AddDivisionRoleUserResult.MaximumNumberOfDivisonAdministratorsReached;

            return AddDivisionRoleUserResult.Success;
        }

        internal virtual bool IsPersonAlreadyADivisionAdministrator(List<DivisionRoleUser> listDivisionRoleUsers, int requestPersonDBPeopleID)
        {
            return
                listDivisionRoleUsers.Any(
                    a =>
                        a.DBPeopleID == requestPersonDBPeopleID &&
                        a.SecurityGroupID == (int) RoleType.DivisionAdministrator);
        }

        internal virtual bool CanWeAddMoreDivisionAdministratorsToDivision(List<DivisionRoleUser> existingDivisionRoleUsers)
        {
            int numberOfDivisionAdministrators = existingDivisionRoleUsers.Count(a => a.SecurityGroupID == (int) RoleType.DivisionAdministrator);
            return numberOfDivisionAdministrators < ConfigurationParameters.GetIntParameter("MaximumDAs");
        }

        internal AddDivisionRoleUserResult CanAddDivisionOwner(List<DivisionRoleUser> existingDivisionRoleUsers)
        {
            if (DoesDivisionAlreadyHaveADivisionOwner(existingDivisionRoleUsers))
                return AddDivisionRoleUserResult.DivisionAlreadyHasADivisionOwner;

            return AddDivisionRoleUserResult.Success;
        }

        internal virtual bool DoesDivisionAlreadyHaveADivisionOwner(List<DivisionRoleUser> existingDivisionRoleUsers)
        {
            return existingDivisionRoleUsers.Any(a => a.SecurityGroupID == (int)RoleType.DivisionOwner);
        }

        internal AddDivisionRoleUserResult GetErrorCodeForMissingPerson(RoleType role)
        {
            if(role==RoleType.DivisionAdministrator)
                return AddDivisionRoleUserResult.InvalidDBPeopleIDForDivisionAdministrator;

            return AddDivisionRoleUserResult.InvalidDBPeopleIDForDivisionOwner;
        }

        internal bool PersonHasNoEmail(HRPerson person)
        {
            return string.IsNullOrWhiteSpace(person.EmailAddress);
        }

        internal bool IsPersonDisabled(HRPerson person)
        {
            if (!person.Enabled)
                return true;

            return person.Revoked.HasValue && person.Revoked.Value;
        }

        internal int GetMinimumNumberOfDivisionAdministrators()
        {
            int? minimumNumberOfDivisionAdministrators =
                ConfigurationParameters.GetIntParameter("MinimumNumberOfDivisionAdministrators");

            if (!minimumNumberOfDivisionAdministrators.HasValue)
                return 2;

            return minimumNumberOfDivisionAdministrators.Value;
        }
    }
}
