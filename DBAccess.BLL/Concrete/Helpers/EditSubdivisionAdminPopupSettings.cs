﻿using DBAccess.BLL.Abstract.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBAccess.BLL.Concrete.Helpers
{
    public class EditSubdivisionAdminPopupSettings : DivisionAdminPopupSettings
    {
        private Func<string, string, string> stringResourceProvider;

        public EditSubdivisionAdminPopupSettings(Func<string,string,string> stringResourceProviderParameter)
        {
            this.stringResourceProvider = stringResourceProviderParameter;
        }

        public override string PopupTitle
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "EditSubdivision");
            }
        }

        public override string ButtonEditDivisionText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "SaveChanges");
            }
        }

        public override string ButtonAddDivisionAdministratorText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddSubDivAdministrator");
            }
        }

        public override string ButtonAddDivisionOwnerText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddSubDivOwner");
            }
        }

        public override bool IsLiteralAddDivisionNameInfoVisible { get { return false; } }
        public override bool IsLiteralAddSubdivisionNameInfoVisible { get { return false; } }
        public override bool IsLiteralEditDivisionNameInfoVisible { get { return false; } }
        public override bool IsLiteralEditSubdivisionNameInfoVisible { get { return true; } }

        public override bool IsLiteralSubdivisionEnabledInfoVisible { get { return true; } }
        public override bool IsLiteralAddDivisionEnabledVisible { get { return false; } }
        public override bool IsLiteralSelectCountryInfoVisible { get { return false; } }
        public override bool IsLiteralSelectCountrySubdivisionInfoVisible { get { return true; } }
        public override bool IsLiteralSelectRecertVisible { get { return false; } }
        public override bool IsLiteralSelectRecertSubdivisionInfoVisible { get { return true; } }

        public override bool ShowDateAcceptedColumn { get { return true; } }
        public override bool ShowDateRecertifyColumn { get { return true; } }

        public override bool ShouldSetDefaultDivisionID { get { return false; } }
        public override bool ShouldSetDefaultDivisionName { get { return false; } }
        public override bool ShouldCheckForDuplicateCountry { get { return false; } }
    }
}