﻿using DBAccess.BLL.Abstract.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBAccess.BLL.Concrete.Helpers
{
    public static class DivisionAdminPopupSettingsFactory
    {
        public static DivisionAdminPopupSettings GetDivisionAdminPopupSettings(string pageType, bool isSubdivision, Func<string,string,string> stringResourceProvider)
        {
            if (pageType == "add" && !isSubdivision)
                return new AddDivisionAdminPopupSettings(stringResourceProvider);
            else if (pageType == "add" && isSubdivision)
                return new AddSubdivisionAdminPopupSettings(stringResourceProvider);
            else if (pageType == "edit" && !isSubdivision)
                return new EditDivisionAdminPopupSettings(stringResourceProvider);
            else
                return new EditSubdivisionAdminPopupSettings(stringResourceProvider);
        }
    }
}