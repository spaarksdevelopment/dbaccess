﻿using DBAccess.BLL.Abstract.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBAccess.BLL.Concrete.Helpers
{
    public class AddDivisionAdminPopupSettings : DivisionAdminPopupSettings
    {
        private Func<string, string, string> stringResourceProvider;

        public AddDivisionAdminPopupSettings(Func<string,string,string> stringResourceProviderParameter)
        {
            this.stringResourceProvider = stringResourceProviderParameter;
        }


        public override string PopupTitle
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddNewDivision");
            }
        }

        public override string ButtonEditDivisionText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddDivision");
            }
        }

        public override string ButtonAddDivisionAdministratorText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddDivAdministrator");
            }
        }

        public override string ButtonAddDivisionOwnerText
        {
            get
            {
                return stringResourceProvider("Resources.CommonResource", "AddDivOwner");
            }
        }

        public override bool IsLiteralAddDivisionNameInfoVisible { get { return true; } }
        public override bool IsLiteralAddSubdivisionNameInfoVisible { get { return false; } }
        public override bool IsLiteralEditDivisionNameInfoVisible { get { return false; } }
        public override bool IsLiteralEditSubdivisionNameInfoVisible { get { return false; } }

        public override bool IsLiteralSubdivisionEnabledInfoVisible { get { return false; } }
        public override bool IsLiteralAddDivisionEnabledVisible { get { return true; } }
        public override bool IsLiteralSelectCountryInfoVisible { get { return true; } }
        public override bool IsLiteralSelectCountrySubdivisionInfoVisible { get { return false; } }
        public override bool IsLiteralSelectRecertVisible { get { return true; } }
        public override bool IsLiteralSelectRecertSubdivisionInfoVisible { get { return false; } }

        public override bool ShowDateAcceptedColumn { get { return false; } }
        public override bool ShowDateRecertifyColumn { get { return true; } }

        public override bool ShouldSetDefaultDivisionID { get { return true; } }
        public override bool ShouldSetDefaultDivisionName { get { return true; } }
        public override bool ShouldCheckForDuplicateCountry { get { return true; } }
    }
}