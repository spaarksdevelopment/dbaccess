﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;

namespace DBAccess.BLL.Concrete
{
    public class LocationService : ILocationService
    {
        private readonly ILocationManager _LocationManager;

        public LocationService()
        {
            ILocationManager _LocationManager = new LocationManager();
        }

        internal LocationService(ILocationManager locationManager)
        {
            _LocationManager = locationManager;
        }

        public LocationCountry GetCountry(int countryID)
        {
            return _LocationManager.GetCountry(countryID);
        }
    }
}
