IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportFile')
BEGIN

CREATE TABLE [dbo].[XLImportFile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[ImportedDate] [datetime2](7) NOT NULL,
	[ImportedBy] [int] NOT NULL,
	[Processed] [bit] NOT NULL,
	[ValidationPassed] [bit] NULL,
	[ErrorMessage] [nvarchar](max) NULL,
 CONSTRAINT [PK_XLImportFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[XLImportFile]  WITH CHECK ADD  CONSTRAINT [FK_XLImportFile_HRPerson] FOREIGN KEY([ImportedBy])
REFERENCES [dbo].[HRPerson] ([dbPeopleID])

ALTER TABLE [dbo].[XLImportFile] CHECK CONSTRAINT [FK_XLImportFile_HRPerson]

END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportFile')
BEGIN
	
	DROP TABLE [dbo].[XLImportFile]
	
END

GO