IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageDivisionRole')
BEGIN

CREATE TABLE [dbo].[XLImportStageDivisionRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Division] [nvarchar](500) NULL,
	[DBPeopleID] [nvarchar](500) NULL,
	[SecurityGroup] [nvarchar](500) NULL,
	[DateAccepted] [nvarchar](500) NULL,
	[DateLastCertified] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStageDivisionRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[XLImportStageDivisionRole]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStageDivisionRole_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStageDivisionRole] CHECK CONSTRAINT [FK_XLImportStageDivisionRole_XLImportFile]


END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageDivisionRole')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageDivisionRole]
	
END

GO