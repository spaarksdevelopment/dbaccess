IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_ReAttemptFailedMessages]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_ReAttemptFailedMessages]
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 02/12/2015
-- Description:	ReAttempts task and task user creation for previously failed HRFeedMessages till maximum attempt threshold reaches.
-- =============================================
CREATE PROCEDURE [dbo].[HRFeed_ReAttemptFailedMessages]
AS
BEGIN
	DECLARE @HRFeedMessageTasksUsersCreationFailedID int
	DECLARE @dbPeopleID int
	DECLARE @RequestMasterID int
	DECLARE @Attempts int
	DECLARE @MaximumNumberOfAttempts int = 0

	SELECT @MaximumNumberOfAttempts = cast(Isnull(Value, 0) as int) FROM AdminValues
	WHERE [Key] = 'MaximumMessageProcessingAttempts'

	DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT HRFeedMessageTasksUsersCreationFailedID, dbPeopleID, RequestMasterID, Attempts
	FROM HRFeedMessageTasksUsersCreationFailed  
	WHERE ISNULL(Processed, 0) = 0 AND Attempts < @MaximumNumberOfAttempts

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @HRFeedMessageTasksUsersCreationFailedID, @dbPeopleID, @RequestMasterID, @Attempts

  WHILE @@FETCH_STATUS = 0
  BEGIN
	DECLARE @success bit
	EXEC up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

	IF(dbo.HaveTasksAndTasksUserBeenCreatedForAllRequests(@RequestMasterID) = 0)
	BEGIN	
		IF(@Attempts = @MaximumNumberOfAttempts - 1)
		BEGIN
			UPDATE HRFeedMessageTasksUsersCreationFailed SET Attempts = Coalesce(Attempts, 1) + 1, Processed = 1, Succeeded = 0  WHERE HRFeedMessageTasksUsersCreationFailedID = @HRFeedMessageTasksUsersCreationFailedID
		END
		ELSE
		BEGIN
			UPDATE HRFeedMessageTasksUsersCreationFailed SET Attempts = Coalesce(Attempts, 1) + 1  WHERE HRFeedMessageTasksUsersCreationFailedID = @HRFeedMessageTasksUsersCreationFailedID
		END					
	END
	ELSE
	BEGIN
		UPDATE HRFeedMessageTasksUsersCreationFailed SET Attempts = Coalesce(Attempts, 1) + 1, Processed = 1, Succeeded = 1  WHERE HRFeedMessageTasksUsersCreationFailedID = @HRFeedMessageTasksUsersCreationFailedID
	END
	
 FETCH NEXT FROM myCursor INTO @HRFeedMessageTasksUsersCreationFailedID, @dbPeopleID, @RequestMasterID, @Attempts
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
  
END



GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_ReAttemptFailedMessages]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_ReAttemptFailedMessages]
END
GO