IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO

/****** Object:  StoredProcedure [dbo].[up_CreateAccessTrigger_DEL]    Script Date: 10/16/2015 5:41:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 10/10/2015
-- Description: Process create access trigger's 'DEL' action. Set HRPerson and mp_user as reoked,
--              cancel any pending pass requests and access requests and offboard if active user
--============================================================

CREATE procedure [dbo].[up_CreateAccessTrigger_DEL]
@HRFeedMessageID int
as
begin

--Get DBPeopleID from trigger
declare @dbPeopleID int 
declare @transactionID int
select @dbPeopleID = cast(HRID as int), @transactionID = TransactionId from HRFeedMessage where HRFeedMessageId = @HRFeedMessageID

--Do nothing if HRPerson record hasn't been created
if(Not Exists(select * from HRPerson where dbPeopleID = @dbPeopleID))
	return

declare @valid bit = 1
declare @CreatedByID int = null

--Update HRPerson as revoked
update HRperson set Revoked=1 where dbPeopleID = @dbPeopleID
update mp_user set Revoked=1 where dbPeopleID = @dbPeopleID

--Check whether the person has active badges
declare @hasBadges bit = 0
select @hasBadges = count(PersonBadgeID) 
from HRPersonBadge
where dbPeopleID = @dbPeopleId and Valid = @Valid and Active = 1

--delete the person's pending requests if any
exec dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, 0

--Create revoke requests if person has approved or actioned pass/access request or a valid badge
declare @hasAccessAndPassRequestsForUser bit
select @hasAccessAndPassRequestsForUser = count(*) from AccessRequest ar
inner join AccessRequestType rt on ar.RequestTypeID = rt.RequestTypeID
inner join AccessRequestStatus rs on ar.RequestStatusID = rs.RequestStatusID
inner join AccessRequestPerson arp on ar.RequestPersonID = arp.RequestPersonID
where rt.Name in ('New Pass','Access')
	and rs.Name in ('Approved', 'Actioned')
	and arp.dbPeopleID = @dbPeopleID

if(@hasBadges = 1 or @hasAccessAndPassRequestsForUser = 1)
begin
	--Create revoke request if person has a valid badge or actioned requests
	exec dbo.HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleID, @Valid = @Valid, @CreatedByID = @CreatedByID

	--Send emails to pass officers
	exec dbo.QueueRevokeAccessEmailsForUser @dbPeopleId = @dbPeopleId

	--Insert in to CATDeleteOffboard table to off-board user after 7 days from now
	insert into CATDeleteOffboard (TransactionID, DBPeopleID, Processed, CreatedDate)
	values (@TransactionId, @dbPeopleID, 0, getdate())
end

end


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO
