IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SkippedRequestsReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_SkippedRequestsReportSearch]
END
GO

/****** Object:  StoredProcedure [dbo].[sp_SkippedRequestsReportSearch]    Script Date: 04/01/2016 16:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 04/12/2015
-- Description:	Get Skipped Requests Details
-- History:
--		 06/01/2016 Martin - Added in the date of the HRFeedMessage for consumption in the report
-- =============================================
CREATE PROCEDURE [dbo].[sp_SkippedRequestsReportSearch] 
	
	@LanguageID INT=1,
	@LogCreatedStartDate datetime2 = NULL,
	@LogCreatedEndDate datetime2 = NULL,
	@Country nvarchar(max) = '',
	@UserdbPeopleID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	IF(@LogCreatedEndDate IS NOT NULL)
	BEGIN
		SET @LogCreatedEndDate = DATEADD(second, 86399, @LogCreatedEndDate);
	END

	DECLARE @IsUserAdmin bit
	SET @IsUserAdmin=0
  
	IF EXISTS(SELECT 1 FROM dbo.mp_SecurityGroupUser WHERE SecurityGroupId=2 and dbPeopleID=@UserdbPeopleID)
	BEGIN 
		SET @IsUserAdmin=1
	END
	SELECT HRPerson.dbPeopleID AS PeopleID, ISNULL(HRPerson.Forename + ' ', '') + ISNULL(HRPerson.Surname, '') AS PersonName, HRPerson.EmailAddress, 
				 HRFeedSkippedRequestLog.CreatedDate, HRFeedSkippedRequestLog.Message, HRFeedMessage.MessageTimestamp  
	FROM HRFeedSkippedRequestLog 
	INNER JOIN HRPerson ON HRFeedSkippedRequestLog.DBPeopleID = HRPerson.dbPeopleID
	INNER JOIN LocationCountry ON HRPerson.CountryName = LocationCountry.Name
	INNER JOIN HRFeedMessage ON HRFeedSkippedRequestLog.TransactionID=HRFeedMessage.TransactionID
	WHERE
	(@LogCreatedStartDate IS NULL OR CreatedDate >= @LogCreatedStartDate)
	AND
	(@LogCreatedEndDate IS NULL OR CreatedDate <= @LogCreatedEndDate)
	AND 
	(@Country = '' OR LocationCountry.CountryID in (select Value from dbo.Split(',', @Country)))
	AND
	(@IsUserAdmin=1)

	ORDER BY HRFeedSkippedRequestLog.ID DESC

END

GO


--//@UNDO



IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SkippedRequestsReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_SkippedRequestsReportSearch]
END
GO


/****** Object:  StoredProcedure [dbo].[sp_SkippedRequestsReportSearch]    Script Date: 05/01/2016 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 04/12/2015
-- Description:	Get Skipped Requests Details
-- =============================================
CREATE PROCEDURE [dbo].[sp_SkippedRequestsReportSearch] 
	
	@LanguageID INT=1,
	@LogCreatedStartDate datetime2 = NULL,
	@LogCreatedEndDate datetime2 = NULL,
	@Country nvarchar(max) = '',
	@UserdbPeopleID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	IF(@LogCreatedEndDate IS NOT NULL)
	BEGIN
		SET @LogCreatedEndDate = DATEADD(second, 86399, @LogCreatedEndDate);
	END

	DECLARE @IsUserAdmin bit
	SET @IsUserAdmin=0
  
	IF EXISTS(SELECT 1 FROM dbo.mp_SecurityGroupUser WHERE SecurityGroupId=2 and dbPeopleID=@UserdbPeopleID)
	BEGIN 
		SET @IsUserAdmin=1
	END
	SELECT HRPerson.dbPeopleID AS PeopleID, ISNULL(HRPerson.Forename + ' ', '') + ISNULL(HRPerson.Surname, '') AS PersonName, HRPerson.EmailAddress, HRFeedSkippedRequestLog.CreatedDate, HRFeedSkippedRequestLog.Message  FROM HRFeedSkippedRequestLog 
	INNER JOIN HRPerson ON HRFeedSkippedRequestLog.DBPeopleID = HRPerson.dbPeopleID
	INNER JOIN LocationCountry ON HRPerson.CountryName = LocationCountry.Name
	WHERE
	(@LogCreatedStartDate IS NULL OR CreatedDate >= @LogCreatedStartDate)
	AND
	(@LogCreatedEndDate IS NULL OR CreatedDate <= @LogCreatedEndDate)
	AND 
	(@Country = '' OR LocationCountry.CountryID in (select Value from dbo.Split(',', @Country)))
	AND
	(@IsUserAdmin=1)

	ORDER BY HRFeedSkippedRequestLog.ID DESC

END


GO
