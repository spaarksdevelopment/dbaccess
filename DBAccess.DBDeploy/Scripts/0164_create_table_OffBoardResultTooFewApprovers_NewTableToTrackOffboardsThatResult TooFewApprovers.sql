IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'OffBoardResultTooFewApprovers'))
BEGIN


CREATE TABLE [dbo].[OffBoardResultTooFewApprovers](
	[OffBoardResultsTooFewApproversID] [int] IDENTITY(1,1) NOT NULL,
	[RequestMasterID] [int] NOT NULL,
 CONSTRAINT [PK_OffBoardResultTooFewApprovers] PRIMARY KEY CLUSTERED 
(
	[OffBoardResultsTooFewApproversID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--//@UNDO




