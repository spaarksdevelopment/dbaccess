IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessArea]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessArea]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Asanka
-- Created Date: 22/12/2015
-- Description: Copy access area from staging table to the AccessArea table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyAccessArea]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblAccessAreaIDs table (ID int)
	DECLARE @tblFloorIDs table (ID int)
	DECLARE @tblCategoryIDs table (ID int)

	--=====================================================================
	-- Fisrt, Insert (only) new floors
	--=====================================================================

	INSERT INTO LocationFloor(BuildingID, Name, Enabled) 
	OUTPUT inserted.FloorID into @tblFloorIDs
	SELECT DISTINCT b.BuildingID, 
		FloorName, 
		1
	FROM XLImportStageAccessArea saa
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		
	WHERE ISNULL(saa.ValidationFailed, 0) = 0 AND saa.FileID = @FileID 
		AND FloorName NOT IN (SELECT FloorName FROM LocationFloor WHERE Name = FloorName AND BuildingID = b.BuildingID)
	
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'LocationFloor', ID FROM @tblFloorIDs

	--=====================================================================
	-- Secondly, Insert (only) new catagories
	--=====================================================================
	
	INSERT INTO Category(id, description) 
	OUTPUT inserted.id into @tblCategoryIDs
	SELECT (SELECT COALESCE(MAX(id),0) FROM Category) + row_number() OVER (ORDER BY ID), 
		CategoryDescription
	FROM XLImportStageAccessArea 
	WHERE ISNULL(ValidationFailed, 0) = 0 AND FileID = @FileID 
		AND CategoryDescription NOT IN (SELECT DISTINCT [description] FROM Category)

	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'Category', ID FROM @tblCategoryIDs

	--=====================================================================
	-- Finally, Insert Access Areas
	--=====================================================================

	INSERT INTO AccessArea(AccessAreaTypeID, FloorID, BuildingID, Name, Enabled, ControlSystemID, RecertPeriod, AccessControlID)
	OUTPUT inserted.AccessAreaID into @tblAccessAreaIDs
	SELECT (SELECT AccessAreaTypeID FROM AccessAreaType where AccessAreaTypeName = saa.AccessAreaType),
		(SELECT FloorID FROM LocationFloor WHERE Name = FloorName AND BuildingID = b.BuildingID),
		b.BuildingID,
		saa.Name,
		1,
		(SELECT ControlSystemID FROM ControlSystem WHERE InstanceName = saa.ControlSystem),
		CAST(saa.RecertPeriod AS tinyint),
		(SELECT id FROM Category WHERE [description] = saa.CategoryDescription)
	FROM XLImportStageAccessArea saa
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sb.City and city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name and b.CityID = city.CityID
	WHERE ISNULL(saa.ValidationFailed, 0) = 0 AND saa.FileID = @FileID
	
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'AccessArea', ID FROM @tblAccessAreaIDs

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessArea]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessArea]
END
GO