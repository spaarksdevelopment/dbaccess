
if not exists(select * from sys.columns 
            where Name = N'CountryID' and Object_ID = Object_ID(N'LocationPassOffice'))
begin

	ALTER TABLE [dbo].[LocationPassOffice] ADD
	[CountryID] [int] NULL
	
	ALTER TABLE [dbo].[LocationPassOffice] ADD CONSTRAINT [FK_LocationPassOffice_LocationCountry] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[LocationCountry] ([CountryID])
	-- Column Exists
end

GO

--//@UNDO

if exists(select * from sys.columns 
            where Name = N'CountryID' and Object_ID = Object_ID(N'LocationPassOffice'))
begin
	ALTER TABLE [dbo].[LocationPassOffice] DROP CONSTRAINT [FK_LocationPassOffice_LocationCountry]
	
	ALTER TABLE [dbo].[LocationPassOffice] DROP
	COLUMN [CountryID]
end

GO









