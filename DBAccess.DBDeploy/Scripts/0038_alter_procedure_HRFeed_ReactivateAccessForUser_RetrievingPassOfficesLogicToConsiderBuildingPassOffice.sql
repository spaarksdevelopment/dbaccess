IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_ReactivateAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_ReactivateAccessForUser]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeed_ReactivateAccessForUser]    Script Date: 6/1/2015 5:30:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==========================================================================
-- History: 28/05/2015 (Wijitha) - Modified pass office retrieving logic considering the building pass offices as well.
--								   Fixed issue of re creating tast for the home pass office										
--==========================================================================

CREATE PROCEDURE [dbo].[HRFeed_ReactivateAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 

--	Flag as Reactivated
UPDATE HRPerson SET Revoked = NULL WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = NULL WHERE dbPeopleID = @dbPeopleId


--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int

exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
exec [dbo].[up_InsertRequestPerson]	@CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

--find out the person's home pass office
declare @homePassOfficeID int
declare @OfficerID varchar(2)

--first, make sure the OfficerID is available here, so it can be used later
select @OfficerID = OfficerID from HRPerson hr1
inner join AccessRequestPerson arp2 on hr1.dbPeopleID=arp2.dbPeopleID
where arp2.RequestPersonID = @RequestPersonID

select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = @CreatedByID,
			@RequestTypeID = 17,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = null,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
			
		fetch next from badgesCursor into @badgeId
	end
end
else
begin
	--we still want to send an revoke request even if there are no badges
	--create it with null for BadgeID
	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 17,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @homePassOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
end

close badgesCursor



--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
--declare passOfficeCursor cursor local fast_forward for
--	select distinct c.PassOfficeID from HRPersonAccessArea p
--		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
--		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
--		inner join LocationCity lc on lc.CityID = lb.CityID
--		inner join LocationCountry c on c.CountryID = lc.CountryID
--		where (p.EndDate is null or p.EndDate > getdate())
--			and c.PassOfficeID <> @homePassOfficeID
--			and p.dbPeopleId = @dbPeopleId

--declare @passOfficeID int
--open passOfficeCursor
--fetch next from passOfficeCursor into @passOfficeID

----make requests for the pass offices
--while (@@FETCH_STATUS = 0)
--	begin

if (@OfficerID is null) or (@OfficerID<>'D' and @OfficerID<>'MD')
begin
	--make a cursor to iterate through the pass offices for the areas that the person has access to
	--(besides their home pass office)
	declare passOfficeCursor cursor local fast_forward for
		select distinct ISNULL(bpo.PassOfficeID, c.PassOfficeID) as PassOfficeID 
		from HRPersonAccessArea p
			inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
			inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
			inner join LocationCity lc on lc.CityID = lb.CityID
			inner join LocationCountry c on c.CountryID = lc.CountryID
			left outer join LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.[Enabled] = 1
		where (p.EndDate is null or p.EndDate > getdate())
			and ISNULL(bpo.PassOfficeID, c.PassOfficeID) <> @homePassOfficeID
			and dbo.HasActivePassOfficers(isnull(bpo.PassOfficeID, c.PassOfficeID)) = 1
			and p.dbPeopleId = @dbPeopleId

	declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 17,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end
else
begin
	--make a cursor to iterate through the pass offices for the areas that the person has access to
	--(besides their home pass office)
	declare passOfficeCursor cursor local fast_forward for
		select PassOfficeID 
		from LocationPassOffice l
		where l.Enabled=1
			and l.PassOfficeID <> @homePassOfficeID
			and dbo.HasActivePassOfficers(l.PassOfficeID) = 1
			
	--declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 17,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end

		

--submit the request
declare @success bit
exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

end




GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_ReactivateAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_ReactivateAccessForUser]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[HRFeed_ReactivateAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 

--	Flag as Reactivated
UPDATE HRPerson SET Revoked = NULL WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = NULL WHERE dbPeopleID = @dbPeopleId


--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int

exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
exec [dbo].[up_InsertRequestPerson]	@CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

--find out the person's home pass office
declare @homePassOfficeID int
declare @OfficerID varchar(2)

--first, make sure the OfficerID is available here, so it can be used later
select @OfficerID = OfficerID from HRPerson hr1
inner join AccessRequestPerson arp2 on hr1.dbPeopleID=arp2.dbPeopleID
where arp2.RequestPersonID = @RequestPersonID

select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = @CreatedByID,
			@RequestTypeID = 17,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = null,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
			
		fetch next from badgesCursor into @badgeId
	end
end
else
begin
	--we still want to send an revoke request even if there are no badges
	--create it with null for BadgeID
	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 17,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @homePassOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
end

close badgesCursor



--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
--declare passOfficeCursor cursor local fast_forward for
--	select distinct c.PassOfficeID from HRPersonAccessArea p
--		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
--		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
--		inner join LocationCity lc on lc.CityID = lb.CityID
--		inner join LocationCountry c on c.CountryID = lc.CountryID
--		where (p.EndDate is null or p.EndDate > getdate())
--			and c.PassOfficeID <> @homePassOfficeID
--			and p.dbPeopleId = @dbPeopleId

--declare @passOfficeID int
--open passOfficeCursor
--fetch next from passOfficeCursor into @passOfficeID

----make requests for the pass offices
--while (@@FETCH_STATUS = 0)
--	begin

if (@OfficerID is null) or (@OfficerID<>'D' and @OfficerID<>'MD')
begin
	--make a cursor to iterate through the pass offices for the areas that the person has access to
	--(besides their home pass office)
	declare passOfficeCursor cursor local fast_forward for
		select distinct c.PassOfficeID from HRPersonAccessArea p
			inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
			inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
			inner join LocationCity lc on lc.CityID = lb.CityID
			inner join LocationCountry c on c.CountryID = lc.CountryID
			where (p.EndDate is null or p.EndDate > getdate())
				and c.PassOfficeID <> @homePassOfficeID
				and p.dbPeopleId = @dbPeopleId

	declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 17,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end
else
begin
	--make a cursor to iterate through the pass offices for the areas that the person has access to
	--(besides their home pass office)
	declare passOfficeCursor cursor local fast_forward for
		select PassOfficeID from LocationPassOffice l
			where l.Enabled=1
			
	--declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 17,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end

		

--submit the request
declare @success bit
exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

end




GO