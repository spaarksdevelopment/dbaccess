IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_InsertRequestPerson]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_InsertRequestPerson]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Authors:			AIS, SC
-- Created:			09/02/2011
-- Description:
--		Submits a master request, and generates tasks
-- History:
--		24/06/2011 (SC) - major update to fix the way that the business area is calculated
--						- also reworked the code to be laid out more clearly and use try/catch/raiserror
--      11/04/2014 (CM) - removed code to fill out BusinessArea fields
--      7/05/2015 (Wijitha) - Modified the loginc of getting the default country when country cannot be located for a person
-- =============================================
CREATE PROCEDURE [dbo].[up_InsertRequestPerson]
(
	@CreatedByUserID Int,
	@RequestMasterID Int,
	@dbPeopleID Int,
	@RequestPersonID Int Output,
	@bCheckBusinessArea Bit
)
As
Begin

	Set NoCount On;

	Set @RequestPersonID = 0;

	Set @bCheckBusinessArea = IsNull (@bCheckBusinessArea, 0);

	Declare @dtNow DateTime2 = SysDateTime();

	Declare @iCount Int =
	(
		Select
			Count (1)
		From
			AccessRequestPerson
		Where
			dbPeopleID = @dbPeopleID
			And
			RequestMasterID = @RequestMasterID
	);

	If (0 != @iCount)
	Begin

		-- the person is already on this request

		Set @RequestPersonID =
		(
			Select
				RequestPersonID
			From
				AccessRequestPerson
			Where
				dbPeopleID = @dbPeopleID
				And
				RequestMasterID = @RequestMasterID
		);

		Set @RequestPersonID = -1;
		Return;

	End

	Begin Try

		Insert Into
			AccessRequestPerson
		(
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorID
			,VendorName
			,RequestMasterID
			,CreatedByID
			,Created
		)
		Select
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorId
			,VendorName
			,@RequestMasterID
			,@CreatedByUserID
			,@dtNow
		From
			HRPerson
		Where
			dbPeopleID = @dbPeopleID;

		Set @RequestPersonID = Scope_Identity();

		
		Declare @iCountryID Int;

		Set @iCountryID =
		(
			Select Top 1 CountryID
			From HRPerson
				Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName				
			Where HRPerson.dbPeopleID = @dbPeopleID
				And	LocationCountry.[Enabled] = 1				
		);

		Set @iCountryID = coalesce(@iCountryID, (select top 1 Value from AdminValues where [Key]='DefaultCountry'), 58); --Set the default country from Admin Values or take the US (id: 58) 

		Update AccessRequestPerson
		Set
			 CountryID = @iCountryID
		Where
			 RequestPersonID = @RequestPersonID;
	End Try
	Begin Catch

			Declare @iState Int;
			Declare @iSeverity Int;
			Declare @sMessage NVarChar(4000);

			Select
			@iState = Error_State (),
			@sMessage = Error_Message (),
			@iSeverity = Error_Severity ()

			PRINT CAST(@iState as varchar(100)) + cAST(@sMessage as varchar(1000)) + cAST(@iSeverity as varchar(100))

		Set @RequestPersonID = 0;
	End Catch

End;

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_InsertRequestPerson]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_InsertRequestPerson]
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Authors:			AIS, SC
-- Created:			09/02/2011
-- Description:
--		Submits a master request, and generates tasks
-- History:
--		24/06/2011 (SC) - major update to fix the way that the business area is calculated
--						- also reworked the code to be laid out more clearly and use try/catch/raiserror
--      11/04/2014 (CM) - removed code to fill out BusinessArea fields
-- =============================================
CREATE PROCEDURE [dbo].[up_InsertRequestPerson]
(
	@CreatedByUserID Int,
	@RequestMasterID Int,
	@dbPeopleID Int,
	@RequestPersonID Int Output,
	@bCheckBusinessArea Bit
)
As
Begin

	Set NoCount On;

	Set @RequestPersonID = 0;

	Set @bCheckBusinessArea = IsNull (@bCheckBusinessArea, 0);

	Declare @dtNow DateTime2 = SysDateTime();

	Declare @iCount Int =
	(
		Select
			Count (1)
		From
			AccessRequestPerson
		Where
			dbPeopleID = @dbPeopleID
			And
			RequestMasterID = @RequestMasterID
	);

	If (0 != @iCount)
	Begin

		-- the person is already on this request

		Set @RequestPersonID =
		(
			Select
				RequestPersonID
			From
				AccessRequestPerson
			Where
				dbPeopleID = @dbPeopleID
				And
				RequestMasterID = @RequestMasterID
		);

		Set @RequestPersonID = -1;
		Return;

	End

	Begin Try

		Insert Into
			AccessRequestPerson
		(
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorID
			,VendorName
			,RequestMasterID
			,CreatedByID
			,Created
		)
		Select
			 dbPeopleID
			,IsExternal
			,EmailAddress
			,Forename
			,Surname
			,Telephone
			,CostCentre
			,UBR
			,VendorId
			,VendorName
			,@RequestMasterID
			,@CreatedByUserID
			,@dtNow
		From
			HRPerson
		Where
			dbPeopleID = @dbPeopleID;

		Set @RequestPersonID = Scope_Identity();

		
		Declare @iCountryID Int;

		Set @iCountryID =
		(
			Select Top 1
				CountryID
			From
				HRPerson
			Inner Join
				LocationCountry
				On
				LocationCountry.Name = HRPerson.CountryName
			Where
				HRPerson.dbPeopleID = @dbPeopleID
				And
				LocationCountry.[Enabled] = 1
		);

		Set @iCountryID = IsNull (@iCountryID, 58);		-- set to UK for now if not available

		Update AccessRequestPerson
		Set
			 CountryID = @iCountryID
		Where
			 RequestPersonID = @RequestPersonID;
	End Try
	Begin Catch

			Declare @iState Int;
			Declare @iSeverity Int;
			Declare @sMessage NVarChar(4000);

			Select
			@iState = Error_State (),
			@sMessage = Error_Message (),
			@iSeverity = Error_Severity ()

			PRINT CAST(@iState as varchar(100)) + cAST(@sMessage as varchar(1000)) + cAST(@iSeverity as varchar(100))

		Set @RequestPersonID = 0;
	End Catch

End;