IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRequestHistory]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetRequestHistory]
END
GO


/****** Object:  UserDefinedFunction [dbo].[GetRequestHistory]    Script Date: 10/03/17 12:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--========================================================================
-- History: 
--			09/03/2017 (Wijitha) - Get the request history for smartcard requests
--			10/03/2017 (Adheeb)  - Set appropriate text for smart card approval status
--========================================================================

CREATE FUNCTION [dbo].[GetRequestHistory]
(
      @ApproverName NVARCHAR(301),
      @AccessApproverName NVARCHAR(301),
      @RequestTypeID INT,
      @Approved BIT,
      @AccessApproved BIT,
      @AwaitingActionFrom varchar(4000),
      @ApprovedDate DATETIME,
      @AccessApprovedDate DATETIME,
      @LineBreakChar NVARCHAR(5)
)
 
 
RETURNS VARCHAR(4000)
AS
BEGIN
      -- Declare the return variable here
      DECLARE @ReturnVal NVARCHAR(4000)
      SET @ReturnVal = ''
      
      IF @RequestTypeID = 2 
      BEGIN
            IF @Approved IS NOT NULL 
            BEGIN
                  IF @Approved = 1 
                        SET @ReturnVal = 'Request approved by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106) + '#' + '\r'
                  ELSE
                        SET @ReturnVal = 'Request rejected by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106) + '#'+ '\r' 
            END
            
            IF @AccessApproved IS NOT NULL 
            BEGIN
                  IF @AccessApproved = 1 
                        SET @ReturnVal = @ReturnVal + 'Access approved by ' + @AccessApproverName + '#' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
                  ELSE
                        SET @ReturnVal = @ReturnVal + 'Access rejected by ' + @AccessApproverName + ' #' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
            END
      END
      ELSE
      BEGIN
            DECLARE @CustomText NVARCHAR(100)
            
            SELECT @CustomText= 
                  CASE @RequestTypeID 
                        WHEN 1 THEN 'Pass Request' 
                        WHEN 7 THEN 'Division Admin role' 
                        WHEN 8 THEN 'Access Area Approver role'
                        WHEN 9 THEN 'Access Area Recertifier role'
                        WHEN 10 THEN 'Request Approver role' 
                        WHEN 11 THEN 'Pass Office role' 
                        WHEN 12 THEN 'Division Owner role'
                  END
            
            IF @Approved IS NOT NULL 
            BEGIN
                IF @Approved = 1 
                    SET @ReturnVal = @CustomText + ' approved by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106)  + '#'+ '\r'
                ELSE
                    SET @ReturnVal = @CustomText + ' rejected by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106)  + '#'+ '\r'
				
            END    
			
			IF @AccessApproved IS NOT NULL AND @RequestTypeID = 1
			BEGIN
			SET @CustomText = 'Smart card approval'
				IF @AccessApproved = 1 
					SET @ReturnVal = @ReturnVal + @CustomText + ' granted by ' + @AccessApproverName + '#' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
				ELSE
					SET @ReturnVal = @ReturnVal + @CustomText + ' declined by ' + @AccessApproverName + ' #' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
			END
      END
      
      IF @AwaitingActionFrom IS NOT NULL
            SET @ReturnVal = @ReturnVal + 'Awaiting action from ' + @AwaitingActionFrom + '\r'
               
      IF (RIGHT(@ReturnVal,2)='\r')
            SET @ReturnVal = LEFT(@ReturnVal, LEN(@ReturnVal)-2)
 
      SET @ReturnVal = Replace(@ReturnVal, '\r', @LineBreakChar)
      SET @ReturnVal = Replace(@ReturnVal, '\\r', @LineBreakChar)
      
      RETURN @ReturnVal
 
END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRequestHistory]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetRequestHistory]
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetRequestHistory]    Script Date: 10/03/17 11:18:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--========================================================================
-- History: 
--			09/03/2017 (Wijitha) - Get the request history for smartcard requests
--========================================================================

CREATE FUNCTION [dbo].[GetRequestHistory]
(
      @ApproverName NVARCHAR(301),
      @AccessApproverName NVARCHAR(301),
      @RequestTypeID INT,
      @Approved BIT,
      @AccessApproved BIT,
      @AwaitingActionFrom varchar(4000),
      @ApprovedDate DATETIME,
      @AccessApprovedDate DATETIME,
      @LineBreakChar NVARCHAR(5)
)
 
 
RETURNS VARCHAR(4000)
AS
BEGIN
      -- Declare the return variable here
      DECLARE @ReturnVal NVARCHAR(4000)
      SET @ReturnVal = ''
      
      IF @RequestTypeID = 2 
      BEGIN
            IF @Approved IS NOT NULL 
            BEGIN
                  IF @Approved = 1 
                        SET @ReturnVal = 'Request approved by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106) + '#' + '\r'
                  ELSE
                        SET @ReturnVal = 'Request rejected by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106) + '#'+ '\r' 
            END
            
            IF @AccessApproved IS NOT NULL 
            BEGIN
                  IF @AccessApproved = 1 
                        SET @ReturnVal = @ReturnVal + 'Access approved by ' + @AccessApproverName + '#' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
                  ELSE
                        SET @ReturnVal = @ReturnVal + 'Access rejected by ' + @AccessApproverName + ' #' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
            END
      END
      ELSE
      BEGIN
            DECLARE @CustomText NVARCHAR(100)
            
            SELECT @CustomText= 
                  CASE @RequestTypeID 
                        WHEN 1 THEN 'Pass Request' 
                        WHEN 7 THEN 'Division Admin role' 
                        WHEN 8 THEN 'Access Area Approver role'
                        WHEN 9 THEN 'Access Area Recertifier role'
                        WHEN 10 THEN 'Request Approver role' 
                        WHEN 11 THEN 'Pass Office role' 
                        WHEN 12 THEN 'Division Owner role'
                  END
            
            IF @Approved IS NOT NULL 
            BEGIN
                IF @Approved = 1 
                    SET @ReturnVal = @CustomText + ' approved by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106)  + '#'+ '\r'
                ELSE
                    SET @ReturnVal = @CustomText + ' rejected by ' + @ApproverName + '#' + CONVERT(NVARCHAR(20), @ApprovedDate, 106)  + '#'+ '\r'
				
            END    
			
			IF @AccessApproved IS NOT NULL 
			BEGIN
				IF @AccessApproved = 1 
					SET @ReturnVal = @ReturnVal + @CustomText + ' approved by ' + @AccessApproverName + '#' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
				ELSE
					SET @ReturnVal = @ReturnVal + @CustomText + ' rejected by ' + @AccessApproverName + ' #' + CONVERT(NVARCHAR(20), ISNULL(@ApprovedDate, @AccessApprovedDate), 106) + '#'+ '\r' 
			END
      END
      
      IF @AwaitingActionFrom IS NOT NULL
            SET @ReturnVal = @ReturnVal + 'Awaiting action from ' + @AwaitingActionFrom + '\r'
               
      IF (RIGHT(@ReturnVal,2)='\r')
            SET @ReturnVal = LEFT(@ReturnVal, LEN(@ReturnVal)-2)
 
      SET @ReturnVal = Replace(@ReturnVal, '\r', @LineBreakChar)
      SET @ReturnVal = Replace(@ReturnVal, '\\r', @LineBreakChar)
      
      RETURN @ReturnVal
 
END


GO