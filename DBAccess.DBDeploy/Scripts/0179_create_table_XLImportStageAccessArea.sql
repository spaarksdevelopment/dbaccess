IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessArea')
BEGIN


CREATE TABLE [dbo].[XLImportStageAccessArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[AccessAreaType] [nvarchar](500) NULL,
	[FloorName] [nvarchar](500) NULL,
	[CategoryDescription] [nvarchar](500) NULL,
	[Building] [nvarchar](500) NULL,
	[ControlSystem] [nvarchar](500) NULL,
	[RecertPeriod] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStageAccessArea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[XLImportStageAccessArea]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStageAccessArea_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])


ALTER TABLE [dbo].[XLImportStageAccessArea] CHECK CONSTRAINT [FK_XLImportStageAccessArea_XLImportFile]


END

GO


--//@UNDO




IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessArea')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageAccessArea]
	
END

GO