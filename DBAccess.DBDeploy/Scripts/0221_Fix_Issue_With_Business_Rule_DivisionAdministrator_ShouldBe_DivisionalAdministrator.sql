DELETE [dbo].[XLImportValidationRule] WHERE ID=41

SET IDENTITY_INSERT [dbo].[XLImportValidationRule] ON 
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (41, 6, N'Error', N'Security Group must be either Division Owner or Divisional Administrator', N'select ID, RowNumber from XLImportStageDivisionRole where SecurityGroup is not null and len(SecurityGroup) > 0  and SecurityGroup not in (''Division Owner'', ''Divisional Administrator'')', 1)
SET IDENTITY_INSERT [dbo].[XLImportValidationRule] OFF

--//@UNDO
DELETE [dbo].[XLImportValidationRule] WHERE ID=41

SET IDENTITY_INSERT [dbo].[XLImportValidationRule] ON 
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (41, 6, N'Error', N'Security Group must be either Division Owner or Division Administrator', N'select ID, RowNumber from XLImportStageDivisionRole where SecurityGroup is not null and len(SecurityGroup) > 0  and SecurityGroup not in (''Division Owner'', ''Divisional Administrator'')', 1)
SET IDENTITY_INSERT [dbo].[XLImportValidationRule] OFF

GO
