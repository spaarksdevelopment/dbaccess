IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_LogSkippedRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[HRFeed_LogSkippedRequests]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_LogSkippedRequests]    Script Date: 12/2/2015 9:56:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- Author: Wijitha Wijenayake
-- Create Date: 01/12/2015
-- Description:  Log skipped revoked and offboard requests due to conflicts in Revoke and Termination Dates
-- History: 16/12/2015 (Wijitha) - Modified logic to consider only the LVER  triggers
--======================================================================

CREATE PROCEDURE [dbo].[HRFeed_LogSkippedRequests]

AS
BEGIN

	DECLARE @HRFeedMsgID int
	DECLARE @TransactionID int
	DECLARE @HRID int
	DECLARE @RequesstType varchar(10)
	DECLARE @RevokeDate datetime
	DECLARE @TerminationDate datetime
	DECLARE @DBPeopleID int

	DECLARE cur_revokeSkip CURSOR FOR 

	SELECT HRFeedMessageId, TransactionId, HRID, 'Revoke' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE TriggerType = 'LVER' AND TriggerAction <> 'DEL' 
		AND RevokeProcessed = 1
		AND ISNULL(RevokeSkippedLogged, 0) = 0
		AND RevokeAccessDateTimestamp <= GETDATE() 
		AND TerminationDate < RevokeAccessDateTimestamp 
		AND DATEADD(d, 7, TerminationDate) < RevokeAccessDateTimestamp 
	
	UNION

	SELECT HRFeedMessageId, TransactionId, HRID, 'Offboard' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE TriggerType = 'LVER' AND TriggerAction <> 'DEL'
		AND Processed = 1
		AND ISNULL(OffboardSkippedLogged, 0) = 0
		AND TerminationDate <= GETDATE() 
		AND RevokeAccessDateTimestamp < TerminationDate
		AND DATEADD(d, 7, RevokeAccessDateTimestamp) < TerminationDate 		 
	ORDER BY HRFeedMessageId

	OPEN cur_revokeSkip

	FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @DBPeopleID = CAST(REPLACE(@HRID, 'C', '') as INT)

		IF(@RequesstType = 'Revoke')
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 16, 'Revoke skipped. Already revoked on ' + CONVERT(varchar(15), @TerminationDate, 106), GETDATE())

			UPDATE HRFeedMessage SEt RevokeSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
		ELSE
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 15, 'Offboard skipped. Already offboarded on ' + CONVERT(varchar(15), @RevokeDate + 7, 106), GETDATE())

			UPDATE HRFeedMessage SEt OffboardSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
	
		FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	END

	CLOSE cur_revokeSkip
	DEALLOCATE cur_revokeSkip

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_LogSkippedRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[HRFeed_LogSkippedRequests]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_LogSkippedRequests]    Script Date: 12/2/2015 9:56:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- Author: Wijitha Wijenayake
-- Create Date: 01/12/2015
-- Description:  Log skipped revoked and offboard requests due to conflicts in Revoke and Termination Dates
--======================================================================

CREATE PROCEDURE [dbo].[HRFeed_LogSkippedRequests]

AS
BEGIN

	DECLARE @HRFeedMsgID int
	DECLARE @TransactionID int
	DECLARE @HRID int
	DECLARE @RequesstType varchar(10)
	DECLARE @RevokeDate datetime
	DECLARE @TerminationDate datetime
	DECLARE @DBPeopleID int

	DECLARE cur_revokeSkip CURSOR FOR 

	SELECT HRFeedMessageId, TransactionId, HRID, 'Revoke' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE RevokeProcessed = 1
		AND ISNULL(RevokeSkippedLogged, 0) = 0
		AND RevokeAccessDateTimestamp <= GETDATE() 
		AND TerminationDate < RevokeAccessDateTimestamp 
		AND DATEADD(d, 7, TerminationDate) < RevokeAccessDateTimestamp 
	
	UNION

	SELECT HRFeedMessageId, TransactionId, HRID, 'Offboard' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE Processed = 1
		AND ISNULL(OffboardSkippedLogged, 0) = 0
		AND TerminationDate <= GETDATE() 
		AND RevokeAccessDateTimestamp < TerminationDate
		AND DATEADD(d, 7, RevokeAccessDateTimestamp) < TerminationDate 		 
	ORDER BY HRFeedMessageId

	OPEN cur_revokeSkip

	FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @DBPeopleID = CAST(REPLACE(@HRID, 'C', '') as INT)

		IF(@RequesstType = 'Revoke')
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 16, 'Revoke skipped. Already revoked on ' + CONVERT(varchar(15), @TerminationDate, 106), GETDATE())

			UPDATE HRFeedMessage SEt RevokeSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
		ELSE
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 15, 'Offboard skipped. Already offboarded on ' + CONVERT(varchar(15), @RevokeDate + 7, 106), GETDATE())

			UPDATE HRFeedMessage SEt OffboardSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
	
		FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	END

	CLOSE cur_revokeSkip
	DEALLOCATE cur_revokeSkip

END

GO