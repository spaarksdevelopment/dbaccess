IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessAreaRecertifiers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessAreaRecertifiers]
END
GO

-- =============================================
-- Author:		Wijitha Wijenayake
-- Create date: 6/01/2016
-- Description:	Copy Staged Access Area Recertifiers in to Actual table
-- =============================================

CREATE PROCEDURE [dbo].[XLImportCopyAccessAreaRecertifiers]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO CorporateDivisionAccessAreaApprover (CorporateDivisionAccessAreaID, Enabled, DateAccepted, RecertificationEmails, IsExternal, dbPeopleID, mp_SecurityGroupID)
	OUTPUT inserted.CorporateDivisionAccessAreaApproverId into @tblIDs
	SELECT CorporateDivisionAccessAreaID,												
		1,
		CAST(DateAccepted AS smalldatetime),
		0,
		0,
		CAST(DBPeopleID AS int),
		(SELECT TOP 1 Id  FROM mp_SecurityGroup where RoleShortName = 'AR')
	FROM XLImportStageAccessAreaRecertifier saar
		INNER JOIN XLImportStageCorporateArea sca on saar.Division = sca.Division AND saar.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry on divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a on a.BuildingID = b.BuildingID AND a.Name = saar.AccessArea
		INNER JOIN CorporateDivision d on d.Name = saar.Division AND d.CountryID = divisionCountry.CountryID
		INNER JOIN CorporateDivisionAccessArea CDAA ON d.DivisionID=CDAA.DivisionID AND a.AccessAreaID=CDAA.AccessAreaID
	WHERE ISNULL(saar.ValidationFailed, 0) = 0 AND saar.FileID = @FileID

	INSERT INTO mp_SecurityGroupUser(SecurityGroupID, dbPeopleID)
	SELECT DISTINCT 8 /*Access Recertifier*/ AS SecurityGroupID, dbPeopleID
	FROM [dbo].[XLImportStageAccessAreaApprover]

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionAccessAreaApprover', ID FROM @tblIDs

END

GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessAreaRecertifiers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessAreaRecertifiers]
END
GO

-- =============================================
-- Author:		Wijitha Wijenayake
-- Create date: 6/01/2016
-- Description:	Copy Staged Access Area Recertifiers in to Actual table
-- =============================================

CREATE PROCEDURE [dbo].[XLImportCopyAccessAreaRecertifiers]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO CorporateDivisionAccessAreaApprover (CorporateDivisionAccessAreaID, Enabled, DateAccepted, RecertificationEmails, IsExternal, dbPeopleID, mp_SecurityGroupID)
	OUTPUT inserted.CorporateDivisionAccessAreaApproverId into @tblIDs
	SELECT CorporateDivisionAccessAreaID,												
		1,
		CAST(DateAccepted AS smalldatetime),
		0,
		0,
		CAST(DBPeopleID AS int),
		(SELECT TOP 1 Id  FROM mp_SecurityGroup where RoleShortName = 'AR')
	FROM XLImportStageAccessAreaRecertifier saar
		INNER JOIN XLImportStageCorporateArea sca on saar.Division = sca.Division AND saar.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry on divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a on a.BuildingID = b.BuildingID AND a.Name = saar.AccessArea
		INNER JOIN CorporateDivision d on d.Name = saar.Division AND d.CountryID = divisionCountry.CountryID
		INNER JOIN CorporateDivisionAccessArea CDAA ON d.DivisionID=CDAA.DivisionID AND a.AccessAreaID=CDAA.AccessAreaID
	WHERE ISNULL(saar.ValidationFailed, 0) = 0 AND saar.FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionAccessAreaApprover', ID FROM @tblIDs

END

GO

