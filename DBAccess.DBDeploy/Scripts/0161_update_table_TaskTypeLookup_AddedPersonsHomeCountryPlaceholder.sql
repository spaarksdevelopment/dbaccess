UPDATE TaskTypeLookup
set Description = 'Access Approval Stage 1: Do you Approve {1} from {8} of {3} to be granted this access?' 
WHERE TaskTypeId = 3 

GO


--//@UNDO


UPDATE TaskTypeLookup
set Description = 'Access Approval Stage 1: Do you Approve {1} from {2} of {3} to be granted this access?' 
WHERE TaskTypeId = 3 

GO