
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_SubmitRequestMaster]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_SubmitRequestMaster]
END
GO


/****** Object:  StoredProcedure [dbo].[up_SubmitRequestMaster]    Script Date: 6/1/2015 1:03:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Authors:             AIS, SC
-- Created:             15/02/2011
-- Description:
--          Submits a master request, AND generates tasks
-- History:
--          22/06/2011 (SC) major UPDATE to fix the way that access is handled (Request Type 2)
--                                  also reworked the code to be laid out more clearly AND use try/catch/raiserror
--          15/07/2011 (SC)   modified to accept CASEs WHERE there is no BusinessAreaID
--          10/11/2014 (CM)   Removed BusinessAreaID from call to CreateTasks
--          01/06/2015 (Wijitha) Ignored disabled pass officers when asigning tasks
--			16/10/2015 (Asanka) Clear revoke flag of a user if an access or pass request is raised for a revoked user
-- =============================================

CREATE PROCEDURE [dbo].[up_SubmitRequestMaster]
(
	@dbPeopleID Int,
	@RequestMasterID Int,
	@Success Bit Output
)
As
BEGIN

	SET NoCount ON;
	SET @Success = 0;

	DECLARE @dtNow DateTime2 = SysDateTime ();
	DECLARE @doToday Date = @dtNow;

	BEGIN Try

		DECLARE @iRequestTypeID Int = (SELECT Top 1 RequestTypeID FROM vw_AccessRequest WHERE RequestMasterID = @RequestMasterID);

		IF (0 = @iRequestTypeID) RaisError (N'Could not find Request Type for Master.', 11, 1);


		-- UPDATE all Requests (AND Master) to be 'Submitted':
		--====================================================
		UPDATE
			AccessRequestMaster
		SET
			RequestTypeID = @iRequestTypeID,
			RequestStatusID = 2                 -- Submitted
		WHERE
			  RequestMasterID = @RequestMasterID;

		UPDATE
			  [AccessRequest]
		SET
			  [RequestStatusID] = 2         -- Submitted
		WHERE
			  RequestPersonID
			  In
			  (
					SELECT RequestPersonID FROM [AccessRequestPerson] WHERE RequestMasterID = @RequestMasterID
			  );
		--======================================================================================================
			
		--[MAHMOUD.ALI]
		-- CREATE TASKS FOR NEW PASS REQUESTS:
		--============================================================
		-- CREATE OLDBADGE DE-ACTIVATION TASKS FOR PASS REPLACEMENT REQUEST(S):
		--=====================================================================
		IF @iRequestTypeID = 1 
		BEGIN
			DECLARE @BadgeID Int;
			DECLARE @testTable Table (BadgeID Int);

			INSERT INTO
				@testTable
			SELECT
				badgeID
			FROM
				AccessRequest
			WHERE
				RequestPersonID IN (SELECT RequestPersonID FROM [AccessRequestPerson] WHERE RequestMasterID = @RequestMasterID)
				AND
				BadgeID IS NOT NULL;

			WHILE (SELECT COUNT(BadgeID) FROM @testTable) > 0
			BEGIN
				SET @BadgeID = (SELECT Top 1 badgeID FROM @testTable);

				IF @BadgeID IS NOT NULL 
				BEGIN
								
					--1- CREATE REPLACEMENT TASK:
					--============================
					INSERT INTO
						Tasks
						(
							[TaskTypeId],
							[RequestId],
							[DivisionId],
							[BusinessAreaId],
							[AccessAreaId],
							[Description],
							[CreatedByID],
							[CreatedDate]
						)
					SELECT
						12,
						AccessRequest.[RequestID],
						AccessRequest.[DivisionID],
						vw_AccessRequestPerson.[BusinessAreaID],
						AccessRequest.[AccessAreaID],
						--'Deactivate ' + (CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END) + ' pass for ' + vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )' + (CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END) As [Description],
						REPLACE(REPLACE(REPLACE('Deactivate  {6} pass for {1}{5}',
						'{1}', vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )'),
						'{5}', CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END),
						--[MAHMOUD.ALI]
						'{6}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END) As [Description], 
						--[/MAHMOUD.ALI]          
						@dbPeopleID,
						@dtNow
					FROM
						vw_AccessRequestPerson 
						INNER JOIN AccessRequest ON AccessRequest.[RequestPersonID] = vw_AccessRequestPerson.[RequestPersonID]
						LEFT OUTER JOIN vw_AccessArea ON vw_AccessArea.[AccessAreaID] = AccessRequest.[AccessAreaID]
						LEFT OUTER JOIN CorporateDivision ON CorporateDivision.[DivisionID] = AccessRequest.[DivisionID]
						LEFT OUTER JOIN HRPersonBadge ON HRPersonBadge.[PersonBadgeID] = AccessRequest.[BadgeID]
						LEFT OUTER JOIN LocationBuilding ON LocationBuilding.[BuildingID] = HRPersonBadge.[BuildingID]
					WHERE
						vw_AccessRequestPerson.[RequestMasterID] = @RequestMasterID
						AND
						AccessRequest.[BadgeID] = @BadgeID
				END

				DELETE FROM @testTable WHERE badgeID = @BadgeID;

			END
		END
			--[/MAHMOUD.ALI]

			-- CREATE ALL TASKS EXCEPT PASS REPLACEMENT TASKS:
			--================================================
			
			IF @iRequestTypeID NOT In (2, 3)
			Begin
				INSERT INTO
					Tasks
					(
						[TaskTypeId],
						[RequestId],
						[DivisionId],
						[BusinessAreaId],
						[AccessAreaId],
						[Description],
						[CreatedByID],
						[CreatedDate]
					)
				SELECT
					TaskTypeLookup.[TaskTypeId],
					AccessRequest.[RequestID],
					AccessRequest.[DivisionID],
					vw_AccessRequestPerson.[BusinessAreaID],
					AccessRequest.[AccessAreaID],
					REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TaskTypeLookup.[Description],
					'{1}', vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )'),
					'{2}', IsNull((SELECT Top 1 CountryName FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + ' of ' + (SELECT Top 1 [UBR] FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + '-' + vw_AccessRequestPerson.UBR_Name,'')),
					'{4}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END),
					'{5}', CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END),
					--[MAHMOUD.ALI]
					'{6}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END),
					'{7}', (CASE WHEN LocationBuilding.[Name] IS NOT NULL THEN ' ' + LocationBuilding.[Name] + '/' ELSE '' END) + (CASE WHEN [FloorName] IS NOT NULL THEN ' ' + [FloorName] + '/' ELSE '' END) + (CASE WHEN [AccessAreaName] IS NOT NULL THEN ' ' + [AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN [AccessAreaTypeName] IS NOT NULL THEN ' ' + [AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN [ENDDate] IS NOT NULL THEN ' until ' + Convert(VarChar(11), [ENDDate], 106) ELSE '' END)),
					'{8}', IsNULL('for the ' + LocationPassOffice.Name, '')) As [Description],
					--[/MAHMOUD.ALI]          
					@dbPeopleID,
					@dtNow
				FROM
					vw_AccessRequestPerson 
					INNER JOIN AccessRequest ON AccessRequest.[RequestPersonID] = vw_AccessRequestPerson.[RequestPersonID]
					INNER JOIN AccessRequestType ON AccessRequestType.[RequestTypeID] = AccessRequest.[RequestTypeID]
					INNER JOIN TaskTypeLookup ON TaskTypeLookup.[TaskTypeId] = AccessRequestType.[TaskTypeID]
					LEFT JOIN LocationPassOffice ON AccessRequest.[PassOfficeID] = LocationPassOffice.[PassOfficeID]
					LEFT OUTER JOIN vw_AccessArea ON vw_AccessArea.[AccessAreaID] = AccessRequest.[AccessAreaID]
					LEFT OUTER JOIN CorporateDivision ON CorporateDivision.[DivisionID] = AccessRequest.[DivisionID]
					LEFT OUTER JOIN HRPersonBadge ON HRPersonBadge.[PersonBadgeID] = AccessRequest.[BadgeID]
					LEFT OUTER JOIN LocationBuilding ON LocationBuilding.[BuildingID] = HRPersonBadge.[BuildingID]
				WHERE
					vw_AccessRequestPerson.[RequestMasterID] = @RequestMasterID;
			End
					  
			--=================================================================
			
			-- Assign tasks to users:
			--========================

			IF (@iRequestTypeID in (1, 14))
			BEGIN
				--new pass/replacement pass, or pass deactivation
				INSERT INTO 
					[TasksUsers]
					(
						[TaskId],
						[dbPeopleID],
						[IsCurrent],
						[Order]
					)
				SELECT
					Tasks.TaskId,
					LocationPassOfficeUser.dbPeopleID,
					1,
					1
				FROM
					Tasks
					INNER JOIN AccessRequest ON Tasks.[RequestId] = AccessRequest.[RequestID]
					INNER JOIN AccessRequestPerson ON AccessRequest.[RequestPersonID] = AccessRequestPerson.[RequestPersonID]
					INNER JOIN AccessRequestDeliveryDetail ON AccessRequestPerson.[RequestMasterID] = AccessRequestDeliveryDetail.[RequestMasterID]
					INNER JOIN LocationPassOfficeUser ON AccessRequestDeliveryDetail.[PassOfficeID] = LocationPassOfficeUser.[PassOfficeID]
				WHERE
					AccessRequestPerson.[RequestMasterID] = @RequestMasterID
					AND
					AccessRequest.RequestTypeID = @iRequestTypeID
					AND
					LocationPassOfficeUser.IsEnabled = 1;

			END
            ELSE IF (@iRequestTypeID In (2, 3))
            Begin
				-- Access / Service
				--creating tasks on a per person basis
				Declare @cmd varchar(max)
				Select
						@cmd = Coalesce(@cmd + ' ', '') + 'EXEC dbo.CreateTasks ' + '@iRequestPersonID=' + cast(AccessRequestPerson.RequestPersonID as varchar(30)) + ',' + '@dtNow=' + '''' + CONVERT(varchar,@dtNow,127) + '''' +',' + '@dbPeopleID=' + cast(@dbPeopleID as varchar(20))+ ','+ '@doToday=' + '''' + CONVERT(varchar,@doToday,127)  + ''''
																
				From
					AccessRequestPerson
				Inner Join
					AccessRequestMaster
					On
					AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
				Where
					AccessRequestMaster.RequestMasterID = @RequestMasterID
				Order By
					RequestPersonID;
				
				EXEC(@cmd)            
            END
            ELSE IF (@iRequestTypeID in (15,16,17,18, 19, 20)) --Revoke, Reactivate, Change start date, Suspend and Unsuspend follow similar logic to offboarding
            BEGIN
				--Offboarding request
				--extra offboarding requests are made for the areas that the person has access to, which have the
				--pass office ID explicitly set - deal with those here, and the rest will get picked up below
				INSERT INTO TasksUsers ([TaskId], [dbPeopleID], [IsCurrent], [Order])
					SELECT t.TaskId, u.dbPeopleID, 1, 1
					FROM Tasks t
						INNER JOIN AccessRequest ar ON ar.RequestID = t.RequestId
						INNER JOIN AccessRequestPerson ON ar.[RequestPersonID] = AccessRequestPerson.[RequestPersonID]
						INNER JOIN LocationPassOfficeUser u on u.PassOfficeID = ar.PassOfficeID
					Where AccessRequestPerson.RequestMasterID = @RequestMasterID
					AND
					u.IsEnabled = 1
            END


            IF (@iRequestTypeID != 2)
            BEGIN
                  -- Now assign to home pass office of request person any remaining tasks which were not assigned
                  -- This will catch any unassigned new pass requests as well
                  INSERT INTO
                        [TasksUsers]
                  (
                        [TaskId],
                        [dbPeopleID],
                        [IsCurrent],
                        [Order]
                  )
                  SELECT
                        Tasks.TaskId,
                        LocationPassOfficeUser.dbPeopleID,
                        1,
                        1
                  FROM
                        Tasks
                  INNER JOIN
                        AccessRequest
                        ON
                        Tasks.RequestId = AccessRequest.RequestID
                  INNER JOIN
                        AccessRequestPerson
                        ON
                        AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
                  INNER JOIN
                        LocationCountry
                        ON
                        AccessRequestPerson.CountryID = LocationCountry.CountryID
                  INNER JOIN
                        LocationPassOfficeUser
                        ON
                        LocationCountry.PassOfficeID = LocationPassOfficeUser.PassOfficeID
                  LEFT OUTER JOIN
                        TasksUsers
                        ON
                        Tasks.TaskId = TasksUsers.TaskId
                  WHERE
                        (AccessRequestPerson.RequestMasterID = @RequestMasterID)
                        AND
                        (TasksUsers.TasksUsersId IS NULL)         -- unassigned (no entry exists)
                        AND
                        (LocationCountry.Enabled = 1)
						AND 
						(LocationPassOfficeUser.IsEnabled = 1);

                  -- Finally, just in CASE any request persons did not have a home pass office
                  -- assign remaining tasks to pass office of location being accessed
                  INSERT INTO
                        [TasksUsers]
                  (
                        [TaskId],
                        [dbPeopleID],
                        [IsCurrent],
                        [Order]
                  )
                  SELECT
                        Tasks.TaskId,
                        LocationPassOfficeUser.dbPeopleID,
                        1,
                        1
                  FROM
                        TasksUsers
                  Right Outer Join
                        vw_AccessArea
                  INNER JOIN
                        LocationPassOfficeUser
                  INNER JOIN
                        LocationCountry
                        ON
                        LocationPassOfficeUser.PassOfficeID = LocationCountry.PassOfficeID
                        ON
                        vw_AccessArea.CountryID = LocationCountry.CountryID
                  INNER JOIN
                        Tasks
                  INNER JOIN
                        AccessRequest
                        ON
                        Tasks.RequestId = AccessRequest.RequestID
                  INNER JOIN
                        AccessRequestPerson
                        ON
                        AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
                        ON
                        vw_AccessArea.AccessAreaID = AccessRequest.AccessAreaID
                        ON
                        TasksUsers.TaskId = Tasks.TaskId
                  WHERE
                        (AccessRequestPerson.RequestMasterID = @RequestMasterID)
                        AND
                        (TasksUsers.TasksUsersId IS NULL)         -- unassigned (no entry exists)
                        AND
                        (LocationCountry.Enabled = 1)
						AND
						(LocationPassOfficeUser.IsEnabled = 1);
            END

			IF(@iRequestTypeID in (1,2))
			BEGIN
			--Clear revoke flag when Pass And Access requests raised
			UPDATE mp_User SET Revoked = NULL WHERE dbPeopleID in (SELECT dbPeopleID FROM [AccessRequestPerson] WHERE RequestMasterID = @RequestMasterID)

			END

            SET @Success = 1;

      END Try
      BEGIN Catch


            -- raise an error with the details of the exception

            DECLARE @iState Int;
            DECLARE @iSeverity Int;
            DECLARE @sMessage NVarChar(4000);

            SELECT
                  @iState = Error_State (),
                  @sMessage = Error_Message (),
                  @iSeverity = Error_Severity ()

            RaisError (@sMessage, @iSeverity, @iState);

      END Catch
END;


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_SubmitRequestMaster]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_SubmitRequestMaster]
END

/****** Object:  StoredProcedure [dbo].[up_SubmitRequestMaster]    Script Date: 6/1/2015 1:01:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Authors:             AIS, SC
-- Created:             15/02/2011
-- Description:
--          Submits a master request, AND generates tasks
-- History:
--          22/06/2011 (SC) major UPDATE to fix the way that access is handled (Request Type 2)
--                                  also reworked the code to be laid out more clearly AND use try/catch/raiserror
--          15/07/2011 (SC)   modified to accept CASEs WHERE there is no BusinessAreaID
--          10/11/2014 (CM)   Removed BusinessAreaID from call to CreateTasks
--          01/06/2015 (Wijitha) Ignored disabled pass officers when asigning tasks
-- =============================================
CREATE PROCEDURE [dbo].[up_SubmitRequestMaster]
(
	@dbPeopleID Int,
	@RequestMasterID Int,
	@Success Bit Output
)
As
BEGIN

	SET NoCount ON;
	SET @Success = 0;

	DECLARE @dtNow DateTime2 = SysDateTime ();
	DECLARE @doToday Date = @dtNow;

	BEGIN Try

		DECLARE @iRequestTypeID Int = (SELECT Top 1 RequestTypeID FROM vw_AccessRequest WHERE RequestMasterID = @RequestMasterID);

		IF (0 = @iRequestTypeID) RaisError (N'Could not find Request Type for Master.', 11, 1);


		-- UPDATE all Requests (AND Master) to be 'Submitted':
		--====================================================
		UPDATE
			AccessRequestMaster
		SET
			RequestTypeID = @iRequestTypeID,
			RequestStatusID = 2                 -- Submitted
		WHERE
			  RequestMasterID = @RequestMasterID;

		UPDATE
			  [AccessRequest]
		SET
			  [RequestStatusID] = 2         -- Submitted
		WHERE
			  RequestPersonID
			  In
			  (
					SELECT RequestPersonID FROM [AccessRequestPerson] WHERE RequestMasterID = @RequestMasterID
			  );
		--======================================================================================================
			
		--[MAHMOUD.ALI]
		-- CREATE TASKS FOR NEW PASS REQUESTS:
		--============================================================
		-- CREATE OLDBADGE DE-ACTIVATION TASKS FOR PASS REPLACEMENT REQUEST(S):
		--=====================================================================
		IF @iRequestTypeID = 1 
		BEGIN
			DECLARE @BadgeID Int;
			DECLARE @testTable Table (BadgeID Int);

			INSERT INTO
				@testTable
			SELECT
				badgeID
			FROM
				AccessRequest
			WHERE
				RequestPersonID IN (SELECT RequestPersonID FROM [AccessRequestPerson] WHERE RequestMasterID = @RequestMasterID)
				AND
				BadgeID IS NOT NULL;

			WHILE (SELECT COUNT(BadgeID) FROM @testTable) > 0
			BEGIN
				SET @BadgeID = (SELECT Top 1 badgeID FROM @testTable);

				IF @BadgeID IS NOT NULL 
				BEGIN
				
					/*
					--Do some processing here
					UPDATE
						dbo.HRPersonBadge
					SET
						Active = 0,
						Valid = 0
					WHERE
						PersonBadgeID = @BadgeID;
					*/
					
					--1- CREATE REPLACEMENT TASK:
					--============================
					INSERT INTO
						Tasks
						(
							[TaskTypeId],
							[RequestId],
							[DivisionId],
							[BusinessAreaId],
							[AccessAreaId],
							[Description],
							[CreatedByID],
							[CreatedDate]
						)
					SELECT
						12,
						AccessRequest.[RequestID],
						AccessRequest.[DivisionID],
						vw_AccessRequestPerson.[BusinessAreaID],
						AccessRequest.[AccessAreaID],
						--'Deactivate ' + (CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END) + ' pass for ' + vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )' + (CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END) As [Description],
						REPLACE(REPLACE(REPLACE('Deactivate  {6} pass for {1}{5}',
						'{1}', vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )'),
						'{5}', CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END),
						--[MAHMOUD.ALI]
						'{6}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END) As [Description], 
						--[/MAHMOUD.ALI]          
						@dbPeopleID,
						@dtNow
					FROM
						vw_AccessRequestPerson 
						INNER JOIN AccessRequest ON AccessRequest.[RequestPersonID] = vw_AccessRequestPerson.[RequestPersonID]
						LEFT OUTER JOIN vw_AccessArea ON vw_AccessArea.[AccessAreaID] = AccessRequest.[AccessAreaID]
						LEFT OUTER JOIN CorporateDivision ON CorporateDivision.[DivisionID] = AccessRequest.[DivisionID]
						LEFT OUTER JOIN HRPersonBadge ON HRPersonBadge.[PersonBadgeID] = AccessRequest.[BadgeID]
						LEFT OUTER JOIN LocationBuilding ON LocationBuilding.[BuildingID] = HRPersonBadge.[BuildingID]
					WHERE
						vw_AccessRequestPerson.[RequestMasterID] = @RequestMasterID
						AND
						AccessRequest.[BadgeID] = @BadgeID
				END

				DELETE FROM @testTable WHERE badgeID = @BadgeID;

			END
		END
			--[/MAHMOUD.ALI]

			-- CREATE ALL TASKS EXCEPT PASS REPLACEMENT TASKS:
			--================================================
			
			IF @iRequestTypeID NOT In (2, 3)
			Begin
				INSERT INTO
					Tasks
					(
						[TaskTypeId],
						[RequestId],
						[DivisionId],
						[BusinessAreaId],
						[AccessAreaId],
						[Description],
						[CreatedByID],
						[CreatedDate]
					)
				SELECT
					TaskTypeLookup.[TaskTypeId],
					AccessRequest.[RequestID],
					AccessRequest.[DivisionID],
					vw_AccessRequestPerson.[BusinessAreaID],
					AccessRequest.[AccessAreaID],
					REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TaskTypeLookup.[Description],
					'{1}', vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )'),
					'{2}', IsNull((SELECT Top 1 CountryName FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + ' of ' + (SELECT Top 1 [UBR] FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + '-' + vw_AccessRequestPerson.UBR_Name,'')),
					'{4}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END),
					'{5}', CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END),
					--[MAHMOUD.ALI]
					'{6}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END),
					'{7}', (CASE WHEN LocationBuilding.[Name] IS NOT NULL THEN ' ' + LocationBuilding.[Name] + '/' ELSE '' END) + (CASE WHEN [FloorName] IS NOT NULL THEN ' ' + [FloorName] + '/' ELSE '' END) + (CASE WHEN [AccessAreaName] IS NOT NULL THEN ' ' + [AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN [AccessAreaTypeName] IS NOT NULL THEN ' ' + [AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN [ENDDate] IS NOT NULL THEN ' until ' + Convert(VarChar(11), [ENDDate], 106) ELSE '' END)),
					'{8}', IsNULL('for the ' + LocationPassOffice.Name, '')) As [Description],
					--[/MAHMOUD.ALI]          
					@dbPeopleID,
					@dtNow
				FROM
					vw_AccessRequestPerson 
					INNER JOIN AccessRequest ON AccessRequest.[RequestPersonID] = vw_AccessRequestPerson.[RequestPersonID]
					INNER JOIN AccessRequestType ON AccessRequestType.[RequestTypeID] = AccessRequest.[RequestTypeID]
					INNER JOIN TaskTypeLookup ON TaskTypeLookup.[TaskTypeId] = AccessRequestType.[TaskTypeID]
					LEFT JOIN LocationPassOffice ON AccessRequest.[PassOfficeID] = LocationPassOffice.[PassOfficeID]
					LEFT OUTER JOIN vw_AccessArea ON vw_AccessArea.[AccessAreaID] = AccessRequest.[AccessAreaID]
					LEFT OUTER JOIN CorporateDivision ON CorporateDivision.[DivisionID] = AccessRequest.[DivisionID]
					LEFT OUTER JOIN HRPersonBadge ON HRPersonBadge.[PersonBadgeID] = AccessRequest.[BadgeID]
					LEFT OUTER JOIN LocationBuilding ON LocationBuilding.[BuildingID] = HRPersonBadge.[BuildingID]
				WHERE
					vw_AccessRequestPerson.[RequestMasterID] = @RequestMasterID;
			End
					  
			--=================================================================
			
			-- Assign tasks to users:
			--========================

			IF (@iRequestTypeID in (1, 14))
			BEGIN
				--new pass/replacement pass, or pass deactivation
				INSERT INTO 
					[TasksUsers]
					(
						[TaskId],
						[dbPeopleID],
						[IsCurrent],
						[Order]
					)
				SELECT
					Tasks.TaskId,
					LocationPassOfficeUser.dbPeopleID,
					1,
					1
				FROM
					Tasks
					INNER JOIN AccessRequest ON Tasks.[RequestId] = AccessRequest.[RequestID]
					INNER JOIN AccessRequestPerson ON AccessRequest.[RequestPersonID] = AccessRequestPerson.[RequestPersonID]
					INNER JOIN AccessRequestDeliveryDetail ON AccessRequestPerson.[RequestMasterID] = AccessRequestDeliveryDetail.[RequestMasterID]
					INNER JOIN LocationPassOfficeUser ON AccessRequestDeliveryDetail.[PassOfficeID] = LocationPassOfficeUser.[PassOfficeID]
				WHERE
					AccessRequestPerson.[RequestMasterID] = @RequestMasterID
					AND
					AccessRequest.RequestTypeID = @iRequestTypeID
					AND
					LocationPassOfficeUser.IsEnabled = 1;

			END
            ELSE IF (@iRequestTypeID In (2, 3))
            Begin
				-- Access / Service
				--creating tasks on a per person basis
				Declare @cmd varchar(max)
				Select
						@cmd = Coalesce(@cmd + ' ', '') + 'EXEC dbo.CreateTasks ' + '@iRequestPersonID=' + cast(AccessRequestPerson.RequestPersonID as varchar(30)) + ',' + '@dtNow=' + '''' + CONVERT(varchar,@dtNow,127) + '''' +',' + '@dbPeopleID=' + cast(@dbPeopleID as varchar(20))+ ','+ '@doToday=' + '''' + CONVERT(varchar,@doToday,127)  + ''''
																
				From
					AccessRequestPerson
				Inner Join
					AccessRequestMaster
					On
					AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
				Where
					AccessRequestMaster.RequestMasterID = @RequestMasterID
				Order By
					RequestPersonID;
					
				PRINT @cmd
				EXEC(@cmd)            
            END
            ELSE IF (@iRequestTypeID in (15,16,17,18)) --Revoke, Reactivate and Change start date follow similar logic to offboarding
            BEGIN
				--Offboarding request
				--extra offboarding requests are made for the areas that the person has access to, which have the
				--pass office ID explicitly set - deal with those here, and the rest will get picked up below
				INSERT INTO TasksUsers ([TaskId], [dbPeopleID], [IsCurrent], [Order])
					SELECT t.TaskId, u.dbPeopleID, 1, 1
					FROM Tasks t
						INNER JOIN AccessRequest ar ON ar.RequestID = t.RequestId
						INNER JOIN AccessRequestPerson ON ar.[RequestPersonID] = AccessRequestPerson.[RequestPersonID]
						INNER JOIN LocationPassOfficeUser u on u.PassOfficeID = ar.PassOfficeID
					Where AccessRequestPerson.RequestMasterID = @RequestMasterID
					AND
					u.IsEnabled = 1
            END


            IF (@iRequestTypeID != 2)
            BEGIN
                  -- Now assign to home pass office of request person any remaining tasks which were not assigned
                  -- This will catch any unassigned new pass requests as well
                  INSERT INTO
                        [TasksUsers]
                  (
                        [TaskId],
                        [dbPeopleID],
                        [IsCurrent],
                        [Order]
                  )
                  SELECT
                        Tasks.TaskId,
                        LocationPassOfficeUser.dbPeopleID,
                        1,
                        1
                  FROM
                        Tasks
                  INNER JOIN
                        AccessRequest
                        ON
                        Tasks.RequestId = AccessRequest.RequestID
                  INNER JOIN
                        AccessRequestPerson
                        ON
                        AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
                  INNER JOIN
                        LocationCountry
                        ON
                        AccessRequestPerson.CountryID = LocationCountry.CountryID
                  INNER JOIN
                        LocationPassOfficeUser
                        ON
                        LocationCountry.PassOfficeID = LocationPassOfficeUser.PassOfficeID
                  LEFT OUTER JOIN
                        TasksUsers
                        ON
                        Tasks.TaskId = TasksUsers.TaskId
                  WHERE
                        (AccessRequestPerson.RequestMasterID = @RequestMasterID)
                        AND
                        (TasksUsers.TasksUsersId IS NULL)         -- unassigned (no entry exists)
                        AND
                        (LocationCountry.Enabled = 1)
						AND 
						(LocationPassOfficeUser.IsEnabled = 1);

                  -- Finally, just in CASE any request persons did not have a home pass office
                  -- assign remaining tasks to pass office of location being accessed
                  INSERT INTO
                        [TasksUsers]
                  (
                        [TaskId],
                        [dbPeopleID],
                        [IsCurrent],
                        [Order]
                  )
                  SELECT
                        Tasks.TaskId,
                        LocationPassOfficeUser.dbPeopleID,
                        1,
                        1
                  FROM
                        TasksUsers
                  Right Outer Join
                        vw_AccessArea
                  INNER JOIN
                        LocationPassOfficeUser
                  INNER JOIN
                        LocationCountry
                        ON
                        LocationPassOfficeUser.PassOfficeID = LocationCountry.PassOfficeID
                        ON
                        vw_AccessArea.CountryID = LocationCountry.CountryID
                  INNER JOIN
                        Tasks
                  INNER JOIN
                        AccessRequest
                        ON
                        Tasks.RequestId = AccessRequest.RequestID
                  INNER JOIN
                        AccessRequestPerson
                        ON
                        AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
                        ON
                        vw_AccessArea.AccessAreaID = AccessRequest.AccessAreaID
                        ON
                        TasksUsers.TaskId = Tasks.TaskId
                  WHERE
                        (AccessRequestPerson.RequestMasterID = @RequestMasterID)
                        AND
                        (TasksUsers.TasksUsersId IS NULL)         -- unassigned (no entry exists)
                        AND
                        (LocationCountry.Enabled = 1)
						AND
						(LocationPassOfficeUser.IsEnabled = 1);
            END

            SET @Success = 1;

      END Try
      BEGIN Catch


            -- raise an error with the details of the exception

            DECLARE @iState Int;
            DECLARE @iSeverity Int;
            DECLARE @sMessage NVarChar(4000);

            SELECT
                  @iState = Error_State (),
                  @sMessage = Error_Message (),
                  @iSeverity = Error_Severity ()

            RaisError (@sMessage, @iSeverity, @iState);

      END Catch
END;







GO


