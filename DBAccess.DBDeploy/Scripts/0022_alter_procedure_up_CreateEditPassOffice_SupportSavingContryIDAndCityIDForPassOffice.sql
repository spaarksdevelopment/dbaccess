IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateEditPassOffice]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateEditPassOffice]
END
GO


/****** Object:  StoredProcedure [dbo].[up_CreateEditPassOffice]    Script Date: 5/18/2015 3:47:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/***Create admin procedure for location pass pffice***/
/***Author E.Parker April 2011***/

CREATE PROCEDURE [dbo].[up_CreateEditPassOffice] 
	@PassOfficeID int,
	@Enabled bit,
	@IsNew bit,
	@Name varchar(max),
	@Code varchar(max),
	@Address varchar(max),
	@Email varchar(max),
	@Telephone varchar(max),
	@CountryID int,
	@CityID int,
	@Result INT OUT
AS

SET NOCOUNT ON;

IF @IsNew> 0 
--new row
	insert into dbo.LocationPassOffice (Name,Code,Address,Enabled,Email,Telephone,CountryID,CityID)
	values (@Name,@Code,@Address, @Enabled, @Email, @Telephone, @CountryID, @CityID)
ELSE
	update dbo.LocationPassOffice
		Set 
			Name = @Name,
			Code = @Code,
			Address = @Address,			
			Enabled = @Enabled,
			Email = @Email,
			Telephone = @Telephone,
			CountryID = @CountryID,
			CityID = @CityID
	WHERE PassOfficeID = @PassOfficeID




IF @@ERROR>0
		SELECT @Result = 0
	Else
		SELECT @Result = 1

--GRANT EXECUTE ON [up_CreateEditRegion] TO dbAccessUser

GO

--//@UNDO

/****** Object:  StoredProcedure [dbo].[up_CreateEditPassOffice]    Script Date: 5/18/2015 3:47:21 PM ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateEditPassOffice]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateEditPassOffice]
END
GO

/****** Object:  StoredProcedure [dbo].[up_CreateEditPassOffice]    Script Date: 5/18/2015 3:48:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/***Create admin procedure for location pass pffice***/
/***Author E.Parker April 2011***/

CREATE PROCEDURE [dbo].[up_CreateEditPassOffice] 
	@PassOfficeID int,
	@Enabled bit,
	@IsNew bit,
	@Name varchar(max),
	@Code varchar(max),
	@Address varchar(max),
	@Email varchar(max),
	@Telephone varchar(max),
	@Result INT OUT
AS

SET NOCOUNT ON;

IF @IsNew> 0 
--new row
	insert into dbo.LocationPassOffice (Name,Code,Address,Enabled,Email,Telephone)
	values (@Name,@Code,@Address, @Enabled, @Email, @Telephone)
ELSE
	update dbo.LocationPassOffice
		Set 
			Name = @Name,
			Code = @Code,
			Address = @Address,			
			Enabled = @Enabled,
			Email = @Email,
			Telephone = @Telephone
	WHERE PassOfficeID = @PassOfficeID




IF @@ERROR>0
		SELECT @Result = 0
	Else
		SELECT @Result = 1

--GRANT EXECUTE ON [up_CreateEditRegion] TO dbAccessUser

GO
