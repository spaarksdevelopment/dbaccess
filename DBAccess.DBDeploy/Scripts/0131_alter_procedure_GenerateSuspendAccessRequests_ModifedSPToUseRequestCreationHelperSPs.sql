IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateSuspendAccessRequests]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GenerateSuspendAccessRequests]
END
GO


/****** Object:  StoredProcedure [dbo].[GenerateSuspendAccessRequests]    Script Date: 11/13/2015 3:07:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--==========================================================================
-- History:		13/11/2015	(Wijitha) - Modifed SP to use common helper methods for request creation and email sending							
--==========================================================================

CREATE PROCEDURE [dbo].[GenerateSuspendAccessRequests]
AS
BEGIN 
	DECLARE @TransactionID int
	DECLARE @dbPeopleID INT
	DECLARE @TerminationDate datetime

	DECLARE SuspensionTriggerCursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT TransactionId, hp.dbPeopleId, TerminationDate 
	FROM HRFeedMessage hrm INNER JOIN 
		 HRPerson hp ON CAST(REPLACE(hrm.HRID, 'C', '') as int) = hp.dbPeopleID
	WHERE 
	(hrm.SuspensionProcessed = 0 OR hrm.SuspensionProcessed IS NULL) AND
	hrm.TriggerType = 'SUSP' AND 
	hrm.TriggerAction <> 'DEL' AND 
	ISNULL(hrm.ReactivationFlag, 'N') ='N' AND
	(hp.Suspended IS NULL OR hp.Suspended = 0)

	OPEN SuspensionTriggerCursor
	FETCH NEXT FROM SuspensionTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate

	WHILE @@FETCH_STATUS = 0
	BEGIN
		--Don't create suspension if termination date is greater than 7 days in the past, because offboard should already be created
		IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
		BEGIN
	
			EXEC SuspendAccessForUser @dbPeopleId = @dbPeopleId, @Valid = 1, @CreatedByID  = NULL
		
		END

		UPDATE HRFeedMessage SET SuspensionProcessed = 1 , SuspensionProcessedDate = GETDATE() WHERE TransactionId = @TransactionId

		FETCH NEXT FROM SuspensionTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate
	END
 
	CLOSE SuspensionTriggerCursor
	DEALLOCATE SuspensionTriggerCursor
 END



GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateSuspendAccessRequests]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GenerateSuspendAccessRequests]
END
GO


/****** Object:  StoredProcedure [dbo].[GenerateSuspendAccessRequests]    Script Date: 11/13/2015 2:58:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GenerateSuspendAccessRequests]
AS
BEGIN 
	DECLARE @TransactionID int
	DECLARE @dbPeopleID INT
	DECLARE @TerminationDate datetime

	DECLARE SuspensionTriggerCursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT TransactionId, hp.dbPeopleId, TerminationDate 
	FROM HRFeedMessage hrm INNER JOIN 
		 HRPerson hp ON CAST(REPLACE(hrm.HRID, 'C', '') as int) = hp.dbPeopleID
	WHERE 
	(hrm.SuspensionProcessed = 0 OR hrm.SuspensionProcessed IS NULL) AND
	hrm.TriggerType = 'SUSP' AND 
	hrm.TriggerAction <> 'DEL' AND 
	ISNULL(hrm.ReactivationFlag, 'N') ='N' AND
	(hp.Suspended IS NULL OR hp.Suspended = 0)

	OPEN SuspensionTriggerCursor
	FETCH NEXT FROM SuspensionTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate

	WHILE @@FETCH_STATUS = 0
	BEGIN
	--Don't create suspension if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		EXEC SuspendAccessForUser @dbPeopleId = @dbPeopleId, @Valid = 1, @CreatedByID  = NULL

		--Send emails to pass officers
		EXEC dbo.QueueEmailsForUser @dbPeopleId = @dbPeopleId, @EmailTypeID = 39
	END

	UPDATE HRFeedMessage SET SuspensionProcessed = 1 , SuspensionProcessedDate = GETDATE() WHERE TransactionId = @TransactionId

	FETCH NEXT FROM SuspensionTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate
	END
 
	CLOSE SuspensionTriggerCursor
	DEALLOCATE SuspensionTriggerCursor
 END



GO


