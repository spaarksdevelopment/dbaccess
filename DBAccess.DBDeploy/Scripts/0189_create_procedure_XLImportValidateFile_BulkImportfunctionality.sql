IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateFile]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateFile]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportValidateFile]    Script Date: 1/6/2016 5:54:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--========================================================================
-- Author: Wijitha Wijenayake
-- Created Date: 17/12/2015
-- Description: Validate staged data copied from excel file.
--========================================================================
CREATE PROCEDURE [dbo].[XLImportValidateFile]
@fileID INT
AS
BEGIN

	--First set the Validation Faild Flag to null for all the staged data.
	DECLARE @stagingTable nvarchar(200)
	DECLARE @sql nvarchar(200)

	DECLARE cur_stage_tables CURSOR FOR 
	SELECT StagingTable FROM XLImportEntity

	OPEN cur_stage_tables

	FETCH NEXT FROM cur_stage_tables INTO @stagingTable

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @sql = 'UPDATE ' + @stagingTable + ' SET ValidationFailed = NULL'
		EXEC sp_executesql @sql
		FETCH NEXT FROM cur_stage_tables INTO @stagingTable
	END

	CLOSE cur_stage_tables
	DEALLOCATE cur_stage_tables

	----------------------------------------------------------

	--Delete All the existing Validation Errors
	DELETE FROM XLImportValidationError

	BEGIN TRY
	--Do validations based on DB scheema and business rules
		EXEC XLImportValidateData @fileID
		EXEC XLImportValidateBusinessRules @fileID

	END TRY
	BEGIN CATCH
		--Abosrbe the exception to avoid breaking the execution.
	END CATCH

	DECLARE @hasErrors bit = 0
	SELECT @hasErrors = CAST(COUNT(1) AS BIT) FROM XLImportValidationError WHERE FileID = @fileID and ErrorType = 'Error'

	IF(@hasErrors = 1)
		UPDATE XLImportFile SET ValidationPassed = 0 WHERE ID = @fileID
	ELSE
		UPDATE XLImportFile SET ValidationPassed = 1 WHERE ID = @fileID

	SELECT entity.Entity, entity.SheetName, valErrors.ErrorType, valErrors.ErrorMessage, valErrors.RowNumbers 
	FROM XLImportValidationError valErrors
		INNER JOIN XLImportEntity entity ON valErrors.EntityID = entity.ID
	WHERE valErrors.FileID = @fileID
	ORDER BY entity.SortOrder, valErrors.ErrorType 

END


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateFile]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateFile]
END
GO