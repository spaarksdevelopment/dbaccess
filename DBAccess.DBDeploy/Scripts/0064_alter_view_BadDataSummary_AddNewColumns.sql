/****** Object:  View [dbo].[BadDataSummary]    Script Date: 13/10/2015 12:43:11 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[BadDataSummary]'))
DROP VIEW [dbo].[BadDataSummary]
GO
/****** Object:  View [dbo].[BadDataSummary]    Script Date: 13/10/2015 12:43:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[BadDataSummary]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[BadDataSummary]
AS
SELECT     TOP (100) PERCENT ErrorFlags, COUNT(ErrorFlags) AS [Number of Errors], CASE (errorflags & 1) WHEN 1 THEN ''InvalidHRID'' END AS InvalidHRID, 
                      CASE (errorflags & 2) WHEN 2 THEN ''InvalidFirstName'' END AS InvalidFirstName, CASE (errorflags & 4) WHEN 4 THEN ''InvalidLastName'' END AS InvalidLastName, 
                      CASE (errorflags & 8) WHEN 8 THEN ''InvalidEmail'' END AS [InvalidEmail], CASE (errorflags & 16) WHEN 16 THEN ''InvalidClass'' END AS InvalidClass, 
                      CASE (errorflags & 32) WHEN 32 THEN ''InvalidUBR'' END AS InvalidUBR, CASE (errorflags & 64) WHEN 64 THEN ''InvalidCostCentre'' END AS InvalidCostCentre, 
                      CASE (errorflags & 128) WHEN 128 THEN ''InvalidCostCentreName'' END AS InvalidCostCentreName, CASE (errorflags & 256) WHEN 256 THEN ''Invalid Legal Entity'' END AS InvalidLegalEntity, 
                      CASE (errorflags & 512) WHEN 512 THEN ''InvalidDivPath'' END AS InvalidDivPath, CASE (errorflags & 1024) 
                      WHEN 1024 THEN ''InvalidCityName'' END AS InvalidCityName, CASE (errorflags & 2048) WHEN 2048 THEN ''InvalidCountryName'' END AS InvalidCountryName, 
                      CASE (errorflags & 4096) WHEN 4096 THEN ''InvalidStatus'' END AS InvalidStatus, CASE (errorflags & 8192) 
                      WHEN 8192 THEN ''InvalidOfficerID'' END AS InvalidOfficerID, CASE (errorflags & 16384) WHEN 16384 THEN ''InvalidPreferredFirstName'' END AS InvalidPreferredFirstName, CASE (errorflags & 32768) 
                      WHEN 32768 THEN ''InvalidPreferredLastName'' END AS InvalidPreferredLastName
FROM         dbo.MasterDataImportBadData
GROUP BY ErrorFlags
ORDER BY ErrorFlags' 
GO

--//@UNDO


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[BadDataSummary]'))
DROP VIEW [dbo].[BadDataSummary]
GO
/****** Object:  View [dbo].[BadDataSummary]    Script Date: 13/10/2015 12:43:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[BadDataSummary]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[BadDataSummary]
AS
SELECT     TOP (100) PERCENT ErrorFlags, COUNT(ErrorFlags) AS [Number of Errors], CASE (errorflags & 1) WHEN 1 THEN ''InvalidHRID'' END AS InvalidHRID, 
                      CASE (errorflags & 2) WHEN 2 THEN ''InvalidFirstName'' END AS InvalidFirstName, CASE (errorflags & 4) WHEN 4 THEN ''InvalidLastName'' END AS InvalidLastName, 
                      CASE (errorflags & 8) WHEN 8 THEN ''InvalidEmail'' END AS [InvalidEmail], CASE (errorflags & 16) WHEN 16 THEN ''InvalidClass'' END AS InvalidClass, 
                      CASE (errorflags & 32) WHEN 32 THEN ''InvalidUBR'' END AS InvalidUBR, CASE (errorflags & 64) WHEN 64 THEN ''InvalidCostCentre'' END AS InvalidCostCentre, 
                      CASE (errorflags & 128) WHEN 128 THEN ''InvalidCostCentreName'' END AS InvalidCostCentreName, CASE (errorflags & 256) WHEN 256 THEN ''Invalid Legal Entity'' END AS InvalidLegalEntity, 
                      CASE (errorflags & 512) WHEN 512 THEN ''InvalidDivPath'' END AS InvalidDivPath, CASE (errorflags & 1024) 
                      WHEN 1024 THEN ''InvalidCityName'' END AS InvalidCityName, CASE (errorflags & 2048) WHEN 2048 THEN ''InvalidCountryName'' END AS InvalidCountryName, 
                      CASE (errorflags & 4096) WHEN 4096 THEN ''InvalidStatus'' END AS InvalidStatus, CASE (errorflags & 8192) 
                      WHEN 8192 THEN ''InvalidOfficerID'' END AS InvalidOfficerID
FROM         dbo.MasterDataImportBadData
GROUP BY ErrorFlags
ORDER BY ErrorFlags' 
GO