/****** Object:  StoredProcedure [dbo].[spr_ImportMasterDataFile]    Script Date: 20/04/2015 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spr_ImportMasterDataFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--	Logic to do the data import
ALTER PROCEDURE [dbo].[spr_ImportMasterDataFile]
AS

	--	WE COULD TRANSACTION THIS BUT WE ARE BETTER FAILING THEN IT WILL GET NOTICED
	TRUNCATE TABLE MasterData;

	INSERT INTO MasterData
	SELECT [HR_ID]
		  ,[FIRST_NAME]
		  ,[MIDDLE_NAME]
		  ,[LAST_NAME]
		  ,[PREFERRED_NAME]
		  ,[EMPLOYEE_EMAIL_ID]
		  ,[EMPLOYEE_CLASS]
		  ,[EMPLOYEE_STATUS]
		  ,[ORGANIZATIONAL_RELATIONSHIP]
		  ,[CORPORATE_TITLE]
		  ,[POSITION_NUMBER]
		  ,[JOB_CODE]
		  ,[TELEPHONE_NO]
		  ,[LOCATION_ID]
		  ,[LOCATION_COUNTRY]
		  ,[LOCATION_CITY]
		  ,[COST_CENTER]
		  ,[COST_CENTER_DESCR]
		  ,[LEGAL_ENTITY]
		  ,[GR_DIVISION_CODE]
		  ,[UBR_PRODUCT_CODE]
		  ,[UBR_PRODUCT_DESCR]
		  ,[UBR_SUB_PRODUCT_CODE]
		  ,[UBR_SUB_PRODUCT_DESCR]
		  ,[EFFECTIVE_DATE_JOB_START]
		  ,[EFFECTIVE_DATE_JOB_END]
		  ,[CREATE ACCESS DATE TIMESTAMP]
		  ,[REVOKE_ACCESS_DATE_TIMESTAMP]
		  ,[MANAGER_HR_ID]
		  ,[MANAGER_EMAIL_ADDRESS]
		  ,[BADGE_ID]
		  ,[HR_ID] AS [dbPeopleId],
		  dbo.[ComputeUBRPath]([GR_DIVISION_CODE])
		  FROM MasterDataImport


	-- Update HRPerson
	UPDATE HRPerson
	SET
		HRPerson.Forename = MasterData.FIRST_NAME,
		HRPerson.Surname = REPLACE(MasterData.Last_Name, ''(E)'', ''''),
		HRPerson.DBDirID = 0,
		HRPerson.EmailAddress = MasterData.EMPLOYEE_EMAIL_ID,
		HRPerson.Telephone = MasterData.TELEPHONE_NO,
		HRPerson.Class = dbo.GetEmployeeClass([ORGANIZATIONAL_RELATIONSHIP],[EMPLOYEE_CLASS]),
		HRPerson.UBR = MasterData.GR_DIVISION_CODE,
		HRPerson.CostCentre = MasterData.COST_CENTER,
		HRPerson.CostCentreName = MasterData.COST_CENTER_DESCR,
		HRPerson.LegalEntityID = MasterData.LEGAL_ENTITY,
		HRPerson.LegalEntity = NULL,
		HRPerson.DivPath = dbo.ComputeUBRPath(MasterData.GR_DIVISION_CODE),
		HRPerson.ManagerName = NULL,
		HRPerson.CityName = MasterData.LOCATION_CITY,
		HRPerson.CountryName = ISO_Countries.Countryorareaname,
		HRPerson.Status = MasterData.EMPLOYEE_STATUS,
		HRPerson.VendorID = 0,
		HRPerson.VendorName = NULL,
		HRPerson.IsExternal = [dbo].[IsEmployeeExternal]([ORGANIZATIONAL_RELATIONSHIP]),
		HRPerson.Enabled = 1,
		HRPerson.OfficerId = MasterData.CORPORATE_TITLE
	FROM HRPerson
	JOIN MasterData ON HRPerson.dbPeopleID = MasterData.dbPeopleID
	JOIN ISO_Countries ON MasterData.LOCATION_COUNTRY = ISO_Countries.ISOALPHA3code
	
	-- Insert any missing HRPerson
	INSERT INTO HRPerson (
		dbPeopleID, Forename, Surname, DBDirID, EmailAddress, Telephone, Class, UBR, CostCentre, 
		CostCentreName, LegalEntityID, LegalEntity, DivPath, ManagerName, CityName, CountryName, 
		Status, VendorID, VendorName, IsExternal, Enabled, OfficerId, Revoked)
	SELECT 
	[HR_ID] [dbPeopleID],
	[FIRST_NAME] [Forename],
	[LAST_NAME] [Surname],
	0 [DBDirID],
	[EMPLOYEE_EMAIL_ID] [EmailAddress],
	[TELEPHONE_NO] [Telephone],
	dbo.GetEmployeeClass([ORGANIZATIONAL_RELATIONSHIP],
	[EMPLOYEE_CLASS]) [Class],
	[GR_DIVISION_CODE] [UBR],
	[COST_CENTER] [CostCentre], 
	[COST_CENTER_DESCR] [CostCentreName],
	[LEGAL_ENTITY] [LegalEntityID],
	NULL [LegalEntity],
	dbo.ComputeUBRPath([GR_DIVISION_CODE]) [DivPath],
	NULL [ManagerName],
	[LOCATION_CITY] [CityName],
	ISO_Countries.Countryorareaname [CountryName],
	[EMPLOYEE_STATUS] [Status],
	0 [VendorID],
	NULL [VendorName],
	[dbo].[IsEmployeeExternal]([ORGANIZATIONAL_RELATIONSHIP]) [IsExternal],
	1 [Enabled],
	MasterData.CORPORATE_TITLE [OfficerId],
	NULL [Revoked]
	FROM MasterData JOIN ISO_Countries ON MasterData.LOCATION_COUNTRY = ISO_Countries.ISOALPHA3code
	WHERE
		dbPeopleId NOT IN (SELECT dbPeopleId FROM HRPerson)

	
	-- Update mp_User
	UPDATE MP_User
	SET
		MP_User.EmailAddress = MasterData.EMPLOYEE_EMAIL_ID,
		MP_User.Forename = MasterData.FIRST_NAME,
		MP_User.Surname = REPLACE(MasterData.Last_Name, ''(E)'', ''''),
		MP_User.Telephone = MasterData.TELEPHONE_NO,
		MP_User.CostCentre = MasterData.COST_CENTER,
		MP_User.UBR = MasterData.GR_DIVISION_CODE,
		MP_User.Enabled = 1,
		MP_User.VendorID = 0
	FROM MP_User
	INNER JOIN MasterData ON MP_User.Id = MasterData.dbPeopleID
	RIGHT JOIN ISO_Countries ON MasterData.LOCATION_COUNTRY = ISO_Countries.ISOALPHA3code
	WHERE
		MasterData.EMPLOYEE_STATUS = ''A''


	-- Insert any missing mp_User
	INSERT INTO mp_User (
		Id, EmailAddress, Forename, Surname, Telephone, CostCentre, UBR, Enabled, VendorID,
		CreatedDateTime, CreatedByUserId, VisitCount, dbPeopleID, LanguageID)
	SELECT 
		[HR_ID],[EMPLOYEE_EMAIL_ID],[FIRST_NAME],[LAST_NAME],[TELEPHONE_NO],[COST_CENTER],[GR_DIVISION_CODE],1,0,
		GetDate(), NULL, 0, [HR_ID], NULL
	FROM MasterData
	WHERE
		dbPeopleId NOT IN (SELECT dbPeopleId FROM mp_User) AND
		MasterData.EMPLOYEE_STATUS = ''A'' AND [LOCATION_COUNTRY]<>''DNK'' AND RTRIM(LTRIM([EMPLOYEE_EMAIL_ID])) <> '''' AND
		dbPeopleID IN (SELECT dbPeopleID FROM HRPerson)
		
	
	--Assign default role to any new users
	INSERT INTO mp_SecurityGroupUser([SecurityGroupId],[dbPeopleID])
	SELECT 1, mp_User.dbPeopleID 
	FROM mp_User LEFT JOIN mp_SecurityGroupUser ON mp_User.dbPeopleID=mp_SecurityGroupUser.dbPeopleID
	WHERE mp_SecurityGroupUser.Id is null
			
' 
END
GO

--//@UNDO
ALTER PROCEDURE [dbo].[spr_ImportMasterDataFile]
AS
 
--     WE COULD TRANSACTION THIS BUT WE ARE BETTER FAILING THEN IT WILL GET NOTICED
TRUNCATE TABLE MasterData;
 
INSERT INTO MasterData
SELECT [HR_ID]
      ,[FIRST_NAME]
      ,[MIDDLE_NAME]
      ,[LAST_NAME]
      ,[PREFERRED_NAME]
      ,[EMPLOYEE_EMAIL_ID]
      ,[EMPLOYEE_CLASS]
      ,[EMPLOYEE_STATUS]
      ,[ORGANIZATIONAL_RELATIONSHIP]
      ,[CORPORATE_TITLE]
      ,[POSITION_NUMBER]
      ,[JOB_CODE]
      ,[TELEPHONE_NO]
      ,[LOCATION_ID]
      ,[LOCATION_COUNTRY]
      ,[LOCATION_CITY]
      ,[COST_CENTER]
      ,[COST_CENTER_DESCR]
      ,[LEGAL_ENTITY]
      ,[GR_DIVISION_CODE]
      ,[UBR_PRODUCT_CODE]
      ,[UBR_PRODUCT_DESCR]
      ,[UBR_SUB_PRODUCT_CODE]
      ,[UBR_SUB_PRODUCT_DESCR]
      ,[EFFECTIVE_DATE_JOB_START]
      ,[EFFECTIVE_DATE_JOB_END]
      ,[CREATE ACCESS DATE TIMESTAMP]
         ,[REVOKE_ACCESS_DATE_TIMESTAMP]
      ,[MANAGER_HR_ID]
      ,[MANAGER_EMAIL_ADDRESS]
      ,[BADGE_ID]
      ,REPLACE([HR_ID], 'C', '') AS [dbPeopleId],
      dbo.[ComputeUBRPath]([GR_DIVISION_CODE])
      FROM MasterDataImport
 
 
GO