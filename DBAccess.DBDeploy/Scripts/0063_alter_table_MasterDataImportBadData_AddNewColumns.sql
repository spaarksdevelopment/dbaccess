ALTER TABLE MasterDataImportBadData 
ADD PREFERRED_FIRST_NAME nvarchar(60)
GO 

ALTER TABLE MasterDataImportBadData 
ADD PREFERRED_LAST_NAME nvarchar(60)
GO

ALTER TABLE MasterDataImportBadData
ADD AMR_ID nvarchar(15)
GO

ALTER TABLE MasterDataImportBadData 
ADD EXTERNAL_PROVIDER nvarchar(15)
GO

ALTER TABLE MasterDataImportBadData 
ADD BUILDING nvarchar(15)
GO

ALTER TABLE MasterDataImportBadData 
ADD [FLOOR] nvarchar(15)
GO

ALTER TABLE MasterDataImportBadData 
ADD PHYSICAL_ACCESS_ONLY nvarchar(1)
GO

ALTER TABLE MasterDataImportBadData 
ADD [DB_SITE_INDICATOR] nvarchar(1)
GO

--//@UNDO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN PREFERRED_FIRST_NAME
GO 

ALTER TABLE MasterDataImportBadData 
DROP COLUMN PREFERRED_LAST_NAME
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN BUILDING
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN EXTERNAL_PROVIDER
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN  AMR_ID 
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN [FLOOR]
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN PHYSICAL_ACCESS_ONLY
GO

ALTER TABLE MasterDataImportBadData 
DROP COLUMN [DB_SITE_INDICATOR]
GO