
UPDATE TaskTypeLookup SET Description = 'Create a {4} pass ({9}) for {1}{5}'
WHERE TaskTypeId=1

GO

--//@UNDO

UPDATE TaskTypeLookup SET Description = 'Create a {4} pass for {1}{5}'
WHERE TaskTypeId=1

GO