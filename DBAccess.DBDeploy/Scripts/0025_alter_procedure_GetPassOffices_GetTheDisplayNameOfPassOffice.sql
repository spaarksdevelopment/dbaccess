IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPassOffices]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GetPassOffices]
END
GO


/****** Object:  StoredProcedure [dbo].[GetPassOffices]    Script Date: 5/18/2015 2:41:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetPassOffices]
 @LanguageID int
As

SELECT PassOffices.*, PassOffices.Country + ' (' + PassOffices.Name + ')' as DisplayName 
FROM (SELECT po.[PassOfficeID]
      ,Case @LanguageID
			when 1 then po.[Name]
			when 2 then ISNULL(po.[GermanName],po.[Name])
			when 3 then ISNULL(po.[ItalyName],po.[Name])
		End as [Name]
		,po.[Code]
		,po.[Address]
		,po.[Enabled]
		,po.[URL]
		,po.[Email]
		,po.[Telephone]
		,po.[CityID]
		,po.[CountryID]
		,c.Name as Country	  
	  FROM [dbo].[LocationPassOffice] po
		INNER JOIN [dbo].[LocationCountry] c ON po.CountryID = c.CountryID		
	  WHERE
		po.[Enabled] = 1
		AND c.[Enabled] = 1		
) AS PassOffices

ORDER BY Country, Name

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPassOffices]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GetPassOffices]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetPassOffices]
 @LanguageID int
As
SELECT [PassOfficeID]
      ,Case @LanguageID
			when 1 then [Name]
			when 2 then ISNULL([GermanName],[Name])
			when 3 then ISNULL([ItalyName],[Name])
		End as [Name]
      ,[Code]
      ,[Address]
      ,[Enabled]
      ,[URL]
      ,[Email]
      ,[Telephone]
      ,[CityID]

  FROM [dbo].[LocationPassOffice]
  where
		[Enabled] = 1
GO
