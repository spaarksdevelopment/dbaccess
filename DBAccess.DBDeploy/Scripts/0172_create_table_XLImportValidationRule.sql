IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportValidationRule')
BEGIN


CREATE TABLE [dbo].[XLImportValidationRule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[ValidationType] [nvarchar](20) NOT NULL,
	[BusinessRule] [nvarchar](max) NOT NULL,
	[Script] [nvarchar](max) NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_XLImportValidationRule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[XLImportValidationRule]  WITH CHECK ADD  CONSTRAINT [FK_XLImportValidationRule_XLImportFile] FOREIGN KEY([EntityID])
REFERENCES [dbo].[XLImportEntity] ([ID])

ALTER TABLE [dbo].[XLImportValidationRule] CHECK CONSTRAINT [FK_XLImportValidationRule_XLImportFile]


END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportValidationRule')
BEGIN
	
	DROP TABLE [dbo].[XLImportValidationRule]
	
END

GO