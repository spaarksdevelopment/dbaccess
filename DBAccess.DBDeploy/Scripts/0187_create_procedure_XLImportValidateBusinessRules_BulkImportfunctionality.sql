IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateBusinessRules]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateBusinessRules]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportValidateBusinessRules]    Script Date: 1/6/2016 5:48:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--========================================================================
-- Author: Wijitha Wijenayake
-- Created Date: 17/12/2015
-- Description: Validate staged data copied from excel, against the business rules
--========================================================================
CREATE PROCEDURE [dbo].[XLImportValidateBusinessRules]
@fileID int
AS
BEGIN

	DECLARE @tblInValidRecords TABLE
	(	
		ID int,
		RowNumber int
	)

	DECLARE @entityID nvarchar(500)
	DECLARE @businessRule nvarchar(500)
	DECLARE @script nvarchar(max)
	DECLARE @errorType nvarchar(20)
	DECLARE @rowNumbers nvarchar(max)
	DECLARE @rowIDs nvarchar(max)
	DECLARE @updateSql nvarchar(max)
	DECLARE @stageTable nvarchar(200)

	DECLARE cur_business_rule CURSOR FOR
	SELECT entity.ID, valRule.ValidationType, valRule.BusinessRule, valRule.Script, entity.StagingTable 
	FROM XLImportValidationRule valRule
		INNER JOIN XLImportEntity entity on entity.ID = valRule.EntityID
	WHERE [Enabled] = 1
	ORDER BY entity.ID

	OPEN cur_business_rule

	FETCH NEXT FROM cur_business_rule INTO @entityID, @errorType, @businessRule, @script, @stageTable

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		BEGIN TRY

		DELETE FROM @tblInValidRecords
		SET @rowNumbers = ''
		SET @rowIDs = ''

		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @script

		SELECT @rowNumbers = @rowNumbers + cast(RowNumber as varchar(10)) + ', ', @rowIDs = @rowIDs + cast(ID as varchar(10)) + ', ' FROM @tblInValidRecords
		SET @rowNumbers = LEFT(@rowNumbers, NULLIF(LEN(@rowNumbers)-1, -1))
		SET @rowIDs = LEFT(@rowIDs, NULLIF(LEN(@rowIDs)-1, -1))

		IF(len(@rowIDs) > 0)
		BEGIN
			INSERT INTO XLImportValidationError
			SELECT @fileID, @entityID, @errorType, @businessRule, @rowNumbers
		END

		SET @updateSql = 'UPDATE ' + @stageTable + ' SET ValidationFailed = 1 WHERE ID IN (' + @rowIDs + ')'
		EXEC sp_executesql @updateSql
		
		END TRY
		BEGIN CATCH
			--Absorbe the error to avoid breaking the execution when exception occur. So that It goes through all the validation rules
		END CATCH

	FETCH NEXT FROM cur_business_rule INTO @entityID, @errorType, @businessRule, @script, @stageTable
	END

	CLOSE cur_business_rule
	DEALLOCATE cur_business_rule

END
GO




--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateBusinessRules]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateBusinessRules]
END
GO