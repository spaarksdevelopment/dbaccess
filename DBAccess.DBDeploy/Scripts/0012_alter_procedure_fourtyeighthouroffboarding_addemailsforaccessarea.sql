/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 21/04/2015 17:14:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FourtyEightHourOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


ALTER Procedure [dbo].[FourtyEightHourOffBoarding] 
AS 
Begin 

SET NOCOUNT ON

--catherine steele
Declare @CreatedByID int = null

--added BH 18 Feb
declare @OfficerID varchar(2)
declare @dbPeopleID int
declare @messageId int


IF Exists
(
	SELECT md.dbPeopleId from HRFeedMessage hrm
		   INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
		   INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
    WHERE 
	hrm.Processed = 0 AND 
	hrm.TriggerType = ''LVER'' AND hrm.TriggerAction <> ''DEL''
		   AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
)
Begin
	
	 DECLARE @HRUpdates TABLE (RowNumber int, dbPeopleID int, messageId int) 
        Insert Into @HRUpdates 
		SELECT 
				ROW_NUMBER() OVER (ORDER BY md.dbPeopleID) AS RowNumber, 
				md.dbPeopleID,
				hrm.TransactionId
		FROM HRFeedMessage hrm
				INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
				INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
		WHERE hrm.Processed = 0 AND 
		hrm.TriggerType = ''LVER'' AND hrm.TriggerAction <> ''DEL''
		AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
	

	DECLARE @RC int
	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)

	Declare @NumberofRows int
	Select @NumberofRows = Count(*) From @HRUpdates
	
	DECLARE @intFlag INT
	SET @intFlag = 1
	
	
	
	--commented out BH 18 Feb
	--Declare @dbPeopleID int
	
	WHILE (@intFlag <= @NumberOfRows)
	Begin
		Select @dbPeopleID = dbPeopleID, @messageId = messageId From @HRUpdates where RowNumber = @intFlag
		
		--added BH 18 Feb
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleID

		--Should already be revoked ie can''t login or be the subject of any requests but just in case
		UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
		UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

		EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID
		
		--Note the message queue ID and date message received. This is required for 48HR auditing
		UPDATE HRFeedMessage
		SET Processed = 1, ProcessedDate = GETDATE()
		WHERE TransactionId = @messageId

		SET @intFlag = @intFlag + 1

		-----------------------------
		---  Offboarded people should be disabled as approvers
		Update CorporateDivisionAccessAreaApprover
		set Enabled=0
		where dbPeopleID=@dbPeopleID
		--------------------

		--    Get the users passoffice email address          
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person''s country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleID
		
		
		--if/else added BH 18 Feb
		if (@OfficerID=''D'' or @OfficerID=''MD'')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, hr2.EmailAddress AS PassOfficeUserEmail, hr2.Forename + '' '' + hr2.Surname AS PassOfficeUserName
				FROM 
				LocationPassOffice INNER JOIN
				LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1

		end
		else
		begin
		INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, HRPerson.EmailAddress AS PassOfficeUserEmail, HRPerson.Forename + '' '' + HRPerson.Surname AS PassOfficeUserName
			 FROM  
			 LocationCountry INNER JOIN 
			 LocationPassOffice ON LocationCountry.PassOfficeID = LocationPassOffice . PassOfficeID 
			 JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
			 JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE
			 LocationPassOfficeUser.IsEnabled = 1 AND LocationPassOffice.Enabled = 1 AND
		      (
				LocationCountry.CountryID=@UserCountryID 
				OR LocationCountry.CountryID IN
				(
				    select distinct c.CountryID from HRPersonAccessArea p
				    inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
				    inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
				    inner join LocationCity lc on lc.CityID = lb.CityID
				    inner join LocationCountry c on c.CountryID = lc.CountryID
				    where (p.EndDate is null or p.EndDate > getdate())
					    and c.CountryID <> @UserCountryID
					    and p.dbPeopleId = @dbPeopleId
				)
			 )
		end


        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @messageId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]=''HttpRoot''

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 36


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '''' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , ''[$$UserEmail$$]'' , @EmailForename + '' '' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , ''[$$HTTP_ROOT$$]'' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , ''[$$NAME$$]'' , @ToName ); 
					SET @Body = REPLACE ( @Body , ''[$$FILE_PATH$$]'' , ''Pages/MyTasks.aspx'' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, ''dbAccess_DO_NOT_REPLY@db.com'' 
							, ''ryan.sheehan@db.com'' 
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 36 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END			
END
END



' 
END
GO


--//@UNDO
/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 21/04/2015 17:15:57 ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
 
 
 
ALTER Procedure [dbo].[FourtyEightHourOffBoarding]
AS
Begin
 
SET NOCOUNT ON
 
--catherine steele
Declare @CreatedByID int = null
 
--added BH 18 Feb
declare @OfficerID varchar(2)
declare @dbPeopleID int
declare @messageId int
 
 
IF Exists
(
       SELECT md.dbPeopleId from HRFeedMessage hrm
                 INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
                 INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
    WHERE
       hrm.Processed = 0 AND
       hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
                 AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
)
Begin
      
       DECLARE @HRUpdates TABLE (RowNumber int, dbPeopleID int, messageId int)
        Insert Into @HRUpdates
              SELECT
                           ROW_NUMBER() OVER (ORDER BY md.dbPeopleID) AS RowNumber,
                           md.dbPeopleID,
                           hrm.TransactionId
              FROM HRFeedMessage hrm
                           INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
                           INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
              WHERE hrm.Processed = 0 AND
              hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
              AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
      
 
       DECLARE @RC int
       DECLARE @To nvarchar(255)
       DECLARE @ToName nvarchar(255)
       DECLARE @Subject nvarchar(255)
       DECLARE @Body nvarchar(1000)
       DECLARE @Guid nvarchar(255)
       DECLARE @EmailForename nvarchar(255)
       DECLARE @EmailSurname nvarchar(255)
       DECLARE @HttpRoot nvarchar(100)
 
       Declare @NumberofRows int
       Select @NumberofRows = Count(*) From @HRUpdates
      
       DECLARE @intFlag INT
       SET @intFlag = 1
      
      
      
       --commented out BH 18 Feb
       --Declare @dbPeopleID int
      
       WHILE (@intFlag <= @NumberOfRows)
       Begin
              Select @dbPeopleID = dbPeopleID, @messageId = messageId From @HRUpdates where RowNumber = @intFlag
             
              --added BH 18 Feb
              select @OfficerID = OfficerID from HRPerson
              where HRPerson.dbPeopleID= @dbPeopleID
 
              --Should already be revoked ie can't login or be the subject of any requests but just in case
              UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
              UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
 
              EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID
             
              --Note the message queue ID and date message received. This is required for 48HR auditing
              UPDATE HRFeedMessage
              SET Processed = 1, ProcessedDate = GETDATE()
              WHERE TransactionId = @messageId
 
              SET @intFlag = @intFlag + 1
 
              -----------------------------
              ---  Offboarded people should be disabled as approvers
              Update CorporateDivisionAccessAreaApprover
              set Enabled=0
              where dbPeopleID=@dbPeopleID
              --------------------
 
              --    Get the users passoffice email address         
              --TABLE TO STORE Recipients
              DECLARE @PassOfficeMembers TABLE
              (
                     RowNumber int,
                     PassOfficeUserEmail nvarchar(200),
                     PassOfficeUserName nvarchar(200)
              )
 
              DELETE FROM @PassOfficeMembers
 
              DECLARE @UserCountryID int
              SET @UserCountryID = 59 --Default to UK, in case person's country cannot be found
 
              SELECT @UserCountryID=CountryID
              FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName
              WHERE dbPeopleID=@dbPeopleID
             
             
              --if/else added BH 18 Feb
              if (@OfficerID='D' or @OfficerID='MD')
              begin
              INSERT INTO @PassOfficeMembers
                           SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, hr2.EmailAddress AS PassOfficeUserEmail, hr2.Forename + ' ' + hr2.Surname AS PassOfficeUserName
                           FROM
                           LocationPassOffice INNER JOIN
                           LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
                           JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
                           where LocationPassOffice.Enabled = 1
                           AND LocationPassOfficeUser.IsEnabled = 1
 
              end
              else
              begin
              INSERT INTO @PassOfficeMembers
                           SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, HRPerson.EmailAddress AS PassOfficeUserEmail, HRPerson.Forename + ' ' + HRPerson.Surname AS PassOfficeUserName
                           FROM 
                           LocationCountry INNER JOIN
                           LocationPassOffice ON LocationCountry.PassOfficeID = LocationPassOffice . PassOfficeID
                           JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
                           JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
                           WHERE LocationCountry.CountryID=@UserCountryID
                           AND LocationPassOfficeUser.IsEnabled = 1
                           --AND LocationCountry.CountryID NOT IN (58,59)        
              end
 
 
        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @messageId
 
              SELECT @HttpRoot=Value
              FROM AdminValues
              WHERE [Key]='HttpRoot'
 
              --Loop through each member of the pass office
              Declare @NumberofPassOfficePeople int
              Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
              DECLARE @loopCounter INT
              SET @loopCounter = 1
 
              -- Table to allow us to not send duplicate emails
              DECLARE @EmailSentTo TABLE
              (
                     PassOfficeUserEmail nvarchar(200)
              )
 
              DELETE FROM @EmailSentTo
 
              WHILE (@loopCounter <= @NumberofPassOfficePeople)
              BEGIN
                     SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 36
 
 
                     SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
                     FROM @PassOfficeMembers
                     WHERE RowNumber = @loopCounter
                    
                     DECLARE @BatchGuid uniqueidentifier
                     SET @BatchGuid = NEWID()
      
                     IF (@To <> '' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
                     BEGIN
                                  SET @Body = REPLACE ( @Body , '[$$UserEmail$$]' , @EmailForename + ' ' + @EmailSurname );
                                  SET @Body = REPLACE ( @Body , '[$$HTTP_ROOT$$]' , @HttpRoot );
                                  SET @Body = REPLACE ( @Body , '[$$NAME$$]' , @ToName );
                                  SET @Body = REPLACE ( @Body , '[$$FILE_PATH$$]' , 'Pages/MyTasks.aspx' );
 
                                  EXEC [up_InsertEmailLog]
                                                @To
                                                , 'dbAccess_DO_NOT_REPLY@db.com'
                                                , 'ryan.sheehan@db.com'
                                                , @Subject
                                                , @Body
                                                , 0
                                                , @BatchGuid
                                                , 36
                                                , NULL
                     END
 
                     INSERT INTO @EmailSentTo SELECT @To                   
 
                     SET @loopCounter = @loopCounter + 1
              END
       END                 
END
END