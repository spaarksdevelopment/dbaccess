IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_GetTaskDetails]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_GetTaskDetails]
END
GO

/****** Object:  StoredProcedure [dbo].[up_GetTaskDetails]    Script Date: 2/28/2017 6:54:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- History: 
--			28/02/2017 (Wijitha) Modified SP to get the request details for Smart Card requests
-- =============================================

CREATE PROCEDURE [dbo].[up_GetTaskDetails] 
(
	@TaskID Int
)
As
Begin

	Set NoCount On;

	Declare @TaskTypeId Int;
	Set @TaskTypeId =
	(
		Select
			TaskTypeId
		From
			Tasks
		Where
			TaskId = @TaskID
	);

	If @TaskTypeId in (1, 12, 13, 14, 15, 16, 17)
	
	Begin

		Select
			t.TaskId,
			t.TaskTypeId,
			p.Forename + ' ' + p.Surname AS [Name],
			p.EmailAddress,
			p.dbPeopleID,
			p.DBDirID,
			IsNull(p.Telephone,''),
			IsNull(arm.Comment,''),
			Convert(varchar,ar.StartDate, 106) AS StartDate,
			Convert(varchar,ar.EndDate, 106) AS EndDate,
			IsNull(adel.ContactEmail,'') AS ContactEmail,
			IsNull(adel.ContactTelephone,'') AS ContactTelephone,
			arm.RequestMasterID ,
			COALESCE(bjr.Name, '') Justification,
			(Select EmailAddress From HRPerson where dbPeopleID = ar.CreatedByID) As RequestedBy,
			CASE WHEN ar.RequestTypeID = 15 THEN IsNUll([dbo].[GetAccessAreasHavingLesserApproversDueToOffboard](arp.[dbPeopleId]), '') 
			ELSE '' END AS LesserApprovalWarning
		From
			Tasks t
		Inner Join
			vw_AccessRequest ar
			On
			t.RequestId = ar.RequestID
		Inner Join
			HRPerson p
			On
			ar.dbPeopleID = p.dbPeopleID
		Inner Join
			AccessRequestPerson arp
			On
			arp.RequestPersonID = ar.RequestPersonID
		Inner Join
			AccessRequestMaster arm
			On
			arm.RequestMasterID = arp.RequestMasterID
		LEFT JOIN 
			[dbo].[AccessRequestDeliveryDetail] adel
			ON arm.RequestMasterID = adel.RequestMasterID
		LEFT JOIN dbo.BadgeJustificationReason bjr ON ar.BadgeJustificationReasonID = bjr.ID
		Where
			t.[TaskId] = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 2		-- Approve Person
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
            ,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID,
			COALESCE(ajr.Name, '') Justification

		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		LEFT JOIN dbo.AccessRequestJustificationReason ajr ON AccessRequest.AccessRequestJustificationReasonID = ajr.ID	
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 3		-- Approve Access
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
			,IsNull (RTrim (Category.[description]) + ' (' + Cast (Category.id As VarChar) + ')', '') As Category
			,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID,
			COALESCE(ajr.Name, '') Justification 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		Left Outer Join
			AccessArea
			On
			AccessArea.AccessAreaID = AccessRequest.AccessAreaID
		Left Outer Join
			Category
			On
			Category.id = AccessArea.AccessControlID
		LEFT JOIN dbo.AccessRequestJustificationReason ajr ON AccessRequest.AccessRequestJustificationReasonID = ajr.ID	
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 7		-- Manual Add Access
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,HRPerson.dbPeopleID
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
			,IsNull (RTrim (Category.[description]) + ' (' + Cast (Category.id As VarChar) + ')', '') As Category
			,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		Left Outer Join
			AccessArea
			On
			AccessArea.AccessAreaID = AccessRequest.AccessAreaID
		Left Outer Join
			Category
			On
			Category.id = AccessArea.AccessControlID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 4 -- This is Business Area Approver request
	Begin

		Select
			t.TaskId,
			t.TaskTypeId,
			ba.[Name] AS [BusinessAreaName],
			d.[Name] AS [DivisionName],
			u.Id,
			u.[Forename] + ' ' + u.[Surname] AS [DivisionAdmin],
			'' RequestMasterID 
		From
			Tasks t
		Inner Join
			CorporateBusinessArea ba
			On
			t.BusinessAreaId = ba.BusinessAreaID
		Inner Join
			CorporateDivision d
			On
			ba.DivisionID = d.DivisionID
		Inner Join
			tasksUsers cu
			On
			cu.taskId = t.taskID
		Inner Join
			mp_User u
			On
			u.dbPeopleID = cu.dbPeopleID
		Where
			t.TaskId = @TaskId
			--AND cu.mp_SecurityGroupID = 3

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId=5 -- Access Area Approver Request
	Begin
		Select
			t.TaskId,
			--t.[Description],
			--IsNull(vmt.[Descript],'') AS [Description],
			t.TaskTypeId,
			p.Forename + ' ' + p.Surname AS [Name],
			p.EmailAddress,
			IsNull(p.Telephone,''),
			IsNull(arm.Comment,''),
			arm.RequestMasterID 
			From [Tasks] t 
			Join vw_AccessRequest ar On t.RequestId = ar.RequestID
			Join HRPerson p On ar.dbPeopleID = p.dbPeopleID
			Join AccessRequestPerson arp On arp.RequestPersonID = ar.RequestPersonID
			Join AccessRequestMaster arm On arm.RequestMasterID = arp.RequestMasterID
			Where t.[TaskId] = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 6		-- AR Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (CorporateDivision.Name + ' (' + CorporateDivision.UBR + ')', '') As DivisionName,
			IsNull (CorporateBusinessArea.Name + ' (' + CorporateBusinessArea.UBR + ')', '') As BusinessAreaName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		
		-- these should exist, but may not...

		Left Outer Join
			CorporateDivision
			On
			CorporateDivision.DivisionID = Tasks.DivisionId
		Left Outer Join
			CorporateBusinessArea
			On
			CorporateBusinessArea.BusinessAreaID = Tasks.BusinessAreaId
		Left Outer Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If (@TaskTypeId In (8, 10))		-- DA/DO Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (CorporateDivision.Name + ' (' + CorporateDivision.UBR + ')', '') As DivisionName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			CorporateDivision
			On
			CorporateDivision.DivisionID = Tasks.DivisionId
		Inner Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 9		-- PO Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (LocationPassOffice.Name, '') As PassOfficeName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			LocationPassOffice
			On
			LocationPassOffice.PassOfficeID = Tasks.PassOfficeId
		Inner Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');
	END
	Else If @TaskTypeId = 11		-- MSP Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (HRPerson_PO.Forename + ' ' + HRPerson_PO.Surname, '') As POName,
			IsNull (HRPerson_PO.EmailAddress, '') As POEmailAddress,
			IsNull (HRPerson_PO.Telephone, '') As POTelephoneNumber,
			IsNull(HRVendor.VendorName,'') As MSPResourceName,
			IsNull(LC.Name,'') as MSPCountry,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join 
			dbo.HrVendorCountry
			On dbo.HrVendorCountry.VendorCountryID = Tasks.VendorCountryID
		Inner Join 
			dbo.HrVendor
			On
			dbo.HrVendor.VendorID = dbo.HrVendorCountry.VendorID
		Inner Join
			dbo.LocationCountry as LC
			On LC.CountryId = dbo.HrVendorCountry.CountryID
		Inner Join
			HRPerson As HRPerson_PO
			On
			HRPerson_PO.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');
	End
	Else If @TaskTypeId = 18		-- Approve Smart Card 
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
			,AccessRequestMaster.RequestMasterID
			,COALESCE(ajr.Name, '') Justification 
			,'Smart Card' as CardType
			,AccessRequest.SmartCardJustification as SmartCardJustification
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		Left Outer Join
			AccessArea
			On
			AccessArea.AccessAreaID = AccessRequest.AccessAreaID
		Left Outer Join
			Category
			On
			Category.id = AccessArea.AccessControlID
		LEFT JOIN dbo.AccessRequestJustificationReason ajr ON AccessRequest.AccessRequestJustificationReasonID = ajr.ID	
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

END
GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_GetTaskDetails]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_GetTaskDetails]
END
GO

/****** Object:  StoredProcedure [dbo].[up_GetTaskDetails]    Script Date: 2/27/2017 5:15:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[up_GetTaskDetails] 
(
	@TaskID Int
)
As
Begin

	Set NoCount On;

	Declare @TaskTypeId Int;
	Set @TaskTypeId =
	(
		Select
			TaskTypeId
		From
			Tasks
		Where
			TaskId = @TaskID
	);

	If @TaskTypeId in (1, 12, 13, 14, 15, 16, 17)
	
	Begin

		Select
			t.TaskId,
			t.TaskTypeId,
			p.Forename + ' ' + p.Surname AS [Name],
			p.EmailAddress,
			p.dbPeopleID,
			p.DBDirID,
			IsNull(p.Telephone,''),
			IsNull(arm.Comment,''),
			Convert(varchar,ar.StartDate, 106) AS StartDate,
			Convert(varchar,ar.EndDate, 106) AS EndDate,
			IsNull(adel.ContactEmail,'') AS ContactEmail,
			IsNull(adel.ContactTelephone,'') AS ContactTelephone,
			arm.RequestMasterID ,
			COALESCE(bjr.Name, '') Justification,
			(Select EmailAddress From HRPerson where dbPeopleID = ar.CreatedByID) As RequestedBy,
			CASE WHEN ar.RequestTypeID = 15 THEN IsNUll([dbo].[GetAccessAreasHavingLesserApproversDueToOffboard](arp.[dbPeopleId]), '') 
			ELSE '' END AS LesserApprovalWarning
		From
			Tasks t
		Inner Join
			vw_AccessRequest ar
			On
			t.RequestId = ar.RequestID
		Inner Join
			HRPerson p
			On
			ar.dbPeopleID = p.dbPeopleID
		Inner Join
			AccessRequestPerson arp
			On
			arp.RequestPersonID = ar.RequestPersonID
		Inner Join
			AccessRequestMaster arm
			On
			arm.RequestMasterID = arp.RequestMasterID
		LEFT JOIN 
			[dbo].[AccessRequestDeliveryDetail] adel
			ON arm.RequestMasterID = adel.RequestMasterID
		LEFT JOIN dbo.BadgeJustificationReason bjr ON ar.BadgeJustificationReasonID = bjr.ID
		Where
			t.[TaskId] = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 2		-- Approve Person
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
            ,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID,
			COALESCE(ajr.Name, '') Justification

		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		LEFT JOIN dbo.AccessRequestJustificationReason ajr ON AccessRequest.AccessRequestJustificationReasonID = ajr.ID	
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 3		-- Approve Access
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
			,IsNull (RTrim (Category.[description]) + ' (' + Cast (Category.id As VarChar) + ')', '') As Category
			,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID,
			COALESCE(ajr.Name, '') Justification 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		Left Outer Join
			AccessArea
			On
			AccessArea.AccessAreaID = AccessRequest.AccessAreaID
		Left Outer Join
			Category
			On
			Category.id = AccessArea.AccessControlID
		LEFT JOIN dbo.AccessRequestJustificationReason ajr ON AccessRequest.AccessRequestJustificationReasonID = ajr.ID	
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 7		-- Manual Add Access
	Begin

		Select
			 Tasks.TaskId
			,Tasks.TaskTypeId
			,HRPerson.dbPeopleID
			,IsNull (HRPerson.Forename + ' ' + HRPerson.Surname, '') AS [Name]
			,IsNull (HRPerson.EmailAddress, '') As EmailAddress
			,IsNull (HRPerson.DBDirID, 0) As DBDirID
			,IsNull (HRPerson.Telephone, '') As Telephone
			,IsNull (AccessRequestMaster.Comment, '') As Comment
			,IsNull (RTrim (Category.[description]) + ' (' + Cast (Category.id As VarChar) + ')', '') As Category
			,Convert(varchar,AccessRequest.StartDate, 106) AS StartDate
			,Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			HRPerson
			On
			HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
		Left Outer Join
			AccessArea
			On
			AccessArea.AccessAreaID = AccessRequest.AccessAreaID
		Left Outer Join
			Category
			On
			Category.id = AccessArea.AccessControlID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 4 -- This is Business Area Approver request
	Begin

		Select
			t.TaskId,
			t.TaskTypeId,
			ba.[Name] AS [BusinessAreaName],
			d.[Name] AS [DivisionName],
			u.Id,
			u.[Forename] + ' ' + u.[Surname] AS [DivisionAdmin],
			'' RequestMasterID 
		From
			Tasks t
		Inner Join
			CorporateBusinessArea ba
			On
			t.BusinessAreaId = ba.BusinessAreaID
		Inner Join
			CorporateDivision d
			On
			ba.DivisionID = d.DivisionID
		Inner Join
			tasksUsers cu
			On
			cu.taskId = t.taskID
		Inner Join
			mp_User u
			On
			u.dbPeopleID = cu.dbPeopleID
		Where
			t.TaskId = @TaskId
			--AND cu.mp_SecurityGroupID = 3

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId=5 -- Access Area Approver Request
	Begin
		Select
			t.TaskId,
			--t.[Description],
			--IsNull(vmt.[Descript],'') AS [Description],
			t.TaskTypeId,
			p.Forename + ' ' + p.Surname AS [Name],
			p.EmailAddress,
			IsNull(p.Telephone,''),
			IsNull(arm.Comment,''),
			arm.RequestMasterID 
			From [Tasks] t 
			Join vw_AccessRequest ar On t.RequestId = ar.RequestID
			Join HRPerson p On ar.dbPeopleID = p.dbPeopleID
			Join AccessRequestPerson arp On arp.RequestPersonID = ar.RequestPersonID
			Join AccessRequestMaster arm On arm.RequestMasterID = arp.RequestMasterID
			Where t.[TaskId] = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 6		-- AR Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (CorporateDivision.Name + ' (' + CorporateDivision.UBR + ')', '') As DivisionName,
			IsNull (CorporateBusinessArea.Name + ' (' + CorporateBusinessArea.UBR + ')', '') As BusinessAreaName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		
		-- these should exist, but may not...

		Left Outer Join
			CorporateDivision
			On
			CorporateDivision.DivisionID = Tasks.DivisionId
		Left Outer Join
			CorporateBusinessArea
			On
			CorporateBusinessArea.BusinessAreaID = Tasks.BusinessAreaId
		Left Outer Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If (@TaskTypeId In (8, 10))		-- DA/DO Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (CorporateDivision.Name + ' (' + CorporateDivision.UBR + ')', '') As DivisionName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			CorporateDivision
			On
			CorporateDivision.DivisionID = Tasks.DivisionId
		Inner Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');

	End

	Else If @TaskTypeId = 9		-- PO Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (LocationPassOffice.Name, '') As PassOfficeName,
			IsNull (HRPerson_DA.Forename + ' ' + HRPerson_DA.Surname, '') As DAName,
			IsNull (HRPerson_DA.EmailAddress, '') As DAEmailAddress,
			IsNull (HRPerson_DA.Telephone, '') As DATelephoneNumber,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join
			LocationPassOffice
			On
			LocationPassOffice.PassOfficeID = Tasks.PassOfficeId
		Inner Join
			HRPerson As HRPerson_DA
			On
			HRPerson_DA.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');
	END
	Else If @TaskTypeId = 11		-- MSP Role Request
	Begin

		Select
			Tasks.TaskId,
			Tasks.TaskTypeId,
			IsNull (AccessRequestMaster.Comment, '') As Comment,
			IsNull (HRPerson_PO.Forename + ' ' + HRPerson_PO.Surname, '') As POName,
			IsNull (HRPerson_PO.EmailAddress, '') As POEmailAddress,
			IsNull (HRPerson_PO.Telephone, '') As POTelephoneNumber,
			IsNull(HRVendor.VendorName,'') As MSPResourceName,
			IsNull(LC.Name,'') as MSPCountry,
			Convert(varchar,AccessRequest.StartDate, 106) AS StartDate,
			Convert(varchar,AccessRequest.EndDate, 106) AS EndDate,
			AccessRequestMaster.RequestMasterID 
		From
			Tasks
		Inner Join
			AccessRequest
			On
			AccessRequest.RequestID = Tasks.RequestId
		Inner Join
			AccessRequestPerson
			On
			AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		Inner Join
			AccessRequestMaster
			On
			AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
		Inner Join 
			dbo.HrVendorCountry
			On dbo.HrVendorCountry.VendorCountryID = Tasks.VendorCountryID
		Inner Join 
			dbo.HrVendor
			On
			dbo.HrVendor.VendorID = dbo.HrVendorCountry.VendorID
		Inner Join
			dbo.LocationCountry as LC
			On LC.CountryId = dbo.HrVendorCountry.CountryID
		Inner Join
			HRPerson As HRPerson_PO
			On
			HRPerson_PO.dbPeopleID = Tasks.CreatedByID
		Where
			Tasks.TaskId = @TaskID

		For XML Path, Root('ROOT');
	End

END
GO