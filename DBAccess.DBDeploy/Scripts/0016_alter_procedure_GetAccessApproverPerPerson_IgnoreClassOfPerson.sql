/****** Object:  UserDefinedFunction [dbo].[GetAccessApproverPerPerson]    Script Date: 13/05/2015 13:43:14 ******/
DROP FUNCTION [dbo].[GetAccessApproverPerPerson]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAccessApproverPerPerson]    Script Date: 13/05/2015 13:43:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetAccessApproverPerPerson]
(
	@iDivisionId Int,
	@iAccessAreaId Int,
	@sHRClassification varchar(100),
	@isExternal bit,
	@ForeName nvarchar(50),
	@SurName nvarchar(50)
	
)
Returns NVarchar(max)
As
Begin

		Declare @Approvers Varchar(max)
		Declare @iHRClassificationId Int
			IF (@isExternal = 1)
			Begin
	   			Set  @iHRClassificationId = null
			End

			IF @iDivisionId IS NOT NULL
			Begin
					SELECT @Approvers = Coalesce(@Approvers + '<br />', '') + EmailAddress   
					FROM 
							HRPerson as hrp  
					where 
							hrp.dbPeopleID IN(
												SELECT 
														dbPeopleID
												FROM	[dbo].[GetAvailableAccessApprovers] (@iDivisionId,@iAccessAreaId,@iHRClassificationId)
											  )
			End			  
			
			--FormatAccessApproverEmail(Clinton Dennis Price(Unclassified):cathey.walker@db.com<br />anastasios.sirigos@db.com<br />mark.purdon@db.com<br />)
			IF @Approvers IS NULL
			BEGIN
				Set @Approvers = 'Error'
			END
			ELSE
			BEGIN
				Set @Approvers =  @ForeName + ' ' + @SurName + '(' + 'Unclassified' + ')' + ':' + @Approvers + '<br />'
			End
			
			RETURN @Approvers

End				  

GO

--//@UNDO

/****** Object:  UserDefinedFunction [dbo].[GetAccessApproverPerPerson]    Script Date: 13/05/2015 13:43:14 ******/
DROP FUNCTION [dbo].[GetAccessApproverPerPerson]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAccessApproverPerPerson]    Script Date: 13/05/2015 13:43:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetAccessApproverPerPerson]
(
	@iDivisionId Int,
	@iAccessAreaId Int,
	@sHRClassification varchar(100),
	@isExternal bit,
	@ForeName nvarchar(50),
	@SurName nvarchar(50)
	
)
Returns NVarchar(max)
As
Begin

		Declare @Approvers Varchar(max)
		Declare @iHRClassificationId Int
		IF (@isExternal = 1)
			Begin
					
					Set  @iHRClassificationId = (Select 		Top 1	hrc.HRClassificationID 
												from 
															HRClassification as hrc
												where
															hrc.Classification = @sHRClassification)
			End
			Else
			Begin
					Set  @iHRClassificationId = null
			End

			--BuildAccessApproverEmail string(cathey.walker@db.com<br />anastasios.sirigos@db.com<br />mark.purdon@db.com)
			IF @iDivisionId IS NOT NULL
			Begin
					SELECT @Approvers = Coalesce(@Approvers + '<br />', '') + EmailAddress   
					FROM 
							HRPerson as hrp  
					where 
							hrp.dbPeopleID IN(
												SELECT 
														dbPeopleID
												FROM	[dbo].[GetAvailableAccessApprovers] (@iDivisionId,@iAccessAreaId,@iHRClassificationId)
											  )
			End			  
			
			--FormatAccessApproverEmail(Clinton Dennis Price(Unclassified):cathey.walker@db.com<br />anastasios.sirigos@db.com<br />mark.purdon@db.com<br />)
			IF @Approvers IS NULL
			BEGIN
				Set @Approvers = 'Error'
			END
			ELSE
			BEGIN

				IF @sHRClassification IS NULL
				BEGIN
						Set @Approvers =  @ForeName + ' ' + @SurName + '(' + 'Unclassified' + ')' + ':' + @Approvers + '<br />'
				END
				Else
				BEGIN
						Declare @iHRDescription Varchar(max)
						Set  @iHRDescription = (Select 		Top 1	hrc.[Description] 
														from 
																	HRClassification as hrc
														where
																	hrc.Classification = @sHRClassification)
						Set @Approvers =   @ForeName + ' ' + @SurName + '(' + @sHRClassification + '-' + @iHRDescription + ')' + ':' + @Approvers + '<br />'
				End
			End
			
			RETURN @Approvers

End				  

GO