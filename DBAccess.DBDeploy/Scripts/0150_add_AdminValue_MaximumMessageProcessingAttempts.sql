IF(NOT EXISTS (SELECT * FROM AdminValues WHERE [Key] = 'MaximumMessageProcessingAttempts'))
BEGIN
	INSERT INTO AdminValues ([Key], Value)
	VALUES ('MaximumMessageProcessingAttempts', 3)
END

--//@UNDO

DELETE FROM AdminValues 
WHERE [Key] = 'MaximumMessageProcessingAttempts'