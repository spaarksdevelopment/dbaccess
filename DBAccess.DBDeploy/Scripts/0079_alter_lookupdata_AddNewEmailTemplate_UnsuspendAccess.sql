﻿IF EXISTS(SELECT * FROM EmailTemplates WHERE [Type]=40)
	DELETE FROM EmailTemplates WHERE [Type]=40
GO

INSERT INTO EmailTemplates([Type],[Subject],[Body])
VALUES
(
	40,
	'DBAccess - New Unsuspend Request',
	'Dear [$$NAME$$],<br/><br/>An unsuspend access request has been created for user  [$$UserEmail$$].<br/>Please use the link below to access the system in order to process this request.<br/>Kind Regards<br />DBAccess System Administrator<br />[$$HTTP_ROOT$$][$$FILE_PATH$$]'
)

GO

--//@UNDO

IF EXISTS(SELECT * FROM EmailTemplates WHERE [Type]=40)
	DELETE FROM EmailTemplates WHERE [Type]=40
GO