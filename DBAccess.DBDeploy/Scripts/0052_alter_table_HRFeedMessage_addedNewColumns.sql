
if not exists(select * from sys.columns 
            where Name = N'AMRID' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD AMRID nvarchar(3) NULL	
end

if not exists(select * from sys.columns 
            where Name = N'PreferredFirstName' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD PreferredFirstName nvarchar(60) NULL
end

if not exists(select * from sys.columns 
            where Name = N'PreferredLastName' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD PreferredLastName nvarchar(60) NULL
end

if not exists(select * from sys.columns 
            where Name = N'CorporateTitle' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD CorporateTitle nvarchar(4) NULL
end

if not exists(select * from sys.columns 
            where Name = N'EP_Code' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD EP_Code nvarchar(10) NULL
end

if not exists(select * from sys.columns 
            where Name = N'PhysicalAccessOnly' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD PhysicalAccessOnly nvarchar(1) NULL
end

if not exists(select * from sys.columns 
            where Name = N'DBSiteIndicator' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD DBSiteIndicator nvarchar(1) NULL
end

if not exists(select * from sys.columns 
            where Name = N'DBEmailAddress' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] ADD DBEmailAddress nvarchar(120) NULL
end

GO

--//@UNDO


if exists(select * from sys.columns 
            where Name = N'AMRID' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN AMRID
end

if exists(select * from sys.columns 
            where Name = N'PreferredFirstName' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN PreferredFirstName
end

if exists(select * from sys.columns 
            where Name = N'PreferredLastName' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN PreferredLastName
end

if exists(select * from sys.columns 
            where Name = N'CorporateTitle' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN CorporateTitle
end

if exists(select * from sys.columns 
            where Name = N'EP_Code' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN EP_Code
end


if exists(select * from sys.columns 
            where Name = N'PhysicalAccessOnly' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN PhysicalAccessOnly
end

if exists(select * from sys.columns 
            where Name = N'DBSiteIndicator' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN DBSiteIndicator
end

if exists(select * from sys.columns 
            where Name = N'DBEmailAddress' and Object_ID = Object_ID(N'HRFeedMessage'))
begin
	ALTER TABLE [dbo].[HRFeedMessage] DROP COLUMN DBEmailAddress
end

GO
