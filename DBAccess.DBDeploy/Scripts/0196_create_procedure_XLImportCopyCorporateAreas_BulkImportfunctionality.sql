IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyCorporateAreas]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyCorporateAreas]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Asanka
-- Created Date: 22/12/2015
-- Description: Copy corporate access areas from staging table to the CorporateDivisionAccessArea table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyCorporateAreas]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO CorporateDivisionAccessArea (DivisionID, AccessAreaID, Enabled, ApprovalTypeID, NumberOfApprovals, Frequency, LastCertifiedDate, DateNextCertification)
	OUTPUT inserted.CorporateDivisionAccessAreaID into @tblIDs
	SELECT (SELECT d.DivisionID 
			FROM CorporateDivision d
			INNER JOIN LocationCountry c on d.CountryID = c.CountryID
			where d.Name = sca.Division ANd c.Name = sd.Country),
			(SELECT a.AccessAreaID 
			FROM AccessArea a
			where a.Name = sca.AccessArea and a.BuildingID = b.BuildingID),
		1,
		(SELECT ApprovalTypeID FROM ApprovalTypeLookup where ApprovalType = sca.ApprovalType),
		CAST(sca.NoOfApprovals AS tinyint),
		CAST(sca.Frequency AS INT),
		CAST(sca.LastCertifiedDate AS datetime),
		CAST(sca.NextCertifiedDate AS smalldatetime)
	FROM XLImportStageCorporateArea sca
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID

	WHERE ISNULL(sca.ValidationFailed, 0) = 0 AND sca.FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionAccessArea', ID FROM @tblIDs

END
GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyCorporateAreas]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyCorporateAreas]
END
GO