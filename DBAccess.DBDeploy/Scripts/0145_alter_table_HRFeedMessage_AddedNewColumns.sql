IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HRFeedMessage' AND COLUMN_NAME = 'RevokeSkippedLogged')
BEGIN
	ALTER TABLE HRFeedMessage 
	ADD RevokeSkippedLogged nvarchar(60)
END
GO 

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HRFeedMessage' AND COLUMN_NAME = 'OffboardSkippedLogged')
BEGIN
	ALTER TABLE HRFeedMessage 
	ADD OffboardSkippedLogged nvarchar(60)
END
GO 

--Set RevokedSkippedLogged and OffboardSkippedLogged flags as '1' for existing records to avoid creating log records for existing records
--Which does not make sence.
UPDATE HRFeedMessage SET RevokeSkippedLogged = 1, OffboardSkippedLogged = 1
WHERE TriggerType = 'LVER' AND TriggerAction <> 'DEL' 
	
--//@UNDO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HRFeedMessage' AND COLUMN_NAME = 'RevokeSkippedLogged')
BEGIN
	ALTER TABLE HRFeedMessage 
	DROP COLUMN RevokeSkippedLogged
END
GO 

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HRFeedMessage' AND COLUMN_NAME = 'OffboardSkippedLogged')
BEGIN
	ALTER TABLE HRFeedMessage 
	DROP COLUMN OffboardSkippedLogged
END
GO