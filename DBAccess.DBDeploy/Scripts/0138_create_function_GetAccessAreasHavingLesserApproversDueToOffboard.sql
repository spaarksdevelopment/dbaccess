IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]    Script Date: 11/17/2015 1:27:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Asanka
-- Create date: 10/11/2015
-- Description:	Returns whether offboading a user may result too few approvers for Access areas.
-- =============================================
CREATE FUNCTION [dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]
(
	@dbPeopleID int
)
RETURNS varchar(max)
AS
BEGIN

	DECLARE @NumberOfAccessAreas int
	declare @accessAreaNames varchar(max) = ''

	--Get threshold value for minimum number of approvers from the Admin Values
	DECLARE @NumberOfApproversThreshold int = 0
	SELECT @NumberOfApproversThreshold = cast(Isnull(Value, 0) as int) FROM AdminValues
	WHERE [Key] = 'AccessAreaNumberOfApprovalsWarningThreshold'

	;WITH ct AS (
	SELECT aa.CorporateDivisionAccessAreaID, COUNT(allApprovers.dbPeopleID)  AS CurrentApprovers
	FROM CorporateDivisionAccessArea aa
	INNER JOIN CorporateDivisionAccessAreaApprover approver ON aa.CorporateDivisionAccessAreaID = approver.CorporateDivisionAccessAreaID
	LEFT OUTER JOIN CorporateDivisionAccessAreaApprover allApprovers ON allApprovers.CorporateDivisionAccessAreaID=aa.CorporateDivisionAccessAreaID AND allApprovers.Enabled = 1
	WHERE approver.dbPeopleID = @dbPeopleID 
	GROUP BY aa.CorporateDivisionAccessAreaID
	)

	SELECT @accessAreaNames = @accessAreaNames + '<br />'+ dbo.GetAccessAreaName(CAA.CorporateDivisionAccessAreaID)
	FROM CorporateDivisionAccessArea caa
		LEFT OUTER JOIN AccessArea aa on aa.AccessAreaID = caa.AccessAreaID
		LEFT OUTER JOIN LocationFloor f on f.FloorID = aa.FloorID
		LEFT OUTER JOIN LocationBuilding b on aa.BuildingID = b.BuildingID
		LEFT OUTER JOIN LocationCity city on city.CityID = b.CityID
	WHERE caa.CorporateDivisionAccessAreaID IN (SELECT CorporateDivisionAccessAreaID FROM ct 
												WHERE CurrentApprovers < @NumberOfApproversThreshold)

	if(LEN(@accessAreaNames) > 0)
		RETURN RIGHT(@accessAreaNames, LEN(@accessAreaNames) - 6)
	
	RETURN NULL
	
END



GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetAccessAreasHavingLesserApproversDueToOffboard]
END
GO