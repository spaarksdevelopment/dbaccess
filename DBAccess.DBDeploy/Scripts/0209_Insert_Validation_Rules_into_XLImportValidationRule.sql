
TRUNCATE TABLE XLImportValidationRule

SET IDENTITY_INSERT [dbo].[XLImportValidationRule] ON 

INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (1, 1, N'Error', N'Country is mandatory field', N'select ID, RowNumber from XLImportStageCity where Country is null or len(Country) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (2, 1, N'Error', N'Country should be added manually before loading data', N'select ID, RowNumber from XLImportStageCity where Country is not null and len(Country) > 0 and Country not in (select Name from LocationCountry)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (4, 1, N'Warning', N'City already exists', N'select sc.ID, sc.RowNumber 
from XLImportStageCity sc 
inner join (select c.Name as city, lc.Name as country 
			from  LocationCountry lc
			inner join LocationCity c on lc.CountryID = c.CountryID) t on t.city = sc.Name and sc.Country = t.country', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (6, 1, N'Error', N'City names should be unique', N';with ct as 
(select ID, Name, RowNumber, ROW_NUMBER() Over(Partition by Name order by RowNumber) as DuplicateRowIndex
from XLImportStageCity
where Name is not null)
select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (7, 4, N'Warning', N'Building already exists', N'select sb.ID, sb.RowNumber 
from XLImportStageBuilding sb
inner join XLImportStageCity sc on sb.City = sc.Name
inner join LocationCountry c on c.Name = sc.Country
inner join LocationCity city on city.Name = sc.Name and city.CountryID = c.CountryID
inner join LocationBuilding b on b.Name = sb.Name and b.CityID=city.CityID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (8, 4, N'Error', N'City is mandatory field', N'select ID, RowNumber from XLImportStageBuilding where City is null or len(city) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (12, 4, N'Error', N'City is not in the city sheet', N'select ID, RowNumber 
from XLImportStageBuildingwhere City is not null and len(City) > 0 and not (City in (select name from XLImportStageCity))', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (14, 2, N'Error', N'Country is mandatory field', N'select ID, RowNumber from XLImportStagePassOffice where Country is null or len(Country ) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (15, 2, N'Error', N'Country should be added manually before loading data', N'select ID, RowNumber from XLImportStagePassOffice where Country is not null and len(Country) > 0 and Country not in (select Name from LocationCountry)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (19, 5, N'Error', N'Country should be added manually before loading data', N'select ID, RowNumber from XLImportStageDivision where Country is not null  and len(Country) > 0  and Country not in (select Name from LocationCountry)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (22, 5, N'Error', N'Country is mandatory field', N'select ID, RowNumber from XLImportStageDivision where Country is null or Len(Country) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (23, 5, N'Error', N'There should be atleast one division', N'select 0 as ID, 0 as RowNumber 
from (select count(1) as recordCount from XLImportStageDivision) t
where recordCount = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (25, 2, N'Error', N'City is not in the city sheet', N'select ID, RowNumber 
from XLImportStagePassOffice
where City is not null  and len(City) > 0 and not (City in (select name from XLImportStageCity))', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (26, 5, N'Error', N'Recert Period must be either 1,3,6 or 12
', N'select ID, RowNumber 
from XLImportStageDivision
where RecertPeriod is not null and len(RecertPeriod) > 0 and CAST(RecertPeriod as int) not in (1, 3, 6, 12)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (27, 5, N'Error', N'UBR must be in the format of G_...', N'select ID, RowNumber from XLImportStageDivision where UBR is not null and len(UBR) >0 and UBR not like ''G_%'' ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (30, 4, N'Error', N'Pass Office is mandaroty field', N'select ID, RowNumber from XLImportStageBuilding where PassOffice is null or len(PassOffice) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (32, 4, N'Error', N'Pass Office is not in the Pass Office Sheet', N'select ID, RowNumber 
from XLImportStageBuilding where PassOffice is not null  and len(PassOffice) > 0 and not (PassOffice in (select name from XLImportStagePassOffice))', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (33, 4, N'Error', N'Building names should be unique', N';with ct as 
(select ID, Name, RowNumber, ROW_NUMBER() Over(Partition by Name order by RowNumber) as DuplicateRowIndex
from XLImportStageBuilding
where Name is not null)
select ID, RowNumber from ct where DuplicateRowIndex >= 2 ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (34, 2, N'Warning', N'Pass Office already exists', N'select distinct sc.ID, sc.RowNumber 
from XLImportStagePassOffice sc 
where Name in  (select Name from LocationPassOffice) ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (35, 5, N'Error', N'Division names should be unique', N';with ct as 
(select ID, Name, RowNumber, ROW_NUMBER() Over(Partition by Name order by RowNumber) as DuplicateRowIndex
from XLImportStageDivision
where Name is not null)
select ID, RowNumber from ct where DuplicateRowIndex >= 2 ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (38, 5, N'Warning', N'Division already exists', N'select distinct sd.ID, sd.RowNumber 
from XLImportStageDivision sd 
inner join CorporateDivision cd on sd.Name = cd.Name 
inner join LocationCountry c on c.Name = sd.Country and c.CountryID = cd.CountryID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (39, 6, N'Error', N'Division must be defined in division sheet', N'select ID, RowNumber from XLImportStageDivisionRole where Division not in (select Name from XLImportStageDivision) and len(Division) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (41, 6, N'Error', N'Security Group must be either Division Owner or Division Administrator', N'select ID, RowNumber from XLImportStageDivisionRole where SecurityGroup is not null and len(SecurityGroup) > 0  and SecurityGroup not in (''Division Owner'', ''Divisional Administrator'')', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (42, 6, N'Error', N'Division name is mandatory field', N'select ID, RowNumber from XLImportStageDivisionRole where Division is null or len(Division) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (43, 6, N'Error', N'Security Group is mandatory field', N'select ID, RowNumber from XLImportStageDivisionRole where SecurityGroup is null or len(SecurityGroup) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (44, 6, N'Error', N'DBPeopleID must exist in the system', N'select ID, RowNumber from XLImportStageDivisionRole where DBPeopleID not in (select dbPeopleID from HRPerson) and len(DBPeopleID) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (45, 5, N'Error', N'Division must have one Division Owner in the Division Role sheet', N'select ID, RowNumber from XLImportStageDivision where Name not in (select Division from XLImportStageDivisionRole where SecurityGroup = ''Division Owner'')', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (47, 5, N'Error', N'Division must have at least one Division Administrator in the Division Role sheet', N'
select ID, RowNumber 
from XLImportStageDivision 
where Name not in (select Division from XLImportStageDivisionRole where SecurityGroup = ''Divisional Administrator'')', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (48, 6, N'Error', N'Division can have only one Division Owner', N';with ct as 
(select ID, Division, SecurityGroup, RowNumber, ROW_NUMBER() Over(Partition by Division, SecurityGroup order by RowNumber) as DuplicateRowIndex
from XLImportStageDivisionRole
where Division is not null and SecurityGroup is not null and SecurityGroup = ''Division Owner'')
select ID, RowNumber from ct where DuplicateRowIndex >= 2 
', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (49, 6, N'Error', N'Same person has been added twice as for the same division as either Division Owner or Division Administrator ', N';with ct as 
(select ID, Division, DBPeopleID, SecurityGroup, RowNumber, ROW_NUMBER() Over(Partition by Division, DBPeopleID, SecurityGroup order by RowNumber) as DuplicateRowIndex
from XLImportStageDivisionRole
where Division is not null and DBPeopleID is not null and SecurityGroup in (''Division Owner'', ''Divisional Administrator''))
select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (50, 14, N'Error', N'Pass Office name is mandatory feild', N'select ID, RowNumber from XLImportStagePassOfficeUser where PassOffice is null or len(PassOffice) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (52, 14, N'Error', N'Same person has been added to the same pass office more than once', N';with ct as 
(select ID, PassOffice, DBPeopleID, RowNumber, ROW_NUMBER() Over(Partition by PassOffice, DBPeopleID order by RowNumber) as DuplicateRowIndex
from XLImportStagePassOfficeUser
where PassOffice is not null and DBPeopleID is not null)
select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (53, 14, N'Error', N'Pass Office is not in the Pass Office Sheet', N'select ID, RowNumber 
from XLImportStagePassOfficeUserwhere PassOffice is not null and len(PassOffice) > 0 and not (PassOffice in (select name from XLImportStagePassOffice))', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (54, 8, N'Error', N'Access Area Type is mandatory field', N'select ID, RowNumber from XLImportStageAccessArea where AccessAreaType is null or len(AccessAreaType) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (55, 8, N'Error', N'Floor name is mandatory field', N'select ID, RowNumber from XLImportStageAccessArea where FloorName is null or len(FloorName) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (57, 8, N'Error', N'Building name is mandatory field', N'select ID, RowNumber from XLImportStageAccessArea where Building is null or len(Building) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (58, 8, N'Error', N'Category Description is mandatory field', N'select ID, RowNumber from XLImportStageAccessArea where CategoryDescription is null or len(CategoryDescription) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (59, 8, N'Error', N'Control System name is mandatory field', N'select ID, RowNumber from XLImportStageAccessArea where ControlSystem is null or len(ControlSystem) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (60, 8, N'Error', N'Building must be defined in the building sheet', N'select ID, RowNumber from XLImportStageAccessArea where Building not in (select Name from XLImportStageBuilding) and len(Building) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (63, 8, N'Error', N'Control system name should exist in the system', N'select ID, RowNumber from XLImportStageAccessArea where ControlSystem not in (select InstanceName from ControlSystem) and len(ControlSystem) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (64, 8, N'Error', N'Access Area Type should exist in the system', N'select ID, RowNumber from XLImportStageAccessArea where AccessAreaType not in (select AccessAreaTypeName from AccessAreaType) and len(AccessAreaType) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (66, 8, N'Warning', N'Access Area already exists', N'SELECT distinct saa.ID, saa.RowNumber
	FROM XLImportStageAccessArea saa
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sb.City and city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name and b.CityID = city.CityID
		INNER JOIN AccessArea a on a.Name = saa.Name and b.BuildingID = a.BuildingID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (67, 8, N'Error', N'Access Area Names should be unique', N';with ct as 
(select ID, Name, RowNumber, ROW_NUMBER() Over(Partition by Name order by RowNumber) as DuplicateRowIndex
from XLImportStageAccessArea
where Name is not null) 
select ID, RowNumber from ct where DuplicateRowIndex >= 2
', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (68, 9, N'Error', N'Division name is mandatory field', N'select ID, RowNumber from XLImportStageCorporateArea where Division is null or len(Division) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (69, 9, N'Error', N'Access Area name is mandatory field', N'select ID, RowNumber from XLImportStageCorporateArea where AccessArea is null or len(AccessArea) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (70, 9, N'Error', N'Approval Type is mandatory field', N'select ID, RowNumber from XLImportStageCorporateArea where ApprovalType is null or len(ApprovalType) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (71, 9, N'Error', N'Division must be defined in the Division sheet', N'select ID, RowNumber from XLImportStageCorporateArea where Division not in (select Name from XLImportStageDivision) and len(Division) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (73, 9, N'Error', N'Access Area must be defined in the Access Area sheet', N'select ID, RowNumber from XLImportStageCorporateArea where AccessArea not in (select Name from XLImportStageAccessArea) and len(AccessArea) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (74, 8, N'Error', N'There should be a Corporate Area for each Access Area', N'select ID, RowNumber from XLImportStageAccessArea where Name not in (select AccessArea from XLImportStageCorporateArea)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (75, 10, N'Error', N'Division name is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaApprover where Division is null or len(Division) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (76, 10, N'Error', N'Access Area name is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaApprover where AccessArea is null or len(AccessArea) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (77, 10, N'Error', N'Division must be defined in the Division sheet', N'select ID, RowNumber from XLImportStageAccessAreaApprover where Division not in (select Name from XLImportStageDivision) and len(Division) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (78, 10, N'Error', N'Access Area must be defined in the Access Area sheet', N'select ID, RowNumber from XLImportStageAccessAreaApprover where AccessArea not in (select Name from XLImportStageAccessArea) and len(AccessArea) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (79, 10, N'Error', N'Same Person has been added as an approver to same Access Area more than once', N';with ct as 
(select ID, Division, AccessArea, DBPeopleID, RowNumber, ROW_NUMBER() Over(Partition by Division, AccessArea, DBPeopleID order by RowNumber) as DuplicateRowIndex
from XLImportStageAccessAreaApprover
where Division is not null and AccessArea is not null and DBPeopleID is not null) 
select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (80, 8, N'Error', N'Access Area must have atleast two approvers in the Access Approver sheet', N'select MAx(sa.ID) as ID , Max(sa.RowNumber) as RowNumber
from  XLImportStageAccessArea sa 
left outer join XLImportStageAccessAreaApprover sap  on sa.Name = sap.AccessArea 
group by sa.Name HAVING count(sa.Name) < 2 and sa.Name is not null', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (81, 13, N'Error', N'Division name is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where Division is null or len(Division) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (82, 13, N'Error', N'Access Area name is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where AccessArea is null or len(AccessArea) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (83, 13, N'Error', N'Division must be defined in the Division sheet', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where Division not in (select Name from XLImportStageDivision) and len(Division) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (84, 13, N'Error', N'Access Area must be defined in the Access Area sheet', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where AccessArea not in (select Name from XLImportStageAccessArea) and len(AccessArea) > 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (85, 8, N'Error', N'There should be at least one Access Area Recertifier for each Access Area', N'select ID, RowNumber from XLImportStageAccessArea where Name not in (select AccessArea from XLImportStageAccessAreaRecertifier)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (86, 1, N'Error', N'Time Zone is mandatory field', N'select ID, RowNumber from XLImportStageCity where TimeZone is null or len(TimeZone) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (87, 1, N'Error', N'Time Zone should exist in the system', N'select ID, RowNumber from XLImportStageCity where TimeZone is not null and len(TimeZone) > 0 and TimeZone not in (select distinct description from TimeZone)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (89, 6, N'Warning', N'Division Role already exists', N'
select  distinct sdr.ID, sdr.RowNumber 
from XLImportStageDivisionRole sdr
inner join XLImportStageDivision sd on sd.Name = sdr.Division
inner join (select d.Name as Division, sg.RoleName as SecurityGroup, dr.dbPeopleID, c.Name as Country 
			from CorporateDivisionRoleUser dr
			inner join CorporateDivision d on dr.DivisionID = d.DivisionID
			inner join LocationCountry c on c.CountryID = d.CountryID
			inner join mp_SecurityGroup sg on sg.Id = dr.mp_SecurityGroupID) t on t.Division = sdr.Division and t.SecurityGroup = sdr.SecurityGroup and sdr.DBPeopleID = t.dbPeopleID and t.Country = sd.Country', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (91, 9, N'Warning', N'Corporate Area already exists', N'select distinct sca.ID, sca.RowNumber
	FROM XLImportStageCorporateArea sca
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry on divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a on a.BuildingID = b.BuildingID AND a.Name = saa.Name
		INNER JOIN CorporateDivision cd on cd.Name = sca.Division and cd.CountryID = divisionCountry.CountryID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (92, 10, N'Warning', N'Access Area Approver already exists', N'

select  distinct saaa.ID, saaa.RowNumber 
from XLImportStageAccessAreaApprover saaa
INNER JOIN XLImportStageCorporateArea sca on saaa.Division = sca.Division AND saaa.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry on divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a on a.BuildingID = b.BuildingID AND a.Name = saaa.AccessArea
		inner join (select d.Name as Division, a.Name as AccessArea, aaa.dbPeopleID, d.CountryID, a.BuildingID
					from CorporateDivisionAccessAreaApprover aaa
					inner join  CorporateDivisionAccessArea  ca on ca.CorporateDivisionAccessAreaID = aaa.CorporateDivisionAccessAreaID
					inner join CorporateDivision d on ca.DivisionID = d.DivisionID
					inner join AccessArea a on a.AccessAreaID = ca.AccessAreaID
					inner join mp_SecurityGroup sg on sg.Id = aaa.mp_SecurityGroupID
					where sg.RoleName=''Access Approver'' ) t on t.Division= saaa.Division and t.CountryID = divisionCountry.CountryID and t.AccessArea = saaa.AccessArea and t.BuildingID = b.BuildingID  and t.dbPeopleID = saaa.DBPeopleID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (93, 13, N'Warning', N'Access Area Recertifier already exists', N'
select  distinct saar.ID, saar.RowNumber 
from XLImportStageAccessAreaRecertifier saar
INNER JOIN XLImportStageCorporateArea sca on saar.Division = sca.Division AND saar.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd on sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry on divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa on saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb on saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name
		INNER JOIN LocationCountry c on c.Name = sc.Country
		INNER JOIN LocationCity city on city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b on b.Name = sb.Name AND b.CityID = city.CityID
		inner join (select d.Name as Division, a.Name as AccessArea, aaa.dbPeopleID, d.CountryID, a.BuildingID
					from CorporateDivisionAccessAreaApprover aaa
					inner join  CorporateDivisionAccessArea  ca on ca.CorporateDivisionAccessAreaID = aaa.CorporateDivisionAccessAreaID
					inner join CorporateDivision d on ca.DivisionID = d.DivisionID
					inner join AccessArea a on a.AccessAreaID = ca.AccessAreaID
					inner join mp_SecurityGroup sg on sg.Id = aaa.mp_SecurityGroupID
					where sg.RoleName=''Access Recertification'' ) t on t.Division= saar.Division and t.CountryID = divisionCountry.CountryID and t.AccessArea = saar.AccessArea and t.BuildingID = b.BuildingID  and t.dbPeopleID = saar.DBPeopleID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (94, 14, N'Warning', N'Pass Office User already exists', N'select distinct spou.ID, spou.RowNumber  
from XLImportStagePassOfficeUser spou
inner join LocationPassOffice po on po.Name = spou.PassOffice
inner join LocationPassOfficeUser pou on pou.PassOfficeID = po.PassOfficeID and pou.dbPeopleID = spou.DBPeopleID', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (95, 4, N'Error', N'Street Address is mandatory field', N'select ID, RowNumber from XLImportStageBuilding where StreetAddress is null or Len(StreetAddress) <= 0
', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (96, 2, N'Error', N'Pass Office Names should be unique', N';with ct as 
(select ID, Name, RowNumber, ROW_NUMBER() Over(Partition by Name order by RowNumber) as DuplicateRowIndex
from XLImportStagePassOffice
where Name is not null)
select ID, RowNumber from ct where DuplicateRowIndex >= 2  ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (97, 6, N'Error', N'Date Accepted is mandatory field', N'select ID, RowNumber from XLImportStageDivisionRole where DateAccepted is null or len(DateAccepted ) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (98, 9, N'Error', N'Last Certified Date is mandatory field', N'select ID, RowNumber from XLImportStageCorporateArea where LastCertifiedDate is null or len(LastCertifiedDate) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (99, 9, N'Error', N'Date Next Certification is mandatory field', N'select ID, RowNumber from XLImportStageCorporateArea where NextCertifiedDate is null or len(NextCertifiedDate) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (100, 10, N'Error', N'DBPeopleID is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaApprover where DBPeopleID is null or len(DBPeopleID) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (101, 10, N'Error', N'Approver SortOrder is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaApprover where ApproverSortOrder is null or len(ApproverSortOrder) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (103, 10, N'Error', N'Date Accepted is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaApprover where DateAccepted is null or len(DateAccepted) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (104, 13, N'Error', N'DBPeopleID is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where DBPeopleID is null or len(DBPeopleID) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (107, 13, N'Error', N'Date Accepted is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where DateAccepted is null or len(DateAccepted) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (108, 13, N'Error', N'Date Next Certification is mandatory field', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where DateNextCertification is null or len(DateNextCertification) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (110, 14, N'Error', N'Date Accepted is mandatory field', N'select ID, RowNumber from XLImportStagePassOfficeUser where DateAccepted is null or len(DateAccepted) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (111, 14, N'Error', N'DBPeopleID is mandatory field', N'select ID, RowNumber from XLImportStagePassOfficeUser where DBPeopleID is null or len(DBPeopleID) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (112, 10, N'Error', N'DBPeopleID must exist in the system', N'select ID, RowNumber from XLImportStageAccessAreaApprover where DBPeopleID is not null and len(DBPeopleID) > 0 and DBPeopleID not in (select dbPeopleID from HRPerson)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (113, 13, N'Error', N'DBPeopleID must exist in the system', N'select ID, RowNumber from XLImportStageAccessAreaRecertifier where DBPeopleID is not null and len(DBPeopleID) > 0 and DBPeopleID not in (select dbPeopleID from HRPerson)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (114, 14, N'Error', N'DBPeopleID must exist in the system', N'select ID, RowNumber from XLImportStagePassOfficeUser where DBPeopleID is not null and len(DBPeopleID) > 0 and DBPeopleID not in (select dbPeopleID from HRPerson)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (115, 2, N'Error', N'City doesn''t belong to selected Country', N'select ID, RowNumber from XLImportStagePassOffice spo where City is not null and len(City) > 0 and Country is not null and len(Country) > 0 and
City not in (select City from XLImportStageCity sc where sc.Country = spo.Country ) 
or City not in (select City from LocationCity lci inner join LocationCountry lco on lci.CountryID = lco.CountryID where lco.Name = spo.Country  )', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (116, 6, N'Error', N'Same person has been added as Division Administrator for the same Division more than once', N';with ct as 
(select ID, Division, DBPeopleID, SecurityGroup, RowNumber, ROW_NUMBER() Over(Partition by Division, DBPeopleID, SecurityGroup order by RowNumber) as DuplicateRowIndex
from XLImportStageDivisionRole
where Division is not null and DBPeopleID is not null and SecurityGroup is not null and SecurityGroup = ''Division Administrator'')
select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (117, 5, N'Error', N'Parent Division is not selected for Subdivision', N'select ID, RowNumber from XLImportStageDivision where IsSubdivision = ''True'' and (ParentDivision is null or len(ParentDivision) = 0 )', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (118, 5, N'Error', N'Parent Division must be defined in the Division sheet', N'select ID, RowNumber from XLImportStageDivision where  ParentDivision not in (select Name from XLImportStageDivision) and  IsSubdivision = ''True'' and ParentDivision is not null and len(ParentDivision) > 0 ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (120, 5, N'Error', N'Parent Division is selected for a non Subdivision', N'select ID, RowNumber from XLImportStageDivision where IsSubdivision = ''False'' and ParentDivision is not null and len(ParentDivision) > 0 ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (121, 5, N'Error', N'Same Division is added as Parent Division', N'select ID, RowNumber from XLImportStageDivision where ParentDivision is not null and len(ParentDivision) > 0 and ParentDivision = Name ', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (123, 6, N'Error', N'DBPeopleID is mandatory field', N'select ID, RowNumber from XLImportStageDivisionRole where DBPeopleID is null or len(DBPeopleID) = 0', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (124, 5, N'Error', N'UBR must exist in the system', N'select ID, RowNumber from XLImportStageDivision where UBR is not null  and len(UBR) > 0  and UBR not in (select UBR_Code from UBRCode)', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (125, 8, N'Error', N'Length of Floor Name cannot exceed 10 charactors', N'select ID, RowNumber from XLImportStageAccessArea where len(FloorName) > 10', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (126, 8, N'Error', N'Length of Category Description cannot exceed 60 charactors', N'select ID, RowNumber from XLImportStageAccessArea where len(CategoryDescription) > 60', 1)
INSERT [dbo].[XLImportValidationRule] ([ID], [EntityID], [ValidationType], [BusinessRule], [Script], [Enabled]) VALUES (127, 9, N'Error', N'Corporate Area names in the same division must be unique',  N';with ct as (select ID, AccessArea, RowNumber, ROW_NUMBER() Over(Partition by Division, AccessArea order by RowNumber) as DuplicateRowIndex from [XLImportStageCorporateArea] where AccessArea is not null) select ID, RowNumber from ct where DuplicateRowIndex >= 2', 1)
SET IDENTITY_INSERT [dbo].[XLImportValidationRule] OFF


GO


--//@UNDO

TRUNCATE TABLE XLImportValidationRule

GO
