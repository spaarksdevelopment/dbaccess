IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateBatchEmailPerUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[CreateBatchEmailPerUser]
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- History: 
-- 20/05/2015 (Asanka) - Using new View vw_EmailDisabledPassOfficePeople instead of previous(now removed) View UKUSPassofficePeople

-- =============================================
CREATE Procedure [dbo].[CreateBatchEmailPerUser]
@To nVarchar(255)
As
Begin
	
   DECLARE @EmailTypes TABLE
   (
		EmailType int
   )
   
   INSERT INTO @EmailTypes 
   SELECT EmailType 
   FROM [dbo].[EmailLog]
   WHERE EmailType is not null AND [To] = @To
   Group By EmailType
										
	--update the set of emails that are being batched to be in progress
   UPDATE TOP (50) [dbo].[EmailLog]
   SET [InProgress] = 1
   FROM 
				[dbo].[EmailLog] JOIN @EmailTypes temp ON dbo.EmailLog.EmailType = temp.EmailType
		where
				dbo.[EmailLog].[EmailType] is not null
		and		[To] = @To
		and		[DateSent] is null
		and		ISNULL([InProgress], 0) != 1
		and
			(	
				[To] NOT IN (SELECT [EmailAddress] FROM [dbo].[vw_EmailDisabledPassOfficePeople])
				--OR
				--([To]='ryan.sheehan@db.com' AND [dbo].EmailLog.EmailType=36)
			) 
					


--This function calls the above emails that 
--are marked as batched and builds the email per user 
	SELECT 
			ID, 
			[To], 
			[From], 
			[CC], 
			[Subject], 
			[Body]
	 FROM 
			[dbo].[fGetEmailsPerPerson] (@To)
					

End



GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateBatchEmailPerUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[CreateBatchEmailPerUser]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[CreateBatchEmailPerUser]
@To nVarchar(255)
As
Begin
	
   DECLARE @EmailTypes TABLE
   (
		EmailType int
   )
   
   INSERT INTO @EmailTypes 
   SELECT EmailType 
   FROM [dbo].[EmailLog]
   WHERE EmailType is not null AND [To] = @To
   Group By EmailType
										
	--update the set of emails that are being batched to be in progress
   UPDATE TOP (50) [dbo].[EmailLog]
   SET [InProgress] = 1
   FROM 
				[dbo].[EmailLog] JOIN @EmailTypes temp ON dbo.EmailLog.EmailType = temp.EmailType
		where
				dbo.[EmailLog].[EmailType] is not null
		and		[To] = @To
		and		[DateSent] is null
		and		ISNULL([InProgress], 0) != 1
		and
			(	
				[To] NOT IN (SELECT [EmailAddress] FROM [dbo].[UKUSPassofficePeople])
				--OR
				--([To]='ryan.sheehan@db.com' AND [dbo].EmailLog.EmailType=36)
			) 
					


--This function calls the above emails that 
--are marked as batched and builds the email per user 
	SELECT 
			ID, 
			[To], 
			[From], 
			[CC], 
			[Subject], 
			[Body]
	 FROM 
			[dbo].[fGetEmailsPerPerson] (@To)
					

End



GO
