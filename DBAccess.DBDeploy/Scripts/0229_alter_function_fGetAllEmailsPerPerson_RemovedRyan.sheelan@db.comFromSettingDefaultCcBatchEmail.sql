IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAllEmailsPerPerson]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[fGetAllEmailsPerPerson]
END
GO

/****** Object:  UserDefinedFunction [dbo].[fGetAllEmailsPerPerson]    Script Date: 12/8/2016 4:27:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================================================
--History: 
--		 8/12/2016 Wijitha Wijenayak - Removed ryan.sheelan@db.com as the hard corded CC email address
--=============================================================================================

CREATE Function [dbo].[fGetAllEmailsPerPerson]
(
	@To nvarchar(255)
)
Returns @emailLog Table
(
	[EmailLogId] int NOT NULL,
	[To] nvarchar(255) NOT NULL,
	[From] nvarchar(255) NOT NULL,
	[CC] nvarchar(255) NULL,
	[Subject] nvarchar(200) NOT NULL,
	[Body] nvarchar(max) NOT NULL,
	[DateCreated] smalldatetime NOT NULL,
	[DateSent] smalldatetime NULL,
	[Resends] int NOT NULL,
	[BatchGuid] uniqueidentifier NOT NULL,
	[EmailType] int NULL,
	[EntityId] int NULL,
	[InProgress] bit NULL
)
As
Begin
		Insert Into
		@emailLog
		(
			[EmailLogId],
			[To],
			[From],
			[CC],
			[Subject],
			[Body],
			[DateCreated],
			[DateSent],
			[Resends],
			[BatchGuid],
			[EmailType],
			[EntityId],
			[InProgress]		
		)
		SELECT [EmailLogId]
			  ,[To]
			  ,[From]
			  ,'' --completely block all the cc's
			  ,[Subject]
			  ,[Body]
			  ,[DateCreated]
			  ,[DateSent]
			  ,[Resends]
			  ,[BatchGuid]
			  ,[EmailType]
			  ,[EntityId]
			  ,ISNULL([InProgress], 1) as [InProgress]
		FROM 
				[dbo].[EmailLog]
		where
				[EmailType] is not null
		and		[EmailType] IN(Select 
										EmailType 
								FROM 
										[dbo].[EmailLog]
								Where
										EmailType is not null
								and		[To] = @To
								Group By
										EmailType
								)
		and		[To] = @To
		and		[DateSent] is null
		and		ISNULL([InProgress], 0) = 1
		Order BY
				[EmailType]

Return;
End;

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAllEmailsPerPerson]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[fGetAllEmailsPerPerson]
END
GO

/****** Object:  UserDefinedFunction [dbo].[fGetAllEmailsPerPerson]    Script Date: 12/8/2016 4:25:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Function [dbo].[fGetAllEmailsPerPerson]
(
	@To nvarchar(255)
)
Returns @emailLog Table
(
	[EmailLogId] int NOT NULL,
	[To] nvarchar(255) NOT NULL,
	[From] nvarchar(255) NOT NULL,
	[CC] nvarchar(255) NULL,
	[Subject] nvarchar(200) NOT NULL,
	[Body] nvarchar(max) NOT NULL,
	[DateCreated] smalldatetime NOT NULL,
	[DateSent] smalldatetime NULL,
	[Resends] int NOT NULL,
	[BatchGuid] uniqueidentifier NOT NULL,
	[EmailType] int NULL,
	[EntityId] int NULL,
	[InProgress] bit NULL
)
As
Begin
		Insert Into
		@emailLog
		(
			[EmailLogId],
			[To],
			[From],
			[CC],
			[Subject],
			[Body],
			[DateCreated],
			[DateSent],
			[Resends],
			[BatchGuid],
			[EmailType],
			[EntityId],
			[InProgress]		
		)
		SELECT [EmailLogId]
			  ,[To]
			  ,[From]
			  ,CASE [CC] WHEN 'ryan.sheehan@db.com' THEN [CC] ELSE '' END AS [CC] --completely block all the cc's
			  ,[Subject]
			  ,[Body]
			  ,[DateCreated]
			  ,[DateSent]
			  ,[Resends]
			  ,[BatchGuid]
			  ,[EmailType]
			  ,[EntityId]
			  ,ISNULL([InProgress], 1) as [InProgress]
		FROM 
				[dbo].[EmailLog]
		where
				[EmailType] is not null
		and		[EmailType] IN(Select 
										EmailType 
								FROM 
										[dbo].[EmailLog]
								Where
										EmailType is not null
								and		[To] = @To
								Group By
										EmailType
								)
		and		[To] = @To
		and		[DateSent] is null
		and		ISNULL([InProgress], 0) = 1
		Order BY
				[EmailType]
				
	

Return;
End;

GO


