IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAccessAreaPassOfficesPerPerson]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
BEGIN
DROP FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson]
END
GO


/****** Object:  UserDefinedFunction [dbo].[fGetAccessAreaPassOfficesPerPerson]    Script Date: 11/5/2015 6:54:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson] 
(
	@dbPeopleID int
)
RETURNS @passOffices TABLE 
(	
	PassOfficeID int 
)
AS
BEGIN
	
	INSERT INTO @passOffices
	SELECT  IsNUll(bpo.PassOfficeID, cpo.PassOfficeID) AS PassOfficeID
	FROM HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		left outer join LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.[Enabled] = 1
		left outer join LocationPassOffice cpo on cpo.PassOfficeID = c.PassOfficeID and cpo.[Enabled] = 1
	WHERE (p.EndDate is null or p.EndDate > getdate())
		and p.dbPeopleId = @dbPeopleID
		and IsNUll(bpo.PassOfficeID, cpo.PassOfficeID) is not null

	RETURN 
END



GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAccessAreaPassOfficesPerPerson]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
BEGIN
DROP FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson]
END
GO


/****** Object:  UserDefinedFunction [dbo].[fGetAccessAreaPassOfficesPerPerson]    Script Date: 11/5/2015 5:04:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson] 
(
	@dbPeopleID int
)
RETURNS @passOffices TABLE 
(	
	PassOfficeID int 
)
AS
BEGIN
	
	INSERT INTO @passOffices
	SELECT  IsNUll(bpo.PassOfficeID, c.PassOfficeID) AS PassOfficeID
	FROM HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		left outer join LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.[Enabled] = 1
	WHERE (p.EndDate is null or p.EndDate > getdate())
		and p.dbPeopleId = @dbPeopleID

	RETURN 
END

GO


