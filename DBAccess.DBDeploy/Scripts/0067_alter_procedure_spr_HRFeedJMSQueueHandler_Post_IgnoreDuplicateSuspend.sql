IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spr_HRFeedJMSQueueHandler_Post]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[spr_HRFeedJMSQueueHandler_Post]
END
GO


/****** Object:  StoredProcedure [dbo].[spr_HRFeedJMSQueueHandler_Post]    Script Date: 10/15/2015 12:21:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spr_HRFeedJMSQueueHandler_Post]
       @HRID nvarchar(11),
       @TransactionID int,
       @TriggerType nvarchar(4)
AS
       SET NOCOUNT ON;
 
       UPDATE HRFeedMessage
       SET
              Processed = 1,
              RevokeProcessed = 1,
              ReactivateProcessed = 1,
              SuspensionProcessed = 1 
       WHERE
              HRID = @HRID AND
              Processed = 0 AND
              TransactionId <> @TransactionID AND
              TriggerType = @TriggerType
 

GO


--//@UNDO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spr_HRFeedJMSQueueHandler_Post]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[spr_HRFeedJMSQueueHandler_Post]
END
GO

/****** Object:  StoredProcedure [dbo].[spr_HRFeedJMSQueueHandler_Post]    Script Date: 10/15/2015 12:21:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spr_HRFeedJMSQueueHandler_Post]
       @HRID nvarchar(11),
       @TransactionID int,
       @TriggerType nvarchar(4)
AS
       SET NOCOUNT ON;
 
       UPDATE HRFeedMessage
       SET
              Processed = 1,
              RevokeProcessed = 1,
              ReactivateProcessed = 1 
       WHERE
              HRID = @HRID AND
              Processed = 0 AND
              TransactionId <> @TransactionID AND
              TriggerType = @TriggerType
 

GO