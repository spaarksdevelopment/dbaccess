IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessAreaApprover')
BEGIN


CREATE TABLE [dbo].[XLImportStageAccessAreaApprover](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccessArea] [nvarchar](500) NULL,
	[DBPeopleID] [nvarchar](500) NULL,
	[Division] [nvarchar](500) NULL,
	[ApproverSortOrder] [nvarchar](500) NULL,
	[DateAccepted] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportAccessAreaApprover] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[XLImportStageAccessAreaApprover]  WITH CHECK ADD  CONSTRAINT [FK_XLImportAccessAreaApprover_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStageAccessAreaApprover] CHECK CONSTRAINT [FK_XLImportAccessAreaApprover_XLImportFile]


END

GO


--//@UNDO



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessAreaApprover')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageAccessAreaApprover]
	
END

GO