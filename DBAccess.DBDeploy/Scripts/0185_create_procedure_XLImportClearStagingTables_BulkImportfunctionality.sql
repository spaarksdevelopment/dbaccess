IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportClearStagingTables]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportClearStagingTables]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportClearStagingTables]    Script Date: 1/6/2016 5:46:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 21/12/2015
-- Description:	Clear Staging tables.
-- =============================================
CREATE PROCEDURE [dbo].[XLImportClearStagingTables]
AS
BEGIN

--Clear all the staging data
TRUNCATE TABLE XLImportStageCity;
TRUNCATE TABLE XLImportStagePassOffice;
TRUNCATE TABLE XLImportStageBuilding;
TRUNCATE TABLE XLImportStageDivision;
TRUNCATE TABLE XLImportStageDivisionRole;
TRUNCATE TABLE XLImportStageAccessArea;
TRUNCATE TABLE XLImportStageCorporateArea;
TRUNCATE TABLE XLImportStageAccessAreaApprover;
TRUNCATE TABLE XLImportStageAccessAreaRecertifier;
TRUNCATE TABLE XLImportStagePassOfficeUser;

--Clear the error table
TRUNCATE TABLE XLImportValidationError 

--Delete all the unprocessed file records
DELETE XLImportFile WHERE Processed = 0

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportClearStagingTables]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportClearStagingTables]
END
GO