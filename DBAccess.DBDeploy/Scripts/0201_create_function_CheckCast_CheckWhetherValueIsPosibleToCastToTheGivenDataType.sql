IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckCast]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[CheckCast]
END
GO

/****** Object:  UserDefinedFunction [dbo].[CheckCast]    Script Date: 1/12/2016 6:38:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--================================================================
-- Author: Wijitha Wijenayake
-- Created Date: 12/01/2015
-- Description: This functions check whether given value is can be casted in to the given data type.
--================================================================
CREATE FUNCTION [dbo].[CheckCast]
(
	@DataType NVARCHAR(50),
	@Value  NVARCHAR(MAX)
)

RETURNS BIT

AS
BEGIN

DECLARE @CastPossible bit = 0

SET @CastPossible = CASE WHEN @DataType IN ('int','bigint', 'tinyint', 'float', 'decimal', 'smallint', 'money') THEN ISNUMERIC(@Value)
						WHEN @DataType IN ('datetime', 'datetime2', 'date', 'smalldatetime') THEN  ISDATE(@value)
						WHEN @DataType IN ('bit') THEN CASE WHEN @Value IN ('true', 'false', '1', '0') THEN 1 ELSE 0 END  
						ELSE 0 END

RETURN @CastPossible

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckCast]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[CheckCast]
END
GO
