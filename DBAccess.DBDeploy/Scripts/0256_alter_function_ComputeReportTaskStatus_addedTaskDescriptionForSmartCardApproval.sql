IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ComputeReportTaskStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[ComputeReportTaskStatus]
END
GO

/****** Object:  UserDefinedFunction [dbo].[ComputeReportTaskStatus]    Script Date: 09/03/17 19:08:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--========================================================================
-- History: 
--			09/03/2017 (Adheeb) - Set Task type for smart card approval
--========================================================================

CREATE FUNCTION [dbo].[ComputeReportTaskStatus] (@RequestTypeId int, @TaskTypeId int, @TaskTypeName nvarchar(25)) RETURNS varchar(50)

BEGIN

DECLARE @ToReturn varchar(50)
SET @ToReturn = @TaskTypeName

IF @TaskTypeId = 3
BEGIN
	SET @ToReturn = 'Approve Access'
END
ELSE IF (@TaskTypeId = 7)
BEGIN
	SET @ToReturn = 'Add Access'
END
ELSE IF (@TaskTypeId = 12 AND @RequestTypeId = 15)
BEGIN
	SET @ToReturn = 'Offboarding'
END
ELSE IF (@TaskTypeId = 12)
BEGIN
	SET @ToReturn = 'Deactivate Badge'
END
ELSE IF (@TaskTypeId = 1)
BEGIN
	SET @ToReturn = 'Create Badge'
END
ELSE IF (@TaskTypeId = 18)
BEGIN
	SET @ToReturn = 'Smart Card Approval'
END


RETURN @ToReturn

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ComputeReportTaskStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[ComputeReportTaskStatus]
END
GO

/****** Object:  UserDefinedFunction [dbo].[ComputeReportTaskStatus]    Script Date: 09/03/17 19:03:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ComputeReportTaskStatus] (@RequestTypeId int, @TaskTypeId int, @TaskTypeName nvarchar(25)) RETURNS varchar(50)

BEGIN

DECLARE @ToReturn varchar(50)
SET @ToReturn = @TaskTypeName

IF @TaskTypeId = 3
BEGIN
	SET @ToReturn = 'Approve Access'
END
ELSE IF (@TaskTypeId = 7)
BEGIN
	SET @ToReturn = 'Add Access'
END
ELSE IF (@TaskTypeId = 12 AND @RequestTypeId = 15)
BEGIN
	SET @ToReturn = 'Offboarding'
END
ELSE IF (@TaskTypeId = 12)
BEGIN
	SET @ToReturn = 'Deactivate Badge'
END
ELSE IF (@TaskTypeId = 1)
BEGIN
	SET @ToReturn = 'Create Badge'
END



RETURN @ToReturn

END

GO

