
/****** Object:  UserDefinedFunction [dbo].[fGetAccessAreaPassOfficesPerPerson]    Script Date: 6/5/2015 11:17:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 2015/06/05
-- Description:	Get all the assigned pass offices for access areas person has access to.
-- =============================================
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAccessAreaPassOfficesPerPerson]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson]
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAccessAreaPassOfficesPerPerson]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson] 
(
	@dbPeopleID int
)
RETURNS @passOffices TABLE 
(	
	PassOfficeID int 
)
AS
BEGIN
	
	INSERT INTO @passOffices
	SELECT  IsNUll(bpo.PassOfficeID, c.PassOfficeID) AS PassOfficeID
	FROM HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		left outer join LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.[Enabled] = 1
	WHERE (p.EndDate is null or p.EndDate > getdate())
		and p.dbPeopleId = @dbPeopleID

	RETURN 
END
' 
END

GO


--//@UNDO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAccessAreaPassOfficesPerPerson]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[fGetAccessAreaPassOfficesPerPerson]
'
END
GO
