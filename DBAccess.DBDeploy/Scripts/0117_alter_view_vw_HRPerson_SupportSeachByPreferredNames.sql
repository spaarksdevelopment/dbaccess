if exists (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_HRPerson]') )
DROP VIEW [dbo].[vw_HRPerson]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_HRPerson]
AS
SELECT        dbo.HRPerson.dbPeopleID, dbo.HRPerson.IsExternal, CASE dbo.HRPerson.IsExternal WHEN 1 THEN 'C' + CAST(dbo.HRPerson.dbPeopleID AS NVARCHAR(12)) 
                         ELSE CAST(dbo.HRPerson.dbPeopleID AS NVARCHAR(12)) END AS FulldbPeopleID, dbo.HRPerson.EmailAddress, dbo.HRPerson.Forename, dbo.HRPerson.Surname, dbo.HRPerson.PreferredFirstName, 
                         dbo.HRPerson.PreferredLastName, dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname AS FullName, dbo.HRPerson.DBDirID, ISNULL(CAST(dbo.HRPerson.DBDirID AS VARCHAR(10)), N'') AS DBDirString, 
                         dbo.HRPerson.Telephone, dbo.HRPerson.CostCentre, dbo.HRPerson.CostCentreName, dbo.HRPerson.UBR, dbo.UBRCode.UBR_Name, dbo.HRPerson.Enabled, dbo.HRPerson.VendorID, 
                         dbo.HRPerson.VendorName, CASE dbo.HRPerson.IsExternal WHEN 1 THEN dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname + ' (' + IsNull(dbo.HRPerson.EmailAddress, '') 
                         + ')' + ' | ' + IsNull(dbo.HRPerson.PreferredFirstName, '') + ' ' + IsNull(dbo.HRPerson.PreferredLastName, '') + ' | ' + ISNULL(dbo.UBRCode.UBR_Name, '') + ' | UBR_Code:' + ISNUll(dbo.HRPerson.UBR, '') 
                         ELSE dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname + ' (' + IsNull(dbo.HRPerson.EmailAddress, '') + ')' + ' | ' + IsNull(dbo.HRPerson.PreferredFirstName, '') 
                         + ' ' + IsNull(dbo.HRPerson.PreferredLastName, '') + ' | ' + ISNULL(dbo.UBRCode.UBR_Name, '') + ' | UBR_Code:' + ISNUll(dbo.HRPerson.UBR, '') END AS Identifier, ISNULL(dbo.HRPerson.Status, '') AS Status, 
                         dbo.HRPerson.Revoked
FROM            dbo.UBRCode RIGHT OUTER JOIN
                         dbo.HRPerson ON dbo.UBRCode.UBR_Code = dbo.HRPerson.UBR
WHERE        (dbo.HRPerson.CountryName NOT IN ('Denmark'))

GO


--//@UNDO

if exists (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_HRPerson]') )
DROP VIEW [dbo].[vw_HRPerson]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_HRPerson]
AS
SELECT     dbo.HRPerson.dbPeopleID, dbo.HRPerson.IsExternal, CASE dbo.HRPerson.IsExternal WHEN 1 THEN 'C' + CAST(dbo.HRPerson.dbPeopleID AS NVARCHAR(12)) 
                      ELSE CAST(dbo.HRPerson.dbPeopleID AS NVARCHAR(12)) END AS FulldbPeopleID, dbo.HRPerson.EmailAddress, dbo.HRPerson.Forename, dbo.HRPerson.Surname, 
                      dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname AS FullName, dbo.HRPerson.DBDirID, ISNULL(CAST(dbo.HRPerson.DBDirID AS VARCHAR(10)), N'') 
                      AS DBDirString, dbo.HRPerson.Telephone, dbo.HRPerson.CostCentre, dbo.HRPerson.CostCentreName, dbo.HRPerson.UBR, dbo.UBRCode.UBR_Name, 
                      dbo.HRPerson.Enabled, dbo.HRPerson.VendorID, dbo.HRPerson.VendorName, 
                      CASE dbo.HRPerson.IsExternal WHEN 1 THEN dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname + ' (' + IsNull(dbo.HRPerson.EmailAddress, '') 
                      + ')' + ' | ' + ISNULL(dbo.UBRCode.UBR_Name, '') + ' | UBR_Code:' + ISNUll(dbo.HRPerson.UBR, '') 
                      ELSE dbo.HRPerson.Forename + ' ' + dbo.HRPerson.Surname + ' (' + IsNull(dbo.HRPerson.EmailAddress, '') + + ') | ' + ISNULL(dbo.UBRCode.UBR_Name, '') 
                      + ' | UBR_Code:' + ISNUll(dbo.HRPerson.UBR, '') END AS Identifier, ISNULL(dbo.HRPerson.Status, '') AS Status, dbo.HRPerson.Revoked
FROM         dbo.UBRCode RIGHT OUTER JOIN
                      dbo.HRPerson ON dbo.UBRCode.UBR_Code = dbo.HRPerson.UBR
WHERE     (dbo.HRPerson.CountryName NOT IN ('Denmark'))

GO



