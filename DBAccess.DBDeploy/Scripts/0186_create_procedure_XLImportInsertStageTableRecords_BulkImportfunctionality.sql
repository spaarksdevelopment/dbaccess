IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportInsertStageTableRecords]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportInsertStageTableRecords]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Asanka
-- Create date: 18/12/2015
-- Description:	Insert records to staging tables.
-- =============================================

CREATE PROCEDURE [dbo].[XLImportInsertStageTableRecords]
	@Table NVARCHAR(255)
   ,@Columns NVARCHAR(MAX)
   ,@Rows NVARCHAR(MAX)
   ,@Success bit out
AS
BEGIN

	DECLARE @SQLQuery AS NVARCHAR(MAX)

	SET @SQLQuery = 'INSERT INTO '+ @Table + '('+ @Columns +')'+ 'VALUES ' + @Rows

	EXECUTE(@SQLQuery)

	IF @@ERROR>0
		SELECT @Success = 0
	ELSE
		SELECT @Success = 1

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportInsertStageTableRecords]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportInsertStageTableRecords]
END
GO