
if not exists(select * from sys.columns 
            where Name = N'PreferredFirstName' and Object_ID = Object_ID(N'HRPerson'))
begin
	ALTER TABLE [dbo].[HRPerson] ADD PreferredFirstName nvarchar(60) NULL
end

if not exists(select * from sys.columns 
            where Name = N'PreferredLastName' and Object_ID = Object_ID(N'HRPerson'))
begin
	ALTER TABLE [dbo].[HRPerson] ADD PreferredLastName nvarchar(60) NULL
end


GO

--//@UNDO


if exists(select * from sys.columns 
            where Name = N'PreferredFirstName' and Object_ID = Object_ID(N'HRPerson'))
begin
	ALTER TABLE [dbo].[HRPerson] DROP COLUMN PreferredFirstName
end

if exists(select * from sys.columns 
            where Name = N'PreferredLastName' and Object_ID = Object_ID(N'HRPerson'))
begin
	ALTER TABLE [dbo].[HRPerson] DROP COLUMN PreferredLastName
end


GO
