IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRequestTypesForReportSearching]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GetRequestTypesForReportSearching]
END
GO

/****** Object:  StoredProcedure [dbo].[GetRequestTypesForReportSearching]    Script Date: 11/17/2015 4:12:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetRequestTypesForReportSearching]
AS
BEGIN
  SELECT  RequestTypeID
  ,CASE RequestTypeID WHEN 1 then 'Pass' 
					WHEN 2 THEN 'Access' 
					WHEN 15 THEN 'Offboarding' 
					WHEN 16 THEN 'Revokation'
					WHEN 19 THEN 'Suspend'
					WHEN 20 THEN 'Unsuspend' END AS RequestType
  
  FROM dbo.AccessRequestType
  WHERE RequestTypeID IN (1, 2, 15, 16, 19, 20)
END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRequestTypesForReportSearching]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[GetRequestTypesForReportSearching]
END
GO


/****** Object:  StoredProcedure [dbo].[GetRequestTypesForReportSearching]    Script Date: 11/17/2015 4:14:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetRequestTypesForReportSearching]
AS
BEGIN
  SELECT  RequestTypeID
  ,CASE RequestTypeID WHEN 1 then 'Pass' WHEN 2 THEN 'Access' WHEN 15 THEN 'Offboarding' WHEN 16 THEN 'Revokation' END AS RequestType
  
  FROM dbo.AccessRequestType
  WHERE RequestTypeID IN (1, 2, 15, 16)
END


GO

