IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportHistory')
BEGIN


CREATE TABLE [dbo].[XLImportHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileID] [int] NOT NULL,
	[RelatedTable] [nvarchar](500) NOT NULL,
	[RelatedID] [int] NOT NULL,
 CONSTRAINT [PK_XLImportHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[XLImportHistory]  WITH CHECK ADD  CONSTRAINT [FK_XLImportHistory_XLImportHistory] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportHistory] CHECK CONSTRAINT [FK_XLImportHistory_XLImportHistory]

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportHistory')
BEGIN
	
	DROP TABLE [dbo].[XLImportHistory]
	
END

GO