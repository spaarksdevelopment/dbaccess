IF NOT EXISTS(SELECT TaskTypeId FROM dbo.TaskTypeLookup WHERE TaskTypeID=17)
BEGIN
	INSERT INTO [dbo].[TaskTypeLookup]
           ([TaskTypeId]
           ,[Type]
           ,[Description]
           ,[EmailTemplateId]
           ,[RoleId])
     VALUES
           (17
           ,'UnsuspendAccess'
           ,'Unsuspend Access for {6} pass for {1}{5}'
           ,NULL
           ,NULL)
END
GO

IF NOT EXISTS(SELECT RequestTypeID FROM dbo.AccessRequestType WHERE RequestTypeID=20)
BEGIN
	INSERT INTO [dbo].[AccessRequestType]
			   ([RequestTypeID]
			   ,[Name]
			   ,[Description]
			   ,[Enabled]
			   ,[TaskTypeID]
			   ,[GermanName]
			   ,[ItalyName])
		 VALUES
			   (
				   20,
				   'Unsuspend Access',
				   'Unsuspend Access for a User',
				   1,
				   17,
				   'Entsperren Zugang',
				   'Accesso Riattivare'
			   )
END
GO



--//@UNDO

IF EXISTS(SELECT TaskTypeId FROM dbo.TaskTypeLookup WHERE TaskTypeID=17)
	DELETE [dbo].[TaskTypeLookup] WHERE TaskTypeID=17
GO

IF EXISTS(SELECT RequestTypeID FROM dbo.AccessRequestType WHERE RequestTypeID=20)
	DELETE dbo.AccessRequestType WHERE RequestTypeID=20
GO