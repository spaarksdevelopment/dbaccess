IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessAreaApprovers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessAreaApprovers]
END
GO

--===============================================================
-- Author: Asanka
-- Created Date: 22/12/2015
-- Description: Copy access area approvers from staging table to the CorporateDivisionAccessAreaApprover table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyAccessAreaApprovers]
@FileID INT
AS
BEGIN

	DECLARE @ValidFile BIT = 0, @ProcessedFile BIT=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID INT)

	INSERT INTO CorporateDivisionAccessAreaApprover (CorporateDivisionAccessAreaID, SortOrder, Enabled, DateAccepted, RecertificationEmails, IsExternal, dbPeopleID, mp_SecurityGroupID)
	OUTPUT inserted.CorporateDivisionAccessAreaApproverId INTO @tblIDs
	SELECT CorporateDivisionAccessAreaID,			
		ApproverSortOrder,
		1,
		CAST(DateAccepted AS smalldatetime),
		0,
		0,
		CAST(DBPeopleID AS INT),
		(SELECT Id  FROM mp_SecurityGroup where RoleShortName = 'AA')
	FROM XLImportStageAccessAreaApprover saaa
		INNER JOIN XLImportStageCorporateArea sca ON saaa.Division = sca.Division AND saaa.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd ON sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry ON divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa ON saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb ON saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc ON sb.City = sc.Name
		INNER JOIN LocationCountry c ON c.Name = sc.Country
		INNER JOIN LocationCity city ON city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b ON b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a ON a.BuildingID = b.BuildingID AND a.Name = saaa.AccessArea
		INNER JOIN CorporateDivision d ON d.Name = saaa.Division AND d.CountryID = divisionCountry.CountryID
		INNER JOIN CorporateDivisionAccessArea CDAA ON d.DivisionID=CDAA.DivisionID AND a.AccessAreaID=CDAA.AccessAreaID
	WHERE ISNULL(saaa.ValidationFailed, 0) = 0 AND saaa.FileID = @FileID

	INSERT INTO mp_SecurityGroupUser(SecurityGroupID, dbPeopleID)
	SELECT DISTINCT 5 /*Access Approver*/ AS SecurityGroupID, dbPeopleID
	FROM [dbo].[XLImportStageAccessAreaApprover]



	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionAccessAreaApprover', ID FROM @tblIDs

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAccessAreaApprovers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAccessAreaApprovers]
END
GO

--===============================================================
-- Author: Asanka
-- Created Date: 22/12/2015
-- Description: Copy access area approvers from staging table to the CorporateDivisionAccessAreaApprover table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyAccessAreaApprovers]
@FileID INT
AS
BEGIN

	DECLARE @ValidFile BIT = 0, @ProcessedFile BIT=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID INT)

	INSERT INTO CorporateDivisionAccessAreaApprover (CorporateDivisionAccessAreaID, SortOrder, Enabled, DateAccepted, RecertificationEmails, IsExternal, dbPeopleID, mp_SecurityGroupID)
	OUTPUT inserted.CorporateDivisionAccessAreaApproverId INTO @tblIDs
	SELECT CorporateDivisionAccessAreaID,			
		ApproverSortOrder,
		1,
		CAST(DateAccepted AS smalldatetime),
		0,
		0,
		CAST(DBPeopleID AS INT),
		(SELECT Id  FROM mp_SecurityGroup where RoleShortName = 'AA')
	FROM XLImportStageAccessAreaApprover saaa
		INNER JOIN XLImportStageCorporateArea sca ON saaa.Division = sca.Division AND saaa.AccessArea = sca.AccessArea
		INNER JOIN XLImportStageDivision sd ON sca.Division = sd.Name
		INNER JOIN LocationCountry divisionCountry ON divisionCountry.Name = sd.Country
		INNER JOIN XLImportStageAccessArea saa ON saa.Name = sca.AccessArea
		INNER JOIN XLImportStageBuilding sb ON saa.Building = sb.Name
		INNER JOIN XLImportStageCity sc ON sb.City = sc.Name
		INNER JOIN LocationCountry c ON c.Name = sc.Country
		INNER JOIN LocationCity city ON city.Name = sc.Name AND city.CountryID = c.CountryID
		INNER JOIN LocationBuilding b ON b.Name = sb.Name AND b.CityID = city.CityID
		INNER JOIN AccessArea a ON a.BuildingID = b.BuildingID AND a.Name = saaa.AccessArea
		INNER JOIN CorporateDivision d ON d.Name = saaa.Division AND d.CountryID = divisionCountry.CountryID
		INNER JOIN CorporateDivisionAccessArea CDAA ON d.DivisionID=CDAA.DivisionID AND a.AccessAreaID=CDAA.AccessAreaID
	WHERE ISNULL(saaa.ValidationFailed, 0) = 0 AND saaa.FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionAccessAreaApprover', ID FROM @tblIDs

END
GO