if(Not Exists (select * from AdminValues where [Key] = 'AccessAreaNumberOfApprovalsWarningThreshold'))
begin
	insert into AdminValues ([Key], Value)
	values ('AccessAreaNumberOfApprovalsWarningThreshold', 2)
end

--//@UNDO

delete from AdminValues 
where [Key] = 'AccessAreaNumberOfApprovalsWarningThreshold'
