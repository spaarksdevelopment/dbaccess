IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PerformAdminSearch]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[sp_PerformAdminSearch]
END
GO


/****** Object:  StoredProcedure [dbo].[sp_PerformAdminSearch]    Script Date: 11/17/2015 4:30:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- EXEC sp_PerformAdminSearch '23-AUG-2014', '29-AUG-2014', NULL, NULL, 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
/* 
EXEC sp_PerformAdminSearch_V3 NULL, NULL, '13-SEP-2014', '19-SEP-2014', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
EXEC sp_PerformAdminSearch NULL, NULL, '13-SEP-2014', '19-SEP-2014', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
*/ 
CREATE PROCEDURE [dbo].[sp_PerformAdminSearch] 
@RevokeAccessStartDate datetime = NULL, 
@RevokeAccessEndDate datetime = NULL, 
@TaskCreatedStartDate datetime = NULL, 
@TaskCreatedEndDate datetime = NULL, 
@PersonNameSearchType nvarchar(50) = 'Contains', 
@PersonNameValueStart nvarchar(150) = '', 
@PersonNameValueEnd nvarchar(150) = '', 
@PersonEmpIdSearchType nvarchar(50) = 'Contains', 
@PersonEmpIdValueStart nvarchar(150) = '', 
@PersonEmpIdValueEnd nvarchar(150) = '', 
@PersonEmailSearchType nvarchar(50) = 'Contains', 
@PersonEmailValueStart nvarchar(150) = '', 
@PersonEmailalueEnd nvarchar(150) = '', 
@PersonDbDirIdSearchType nvarchar(50) = 'Contains', 
@PersonDbDirIdValueStart nvarchar(150) = '', 
@PersonDbDirIdValueEnd nvarchar(150) = '', 
@TaskCompletedStartDate datetime = NULL, 
@TaskCompletedEndDate datetime = NULL, 
@PassOffice nvarchar(250) = '', 
@RequestStatusId int = NULL, 
@CountryId int = NULL 

AS 
    
  IF(@TaskCreatedEndDate IS NOT NULL) 
  BEGIN 
        SET @TaskCreatedEndDate = DATEADD(second, 86399, @TaskCreatedEndDate); 
  END 
  
  IF(@TaskCompletedEndDate IS NOT NULL) 
  BEGIN 
        SET @TaskCompletedEndDate = DATEADD(second, 86399, @TaskCompletedEndDate); 
  END 
  
  IF(@RevokeAccessEndDate IS NOT NULL) 
  BEGIN 
        SET @RevokeAccessEndDate = DATEADD(second, 86399, @RevokeAccessEndDate); 
  END 
  
SELECT 
        NULL As RevokeAccessDate, --HRFeedMessage.RevokeAccessDateTimestamp AS RevokeAccessDate, 
        Tasks.TaskId, 
        Tasks.CreatedDate, 
        AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname AS [PersonName], 
        AccessRequestPerson.dbPeopleId as [EmployeeId], 
        AccessRequestPerson.EmailAddress, 
        AccessRequestType.[Description] As RequestType, 
        NULL AS DBDirId, 
        Tasks.CompletedDate, 
        AccessRequest.PassOfficeId, 
        (SELECT [Name] FROM LocationPassOffice WHERE PassOfficeID = AccessRequest.PassOfficeId) AS PassOfficeName, 
        Tasks.TaskStatusID AS RequestStatusId, 
        (SELECT [Status] FROM TaskStatusLookup WHERE TaskStatusId =  Tasks.TaskStatusID) AS RequestName 
FROM (SELECT * FROM Tasks UNION SELECT * FROM TasksArchive) Tasks 
INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId 
INNER JOIN AccessRequestType ON AccessRequest.RequestTypeID = AccessRequestType.RequestTypeID 
INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID 
LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID = vw_AccessArea.AccessAreaID 
WHERE 
AccessRequest.RequestTypeID IN (15, 16, 19, 20) AND 
( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE))) 
AND 
( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) 
AND 
( (@TaskCompletedStartDate IS NULL) OR (@TaskCompletedStartDate IS NOT NULL AND CompletedDate >= CAST(@TaskCompletedStartDate AS DATE))) 
AND 
( (@TaskCompletedEndDate IS NULL) OR (@TaskCompletedEndDate IS NOT NULL AND CompletedDate <= CAST(@TaskCompletedEndDate AS DATE))) 
AND 
( 
        (@CountryId IS NULL) OR (@CountryId IS NOT NULL AND vw_AccessArea.CountryID = @CountryId) 
) 
AND 
( (@PassOffice = '') OR (@PassOffice <> '' AND 
CHARINDEX( 
        ',' + CAST(AccessRequest.PassOfficeId AS NVARCHAR(10)) + ',', 
        ',' + @PassOffice + ',' 
        ) > 0 
)) 
AND 
( (@RequestStatusId IS NULL) OR (@RequestStatusId IS NOT NULL AND Tasks.TaskStatusID = @RequestStatusId)) 
AND 
( 
        (@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) ) 
        OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) ) 
        OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) = (@PersonNameValueStart)) ) ) 
        OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) = (@PersonNameValueStart)) ) ) 
        OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
                        OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) ) 
) 
AND 
( 
        (@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) ) 
        OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) LIKE (@PersonEmpIdValueStart + '%')) ) ) 
        OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) ) 
        OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) ) 
        OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
                        OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) ) 
) 
AND 
( 
        (@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) ) 
        OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) ) 
        OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) = (@PersonEmailValueStart)) ) ) 
        OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  AccessRequestPerson.EmailAddress) = (@PersonEmailValueStart)) ) ) 
        OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
                        OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( AccessRequestPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) ) 
) 
AND 
( 
        (@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) ) 
        OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) LIKE (@PersonDbDirIdValueStart + '%')) ) ) 
        OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) = (@PersonDbDirIdValueStart)) ) ) 
        OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( 0) = (@PersonDbDirIdValueStart)) ) ) 
        OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
                        OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( 0) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) ) 
) 
ORDER BY Tasks.TaskId 
GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PerformAdminSearch]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[sp_PerformAdminSearch]
END
GO

/****** Object:  StoredProcedure [dbo].[]    Script Date: 11/17/2015 4:25:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- EXEC sp_PerformAdminSearch '23-AUG-2014', '29-AUG-2014', NULL, NULL, 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
/* 
EXEC sp_PerformAdminSearch_V3 NULL, NULL, '13-SEP-2014', '19-SEP-2014', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
EXEC sp_PerformAdminSearch NULL, NULL, '13-SEP-2014', '19-SEP-2014', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', 'Contains', '', '', NULL, NULL, '', NULL, NULL 
*/ 
CREATE PROCEDURE [dbo].[sp_PerformAdminSearch] 
@RevokeAccessStartDate datetime = NULL, 
@RevokeAccessEndDate datetime = NULL, 
@TaskCreatedStartDate datetime = NULL, 
@TaskCreatedEndDate datetime = NULL, 
@PersonNameSearchType nvarchar(50) = 'Contains', 
@PersonNameValueStart nvarchar(150) = '', 
@PersonNameValueEnd nvarchar(150) = '', 
@PersonEmpIdSearchType nvarchar(50) = 'Contains', 
@PersonEmpIdValueStart nvarchar(150) = '', 
@PersonEmpIdValueEnd nvarchar(150) = '', 
@PersonEmailSearchType nvarchar(50) = 'Contains', 
@PersonEmailValueStart nvarchar(150) = '', 
@PersonEmailalueEnd nvarchar(150) = '', 
@PersonDbDirIdSearchType nvarchar(50) = 'Contains', 
@PersonDbDirIdValueStart nvarchar(150) = '', 
@PersonDbDirIdValueEnd nvarchar(150) = '', 
@TaskCompletedStartDate datetime = NULL, 
@TaskCompletedEndDate datetime = NULL, 
@PassOffice nvarchar(250) = '', 
@RequestStatusId int = NULL, 
@CountryId int = NULL 

AS 
    
  IF(@TaskCreatedEndDate IS NOT NULL) 
  BEGIN 
        SET @TaskCreatedEndDate = DATEADD(second, 86399, @TaskCreatedEndDate); 
  END 
  
  IF(@TaskCompletedEndDate IS NOT NULL) 
  BEGIN 
        SET @TaskCompletedEndDate = DATEADD(second, 86399, @TaskCompletedEndDate); 
  END 
  
  IF(@RevokeAccessEndDate IS NOT NULL) 
  BEGIN 
        SET @RevokeAccessEndDate = DATEADD(second, 86399, @RevokeAccessEndDate); 
  END 
  
SELECT 
        NULL As RevokeAccessDate, --HRFeedMessage.RevokeAccessDateTimestamp AS RevokeAccessDate, 
        Tasks.TaskId, 
        Tasks.CreatedDate, 
        AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname AS [PersonName], 
        AccessRequestPerson.dbPeopleId as [EmployeeId], 
        AccessRequestPerson.EmailAddress, 
        AccessRequestType.[Description] As RequestType, 
        NULL AS DBDirId, 
        Tasks.CompletedDate, 
        AccessRequest.PassOfficeId, 
        (SELECT [Name] FROM LocationPassOffice WHERE PassOfficeID = AccessRequest.PassOfficeId) AS PassOfficeName, 
        Tasks.TaskStatusID AS RequestStatusId, 
        (SELECT [Status] FROM TaskStatusLookup WHERE TaskStatusId =  Tasks.TaskStatusID) AS RequestName 
FROM (SELECT * FROM Tasks UNION SELECT * FROM TasksArchive) Tasks 
INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId 
INNER JOIN AccessRequestType ON AccessRequest.RequestTypeID = AccessRequestType.RequestTypeID 
INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID 
LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID = vw_AccessArea.AccessAreaID 
WHERE 
AccessRequest.RequestTypeID IN (15, 16) AND 
( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE))) 
AND 
( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) 
AND 
( (@TaskCompletedStartDate IS NULL) OR (@TaskCompletedStartDate IS NOT NULL AND CompletedDate >= CAST(@TaskCompletedStartDate AS DATE))) 
AND 
( (@TaskCompletedEndDate IS NULL) OR (@TaskCompletedEndDate IS NOT NULL AND CompletedDate <= CAST(@TaskCompletedEndDate AS DATE))) 
AND 
( 
        (@CountryId IS NULL) OR (@CountryId IS NOT NULL AND vw_AccessArea.CountryID = @CountryId) 
) 
AND 
( (@PassOffice = '') OR (@PassOffice <> '' AND 
CHARINDEX( 
        ',' + CAST(AccessRequest.PassOfficeId AS NVARCHAR(10)) + ',', 
        ',' + @PassOffice + ',' 
        ) > 0 
)) 
AND 
( (@RequestStatusId IS NULL) OR (@RequestStatusId IS NOT NULL AND Tasks.TaskStatusID = @RequestStatusId)) 
AND 
( 
        (@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) ) 
        OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) ) 
        OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) = (@PersonNameValueStart)) ) ) 
        OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) = (@PersonNameValueStart)) ) ) 
        OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
                        OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) ) 
) 
AND 
( 
        (@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) ) 
        OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) LIKE (@PersonEmpIdValueStart + '%')) ) ) 
        OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) ) 
        OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) ) 
        OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
                        OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(AccessRequestPerson.dbPeopleId AS nvarchar(150)) ) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) ) 
) 
AND 
( 
        (@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) ) 
        OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) ) 
        OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  AccessRequestPerson.EmailAddress) = (@PersonEmailValueStart)) ) ) 
        OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  AccessRequestPerson.EmailAddress) = (@PersonEmailValueStart)) ) ) 
        OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
                        OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( AccessRequestPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) ) 
) 
AND 
( 
        (@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) ) 
        OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) LIKE (@PersonDbDirIdValueStart + '%')) ) ) 
        OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( 0) = (@PersonDbDirIdValueStart)) ) ) 
        OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( 0) = (@PersonDbDirIdValueStart)) ) ) 
        OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
                        OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( 0) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) ) 
) 
ORDER BY Tasks.TaskId 
GO


