IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArchiveEmailLog]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[ArchiveEmailLog]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- History: 
-- 20/05/2015 (Asanka) - Using new View vw_EmailDisabledPassOfficePeople instead of previous(now removed) View UKUSPassofficePeople

-- =============================================
CREATE  PROCEDURE  [dbo].[ArchiveEmailLog] 
AS 
BEGIN 
    BEGIN 
		INSERT into dbo.EmailLogHistory
		SELECT * from dbo.EmailLog WHERE ([InProgress]=1 AND [DateSent] IS NOT NULL)

		INSERT into dbo.EmailLogHistory
		SELECT * FROM dbo.EmailLog WHERE  [To]  IN (
				SELECT [EmailAddress] FROM [dbo].[vw_EmailDisabledPassOfficePeople] --WHERE [EmailAddress] <> 'ryan.sheehan@db.com'
				)

		--INSERT into dbo.EmailLogHistory
		--SELECT * FROM dbo.EmailLog WHERE [To] = 'ryan.sheehan@db.com' AND NOT EmailType = 36

		INSERT into dbo.EmailLogHistory
		SELECT * FROM dbo.EmailLog WHERE DateCreated <dateadd(dd,-30,getdate())

		--Archive anyone who is not active. Their email address no longer works
		INSERT into dbo.EmailLogHistory
		SELECT TOP 10000 e.* FROM dbo.EmailLog e JOIN dbo.HRPerson h ON e.[To]=h.EmailAddress
		WHERE h.[Status] not in ('A','P','L') 

		INSERT into dbo.EmailLogHistory
		SELECT TOP 10000 e.* 
		FROM dbo.EmailLog e
		WHERE e.[To] =''
		
		--	for the subset, we need to work out which are in the americas		
		INSERT into dbo.EmailLogHistory
		select EmailLog.*  FROM EmailLog 
		INNER JOIN hrperson hrp ON EmailLog.[To] = hrp.EmailAddress
		INNER JOIN LocationCountry lc ON hrp.CountryName = lc.Name
		WHERE EmailLog.EmailType IN (2,16,25) AND lc.RegionID = 10

		--	Added to catch the CC field
		update dbo.EmailLogHistory set [cc] = '' where EmailLogId in (
		select EmailLog.EmailLogId  FROM EmailLog 
		INNER JOIN hrperson hrp ON EmailLog.[CC] = hrp.EmailAddress
		INNER JOIN LocationCountry lc ON hrp.CountryName = lc.Name
		WHERE EmailLog.EmailType IN (2,16,25) AND lc.RegionID = 10)

	 END
	 -- NOTE: UNion is better that using WHERE ([InProgress]=1 AND [DateSent] IS NOT NULL OR [To]  IN (SELECT [EmailAddress] FROM [dbo].[UKUSPassofficePeople]) OR      DateCreated >dateadd(dd,-30,getdate())
	 
       IF @@error=0
        BEGIN
			DELETE dbo.EmailLog 
			FROM EmailLog INNER JOIN EmailLogHistory
			ON EmailLog.EmailLogId=EmailLogHistory.EmailLogId
			
        END
        
END



GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArchiveEmailLog]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[ArchiveEmailLog]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE  [dbo].[ArchiveEmailLog] 
AS 
BEGIN 
    BEGIN 
		INSERT into dbo.EmailLogHistory
		SELECT * from dbo.EmailLog WHERE ([InProgress]=1 AND [DateSent] IS NOT NULL)

		INSERT into dbo.EmailLogHistory
		SELECT * FROM dbo.EmailLog WHERE  [To]  IN (
				SELECT [EmailAddress] FROM [dbo].[UKUSPassofficePeople] --WHERE [EmailAddress] <> 'ryan.sheehan@db.com'
				)

		--INSERT into dbo.EmailLogHistory
		--SELECT * FROM dbo.EmailLog WHERE [To] = 'ryan.sheehan@db.com' AND NOT EmailType = 36

		INSERT into dbo.EmailLogHistory
		SELECT * FROM dbo.EmailLog WHERE DateCreated <dateadd(dd,-30,getdate())

		--Archive anyone who is not active. Their email address no longer works
		INSERT into dbo.EmailLogHistory
		SELECT TOP 10000 e.* FROM dbo.EmailLog e JOIN dbo.HRPerson h ON e.[To]=h.EmailAddress
		WHERE h.[Status] not in ('A','P','L') 

		INSERT into dbo.EmailLogHistory
		SELECT TOP 10000 e.* 
		FROM dbo.EmailLog e
		WHERE e.[To] =''
		
		--	for the subset, we need to work out which are in the americas		
		INSERT into dbo.EmailLogHistory
		select EmailLog.*  FROM EmailLog 
		INNER JOIN hrperson hrp ON EmailLog.[To] = hrp.EmailAddress
		INNER JOIN LocationCountry lc ON hrp.CountryName = lc.Name
		WHERE EmailLog.EmailType IN (2,16,25) AND lc.RegionID = 10

		--	Added to catch the CC field
		update dbo.EmailLogHistory set [cc] = '' where EmailLogId in (
		select EmailLog.EmailLogId  FROM EmailLog 
		INNER JOIN hrperson hrp ON EmailLog.[CC] = hrp.EmailAddress
		INNER JOIN LocationCountry lc ON hrp.CountryName = lc.Name
		WHERE EmailLog.EmailType IN (2,16,25) AND lc.RegionID = 10)

	 END
	 -- NOTE: UNion is better that using WHERE ([InProgress]=1 AND [DateSent] IS NOT NULL OR [To]  IN (SELECT [EmailAddress] FROM [dbo].[UKUSPassofficePeople]) OR      DateCreated >dateadd(dd,-30,getdate())
	 
       IF @@error=0
        BEGIN
			DELETE dbo.EmailLog 
			FROM EmailLog INNER JOIN EmailLogHistory
			ON EmailLog.EmailLogId=EmailLogHistory.EmailLogId
			
        END
        
END



GO