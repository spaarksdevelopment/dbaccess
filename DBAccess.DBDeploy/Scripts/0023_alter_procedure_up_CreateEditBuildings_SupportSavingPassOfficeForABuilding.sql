IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateEditBuildings]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateEditBuildings]
END
GO



/****** Object:  StoredProcedure [dbo].[up_CreateEditBuildings]    Script Date: 5/18/2015 3:52:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/***Create admin procedure for location buildings***/
/***Author E.Parker April 2011***/

CREATE PROCEDURE [dbo].[up_CreateEditBuildings] 
	@BuildingID int,
	@Name varchar(255),
	@Enabled bit,
	@IsNew bit,
	@CityID int,
	@StreetAddress varchar(max),
	@LandlordID int,
	@VisitorID int,
	@DBMovesLocationID int = null,
	@PassOfficeID int = null,
	@Result INT OUT
AS

SET NOCOUNT ON;

IF @IsNew = 'True'
--new row
	insert into dbo.LocationBuilding (CityID,Name,StreetAddress,Enabled, LandlordID, VisitorControlSystem, DBMovesLocationID, PassOfficeID)
	values (@CityID, @Name, @StreetAddress, @Enabled, @LandlordID, @VisitorID, @DBMovesLocationID, @PassOfficeID)
ELSE
	update dbo.LocationBuilding
	Set CityID = @CityID,
	Name = @Name,
	StreetAddress = @StreetAddress,
	Enabled= @Enabled,
	LandlordID = @LandlordID,
	VisitorControlSystem = @VisitorID,
	DBMovesLocationID = @DBMovesLocationID,
	PassOfficeID = @PassOfficeID
	Where BuildingID  = @BuildingID




IF @@ERROR>0
		SELECT @Result = 0
	Else
		SELECT @Result = 1

GRANT EXECUTE ON [up_CreateEditBuildings] TO dbAccessUser

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateEditBuildings]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateEditBuildings]
END
GO

/****** Object:  StoredProcedure [dbo].[up_CreateEditBuildings]    Script Date: 5/18/2015 3:52:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/***Create admin procedure for location buildings***/
/***Author E.Parker April 2011***/

CREATE PROCEDURE [dbo].[up_CreateEditBuildings] 
	@BuildingID int,
	@Name varchar(255),
	@Enabled bit,
	@IsNew bit,
	@CityID int,
	@StreetAddress varchar(max),
	@LandlordID int,
	@VisitorID int,
	@DBMovesLocationID int = null,
	@Result INT OUT
AS

SET NOCOUNT ON;

IF @IsNew = 'True'
--new row
	insert into dbo.LocationBuilding (CityID,Name,StreetAddress,Enabled, LandlordID, VisitorControlSystem, DBMovesLocationID)
	values (@CityID, @Name, @StreetAddress, @Enabled, @LandlordID, @VisitorID, @DBMovesLocationID)
ELSE
	update dbo.LocationBuilding
	Set CityID = @CityID,
	Name = @Name,
	StreetAddress = @StreetAddress,
	Enabled= @Enabled,
	LandlordID = @LandlordID,
	VisitorControlSystem = @VisitorID,
	DBMovesLocationID = @DBMovesLocationID
	Where BuildingID  = @BuildingID




IF @@ERROR>0
		SELECT @Result = 0
	Else
		SELECT @Result = 1

GRANT EXECUTE ON [up_CreateEditBuildings] TO dbAccessUser

GO


