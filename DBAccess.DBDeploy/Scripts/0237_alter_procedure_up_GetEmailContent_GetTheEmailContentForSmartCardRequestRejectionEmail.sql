IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_GetEmailContent]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_GetEmailContent]
END
GO

/****** Object:  StoredProcedure [dbo].[up_GetEmailContent]    Script Date: 2/28/2017 6:56:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		AC
-- Create date: 21/03/2011
-- Description:	Gets email subject, content and the 'to' email address. 
-- History: 
--			28/02/2017 (Wijitha) Modified SP to get the content for Smart Card Rejection Email
-- =============================================
CREATE PROCEDURE [dbo].[up_GetEmailContent] 
(
	@TemplateTypeId int,
	@EntityId int,
	@dbPeopleID int,
	@HttpRoot nvarchar(100),
	@Comment  nvarchar(500) out,
	@Content nvarchar(1000) out,
	@Subject nvarchar(200) out,
	@ToAddress nvarchar(200) out,
	@CCAddress nvarchar(200) out
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @RecipientId int
	DECLARE @Recipient nvarchar(100)
	DECLARE @UserName nvarchar(100)
	DECLARE @BusinessAreaID INT
	DECLARE @BusinessArea nvarchar(100)
	DECLARE @CorporateDivisionAccessAreaId INT	
	DECLARE @AccessAreaID int
	DECLARE @AccessArea nvarchar(500)
	DECLARE @TaskStatusId INT
	DECLARE @TaskStatus nvarchar(15)
	DECLARE @RequestId int
	Declare @sPassOffice NVarChar(250);
	Declare @sDivision NVarChar(255);
	
	SET @UserName = (SELECT Forename + ' ' + Surname FROM dbo.HRPerson WHERE dbPeopleID = @dbPeopleID);
	SET @Content = (SELECT [Body] FROM EmailTemplates WHERE [Type] = @TemplateTypeId)
	SELECT @Subject = (SELECT [Subject] FROM EmailTemplates WHERE [Type] = @TemplateTypeId)
	
	IF (@TemplateTypeId = 11) --  request submitted
	BEGIN
		SET @RecipientId = @dbPeopleID
		
	END
	ELSE IF (@TemplateTypeId = 3)  -- Business Area Approver request from DA to user
	BEGIN

		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = (SELECT BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId))
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);
		
	END
	ELSE IF (@TemplateTypeId = 4) -- Business Area Approver response from User to DA
	BEGIN
				
		SELECT @TaskStatusId = TaskStatusId, @BusinessAreaID = BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId
		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE [BusinessAreaID] = @BusinessAreaID)
		SET @RecipientId = (SELECT CreatedByID FROM Tasks WHERE TaskId = @EntityId)
	END		
	ELSE IF (@TemplateTypeId = 6) OR (@TemplateTypeId = 18) -- Access Area Approver / Recertifier request from DA to user
	BEGIN		
		SELECT @AccessAreaID = AccessAreaID FROM [Tasks] WHERE TaskId = @EntityId
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);		
	END
	ELSE IF (@TemplateTypeId = 7)  -- Business Area Approver removal to user
	BEGIN
		SELECT @RecipientId=dbPeopleID, @BusinessAreaId=BusinessAreaId FROM CorporateBusinessAreaApprover WHERE CorporateBusinessAreaApproverId = @EntityId 
		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = @BusinessAreaId)	
	END
	ELSE IF (@TemplateTypeId = 8) OR (@TemplateTypeId = 17) -- Access Area Approver / Recertifier removal to user
	BEGIN
		SELECT @RecipientId=dbPeopleID, @CorporateDivisionAccessAreaId=CorporateDivisionAccessAreaId FROM CorporateDivisionAccessAreaApprover WHERE CorporateDivisionAccessAreaApproverId = @EntityId 
		SET @AccessAreaID = (SELECT AccessAreaID FROM vw_DivisionAccessArea WHERE CorporateDivisionAccessAreaId = @CorporateDivisionAccessAreaId)
		
	END
	ELSE IF (@TemplateTypeId = 10) -- Access Area Approval recertification due in 5 days
	BEGIN
		SELECT @RecipientId=dbPeopleID, @CorporateDivisionAccessAreaId=CorporateDivisionAccessAreaId FROM CorporateDivisionAccessAreaApprover WHERE CorporateDivisionAccessAreaApproverId = @EntityId 
		--execute the stored procedure to get the AccessArea
		EXEC @AccessArea = [dbo].[up_GetJoinedAccessAreasNames] @INN_UserID = @RecipientId				
	END
	ELSE IF (@TemplateTypeId = 12) OR (@TemplateTypeId = 13) -- Access Area Approver or Recertifier Response
	BEGIN
		
		SELECT @RecipientId = CreatedByID, @TaskStatusId = TaskStatusId, @AccessAreaID = AccessAreaId FROM Tasks WHERE TaskId = @EntityId
				
	END
	ELSE IF (@TemplateTypeId = 14) OR (@TemplateTypeId = 15) -- Request Approval or Access Approval
	BEGIN
		SELECT @TaskStatusId=TaskStatusId, @AccessAreaID = AccessAreaId, @RequestId = RequestId FROM [Tasks] WHERE TaskId = @EntityId		
		SELECT @RecipientId=CreatedByID, @CCAddress=EmailAddress FROM vw_AccessRequest WHERE RequestId = @RequestId
	END
	ELSE IF (@TemplateTypeId = 16) -- New Pass Complete
	BEGIN
		SET @TaskStatus = (SELECT [Status] FROM TaskStatusLookup WHERE [TaskStatusId] = (SELECT TaskStatusId FROM [Tasks] WHERE TaskId = @EntityId))
		SELECT @RecipientId = CreatedByID, @RequestId = RequestId FROM Tasks WHERE TaskId = @EntityId
		SELECT @CCAddress = EmailAddress FROM vw_BadgeRequest WHERE RequestID = @RequestId
		SET @Comment = NULL
	END
	ELSE IF (@TemplateTypeId = 23)  -- Division Administator Add to user
	BEGIN

		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = (SELECT BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId));
		If (@BusinessArea Is Null) Set @BusinessArea = (SELECT Name FROM CorporateDivision WHERE DivisionID = (SELECT DivisionId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);
		
	END
	ELSE IF (@TemplateTypeId = 26)  -- Pass Office Add to user
	BEGIN

		SET @sPassOffice = (SELECT Name FROM LocationPassOffice WHERE PassOfficeID = (SELECT PassOfficeId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);

	END
	ELSE IF (@TemplateTypeId = 27)  -- Division Owner Add to user
	BEGIN

		SET @sDivision = (SELECT Name FROM CorporateDivision WHERE DivisionID = (SELECT DivisionId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);

	END

	ELSE IF (@TemplateTypeId = 42) --Smart Card Request Rejection
	BEGIN
		
		DECLARE @RequestPersonId INT
		DECLARE @ApplicantName NVARCHAR(200)
		DECLARE @CountryName NVARCHAR(200)
		DECLARE @UBRCode NVARCHAR(200)
		DECLARE @UBRName NVARCHAR(200)
		DECLARE @TaskComment NVARCHAR(200)
		
		SELECT @RequestId = RequestId, @TaskComment = TaskComment, @RecipientId = CreatedByID FROM [Tasks] WHERE TaskId = @EntityId	

		SELECT @RequestpersonId = RequestPersonId from AccessRequest WHERE RequestId = @RequestId

		SELECT @ApplicantName = ForeName + ' ' + SurName, @CountryName = CountryName, @UBRCode = UBR_Code, @UBRName = UBR_Name 
		FROM vw_AccessRequestperson 
		WHERE RequestPersonId=@RequestpersonId
	END

	SELECT @Recipient = Forename + ' ' + Surname, @ToAddress=EmailAddress FROM dbo.HRPerson WHERE dbPeopleID = @RecipientId
	
	IF @TaskStatusId IS NOT NULL
		SET @TaskStatus = (SELECT [Status] FROM TaskStatusLookup WHERE [TaskStatusId] = @TaskStatusId)
		
	IF @AccessAreaID IS NOT NULL
		SET @AccessArea = (SELECT aa.CountryName + ' / ' + aa.CityName + ' / ' + aa.BuildingName + ' / ' + aa.FloorName + ' / ' + aa.AccessAreaName AS [AccessAreaName] FROM vw_AccessArea aa WHERE AccessAreaID = @AccessAreaID)
	
	SET @Content = REPLACE(@Content,'[$$NAME$$]',ISNULL(@Recipient,''))
	SET @Content = REPLACE(@Content,'[$$USER_NAME$$]',ISNULL(@UserName,''))
	SET @Content = REPLACE(@Content,'[$$ACCESS_AREA$$]',ISNULL(@AccessArea,''))
	SET @Content = REPLACE(@Content,'[$$BUSINESS_AREA$$]',ISNULL(@BusinessArea,''))
	SET @Content = REPLACE(@Content,'[$$PASS_OFFICE$$]',ISNULL(@sPassOffice,''))
	SET @Content = REPLACE(@Content,'[$$DIVISION$$]',ISNULL(@sDivision,''))
	SET @Content = REPLACE(@Content,'[$$RESPONSE$$]',ISNULL(@TaskStatus,''))
	SET @Content = REPLACE(@Content,'[$$HTTP_ROOT$$]',ISNULL(@HttpRoot,''))
	SET @Content = REPLACE(@Content,'[$$RequestId$$]',ISNULL(@RequestId,''))
	SET @Content = REPLACE(@Content,'[$$ApplicantName$$]',ISNULL(@ApplicantName,''))
	SET @Content = REPLACE(@Content,'[$$UbrCode$$]',ISNULL(@UBRCode,''))
	SET @Content = REPLACE(@Content,'[$$UbrName$$]',ISNULL(@UBRName,''))
	SET @Content = REPLACE(@Content,'[$$CountryName$$]',ISNULL(@CountryName,''))
	SET @Content = REPLACE(@Content,'[$$Comment$$]',ISNULL(@TaskComment,''))


	IF @Comment IS NOT NULL AND LEN(@Comment) > 0
		SET @Content = @Content + '<br><br>Comments:<br>' + @Comment

	IF @ToAddress = @CCAddress
		SET @CCAddress = NULL

	SELECT @Content

END

--GRANT EXECUTE ON [up_GetEmailContent] TO dbAccessUser
GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_GetEmailContent]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_GetEmailContent]
END
GO

/****** Object:  StoredProcedure [dbo].[up_GetEmailContent]    Script Date: 2/28/2017 5:49:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		AC
-- Create date: 21/03/2011
-- Description:	Gets email subject, content and the 'to' email address. 
-- =============================================
CREATE PROCEDURE [dbo].[up_GetEmailContent] 
(
	@TemplateTypeId int,
	@EntityId int,
	@dbPeopleID int,
	@HttpRoot nvarchar(100),
	@Comment  nvarchar(500) out,
	@Content nvarchar(1000) out,
	@Subject nvarchar(200) out,
	@ToAddress nvarchar(200) out,
	@CCAddress nvarchar(200) out
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @RecipientId int
	DECLARE @Recipient nvarchar(100)
	DECLARE @UserName nvarchar(100)
	DECLARE @BusinessAreaID INT
	DECLARE @BusinessArea nvarchar(100)
	DECLARE @CorporateDivisionAccessAreaId INT	
	DECLARE @AccessAreaID int
	DECLARE @AccessArea nvarchar(500)
	DECLARE @TaskStatusId INT
	DECLARE @TaskStatus nvarchar(15)
	DECLARE @RequestId int
	Declare @sPassOffice NVarChar(250);
	Declare @sDivision NVarChar(255);
	
	SET @UserName = (SELECT Forename + ' ' + Surname FROM dbo.HRPerson WHERE dbPeopleID = @dbPeopleID);
	SET @Content = (SELECT [Body] FROM EmailTemplates WHERE [Type] = @TemplateTypeId)
	SELECT @Subject = (SELECT [Subject] FROM EmailTemplates WHERE [Type] = @TemplateTypeId)
	
	IF (@TemplateTypeId = 11) --  request submitted
	BEGIN
		SET @RecipientId = @dbPeopleID
		
	END
	ELSE IF (@TemplateTypeId = 3)  -- Business Area Approver request from DA to user
	BEGIN

		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = (SELECT BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId))
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);
		
	END
	ELSE IF (@TemplateTypeId = 4) -- Business Area Approver response from User to DA
	BEGIN
				
		SELECT @TaskStatusId = TaskStatusId, @BusinessAreaID = BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId
		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE [BusinessAreaID] = @BusinessAreaID)
		SET @RecipientId = (SELECT CreatedByID FROM Tasks WHERE TaskId = @EntityId)
	END		
	ELSE IF (@TemplateTypeId = 6) OR (@TemplateTypeId = 18) -- Access Area Approver / Recertifier request from DA to user
	BEGIN		
		SELECT @AccessAreaID = AccessAreaID FROM [Tasks] WHERE TaskId = @EntityId
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);		
	END
	ELSE IF (@TemplateTypeId = 7)  -- Business Area Approver removal to user
	BEGIN
		SELECT @RecipientId=dbPeopleID, @BusinessAreaId=BusinessAreaId FROM CorporateBusinessAreaApprover WHERE CorporateBusinessAreaApproverId = @EntityId 
		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = @BusinessAreaId)	
	END
	ELSE IF (@TemplateTypeId = 8) OR (@TemplateTypeId = 17) -- Access Area Approver / Recertifier removal to user
	BEGIN
		SELECT @RecipientId=dbPeopleID, @CorporateDivisionAccessAreaId=CorporateDivisionAccessAreaId FROM CorporateDivisionAccessAreaApprover WHERE CorporateDivisionAccessAreaApproverId = @EntityId 
		SET @AccessAreaID = (SELECT AccessAreaID FROM vw_DivisionAccessArea WHERE CorporateDivisionAccessAreaId = @CorporateDivisionAccessAreaId)
		
	END
	ELSE IF (@TemplateTypeId = 10) -- Access Area Approval recertification due in 5 days
	BEGIN
		SELECT @RecipientId=dbPeopleID, @CorporateDivisionAccessAreaId=CorporateDivisionAccessAreaId FROM CorporateDivisionAccessAreaApprover WHERE CorporateDivisionAccessAreaApproverId = @EntityId 
		--execute the stored procedure to get the AccessArea
		EXEC @AccessArea = [dbo].[up_GetJoinedAccessAreasNames] @INN_UserID = @RecipientId				
	END
	ELSE IF (@TemplateTypeId = 12) OR (@TemplateTypeId = 13) -- Access Area Approver or Recertifier Response
	BEGIN
		
		SELECT @RecipientId = CreatedByID, @TaskStatusId = TaskStatusId, @AccessAreaID = AccessAreaId FROM Tasks WHERE TaskId = @EntityId
				
	END
	ELSE IF (@TemplateTypeId = 14) OR (@TemplateTypeId = 15) -- Request Approval or Access Approval
	BEGIN
		SELECT @TaskStatusId=TaskStatusId, @AccessAreaID = AccessAreaId, @RequestId = RequestId FROM [Tasks] WHERE TaskId = @EntityId		
		SELECT @RecipientId=CreatedByID, @CCAddress=EmailAddress FROM vw_AccessRequest WHERE RequestId = @RequestId
	END
	ELSE IF (@TemplateTypeId = 16) -- New Pass Complete
	BEGIN
		SET @TaskStatus = (SELECT [Status] FROM TaskStatusLookup WHERE [TaskStatusId] = (SELECT TaskStatusId FROM [Tasks] WHERE TaskId = @EntityId))
		SELECT @RecipientId = CreatedByID, @RequestId = RequestId FROM Tasks WHERE TaskId = @EntityId
		SELECT @CCAddress = EmailAddress FROM vw_BadgeRequest WHERE RequestID = @RequestId
		SET @Comment = NULL
	END
	ELSE IF (@TemplateTypeId = 23)  -- Division Administator Add to user
	BEGIN

		SET @BusinessArea = (SELECT Name FROM CorporateBusinessArea WHERE BusinessAreaID = (SELECT BusinessAreaId FROM [Tasks] WHERE TaskId = @EntityId));
		If (@BusinessArea Is Null) Set @BusinessArea = (SELECT Name FROM CorporateDivision WHERE DivisionID = (SELECT DivisionId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);
		
	END
	ELSE IF (@TemplateTypeId = 26)  -- Pass Office Add to user
	BEGIN

		SET @sPassOffice = (SELECT Name FROM LocationPassOffice WHERE PassOfficeID = (SELECT PassOfficeId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);

	END
	ELSE IF (@TemplateTypeId = 27)  -- Division Owner Add to user
	BEGIN

		SET @sDivision = (SELECT Name FROM CorporateDivision WHERE DivisionID = (SELECT DivisionId FROM [Tasks] WHERE TaskId = @EntityId));
		SET @RecipientId = (SELECT dbPeopleID FROM TasksUsers WHERE TaskId = @EntityId);

	END

	SELECT @Recipient = Forename + ' ' + Surname, @ToAddress=EmailAddress FROM dbo.HRPerson WHERE dbPeopleID = @RecipientId
	
	IF @TaskStatusId IS NOT NULL
		SET @TaskStatus = (SELECT [Status] FROM TaskStatusLookup WHERE [TaskStatusId] = @TaskStatusId)
		
	IF @AccessAreaID IS NOT NULL
		SET @AccessArea = (SELECT aa.CountryName + ' / ' + aa.CityName + ' / ' + aa.BuildingName + ' / ' + aa.FloorName + ' / ' + aa.AccessAreaName AS [AccessAreaName] FROM vw_AccessArea aa WHERE AccessAreaID = @AccessAreaID)
	
	SET @Content = REPLACE(@Content,'[$$NAME$$]',ISNULL(@Recipient,''))
	SET @Content = REPLACE(@Content,'[$$USER_NAME$$]',ISNULL(@UserName,''))
	SET @Content = REPLACE(@Content,'[$$ACCESS_AREA$$]',ISNULL(@AccessArea,''))
	SET @Content = REPLACE(@Content,'[$$BUSINESS_AREA$$]',ISNULL(@BusinessArea,''))
	SET @Content = REPLACE(@Content,'[$$PASS_OFFICE$$]',ISNULL(@sPassOffice,''))
	SET @Content = REPLACE(@Content,'[$$DIVISION$$]',ISNULL(@sDivision,''))
	SET @Content = REPLACE(@Content,'[$$RESPONSE$$]',ISNULL(@TaskStatus,''))
	SET @Content = REPLACE(@Content,'[$$HTTP_ROOT$$]',ISNULL(@HttpRoot,''))

	IF @Comment IS NOT NULL AND LEN(@Comment) > 0
		SET @Content = @Content + '<br><br>Comments:<br>' + @Comment

	IF @ToAddress = @CCAddress
		SET @CCAddress = NULL

	SELECT @Content

END

--GRANT EXECUTE ON [up_GetEmailContent] TO dbAccessUser


GO

