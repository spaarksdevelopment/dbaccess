IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagePassOffice')
BEGIN

/****** Object:  Table [dbo].[XLImportStagePassOffice]    Script Date: 1/8/2016 9:13:47 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[XLImportStagePassOffice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Enabled] [nvarchar](500) NULL,
	[Country] [nvarchar](500) NULL,
	[City] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Telephone] [nvarchar](500) NULL,
	[EmailEnabled] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStagePassOffice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[XLImportStagePassOffice]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStagePassOffice_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStagePassOffice] CHECK CONSTRAINT [FK_XLImportStagePassOffice_XLImportFile]

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagePassOffice')
BEGIN
	
	DROP TABLE [dbo].[XLImportStagePassOffice]
	
END

GO