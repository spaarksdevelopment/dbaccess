/****** Object:  UserDefinedFunction [dbo].[GetEmployeeClass]    Script Date: 21/04/2015 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeClass]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[IsEmployeeExternal]
'
END

GO

--//@UNDO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeClass]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:	 MCS
-- Create date: 14/04/2015
-- Description: Is this person an internal employee (0) or an external employee (1)
-- =============================================
CREATE FUNCTION [dbo].[IsEmployeeExternal]
(
	@OrganizationalRelationship nvarchar(3)
)
RETURNS bit
AS
BEGIN
	DECLARE @Result BIT

	IF @OrganizationalRelationship = ''CWR''
	   RETURN 1

	RETURN 0

END
' 
END

GO