ALTER TABLE HRPerson 
ADD Suspended bit
GO 

--//@UNDO

ALTER TABLE HRPerson 
DROP COLUMN Suspended
GO 