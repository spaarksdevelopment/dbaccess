IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedChangeStartDate]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedChangeStartDate]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeedChangeStartDate]    Script Date: 11/10/2015 3:20:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================================
--History: By Wijitha (09/11/2015) - Modified SP to user ChangeStartDateProcessed
--		   to filter unprocessed triggers, instead od 'processedFlag'
--=============================================================================

CREATE PROCEDURE [dbo].[HRFeedChangeStartDate]
AS

DECLARE @TransactionId int
DECLARE @CreateAccessTimestamp datetime
DECLARE @masterDataDBPeopleId int

DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
SELECT TransactionId, CreateAccessTimestamp, md.dbPeopleId 
FROM HRFeedMessage hrm 
	JOIN MasterData md ON hrm.HRID=md.HR_ID 
	JOIN HRPerson hp ON md.dbPeopleId=hp.dbPeopleID
WHERE IsNull(hrm.ChangeStartDateProcessed, 0) = 0 
AND hrm.TriggerType = 'JOIN'
AND hrm.TriggerAction = 'UPD'
AND [dbo].[HasBadgeBeenCreatedForPersonWithStartDate](md.dbPeopleId)=1
AND [dbo].[DoWeNeedToGenerateChangeStartDateTasks](hrm.[CreateAccessTimestamp], md.[CREATE ACCESS DATE TIMESTAMP], hp.[CountryName], hp.[CityName])=1

OPEN myCursor

FETCH NEXT FROM myCursor INTO @TransactionId, @CreateAccessTimestamp, @masterDataDBPeopleId

WHILE @@FETCH_STATUS = 0
BEGIN
	exec [dbo].[HRFeed_CreateChangeStartDateTask] @dbPeopleId = @masterDataDBPeopleId, @NewStartDate = @CreateAccessTimestamp  

	UPDATE HRFeedMessage SET ChangeStartDateProcessed = 1, ChangeStartDateProcessedDate = getDate() WHERE TransactionId = @TransactionId

FETCH NEXT FROM myCursor INTO @TransactionId, @CreateAccessTimestamp, @masterDataDBPeopleId
END

CLOSE myCursor
DEALLOCATE myCursor


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedChangeStartDate]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedChangeStartDate]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeedChangeStartDate]    Script Date: 11/9/2015 11:31:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[HRFeedChangeStartDate]
AS

DECLARE @TransactionId int
DECLARE @CreateAccessTimestamp datetime
DECLARE @masterDataDBPeopleId int

DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
SELECT TransactionId, CreateAccessTimestamp, md.dbPeopleId 
FROM HRFeedMessage hrm 
	JOIN MasterData md ON hrm.HRID=md.HR_ID 
	JOIN HRPerson hp ON md.dbPeopleId=hp.dbPeopleID
WHERE Processed = 0 AND TriggerType = 'JOIN'
AND TriggerAction<>'DEL'
AND [dbo].[HasBadgeBeenCreatedForPersonWithStartDate](md.dbPeopleId)=1
AND [dbo].[DoWeNeedToGenerateChangeStartDateTasks](hrm.[CreateAccessTimestamp], md.[CREATE ACCESS DATE TIMESTAMP], hp.[CountryName], hp.[CityName])=1

OPEN myCursor

FETCH NEXT FROM myCursor INTO @TransactionId, @CreateAccessTimestamp, @masterDataDBPeopleId

WHILE @@FETCH_STATUS = 0
BEGIN
	exec [dbo].[HRFeed_CreateChangeStartDateTask] @dbPeopleId = @masterDataDBPeopleId, @NewStartDate = @CreateAccessTimestamp  

	UPDATE HRFeedMessage SET Processed = 1, ProcessedDate = getDate() WHERE TransactionId = @TransactionId

FETCH NEXT FROM myCursor INTO @TransactionId, @CreateAccessTimestamp, @masterDataDBPeopleId
END

CLOSE myCursor
DEALLOCATE myCursor

GO


