﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QueueEmailsForUser]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[QueueEmailsForUser]
END
GO

--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 14/10/2015
--		MCS-19/10/2015: Modified to allow use with any email template type 
-- Description: Notify relevant pass office users by emails when revoking a user
--============================================================
CREATE PROCEDURE [dbo].[QueueEmailsForUser]
@dbPeopleID int,
@EmailTypeID int 

AS
BEGIN

DECLARE @To nvarchar(255)
DECLARE @ToName nvarchar(255)
DECLARE @Subject nvarchar(255)
DECLARE @Body nvarchar(1000)
DECLARE @Guid nvarchar(255)
DECLARE @EmailForename nvarchar(255)
DECLARE @EmailSurname nvarchar(255)
DECLARE @HttpRoot nvarchar(100)
DECLARE @OfficerID varchar(2)

--TABLE TO STORE Recipients
DECLARE @PassOfficeMembers TABLE
(
	RowNumber int,
	PassOfficeUserEmail nvarchar(200),
	PassOfficeUserName nvarchar(200)
)

DELETE FROM @PassOfficeMembers

DECLARE @UserCountryID int
SET @UserCountryID = 59 --Default to UK, in case person's country cannot be found

SELECT @UserCountryID=CountryID
FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
WHERE dbPeopleID=@dbPeopleID
		
SELECT @OfficerID = OfficerID from HRPerson
WHERE HRPerson.dbPeopleID= @dbPeopleID
		
IF (@OfficerID='D' or @OfficerID='MD')
BEGIN
INSERT INTO @PassOfficeMembers
		SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, 
			hr2.EmailAddress AS PassOfficeUserEmail, 
			hr2.Forename + ' ' + hr2.Surname AS PassOfficeUserName
		FROM LocationPassOffice 
			INNER JOIN	LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
			JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
		WHERE LocationPassOffice.Enabled = 1
			AND LocationPassOfficeUser.IsEnabled = 1
			AND LocationPassOffice.EmailEnabled = 1
END
ELSE
BEGIN
INSERT INTO @PassOfficeMembers
		SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, 
		HRPerson.EmailAddress AS PassOfficeUserEmail, 
		HRPerson.Forename + ' ' + HRPerson.Surname AS PassOfficeUserName
		FROM LocationPassOffice
		JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
		JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
		WHERE LocationPassOffice.Enabled = 1
		AND LocationPassOfficeUser.IsEnabled = 1 
		AND LocationPassOffice.EmailEnabled = 1
		AND LocationPassOffice.PassOfficeID in (SELECT DISTINCT PassOfficeID FROM LocationCountry WHERE CountryID = @UserCountryID --Get the default PassOffice in the user's country.
												UNION 
												SELECT PassOfficeID 
												FROM [dbo].[fGetAccessAreaPassOfficesPerPerson] (@dbPeopleID))
END

SELECT @EmailForename = Forename , @EmailSurname = Surname FROM HRPerson WHERE dbPeopleID = @dbPeopleID

SELECT @HttpRoot=Value
FROM AdminValues
WHERE [Key]='HttpRoot'

--Loop through each member of the pass office
DECLARE @NumberofPassOfficePeople int
SELECT @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
DECLARE @loopCounter INT
SET @loopCounter = 1

-- Table to allow us to not send duplicate emails
DECLARE @EmailSentTo TABLE
(
	PassOfficeUserEmail nvarchar(200)
)

DELETE FROM @EmailSentTo

WHILE (@loopCounter <= @NumberofPassOfficePeople)
BEGIN
	SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = @EmailTypeID

	SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
	FROM @PassOfficeMembers
	WHERE RowNumber = @loopCounter
			
	DECLARE @BatchGuid uniqueidentifier
	SET @BatchGuid = NEWID()
	
	IF (@To <> '' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
	BEGIN 
			SET @Body = REPLACE ( @Body , '[$$UserEmail$$]' , @EmailForename + ' ' + @EmailSurname ); 
			SET @Body = REPLACE ( @Body , '[$$HTTP_ROOT$$]' , @HttpRoot ); 
			SET @Body = REPLACE ( @Body , '[$$NAME$$]' , @ToName ); 
			SET @Body = REPLACE ( @Body , '[$$FILE_PATH$$]' , 'Pages/MyTasks.aspx' ); 

			EXEC [up_InsertEmailLog] 
					@To 
					, 'dbAccess_DO_NOT_REPLY@db.com' 
					, 'ryan.sheehan@db.com'
					, @Subject 
					, @Body 
					, 0 
					, @BatchGuid
					, @EmailTypeID 
					, NULL 
	END 

	INSERT INTO @EmailSentTo SELECT @To			

	SET @loopCounter = @loopCounter + 1
END

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QueueEmailsForUser]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[QueueEmailsForUser]
END
GO

