/****** Object:  UserDefinedFunction [dbo].[IsEmployeeExternal]    Script Date: 20/04/2015 16:24:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsEmployeeExternal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:	 MCS
-- Create date: 14/04/2015
-- Description: Is this person an internal employee (0) or an external employee (1)
-- =============================================
CREATE FUNCTION [dbo].[IsEmployeeExternal]
(
	@OrganizationalRelationship nvarchar(3)
)
RETURNS bit
AS
BEGIN
	DECLARE @Result BIT

	IF @OrganizationalRelationship = ''CWR''
	   RETURN 1

	RETURN 0

END
' 
END

GO
