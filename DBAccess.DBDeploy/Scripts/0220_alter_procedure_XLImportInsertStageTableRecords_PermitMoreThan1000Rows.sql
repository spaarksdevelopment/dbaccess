IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportInsertStageTableRecords]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportInsertStageTableRecords]
END
GO

-- =============================================
-- Author:		Asanka
-- Create date: 18/12/2015
-- Description:	Insert records to staging tables.
--						Modified 2/3/2016. Altered to permit more than 1000 rows as input
-- =============================================

CREATE PROCEDURE [dbo].[XLImportInsertStageTableRecords]
	@Table NVARCHAR(255)
   ,@Columns NVARCHAR(MAX)
   ,@Rows NVARCHAR(MAX)
   ,@Success bit out
AS
BEGIN

    	DECLARE @RowsTable TABLE
		(
			RowID SMALLINT,
			[Value] NVARCHAR(500) 
		)

		INSERT INTO @RowsTable(RowID, Value)
		SELECT * FROM [dbo].[SplitMax] (
		   '),'
		  ,@Rows)

		DECLARE @CurrentRowID INT
		DECLARE @NumberOfRows INT
		DECLARE @CurrentRowValue NVARCHAR(500)

		SET @CurrentRowID = 1

		SELECT @NumberOfRows=(SELECT COUNT(1) FROM @RowsTable)

		WHILE @CurrentRowID<=@NumberOfRows
		BEGIN
			SELECT @CurrentRowValue = Value 
			FROM @RowsTable 
			WHERE RowID = @CurrentRowID

			SET @CurrentRowValue = REPLACE(@CurrentRowValue, ', (', '(')

			--IF last char isn't an ) then add one
			IF(SUBSTRING(REVERSE(@CurrentRowValue),1,1)<>')')
				SET @CurrentRowValue = @CurrentRowValue + ')'

			DECLARE @SQLQuery AS NVARCHAR(MAX)
			SET @SQLQuery = 'INSERT INTO '+ @Table + '('+ @Columns +')'+ 'VALUES ' + @CurrentRowValue
			EXECUTE(@SQLQuery)	

			SET @CurrentRowID = @CurrentRowID + 1
		END

		IF @@ERROR>0
			SELECT @Success = 0
		ELSE
			SELECT @Success = 1
END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportInsertStageTableRecords]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportInsertStageTableRecords]
END
GO

-- =============================================
-- Author:		Asanka
-- Create date: 18/12/2015
-- Description:	Insert records to staging tables.
-- =============================================

CREATE PROCEDURE [dbo].[XLImportInsertStageTableRecords]
	@Table NVARCHAR(255)
   ,@Columns NVARCHAR(MAX)
   ,@Rows NVARCHAR(MAX)
   ,@Success bit out
AS
BEGIN

	DECLARE @SQLQuery AS NVARCHAR(MAX)

	SET @SQLQuery = 'INSERT INTO '+ @Table + '('+ @Columns +')'+ 'VALUES ' +@Rows

	EXECUTE(@SQLQuery)	
	
	IF @@ERROR>0
		SELECT @Success = 0
	ELSE
		SELECT @Success = 1

END

GO