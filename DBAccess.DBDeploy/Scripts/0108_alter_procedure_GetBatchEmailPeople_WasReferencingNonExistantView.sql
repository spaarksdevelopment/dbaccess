IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBatchEmailPeople]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[GetBatchEmailPeople]
END
GO



/****** Object:  StoredProcedure [dbo].[GetBatchEmailPeople]    Script Date: 10/27/2015 3:28:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetBatchEmailPeople]
As
Select [To] 
FROM 
		[dbo].[EmailLog]
Where
		[To] != ''
and		[DateSent] is null
and		[EmailType] is not null
and		ISNULL([InProgress], 0) != 1
and		(	
				[To] NOT IN (SELECT [EmailAddress] FROM [dbo].[vw_EmailDisabledPassOfficePeople])
		)
Group By
		[To]
order by [To] desc

GO

--//@UNDO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBatchEmailPeople]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[GetBatchEmailPeople]
END
GO

--Note this shouldn't rollback, as it's already been applied to production

/****** Object:  StoredProcedure [dbo].[GetBatchEmailPeople]    Script Date: 10/27/2015 3:28:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetBatchEmailPeople]
As
Select [To] 
FROM 
		[dbo].[EmailLog]
Where
		[To] != ''
and		[DateSent] is null
and		[EmailType] is not null
and		ISNULL([InProgress], 0) != 1
and		(	
				[To] NOT IN (SELECT [EmailAddress] FROM [dbo].[vw_EmailDisabledPassOfficePeople])
		)
Group By
		[To]
order by [To] desc

GO