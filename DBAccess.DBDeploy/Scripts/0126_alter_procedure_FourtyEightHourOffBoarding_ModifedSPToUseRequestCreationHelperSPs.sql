IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FourtyEightHourOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[FourtyEightHourOffBoarding]
END
GO

/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 11/11/2015 4:01:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--       16/10/2015 Wijitha - Modified the logic of selecting offboarding users to select revoked users by CAT DEL triggers as well
--       22/10/2015 Wijitha - Fixed issue of not sending user's home country's pass officers, when the default pass office is outside of home country
--       11/11/2015 Wijitha - Modifed SP to user generic helper methods to create requests and send emails
--======================================================================

CREATE Procedure [dbo].[FourtyEightHourOffBoarding] 
AS
BEGIN 

SET NOCOUNT ON

DECLARE @HRUpdates TABLE 
(
RowNumber int, 
dbPeopleID int, 
messageId int, 
isCATRevoke bit
) 
    
INSERT INTO @HRUpdates 
SELECT ROW_NUMBER() OVER (ORDER BY offboard.dbPeopleID) AS RowNumber, offboard.dbPeopleId, offboard.TransactionId, offboard.IsCATRevoke
FROM 
	(SELECT md.dbPeopleID,
		hrm.TransactionId,
		0 as isCATRevoke
	FROM HRFeedMessage hrm
		INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
		INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
	WHERE hrm.Processed = 0 AND 
		hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
		AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
	
	UNION

	SELECT c.DBPeopleID, 
		c.TransactionID, 
		1 AS isCATRevoke 
	FROM CATDeleteOffboard c
	WHERE c.Processed = 0 AND c.CreatedDate <= DATEADD(d,-7,GETDATE())
	) AS offboard

DECLARE @NumberofRows int
SELECT @NumberofRows = Count(*) FROM @HRUpdates

IF (@NumberofRows > 0)
BEGIN	

	DECLARE @CreatedByID int = null
	DECLARE @dbPeopleID int
	DECLARE @messageId int
	DECLARE @isCATRevoke bit
	DECLARE @intFlag int = 1

	WHILE (@intFlag <= @NumberOfRows)
	BEGIN

		SELECT @dbPeopleID = dbPeopleID, 
			@messageId = messageId, 
			@isCATRevoke = isCATRevoke 
		FROM @HRUpdates 
		WHERE RowNumber = @intFlag

		--Execute SP to off board single user
		EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID

		--Mark triggers as processed
		IF(@isCATRevoke = 1)
		BEGIN
			UPDATE CATDeleteOffboard
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END
		ELSE 
		BEGIN
			UPDATE HRFeedMessage
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END

		SET @intFlag = @intFlag + 1

	END			
END
END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FourtyEightHourOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[FourtyEightHourOffBoarding]
END
GO


/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 11/11/2015 2:59:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--       16/10/2015 Wijitha - Modified the logic of selecting offboarding users to select revoked users by CAT DEL triggers as well
--       22/10/2015 Wijitha - Fixed issue of not sending user's home country's pass officers, when the default pass office is outside of home country
--======================================================================

CREATE Procedure [dbo].[FourtyEightHourOffBoarding] 
AS 
Begin 

SET NOCOUNT ON

--catherine steele
Declare @CreatedByID int = null

--added BH 18 Feb
declare @OfficerID varchar(2)
declare @dbPeopleID int
declare @messageId int
declare @isCATRevoke bit


IF Exists
(
	SELECT md.dbPeopleId from HRFeedMessage hrm
		   INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
		   INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
    WHERE 
	hrm.Processed = 0 AND 
	hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
		   AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))

	UNION

	SELECT DBPeopleID
	FROM CATDeleteOffboard
	WHERE Processed=0 AND CreatedDate <= DATEADD(d,-7,GETDATE())
)
Begin
	
	DECLARE @HRUpdates TABLE (RowNumber int, dbPeopleID int, messageId int, isCATRevoke bit) 
    
	INSERT INTO @HRUpdates 
	SELECT ROW_NUMBER() OVER (ORDER BY offboard.dbPeopleID) AS RowNumber, offboard.dbPeopleId, offboard.TransactionId, offboard.IsCATRevoke
	FROM 
		(SELECT md.dbPeopleID,
			hrm.TransactionId,
			0 as isCATRevoke
		FROM HRFeedMessage hrm
			INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
			INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
		WHERE hrm.Processed = 0 AND 
			hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
			AND hrm.[TerminationDate] is not null AND (hrm.[TerminationDate] <= DATEADD(d,-7,GETDATE()))
	
		UNION

		SELECT c.DBPeopleID, 
			c.TransactionID, 
			1 AS isCATRevoke 
		FROM CATDeleteOffboard c
		WHERE c.Processed = 0 AND c.CreatedDate <= DATEADD(d,-7,GETDATE())
		) AS offboard


	DECLARE @RC int
	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)

	Declare @NumberofRows int
	Select @NumberofRows = Count(*) From @HRUpdates
	
	DECLARE @intFlag INT
	SET @intFlag = 1
	
	--commented out BH 18 Feb
	--Declare @dbPeopleID int
	
	WHILE (@intFlag <= @NumberOfRows)
	Begin
		Select @dbPeopleID = dbPeopleID, @messageId = messageId, @isCATRevoke = isCATRevoke From @HRUpdates where RowNumber = @intFlag
		
		--added BH 18 Feb
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleID

		--Should already be revoked ie can't login or be the subject of any requests but just in case
		UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
		UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

		EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID
		
		--Note the message queue ID and date message received. This is required for 48HR auditing
		if(@isCATRevoke = 1)
		begin
			UPDATE CATDeleteOffboard
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		end
		else 
		begin
			UPDATE HRFeedMessage
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		end

		SET @intFlag = @intFlag + 1

		-----------------------------
		---  Offboarded people should be disabled as approvers
		Update CorporateDivisionAccessAreaApprover
		set Enabled=0
		where dbPeopleID=@dbPeopleID
		--------------------

		--    Get the users passoffice email address          
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person's country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleID
		
		
		IF (@OfficerID='D' or @OfficerID='MD')
		BEGIN
			 INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, 
					hr2.EmailAddress AS PassOfficeUserEmail, 
					hr2.Forename + ' ' + hr2.Surname AS PassOfficeUserName
			 FROM LocationPassOffice 
					INNER JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
					JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
			 WHERE LocationPassOffice.Enabled = 1
					AND LocationPassOfficeUser.IsEnabled = 1
					AND LocationPassOffice.EmailEnabled = 1

		END
		ELSE
		BEGIN
			 INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, 
				HRPerson.EmailAddress AS PassOfficeUserEmail, 
				HRPerson.Forename + ' ' + HRPerson.Surname AS PassOfficeUserName
			 FROM LocationPassOffice 
				JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE LocationPassOffice.Enabled = 1				
			       AND LocationPassOffice.EmailEnabled = 1
				   AND LocationPassOfficeUser.IsEnabled = 1 
				   AND LocationPassOffice.PassOfficeID in (select distinct PassOfficeID from LocationCountry where CountryID = @UserCountryID --Get the default PassOffice in the user home country.
															union
															select distinct PassOfficeID from LocationPassOffice where CountryID = @UserCountryID --Get all pass offices in the user home country
															union
															select PassOfficeID 
															from [dbo].[fGetAccessAreaPassOfficesPerPerson] (@dbPeopleID))
		END

        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @messageId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]='HttpRoot'

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 36


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , '[$$UserEmail$$]' , @EmailForename + ' ' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , '[$$HTTP_ROOT$$]' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , '[$$NAME$$]' , @ToName ); 
					SET @Body = REPLACE ( @Body , '[$$FILE_PATH$$]' , 'Pages/MyTasks.aspx' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, 'dbAccess_DO_NOT_REPLY@db.com' 
							, 'ryan.sheehan@db.com' 
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 36 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END			
END
END




GO


