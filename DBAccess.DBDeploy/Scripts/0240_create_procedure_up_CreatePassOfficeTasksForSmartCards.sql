IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreatePassOfficeTasksForSmartCards]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_CreatePassOfficeTasksForSmartCards]
END
GO

--====================================================================================
-- Created By: Wijitha Wijenayake
-- Created Date: 01/03/2017
-- Description: Create pass officer's tasks for Smart Card requests. For Smart Card requests pass officer tasks wont be created when sumitting the requests 
--				due to they have to under go another approval process in between
-- 
-- History: 
--			01/03/2017 (Wijitha) - Initial Create
--====================================================================================
CREATE PROCEDURE up_CreatePassOfficeTasksForSmartCards
@dbPeopleID INT,
@AccessRequestID INT,
@success bit  OUTPUT
AS
BEGIN
	SET NoCount ON;
	SET @Success = 0;

	DECLARE @dtNow DateTime2 = SysDateTime ();
	DECLARE @doToday Date = @dtNow;

	BEGIN Try

		DECLARE @iRequestTypeID Int = 0
		DECLARE @isSmartCardRequest bit = 0
		
		SELECT Top 1 @iRequestTypeID = RequestTypeID, @isSmartCardRequest = IsNUll(IsSmartCard, 0)
		FROM vw_AccessRequest 
		WHERE RequestId = @AccessRequestID;

		IF (@iRequestTypeID <> 1) RaisError (N'This is not a Pass Request. This procedure only handles smart card pass requests.', 11, 1);

		IF (@isSmartCardRequest = 0) RaisError(N'This is not a Smart Card request. This procedure only handles smart card pass requests.', 11, 1)

		--1) First Create Pass Request Tasks
		INSERT INTO
			Tasks
			(
				[TaskTypeId],
				[RequestId],
				[DivisionId],
				[BusinessAreaId],
				[AccessAreaId],
				[Description],
				[CreatedByID],
				[CreatedDate]
			)
		SELECT
			TaskTypeLookup.[TaskTypeId],
			AccessRequest.[RequestID],
			AccessRequest.[DivisionID],
			vw_AccessRequestPerson.[BusinessAreaID],
			AccessRequest.[AccessAreaID],
			Replace(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TaskTypeLookup.[Description],
			'{1}', vw_AccessRequestPerson.[Forename] + ' ' + vw_AccessRequestPerson.[Surname] + ' ( ' + vw_AccessRequestPerson.[FulldbPeopleID] + ', ' + IsNull(vw_AccessRequestPerson.EmailAddress,'') + ', ' + CONVERT(VARCHAR(7),vw_AccessRequestPerson.[dbDirID]) + ' )'),
			'{2}', IsNull((SELECT Top 1 CountryName FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + ' of ' + (SELECT Top 1 [UBR] FROM [HRPerson]  WHERE[dbPeopleID] = [vw_AccessRequestPerson].dbPeopleID) + '-' + vw_AccessRequestPerson.UBR_Name,'')),
			'{4}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END),
			'{5}', CASE WHEN IsNull(HRPersonBadge.BadgeType,'') = 'LL' AND LocationBuilding.Name IS NOT NULL THEN ' for access to ' + LocationBuilding.Name ELSE '' END),
			'{6}', CASE IsNull(HRPersonBadge.BadgeType,'') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END),
			'{7}', (CASE WHEN LocationBuilding.[Name] IS NOT NULL THEN ' ' + LocationBuilding.[Name] + '/' ELSE '' END) + (CASE WHEN [FloorName] IS NOT NULL THEN ' ' + [FloorName] + '/' ELSE '' END) + (CASE WHEN [AccessAreaName] IS NOT NULL THEN ' ' + [AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN [AccessAreaTypeName] IS NOT NULL THEN ' ' + [AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN [ENDDate] IS NOT NULL THEN ' until ' + Convert(VarChar(11), [ENDDate], 106) ELSE '' END)),
			'{8}', IsNULL('for the ' + LocationPassOffice.Name, '')), '{9}', 'Smart Card') As [Description],
			@dbPeopleID,
			@dtNow
		FROM
			vw_AccessRequestPerson 
			INNER JOIN AccessRequest ON AccessRequest.[RequestPersonID] = vw_AccessRequestPerson.[RequestPersonID]
			INNER JOIN AccessRequestType ON AccessRequestType.[RequestTypeID] = AccessRequest.[RequestTypeID]
			INNER JOIN TaskTypeLookup ON TaskTypeLookup.[TaskTypeId] = AccessRequestType.[TaskTypeID]
			LEFT JOIN LocationPassOffice ON AccessRequest.[PassOfficeID] = LocationPassOffice.[PassOfficeID]
			LEFT OUTER JOIN vw_AccessArea ON vw_AccessArea.[AccessAreaID] = AccessRequest.[AccessAreaID]
			LEFT OUTER JOIN CorporateDivision ON CorporateDivision.[DivisionID] = AccessRequest.[DivisionID]
			LEFT OUTER JOIN HRPersonBadge ON HRPersonBadge.[PersonBadgeID] = AccessRequest.[BadgeID]
			LEFT OUTER JOIN LocationBuilding ON LocationBuilding.[BuildingID] = HRPersonBadge.[BuildingID]

		WHERE AccessRequest.RequestID = @AccessRequestID
	
		--2) Assign the tasks to the pass officers of nominated delivery pass office
		INSERT INTO 
			[TasksUsers]
			(
				[TaskId],
				[dbPeopleID],
				[IsCurrent],
				[Order]
			)
		SELECT
			Tasks.TaskId,
			LocationPassOfficeUser.dbPeopleID,
			1,
			1
		FROM
			Tasks
			INNER JOIN AccessRequest ON Tasks.[RequestId] = AccessRequest.[RequestID]
			INNER JOIN AccessRequestPerson ON AccessRequest.[RequestPersonID] = AccessRequestPerson.[RequestPersonID]
			INNER JOIN AccessRequestDeliveryDetail ON AccessRequestPerson.[RequestMasterID] = AccessRequestDeliveryDetail.[RequestMasterID]
			INNER JOIN LocationPassOfficeUser ON AccessRequestDeliveryDetail.[PassOfficeID] = LocationPassOfficeUser.[PassOfficeID]
		WHERE AccessRequest.RequestID = @AccessRequestID
			AND	AccessRequest.RequestTypeID = @iRequestTypeID
			AND	LocationPassOfficeUser.IsEnabled = 1;

		--3) Assign tasks to the pass officers of User's Home pass office (Just in case)
		INSERT INTO
			[TasksUsers]
		(
			[TaskId],
			[dbPeopleID],
			[IsCurrent],
			[Order]
		)
		SELECT
			Tasks.TaskId,
			LocationPassOfficeUser.dbPeopleID,
			1,
			1
		FROM Tasks
			INNER JOIN AccessRequest ON Tasks.RequestId = AccessRequest.RequestID
			INNER JOIN AccessRequestPerson ON AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
			INNER JOIN LocationCountry ON AccessRequestPerson.CountryID = LocationCountry.CountryID
			INNER JOIN LocationPassOfficeUser ON LocationCountry.PassOfficeID = LocationPassOfficeUser.PassOfficeID
			LEFT OUTER JOIN TasksUsers ON Tasks.TaskId = TasksUsers.TaskId
		WHERE  AccessRequest.RequestID = @AccessRequestID
			AND (TasksUsers.TasksUsersId IS NULL)   
			AND (LocationCountry.Enabled = 1)
			AND (LocationPassOfficeUser.IsEnabled = 1);

		--4) Finally (Just in case), assign tasks to pass office of location being accessed
		INSERT INTO
			[TasksUsers]
		(
			[TaskId],
			[dbPeopleID],
			[IsCurrent],
			[Order]
		)
		SELECT
			Tasks.TaskId,
			LocationPassOfficeUser.dbPeopleID,
			1,
			1
		FROM Tasks 
			INNER JOIN TasksUsers ON TasksUsers.TaskId = Tasks.TaskId
			INNER JOIN AccessRequest ON Tasks.RequestId = AccessRequest.RequestID
			Right Outer Join vw_AccessArea ON vw_AccessArea.AccessAreaID = AccessRequest.AccessAreaID
			INNER JOIN AccessRequestPerson ON AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID
			INNER JOIN LocationCountry  ON vw_AccessArea.CountryID = LocationCountry.CountryID
			INNER JOIN LocationPassOfficeUser ON LocationPassOfficeUser.PassOfficeID = LocationCountry.PassOfficeID                
		WHERE AccessRequest.RequestID = @AccessRequestID
			AND (TasksUsers.TasksUsersId IS NULL)        
			AND (LocationCountry.Enabled = 1)
			AND	(LocationPassOfficeUser.IsEnabled = 1);

	SET @Success = 1;

	END Try
	BEGIN Catch

		-- raise an error with the details of the exception
		DECLARE @iState Int;
		DECLARE @iSeverity Int;
		DECLARE @sMessage NVarChar(4000);

		SELECT
				@iState = Error_State (),
				@sMessage = Error_Message (),
				@iSeverity = Error_Severity ()

		RaisError (@sMessage, @iSeverity, @iState);

	END Catch
END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreatePassOfficeTasksForSmartCards]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_CreatePassOfficeTasksForSmartCards]
END
GO
