IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyCities]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyCities]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 18/12/2015
-- Description: Copi cities from staging table to the LocationCity table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyCities]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO LocationCity (CountryID, Name, Enabled, TimeZoneID)
	OUTPUT inserted.CityID into @tblIDs 
	SELECT (SELECT TOP 1 CountryID FROM LocationCountry WHERE Name = Country) , 
		Name, 
		1, 
		(SELECT TOP 1 TimeZoneID FROM TimeZone WHERE Description = TimeZone) 
	FROM XLImportStageCity 
	WHERE ISNULL(ValidationFailed, 0) = 0 AND FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'LocationCity', ID FROM @tblIDs

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyCities]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyCities]
END
GO