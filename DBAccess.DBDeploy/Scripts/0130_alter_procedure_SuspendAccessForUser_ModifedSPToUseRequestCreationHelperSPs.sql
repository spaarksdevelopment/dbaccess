IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuspendAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[SuspendAccessForUser]
END
GO

/****** Object:  StoredProcedure [dbo].[SuspendAccessForUser]    Script Date: 11/13/2015 3:07:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==========================================================================
-- History: 20/10/2015 (MCS) - Created new procedure in order to generate suspension task for a single user
--   		13/11/2015	(Wijitha) - Modifed SP to use common helper methods for request creation and email sending							
--==========================================================================

CREATE PROCEDURE [dbo].[SuspendAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 

--Flag as Revoked
UPDATE HRPerson SET Revoked = 1, Suspended=1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

--Create Suspend Access Requests
EXEC dbo.HRFeed_CreatePassOfficeRequestsForUser @dbPeopleId = @dbPeopleId, @requestTypeId = 19, @createdById = @CreatedByID

--Queue Emails
EXEC dbo.HRFeed_QueuePassOfficeRequestEmailsForUser @dbPeopleID = @dbPeopleId, @EmailTypeID = 39

end

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuspendAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[SuspendAccessForUser]
END
GO


/****** Object:  StoredProcedure [dbo].[SuspendAccessForUser]    Script Date: 11/13/2015 2:58:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==========================================================================
-- History: 20/10/2015 (MCS) - Created new procedure in order to generate suspension task for a single user									
--==========================================================================

CREATE PROCEDURE [dbo].[SuspendAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 


--	Flag as Revoked
UPDATE HRPerson SET Revoked = 1, Suspended=1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId


--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int

exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
exec [dbo].[up_InsertRequestPerson]	@CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

--find out the person's home pass office
declare @homePassOfficeID int
declare @homeCountryID int
declare @OfficerID varchar(2)

--first, make sure the OfficerID is available here, so it can be used later
select @OfficerID = OfficerID from HRPerson hr1
inner join AccessRequestPerson arp2 on hr1.dbPeopleID=arp2.dbPeopleID
where arp2.RequestPersonID = @RequestPersonID

select @homePassOfficeID = PassOfficeID, @homeCountryID=lc.CountryID 
FROM AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = @CreatedByID,
			@RequestTypeID = 19,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = null,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
			
		fetch next from badgesCursor into @badgeId
	end
end
else
begin
	--we still want to send an revoke request even if there are no badges
	--create it with null for BadgeID
	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 19,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @homePassOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
end

close badgesCursor


if (@OfficerID is null) or (@OfficerID<>'D' and @OfficerID<>'MD')
begin
	--make a cursor to iterate through the pass offices for the areas that the person has access to
	--(besides their home pass office)
	declare passOfficeCursor cursor local fast_forward for
       SELECT DISTINCT IsNull(bpo.PassOfficeID, c.PassOfficeID) as PassOfficeID from HRPersonAccessArea p
       JOIN AccessArea aa on aa.AccessAreaID = p.AccessAreaID
       JOIN LocationBuilding lb on lb.BuildingID = aa.BuildingID
       JOIN LocationCity lc on lc.CityID = lb.CityID
       JOIN LocationCountry c on c.CountryID = lc.CountryID
	  LEFT OUTER JOIN LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.Enabled = 1
       WHERE (p.EndDate is null or p.EndDate > getdate())
				 AND ISNULL(bpo.CountryID, c.CountryID) <> @homeCountryID
				 AND dbo.HasActivePassOfficers(isnull(bpo.PassOfficeID, c.PassOfficeID)) = 1
                     AND p.dbPeopleId = @dbPeopleId
	  UNION
	  SELECT DISTINCT PassOfficeID 
	  FROM LocationPassOffice 
	  WHERE CountryID = @homeCountryID AND PassOfficeID<>@homePassOfficeID AND Enabled=1

	declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 19,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end
else
begin
	--make a cursor to iterate through all the pass offices besides their home pass office
	declare passOfficeCursor cursor local fast_forward for
		select PassOfficeID 
		from LocationPassOffice l
		where l.Enabled=1
			and l.PassOfficeID <> @homePassOfficeID
			and dbo.HasActivePassOfficers(l.PassOfficeID) = 1
			
	--declare @passOfficeID int
	open passOfficeCursor
	fetch next from passOfficeCursor into @passOfficeID

	--make requests for the pass offices
	while (@@FETCH_STATUS = 0)
		begin
			exec dbo.up_InsertRequest
				@CreatedByUserID = @CreatedByID,
				@RequestTypeID = 19,
				@AccessAreaID = null,
				@RequestPersonID = @RequestPersonID,
				@StartDate = null,
				@EndDate = null,
				@BadgeID = null,
				@DivisionID = null,
				@iPassOfficeID = @passOfficeID,
				@BusinessAreaID = null,
				@RequestID = @RequestID output,
				@ExternalSystemRequestID = null,
				@ExternalSystemID = null,
				@ClassificationID = null
			
			fetch next from passOfficeCursor into @passOfficeID
		end

	close passOfficeCursor
	DEALLOCATE passOfficeCursor
end


--submit the request
declare @success bit
exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output


end


GO


