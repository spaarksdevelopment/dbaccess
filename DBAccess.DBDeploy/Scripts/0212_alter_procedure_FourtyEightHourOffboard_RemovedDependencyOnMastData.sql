IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FourtyEightHourOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[FourtyEightHourOffBoarding]
END
GO

/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 06/01/2016 12:12:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--       16/10/2015 Wijitha - Modified the logic of selecting offboarding users to select revoked users by CAT DEL triggers as well
--       22/10/2015 Wijitha - Fixed issue of not sending user's home country's pass officers, when the default pass office is outside of home country
--       11/11/2015 Wijitha - Modifed SP to user generic helper methods to create requests and send emails
--       24/11/2015 Wijitha - Modified offboard logic to offboard a user 7 days after the Revoke date. (Not based on the Termination date)
--       06/01/2016 Martin - Removed dependency on master data in order to prevent offboards failing for people missing from master data file
--======================================================================

CREATE Procedure [dbo].[FourtyEightHourOffBoarding] 
AS
BEGIN 

SET NOCOUNT ON

DECLARE @HRUpdates TABLE 
(
RowNumber int, 
dbPeopleID int, 
messageId int, 
isCATRevoke bit
) 
    
INSERT INTO @HRUpdates 
SELECT ROW_NUMBER() OVER (ORDER BY offboard.dbPeopleID) AS RowNumber, offboard.dbPeopleId, offboard.TransactionId, offboard.IsCATRevoke
FROM 
	(SELECT hrm.HRID as dbPeopleID,
		hrm.TransactionId,
		0 as isCATRevoke
	FROM HRFeedMessage hrm
		INNER JOIN HRPerson hp ON hrm.HRID=hp.dbPeopleID
	WHERE hrm.Processed = 0 AND 
		hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
		AND DATEADD(d, 7, hrm.RevokeAccessDateTimestamp) <= GETDATE() 
	
	UNION

	SELECT c.DBPeopleID, 
		c.TransactionID, 
		1 AS isCATRevoke 
	FROM CATDeleteOffboard c
	WHERE c.Processed = 0 AND DATEADD(d, 7, c.CreatedDate) <= GETDATE()
	) AS offboard

DECLARE @NumberofRows int
SELECT @NumberofRows = Count(*) FROM @HRUpdates

IF (@NumberofRows > 0)
BEGIN	

	DECLARE @CreatedByID int = null
	DECLARE @dbPeopleID int
	DECLARE @messageId int
	DECLARE @isCATRevoke bit
	DECLARE @intFlag int = 1

	WHILE (@intFlag <= @NumberOfRows)
	BEGIN

		SELECT @dbPeopleID = dbPeopleID, 
			@messageId = messageId, 
			@isCATRevoke = isCATRevoke 
		FROM @HRUpdates 
		WHERE RowNumber = @intFlag

		--Execute SP to off board single user
		EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID

		--Mark triggers as processed
		IF(@isCATRevoke = 1)
		BEGIN
			UPDATE CATDeleteOffboard
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END
		ELSE 
		BEGIN
			UPDATE HRFeedMessage
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END

		SET @intFlag = @intFlag + 1

	END			
END
END


GO


--//@UNDO



IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FourtyEightHourOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[FourtyEightHourOffBoarding]
END
GO

/****** Object:  StoredProcedure [dbo].[FourtyEightHourOffBoarding]    Script Date: 06/01/2016 12:12:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--       16/10/2015 Wijitha - Modified the logic of selecting offboarding users to select revoked users by CAT DEL triggers as well
--       22/10/2015 Wijitha - Fixed issue of not sending user's home country's pass officers, when the default pass office is outside of home country
--       11/11/2015 Wijitha - Modifed SP to user generic helper methods to create requests and send emails
--       24/11/2015 Wijitha - Modified offboard logic to offboard a user 7 days after the Revoke date. (Not based on the Termination date)
--======================================================================

CREATE Procedure [dbo].[FourtyEightHourOffBoarding] 
AS
BEGIN 

SET NOCOUNT ON

DECLARE @HRUpdates TABLE 
(
RowNumber int, 
dbPeopleID int, 
messageId int, 
isCATRevoke bit
) 
    
INSERT INTO @HRUpdates 
SELECT ROW_NUMBER() OVER (ORDER BY offboard.dbPeopleID) AS RowNumber, offboard.dbPeopleId, offboard.TransactionId, offboard.IsCATRevoke
FROM 
	(SELECT md.dbPeopleID,
		hrm.TransactionId,
		0 as isCATRevoke
	FROM HRFeedMessage hrm
		INNER JOIN MasterData md ON hrm.HRID = md.HR_ID
		INNER JOIN HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
	WHERE hrm.Processed = 0 AND 
		hrm.TriggerType = 'LVER' AND hrm.TriggerAction <> 'DEL'
		AND DATEADD(d, 7, hrm.RevokeAccessDateTimestamp) <= GETDATE() 
	
	UNION

	SELECT c.DBPeopleID, 
		c.TransactionID, 
		1 AS isCATRevoke 
	FROM CATDeleteOffboard c
	WHERE c.Processed = 0 AND DATEADD(d, 7, c.CreatedDate) <= GETDATE()
	) AS offboard

DECLARE @NumberofRows int
SELECT @NumberofRows = Count(*) FROM @HRUpdates

IF (@NumberofRows > 0)
BEGIN	

	DECLARE @CreatedByID int = null
	DECLARE @dbPeopleID int
	DECLARE @messageId int
	DECLARE @isCATRevoke bit
	DECLARE @intFlag int = 1

	WHILE (@intFlag <= @NumberOfRows)
	BEGIN

		SELECT @dbPeopleID = dbPeopleID, 
			@messageId = messageId, 
			@isCATRevoke = isCATRevoke 
		FROM @HRUpdates 
		WHERE RowNumber = @intFlag

		--Execute SP to off board single user
		EXEC [dbo].[PassOfficeUserOffBoarding] @dbPeopleId = @dbPeopleID, @Valid = 1, @CreatedByID = @CreatedByID

		--Mark triggers as processed
		IF(@isCATRevoke = 1)
		BEGIN
			UPDATE CATDeleteOffboard
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END
		ELSE 
		BEGIN
			UPDATE HRFeedMessage
			SET Processed = 1, ProcessedDate = GETDATE()
			WHERE TransactionId = @messageId
		END

		SET @intFlag = @intFlag + 1

	END			
END
END


GO
