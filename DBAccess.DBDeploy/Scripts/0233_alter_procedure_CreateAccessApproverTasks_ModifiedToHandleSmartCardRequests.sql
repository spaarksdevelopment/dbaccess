IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateAccessApproverTasks]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[CreateAccessApproverTasks]
END
GO

/****** Object:  StoredProcedure [dbo].[CreateAccessApproverTasks]    Script Date: 2/28/2017 6:46:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- History: 
--			22/02/2017 (Wijitha) Modified SP to create approver tasks for smart card requests
-- =============================================
CREATE PROCEDURE [dbo].[CreateAccessApproverTasks]
		  @RequestID INT,
		  @dtNow DateTime2,
		  @dbPeopleID int,
		  @doToday Date
	AS
	BEGIN
    
    -----------------------------------------------
    -- Now create the Access approval Tasks
    -----------------------------------------------
    INSERT INTO Tasks
    (
		  TaskTypeId,
		  TaskStatusId,
		  RequestId,
		  DivisionId,
		  BusinessAreaId,
		  AccessAreaId,
		  [Description],
		  CreatedByID,
		  CreatedDate
    )
    SELECT
		  TaskTypeLookup.TaskTypeId
		  ,1          -- Pending
		  ,AccessRequest.RequestID
		  ,AccessRequest.DivisionID
		  ,AccessRequestPerson.BusinessAreaID
		  ,AccessRequest.AccessAreaID
		  ,Replace(Replace(Replace(TaskTypeLookup.[Description],
				'{1}', AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname + ' ( ' + (CASE HRPerson.IsExternal WHEN 1 THEN N'C' + Cast(AccessRequestPerson.dbPeopleID As NVarChar(7)) + ', ' + isnull(AccessRequestPerson.EmailAddress,'') + ', ' + (SELECT Cast(DBDirID AS NVarChar(25))  FROM dbo.HRPerson WHERE dbPeopleID = AccessRequestPerson.dbPeopleID)  ELSE N' ' + Cast(AccessRequestPerson.dbPeopleID As NVarChar(7)) + ', ' + isnull(AccessRequestPerson.EmailAddress,'') + ', ' + (SELECT Cast(DBDirID AS NVarChar(25))  FROM dbo.HRPerson WHERE dbPeopleID = AccessRequestPerson.dbPeopleID) END) + ' )'),
				'{2}', IsNull(HRPerson.CountryName,'')),
				'{3}', IsNull ((SELECT Top 1 [UBR] FROM [HRPerson]  WHERE [dbPeopleID] = HRPerson.dbPeopleID) + ' - ' + CorporateDivision.Name, '')) As [Description]
		  ,@dbPeopleID
		  ,@dtNow
    FROM
		  AccessRequest
    INNER JOIN
		  AccessRequestType
		  On
		  AccessRequestType.RequestTypeID = AccessRequest.RequestTypeID
    INNER JOIN
		  AccessRequestPerson
		  On
		  AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
    INNER JOIN
		  HRPerson
		  On
		  HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
    Left Outer JOIN
		  vw_AccessArea
		  On
		  vw_AccessArea.AccessAreaID = AccessRequest.AccessAreaID
    Left Outer JOIN
		  CorporateDivision
		  On
		  CorporateDivision.DivisionID = AccessRequestPerson.DivisionID
    Cross JOIN
		  TaskTypeLookup
    WHERE
		  TaskTypeLookup.TaskTypeId = case when AccessRequest.RequestTypeID = 1 then 18 else 3 end   -- If a smart card request create 'SmartCardApprove' task. Else 'AccessApprove' task
		  And
		  AccessRequest.RequestID = @RequestId;

    -- Assign the Task to the appropriate users             

    DECLARE @iTaskID INT = 0;
    DECLARE @iDivisionID INT = 0;
    DECLARE @iAccessAreaID INT = 0;
    DECLARE @iClassificationID INT = 0;
    DECLARE @DoesAccessAreaUseConcurrentApprovals BIT = 0;
    DECLARE @iNumberOfApprovals INT = 0;

    SET @iTaskID = Scope_Identity();

    -- get the classification for the associated user
    SELECT
		  @iDivisionID = CorporateDivisionAccessArea.DivisionID,
		  @iAccessAreaID = CorporateDivisionAccessArea.AccessAreaID,
		  @iClassificationID = IsNull (HRClassification.HRClassificationID, 0),
		  @DoesAccessAreaUseConcurrentApprovals = (CASE CorporateDivisionAccessArea.ApprovalTypeID WHEN 2 THEN 1 ELSE 0 END),
		  @iNumberOfApprovals = CorporateDivisionAccessArea.NumberOfApprovals
    FROM
		  Tasks
    INNER JOIN
		  AccessRequest
		  On
		  AccessRequest.RequestID = Tasks.RequestId
    INNER JOIN
		  AccessRequestPerson
		  On
		  AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
    INNER JOIN
		  CorporateDivisionAccessArea
		  On
		  CorporateDivisionAccessArea.DivisionID = AccessRequest.DivisionID
		  And
		  CorporateDivisionAccessArea.AccessAreaID = AccessRequest.AccessAreaID
    INNER JOIN
		  HRPerson
		  On
		  HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
    Left Outer JOIN
		  HRClassification
		  On
		  HRClassification.Classification = HRPerson.Class
    WHERE
		  Tasks.TaskId = @iTaskID;

    IF (0 = @iAccessAreaID) RaisError ('Cannot identify Access Area for Task.', 11, 1);

    INSERT INTO
		  TasksUsers
    (
		  TaskId,
		  dbPeopleID,
		  TaskStatusId,
		  IsCurrent,
		  [Order],
		  CreatedDateTime,
		  mp_SecurityGroupID
    )
    SELECT
		  @iTaskID,
		  dbPeopleID,
		  1,          -- Pending
		  0,
		  ID,
		  @dtNow,
		  RoleID
    FROM
		  [dbo].[GetAvailableAccessApprovers] (@iDivisionID, @iAccessAreaID, @iClassificationID);

    IF (Not Exists (SELECT TasksUsersId FROM TasksUsers WHERE TaskId = @iTaskID)) RaisError (N'No Approvers available for Task.', 11, 1);

    DECLARE @mp_SecurityGroupID INT = 5; --  AAs
    EXEC [dbo].[MarkAccessApproverTaskUsersAsPending] @DoesAccessAreaUseConcurrentApprovals, @iTaskID, @iNumberOfApprovals, @doToday, @dtNow, @mp_SecurityGroupID
END
GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateAccessApproverTasks]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[CreateAccessApproverTasks]
END
GO

/****** Object:  StoredProcedure [dbo].[CreateAccessApproverTasks]    Script Date: 2/22/2017 5:58:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAccessApproverTasks]
		  @RequestID INT,
		  @dtNow DateTime2,
		  @dbPeopleID int,
		  @doToday Date
	AS
	BEGIN
    
    -----------------------------------------------
    -- Now create the Access approval Tasks
    -----------------------------------------------
    INSERT INTO Tasks
    (
		  TaskTypeId,
		  TaskStatusId,
		  RequestId,
		  DivisionId,
		  BusinessAreaId,
		  AccessAreaId,
		  [Description],
		  CreatedByID,
		  CreatedDate
    )
    SELECT
		  TaskTypeLookup.TaskTypeId
		  ,1          -- Pending
		  ,AccessRequest.RequestID
		  ,AccessRequest.DivisionID
		  ,AccessRequestPerson.BusinessAreaID
		  ,AccessRequest.AccessAreaID
		  ,Replace(Replace(Replace(TaskTypeLookup.[Description],
				'{1}', AccessRequestPerson.Forename + ' ' + AccessRequestPerson.Surname + ' ( ' + (CASE HRPerson.IsExternal WHEN 1 THEN N'C' + Cast(AccessRequestPerson.dbPeopleID As NVarChar(7)) + ', ' + isnull(AccessRequestPerson.EmailAddress,'') + ', ' + (SELECT Cast(DBDirID AS NVarChar(25))  FROM dbo.HRPerson WHERE dbPeopleID = AccessRequestPerson.dbPeopleID)  ELSE N' ' + Cast(AccessRequestPerson.dbPeopleID As NVarChar(7)) + ', ' + isnull(AccessRequestPerson.EmailAddress,'') + ', ' + (SELECT Cast(DBDirID AS NVarChar(25))  FROM dbo.HRPerson WHERE dbPeopleID = AccessRequestPerson.dbPeopleID) END) + ' )'),
				'{2}', IsNull(HRPerson.CountryName,'')),
				'{3}', IsNull ((SELECT Top 1 [UBR] FROM [HRPerson]  WHERE [dbPeopleID] = HRPerson.dbPeopleID) + ' - ' + CorporateDivision.Name, '')) As [Description]
		  ,@dbPeopleID
		  ,@dtNow
    FROM
		  AccessRequest
    INNER JOIN
		  AccessRequestType
		  On
		  AccessRequestType.RequestTypeID = AccessRequest.RequestTypeID
    INNER JOIN
		  AccessRequestPerson
		  On
		  AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
    INNER JOIN
		  HRPerson
		  On
		  HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
    Left Outer JOIN
		  vw_AccessArea
		  On
		  vw_AccessArea.AccessAreaID = AccessRequest.AccessAreaID
    Left Outer JOIN
		  CorporateDivision
		  On
		  CorporateDivision.DivisionID = AccessRequestPerson.DivisionID
    Cross JOIN
		  TaskTypeLookup
    WHERE
		  TaskTypeLookup.TaskTypeId = 3       -- approve access
		  And
		  AccessRequest.RequestID = @RequestId;

    -- Assign the Task to the appropriate users             

    DECLARE @iTaskID INT = 0;
    DECLARE @iDivisionID INT = 0;
    DECLARE @iAccessAreaID INT = 0;
    DECLARE @iClassificationID INT = 0;
    DECLARE @DoesAccessAreaUseConcurrentApprovals BIT = 0;
    DECLARE @iNumberOfApprovals INT = 0;

    SET @iTaskID = Scope_Identity();

    -- get the classification for the associated user
    SELECT
		  @iDivisionID = CorporateDivisionAccessArea.DivisionID,
		  @iAccessAreaID = CorporateDivisionAccessArea.AccessAreaID,
		  @iClassificationID = IsNull (HRClassification.HRClassificationID, 0),
		  @DoesAccessAreaUseConcurrentApprovals = (CASE CorporateDivisionAccessArea.ApprovalTypeID WHEN 2 THEN 1 ELSE 0 END),
		  @iNumberOfApprovals = CorporateDivisionAccessArea.NumberOfApprovals
    FROM
		  Tasks
    INNER JOIN
		  AccessRequest
		  On
		  AccessRequest.RequestID = Tasks.RequestId
    INNER JOIN
		  AccessRequestPerson
		  On
		  AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
    INNER JOIN
		  CorporateDivisionAccessArea
		  On
		  CorporateDivisionAccessArea.DivisionID = AccessRequest.DivisionID
		  And
		  CorporateDivisionAccessArea.AccessAreaID = AccessRequest.AccessAreaID
    INNER JOIN
		  HRPerson
		  On
		  HRPerson.dbPeopleID = AccessRequestPerson.dbPeopleID
    Left Outer JOIN
		  HRClassification
		  On
		  HRClassification.Classification = HRPerson.Class
    WHERE
		  Tasks.TaskId = @iTaskID;

    IF (0 = @iAccessAreaID) RaisError ('Cannot identify Access Area for Task.', 11, 1);

    INSERT INTO
		  TasksUsers
    (
		  TaskId,
		  dbPeopleID,
		  TaskStatusId,
		  IsCurrent,
		  [Order],
		  CreatedDateTime,
		  mp_SecurityGroupID
    )
    SELECT
		  @iTaskID,
		  dbPeopleID,
		  1,          -- Pending
		  0,
		  ID,
		  @dtNow,
		  RoleID
    FROM
		  [dbo].[GetAvailableAccessApprovers] (@iDivisionID, @iAccessAreaID, @iClassificationID);

    IF (Not Exists (SELECT TasksUsersId FROM TasksUsers WHERE TaskId = @iTaskID)) RaisError (N'No Approvers available for Task.', 11, 1);

    DECLARE @mp_SecurityGroupID INT = 5; --  AAs
    EXEC [dbo].[MarkAccessApproverTaskUsersAsPending] @DoesAccessAreaUseConcurrentApprovals, @iTaskID, @iNumberOfApprovals, @doToday, @dtNow, @mp_SecurityGroupID
END
GO
