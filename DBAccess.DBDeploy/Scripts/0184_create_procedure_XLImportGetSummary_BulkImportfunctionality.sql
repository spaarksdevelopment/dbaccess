IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportGetSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportGetSummary]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportGetSummary]    Script Date: 1/7/2016 4:59:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 23/12/2015
-- Description: Get summay of a bulk data import for a given FileID
--===============================================================
CREATE PROCEDURE [dbo].[XLImportGetSummary]
@FileID int
AS
BEGIN

	SELECT e.SheetName, e.DestinationTable as RelatedTable, IsNUll(h.RecordCount, 0) as RecordCount
	FROM XLImportEntity e
	OUTER APPLY 
	(
		SELECT COUNT(RelatedID) AS RecordCount  
		FROM XLImportHistory h
			LEFT OUTER JOIN CorporateDivisionAccessAreaApprover aaa on h.RelatedTable = 'CorporateDivisionAccessAreaApprover' and h.RelatedID = aaa.CorporateDivisionAccessAreaApproverId
			LEFT OUTER JOIN mp_SecurityGroup sg on aaa.mp_SecurityGroupID = sg.Id
		WHERE h.FileID=@FileID and CASE WHEN sg.RoleShortName = 'AR' THEN 'AccessAreaRecertifiers' ELSE RelatedTable END = CASE WHEN e.Entity='AccessAreaRecertifiers' THEN e.Entity ELSE e.DestinationTable END
		GROUP BY h.RelatedTable, sg.RoleShortName 
	) h
	ORDER BY e.SortOrder

END

GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportGetSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportGetSummary]
END
GO