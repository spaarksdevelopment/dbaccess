IF NOT EXISTS(SELECT TaskTypeId FROM dbo.TaskTypeLookup WHERE TaskTypeID=16)
BEGIN
	INSERT INTO [dbo].[TaskTypeLookup]
           ([TaskTypeId]
           ,[Type]
           ,[Description]
           ,[EmailTemplateId]
           ,[RoleId])
     VALUES
           (16
           ,'SuspendAccess'
           ,'Suspend Access for {6} pass for {1}{5}'
           ,NULL
           ,NULL)
END
GO

IF NOT EXISTS(SELECT RequestTypeID FROM dbo.AccessRequestType WHERE RequestTypeID=19)
BEGIN
	INSERT INTO [dbo].[AccessRequestType]
			   ([RequestTypeID]
			   ,[Name]
			   ,[Description]
			   ,[Enabled]
			   ,[TaskTypeID]
			   ,[GermanName]
			   ,[ItalyName])
		 VALUES
			   (
				   19,
				   'Suspend Access',
				   'Suspend Access for a User',
				   1,
				   16,
				   'Suspendieren Zugang',
				   'Sospendere l''Accesso'
			   )
END
GO



--//@UNDO

IF EXISTS(SELECT TaskTypeId FROM dbo.TaskTypeLookup WHERE TaskTypeID=16)
	DELETE [dbo].[TaskTypeLookup] WHERE TaskTypeID=16 
GO

IF EXISTS(SELECT RequestTypeID FROM dbo.AccessRequestType WHERE RequestTypeID=19)
	DELETE dbo.AccessRequestType WHERE RequestTypeID=19
GO