IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageDivision')
BEGIN

CREATE TABLE [dbo].[XLImportStageDivision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[UBR] [nvarchar](500) NULL,
	[Country] [nvarchar](500) NULL,
	[RecertPeriod] [nvarchar](500) NULL,
	[IsSubdivision] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
	[ParentDivision] [nvarchar](500) NULL,
 CONSTRAINT [PK_XLImportStageDivision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[XLImportStageDivision]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStageDivision_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStageDivision] CHECK CONSTRAINT [FK_XLImportStageDivision_XLImportFile]

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageDivision')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageDivision]
	
END

GO