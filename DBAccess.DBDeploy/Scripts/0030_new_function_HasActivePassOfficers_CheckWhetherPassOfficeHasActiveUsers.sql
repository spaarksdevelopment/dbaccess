
/****** Object:  UserDefinedFunction [dbo].[HasActivePassOfficers]    Script Date: 6/1/2015 5:37:22 PM ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HasActivePassOfficers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[HasActivePassOfficers]
'
END
GO

/****** Object:  UserDefinedFunction [dbo].[HasActivePassOfficers]    Script Date: 6/1/2015 5:37:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================
-- Author: Wijitha
-- Created Date: 0/06/2015
-- Description: Check whether pass office has active pass officers
-- History: 
-- ================================================
CREATE FUNCTION [dbo].[HasActivePassOfficers]
(
	@PassOfficeID int
)
RETURNS bit
AS
BEGIN
	
	DECLARE @hasPassOfficers bit = 0

	if((select count(*)
		from LocationPassOfficeUser
		where PassOfficeID = @PassOfficeID
			and IsEnabled = 1) > 0)
	begin
	 set @hasPassOfficers = 1
	end	
	
	RETURN @hasPassOfficers

END

GO

--//@UNDO

/****** Object:  UserDefinedFunction [dbo].[HasActivePassOfficers]    Script Date: 6/1/2015 5:37:22 PM ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HasActivePassOfficers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[HasActivePassOfficers]
'
END
GO


