
DECLARE @AccessAreaName VARCHAR(200) = 'Smart Card Approval'
 
DECLARE @RegionId INT , @PassOfficeId INT
SELECT @RegionId=RegionID, @PassOfficeId=PassOfficeID
FROM LocationCountry
WHERE Name='United Kingdom'
 
IF (NOT EXISTS (SELECT * FROM AccessArea WHERE Name = @AccessAreaName) AND @RegionId > 0 AND @PassOfficeId > 0)
BEGIN
       DECLARE @tblIds table (Name VARCHAR(50), Value int)
 
       INSERT INTO LocationCountry (RegionId, Name, Enabled, passOfficeId)
       OUTPUT 'CountryID', INSERTed.CountryId
       INTO @tblIds
       VALUES (@RegionId, 'SmartCard_Country', 1, @PassOfficeId)
 
       INSERT INTO LocationCity (CountryID, Name, Enabled, TimeZoneID)
       OUTPUT 'CityID', INSERTed.CityId
       INTO @tblIds
       VALUES ((SELECT Value FROM @tblIds WHERE name='CountryID'), 'SmartCard_City', 1, 16)
 
       INSERT INTO LocationBuilding (CityID, Name, Enabled)
       OUTPUT 'BuildingID', INSERTed.BuildingId
       INTO @tblIds
       VALUES ((SELECT Value FROM @tblIds WHERE name='CityID'), 'SmartCard_Building', 1)
 
       INSERT INTO LocationFloor (BuildingID, Name, Enabled)
       OUTPUT 'FloorID', INSERTed.FloorID
       INTO @tblIds
       VALUES ((SELECT Value FROM @tblIds WHERE name='BuildingID'), 'SMF', 1)
 
       INSERT INTO CorporateDivision (CountryID, Name, UBR, Enabled, IsSubdivision, RecertPeriod)
       OUTPUT 'DivisionID', INSERTed.DivisionID
       INTO @tblIds
       VALUES ((SELECT Value FROM @tblIds WHERE name='CountryID'), 'G_0183-Dummy-Division-SmartCard','G_0183', 1, 0, 6)
 
       INSERT INTO AccessArea (AccessAreaTypeID, FloorID, BuildingID, Name, Enabled, ControlSystemID, AccessControlID, RecertPeriod)
       OUTPUT 'AccessAreaID', INSERTed.AccessAreaID
       INTO @tblIds
       VALUES (1, (SELECT Value FROM @tblIds WHERE name='FloorID'), (SELECT Value FROM @tblIds WHERE name='BuildingID'), @AccessAreaName, 1, 1, 1552, 12)
      
       INSERT INTO CorporateDivisionAccessArea (DivisionID, AccessAreaID, Enabled, NumberOfApprovals, ApprovalTypeID, Frequency, lastCertifiedDate, DateNextCertification)
       VALUES (
       (SELECT Value FROM @tblIds WHERE name='DivisionID'),
       (SELECT Value FROM @tblIds WHERE name='AccessAreaID'),
       1,1,2,12, getdate(),'2018-12-31')
 
END
ELSE
BEGIN
       PRINT 'Access Area already exists'
END

GO

--//@UNDO

DECLARE @DeleteAccessAreaName VARCHAR(200) = 'Smart Card Approval'

DECLARE @tblDeleteIds table (Name VARCHAR(50), Value int)

INSERT INTO @tblDeleteIds
SELECT 'AccessAreaId', AccessAreaID FROM AccessArea WHERE Name=@DeleteAccessAreaName

INSERT INTO @tblDeleteIds
SELECT 'DivisionID', DivisionID FROM CorporateDivisionAccessArea WHERE AccessAreaId= (SELECT Value FROM @tblDeleteIds WHERE Name='AccessAreaId')

INSERT INTO @tblDeleteIds
SELECT 'FloorId', FloorID FROM AccessArea 
WHERE AccessAreaId= (SELECT Value FROM @tblDeleteIds WHERE Name='AccessAreaId')

INSERT INTO @tblDeleteIds
SELECT 'BuildingId', BuildingID FROM AccessArea 
WHERE AccessAreaId= (SELECT Value FROM @tblDeleteIds WHERE Name='AccessAreaId')

INSERT INTO @tblDeleteIds
SELECT 'CityId', CityID FROM LocationBuilding 
WHERE BuildingID= (SELECT Value FROM @tblDeleteIds WHERE Name='BuildingId')

INSERT INTO @tblDeleteIds
SELECT 'CountryId', CountryID FROM LocationCity 
WHERE CityID= (SELECT Value FROM @tblDeleteIds WHERE Name='CityId')

DELETE CorporateDivisionAccessArea WHERE AccessAreaID in (SELECT Value FROM @tblDeleteIds WHERE Name='AccessAreaId')
DELETE CorporateDivision WHERE DivisionID in (SELECT Value FROM @tblDeleteIds WHERE Name='DivisionId')
DELETE AccessArea WHERE AccessAreaID in (SELECT Value FROM @tblDeleteIds WHERE Name='AccessAreaId')
DELETE LocationFloor WHERE FloorId in (SELECT Value FROM @tblDeleteIds WHERE Name='FloorId')
DELETE LocationBuilding WHERE BuildingId in (SELECT Value FROM @tblDeleteIds WHERE Name='BuildingId')
DELETE LocationCity WHERE CityID in (SELECT Value FROM @tblDeleteIds WHERE Name='CityId')
DELETE LocationCountry WHERE CountryID in (SELECT Value FROM @tblDeleteIds WHERE Name='CountryId')


GO