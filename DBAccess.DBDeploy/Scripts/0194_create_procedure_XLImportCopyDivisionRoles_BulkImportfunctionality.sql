IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyDivisionRoles]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyDivisionRoles]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 21/12/2015
-- Description: Copy Division Roles from staging table to the CorporateDivisionRoleUser table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyDivisionRoles]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)
	
	INSERT INTO CorporateDivisionRoleUser (DivisionID, mp_SecurityGroupID, DateAccepted, DateLastCertified, IsRecertified, dbPeopleID)
	OUTPUT inserted.DivisionRoleUserID INTO @tblIDs
	SELECT (SELECT DivisionID 
			FROM CorporateDivision
			WHERE CountryID = C.CountryID AND Name = sd.Name),
		(SELECT Id FROM mp_SecurityGroup WHERE RoleName = SecurityGroup),
		CAST(DateAccepted AS datetime2),
		CAST(DateLastCertified AS datetime2),
		1,
		CAST(DBPeopleID AS INT)
	FROM XLImportStageDivisionRole dr 
		INNER JOIN XLImportStageDivision sd on dr.Division = sd.Name
		INNER JOIN LocationCountry c on sd.Country = c.Name
	WHERE dr.FileID  = @FileID AND ISNULL(dr.ValidationFailed, 0) = 0

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivisionRoleUser', ID FROM @tblIDs

END



GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyDivisionRoles]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyDivisionRoles]
END
GO