IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'HRFeedMessageTasksUsersCreationFailed'))
BEGIN


CREATE TABLE [dbo].[HRFeedMessageTasksUsersCreationFailed](
	[HRFeedMessageTasksUsersCreationFailedID] [int] IDENTITY(1,1) NOT NULL,
	[dbPeopleID] [int] NOT NULL,
	[RequestMasterID] [int] NOT NULL,
	[TransactionID] [int] NOT NULL,
	[Attempts] [int] NULL,
	[Processed] [bit] NULL,
	[Succeeded] [bit] NULL,
 CONSTRAINT [PK_HRFeedMessageFailed] PRIMARY KEY CLUSTERED 
(
	[HRFeedMessageTasksUsersCreationFailedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]




END

GO

--//@UNDO




