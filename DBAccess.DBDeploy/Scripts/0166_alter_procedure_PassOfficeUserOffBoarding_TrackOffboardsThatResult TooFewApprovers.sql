IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PassOfficeUserOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[PassOfficeUserOffBoarding]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=================================================
-- History: 
--		26/05/2015 (Asanka) - Modified the logic of taking pass office users based on the building pass office.
--      11/11/2015 (Wijitha) - Modifed the SP to use generic helper methods for request creation and email sending
--      02/12/2015 (Asanka) - Return RequestMasterID
--      01/01/2016 (Asanka) - Track offboards that results access areas having too few approvers 
--=================================================

CREATE PROCEDURE [dbo].[PassOfficeUserOffBoarding]
	@dbPeopleId int,
	@Valid bit,
	@CreatedByID int
AS
begin

DECLARE @RequestMasterID int

--Should already be revoked ie can't login or be the subject of any requests but just in case
UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

--Create requests for pass officers
EXEC @RequestMasterID = dbo.HRFeed_CreatePassOfficeRequestsForUser @dbPeopleId = @dbPeopleId, @requestTypeId = 15, @createdByID = @CreatedByID

--Send emails to pass offices
EXEC dbo.HRFeed_QueuePassOfficeRequestEmailsForUser @dbPeopleID = @dbPeopleID, @EmailTypeID = 36
 
 --Offboarded people should be disabled as approvers
UPDATE CorporateDivisionAccessAreaApprover
SET [Enabled]=0
WHERE dbPeopleID=@dbPeopleID

--Delete the person's requests
DECLARE @HasBadges bit
SELECT @HasBadges = count(PersonBadgeID)
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid
 
EXEC dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @HasBadges
 
 --Insert a record to OffBoardResultTooFewApprovers table if offboard results access areas having too few approvers 
IF(LEN([dbo].[GetAccessAreasHavingLesserApproversDueToOffboard](@dbPeopleID)) > 0)
BEGIN
	INSERT INTO OffBoardResultTooFewApprovers(RequestMasterID) VALUES (@RequestMasterID)
END

RETURN (@RequestMasterID)

END


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PassOfficeUserOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[PassOfficeUserOffBoarding]
END
GO


/****** Object:  StoredProcedure [dbo].[PassOfficeUserOffBoarding]    Script Date: 11/11/2015 3:52:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=================================================
-- History: 
--		26/05/2015 (Asanka) - Modified the logic of taking pass office users based on the building pass office.
--      11/11/2015 (Wijitha) - Modifed the SP to use generic helper methods for request creation and email sending
--      02/12/2015 (Asanka) - Return RequestMasterID
--=================================================

CREATE PROCEDURE [dbo].[PassOfficeUserOffBoarding]
	@dbPeopleId int,
	@Valid bit,
	@CreatedByID int
AS
begin

DECLARE @RequestMasterID int

--Should already be revoked ie can't login or be the subject of any requests but just in case
UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

--Create requests for pass officers
EXEC @RequestMasterID = dbo.HRFeed_CreatePassOfficeRequestsForUser @dbPeopleId = @dbPeopleId, @requestTypeId = 15, @createdByID = @CreatedByID

--Send emails to pass offices
EXEC dbo.HRFeed_QueuePassOfficeRequestEmailsForUser @dbPeopleID = @dbPeopleID, @EmailTypeID = 36
 
 --Offboarded people should be disabled as approvers
UPDATE CorporateDivisionAccessAreaApprover
SET [Enabled]=0
WHERE dbPeopleID=@dbPeopleID

--Delete the person's requests
DECLARE @HasBadges bit
SELECT @HasBadges = count(PersonBadgeID)
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid
 
EXEC dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @HasBadges
 
RETURN (@RequestMasterID)

END

GO


