IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_AccessRequest]'))
	DROP VIEW [dbo].vw_AccessRequest
GO

/****** Object:  View [dbo].[vw_AccessRequest]    Script Date: 2/28/2017 6:48:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_AccessRequest]
AS
SELECT        dbo.AccessRequest.RequestID, dbo.AccessRequest.RequestTypeID, dbo.AccessRequestType.Name AS RequestType, dbo.AccessRequestType.Description AS RequestTypeDescription, 
                         dbo.AccessRequestType.Enabled AS RequestTypeEnabled, dbo.AccessRequestStatus.RequestStatusID, dbo.AccessRequestStatus.Name AS RequestStatus, 
                         dbo.AccessRequestStatus.Description AS RequestStatusDescription, dbo.AccessRequestStatus.Enabled AS RequestStatusEnabled, dbo.AccessRequest.RequestPersonID, dbo.AccessRequestPerson.dbPeopleID, 
                         CASE IsExternal WHEN 1 THEN 'C' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) ELSE ' ' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) END AS FulldbPeopleID, 
                         dbo.AccessRequestPerson.Forename, dbo.AccessRequestPerson.Surname, dbo.AccessRequestPerson.CostCentre, dbo.AccessRequestPerson.Telephone, dbo.AccessRequestPerson.EmailAddress, 
                         dbo.AccessRequestPerson.VendorID, dbo.AccessRequestPerson.VendorName, dbo.AccessRequestPerson.RequestMasterID, dbo.AccessRequestMaster.CreatedByID AS MasterCreatedByUserID, 
                         dbo.AccessRequestMaster.Created AS MasterCreated, dbo.AccessArea.AccessAreaID, dbo.AccessArea.Name AS AccessArea, dbo.AccessArea.AccessAreaTypeID, dbo.AccessAreaType.AccessAreaTypeName, 
                         dbo.AccessAreaType.Description AS AccessAreaTypeDescription, dbo.AccessAreaType.Enabled AS AccessAreaTypeEnabled, dbo.AccessArea.FloorID, dbo.AccessArea.BuildingID, 
                         dbo.AccessArea.Enabled AS AccessAreaEnabled, dbo.AccessRequest.StartDate, dbo.AccessRequest.EndDate, dbo.AccessRequest.Approved, dbo.AccessRequest.ApprovedDate, dbo.AccessRequest.BadgeID, 
                         dbo.AccessRequest.CreatedByID, dbo.AccessRequest.Created, dbo.AccessRequest.ApprovedByID, dbo.AccessRequest.AccessApproved, dbo.AccessRequest.AccessApprovedDate, 
                         dbo.AccessRequest.AccessApprovedByID, dbo.AccessRequestPerson.CountryID, dbo.LocationCountry.Name AS CountryName, dbo.AccessRequest.DivisionID, dbo.CorporateDivision.Name AS DivisionName, 
                         dbo.AccessRequest.BusinessAreaID, dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.AccessRequestDeliveryDetail.DeliveryOptionID, dbo.AccessRequestDeliveryOption.DeliveryOption, 
                         dbo.AccessRequestDeliveryDetail.ContactEmail, dbo.AccessRequestDeliveryDetail.ContactTelephone, dbo.LocationPassOffice.PassOfficeID, dbo.LocationPassOffice.Name AS PassOfficeName, 
                         dbo.LocationPassOffice.Code AS PassOfficeCode, dbo.AccessArea.ControlSystemID, dbo.AccessArea.AccessControlID, dbo.AccessRequest.BadgeJustificationReasonID, 
                         dbo.AccessRequest.AccessRequestJustificationReasonID, dbo.AccessRequest.IsSmartCard, dbo.AccessRequest.SmartCardJustification
FROM            dbo.LocationCountry RIGHT OUTER JOIN
                         dbo.AccessRequestDeliveryOption INNER JOIN
                         dbo.AccessRequestDeliveryDetail ON dbo.AccessRequestDeliveryOption.DeliveryOptionID = dbo.AccessRequestDeliveryDetail.DeliveryOptionID RIGHT OUTER JOIN
                         dbo.AccessRequestMaster INNER JOIN
                         dbo.AccessRequestPerson ON dbo.AccessRequestMaster.RequestMasterID = dbo.AccessRequestPerson.RequestMasterID ON 
                         dbo.AccessRequestDeliveryDetail.RequestMasterID = dbo.AccessRequestMaster.RequestMasterID LEFT OUTER JOIN
                         dbo.LocationPassOffice ON dbo.AccessRequestDeliveryDetail.PassOfficeID = dbo.LocationPassOffice.PassOfficeID ON dbo.LocationCountry.CountryID = dbo.AccessRequestPerson.CountryID RIGHT OUTER JOIN
                         dbo.AccessArea INNER JOIN
                         dbo.AccessAreaType ON dbo.AccessArea.AccessAreaTypeID = dbo.AccessAreaType.AccessAreaTypeID RIGHT OUTER JOIN
                         dbo.AccessRequestType INNER JOIN
                         dbo.AccessRequestStatus INNER JOIN
                         dbo.AccessRequest ON dbo.AccessRequestStatus.RequestStatusID = dbo.AccessRequest.RequestStatusID ON dbo.AccessRequestType.RequestTypeID = dbo.AccessRequest.RequestTypeID LEFT OUTER JOIN
                         dbo.CorporateBusinessArea ON dbo.AccessRequest.BusinessAreaID = dbo.CorporateBusinessArea.BusinessAreaID LEFT OUTER JOIN
                         dbo.CorporateDivision ON dbo.AccessRequest.DivisionID = dbo.CorporateDivision.DivisionID ON dbo.AccessArea.AccessAreaID = dbo.AccessRequest.AccessAreaID ON 
                         dbo.AccessRequestPerson.RequestPersonID = dbo.AccessRequest.RequestPersonID

GO

--//@UNDO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_AccessRequest]'))
	DROP VIEW [dbo].vw_AccessRequest
GO

/****** Object:  View [dbo].[vw_AccessRequest]    Script Date: 2/22/2017 6:20:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_AccessRequest]
AS
SELECT     dbo.AccessRequest.RequestID, dbo.AccessRequest.RequestTypeID, dbo.AccessRequestType.Name AS RequestType, 
                      dbo.AccessRequestType.Description AS RequestTypeDescription, dbo.AccessRequestType.Enabled AS RequestTypeEnabled, 
                      dbo.AccessRequestStatus.RequestStatusID, dbo.AccessRequestStatus.Name AS RequestStatus, dbo.AccessRequestStatus.Description AS RequestStatusDescription, 
                      dbo.AccessRequestStatus.Enabled AS RequestStatusEnabled, dbo.AccessRequest.RequestPersonID, dbo.AccessRequestPerson.dbPeopleID, 
                      CASE IsExternal WHEN 1 THEN 'C' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) 
                      ELSE ' ' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) END AS FulldbPeopleID,
                      dbo.AccessRequestPerson.Forename, dbo.AccessRequestPerson.Surname, dbo.AccessRequestPerson.CostCentre, dbo.AccessRequestPerson.Telephone, 
                      dbo.AccessRequestPerson.EmailAddress, dbo.AccessRequestPerson.VendorID, dbo.AccessRequestPerson.VendorName, 
                      dbo.AccessRequestPerson.RequestMasterID, dbo.AccessRequestMaster.CreatedByID AS MasterCreatedByUserID, 
                      dbo.AccessRequestMaster.Created AS MasterCreated, dbo.AccessArea.AccessAreaID, dbo.AccessArea.Name AS AccessArea, dbo.AccessArea.AccessAreaTypeID, 
                      dbo.AccessAreaType.AccessAreaTypeName, dbo.AccessAreaType.Description AS AccessAreaTypeDescription, 
                      dbo.AccessAreaType.Enabled AS AccessAreaTypeEnabled, dbo.AccessArea.FloorID, dbo.AccessArea.BuildingID, dbo.AccessArea.Enabled AS AccessAreaEnabled, 
                      dbo.AccessRequest.StartDate, dbo.AccessRequest.EndDate, dbo.AccessRequest.Approved, dbo.AccessRequest.ApprovedDate, dbo.AccessRequest.BadgeID, 
                      dbo.AccessRequest.CreatedByID, dbo.AccessRequest.Created, dbo.AccessRequest.ApprovedByID, dbo.AccessRequest.AccessApproved, 
                      dbo.AccessRequest.AccessApprovedDate, dbo.AccessRequest.AccessApprovedByID, dbo.AccessRequestPerson.CountryID, 
                      dbo.LocationCountry.Name AS CountryName, dbo.AccessRequest.DivisionID, dbo.CorporateDivision.Name AS DivisionName, dbo.AccessRequest.BusinessAreaID, 
                      dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.AccessRequestDeliveryDetail.DeliveryOptionID, dbo.AccessRequestDeliveryOption.DeliveryOption, 
                      dbo.AccessRequestDeliveryDetail.ContactEmail, dbo.AccessRequestDeliveryDetail.ContactTelephone, dbo.LocationPassOffice.PassOfficeID, 
                      dbo.LocationPassOffice.Name AS PassOfficeName, dbo.LocationPassOffice.Code AS PassOfficeCode, dbo.AccessArea.ControlSystemID, 
                      dbo.AccessArea.AccessControlID,
                      dbo.AccessRequest.BadgeJustificationReasonID,
                      dbo.AccessRequest.AccessRequestJustificationReasonID
FROM         dbo.LocationCountry RIGHT OUTER JOIN
                      dbo.AccessRequestDeliveryOption INNER JOIN
                      dbo.AccessRequestDeliveryDetail ON dbo.AccessRequestDeliveryOption.DeliveryOptionID = dbo.AccessRequestDeliveryDetail.DeliveryOptionID RIGHT OUTER JOIN
                      dbo.AccessRequestMaster INNER JOIN
                      dbo.AccessRequestPerson ON dbo.AccessRequestMaster.RequestMasterID = dbo.AccessRequestPerson.RequestMasterID ON 
                      dbo.AccessRequestDeliveryDetail.RequestMasterID = dbo.AccessRequestMaster.RequestMasterID LEFT OUTER JOIN
                      dbo.LocationPassOffice ON dbo.AccessRequestDeliveryDetail.PassOfficeID = dbo.LocationPassOffice.PassOfficeID ON 
                      dbo.LocationCountry.CountryID = dbo.AccessRequestPerson.CountryID RIGHT OUTER JOIN
                      dbo.AccessArea INNER JOIN
                      dbo.AccessAreaType ON dbo.AccessArea.AccessAreaTypeID = dbo.AccessAreaType.AccessAreaTypeID RIGHT OUTER JOIN
                      dbo.AccessRequestType INNER JOIN
                      dbo.AccessRequestStatus INNER JOIN
                      dbo.AccessRequest ON dbo.AccessRequestStatus.RequestStatusID = dbo.AccessRequest.RequestStatusID ON 
                      dbo.AccessRequestType.RequestTypeID = dbo.AccessRequest.RequestTypeID LEFT OUTER JOIN
                      dbo.CorporateBusinessArea ON dbo.AccessRequest.BusinessAreaID = dbo.CorporateBusinessArea.BusinessAreaID LEFT OUTER JOIN
                      dbo.CorporateDivision ON dbo.AccessRequest.DivisionID = dbo.CorporateDivision.DivisionID ON 
                      dbo.AccessArea.AccessAreaID = dbo.AccessRequest.AccessAreaID ON dbo.AccessRequestPerson.RequestPersonID = dbo.AccessRequest.RequestPersonID


GO
