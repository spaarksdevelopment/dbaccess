IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessAreaRecertifier')
BEGIN

CREATE TABLE [dbo].[XLImportStageAccessAreaRecertifier](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DBPeopleID] [nvarchar](500) NULL,
	[Division] [nvarchar](500) NULL,
	[AccessArea] [nvarchar](500) NULL,
	[DateAccepted] [nvarchar](500) NULL,
	[DateNextCertification] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStagePassOfficeUser_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[XLImportStageAccessAreaRecertifier]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStagePassOfficeUser_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStageAccessAreaRecertifier] CHECK CONSTRAINT [FK_XLImportStagePassOfficeUser_XLImportFile]

END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageAccessAreaRecertifier')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageAccessAreaRecertifier]
	
END

GO