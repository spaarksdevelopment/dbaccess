IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO

/****** Object:  StoredProcedure [dbo].[up_CreateAccessTrigger_DEL]    Script Date: 11/13/2015 12:54:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 10/10/2015
-- Description: Process create access trigger's 'DEL' action. Set HRPerson and mp_user as reoked,
--              cancel any pending pass requests and access requests and offboard if active user
-- History: 13/11/2015 (Wijitha) - Used generic SPs of request creation and email sending
--============================================================

CREATE procedure [dbo].[up_CreateAccessTrigger_DEL]
@HRFeedMessageID int
as
begin

--Get DBPeopleID from trigger
DECLARE @dbPeopleID int 
DECLARE @transactionID int
SELECT @dbPeopleID = cast(HRID as int), @transactionID = TransactionId from HRFeedMessage where HRFeedMessageId = @HRFeedMessageID

--Do nothing if HRPerson record hasn't been created
IF(Not Exists(select * from HRPerson where dbPeopleID = @dbPeopleID))
	return

DECLARE @valid bit = 1
DECLARE @CreatedByID int = null

--Update HRPerson as revoked
update HRperson set Revoked=1 where dbPeopleID = @dbPeopleID
update mp_user set Revoked=1 where dbPeopleID = @dbPeopleID

--Check whether the person has active badges
DECLARE @hasBadges bit = 0
SELECT @hasBadges = count(PersonBadgeID) 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid and Active = 1

--delete the person's pending requests if any
EXEC dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, 0

--Create revoke requests if person has approved or actioned pass/access request or a valid badge
DECLARE @hasAccessAndPassRequestsForUser bit
SELECT @hasAccessAndPassRequestsForUser = count(*) from AccessRequest ar
	INNER JOIN AccessRequestType rt on ar.RequestTypeID = rt.RequestTypeID
	INNER JOIN AccessRequestStatus rs on ar.RequestStatusID = rs.RequestStatusID
	INNER JOIN AccessRequestPerson arp on ar.RequestPersonID = arp.RequestPersonID
WHERE rt.Name in ('New Pass','Access')
	and rs.Name in ('Approved', 'Actioned')
	and arp.dbPeopleID = @dbPeopleID

IF(@hasBadges = 1 or @hasAccessAndPassRequestsForUser = 1)
BEGIN
	--Create revoke request if person has a valid badge or actioned requests
	EXEC dbo.HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleID, @Valid = @Valid, @CreatedByID = @CreatedByID
	
	--Insert in to CATDeleteOffboard table to off-board user after 7 days from now
	INSERT INTO CATDeleteOffboard (TransactionID, DBPeopleID, Processed, CreatedDate)
	VALUES (@TransactionId, @dbPeopleID, 0, getdate())
END

END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO

/****** Object:  StoredProcedure [dbo].[up_CreateAccessTrigger_DEL]    Script Date: 11/13/2015 12:51:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 10/10/2015
-- Description: Process create access trigger's 'DEL' action. Set HRPerson and mp_user as reoked,
--              cancel any pending pass requests and access requests and offboard if active user
--============================================================

CREATE procedure [dbo].[up_CreateAccessTrigger_DEL]
@HRFeedMessageID int
as
begin

--Get DBPeopleID from trigger
DECLARE @dbPeopleID int 
DECLARE @transactionID int
SELECT @dbPeopleID = cast(HRID as int), @transactionID = TransactionId from HRFeedMessage where HRFeedMessageId = @HRFeedMessageID

--Do nothing if HRPerson record hasn't been created
IF(Not Exists(select * from HRPerson where dbPeopleID = @dbPeopleID))
	return

DECLARE @valid bit = 1
DECLARE @CreatedByID int = null

--Update HRPerson as revoked
update HRperson set Revoked=1 where dbPeopleID = @dbPeopleID
update mp_user set Revoked=1 where dbPeopleID = @dbPeopleID

--Check whether the person has active badges
DECLARE @hasBadges bit = 0
SELECT @hasBadges = count(PersonBadgeID) 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid and Active = 1

--delete the person's pending requests if any
EXEC dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, 0

--Create revoke requests if person has approved or actioned pass/access request or a valid badge
DECLARE @hasAccessAndPassRequestsForUser bit
SELECT @hasAccessAndPassRequestsForUser = count(*) from AccessRequest ar
	INNER JOIN AccessRequestType rt on ar.RequestTypeID = rt.RequestTypeID
	INNER JOIN AccessRequestStatus rs on ar.RequestStatusID = rs.RequestStatusID
	INNER JOIN AccessRequestPerson arp on ar.RequestPersonID = arp.RequestPersonID
WHERE rt.Name in ('New Pass','Access')
	and rs.Name in ('Approved', 'Actioned')
	and arp.dbPeopleID = @dbPeopleID

IF(@hasBadges = 1 or @hasAccessAndPassRequestsForUser = 1)
BEGIN
	--Create revoke request if person has a valid badge or actioned requests
	EXEC dbo.HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleID, @Valid = @Valid, @CreatedByID = @CreatedByID

	--Send emails to pass officers
	EXEC dbo.QueueEmailsForUser @dbPeopleId = @dbPeopleId, @EmailTypeID = 37

	--Insert in to CATDeleteOffboard table to off-board user after 7 days from now
	INSERT INTO CATDeleteOffboard (TransactionID, DBPeopleID, Processed, CreatedDate)
	VALUES (@TransactionId, @dbPeopleID, 0, getdate())
END

END


GO


