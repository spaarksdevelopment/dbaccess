IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportRollbackFile]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportRollbackFile]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportRollbackFile]    Script Date: 1/13/2016 1:26:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 23/12/2015
-- Description: Rollback imported data through bulk data import
--===============================================================

CREATE PROCEDURE [dbo].[XLImportRollbackFile]
@FileID int,
@ErrorMessage nvarchar(500) = '' OUTPUT

AS
BEGIN

	--Check whether access areas has been already used
	IF(EXISTS(SELECT * FROM AccessRequest
					WHERE AccessAreaID in (SELECT RelatedID FROM XLImportHistory WHERE FileID = @FileID AND RelatedTable = 'AccessArea')))
	BEGIN
		SET @ErrorMessage = 'Unable to rollback file. Access Requests have been created for the Access Areas imported from this file'
	END

	--If there are validation errors do not proceed.
	IF(LEN(@ErrorMessage) > 0)
		RETURN

	DECLARE @RelatedTable nvarchar(200), @RelatedID int, @sql nvarchar(500), @KeyCol nvarchar(100)


	BEGIN TRANSACTION

	BEGIN TRY

		DECLARE  cur CURSOR FOR
		SELECT h.RelatedTable, h.RelatedID, Col.COLUMN_NAME as KeyColoumn 
		FROM XLImportHistory h
			LEFT OUTER JOIN XLImportEntity  e on e.DestinationTable = h.RelatedTable
			LEFT OUTER JOIN (SELECT 'LocationFloor' as DestinationTable, 4 as SortOrder) f on h.RelatedTable  = f.DestinationTable
			INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tab on tab.TABLE_NAME = IsNUll(e.DestinationTable,  f.DestinationTable) and tab.CONSTRAINT_TYPE ='PRIMARY KEY'
			INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col on tab.CONSTRAINT_NAME = col.CONSTRAINT_NAME
		WHERE h.FileID = @FileID
		ORDER BY Isnull(e.SortOrder, f.SortOrder) desc, h.RelatedID desc

		OPEN cur

		FETCH NEXT FROM cur INTO @RelatedTable, @RelatedID, @KeyCol

		WHILE(@@FETCH_STATUS = 0)
		BEGIN

			SET @sql = 'delete ' + @RelatedTable + ' WHERE ' + @KeyCol + ' = ' + cast(@RelatedID as Varchar(100))

			EXEC sp_executesql @sql

			FETCH NEXT FROM cur INTO @RelatedTable, @RelatedID, @KeyCol

		END

		CLOSE cur
		DEALLOCATE cur

		--Delete Categories Added manually as it doesn't capture from above query
		DELETE Category WHERE id IN (SELECT RelatedID FROM XLImportHistory WHERE FileID=@FileID AND RelatedTable='Category')

		--Delete history records
		DELETE XLImportHistory WHERE FileID = @FileID

		--Delete file record
		DELETE XLImportFile WHERE ID = @FileID

		SET @ErrorMessage = NULL

		PRINT 'File rollback sucessfully'

		COMMIT TRANSACTION

	END TRY

	BEGIN CATCH	

		IF(@@TRANCOUNT > 0)
			ROLLBACK TRANSACTION

		SET @ErrorMessage = 'This file cannot rollback. Data uploaded using this file are in use.'

		PRINT 'Error Rollbacking file ' + Error_Message() 

		CLOSE cur
		DEALLOCATE cur

	END CATCH

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportRollbackFile]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportRollbackFile]
END
GO