IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_UpdateTaskStatus_TaskType3]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType3]
END
GO



/****** Object:  StoredProcedure [dbo].[up_UpdateTaskStatus_TaskType3]    Script Date: 5/29/2015 12:43:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--========================================================
-- History: 19/05/2015 (Wijitha) - Modifed the logic of retriving the pass office based on the builing pass office for the task related to Access Requests.	
--			28/05/2015 (Wijitha) - Ignored creating tasks for disabled pass officers when approving access requests.
--========================================================
CREATE PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType3] 
    @TaskId INT, 
    @TaskStatusId INT, 
    @CompletedById INT, 
    @Comment VarChar(500), 
    @Result int Output 
AS 
    SET NoCount On; 
    SET @Result = 0; 

    DECLARE @dtNow DateTime2 = GetDate(); 
    DECLARE @doToday Date = @dtNow; 
    DECLARE @iReferences INT = 0; 
    DECLARE @NumberOfApprovals INT = 0; 
    DECLARE @TaskTypeId INT; 
    DECLARE @DivisionId INT;
    DECLARE @BusinessAreaId INT; 
    DECLARE @RequestId INT; 
    DECLARE @AccessAreaId INT; 
    DECLARE @iPassOfficeId INT; 
    DECLARE @ickCompletedByID int; 
        

    --code added by BH & MS 07/05/2014 to prevent tasktype 3 producing >1 tasktype 7 (ie: duplicate tasks) 
    IF EXISTS (SELECT TaskID from Tasks        WHERE RequestID = @RequestId AND TaskTypeID = 7) 
	   RETURN 

    SELECT 
          @TaskTypeId = TaskTypeId, 
          @DivisionId = DivisionId, 
          @BusinessAreaId = BusinessAreaId, 
          @RequestId = RequestId, 
          @AccessAreaId = AccessAreaId, 
          @iPassOfficeId = PassOfficeId, 
          @ickCompletedByID = CompletedByID 
    FROM 
          Tasks 
    WHERE 
          TaskId = @TaskId; 
        
    --Check if the Task has CompletedByID NULL 
    --IF it is null Go Ahead and process it if not then by pass all the processing 
    IF(@ickCompletedByID is NULL) 
    BEGIN 
	   -- update the task user entry for this approver 
	   UPDATE TasksUsers 
	   SET 
	   IsCurrent = 0, 
	   TaskStatusId = @TaskStatusId, 
	   CompletedDate = @dtNow 
				    WHERE 
	   TaskId = @TaskId 
	   And 
	   dbPeopleID = @CompletedById; 

	   -- IF the approver has approved (AA/DA/PO) check if fully approved 
	   -- then update the request and create tasks for pass office --control system actions table 

	   DECLARE @bFinished_AA BIT = 0; 

	   IF (@TaskStatusId = 3) 
	   BEGIN 
		  -- check if required number of approvals have been met 
		  SET @NumberOfApprovals = 
		  ( 
				SELECT Top 1 
				    NumberOfApprovals 
				FROM 
				    CorporateDivisionAccessArea 
				WHERE 
				    DivisionID = @DivisionId 
				    And 
				    AccessAreaID = @AccessAreaID 
		  ); 
		  SET @NumberOfApprovals = IsNull (@NumberOfApprovals, 1); 
	   
		  -- count the number of task entries already approved 
		  IF (@NumberOfApprovals = 1        -- this person is an approver (saves a query ON the DB) 
				Or (SELECT Count(TasksUsersId) 
					FROM TasksUsers 
					WHERE TaskId = @TaskId And TaskStatusID = @TaskStatusId) >= @NumberOfApprovals) 
		  BEGIN 
			 -- required number of Access Approvals has been met 
			UPDATE AccessRequest 
            SET AccessApproved = 1, 
                RequestStatusID = 3,          -- approved 
                AccessApprovedDate = @dtNow, 
                AccessApprovedByID = @CompletedById 
            WHERE RequestID = @RequestId; 

			SET @bFinished_AA = 1; 

			--  pick up the comments for the AA 
			DECLARE @sTaskCommentAA NVarChar(500); 
			SET @sTaskCommentAA = (SELECT IsNull (TaskComment, N'') As TaskComment FROM Tasks WHERE TaskId = @TaskId); 

			 ----------------------------------------------- 
			 -- Now create the Pass Office approval Task 
			 ----------------------------------------------- 
			 -- for any access areas which are not associated with a control system, create tasks for manual updating 

			INSERT INTO 
            Tasks 
            ( 
                TaskTypeId, 
                TaskStatusId, 
                RequestId, 
                DivisionId, 
                BusinessAreaId, 
                AccessAreaId, 
                [Description], 
                CreatedByID, 
                CreatedDate 
            ) 
            SELECT 
                7, 
                1, 
                RequestID, 
                DivisionID, 
                BusinessAreaID, 
                vw_AccessRequest.AccessAreaID 
                ,'Access Approver Commented:' + @sTaskCommentAA + '   ' + 
                'Add Access for ' + Forename + ' ' + Surname + ' (' + FulldbPeopleID + ') to access ' + 
								    Isnull([BuildingName],'') + '/' + ISNull([FloorName],'') + '/' + 
								    IsNull([AccessAreaName],'') + ' (' + vw_AccessArea.AccessAreaTypeName + ')' + (CASE WHEN EndDate Is 
								    Not Null THEN ' until ' + Convert(VarChar(11), EndDate, 106) ELSE '' END), 
                @CompletedById, 
                @dtNow 
            FROM vw_AccessRequest 
				INNER JOIN vw_AccessArea On vw_AccessArea.AccessAreaID = vw_AccessRequest.AccessAreaID 
            WHERE 
                RequestID = @RequestId 
                                                                        
			 -- Assign the Task to the pass office 
			 
			INSERT INTO 
            TasksUsers 
            (   
				TaskId,                                     
                TaskStatusId, 
                IsCurrent, 
                [Order], 
                CreatedDateTime, 
                mp_SecurityGroupID, 
                dbPeopleID 
            ) 
            SELECT 
                Tasks.TaskId,                                     
                1,-- Pending 
                1, 
                1, 
                @dtNow, 
                6,-- Pass Office 
                hrperson.dbPeopleID         
                                        
            FROM TasksUsers 
				Right Outer JOIN Tasks On Tasks.TaskId = TasksUsers.TaskId 
				INNER JOIN AccessRequest on AccessRequest.RequestID = Tasks.RequestId
				INNER JOIN LocationPassOffice On LocationPassOffice.PassOfficeID = AccessRequest.PassOfficeID 
				INNER JOIN LocationPassOfficeUser On LocationPassOfficeUser.PassOfficeID = LocationPassOffice.PassOfficeID 
				INNER JOIN hrPerson On hrPerson.dbPeopleID = LocationPassOfficeUser.dbPeopleID 
            WHERE 
                Tasks.TaskTypeId = 7 
                And TasksUsers.TasksUsersId Is Null
				AND LocationPassOfficeUser.IsEnabled = 1
		  END 
	   END 
	   ELSE 
	   BEGIN 
		  -- Request Access was rejected 
		  UPDATE AccessRequest 
		  SET 
				AccessApproved = 0, 
				RequestStatusID = 10, 
				AccessApprovedDate = @dtNow, 
				AccessApprovedById = @CompletedById 
		  WHERE 
				RequestID = @RequestId; 

		  SET @bFinished_AA = 1; 
	   END 

	   IF (1 = @bFinished_AA) 
	   BEGIN 
		  -- AA Task is finished 
		  UPDATE 
				Tasks 
		  SET 
				TaskStatusId = @TaskStatusId, 
				CompletedByID = @CompletedById, 
				CompletedDate = @dtNow, 
				TaskComment = @Comment 
		  WHERE 
				TaskId = @TaskId; 

		  -- clear out any entries that we no longer need 
		  UPDATE 
				TasksUsers 
		  SET 
				IsCurrent = 0 
		  WHERE 
				TaskId = @TaskId; 

		  DELETE FROM 
				TasksUsers 
		  WHERE 
				TaskId = @TaskId 
				And AssignedDate Is Null 
				And CompletedDate Is Null; 
	   END 
              
       exec up_UpdateTaskStatus_Common 
       @RequestId, 
       @TaskId, 
       @TaskTypeId, 
       @TaskStatusId, 
       @CompletedById, 
       @Comment 
                        
		SET @Result = 1; 
	END 
    ELSE 
    BEGIN 
		SET @Result = 0; 
    END 

GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_UpdateTaskStatus_TaskType3]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType3]
END
GO

/****** Object:  StoredProcedure [dbo].[up_UpdateTaskStatus_TaskType3]    Script Date: 5/19/2015 3:34:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType3] 
    @TaskId INT, 
    @TaskStatusId INT, 
    @CompletedById INT, 
    @Comment VarChar(500), 
    @Result int Output 
AS 
    SET NoCount On; 
    SET @Result = 0; 

    DECLARE @dtNow DateTime2 = GetDate(); 
    DECLARE @doToday Date = @dtNow; 
    DECLARE @iReferences INT = 0; 
    DECLARE @NumberOfApprovals INT = 0; 
    DECLARE @TaskTypeId INT; 
    DECLARE @DivisionId INT;
    DECLARE @BusinessAreaId INT; 
    DECLARE @RequestId INT; 
    DECLARE @AccessAreaId INT; 
    DECLARE @iPassOfficeId INT; 
    DECLARE @ickCompletedByID int; 
        

    --code added by BH & MS 07/05/2014 to prevent tasktype 3 producing >1 tasktype 7 (ie: duplicate tasks) 
    IF EXISTS (SELECT TaskID from Tasks        WHERE RequestID = @RequestId AND TaskTypeID = 7) 
	   RETURN 

    SELECT 
          @TaskTypeId = TaskTypeId, 
          @DivisionId = DivisionId, 
          @BusinessAreaId = BusinessAreaId, 
          @RequestId = RequestId, 
          @AccessAreaId = AccessAreaId, 
          @iPassOfficeId = PassOfficeId, 
          @ickCompletedByID = CompletedByID 
    FROM 
          Tasks 
    WHERE 
          TaskId = @TaskId; 
        
    --Check if the Task has CompletedByID NULL 
    --IF it is null Go Ahead and process it if not then by pass all the processing 
    IF(@ickCompletedByID is NULL) 
    BEGIN 
	   -- update the task user entry for this approver 
	   UPDATE TasksUsers 
	   SET 
	   IsCurrent = 0, 
	   TaskStatusId = @TaskStatusId, 
	   CompletedDate = @dtNow 
				    WHERE 
	   TaskId = @TaskId 
	   And 
	   dbPeopleID = @CompletedById; 

	   -- IF the approver has approved (AA/DA/PO) check if fully approved 
	   -- then update the request and create tasks for pass office --control system actions table 

	   DECLARE @bFinished_AA BIT = 0; 

	   IF (@TaskStatusId = 3) 
	   BEGIN 
		  -- check if required number of approvals have been met 
		  SET @NumberOfApprovals = 
		  ( 
				SELECT Top 1 
				    NumberOfApprovals 
				FROM 
				    CorporateDivisionAccessArea 
				WHERE 
				    DivisionID = @DivisionId 
				    And 
				    AccessAreaID = @AccessAreaID 
		  ); 
		  SET @NumberOfApprovals = IsNull (@NumberOfApprovals, 1); 
	   
		  -- count the number of task entries already approved 
		  IF 
		  ( 
			 @NumberOfApprovals = 1        -- this person is an approver (saves a query ON the DB) 
			 Or 
			 ( 
				SELECT 
					   Count(TasksUsersId) 
				FROM 
					   TasksUsers 
				WHERE 
					   TaskId = @TaskId 
					   And 
					   TaskStatusID = @TaskStatusId 
			 ) >= @NumberOfApprovals 
		  ) 
		  BEGIN 
			 -- required number of Access Approvals has been met 
			 	 	 	 	 	 	 	 	 UPDATE 
                AccessRequest 
            SET 
                AccessApproved = 1, 
                RequestStatusID = 3,          -- approved 
                AccessApprovedDate = @dtNow, 
                AccessApprovedByID = @CompletedById 
            WHERE 
                RequestID = @RequestId; 

			 SET @bFinished_AA = 1; 

			 --  pick up the comments for the AA 
			 DECLARE @sTaskCommentAA NVarChar(500); 
			 SET @sTaskCommentAA = (SELECT IsNull (TaskComment, N'') As TaskComment FROM Tasks WHERE TaskId = @TaskId); 

			 ----------------------------------------------- 
			 -- Now create the Pass Office approval Task 
			 ----------------------------------------------- 
			 -- for any access areas which are not associated with a control system, create tasks for manual updating 

			INSERT INTO 
            Tasks 
            ( 
                TaskTypeId, 
                TaskStatusId, 
                RequestId, 
                DivisionId, 
                BusinessAreaId, 
                AccessAreaId, 
                [Description], 
                CreatedByID, 
                CreatedDate 
            ) 
            SELECT 
                7, 
                1, 
                RequestID, 
                DivisionID, 
                BusinessAreaID, 
                vw_AccessRequest.AccessAreaID 
                ,'Access Approver Commented:' + @sTaskCommentAA + '   ' + 
                'Add Access for ' + Forename + ' ' + Surname + ' (' + FulldbPeopleID + ') to access ' + 
								    Isnull([BuildingName],'') + '/' + ISNull([FloorName],'') + '/' + 
								    IsNull([AccessAreaName],'') + ' (' + vw_AccessArea.AccessAreaTypeName + ')' + (CASE WHEN EndDate Is 
								    Not Null THEN ' until ' + Convert(VarChar(11), EndDate, 106) ELSE '' END), 
                @CompletedById, 
                @dtNow 
            FROM 
                vw_AccessRequest 
            INNER JOIN 
                vw_AccessArea 
                On 
                vw_AccessArea.AccessAreaID = vw_AccessRequest.AccessAreaID 
            WHERE 
                RequestID = @RequestId 
                                                                        
			 -- Assign the Task to the pass office 

			INSERT INTO 
            TasksUsers 
            ( 
                TaskId, 
                                    
                TaskStatusId, 
                IsCurrent, 
                [Order], 
                CreatedDateTime, 
                mp_SecurityGroupID, 
                                                    dbPeopleID 
            ) 
            SELECT 
                Tasks.TaskId, 
                                    
                1,                -- Pending 
                1, 
                1, 
                @dtNow, 
                6    ,             -- Pass Office 
                                                    hrperson.dbPeopleID         

                                        
            FROM 
                TasksUsers 
            Right Outer JOIN 
                Tasks 
                On 
                Tasks.TaskId = TasksUsers.TaskId 
            INNER JOIN 
                vw_AccessArea 
                On 
                vw_AccessArea.AccessAreaID = Tasks.AccessAreaId 
            INNER JOIN 
                LocationCountry 
                On 
                LocationCountry.CountryID = vw_AccessArea.CountryID 
            INNER JOIN 
                LocationPassOffice 
                On 
                LocationPassOffice.PassOfficeID = LocationCountry.PassOfficeID 
            INNER JOIN 
                LocationPassOfficeUser 
                On 
                LocationPassOfficeUser.PassOfficeID = LocationPassOffice.PassOfficeID 
            INNER JOIN 
                hrPerson 
                On 
                hrPerson.dbPeopleID = LocationPassOfficeUser.dbPeopleID 
            WHERE 
                Tasks.TaskTypeId = 7 
                And 
                TasksUsers.TasksUsersId Is Null; 
		  END 
	   END 
	   ELSE 
	   BEGIN 
		  -- Request Access was rejected 
		  UPDATE AccessRequest 
		  SET 
				AccessApproved = 0, 
				RequestStatusID = 10, 
				AccessApprovedDate = @dtNow, 
				AccessApprovedById = @CompletedById 
		  WHERE 
				RequestID = @RequestId; 

		  SET @bFinished_AA = 1; 
	   END 

	   IF (1 = @bFinished_AA) 
	   BEGIN 

		  -- AA Task is finished 

		  UPDATE 
				Tasks 
		  SET 
				TaskStatusId = @TaskStatusId, 
				CompletedByID = @CompletedById, 
				CompletedDate = @dtNow, 
				TaskComment = @Comment 
		  WHERE 
				TaskId = @TaskId; 

		  -- clear out any entries that we no longer need 

		  UPDATE 
				TasksUsers 
		  SET 
				IsCurrent = 0 
		  WHERE 
				TaskId = @TaskId; 

		  DELETE FROM 
				TasksUsers 
		  WHERE 
				TaskId = @TaskId 
				And 
				AssignedDate Is Null 
				And 
				CompletedDate Is Null; 
	   END 
              
        exec up_UpdateTaskStatus_Common 
        @RequestId, 
        @TaskId, 
        @TaskTypeId, 
        @TaskStatusId, 
        @CompletedById, 
        @Comment 
                        
        SET @Result = 1; 
    END 
    ELSE 
    BEGIN 
            SET @Result = 0; 
    END 

GO