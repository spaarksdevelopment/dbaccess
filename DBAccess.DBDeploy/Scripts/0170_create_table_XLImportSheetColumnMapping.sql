IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportSheetColumnMapping')
BEGIN

CREATE TABLE [dbo].[XLImportSheetColumnMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[SheetColumn] [varchar](200) NOT NULL,
	[StagingTableColumn] [varchar](200) NOT NULL,
 CONSTRAINT [PK_XLImportSheetStagingColumnMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [dbo].[XLImportSheetColumnMapping]  WITH CHECK ADD  CONSTRAINT [FK_XLImportSheetColumnMapping_XLImportEntity] FOREIGN KEY([EntityID])
REFERENCES [dbo].[XLImportEntity] ([ID])

ALTER TABLE [dbo].[XLImportSheetColumnMapping] CHECK CONSTRAINT [FK_XLImportSheetColumnMapping_XLImportEntity]

END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportSheetColumnMapping')
BEGIN
	
	DROP TABLE [dbo].[XLImportSheetColumnMapping]
	
END

GO