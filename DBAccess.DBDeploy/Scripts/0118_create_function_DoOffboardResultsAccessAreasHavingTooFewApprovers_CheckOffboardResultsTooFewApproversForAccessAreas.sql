IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers]
(
	@dbPeopleID int
)
RETURNS bit
AS
BEGIN

	DECLARE @NumberOfAccessAreas int

	;WITH ct AS (
	SELECT aa.CorporateDivisionAccessAreaID, MAX(aa.NumberOfApprovals) AS RequiredApprovers, COUNT(allApprovers.dbPeopleID)  AS CurrentApprovers-- *, allApprovers.dbPeopleID
	FROM CorporateDivisionAccessArea aa
	INNER JOIN CorporateDivisionAccessAreaApprover approver ON aa.CorporateDivisionAccessAreaID = approver.CorporateDivisionAccessAreaID
	LEFT OUTER JOIN CorporateDivisionAccessAreaApprover allApprovers ON allApprovers.CorporateDivisionAccessAreaID=aa.CorporateDivisionAccessAreaID 
	WHERE approver.dbPeopleID = @dbPeopleID and allApprovers.Enabled = 1
	GROUP BY aa.CorporateDivisionAccessAreaID
	)

	SELECT @NumberOfAccessAreas = Count(*) FROM ct
	WHERE CurrentApprovers  <= RequiredApprovers

	IF @NumberOfAccessAreas > 0 
		RETURN 1

	RETURN 0
END


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers]
END
GO

