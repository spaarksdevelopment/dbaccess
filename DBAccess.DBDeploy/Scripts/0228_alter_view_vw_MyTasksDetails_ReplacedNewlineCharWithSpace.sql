IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasksDetails]') )
	DROP VIEW [dbo].[vw_MyTasksDetails]
GO

CREATE VIEW [dbo].[vw_MyTasksDetails]
AS
SELECT DISTINCT 
                         vmt.TaskId, vmt.TaskTypeId, vmt.Type, vmt.TaskStatusId, vmt.Status, REPLACE(REPLACE(vmt.Description, CHAR(13) + CHAR(10), ' '), CHAR(10), ' ') AS Description, 
                         CASE WHEN ar.RequestTypeID = 15 THEN 'Off Boarding ' + Replace(Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', 
                         vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')), '{2}', IsNull(vmt.[CountryName], '')), '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], 
                         '')), '{4}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND 
                         vmt.[BuildingName] IS NOT NULL THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)), '{8}', 
                         IsNull(vmt.[ApplicantCountryName], '')) + (CASE WHEN EXISTS
                             (SELECT        *
                               FROM            OffBoardResultTooFewApprovers
                               WHERE        RequestMasterID = arp.RequestMasterID) THEN '. The person being offboarded is an approver for some access areas (See Details). Please add additional approvers to these areas.' ELSE '' END) 
                         ELSE Replace(Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')), '{2}', IsNull(vmt.[CountryName], '')), 
                         '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], '')), '{4}', CASE IsNull(vmt.[BadgeType], '') 
                         WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND vmt.[BuildingName] IS NOT NULL 
                         THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)), '{8}', 
                         IsNull(vmt.[ApplicantCountryName], '')) END AS Descript, vmt.CreatedByID, vmt.CreatedDate, vmt.CompletedByID, vmt.CompletedDate, vmt.RequestId, vmt.AccessAreaId, vmt.dbPeopleId, vmt.IsCurrent, 
                         vmt.[Order], vmt.AccessAreaTypeID, vmt.AccessAreaTypeName, vmt.AccessAreaName, vmt.FloorID, vmt.FloorName, vmt.BuildingID, vmt.BuildingCode, vmt.BuildingName, vmt.CityID, vmt.CityCode, 
                         vmt.CityName, vmt.CountryID, vmt.CountryCode, vmt.CountryName, vmt.RegionCode, vmt.RegionName, vmt.DivisionId, vmt.DivisionName, vmt.DivisionUBR, vmt.BusinessAreaName, vmt.BusinessAreaUBR, 
                         vmt.BusinessAreaId, vmt.TaskComment, vmt.BadgeType, vmt.Classification, vmt.RequestComment, vmt.RequestBy, vmt.RequestEndDate, vmt.RequestMasterID, vmt.RequestStartDate, vmt.Applicant, 
                         vmt.Pending, COALESCE (bjr.Name, ajr.Name, '') AS Justification
FROM            dbo.vw_MyTasks AS vmt INNER JOIN
                         dbo.TaskTypeLookup AS tsl ON tsl.TaskTypeId = vmt.TaskTypeId INNER JOIN
                         dbo.AccessRequest AS ar ON ar.RequestID = vmt.RequestId INNER JOIN
                         dbo.AccessRequestPerson AS arp ON arp.RequestPersonID = ar.RequestPersonID LEFT OUTER JOIN
                         dbo.BadgeJustificationReason AS bjr ON ar.BadgeJustificationReasonID = bjr.ID LEFT OUTER JOIN
                         dbo.AccessRequestJustificationReason AS ajr ON ar.AccessRequestJustificationReasonID = ajr.ID

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasksDetails]') )
	DROP VIEW [dbo].[vw_MyTasksDetails]
GO

CREATE VIEW [dbo].[vw_MyTasksDetails]
AS
SELECT DISTINCT 
                         vmt.TaskId, vmt.TaskTypeId, vmt.Type, vmt.TaskStatusId, vmt.Status, REPLACE(ISNULL(vmt.Description, ''), CHAR(13) + CHAR(10), ' ') AS Description, 
                         CASE WHEN ar.RequestTypeID = 15 THEN 'Off Boarding ' + Replace(Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', 
                         vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')), '{2}', IsNull(vmt.[CountryName], '')), '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], 
                         '')), '{4}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND 
                         vmt.[BuildingName] IS NOT NULL THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)), '{8}', 
                         IsNull(vmt.[ApplicantCountryName], '')) + (CASE WHEN EXISTS
                             (SELECT        *
                               FROM            OffBoardResultTooFewApprovers
                               WHERE        RequestMasterID = arp.RequestMasterID) THEN '. The person being offboarded is an approver for some access areas (See Details). Please add additional approvers to these areas.' ELSE '' END) 
                         ELSE Replace(Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')), '{2}', IsNull(vmt.[CountryName], '')), 
                         '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], '')), '{4}', CASE IsNull(vmt.[BadgeType], '') 
                         WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND vmt.[BuildingName] IS NOT NULL 
                         THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)), '{8}', 
                         IsNull(vmt.[ApplicantCountryName], '')) END AS Descript, vmt.CreatedByID, vmt.CreatedDate, vmt.CompletedByID, vmt.CompletedDate, vmt.RequestId, vmt.AccessAreaId, vmt.dbPeopleId, vmt.IsCurrent, 
                         vmt.[Order], vmt.AccessAreaTypeID, vmt.AccessAreaTypeName, vmt.AccessAreaName, vmt.FloorID, vmt.FloorName, vmt.BuildingID, vmt.BuildingCode, vmt.BuildingName, vmt.CityID, vmt.CityCode, 
                         vmt.CityName, vmt.CountryID, vmt.CountryCode, vmt.CountryName, vmt.RegionCode, vmt.RegionName, vmt.DivisionId, vmt.DivisionName, vmt.DivisionUBR, vmt.BusinessAreaName, vmt.BusinessAreaUBR, 
                         vmt.BusinessAreaId, vmt.TaskComment, vmt.BadgeType, vmt.Classification, vmt.RequestComment, vmt.RequestBy, vmt.RequestEndDate, vmt.RequestMasterID, vmt.RequestStartDate, vmt.Applicant, 
                         vmt.Pending, COALESCE (bjr.Name, ajr.Name, '') AS Justification
FROM            dbo.vw_MyTasks AS vmt INNER JOIN
                         dbo.TaskTypeLookup AS tsl ON tsl.TaskTypeId = vmt.TaskTypeId INNER JOIN
                         dbo.AccessRequest AS ar ON ar.RequestID = vmt.RequestId INNER JOIN
                         dbo.AccessRequestPerson AS arp ON arp.RequestPersonID = ar.RequestPersonID LEFT OUTER JOIN
                         dbo.BadgeJustificationReason AS bjr ON ar.BadgeJustificationReasonID = bjr.ID LEFT OUTER JOIN
                         dbo.AccessRequestJustificationReason AS ajr ON ar.AccessRequestJustificationReasonID = ajr.ID

GO
