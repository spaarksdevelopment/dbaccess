IF EXISTS (
    SELECT * FROM dbo.sysobjects 
    WHERE NAME = 'trg_UpdateOutOfDateMessagesForEmpId' 
    AND OBJECTPROPERTY(id, 'IsTrigger') = 1
) 

DROP TRIGGER trg_UpdateOutOfDateMessagesForEmpId

GO

--//@UNDO

IF EXISTS (
    SELECT * FROM dbo.sysobjects 
    WHERE NAME = 'trg_UpdateOutOfDateMessagesForEmpId' 
    AND OBJECTPROPERTY(id, 'IsTrigger') = 1
) 

/****** Object:  Trigger [dbo].[trg_UpdateOutOfDateMessagesForEmpId]    Script Date: 28/10/2015 14:05:20 ******/

SET ANSI_NULLS ON

GO

 

SET QUOTED_IDENTIFIER ON

GO

 

 

-- =============================================

-- Author:           <Author,,Name>

-- Create date: <Create Date,,>

-- Description:      <Description,,>

-- =============================================

CREATE TRIGGER [dbo].[trg_UpdateOutOfDateMessagesForEmpId]

   ON  [dbo].[HRFeedMessage]

   AFTER INSERT

AS

BEGIN

 

       DECLARE @HRID nvarchar(11)

       DECLARE @TransactionID int

       DECLARE @TriggerType nvarchar(4)

 

       SET NOCOUNT ON;

 

 

    -- Insert statements for trigger here

       SELECT @TriggerType = TriggerType, @HRID = HRID, @TransactionID = TransactionId FROM inserted

      

       UPDATE HRFeedMessage

       SET

              Processed = 1,

              RevokeProcessed = 1,

              ReactivateProcessed = 1 

       WHERE

              HRID = @HRID AND

              Processed = 0 AND

              TransactionId <> @TransactionID AND

              TriggerType = @TriggerType

 

 

END

 

 

GO