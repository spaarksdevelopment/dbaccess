IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagePassOfficeUser')
BEGIN

CREATE TABLE [dbo].[XLImportStagePassOfficeUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PassOffice] [nvarchar](500) NULL,
	[DBPeopleID] [nvarchar](500) NULL,
	[CreatedDate] [nvarchar](500) NULL,
	[DateAccepted] [nvarchar](500) NULL,
	[DateLastCertified] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStagePassOfficeUser1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[XLImportStagePassOfficeUser]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStagePassOfficeUser_XLImportFile1] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStagePassOfficeUser] CHECK CONSTRAINT [FK_XLImportStagePassOfficeUser_XLImportFile1]


END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagePassOfficeUser')
BEGIN
	
	DROP TABLE [dbo].[XLImportStagePassOfficeUser]
	
END

GO