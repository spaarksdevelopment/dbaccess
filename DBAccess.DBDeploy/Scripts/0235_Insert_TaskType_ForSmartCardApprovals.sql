
IF NOT EXISTS (SELECT * FROM TaskTypeLookup WHERE TaskTypeId=18)
BEGIN
	INSERT INTO TaskTypeLookup (TaskTypeId, Type, Description)
	VALUES (18, 'ApproveSmartCard', 'Smart Card Approval Stage 1: Do you approve granting a Smart Card for {1}?')
END

GO

--//@UNDO

DELETE TaskTypeLookup 
WHERE TaskTypeId=18

GO


