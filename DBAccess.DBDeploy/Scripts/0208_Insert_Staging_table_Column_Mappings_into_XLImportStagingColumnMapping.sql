TRUNCATE TABLE XLImportStagingColumnMapping

SET IDENTITY_INSERT [dbo].[XLImportStagingColumnMapping] ON 
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (1, 1, N'Name', N'Name')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (2, 4, N'Name', N'Name')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (3, 4, N'StreetAddress', N'StreetAddress')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (4, 2, N'Name', N'Name')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (5, 2, N'Email', N'Email')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (6, 2, N'Telephone', N'Telephone')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (7, 2, N'Enabled', N'Enabled')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (8, 2, N'EmailEnabled', N'EmailEnabled')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (9, 5, N'Name', N'Name')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (10, 5, N'UBR', N'UBR')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (11, 5, N'RecertPeriod', N'RecertPeriod')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (12, 6, N'DBPeopleID', N'dbPeopleID')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (13, 6, N'DateAccepted', N'DateAccepted')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (14, 6, N'DateLastCertified', N'DateLastCertified')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (15, 14, N'DBPeopleID', N'dbPeopleID')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (16, 14, N'CreatedDate', N'CreatedDateTime')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (17, 14, N'DateAccepted', N'DateAccepted')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (18, 14, N'DateLastCertified', N'DateLastCertified')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (19, 8, N'Name', N'Name')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (20, 8, N'RecertPeriod', N'RecertPeriod')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (21, 9, N'NoOfApprovals', N'NumberOfApprovals')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (22, 9, N'Frequency', N'Frequency')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (23, 9, N'LastCertifiedDate', N'LastCertifiedDate')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (24, 9, N'NextCertifiedDate', N'DateNextCertification')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (26, 10, N'DBPeopleID', N'dbPeopleID')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (27, 10, N'ApproverSortOrder', N'SortOrder')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (28, 10, N'DateAccepted', N'DateAccepted')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (29, 2, N'Code', N'Code')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (30, 13, N'DBPeopleID', N'dbPeopleID')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (31, 13, N'DateAccepted', N'DateAccepted')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (32, 5, N'IsSubdivision', N'IsSubdivision')
INSERT [dbo].[XLImportStagingColumnMapping] ([ID], [EntityID], [StagingTableColumn], [DestinationTableColumn]) VALUES (33, 13, N'DateNextCertification', N'DateNextCertification')
SET IDENTITY_INSERT [dbo].[XLImportStagingColumnMapping] OFF
GO

--//@UNDO
TRUNCATE TABLE XLImportStagingColumnMapping

GO

