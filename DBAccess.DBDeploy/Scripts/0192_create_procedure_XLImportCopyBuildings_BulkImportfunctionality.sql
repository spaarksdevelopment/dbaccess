IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyBuildings]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyBuildings]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 18/12/2015
-- Description: Copy buildings from staging table to the LocationBuilding table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyBuildings]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO LocationBuilding (CityID, Name, StreetAddress, Enabled, PassOfficeID)
	OUTPUT inserted.BuildingID into @tblIDs
	SELECT (SELECT CityID FROM LocationCity city 
					INNER JOIN LocationCountry c on c.CountryID = city.CountryID 
					where city.Name = sc.Name and c.Name = sc.Country),
		sb.Name,
		sb.StreetAddress,
		1,
		(SELECT PassOfficeID FROM LocationPassOffice WHERE Name = PassOffice)
	FROM XLImportStageBuilding sb
		INNER JOIN XLImportStageCity sc on sb.City = sc.Name		
	WHERE ISNULL(sb.ValidationFailed, 0) = 0 AND sb.FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'LocationBuilding', ID FROM @tblIDs

END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyBuildings]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyBuildings]
END
GO