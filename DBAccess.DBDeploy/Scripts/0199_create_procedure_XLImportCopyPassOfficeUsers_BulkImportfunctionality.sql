IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyPassOfficeUsers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyPassOfficeUsers]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Asanka
-- Created Date: 22/12/2015
-- Description: Copy pass office users from staging table to the LocationPassOfficeUser table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyPassOfficeUsers]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO LocationPassOfficeUser (PassOfficeID, CreatedDateTime, DateAccepted, DateLastCertified, IsEnabled, dbPeopleID, IsRecertified, mp_SecurityGroupID)
	OUTPUT inserted.PassOfficeUserID into @tblIDs
	SELECT (SELECT PassOfficeID FROM LocationPassOffice where Name = PassOffice),
		CAST(CreatedDate AS datetime),
		CAST(DateAccepted AS datetime),
		CAST(DateLastCertified AS datetime),
		1,
		DBPeopleID,
		1,
		(SELECT Id  FROM mp_SecurityGroup where RoleShortName = 'PO')
	FROM XLImportStagePassOfficeUser 
	WHERE ISNULL(ValidationFailed, 0) = 0 AND FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'LocationPassOfficeUser', ID FROM @tblIDs

END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyPassOfficeUsers]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyPassOfficeUsers]
END
GO