
IF NOT EXISTS (SELECT * FROM EmailTemplates WHERE Type = 41)
BEGIN
	INSERT INTO EmailTemplates (Type, Subject, Body)
	VALUES 
	(41, 'DBAccess - Smart Card Approval Awaiting','Smart Card Request.<br />RequestId : ([$$RequestId$$]) Submitted by : [$$NAME$$].<br />Applicant: [$$ApplicantName$$] from [$$CountryName$$] of [$$UbrCode$$], [$$UbrName$$]<br />')
END


IF NOT EXISTS (SELECT * FROM EmailTemplates WHERE Type = 42)
BEGIN
	INSERT INTO EmailTemplates (Type, Subject, Body)
	VALUES 
	(42, 'DBAccess - Smart Card Request Rejected', 'Dear [$$NAME$$],<br /><br />Smart Card request made by you is rejected.<br />Request ID: [$$RequestId$$]<br />Applicant: [$$ApplicantName$$] from [$$CountryName$$] of [$$UbrCode$$], [$$UbrName$$]<br />Comment: [$$Comment$$]
	<br /><br />Kind Regards<br />DBAccess System Administrator')
END

GO

--//@UNDO

DELETE EmailTemplates
WHERE Type in (41, 42)

GO