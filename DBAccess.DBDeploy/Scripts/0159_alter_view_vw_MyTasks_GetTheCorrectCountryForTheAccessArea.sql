IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasks]') )
	DROP VIEW [dbo].[vw_MyTasks]
GO

/****** Object:  View [dbo].[vw_MyTasks]    Script Date: 12/7/2015 10:08:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MyTasks]
AS
SELECT DISTINCT 
                         dbo.Tasks.TaskId, dbo.Tasks.TaskTypeId, dbo.TaskTypeLookup.Type, dbo.Tasks.TaskStatusId, dbo.TaskStatusLookup.Status, dbo.Tasks.Description, dbo.Tasks.CreatedByID, dbo.Tasks.CreatedDate, 
                         dbo.Tasks.CompletedByID, dbo.Tasks.CompletedDate, dbo.Tasks.RequestId, dbo.Tasks.AccessAreaId, dbo.TasksUsers.dbPeopleId, dbo.TasksUsers.IsCurrent, dbo.TasksUsers.[Order], 
                         dbo.vw_AccessArea.AccessAreaTypeID, dbo.vw_AccessArea.AccessAreaTypeName, dbo.vw_AccessArea.AccessAreaName, dbo.vw_AccessArea.FloorID, dbo.vw_AccessArea.FloorName, 
                         dbo.vw_AccessArea.BuildingID, dbo.vw_AccessArea.BuildingCode, dbo.vw_AccessArea.BuildingName, dbo.vw_AccessArea.CityID, dbo.vw_AccessArea.CityCode, dbo.vw_AccessArea.CityName, 
                         dbo.vw_AccessArea.CountryID, dbo.vw_AccessArea.CountryCode, (CASE WHEN (CorporateDivision.CountryID IS NOT NULL AND LocationCountry.CountryID IS NOT NULL AND dbo.Tasks.AccessAreaId IS NOT NULL
                          AND dbo.vw_AccessArea.AccessAreaID IS NOT NULL) THEN dbo.vw_AccessArea.CountryName WHEN (CorporateDivision.CountryID IS NOT NULL AND dbo.LocationCountry.CountryID IS NOT NULL) 
                         THEN dbo.LocationCountry.[Name] WHEN (dbo.Tasks.AccessAreaId IS NOT NULL AND dbo.vw_AccessArea.AccessAreaID IS NOT NULL) THEN dbo.vw_AccessArea.CountryName END) AS CountryName, 
                         dbo.vw_AccessArea.RegionCode, dbo.vw_AccessArea.RegionName, dbo.Tasks.DivisionId, dbo.CorporateDivision.Name AS DivisionName, dbo.CorporateDivision.UBR AS DivisionUBR, 
                         dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.CorporateBusinessArea.UBR AS BusinessAreaUBR, dbo.Tasks.BusinessAreaId, dbo.Tasks.TaskComment, dbo.vw_BadgeRequest.BadgeType, 
                         dbo.Tasks.Pending, hrc.Classification, arm.Comment AS RequestComment, hrp.Forename + ' ' + hrp.Surname AS RequestBy, ar.EndDate AS RequestEndDate, arm.RequestMasterID, 
                         ar.StartDate AS RequestStartDate, arp.Forename + ' ' + arp.Surname AS Applicant, dbo.LocationCountry.Name, hrpr.CountryName AS ApplicantCountryName
FROM            dbo.Tasks INNER JOIN
                         dbo.TaskStatusLookup ON dbo.TaskStatusLookup.TaskStatusId = dbo.Tasks.TaskStatusId INNER JOIN
                         dbo.TasksUsers ON dbo.TasksUsers.TaskId = dbo.Tasks.TaskId INNER JOIN
                         dbo.TaskTypeLookup ON dbo.TaskTypeLookup.TaskTypeId = dbo.Tasks.TaskTypeId LEFT OUTER JOIN
                         dbo.vw_BadgeRequest ON dbo.vw_BadgeRequest.RequestID = dbo.Tasks.RequestId LEFT OUTER JOIN
                         dbo.CorporateDivision ON dbo.CorporateDivision.DivisionID = dbo.Tasks.DivisionId LEFT OUTER JOIN
                         dbo.CorporateBusinessArea ON dbo.CorporateBusinessArea.BusinessAreaID = dbo.Tasks.BusinessAreaId LEFT OUTER JOIN
                         dbo.LocationCountry ON dbo.LocationCountry.CountryID = dbo.CorporateDivision.CountryID LEFT OUTER JOIN
                         dbo.vw_AccessArea ON dbo.vw_AccessArea.AccessAreaID = dbo.Tasks.AccessAreaId INNER JOIN
                         dbo.AccessRequest AS ar ON ar.RequestID = dbo.Tasks.RequestId INNER JOIN
                         dbo.AccessRequestPerson AS arp ON arp.RequestPersonID = ar.RequestPersonID INNER JOIN
                         dbo.AccessRequestMaster AS arm ON arm.RequestMasterID = arp.RequestMasterID LEFT OUTER JOIN
                         dbo.HRPerson AS hrp ON hrp.dbPeopleID = ar.CreatedByID LEFT OUTER JOIN
                         dbo.HRPerson AS hrpr ON hrpr.dbPeopleID = arp.dbPeopleID LEFT OUTER JOIN
                         dbo.HRClassification AS hrc ON hrc.HRClassificationID = ar.Classification
WHERE        (dbo.TasksUsers.IsCurrent = 1)

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasks]') )
	DROP VIEW [dbo].[vw_MyTasks]
GO

/****** Object:  View [dbo].[vw_MyTasks]    Script Date: 12/2/2015 12:31:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MyTasks]
AS
SELECT DISTINCT 
                         dbo.Tasks.TaskId, dbo.Tasks.TaskTypeId, dbo.TaskTypeLookup.Type, dbo.Tasks.TaskStatusId, dbo.TaskStatusLookup.Status, dbo.Tasks.Description, dbo.Tasks.CreatedByID, dbo.Tasks.CreatedDate, 
                         dbo.Tasks.CompletedByID, dbo.Tasks.CompletedDate, dbo.Tasks.RequestId, dbo.Tasks.AccessAreaId, dbo.TasksUsers.dbPeopleId, dbo.TasksUsers.IsCurrent, dbo.TasksUsers.[Order], 
                         dbo.vw_AccessArea.AccessAreaTypeID, dbo.vw_AccessArea.AccessAreaTypeName, dbo.vw_AccessArea.AccessAreaName, dbo.vw_AccessArea.FloorID, dbo.vw_AccessArea.FloorName, 
                         dbo.vw_AccessArea.BuildingID, dbo.vw_AccessArea.BuildingCode, dbo.vw_AccessArea.BuildingName, dbo.vw_AccessArea.CityID, dbo.vw_AccessArea.CityCode, dbo.vw_AccessArea.CityName, 
                         dbo.vw_AccessArea.CountryID, dbo.vw_AccessArea.CountryCode, (CASE WHEN (CorporateDivision.CountryID IS NOT NULL AND LocationCountry.CountryID IS NOT NULL AND dbo.Tasks.AccessAreaId IS NOT NULL
                          AND dbo.vw_AccessArea.AccessAreaID IS NOT NULL) THEN dbo.LocationCountry.[Name] WHEN (CorporateDivision.CountryID IS NOT NULL AND dbo.LocationCountry.CountryID IS NOT NULL) 
                         THEN dbo.LocationCountry.[Name] WHEN (dbo.Tasks.AccessAreaId IS NOT NULL AND dbo.vw_AccessArea.AccessAreaID IS NOT NULL) THEN dbo.vw_AccessArea.CountryName END) AS CountryName, 
                         dbo.vw_AccessArea.RegionCode, dbo.vw_AccessArea.RegionName, dbo.Tasks.DivisionId, dbo.CorporateDivision.Name AS DivisionName, dbo.CorporateDivision.UBR AS DivisionUBR, 
                         dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.CorporateBusinessArea.UBR AS BusinessAreaUBR, dbo.Tasks.BusinessAreaId, dbo.Tasks.TaskComment, dbo.vw_BadgeRequest.BadgeType, 
                         dbo.Tasks.Pending, hrc.Classification, arm.Comment AS RequestComment, hrp.Forename + ' ' + hrp.Surname AS RequestBy, ar.EndDate AS RequestEndDate, arm.RequestMasterID, 
                         ar.StartDate AS RequestStartDate, arp.Forename + ' ' + arp.Surname AS Applicant, dbo.LocationCountry.Name
FROM            dbo.Tasks INNER JOIN
                         dbo.TaskStatusLookup ON dbo.TaskStatusLookup.TaskStatusId = dbo.Tasks.TaskStatusId INNER JOIN
                         dbo.TasksUsers ON dbo.TasksUsers.TaskId = dbo.Tasks.TaskId INNER JOIN
                         dbo.TaskTypeLookup ON dbo.TaskTypeLookup.TaskTypeId = dbo.Tasks.TaskTypeId LEFT OUTER JOIN
                         dbo.vw_BadgeRequest ON dbo.vw_BadgeRequest.RequestID = dbo.Tasks.RequestId LEFT OUTER JOIN
                         dbo.CorporateDivision ON dbo.CorporateDivision.DivisionID = dbo.Tasks.DivisionId LEFT OUTER JOIN
                         dbo.CorporateBusinessArea ON dbo.CorporateBusinessArea.BusinessAreaID = dbo.Tasks.BusinessAreaId LEFT OUTER JOIN
                         dbo.LocationCountry ON dbo.LocationCountry.CountryID = dbo.CorporateDivision.CountryID LEFT OUTER JOIN
                         dbo.vw_AccessArea ON dbo.vw_AccessArea.AccessAreaID = dbo.Tasks.AccessAreaId INNER JOIN
                         dbo.AccessRequest AS ar ON ar.RequestID = dbo.Tasks.RequestId INNER JOIN
                         dbo.AccessRequestPerson AS arp ON arp.RequestPersonID = ar.RequestPersonID INNER JOIN
                         dbo.AccessRequestMaster AS arm ON arm.RequestMasterID = arp.RequestMasterID LEFT OUTER JOIN
                         dbo.HRPerson AS hrp ON hrp.dbPeopleID = ar.CreatedByID LEFT OUTER JOIN
                         dbo.HRClassification AS hrc ON hrc.HRClassificationID = ar.Classification
WHERE        (dbo.TasksUsers.IsCurrent = 1)

GO
