IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageCorporateArea')
BEGIN


CREATE TABLE [dbo].[XLImportStageCorporateArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Division] [nvarchar](500) NULL,
	[AccessArea] [nvarchar](500) NULL,
	[ApprovalType] [nvarchar](500) NULL,
	[NoOfApprovals] [nvarchar](500) NULL,
	[Frequency] [nvarchar](500) NULL,
	[LastCertifiedDate] [nvarchar](500) NULL,
	[NextCertifiedDate] [nvarchar](500) NULL,
	[FileID] [int] NOT NULL,
	[RowNumber] [int] NOT NULL,
	[ValidationFailed] [bit] NULL,
 CONSTRAINT [PK_XLImportStageCorporateArea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[XLImportStageCorporateArea]  WITH CHECK ADD  CONSTRAINT [FK_XLImportStageCorporateArea_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])

ALTER TABLE [dbo].[XLImportStageCorporateArea] CHECK CONSTRAINT [FK_XLImportStageCorporateArea_XLImportFile]


END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStageCorporateArea')
BEGIN
	
	DROP TABLE [dbo].[XLImportStageCorporateArea]
	
END

GO