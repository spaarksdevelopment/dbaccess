﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO

--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 10/10/2015
-- Description: Process create access trigger's 'DEL' action. Set HRPerson and mp_user as reoked,
--              cancel any pending pass requests and access requests and offboard if active user
--============================================================

CREATE PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
@dbPeopleID int
AS
BEGIN

	DECLARE @valid bit = 1
	DECLARE @CreatedByID int = null

	--Do nothing if HRPerson record hasn't been created
	IF(Not Exists(SELECT * FROM HRPerson WHERE dbPeopleID = @dbPeopleID))
		return

	--Update HRPerson as revoked
	update HRperson SET Revoked=1 WHERE dbPeopleID = @dbPeopleID
	update mp_user SET Revoked=1 WHERE dbPeopleID = @dbPeopleID

	--Check whether the person has active badges
	DECLARE @hasBadges bit = 0
	SELECT @hasBadges = count(PersonBadgeID) 
	FROM HRPersonBadge
	WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

	--delete the person's pending requests if any
	EXEC dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @hasBadges

	--Create revoke requests if person has approved or actioned pass/access request or a valid badge
	DECLARE @hasAccessAndPassRequestsForUser bit
	SELECT @hasAccessAndPassRequestsForUser = count(*) from AccessRequest ar
	inner join AccessRequestType rt on ar.RequestTypeID = rt.RequestTypeID
	inner join AccessRequestStatus rs on ar.RequestStatusID = rs.RequestStatusID
	inner join AccessRequestPerson arp on ar.RequestPersonID = arp.RequestPersonID
	WHERE rt.Name in ('New Pass','Access')
		and rs.Name in ('Approved', 'Actioned')
		and arp.dbPeopleID = @dbPeopleID

	IF(@hasBadges = 1 or @hasAccessAndPassRequestsForUser = 1)
	BEGIN
		--Create revoke request if person has a valid badge or actioned requests
		EXEC dbo.HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleID, @Valid = @Valid, @CreatedByID = @CreatedByID

		--Send emails to pass officers
		EXEC dbo.QueueEmailsForUser @dbPeopleId = @dbPeopleId, @EmailTypeID = 37
	END
END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_CreateAccessTrigger_DEL]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_CreateAccessTrigger_DEL]
END
GO

--============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 10/10/2015
-- Description: Process create access trigger's 'DEL' action. Set HRPerson and mp_user as reoked,
--              cancel any pending pass requests and access requests and offboard if active user
--============================================================

CREATE procedure [dbo].[up_CreateAccessTrigger_DEL]
@dbPeopleID int
as
begin

--exec up_CreateAccessTrigger_DEL 1111898
--declare @dbPeopleID int = 1111898

declare @valid bit = 1
declare @CreatedByID int = null

--Do nothing if HRPerson record hasn't been created
if(Not Exists(select * from HRPerson where dbPeopleID = @dbPeopleID))
	return

--Update HRPerson as revoked
update HRperson set Revoked=1 where dbPeopleID = @dbPeopleID
update mp_user set Revoked=1 where dbPeopleID = @dbPeopleID

--Check whether the person has active badges
declare @hasBadges bit = 0
select @hasBadges = count(PersonBadgeID) 
from HRPersonBadge
where dbPeopleID = @dbPeopleId and Valid = @Valid

--delete the person's pending requests if any
exec dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @hasBadges

--Create revoke requests if person has approved or actioned pass/access request or a valid badge
declare @hasAccessAndPassRequestsForUser bit
select @hasAccessAndPassRequestsForUser = count(*) from AccessRequest ar
inner join AccessRequestType rt on ar.RequestTypeID = rt.RequestTypeID
inner join AccessRequestStatus rs on ar.RequestStatusID = rs.RequestStatusID
inner join AccessRequestPerson arp on ar.RequestPersonID = arp.RequestPersonID
where rt.Name in ('New Pass','Access')
	and rs.Name in ('Approved', 'Actioned')
	and arp.dbPeopleID = @dbPeopleID

if(@hasBadges = 1 or @hasAccessAndPassRequestsForUser = 1)
begin
	--Create revoke request if person has a valid badge or actioned requests
	exec dbo.HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleID, @Valid = @Valid, @CreatedByID = @CreatedByID

	--Send emails to pass officers
	exec dbo.QueueRevokeAccessEmailsForUser @dbPeopleId = @dbPeopleId
end

end

GO