IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_RevokeAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_RevokeAccessForUser]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_RevokeAccessForUser]    Script Date: 11/11/2015 4:08:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==========================================================================
-- History: 28/05/2015 (Wijitha) - Modified pass office retrieving logic considering the building pass offices as well.
--								   Fixed issue of re creating tast for the home pass office	
--			11/11/2015 (Wijitha) - Used Generic request creation SP.	
--          02/12/2015 (Asanka) - Return RequestMasterID								
--==========================================================================

CREATE PROCEDURE [dbo].[HRFeed_RevokeAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int
AS 
BEGIN

DECLARE @RequestMasterID int

--Flag as Revoked
UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

EXEC @RequestMasterID = dbo.HRFeed_CreatePassOfficeRequestsForUser @dbPeopleID = @dbpeopleID,  @requestTypeID = 16, @createdById = null

EXEC dbo.HRFeed_QueuePassOfficeRequestEmailsForUser @dbPeopleID = @dbpeopleID, @EmailTypeID = 37

RETURN (@RequestMasterID)

END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_RevokeAccessForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_RevokeAccessForUser]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeed_RevokeAccessForUser]    Script Date: 11/11/2015 1:03:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==========================================================================
-- History: 28/05/2015 (Wijitha) - Modified pass office retrieving logic considering the building pass offices as well.
--								   Fixed issue of re creating tast for the home pass office		
--			11/11/2015 (Wijitha) - Used Generic request creation SP.									
--==========================================================================

CREATE PROCEDURE [dbo].[HRFeed_RevokeAccessForUser] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
AS 
BEGIN


--Flag as Revoked
UPDATE HRPerson SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId
UPDATE mp_User SET Revoked = 1 WHERE dbPeopleID = @dbPeopleId

EXEC dbo.HRFeed_CreatePassOfficeRequestsForUser @dbPeopleID = @dbpeopleID,  @requestTypeID = 16, @createdById = null

EXEC dbo.HRFeed_QueuePassOfficeRequestEmailsForUser @dbPeopleID = @dbpeopleID, @EmailTypeID = 37

END

GO


