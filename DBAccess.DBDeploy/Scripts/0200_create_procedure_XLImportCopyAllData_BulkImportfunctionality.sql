IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAllData]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAllData]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 18/12/2015
-- Description: Copy data from staging tables to actual tables
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyAllData]
@FileID int,
@ErrorMessage nvarchar(max) = null output
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit = 1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	--===================================================================================
	--Execute each SP to copy each staged entity according to the dependency order.
	-- 1-Cities, 2-Pass Offices, 3-Buildings, 4-Divisions, 5-Division Roles, 6-Access Areas, 7-Corporate Areas, 8-Access Area Approvers, 9-Access Area Recertifiers, 10-Pass Office Users
	--===================================================================================

	BEGIN TRY

		BEGIN TRANSACTION

		EXEC XLImportCopyCities @FileID -- 1) Cities
		EXEC XLImportCopyPassOffices @FileID -- 2) Pass Offices
		EXEC XLImportCopyBuildings @FileID --3) Buildings
		EXEC XLImportCopyDivisions @FileID -- 4) Divisions
		EXEC XLImportCopyDivisionRoles @FileID -- 5) Division Roles
		EXEC XLImportCopyAccessArea @FileID -- 6) Access Areas
		EXEC XLImportCopyCorporateAreas @FileID -- 7) Corporate Areas
		EXEC XLImportCopyAccessAreaApprovers @FileID -- 8) Access Area Approvers
		EXEC XLImportCopyAccessAreaRecertifiers @FileID-- 9) Access Area Recertifiers
		EXEC XLImportCopyPassOfficeUsers @FileID-- 10) Pass Office Users
		UPDATE XLImportFile SET Processed = 1, ErrorMessage = null WHERE ID = @FileID

		--Clear all the staging and temp data after successfully processing the file
		EXEC XLImportClearStagingTables

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		IF(@@TRANCOUNT > 0)
			ROLLBACK TRANSACTION

	    SET @ErrorMessage = ERROR_MESSAGE()

		UPDATE XLImportFile SET Processed = 0, ErrorMessage =  @ErrorMessage WHERE ID = @FileID

		SET @ErrorMessage = 'Error importing data. Contact your system administrator for help.'

	END CATCH

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyAllData]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyAllData]
END
GO