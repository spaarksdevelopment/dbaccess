IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasksDetails]'))
DROP VIEW [dbo].vw_MyTasksDetails
GO
/****** Object:  View [dbo].[[vw_MyTasksDetails]    Script Date: 11/11/2015 11:34:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MyTasksDetails]
AS
SELECT DISTINCT 
                         vmt.TaskId, vmt.TaskTypeId, vmt.Type, vmt.TaskStatusId, vmt.Status, REPLACE(ISNULL(vmt.Description, ''), CHAR(13) + CHAR(10), ' ') AS Description, 
                         CASE WHEN ar.RequestTypeID = 15 THEN 'Off Boarding ' + Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) 
                         + ') ')), '{2}', IsNull(vmt.[CountryName], '')), '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], '')), '{4}', CASE IsNull(vmt.[BadgeType], '') 
                         WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND vmt.[BuildingName] IS NOT NULL 
                         THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)) 
                         + (CASE WHEN [dbo].[DoOffboardResultsAccessAreasHavingTooFewApprovers](arp.[dbPeopleId]) = 1 THEN '.  This results some access areas having too few approvers' ELSE '' END) 
                         ELSE Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description], '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')), '{2}', IsNull(vmt.[CountryName], '')), '{3}', 
                         IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR], '')) + ' - ' + vmt.[DivisionName], '')), '{4}', CASE IsNull(vmt.[BadgeType], '') 
                         WHEN 'DB' THEN 'replacement DB' WHEN 'LL' THEN 'replacement Landlord' ELSE 'new DB' END), '{5}', CASE WHEN IsNull(vmt.[BadgeType], '') = 'LL' AND vmt.[BuildingName] IS NOT NULL 
                         THEN ' for access to ' + vmt.[BuildingName] ELSE '' END), '{6}', CASE IsNull(vmt.[BadgeType], '') WHEN 'DB' THEN 'DB' WHEN 'LL' THEN 'Landlord' ELSE 'DB' END), '{7}', 
                         (CASE WHEN vmt.[BuildingName] IS NOT NULL THEN ' ' + vmt.[BuildingName] + '/' ELSE '' END) + (CASE WHEN vmt.[FloorName] IS NOT NULL THEN ' ' + vmt.[FloorName] + '/' ELSE '' END) 
                         + (CASE WHEN vmt.[AccessAreaName] IS NOT NULL THEN ' ' + vmt.[AccessAreaName] ELSE '' END) + ' (' + (CASE WHEN vmt.[AccessAreaTypeName] IS NOT NULL 
                         THEN ' ' + vmt.[AccessAreaTypeName] ELSE '' END) + ')' + (CASE WHEN vmt.[RequestEndDate] IS NOT NULL THEN ' until ' + CONVERT(VarChar(11), vmt.[RequestEndDate], 106) ELSE '' END)) END AS Descript, 
                         vmt.CreatedByID, vmt.CreatedDate, vmt.CompletedByID, vmt.CompletedDate, vmt.RequestId, vmt.AccessAreaId, vmt.dbPeopleId, vmt.IsCurrent, vmt.[Order], vmt.AccessAreaTypeID, vmt.AccessAreaTypeName, 
                         vmt.AccessAreaName, vmt.FloorID, vmt.FloorName, vmt.BuildingID, vmt.BuildingCode, vmt.BuildingName, vmt.CityID, vmt.CityCode, vmt.CityName, vmt.CountryID, vmt.CountryCode, vmt.CountryName, 
                         vmt.RegionCode, vmt.RegionName, vmt.DivisionId, vmt.DivisionName, vmt.DivisionUBR, vmt.BusinessAreaName, vmt.BusinessAreaUBR, vmt.BusinessAreaId, vmt.TaskComment, vmt.BadgeType, 
                         vmt.Classification, vmt.RequestComment, vmt.RequestBy, vmt.RequestEndDate, vmt.RequestMasterID, vmt.RequestStartDate, vmt.Applicant, vmt.Pending, COALESCE (bjr.Name, ajr.Name, '') 
                         AS Justification
FROM            dbo.vw_MyTasks AS vmt INNER JOIN
                         dbo.TaskTypeLookup AS tsl ON tsl.TaskTypeId = vmt.TaskTypeId INNER JOIN
                         dbo.AccessRequest AS ar ON ar.RequestID = vmt.RequestId INNER JOIN
                         dbo.AccessRequestPerson AS arp ON arp.RequestPersonID = ar.RequestPersonID LEFT OUTER JOIN
                         dbo.BadgeJustificationReason AS bjr ON ar.BadgeJustificationReasonID = bjr.ID LEFT OUTER JOIN
                         dbo.AccessRequestJustificationReason AS ajr ON ar.AccessRequestJustificationReasonID = ajr.ID


GO


--//@UNDO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyTasksDetails]'))
DROP VIEW [dbo].vw_MyTasksDetails
GO
/****** Object:  View [dbo].[vw_MyTasksDetails]   Script Date: 11/10/2015 11:24:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_MyTasksDetails]
       AS
        SELECT Distinct
              vmt.[TaskId]
         ,vmt.[TaskTypeId]
         ,vmt.[Type]
         ,vmt.[TaskStatusId]
         ,vmt.[Status]
         ,Replace(IsNull(vmt.[Description],''), char(13) + char(10), ' ') AS [Description]
         --,Replace(Replace(Replace(Replace(Replace(tsl.[Description],'{1}', vmt.[Applicant] + ' (' + arp.[EmailAddress] + ') '),'{2}', IsNull(vmt.[CountryName],'')),'{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR],'')) + ' - ' + vmt.[DivisionName], '')),'{4}',''),'{5}','') AS [Descript]
         ,CASE
              WHEN ar.RequestTypeID = 15 THEN
                     'Off Boarding ' +     
                     Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description],
                     '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')),
                     '{2}', IsNull(vmt.[CountryName],'')),
                     '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR],'')) + ' - ' + vmt.[DivisionName], '')),
                     '{4}', Case IsNull(vmt.[BadgeType],'') When 'DB' Then 'replacement DB' When 'LL' Then 'replacement Landlord' Else 'new DB' End),
                     '{5}', Case When IsNull(vmt.[BadgeType],'') = 'LL' And vmt.[BuildingName] Is Not Null Then ' for access to ' + vmt.[BuildingName] Else '' End),
                     '{6}', Case IsNull(vmt.[BadgeType],'') When 'DB' Then 'DB' When 'LL' Then 'Landlord' Else 'DB' End),
                     '{7}', (Case When vmt.[BuildingName] Is Not Null Then ' ' + vmt.[BuildingName] + '/' Else '' End) + (Case When vmt.[FloorName] Is Not Null Then ' ' + vmt.[FloorName] + '/' Else '' End) + (Case When vmt.[AccessAreaName] Is Not Null Then ' ' + vmt.[AccessAreaName] Else '' End) + ' (' + (Case When vmt.[AccessAreaTypeName] Is Not Null Then ' ' + vmt.[AccessAreaTypeName] Else '' End) + ')' + (Case When vmt.[RequestEndDate] Is Not Null Then ' until ' + Convert(VarChar(11), vmt.[RequestEndDate], 106) Else '' End))
              ELSE
                     Replace(Replace(Replace(Replace(Replace(REPLACE(Replace(tsl.[Description],
                     '{1}', vmt.[Applicant] + (' (' + CAST(arp.[dbPeopleId] AS NVARCHAR(50)) + ') ')),
                     '{2}', IsNull(vmt.[CountryName],'')),
                     '{3}', IsNull(IsNull(vmt.[DivisionUBR], ISNULL(vmt.[BusinessAreaUBR],'')) + ' - ' + vmt.[DivisionName], '')),
                     '{4}', Case IsNull(vmt.[BadgeType],'') When 'DB' Then 'replacement DB' When 'LL' Then 'replacement Landlord' Else 'new DB' End),
                     '{5}', Case When IsNull(vmt.[BadgeType],'') = 'LL' And vmt.[BuildingName] Is Not Null Then ' for access to ' + vmt.[BuildingName] Else '' End),
                     '{6}', Case IsNull(vmt.[BadgeType],'') When 'DB' Then 'DB' When 'LL' Then 'Landlord' Else 'DB' End),
                     '{7}', (Case When vmt.[BuildingName] Is Not Null Then ' ' + vmt.[BuildingName] + '/' Else '' End) + (Case When vmt.[FloorName] Is Not Null Then ' ' + vmt.[FloorName] + '/' Else '' End) + (Case When vmt.[AccessAreaName] Is Not Null Then ' ' + vmt.[AccessAreaName] Else '' End) + ' (' + (Case When vmt.[AccessAreaTypeName] Is Not Null Then ' ' + vmt.[AccessAreaTypeName] Else '' End) + ')' + (Case When vmt.[RequestEndDate] Is Not Null Then ' until ' + Convert(VarChar(11), vmt.[RequestEndDate], 106) Else '' End))
              END As [Descript]
 
         ,vmt.[CreatedByID]
         ,vmt.[CreatedDate]
         ,vmt.[CompletedByID]
         ,vmt.[CompletedDate]
         ,vmt.[RequestId]
         ,vmt.[AccessAreaId]
         ,vmt.[dbPeopleId]
         ,vmt.[IsCurrent]
         ,vmt.[Order]
         ,vmt.[AccessAreaTypeID]
         ,vmt.[AccessAreaTypeName]
         ,vmt.[AccessAreaName]
         ,vmt.[FloorID]
         ,vmt.[FloorName]
         ,vmt.[BuildingID]
         ,vmt.[BuildingCode]
         ,vmt.[BuildingName]
         ,vmt.[CityID]
         ,vmt.[CityCode]
         ,vmt.[CityName]
         ,vmt.[CountryID]
         ,vmt.[CountryCode]
         ,vmt.[CountryName]
         ,vmt.[RegionCode]
         ,vmt.[RegionName]
         ,vmt.[DivisionId]
         ,vmt.[DivisionName]
         ,vmt.[DivisionUBR]
         ,vmt.[BusinessAreaName]
         ,vmt.[BusinessAreaUBR]
         ,vmt.[BusinessAreaId]
         ,vmt.[TaskComment]
         ,vmt.[BadgeType]
         ,vmt.[Classification]
         ,vmt.[RequestComment]
         ,vmt.[RequestBy]
         ,vmt.[RequestEndDate]
         ,vmt.[RequestMasterID]
         ,vmt.[RequestStartDate]
         ,vmt.[Applicant]
         ,vmt.[Pending]
         ,COALESCE(bjr.Name, ajr.Name, '') Justification
  FROM
              [dbo].[vw_MyTasks]vmt
       INNER JOIN [dbo].[TaskTypeLookup]tsl ON tsl.[TaskTypeId] = vmt.[TaskTypeId]
       INNER JOIN [dbo].[AccessRequest] ar ON ar.[RequestID] = vmt.[RequestId]
       INNER JOIN [dbo].[AccessRequestPerson] arp ON arp.[RequestPersonID] = ar.[RequestPersonID]
       LEFT JOIN dbo.BadgeJustificationReason bjr ON ar.BadgeJustificationReasonID = bjr.ID
       LEFT JOIN dbo.AccessRequestJustificationReason ajr ON ar.AccessRequestJustificationReasonID = ajr.ID
 
 


GO



