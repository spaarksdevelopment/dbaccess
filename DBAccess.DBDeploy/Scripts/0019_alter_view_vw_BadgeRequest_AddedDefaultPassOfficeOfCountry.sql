/****** Object:  View [dbo].[vw_BadgeRequest]    Script Date: 5/18/2015 3:14:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_BadgeRequest]
AS
SELECT     dbo.AccessRequestMaster.RequestMasterID, dbo.AccessRequestMaster.CreatedByID AS MasterCreatedByUserID, 
                      dbo.AccessRequestMaster.Created AS MasterCreated, dbo.AccessRequestPerson.RequestPersonID, dbo.AccessRequestPerson.dbPeopleID, 
                      CASE HR.IsExternal WHEN 1 THEN 'C' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) 
                      ELSE ' ' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) END AS FulldbPeopleID, dbo.AccessRequestPerson.Forename, 
                      dbo.AccessRequestPerson.Surname, dbo.AccessRequestPerson.CostCentre, dbo.CostCentre.CostCentreName, dbo.UBRCode.UBR_Code, dbo.UBRCode.UBR_Name, 
                      dbo.AccessRequestPerson.Telephone, dbo.AccessRequestPerson.EmailAddress, dbo.AccessRequestPerson.VendorID, dbo.AccessRequestPerson.VendorName, 
                      dbo.AccessRequestPerson.DivisionID, dbo.CorporateDivision.Name AS DivisionName, dbo.AccessRequestPerson.BusinessAreaID, 
                      dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.AccessRequestPerson.IsExternal, dbo.AccessRequest.RequestID, 
                      dbo.AccessRequest.RequestStatusID, dbo.AccessRequest.BadgeID, dbo.HRPersonBadge.BadgeType, dbo.HRPersonBadge.MiFareNumber, 
                      dbo.HRPersonBadge.LandlordID, dbo.ControlLandlord.Name AS LandlordName, dbo.HRPersonBadge.BuildingID, dbo.LocationBuilding.Name AS BuildingName, 
                      dbo.AccessRequestPerson.CreatedByID AS PersonCreatedByUserID, dbo.AccessRequestPerson.Created AS PersonCreated, 
                      CASE WHEN dbo.AccessRequest.BadgeID IS NULL THEN 'N' ELSE 'R' END AS Issue, CASE ISNULL(dbo.HRPersonBadge.BadgeType, '') 
                      WHEN 'LL' THEN 'Landlord: ' + dbo.LocationBuilding.Name + ' (' + dbo.ControlLandlord.Name + ')' ELSE dbo.HRPersonBadge.BadgeType END AS BadgeTag, 
                      dbo.AccessRequest.CreatedByID, dbo.AccessRequest.Created, dbo.AccessRequestPerson.CountryID, dbo.LocationCountry.Name AS CountryName, 
                      dbo.AccessRequestMaster.Comment, dbo.AccessRequestDeliveryDetail.DeliveryOptionID, dbo.AccessRequestDeliveryDetail.PassOfficeID, 
                      dbo.AccessRequestDeliveryDetail.ContactEmail, dbo.AccessRequestDeliveryDetail.ContactTelephone, dbo.AccessRequestDeliveryOption.DeliveryOption, 
                      dbo.LocationPassOffice.Name AS PassOfficeName, dbo.LocationPassOffice.Code AS PassOfficeCode, dbo.LocationPassOffice.Address AS PassOfficeAddress, 
                      dbo.AccessRequest.StartDate, dbo.AccessRequest.EndDate,
                      dbo.LocationCountry.PassOfficeID As CountryDefaultPassOffice, HR.dbDirID
FROM         dbo.UBRCode RIGHT OUTER JOIN
                      dbo.AccessRequest INNER JOIN
                      dbo.AccessRequestPerson ON dbo.AccessRequest.RequestPersonID = dbo.AccessRequestPerson.RequestPersonID INNER JOIN
                      dbo.AccessRequestMaster ON dbo.AccessRequestPerson.RequestMasterID = dbo.AccessRequestMaster.RequestMasterID LEFT OUTER JOIN
                      dbo.AccessRequestDeliveryDetail INNER JOIN
                      dbo.AccessRequestDeliveryOption ON dbo.AccessRequestDeliveryDetail.DeliveryOptionID = dbo.AccessRequestDeliveryOption.DeliveryOptionID LEFT OUTER JOIN
                      dbo.LocationPassOffice ON dbo.AccessRequestDeliveryDetail.PassOfficeID = dbo.LocationPassOffice.PassOfficeID ON 
                      dbo.AccessRequestMaster.RequestMasterID = dbo.AccessRequestDeliveryDetail.RequestMasterID LEFT OUTER JOIN
                      dbo.LocationCountry ON dbo.AccessRequestPerson.CountryID = dbo.LocationCountry.CountryID ON 
                      dbo.UBRCode.UBR_Code = dbo.AccessRequestPerson.UBR LEFT OUTER JOIN
                      dbo.HRPersonBadge LEFT OUTER JOIN
                      dbo.LocationBuilding ON dbo.HRPersonBadge.BuildingID = dbo.LocationBuilding.BuildingID LEFT OUTER JOIN
                      dbo.ControlLandlord ON dbo.HRPersonBadge.LandlordID = dbo.ControlLandlord.LandlordID ON 
                      dbo.AccessRequest.BadgeID = dbo.HRPersonBadge.PersonBadgeID LEFT OUTER JOIN
                      dbo.CostCentre ON dbo.AccessRequestPerson.CostCentre = dbo.CostCentre.CostCentre LEFT OUTER JOIN
                      dbo.CorporateBusinessArea ON dbo.AccessRequestPerson.BusinessAreaID = dbo.CorporateBusinessArea.BusinessAreaID LEFT OUTER JOIN
                      dbo.CorporateDivision ON dbo.AccessRequestPerson.DivisionID = dbo.CorporateDivision.DivisionID
                      INNER JOIN dbo.HrPerson as HR on HR.DbPeopleID =   dbo.AccessRequestPerson.dbPeopleID
WHERE     (dbo.AccessRequest.RequestTypeID = 1)


GO

--//@UNDO

/****** Object:  View [dbo].[vw_BadgeRequest]    Script Date: 5/18/2015 3:16:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_BadgeRequest]
AS
SELECT     dbo.AccessRequestMaster.RequestMasterID, dbo.AccessRequestMaster.CreatedByID AS MasterCreatedByUserID, 
                      dbo.AccessRequestMaster.Created AS MasterCreated, dbo.AccessRequestPerson.RequestPersonID, dbo.AccessRequestPerson.dbPeopleID, 
                      CASE HR.IsExternal WHEN 1 THEN 'C' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) 
                      ELSE ' ' + CAST(dbo.AccessRequestPerson.dbPeopleID AS VARCHAR(7)) END AS FulldbPeopleID, dbo.AccessRequestPerson.Forename, 
                      dbo.AccessRequestPerson.Surname, dbo.AccessRequestPerson.CostCentre, dbo.CostCentre.CostCentreName, dbo.UBRCode.UBR_Code, dbo.UBRCode.UBR_Name, 
                      dbo.AccessRequestPerson.Telephone, dbo.AccessRequestPerson.EmailAddress, dbo.AccessRequestPerson.VendorID, dbo.AccessRequestPerson.VendorName, 
                      dbo.AccessRequestPerson.DivisionID, dbo.CorporateDivision.Name AS DivisionName, dbo.AccessRequestPerson.BusinessAreaID, 
                      dbo.CorporateBusinessArea.Name AS BusinessAreaName, dbo.AccessRequestPerson.IsExternal, dbo.AccessRequest.RequestID, 
                      dbo.AccessRequest.RequestStatusID, dbo.AccessRequest.BadgeID, dbo.HRPersonBadge.BadgeType, dbo.HRPersonBadge.MiFareNumber, 
                      dbo.HRPersonBadge.LandlordID, dbo.ControlLandlord.Name AS LandlordName, dbo.HRPersonBadge.BuildingID, dbo.LocationBuilding.Name AS BuildingName, 
                      dbo.AccessRequestPerson.CreatedByID AS PersonCreatedByUserID, dbo.AccessRequestPerson.Created AS PersonCreated, 
                      CASE WHEN dbo.AccessRequest.BadgeID IS NULL THEN 'N' ELSE 'R' END AS Issue, CASE ISNULL(dbo.HRPersonBadge.BadgeType, '') 
                      WHEN 'LL' THEN 'Landlord: ' + dbo.LocationBuilding.Name + ' (' + dbo.ControlLandlord.Name + ')' ELSE dbo.HRPersonBadge.BadgeType END AS BadgeTag, 
                      dbo.AccessRequest.CreatedByID, dbo.AccessRequest.Created, dbo.AccessRequestPerson.CountryID, dbo.LocationCountry.Name AS CountryName, 
                      dbo.AccessRequestMaster.Comment, dbo.AccessRequestDeliveryDetail.DeliveryOptionID, dbo.AccessRequestDeliveryDetail.PassOfficeID, 
                      dbo.AccessRequestDeliveryDetail.ContactEmail, dbo.AccessRequestDeliveryDetail.ContactTelephone, dbo.AccessRequestDeliveryOption.DeliveryOption, 
                      dbo.LocationPassOffice.Name AS PassOfficeName, dbo.LocationPassOffice.Code AS PassOfficeCode, dbo.LocationPassOffice.Address AS PassOfficeAddress, 
                      dbo.AccessRequest.StartDate, dbo.AccessRequest.EndDate,
                      HR.dbDirID
FROM         dbo.UBRCode RIGHT OUTER JOIN
                      dbo.AccessRequest INNER JOIN
                      dbo.AccessRequestPerson ON dbo.AccessRequest.RequestPersonID = dbo.AccessRequestPerson.RequestPersonID INNER JOIN
                      dbo.AccessRequestMaster ON dbo.AccessRequestPerson.RequestMasterID = dbo.AccessRequestMaster.RequestMasterID LEFT OUTER JOIN
                      dbo.AccessRequestDeliveryDetail INNER JOIN
                      dbo.AccessRequestDeliveryOption ON dbo.AccessRequestDeliveryDetail.DeliveryOptionID = dbo.AccessRequestDeliveryOption.DeliveryOptionID LEFT OUTER JOIN
                      dbo.LocationPassOffice ON dbo.AccessRequestDeliveryDetail.PassOfficeID = dbo.LocationPassOffice.PassOfficeID ON 
                      dbo.AccessRequestMaster.RequestMasterID = dbo.AccessRequestDeliveryDetail.RequestMasterID LEFT OUTER JOIN
                      dbo.LocationCountry ON dbo.AccessRequestPerson.CountryID = dbo.LocationCountry.CountryID ON 
                      dbo.UBRCode.UBR_Code = dbo.AccessRequestPerson.UBR LEFT OUTER JOIN
                      dbo.HRPersonBadge LEFT OUTER JOIN
                      dbo.LocationBuilding ON dbo.HRPersonBadge.BuildingID = dbo.LocationBuilding.BuildingID LEFT OUTER JOIN
                      dbo.ControlLandlord ON dbo.HRPersonBadge.LandlordID = dbo.ControlLandlord.LandlordID ON 
                      dbo.AccessRequest.BadgeID = dbo.HRPersonBadge.PersonBadgeID LEFT OUTER JOIN
                      dbo.CostCentre ON dbo.AccessRequestPerson.CostCentre = dbo.CostCentre.CostCentre LEFT OUTER JOIN
                      dbo.CorporateBusinessArea ON dbo.AccessRequestPerson.BusinessAreaID = dbo.CorporateBusinessArea.BusinessAreaID LEFT OUTER JOIN
                      dbo.CorporateDivision ON dbo.AccessRequestPerson.DivisionID = dbo.CorporateDivision.DivisionID
                      INNER JOIN dbo.HrPerson as HR on HR.DbPeopleID =   dbo.AccessRequestPerson.dbPeopleID
WHERE     (dbo.AccessRequest.RequestTypeID = 1)

GO





