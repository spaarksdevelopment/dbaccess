IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DAAccessReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_DAAccessReportSearch]
END
GO

CREATE PROCEDURE [dbo].[sp_DAAccessReportSearch]
(
	@LanguageID INT=1,
	@TaskCreatedStartDate datetime = NULL,
	@TaskCreatedEndDate datetime = NULL,
    @PersonNameSearchType nvarchar(50) = 'Contains',
	@PersonNameValueStart nvarchar(150) = '',
	@PersonNameValueEnd nvarchar(150) = '',
	@PersonEmpIdSearchType nvarchar(50) = 'Contains',
	@PersonEmpIdValueStart nvarchar(150) = '',
	@PersonEmpIdValueEnd nvarchar(150) = '',
	@PersonEmailSearchType nvarchar(50) = 'Contains',
	@PersonEmailValueStart nvarchar(150) = '',
	@PersonEmailalueEnd nvarchar(150) = '',
	@PersonDbDirIdSearchType nvarchar(50) = 'Contains',
	@PersonDbDirIdValueStart nvarchar(150) = '',
	@PersonDbDirIdValueEnd nvarchar(150) = '',
	@RequestorNameSearchType nvarchar(50) = 'Contains'	,
	@RequestorNameValueStart nvarchar(150) = '',
	@RequestorNameValueEnd nvarchar(150) = '',	
	@RequestorEmpIdSearchType nvarchar(50) = 'Contains',
	@RequestorEmpIdValueStart nvarchar(150) = '',
	@RequestorEmpIdValueEnd nvarchar(150) = '',
	@RequestorEmailSearchType nvarchar(50) = 'Contains',
	@RequestorEmailValueStart nvarchar(150) = '',
	@RequestorEmailalueEnd nvarchar(150) = '',
	@RequestorDbDirIdSearchType nvarchar(50) = 'Contains',
	@RequestorDbDirIdValueStart nvarchar(150) = '',
	@RequestorDbDirIdValueEnd nvarchar(150) = '',
	@TaskStatusId int = NULL,
	@CountryID int = NULL , 
	@CityID int = NULL ,
	@BuildingID int = NULL ,
	@FloorID int = NULL,
	@Restricted bit = 1,
	@SearchingUserId int,
	@RequestStatusFullId int = NULL,
	@AccessAreaId int = NULL,
	@RequestTypeID int = NULL
)

AS
BEGIN
SET NOCOUNT ON

	DECLARE @UBRTable TABLE (UBR_Code NVARCHAR(6), UBR_Name nvarchar(50), Parent NVARCHAR(6))
	DECLARE @AccessAreaIdsTable TABLE (AccessAreaId INT NULL)
 
	IF (@Restricted = 1)
	BEGIN 
	
		DECLARE @CurrentUBR nchar(6)
		DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR

		SELECT DISTINCT UBR
		  FROM [CorporateDivision]
		  INNER JOIN dbo.CorporateDivisionRoleUser ON [CorporateDivision].DivisionID = CorporateDivisionRoleUser.DivisionID
		  WHERE CorporateDivisionRoleUser.dbPeopleID = @SearchingUserId		  
		  
		 OPEN myCursor
		 
		 FETCH NEXT FROM myCursor INTO @CurrentUBR
		 
		 WHILE @@FETCH_STATUS = 0
		 BEGIN
			INSERT INTO @UBRTable exec [dbo].[up_GetUBRNodes] @CurrentUBR		 
			FETCH NEXT FROM myCursor INTO @CurrentUBR
		 END
		 
		 CLOSE myCursor
		 DEALLOCATE myCursor	
		 
		 INSERT INTO @AccessAreaIdsTable VALUES (NULL)
		 
		 INSERT INTO @AccessAreaIdsTable 
		 Select Distinct AccessAreaID
	From
	(
		Select
			 vw_DivisionAccessArea.CorporateDivisionAccessAreaID
			,vw_DivisionAccessArea.CountryID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.CountryName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.CountryName) From LocationCountry where  CountryID = vw_DivisionAccessArea.CountryID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.CountryName) From LocationCountry where  CountryID = vw_DivisionAccessArea.CountryID)
			end as CountryName
			,vw_DivisionAccessArea.CityID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.CityName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.CityName) From LocationCity where  CityID = vw_DivisionAccessArea.CityID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.CityName) From LocationCity where  CityID = vw_DivisionAccessArea.CityID) 
			end as CityName
			,vw_DivisionAccessArea.BuildingID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.BuildingName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.BuildingName) From LocationBuilding where  BuildingID = vw_DivisionAccessArea.BuildingID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.BuildingName) From LocationBuilding where  BuildingID = vw_DivisionAccessArea.BuildingID)
			end as BuildingName
			,vw_DivisionAccessArea.FloorID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.FloorName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.FloorName) From LocationFloor where  FloorID = vw_DivisionAccessArea.FloorID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.FloorName) From LocationFloor where  FloorID = vw_DivisionAccessArea.FloorID)
			end as FloorName
			,vw_DivisionAccessArea.AccessAreaID			
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.AccessAreaName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.AccessAreaName) From AccessArea where  AccessAreaID = vw_DivisionAccessArea.AccessAreaID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.AccessAreaName) From AccessArea where  AccessAreaID = vw_DivisionAccessArea.AccessAreaID)
			end as AccessAreaName			
			,vw_DivisionAccessArea.AccessAreaEnabled
			,vw_DivisionAccessArea.AccessAreaTypeID
			,vw_DivisionAccessArea.AccessAreaTypeName
			,vw_DivisionAccessArea.AccessAreaTypeDescription
			,vw_DivisionAccessArea.DivisionID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.DivisionName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.DivisionName) From CorporateDivision where  DivisionID = vw_DivisionAccessArea.DivisionID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.DivisionName) From CorporateDivision where  DivisionID = vw_DivisionAccessArea.DivisionID)
			end as DivisionName
			,vw_DivisionAccessArea.DivisionEnabled
			,vw_DivisionAccessArea.NumberOfApprovals
			,vw_DivisionAccessArea.ApprovalTypeID
			,vw_DivisionAccessArea.ApprovalType
			,vw_DivisionAccessArea.[Description]
			,vw_DivisionAccessArea.ControlSystemID
			,vw_DivisionAccessArea.ControlSystemName
			,vw_DivisionAccessArea.AccessControlID
			,vw_DivisionAccessArea.Frequency
			,vw_DivisionAccessArea.LastCertifiedDate
			,vw_DivisionAccessArea.DivisionAccessAreaEnabled
			
		From
			vw_DivisionAccessArea
		Left Outer Join
			CorporateDivisionAccessAreaApprover
			On
			CorporateDivisionAccessAreaApprover.CorporateDivisionAccessAreaID = vw_DivisionAccessArea.CorporateDivisionAccessAreaID
		Left Outer Join
			mp_SecurityGroup
			On
			CorporateDivisionAccessAreaApprover.mp_SecurityGroupID = mp_SecurityGroup.Id
		Left Outer Join
			mp_User u1
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u1.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 1 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u2
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u2.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 2 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u3
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u3.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 3 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u4
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u4.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 4 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u5
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u5.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 5 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Where
		(
			(
				(
					vw_DivisionAccessArea.DivisionID
					In
					(
						Select Distinct
							 CorporateDivision.DivisionID
						From
							CorporateDivision
						Inner Join
							LocationCountry
							On
							LocationCountry.CountryID = CorporateDivision.CountryID
						Where
							CorporateDivision.[Enabled] = 1
					)
					And Exists
					(
						Select
							Id
						From
							mp_SecurityGroupUser
						Where
							dbPeopleID = @SearchingUserId
							And
							SecurityGroupId = 2		-- System Administrator (ADMIN)
					)
				)
				Or
				vw_DivisionAccessArea.DivisionID
				In
				(
					Select Distinct
						 CorporateDivision.DivisionID
					From
						CorporateDivision
					Inner Join
						LocationCountry
						On
						LocationCountry.CountryID = CorporateDivision.CountryID
					Inner Join
						LocationCountryUser
						On
						LocationCountryUser.CountryID = LocationCountry.CountryID
					Inner Join
						CorporateDivisionRoleUser
						On
						CorporateDivisionRoleUser.DivisionID = CorporateDivision.DivisionID
					Where
						CorporateDivision.[Enabled] = 1
						And
						LocationCountryUser.dbPeopleID = @SearchingUserId
						And
						CorporateDivisionRoleUser.dbPeopleID = @SearchingUserId
						And
						CorporateDivisionRoleUser.mp_SecurityGroupID In (3, 7)		-- Division Administrator (DA) / Division Owner (DO)
				)
			)
		)
		Group By
			vw_DivisionAccessArea.CorporateDivisionAccessAreaID
			,vw_DivisionAccessArea.CountryID
			,vw_DivisionAccessArea.CountryName
			,vw_DivisionAccessArea.CityID
			,vw_DivisionAccessArea.CityName
			,vw_DivisionAccessArea.BuildingID
			,vw_DivisionAccessArea.BuildingName
			,vw_DivisionAccessArea.FloorID
			,vw_DivisionAccessArea.FloorName
			,vw_DivisionAccessArea.AccessAreaID
			,vw_DivisionAccessArea.AccessAreaName
			,vw_DivisionAccessArea.AccessAreaEnabled
			,vw_DivisionAccessArea.AccessAreaTypeID
			,vw_DivisionAccessArea.AccessAreaTypeName
			,vw_DivisionAccessArea.AccessAreaTypeDescription
			,vw_DivisionAccessArea.DivisionID
			,vw_DivisionAccessArea.DivisionName
			,vw_DivisionAccessArea.DivisionEnabled
			,vw_DivisionAccessArea.NumberOfApprovals
			,vw_DivisionAccessArea.ApprovalTypeID
			,vw_DivisionAccessArea.ApprovalType
			,vw_DivisionAccessArea.[Description]
			,vw_DivisionAccessArea.ControlSystemID
			,vw_DivisionAccessArea.ControlSystemName
			,vw_DivisionAccessArea.AccessControlID
			,vw_DivisionAccessArea.Frequency
			,vw_DivisionAccessArea.LastCertifiedDate
			,vw_DivisionAccessArea.DivisionAccessAreaEnabled
		
		
		
	) As
		temptable
	Where
		DivisionEnabled = 1
		And
		AccessAreaEnabled = 1
		And
		DivisionAccessAreaEnabled = 1
	Group By
		 CorporateDivisionAccessAreaID
		,CountryID
		,CountryName
		,CityID
		,CityName
		,BuildingID
		,BuildingName
		,FloorID
		,FloorName
		,AccessAreaID
		,AccessAreaName
		,AccessAreaEnabled
		,AccessAreaTypeID
		,AccessAreaTypeName
		,AccessAreaTypeDescription
		,DivisionID
		,DivisionName
		,NumberOfApprovals
		,ApprovalTypeID
		,ApprovalType
		,[Description]
		,ControlSystemID
		,ControlSystemName
		,AccessControlID
		,Frequency
		,LastCertifiedDate
		
	END

  IF(@TaskCreatedEndDate IS NOT NULL)
  BEGIN
	SET @TaskCreatedEndDate = DATEADD(day, 1, @TaskCreatedEndDate);
  END
 
 
 IF (@Restricted = 0)
 BEGIN
  
SELECT * FROM (
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' ' + HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator,
  
        AccessRequestPerson.UBR
  , vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName AccessArea,
  CASE
	WHEN AccessRequest.IsSmartCard IS NULL AND Tasks.TaskTypeId = 1 THEN 'Standard' 
	WHEN  AccessRequest.IsSmartCard = 1 AND Tasks.TaskTypeId = 1 THEN 'Smart' 
	END AS CardType         
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive where ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON AccessRequest.CreatedByID=HR2.dbPeopleId  
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
  
WHERE
  AccessRequest.RequestTypeID <> 15
  AND
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))


AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@TaskStatusId IS NULL) OR (@TaskStatusId IS NOT NULL AND Tasks.TaskStatusID = @TaskStatusId))


)query1


--ORDER BY CreatedDate DESC
ORDER BY  RequestID ,  TaskId DESC
END

ELSE
BEGIN


SELECT * FROM (
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' ' + HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator,
  
        AccessRequestPerson.UBR
  , vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName AccessArea     ,
  (SELECT Top 1 CountryId from LocationCountry WHERE [Name] = HRPerson.CountryName) AS CountryId,
  AccessRequest.AccessAreaID,
  CASE
	WHEN AccessRequest.IsSmartCard IS NULL AND Tasks.TaskTypeId = 1 THEN 'Standard' 
	WHEN  AccessRequest.IsSmartCard = 1 AND Tasks.TaskTypeId = 1 THEN 'Smart' 
	END AS CardType
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive where ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON AccessRequest.CreatedByID=HR2.dbPeopleId  
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
  
WHERE
  AccessRequest.RequestTypeID <> 15  
  AND
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))


AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@TaskStatusId IS NULL) OR (@TaskStatusId IS NOT NULL AND Tasks.TaskStatusID = @TaskStatusId))


)query1 INNER JOIN (SELECT UBR_Code FROM @UBRTable) joinner ON query1.UBR = joinner.UBR_Code
INNER JOIN (SELECT AccessAreaID from @AccessAreaIdsTable) accessJoiner ON query1.AccessAreaID = accessJoiner.AccessAreaId
ORDER BY  RequestID ,  TaskId DESC


END

  
END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DAAccessReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_DAAccessReportSearch]
END
GO

/****** Object:  StoredProcedure [dbo].[sp_DAAccessReportSearch]    Script Date: 3/9/2017 4:19:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================================================
-- History:
--			09/03/2017 (Adheeb) - Added card type in to the report
--===================================================================

CREATE PROCEDURE [dbo].[sp_DAAccessReportSearch]
(
	@LanguageID INT=1,
	@TaskCreatedStartDate datetime = NULL,
	@TaskCreatedEndDate datetime = NULL,
    @PersonNameSearchType nvarchar(50) = 'Contains',
	@PersonNameValueStart nvarchar(150) = '',
	@PersonNameValueEnd nvarchar(150) = '',
	@PersonEmpIdSearchType nvarchar(50) = 'Contains',
	@PersonEmpIdValueStart nvarchar(150) = '',
	@PersonEmpIdValueEnd nvarchar(150) = '',
	@PersonEmailSearchType nvarchar(50) = 'Contains',
	@PersonEmailValueStart nvarchar(150) = '',
	@PersonEmailalueEnd nvarchar(150) = '',
	@PersonDbDirIdSearchType nvarchar(50) = 'Contains',
	@PersonDbDirIdValueStart nvarchar(150) = '',
	@PersonDbDirIdValueEnd nvarchar(150) = '',
	@RequestorNameSearchType nvarchar(50) = 'Contains'	,
	@RequestorNameValueStart nvarchar(150) = '',
	@RequestorNameValueEnd nvarchar(150) = '',	
	@RequestorEmpIdSearchType nvarchar(50) = 'Contains',
	@RequestorEmpIdValueStart nvarchar(150) = '',
	@RequestorEmpIdValueEnd nvarchar(150) = '',
	@RequestorEmailSearchType nvarchar(50) = 'Contains',
	@RequestorEmailValueStart nvarchar(150) = '',
	@RequestorEmailalueEnd nvarchar(150) = '',
	@RequestorDbDirIdSearchType nvarchar(50) = 'Contains',
	@RequestorDbDirIdValueStart nvarchar(150) = '',
	@RequestorDbDirIdValueEnd nvarchar(150) = '',
	@TaskStatusId int = NULL,
	@CountryID int = NULL , 
	@CityID int = NULL ,
	@BuildingID int = NULL ,
	@FloorID int = NULL,
	@Restricted bit = 1,
	@SearchingUserId int,
	@RequestStatusFullId int = NULL,
	@AccessAreaId int = NULL,
	@RequestTypeID int = NULL
)

AS
BEGIN
SET NOCOUNT ON

	DECLARE @UBRTable TABLE (UBR_Code NVARCHAR(6), UBR_Name nvarchar(50), Parent NVARCHAR(6))
	DECLARE @AccessAreaIdsTable TABLE (AccessAreaId INT NULL)
 
	IF (@Restricted = 1)
	BEGIN 
	
		DECLARE @CurrentUBR nchar(6)
		DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR

		SELECT DISTINCT UBR
		  FROM [CorporateDivision]
		  INNER JOIN dbo.CorporateDivisionRoleUser ON [CorporateDivision].DivisionID = CorporateDivisionRoleUser.DivisionID
		  WHERE CorporateDivisionRoleUser.dbPeopleID = @SearchingUserId		  
		  
		 OPEN myCursor
		 
		 FETCH NEXT FROM myCursor INTO @CurrentUBR
		 
		 WHILE @@FETCH_STATUS = 0
		 BEGIN
			INSERT INTO @UBRTable exec [dbo].[up_GetUBRNodes] @CurrentUBR		 
			FETCH NEXT FROM myCursor INTO @CurrentUBR
		 END
		 
		 CLOSE myCursor
		 DEALLOCATE myCursor	
		 
		 INSERT INTO @AccessAreaIdsTable VALUES (NULL)
		 
		 INSERT INTO @AccessAreaIdsTable 
		 Select Distinct AccessAreaID
	From
	(
		Select
			 vw_DivisionAccessArea.CorporateDivisionAccessAreaID
			,vw_DivisionAccessArea.CountryID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.CountryName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.CountryName) From LocationCountry where  CountryID = vw_DivisionAccessArea.CountryID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.CountryName) From LocationCountry where  CountryID = vw_DivisionAccessArea.CountryID)
			end as CountryName
			,vw_DivisionAccessArea.CityID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.CityName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.CityName) From LocationCity where  CityID = vw_DivisionAccessArea.CityID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.CityName) From LocationCity where  CityID = vw_DivisionAccessArea.CityID) 
			end as CityName
			,vw_DivisionAccessArea.BuildingID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.BuildingName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.BuildingName) From LocationBuilding where  BuildingID = vw_DivisionAccessArea.BuildingID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.BuildingName) From LocationBuilding where  BuildingID = vw_DivisionAccessArea.BuildingID)
			end as BuildingName
			,vw_DivisionAccessArea.FloorID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.FloorName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.FloorName) From LocationFloor where  FloorID = vw_DivisionAccessArea.FloorID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.FloorName) From LocationFloor where  FloorID = vw_DivisionAccessArea.FloorID)
			end as FloorName
			,vw_DivisionAccessArea.AccessAreaID			
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.AccessAreaName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.AccessAreaName) From AccessArea where  AccessAreaID = vw_DivisionAccessArea.AccessAreaID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.AccessAreaName) From AccessArea where  AccessAreaID = vw_DivisionAccessArea.AccessAreaID)
			end as AccessAreaName			
			,vw_DivisionAccessArea.AccessAreaEnabled
			,vw_DivisionAccessArea.AccessAreaTypeID
			,vw_DivisionAccessArea.AccessAreaTypeName
			,vw_DivisionAccessArea.AccessAreaTypeDescription
			,vw_DivisionAccessArea.DivisionID
			,Case @LanguageID
					when 1 then vw_DivisionAccessArea.DivisionName
					when 2 then (Select ISNULL(GermanName,vw_DivisionAccessArea.DivisionName) From CorporateDivision where  DivisionID = vw_DivisionAccessArea.DivisionID)
					when 3 then (Select ISNULL(ItalyName,vw_DivisionAccessArea.DivisionName) From CorporateDivision where  DivisionID = vw_DivisionAccessArea.DivisionID)
			end as DivisionName
			,vw_DivisionAccessArea.DivisionEnabled
			,vw_DivisionAccessArea.NumberOfApprovals
			,vw_DivisionAccessArea.ApprovalTypeID
			,vw_DivisionAccessArea.ApprovalType
			,vw_DivisionAccessArea.[Description]
			,vw_DivisionAccessArea.ControlSystemID
			,vw_DivisionAccessArea.ControlSystemName
			,vw_DivisionAccessArea.AccessControlID
			,vw_DivisionAccessArea.Frequency
			,vw_DivisionAccessArea.LastCertifiedDate
			,vw_DivisionAccessArea.DivisionAccessAreaEnabled
			
		From
			vw_DivisionAccessArea
		Left Outer Join
			CorporateDivisionAccessAreaApprover
			On
			CorporateDivisionAccessAreaApprover.CorporateDivisionAccessAreaID = vw_DivisionAccessArea.CorporateDivisionAccessAreaID
		Left Outer Join
			mp_SecurityGroup
			On
			CorporateDivisionAccessAreaApprover.mp_SecurityGroupID = mp_SecurityGroup.Id
		Left Outer Join
			mp_User u1
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u1.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 1 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u2
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u2.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 2 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u3
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u3.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 3 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u4
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u4.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 4 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Left Outer Join
			mp_User u5
			On
			CorporateDivisionAccessAreaApprover.dbPeopleID = u5.dbPeopleID And CorporateDivisionAccessAreaApprover.SortOrder = 5 And CorporateDivisionAccessAreaApprover.Enabled = 1
		Where
		(
			(
				(
					vw_DivisionAccessArea.DivisionID
					In
					(
						Select Distinct
							 CorporateDivision.DivisionID
						From
							CorporateDivision
						Inner Join
							LocationCountry
							On
							LocationCountry.CountryID = CorporateDivision.CountryID
						Where
							CorporateDivision.[Enabled] = 1
					)
					And Exists
					(
						Select
							Id
						From
							mp_SecurityGroupUser
						Where
							dbPeopleID = @SearchingUserId
							And
							SecurityGroupId = 2		-- System Administrator (ADMIN)
					)
				)
				Or
				vw_DivisionAccessArea.DivisionID
				In
				(
					Select Distinct
						 CorporateDivision.DivisionID
					From
						CorporateDivision
					Inner Join
						LocationCountry
						On
						LocationCountry.CountryID = CorporateDivision.CountryID
					Inner Join
						LocationCountryUser
						On
						LocationCountryUser.CountryID = LocationCountry.CountryID
					Inner Join
						CorporateDivisionRoleUser
						On
						CorporateDivisionRoleUser.DivisionID = CorporateDivision.DivisionID
					Where
						CorporateDivision.[Enabled] = 1
						And
						LocationCountryUser.dbPeopleID = @SearchingUserId
						And
						CorporateDivisionRoleUser.dbPeopleID = @SearchingUserId
						And
						CorporateDivisionRoleUser.mp_SecurityGroupID In (3, 7)		-- Division Administrator (DA) / Division Owner (DO)
				)
			)
		)
		Group By
			vw_DivisionAccessArea.CorporateDivisionAccessAreaID
			,vw_DivisionAccessArea.CountryID
			,vw_DivisionAccessArea.CountryName
			,vw_DivisionAccessArea.CityID
			,vw_DivisionAccessArea.CityName
			,vw_DivisionAccessArea.BuildingID
			,vw_DivisionAccessArea.BuildingName
			,vw_DivisionAccessArea.FloorID
			,vw_DivisionAccessArea.FloorName
			,vw_DivisionAccessArea.AccessAreaID
			,vw_DivisionAccessArea.AccessAreaName
			,vw_DivisionAccessArea.AccessAreaEnabled
			,vw_DivisionAccessArea.AccessAreaTypeID
			,vw_DivisionAccessArea.AccessAreaTypeName
			,vw_DivisionAccessArea.AccessAreaTypeDescription
			,vw_DivisionAccessArea.DivisionID
			,vw_DivisionAccessArea.DivisionName
			,vw_DivisionAccessArea.DivisionEnabled
			,vw_DivisionAccessArea.NumberOfApprovals
			,vw_DivisionAccessArea.ApprovalTypeID
			,vw_DivisionAccessArea.ApprovalType
			,vw_DivisionAccessArea.[Description]
			,vw_DivisionAccessArea.ControlSystemID
			,vw_DivisionAccessArea.ControlSystemName
			,vw_DivisionAccessArea.AccessControlID
			,vw_DivisionAccessArea.Frequency
			,vw_DivisionAccessArea.LastCertifiedDate
			,vw_DivisionAccessArea.DivisionAccessAreaEnabled
		
		
		
	) As
		temptable
	Where
		DivisionEnabled = 1
		And
		AccessAreaEnabled = 1
		And
		DivisionAccessAreaEnabled = 1
	Group By
		 CorporateDivisionAccessAreaID
		,CountryID
		,CountryName
		,CityID
		,CityName
		,BuildingID
		,BuildingName
		,FloorID
		,FloorName
		,AccessAreaID
		,AccessAreaName
		,AccessAreaEnabled
		,AccessAreaTypeID
		,AccessAreaTypeName
		,AccessAreaTypeDescription
		,DivisionID
		,DivisionName
		,NumberOfApprovals
		,ApprovalTypeID
		,ApprovalType
		,[Description]
		,ControlSystemID
		,ControlSystemName
		,AccessControlID
		,Frequency
		,LastCertifiedDate
		
	END

  IF(@TaskCreatedEndDate IS NOT NULL)
  BEGIN
	SET @TaskCreatedEndDate = DATEADD(day, 1, @TaskCreatedEndDate);
  END
 
 
 IF (@Restricted = 0)
 BEGIN
  
SELECT * FROM (
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' ' + HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator,
  
        AccessRequestPerson.UBR
  , vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName AccessArea           
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive where ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON AccessRequest.CreatedByID=HR2.dbPeopleId  
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
  
WHERE
  AccessRequest.RequestTypeID <> 15
  AND
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))


AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@TaskStatusId IS NULL) OR (@TaskStatusId IS NOT NULL AND Tasks.TaskStatusID = @TaskStatusId))


)query1


--ORDER BY CreatedDate DESC
ORDER BY  RequestID ,  TaskId DESC
END

ELSE
BEGIN


SELECT * FROM (
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' ' + HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator,
  
        AccessRequestPerson.UBR
  , vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName AccessArea     ,
  (SELECT Top 1 CountryId from LocationCountry WHERE [Name] = HRPerson.CountryName) AS CountryId,
  AccessRequest.AccessAreaID
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive where ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON AccessRequest.CreatedByID=HR2.dbPeopleId  
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
  
WHERE
  AccessRequest.RequestTypeID <> 15
  AND
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))


AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@TaskStatusId IS NULL) OR (@TaskStatusId IS NOT NULL AND Tasks.TaskStatusID = @TaskStatusId))


)query1 INNER JOIN (SELECT UBR_Code FROM @UBRTable) joinner ON query1.UBR = joinner.UBR_Code
INNER JOIN (SELECT AccessAreaID from @AccessAreaIdsTable) accessJoiner ON query1.AccessAreaID = accessJoiner.AccessAreaId
ORDER BY  RequestID ,  TaskId DESC

END
  
END

GO
