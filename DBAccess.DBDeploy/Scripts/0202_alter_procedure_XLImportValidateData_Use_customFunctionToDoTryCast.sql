IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateData]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateData]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportValidateData]    Script Date: 1/12/2016 6:41:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--========================================================================
-- Author: Wijitha Wijenayake
-- Created Date: 17/12/2015
-- Description: Validate staged data copied from excel, against the destination table scheema. 
-- History: 
--				Wijitha (12/01/2016) - Replaced Try_Cast method with a UDF due to SQL Server 2008 does not support Try_Cast	
--========================================================================
CREATE PROCEDURE [dbo].[XLImportValidateData]
@fileID INT

AS
BEGIN

DECLARE @entityID INT
DECLARE @stageTable NVARCHAR(500)
DECLARE @stageTableColumn NVARCHAR(500)
DECLARE @sheetColumn NVARCHAR(500)
DECLARE @isNullable NVARCHAR(10)
DECLARE @dataType NVARCHAR(50)
DECLARE @maxLength INT
DECLARE @rowNumbers NVARCHAR(max) = ''
DECLARE @rowIDs NVARCHAR(max) = ''
DECLARE @sql NVARCHAR(max) = ''
DECLARE @updateSql nvarchar(max) = ''

DECLARE @tblInValidRecords TABLE
(
	ID INT,
	RowNumber INT,
	ErrorType INT
)

DECLARE cur_columnMapplings CURSOR FOR 

SELECT entity.ID, entity.StagingTable, columnMap.StagingTableColumn, sheetColMap.SheetColumn, col.IS_NULLABLE, col.DATA_TYPE, col.CHARACTER_MAXIMUM_LENGTH
FROM XLImportStagingColumnMapping columnMap
	INNER JOIN XLImportSheetColumnMapping sheetColMap on columnMap.EntityID = sheetColMap.EntityID AND columnMap.StagingTableColumn = sheetColMap.StagingTableColumn
	INNER JOIN XLImportEntity entity on entity.ID = columnMap.EntityID
	INNER JOIN INFORMATION_SCHEMA.COLUMNS col ON col.TABLE_NAME = entity.DestinationTable AND col.COLUMN_NAME = columnMap.DestinationTableColumn

OPEN cur_columnMapplings

FETCH NEXT FROM cur_columnMapplings INTO @entityID, @stageTable, @stageTableColumn, @sheetColumn, @isNullable, @DataType, @maxLength

WHILE (@@FETCH_STATUS = 0)
BEGIN

	DELETE FROM @tblInValidRecords	

	--Check for null
	IF(@isNullable = 'NO')
	BEGIN	
		SET @sql = 'SELECT ID, RowNumber, 1 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND (' + @stageTableColumn + ' IS NULL OR len(' + @stageTableColumn + ') = 0)'

		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql
	end

	IF(@DataType IN ('NNVARCHAR','NVARCHAR','TEXT','NTEXT', 'VARCHAR', 'CHAR', 'NCHAR')) --If a Text field, then Check for the max length
	BEGIN

		SET @sql = 'SELECT ID, RowNumber, 2 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND Len(' + @stageTableColumn + ') > ' + cast(@maxLength as NVARCHAR(10))
	
		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql

	END
	ELSE --If a Non Text field Check the data type
	BEGIN
		SET @sql = 'SELECT ID, RowNumber, 3 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND LEN(' + @stageTableColumn + ') > 0 AND dbo.CheckCast(''' + @DataType  + ''', ' + @stageTableColumn + ') = 0'

		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql

	END

	DECLARE @errorType INT = 1
	WHILE (@errorType <=3)
	BEGIN

		SET @rowNumbers = ''
		SET @rowIDs  = ''

		SELECT @rowNumbers = @rowNumbers + cast(RowNumber as NVARCHAR(10)) + ', ', @rowIDs = @rowIDs + cast(ID as NVARCHAR(10)) + ', '  FROM @tblInValidRecords where ErrorType = @errorType
		SET @rowNumbers = LEFT(@rowNumbers, NULLIF(LEN(@rowNumbers)-1, -1))
		SET @rowIDs = LEFT(@rowIDs, NULLIF(LEN(@rowIDs)-1, -1))

		IF(len(@rowIDs) > 0)
		BEGIN
			INSERT INTO XLImportValidationError
			SELECT @fileID, @entityID, 'Error', 
				CASE WHEN @errorType = 1 THEN  @sheetColumn + ' is mandatory field'
					WHEN @errorType = 2 THEN 'Length of ' + @sheetColumn + ' cannot exceed ' + cast(@maxLength as NVARCHAR(10)) + ' charactors'
					WHEN @errorType = 3 THEN 'Invalid data type. ' + @sheetColumn + ' should be a ' + @DataType END,		
				@rowNumbers

			SET @updateSql = 'UPDATE ' + @stageTable + ' SET ValidationFailed = 1 WHERE ID IN (' + @rowIDs + ')'
			EXEC sp_executesql @updateSql
		END

		SET @errorType = @errorType + 1
	END

	FETCH NEXT FROM cur_columnMapplings INTO @entityID, @stageTable, @stageTableColumn, @sheetColumn, @isNullable, @DataType, @maxLength

END

CLOSE cur_columnMapplings
DEALLOCATE cur_columnMapplings

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportValidateData]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportValidateData]
END
GO

/****** Object:  StoredProcedure [dbo].[XLImportValidateData]    Script Date: 1/6/2016 5:50:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--========================================================================
-- Author: Wijitha Wijenayake
-- Created Date: 17/12/2015
-- Description: Validate staged data copied from excel, against the destination table scheema. 
--========================================================================
CREATE PROCEDURE [dbo].[XLImportValidateData]
@fileID INT

AS
BEGIN

DECLARE @entityID INT
DECLARE @stageTable NVARCHAR(500)
DECLARE @stageTableColumn NVARCHAR(500)
DECLARE @sheetColumn NVARCHAR(500)
DECLARE @isNullable NVARCHAR(10)
DECLARE @dataType NVARCHAR(50)
DECLARE @maxLength INT
DECLARE @rowNumbers NVARCHAR(max) = ''
DECLARE @rowIDs NVARCHAR(max) = ''
DECLARE @sql NVARCHAR(max) = ''
DECLARE @updateSql nvarchar(max) = ''

DECLARE @tblInValidRecords TABLE
(
	ID INT,
	RowNumber INT,
	ErrorType INT
)

DECLARE cur_columnMapplings CURSOR FOR 

SELECT entity.ID, entity.StagingTable, columnMap.StagingTableColumn, sheetColMap.SheetColumn, col.IS_NULLABLE, col.DATA_TYPE, col.CHARACTER_MAXIMUM_LENGTH
FROM XLImportStagingColumnMapping columnMap
	INNER JOIN XLImportSheetColumnMapping sheetColMap on columnMap.EntityID = sheetColMap.EntityID AND columnMap.StagingTableColumn = sheetColMap.StagingTableColumn
	INNER JOIN XLImportEntity entity on entity.ID = columnMap.EntityID
	INNER JOIN INFORMATION_SCHEMA.COLUMNS col ON col.TABLE_NAME = entity.DestinationTable AND col.COLUMN_NAME = columnMap.DestinationTableColumn

OPEN cur_columnMapplings

FETCH NEXT FROM cur_columnMapplings INTO @entityID, @stageTable, @stageTableColumn, @sheetColumn, @isNullable, @DataType, @maxLength

WHILE (@@FETCH_STATUS = 0)
BEGIN

	DELETE FROM @tblInValidRecords	

	--Check for null
	IF(@isNullable = 'NO')
	BEGIN	
		SET @sql = 'SELECT ID, RowNumber, 1 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND (' + @stageTableColumn + ' IS NULL OR len(' + @stageTableColumn + ') = 0)'

		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql
	end

	IF(@DataType IN ('NNVARCHAR','NVARCHAR','TEXT','NTEXT', 'VARCHAR', 'CHAR', 'NCHAR')) --If a Text field, then Check for the max length
	BEGIN

		SET @sql = 'SELECT ID, RowNumber, 2 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND Len(' + @stageTableColumn + ') > ' + cast(@maxLength as NVARCHAR(10))
	
		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql

	END
	ELSE --If a Non Text field Check the data type
	BEGIN
		SET @sql = 'SELECT ID, RowNumber, 3 FROM ' + @stageTable + ' WHERE FileID = ' + CAST(@fileID as Varchar(20)) + ' AND LEN(' + @stageTableColumn + ') > 0 AND TRY_CAST(' + @stageTableColumn + ' as ' + @DataType + ') IS NULL'

		INSERT INTO @tblInValidRecords
		EXEC sp_executesql @sql

	END

	DECLARE @errorType INT = 1
	WHILE (@errorType <=3)
	BEGIN

		SET @rowNumbers = ''
		SET @rowIDs  = ''

		SELECT @rowNumbers = @rowNumbers + cast(RowNumber as NVARCHAR(10)) + ', ', @rowIDs = @rowIDs + cast(ID as NVARCHAR(10)) + ', '  FROM @tblInValidRecords where ErrorType = @errorType
		SET @rowNumbers = LEFT(@rowNumbers, NULLIF(LEN(@rowNumbers)-1, -1))
		SET @rowIDs = LEFT(@rowIDs, NULLIF(LEN(@rowIDs)-1, -1))

		IF(len(@rowIDs) > 0)
		BEGIN
			INSERT INTO XLImportValidationError
			SELECT @fileID, @entityID, 'Error', 
				CASE WHEN @errorType = 1 THEN  @sheetColumn + ' is mandatory field'
					WHEN @errorType = 2 THEN 'Length of ' + @sheetColumn + ' cannot exceed ' + cast(@maxLength as NVARCHAR(10)) + ' charactors'
					WHEN @errorType = 3 THEN 'Invalid data type. ' + @sheetColumn + ' should be a ' + @DataType END,		
				@rowNumbers

			SET @updateSql = 'UPDATE ' + @stageTable + ' SET ValidationFailed = 1 WHERE ID IN (' + @rowIDs + ')'
			EXEC sp_executesql @updateSql
		END

		SET @errorType = @errorType + 1
	END

	FETCH NEXT FROM cur_columnMapplings INTO @entityID, @stageTable, @stageTableColumn, @sheetColumn, @isNullable, @DataType, @maxLength

END

CLOSE cur_columnMapplings
DEALLOCATE cur_columnMapplings

END

GO