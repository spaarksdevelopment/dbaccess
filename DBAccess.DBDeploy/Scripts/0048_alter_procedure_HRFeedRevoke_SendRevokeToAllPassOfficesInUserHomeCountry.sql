﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
    
DROP PROCEDURE [dbo].[HRFeedRevoke]

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--
--======================================================================
 CREATE PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
 SELECT TransactionId, HRID, TerminationDate FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, ''C'', '''') as int)=hp.dbpeopleid
 WHERE RevokeProcessed = 0 AND TriggerType = ''LVER'' AND TriggerAction <> ''DEL''
 AND RevokeAccessDateTimestamp <= GETDATE() AND (hp.Revoked IS NULL OR hp.Revoked = 0)

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int
 
	SELECT @ReplacedHRID = REPLACE(@HRID, ''C'', '''')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
 	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)
	DECLARE @OfficerID varchar(2)

	--Don''t create revoke if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		exec HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL
		
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person''s country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleIdConvert
		
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleIdConvert
		
		if (@OfficerID=''D'' or @OfficerID=''MD'')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, 
					hr2.EmailAddress AS PassOfficeUserEmail, 
					hr2.Forename + '' '' + hr2.Surname AS PassOfficeUserName
				FROM LocationPassOffice 
					INNER JOIN	LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
					JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
					AND LocationPassOfficeUser.IsEnabled = 1
					AND LocationPassOffice.EmailEnabled = 1
		end
		else
		begin
		INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, 
				HRPerson.EmailAddress AS PassOfficeUserEmail, 
				HRPerson.Forename + '' '' + HRPerson.Surname AS PassOfficeUserName
			 FROM LocationPassOffice
				JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE LocationPassOffice.Enabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1 
				AND LocationPassOffice.EmailEnabled = 1
				AND LocationPassOffice.PassOfficeID in 
				(
				    SELECT distinct PassOfficeID from LocationPassOffice where CountryID = @UserCountryID --Get all pass offices in the user''s home country
				    UNION
				    SELECT IsNUll(bpo.PassOfficeID, c.PassOfficeID) as PassOfficeID --Get pass offices of buildings where user has access to outside of their home country
				    FROM HRPersonAccessArea p
					   JOIN AccessArea aa on aa.AccessAreaID = p.AccessAreaID
					   JOIN LocationBuilding lb on lb.BuildingID = aa.BuildingID
					   JOIN LocationCity lc on lc.CityID = lb.CityID
					   JOIN LocationCountry c on c.CountryID = lc.CountryID
					   LEFT OUTER JOIN LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID AND bpo.[Enabled] = 1
				    WHERE (p.EndDate IS NULL OR p.EndDate > getdate())
					   AND p.dbPeopleId = @dbPeopleIdConvert AND c.CountryID <> @UserCountryID
			    )
		END

        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @TransactionId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]=''HttpRoot''

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 37


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '''' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , ''[$$UserEmail$$]'' , @EmailForename + '' '' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , ''[$$HTTP_ROOT$$]'' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , ''[$$NAME$$]'' , @ToName ); 
					SET @Body = REPLACE ( @Body , ''[$$FILE_PATH$$]'' , ''Pages/MyTasks.aspx'' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, ''dbAccess_DO_NOT_REPLY@db.com'' 
							, ''ryan.sheehan@db.com''
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 37 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END
	 
	UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
 
 



' 
END
GO


--//@UNDO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[HRFeedRevoke]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--
--======================================================================
 CREATE PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
 SELECT TransactionId, HRID, TerminationDate FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, ''C'', '''') as int)=hp.dbpeopleid
 WHERE RevokeProcessed = 0 AND TriggerType = ''LVER'' AND TriggerAction <> ''DEL''
 AND RevokeAccessDateTimestamp <= GETDATE() AND (hp.Revoked IS NULL OR hp.Revoked = 0)

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int
 
	SELECT @ReplacedHRID = REPLACE(@HRID, ''C'', '''')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
 	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)
	DECLARE @OfficerID varchar(2)

	--Don''t create revoke if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		exec HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL
		
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person''s country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleIdConvert
		
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleIdConvert
		
		if (@OfficerID=''D'' or @OfficerID=''MD'')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, 
					hr2.EmailAddress AS PassOfficeUserEmail, 
					hr2.Forename + '' '' + hr2.Surname AS PassOfficeUserName
				FROM LocationPassOffice 
					INNER JOIN	LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
					JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
					AND LocationPassOfficeUser.IsEnabled = 1
					AND LocationPassOffice.EmailEnabled = 1
		end
		else
		begin
		INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, 
				HRPerson.EmailAddress AS PassOfficeUserEmail, 
				HRPerson.Forename + '' '' + HRPerson.Surname AS PassOfficeUserName
			 FROM LocationPassOffice
				JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE LocationPassOffice.Enabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1 
				AND LocationPassOffice.EmailEnabled = 1
				AND LocationPassOffice.PassOfficeID in (select distinct PassOfficeID from LocationCountry where CountryID = @UserCountryID --Get the default PassOffice in the user''s country.
														union 
														select  IsNUll(bpo.PassOfficeID, c.PassOfficeID) as PassOfficeID --Get pass offices of buildings where user has access to.
														from HRPersonAccessArea p
															inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
															inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
															inner join LocationCity lc on lc.CityID = lb.CityID
															inner join LocationCountry c on c.CountryID = lc.CountryID
															left outer join LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID and bpo.[Enabled] = 1
														where (p.EndDate is null or p.EndDate > getdate())
															and p.dbPeopleId = @dbPeopleIdConvert)
		end

        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @TransactionId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]=''HttpRoot''

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 37


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '''' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , ''[$$UserEmail$$]'' , @EmailForename + '' '' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , ''[$$HTTP_ROOT$$]'' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , ''[$$NAME$$]'' , @ToName ); 
					SET @Body = REPLACE ( @Body , ''[$$FILE_PATH$$]'' , ''Pages/MyTasks.aspx'' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, ''dbAccess_DO_NOT_REPLY@db.com'' 
							, ''ryan.sheehan@db.com''
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 37 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END
	 
	UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
 
 



' 
END
GO
