if not exists(select * from sys.columns 
            where Name = N'EmailEnabled' and Object_ID = Object_ID(N'LocationPassOffice'))
begin
	ALTER TABLE dbo.LocationPassOffice ADD
	EmailEnabled bit NOT NULL CONSTRAINT DF_LocationPassOffice_EmailEnabled DEFAULT ((0))
end
GO

--//@UNDO
if exists(select * from sys.columns 
            where Name = N'EmailEnabled' and Object_ID = Object_ID(N'LocationPassOffice'))
begin
	ALTER TABLE dbo.LocationPassOffice
	DROP CONSTRAINT DF_LocationPassOffice_EmailEnabled

	ALTER TABLE dbo.LocationPassOffice
	DROP COLUMN EmailEnabled

end
GO