﻿IF EXISTS(SELECT * FROM EmailTemplates WHERE [Type]=39)
	DELETE FROM EmailTemplates WHERE [Type]=39
GO

INSERT INTO EmailTemplates([Type],[Subject],[Body])
VALUES
(
	39,
	'DBAccess - New Suspension Request',
	'Dear [$$NAME$$],<br/><br/>A suspension request has been created for user  [$$UserEmail$$].<br/>Please use the link below to access the system in order to process this request.<br/>Kind Regards<br />DBAccess System Administrator<br />[$$HTTP_ROOT$$][$$FILE_PATH$$]'
)

GO

--//@UNDO

IF EXISTS(SELECT * FROM EmailTemplates WHERE [Type]=39)
	DELETE FROM EmailTemplates WHERE [Type]=39
GO