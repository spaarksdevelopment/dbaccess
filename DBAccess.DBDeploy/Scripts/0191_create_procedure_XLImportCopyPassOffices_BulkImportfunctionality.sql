IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyPassOffices]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyPassOffices]
END
GO


/****** Object:  StoredProcedure [dbo].[XLImportCopyPassOffices]    Script Date: 1/8/2016 9:42:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 18/12/2015
-- Description: Copy pass offices from staging table to the LocationPassOffice table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyPassOffices]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit = 1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO LocationPassOffice (Name, Code, Enabled, Email, Telephone, CountryID, CityID, EmailEnabled)
	OUTPUT inserted.PassOfficeID into @tblIDs 
	SELECT Name,'', CAST(Enabled AS bit), Email, Telephone, 
		(SELECT CountryID FROM LocationCountry WHERE Name = Country),
		(SELECT CityID 
		FROM LocationCity city 
			INNER JOIN LocationCountry country on city.CountryID = country.CountryID 
		WHERE country.Name = Country and city.Name = City),
		CAST(EmailEnabled AS BIT)
	FROM XLImportStagePassOffice
	WHERE ISNULL(ValidationFailed, 0) = 0 AND FileID = @FileID

	--Insert newly imported Records IDs in to the History table for rollback and audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'LocationPassOffice', ID FROM @tblIDs

END


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyPassOffices]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyPassOffices]
END
GO