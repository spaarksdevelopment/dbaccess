IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagingColumnMapping')
BEGIN

CREATE TABLE [dbo].[XLImportStagingColumnMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[StagingTableColumn] [nvarchar](500) NOT NULL,
	[DestinationTableColumn] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_XLImportEntityColumnMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[XLImportStagingColumnMapping]  WITH CHECK ADD  CONSTRAINT [FK_XLImportEntityColumnMapping_XLImportEntity] FOREIGN KEY([EntityID])
REFERENCES [dbo].[XLImportEntity] ([ID])

ALTER TABLE [dbo].[XLImportStagingColumnMapping] CHECK CONSTRAINT [FK_XLImportEntityColumnMapping_XLImportEntity]


END

GO


--//@UNDO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportStagingColumnMapping')
BEGIN
	
	DROP TABLE [dbo].[XLImportStagingColumnMapping]
	
END

GO