IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyDivisions]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyDivisions]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===============================================================
-- Author: Wijitha Wijenayake
-- Created Date: 21/12/2015
-- Description: Copy Corporate Divisions from staging table to the CorporateDivision table
--===============================================================

CREATE PROCEDURE [dbo].[XLImportCopyDivisions]
@FileID int
AS
BEGIN

	DECLARE @ValidFile bit = 0, @ProcessedFile bit=1
	SELECT @ValidFile = ValidationPassed, @ProcessedFile = Processed 
	FROM XLImportFile WHERE ID= @FileID

	--Do not copy data if file validation has faild or already processed file
	IF(@ValidFile = 0 OR @ProcessedFile = 1)
		RETURN

	DECLARE @tblIDs table (ID int)

	INSERT INTO CorporateDivision (CountryID, Name, UBR, Enabled, RecertPeriod, IsSubdivision)
	OUTPUT inserted.DivisionID INTO @tblIDs
	SELECT (SELECT CountryID FROM LocationCountry WHERE Name = Country),
		Name,
		UBR,
		1,
		CAST(RecertPeriod AS INT),
		CAST(IsSubdivision AS BIT)
	FROM XLImportStageDivision
	WHERE FileID = @FileID AND ISNULL(ValidationFailed, 0) = 0 

	--Update parent division of sub divisions. 
	UPDATE cd SET cd.SubdivisionParentDivisionID = pcd.DivisionID  
	FROM XLImportStageDivision sd 
		INNER JOIN LocationCountry lc ON sd.Country = lc.Name
		INNER JOIN CorporateDivision cd ON lc.CountryID = cd.CountryID and sd.Name = cd.Name
		INNER JOIN XLImportStageDivision psd ON sd.ParentDivision = psd.Name
		INNER JOIN LocationCountry plc ON psd.Country = plc.Name
		INNER JOIN CorporateDivision pcd ON plc.CountryID = pcd.CountryID and psd.Name = pcd.Name
	WHERE sd.FileID = @FileID AND ISNULL(sd.ValidationFailed, 0) = 0 AND cd.IsSubdivision = 1


	--Add a record into the history table for audit purposes
	INSERT INTO XLImportHistory (FileID, RelatedTable, RelatedID)
	SELECT @FileID, 'CorporateDivision', ID FROM @tblIDs

END

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XLImportCopyDivisions]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[XLImportCopyDivisions]
END
GO