IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

 ALTER PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
 SELECT TransactionId, HRID, TerminationDate FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, ''C'', '''') as int)=hp.dbpeopleid
 WHERE RevokeProcessed = 0 AND TriggerType = ''LVER'' AND TriggerAction <> ''DEL''
 AND RevokeAccessDateTimestamp <= GETDATE()

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int
 
	SELECT @ReplacedHRID = REPLACE(@HRID, ''C'', '''')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
 	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)
	DECLARE @OfficerID varchar(2)

	--Don''t create revoke if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		exec HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL
		
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person''s country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleIdConvert
		
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleIdConvert
		
		if (@OfficerID=''D'' or @OfficerID=''MD'')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, hr2.EmailAddress AS PassOfficeUserEmail, hr2.Forename + '' '' + hr2.Surname AS PassOfficeUserName
				FROM 
				LocationPassOffice INNER JOIN
				LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1
		end
		else
		begin
		INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, HRPerson.EmailAddress AS PassOfficeUserEmail, HRPerson.Forename + '' '' + HRPerson.Surname AS PassOfficeUserName
			 FROM  
			 LocationCountry INNER JOIN 
			 LocationPassOffice ON LocationCountry.PassOfficeID = LocationPassOffice . PassOfficeID 
			 JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
			 JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE
			 LocationPassOfficeUser.IsEnabled = 1 AND LocationPassOffice.Enabled = 1 AND
		      (
				LocationCountry.CountryID=@UserCountryID 
				OR LocationCountry.CountryID IN
				(
				    select distinct c.CountryID from HRPersonAccessArea p
				    inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
				    inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
				    inner join LocationCity lc on lc.CityID = lb.CityID
				    inner join LocationCountry c on c.CountryID = lc.CountryID
				    where (p.EndDate is null or p.EndDate > getdate())
					    and c.CountryID <> @UserCountryID
					    and p.dbPeopleId = @dbPeopleIdConvert
				)
			 )		
		end


        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @TransactionId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]=''HttpRoot''

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 37


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '''' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , ''[$$UserEmail$$]'' , @EmailForename + '' '' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , ''[$$HTTP_ROOT$$]'' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , ''[$$NAME$$]'' , @ToName ); 
					SET @Body = REPLACE ( @Body , ''[$$FILE_PATH$$]'' , ''Pages/MyTasks.aspx'' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, ''dbAccess_DO_NOT_REPLY@db.com'' 
							, ''ryan.sheehan@db.com''
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 37 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END
	 
	UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
 
 


' 
END
GO

--//@UNDO

 ALTER PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
 SELECT TransactionId, HRID, TerminationDate FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, 'C', '') as int)=hp.dbpeopleid
 WHERE RevokeProcessed = 0 AND TriggerType = 'LVER' AND TriggerAction <> 'DEL'
 AND RevokeAccessDateTimestamp <= GETDATE()

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int
 
	SELECT @ReplacedHRID = REPLACE(@HRID, 'C', '')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
 	DECLARE @To nvarchar(255)
	DECLARE @ToName nvarchar(255)
	DECLARE @Subject nvarchar(255)
	DECLARE @Body nvarchar(1000)
	DECLARE @Guid nvarchar(255)
	DECLARE @EmailForename nvarchar(255)
	DECLARE @EmailSurname nvarchar(255)
	DECLARE @HttpRoot nvarchar(100)
	DECLARE @OfficerID varchar(2)

	--Don't create revoke if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		exec HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL
		
		--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person's country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleIdConvert
		
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleIdConvert
		
		if (@OfficerID='D' or @OfficerID='MD')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, hr2.EmailAddress AS PassOfficeUserEmail, hr2.Forename + ' ' + hr2.Surname AS PassOfficeUserName
				FROM 
				LocationPassOffice INNER JOIN
				LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1
		end
		else
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, HRPerson.EmailAddress AS PassOfficeUserEmail, HRPerson.Forename + ' ' + HRPerson.Surname AS PassOfficeUserName
				FROM  
				LocationCountry INNER JOIN 
				LocationPassOffice ON LocationCountry.PassOfficeID = LocationPassOffice . PassOfficeID 
				JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
				WHERE LocationCountry.CountryID=@UserCountryID 
				AND LocationPassOfficeUser.IsEnabled = 1
				--AND LocationCountry.CountryID NOT IN (58,59)		
		end


        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @TransactionId

		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]='HttpRoot'

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 37


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , '[$$UserEmail$$]' , @EmailForename + ' ' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , '[$$HTTP_ROOT$$]' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , '[$$NAME$$]' , @ToName ); 
					SET @Body = REPLACE ( @Body , '[$$FILE_PATH$$]' , 'Pages/MyTasks.aspx' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, 'dbAccess_DO_NOT_REPLY@db.com' 
							, 'ryan.sheehan@db.com'
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 37 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
	END
	 
	UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
 
 



GO


