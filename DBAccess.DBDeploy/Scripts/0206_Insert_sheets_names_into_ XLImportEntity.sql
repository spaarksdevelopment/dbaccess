
DELETE XLImportEntity

SET IDENTITY_INSERT [dbo].[XLImportEntity] ON 

INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (1, N'City', N'City', N'XLImportStageCity', N'LocationCity', 1)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (2, N'PassOffice', N'Pass Office', N'XLImportStagePassOffice', N'LocationPassOffice', 2)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (4, N'Building', N'Building', N'XLImportStageBuilding', N'LocationBuilding', 3)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (5, N'Division', N'Division', N'XLImportStageDivision', N'CorporateDivision', 4)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (6, N'DivisionRole', N'Division Roles', N'XLImportStageDivisionRole', N'CorporateDivisionRoleUser', 5)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (8, N'AccessArea', N'Access Area', N'XLImportStageAccessArea', N'AccessArea', 6)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (9, N'CorporateArea', N'Corporate Area', N'XLImportStageCorporateArea', N'CorporateDivisionAccessArea', 7)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (10, N'AccessAreaApprovers', N'Access Area Approvers', N'XLImportStageAccessAreaApprover', N'CorporateDivisionAccessAreaApprover', 8)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (13, N'AccessAreaRecertifiers', N'Access Area Recertifiers', N'XLImportStageAccessAreaRecertifier', N'CorporateDivisionAccessAreaApprover', 9)
INSERT [dbo].[XLImportEntity] ([ID], [Entity], [SheetName], [StagingTable], [DestinationTable], [SortOrder]) VALUES (14, N'PassOfficeUser', N'Pass Office User', N'XLImportStagePassOfficeUser', N'LocationPassOfficeUser', 10)
SET IDENTITY_INSERT [dbo].[XLImportEntity] OFF

GO

--//@UNDO

DELETE XLImportEntity

GO