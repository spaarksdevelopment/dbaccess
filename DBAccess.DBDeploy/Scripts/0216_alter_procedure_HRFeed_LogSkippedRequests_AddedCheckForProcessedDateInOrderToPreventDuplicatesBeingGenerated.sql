IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_LogSkippedRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[HRFeed_LogSkippedRequests]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_LogSkippedRequests]    Script Date: 05/01/2016 11:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- Author: Wijitha Wijenayake
-- Create Date: 01/12/2015
-- Description:  Log skipped revoked and offboard requests due to conflicts in Revoke and Termination Dates

-- History:
--		 06/01/2016 Martin - Added in TaskID in order to be consumed by the report
--		 06/01/2016 Martin - Added check for ProcessedDate and RevokeProcessedDate in order to prevent duplicates in the report
--======================================================================

CREATE PROCEDURE [dbo].[HRFeed_LogSkippedRequests]

AS
BEGIN

	DECLARE @HRFeedMsgID int
	DECLARE @TransactionID int
	DECLARE @HRID int
	DECLARE @RequesstType varchar(10)
	DECLARE @RevokeDate datetime
	DECLARE @TerminationDate datetime
	DECLARE @DBPeopleID int
	DECLARE @TaskID int

	DECLARE cur_revokeSkip CURSOR FOR 
		
	SELECT HRFeedMessageId, TransactionId, HRID, 'Revoke' as RequestType, RevokeAccessDateTimestamp, TerminationDate, TaskID
	FROM HRFeedMessage 
	CROSS APPLY
	(
		SELECT TOP 1 dbpeopleID, TaskID 
		FROM vw_AccessRequest JOIN Tasks ON vw_AccessRequest.RequestID=Tasks.RequestID
		WHERE dbpeopleID=HRFeedMessage.HRID AND vw_AccessRequest.RequestTypeID=16
	) AccessRequests
	WHERE RevokeProcessed = 1
		AND ISNULL(RevokeSkippedLogged, 0) = 0
		AND RevokeAccessDateTimestamp <= GETDATE() 
		AND TerminationDate < RevokeAccessDateTimestamp 
		--Need to check these, otherwise a duplicate message (ie all marked as processed=1) will generate a duplicate entry in the report
		AND [ProcessedDate] IS NOT NULL
		AND [RevokeProcessedDate] IS NOT NULL
	UNION

	SELECT HRFeedMessageId, TransactionId, HRID, 'Offboard' as RequestType, RevokeAccessDateTimestamp, TerminationDate, TaskID
	FROM HRFeedMessage
    CROSS APPLY
	(
		SELECT TOP 1 dbpeopleID, TaskID 
		FROM vw_AccessRequest JOIN Tasks ON vw_AccessRequest.RequestID=Tasks.RequestID
		WHERE dbpeopleID=HRFeedMessage.HRID AND vw_AccessRequest.RequestTypeID=15
	) AccessRequests
	WHERE Processed = 1
		AND ISNULL(OffboardSkippedLogged, 0) = 0
		AND TerminationDate <= GETDATE() 
		AND DATEADD(d, 7, RevokeAccessDateTimestamp) < TerminationDate 	
		--Need to check these, otherwise a duplicate message (ie all marked as processed=1) will generate a duplicate entry in the report
		AND [ProcessedDate] IS NOT NULL
		AND [RevokeProcessedDate] IS NOT NULL	 
	ORDER BY HRFeedMessageId

	OPEN cur_revokeSkip

	FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate, @TaskID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @DBPeopleID = CAST(REPLACE(@HRID, 'C', '') as INT)

		IF(@RequesstType = 'Revoke')
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate, TaskID) 
			VALUES	(@TransactionID, @DBPeopleID, 16, 'Revoke skipped. Already revoked on ' + CONVERT(varchar(15), @TerminationDate, 106), GETDATE(), @TaskID)

			UPDATE HRFeedMessage SET RevokeSkippedLogged = 1 WHERE HRFeedMessageId = @HRFeedMsgID

		END
		ELSE
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate, TaskID) 
			VALUES	(@TransactionID, @DBPeopleID, 15, 'Offboard skipped. Already offboarded on ' + CONVERT(varchar(15), @RevokeDate + 7, 106), GETDATE(), @TaskID)

			UPDATE HRFeedMessage SET OffboardSkippedLogged = 1 WHERE HRFeedMessageId = @HRFeedMsgID

		END
	
		FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate, @TaskID

	END

	CLOSE cur_revokeSkip
	DEALLOCATE cur_revokeSkip

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_LogSkippedRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[HRFeed_LogSkippedRequests]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_LogSkippedRequests]    Script Date: 05/01/2016 11:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- Author: Wijitha Wijenayake
-- Create Date: 01/12/2015
-- Description:  Log skipped revoked and offboard requests due to conflicts in Revoke and Termination Dates
--======================================================================

CREATE PROCEDURE [dbo].[HRFeed_LogSkippedRequests]

AS
BEGIN

	DECLARE @HRFeedMsgID int
	DECLARE @TransactionID int
	DECLARE @HRID int
	DECLARE @RequesstType varchar(10)
	DECLARE @RevokeDate datetime
	DECLARE @TerminationDate datetime
	DECLARE @DBPeopleID int

	DECLARE cur_revokeSkip CURSOR FOR 

	SELECT HRFeedMessageId, TransactionId, HRID, 'Revoke' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE RevokeProcessed = 1
		AND ISNULL(RevokeSkippedLogged, 0) = 0
		AND RevokeAccessDateTimestamp <= GETDATE() 
		AND TerminationDate < RevokeAccessDateTimestamp 
		AND DATEADD(d, 7, TerminationDate) < RevokeAccessDateTimestamp 
	
	UNION

	SELECT HRFeedMessageId, TransactionId, HRID, 'Offboard' as RequestType, RevokeAccessDateTimestamp, TerminationDate
	FROM HRFeedMessage
	WHERE Processed = 1
		AND ISNULL(OffboardSkippedLogged, 0) = 0
		AND TerminationDate <= GETDATE() 
		AND RevokeAccessDateTimestamp < TerminationDate
		AND DATEADD(d, 7, RevokeAccessDateTimestamp) < TerminationDate 		 
	ORDER BY HRFeedMessageId

	OPEN cur_revokeSkip

	FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @DBPeopleID = CAST(REPLACE(@HRID, 'C', '') as INT)

		IF(@RequesstType = 'Revoke')
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 16, 'Revoke skipped. Already revoked on ' + CONVERT(varchar(15), @TerminationDate, 106), GETDATE())

			UPDATE HRFeedMessage SEt RevokeSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
		ELSE
		BEGIN

			INSERT INTO HRFeedSkippedRequestLog (TransactionID, DBPeopleID, RequestTypeID, [Message], CreatedDate) 
			VALUES	(@TransactionID, @DBPeopleID, 15, 'Offboard skipped. Already offboarded on ' + CONVERT(varchar(15), @RevokeDate + 7, 106), GETDATE())

			UPDATE HRFeedMessage SEt OffboardSkippedLogged = 1 where HRFeedMessageId = @HRFeedMsgID

		END
	
		FETCH NEXT FROM cur_revokeSkip INTO @HRFeedMsgID, @TransactionID, @HRID, @RequesstType, @RevokeDate, @TerminationDate

	END

	CLOSE cur_revokeSkip
	DEALLOCATE cur_revokeSkip

END

GO