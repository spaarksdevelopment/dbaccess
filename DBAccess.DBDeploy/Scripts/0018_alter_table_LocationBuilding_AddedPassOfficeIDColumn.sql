
if not exists(select * from sys.columns 
            where Name = N'PassOfficeID' and Object_ID = Object_ID(N'LocationBuilding'))
begin
	ALTER TABLE [dbo].[LocationBuilding] ADD
	[PassOfficeID] [int] NULL

	ALTER TABLE [dbo].[LocationBuilding] ADD CONSTRAINT [FK_LocationBuilding_LocationPassOffice] FOREIGN KEY ([PassOfficeID]) REFERENCES [dbo].[LocationPassOffice] ([PassOfficeID])
end
GO

--//@UNDO
if exists(select * from sys.columns 
            where Name = N'PassOfficeID' and Object_ID = Object_ID(N'LocationBuilding'))
begin
	ALTER TABLE [dbo].[LocationBuilding] DROP CONSTRAINT [FK_LocationBuilding_LocationPassOffice]

	ALTER TABLE [dbo].[LocationBuilding] DROP
	COLUMN [PassOfficeID]
end
GO