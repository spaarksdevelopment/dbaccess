
IF COL_LENGTH('AccessRequestArchive', 'IsSmartCard') IS NULL
BEGIN
	ALTER TABLE AccessRequestArchive 
	ADD IsSmartCard BIT
END

IF COL_LENGTH('AccessRequestArchive', 'SmartCardJustification') IS NULL
BEGIN
	ALTER TABLE AccessRequestArchive 
	ADD SmartCardJustification NVARCHAR(MAX)
END

GO 

--//@UNDO

IF COL_LENGTH('AccessRequestArchive', 'IsSmartCard') IS NOT NULL
BEGIN
	ALTER TABLE AccessRequestArchive 
	DROP COLUMN IsSmartCard
END

IF COL_LENGTH('AccessRequestArchive', 'SmartCardJustification') IS NOT NULL
BEGIN
	ALTER TABLE AccessRequestArchive 
	DROP COLUMN SmartCardJustification
END

GO 
