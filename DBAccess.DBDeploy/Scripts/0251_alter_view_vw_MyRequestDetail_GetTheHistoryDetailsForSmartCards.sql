IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyRequestsDetail]') )
	DROP VIEW [dbo].[vw_MyRequestsDetail]
GO

/****** Object:  View [dbo].[vw_MyRequestsDetail]    Script Date: 3/9/2017 12:46:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_MyRequestsDetail]
AS
SELECT        *, CASE WHEN
                             ((SELECT        TOP 1 TaskStatusId
                                 FROM            Tasks
                                 WHERE        RequestID = T1.[RequestID] ORDER BY TaskId DESC) = 2 ) THEN [dbo].[GetRequestHistory](ApproverName, AccessApproverName, RequestTypeID, Approved, AccessApproved, AwaitingActionFrom, ApprovedDate, 
                         AccessApprovedDate, '\r') + '\r' + ' Task Comments: ' +
                             (SELECT        TOP 1 TaskComment
                               FROM            Tasks
                               WHERE        RequestID = T1.[RequestID] ORDER BY TaskId DESC) ELSE [dbo].[GetRequestHistory](ApproverName, AccessApproverName, RequestTypeID, Approved, AccessApproved, AwaitingActionFrom, ApprovedDate, 
                         AccessApprovedDate, '\r') END AS [History]
FROM            (SELECT        arm.requestMasterID, arm.CreatedByID, arm.Created, AR.RequestTypeID, ART.Name AS RequestType, arp.Forename, arp.Surname, ARM.RequestStatusID, ARS.Name AS 'StatusDesc', 
                                                    lf.name AS 'FloorName', lb.name AS 'BuildingName', lc.name AS 'CityName', lcc.name AS 'CountryName', lr.Name AS 'RegionName', AA.Name AS 'AccessAreaName', 
                                                    AA.AccessAreaID AS 'AccessAreaID', AA.AccessAreaTypeID, arp.Forename + ' ' + arp.Surname AS 'ApplicantName', LB.LandlordID, arp.VendorName, AR.StartDate, AR.EndDate, NULL 
                                                    AS SponsorUserID, NULL AS SponsorName, NULL AS SponsorEmail, AR.Approved, AR.ApprovedDate, AR.ApprovedByID, AR.AccessApproved, AR.AccessApprovedDate, AR.AccessApprovedByID, 
                                                    Approver.Forename + ' ' + Approver.Surname ApproverName, AccessApprover.Forename + ' ' + AccessApprover.Surname AccessApproverName,
                                                        /*got rid of the while loop logic in function*/ (SELECT        TOP 1 CASE WHEN ((AR.Approved IS NULL OR
                                                                                                                                                                                 AR.AccessApproved IS NULL) AND (TasksUsers.mp_SecurityGroupID = 6 AND
                                                                                                                                                                                     (SELECT        COUNT(*)
                                                                                                                                                                                       FROM            mp_SecurityGroupUser
                                                                                                                                                                                       WHERE        SecurityGroupId = 6 AND dbPeopleID = hrPerson.dbPeopleID) > 0)) THEN
                                                                                                                                                                                     (SELECT Name FROM LocationPassOffice WHERE passOfficeId = AR.PassOfficeID) /*else*/ WHEN (AR.Approved IS NULL OR
                                                                                                                                                                                 AR.AccessApproved IS NULL) THEN reverse(stuff(reverse(STUFF
                                                                                                                                                                                     ((SELECT        CASE WHEN len(hrPerson.EmailAddress) 
                                                                                                                                                                                                                  > 0 THEN ' ' + hrPerson.Forename + ' ' + hrPerson.Surname + ' (' + hrPerson.EmailAddress + ')' + '\r' /*+ Cast(TasksUsers.mp_SecurityGroupID as varchar(50)) + '-' + Cast(hrPerson.dbPeopleID as varchar(50))*/ ELSE
                                                                                                                                                                                                                   ' ' + hrPerson.Forename + ' ' + hrPerson.Surname + '\r' END
                                                                                                                                                                                         FROM            hrPerson INNER JOIN
                                                                                                                                                                                                                  TasksUsers ON hrPerson.dbPeopleID = TasksUsers.dbPeopleID RIGHT OUTER JOIN
                                                                                                                                                                                                                  Tasks ON TasksUsers.TaskId = Tasks.TaskId
                                                                                                                                                                                         WHERE        (Tasks.TaskStatusId = 1) AND (TasksUsers.IsCurrent = 1) AND (Tasks.RequestId = AR.RequestID) FOR XML PATH('')), 1, 1, '')), 
                                                                                                                                                                                 1, 2, '')) ELSE NULL END
                                                                                                                                                        FROM            hrPerson INNER JOIN
                                                                                                                                                                                 TasksUsers ON hrPerson.dbPeopleID = TasksUsers.dbPeopleID RIGHT OUTER JOIN
                                                                                                                                                                                 Tasks ON TasksUsers.TaskId = Tasks.TaskId
                                                                                                                                                        WHERE        (Tasks.TaskStatusId = 1) AND (TasksUsers.IsCurrent = 1) AND (Tasks.RequestId = AR.RequestID)
                                                                                                                                                        ORDER BY TasksUsers.TasksUsersId DESC) AS AwaitingActionFrom, LF.FloorID, AR.RequestID, AR.IsSmartCard
                          FROM            dbo.AccessRequestMaster AS ARM INNER JOIN
                                                    dbo.AccessRequestPerson AS ARP ON arm.RequestMasterID = arp.RequestMasterID INNER JOIN
                                                    dbo.AccessRequest AS AR ON arp.RequestPersonID = ar.RequestPersonID INNER JOIN
                                                    dbo.AccessRequestStatus AS ARS ON ARS.RequestStatusID = ARM.RequestStatusID INNER JOIN
                                                    dbo.AccessRequestType AS ART ON ART.RequestTypeID = AR.RequestTypeID LEFT OUTER JOIN
                                                    dbo.AccessArea AS AA ON ar.AccessAreaID = aa.AccessAreaID LEFT OUTER JOIN
                                                    dbo.LocationFloor AS LF ON aa.FloorID = lf.FloorID LEFT OUTER JOIN
                                                    dbo.LocationBuilding AS LB /*on LF.BuildingID = lb.BuildingID*/ ON aa.BuildingID = lb.BuildingID LEFT OUTER JOIN
                                                    dbo.LocationCity AS LC ON lb.CityID = lc.CityID LEFT OUTER JOIN
                                                    dbo.LocationCountry AS LCC ON Lc.CountryID = lcc.CountryID LEFT OUTER JOIN
                                                    dbo.LocationRegion AS LR ON LCC.RegionID = lr.RegionID LEFT OUTER JOIN
                                                    hrPerson AS Approver ON AR.ApprovedByID = Approver.dbPeopleID LEFT OUTER JOIN
                                                    hrPerson AS AccessApprover ON AR.AccessApprovedByID = AccessApprover.dbPeopleID) AS T1
UNION ALL

SELECT        arm.requestMasterID, arm.CreatedByID, arm.Created, 4 AS RequestTypeID, 'Visitor' AS RequestType, arp.Forename, arp.Surname, ARM.RequestStatusID, ARS.Name AS 'StatusDesc', lf.name AS 'FloorName', 
                         lb.name AS 'BuildingName', lc.name AS 'CityName', lcc.name AS 'CountryName', lr.Name AS 'RegionName', NULL AS 'AccessAreaName', 0 AS 'AccessAreaID', NULL AS AccessAreaTypeID, 
                         arp.Forename + ' ' + arp.Surname AS 'ApplicantName', LB.LandlordID, arp.VendorName, AR.StartDate, AR.EndDate, ARM.SponsorUserID, Sponsor.Forename + ' ' + Sponsor.Surname AS SponsorName, 
                         Sponsor.EmailAddress AS SponsorEmail, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, LF.FloorID, AR.VisitorRequestID, 0 as IsSmartCard, '' History
FROM            dbo.AccessRequestVisitorRequest AS AR INNER JOIN
                         dbo.AccessRequestPerson AS ARP ON AR.RequestPersonID = ARP.RequestPersonID INNER JOIN
                         dbo.AccessRequestMaster AS ARM ON ARP.RequestMasterID = ARM.RequestMasterID INNER JOIN
                         dbo.AccessRequestStatus AS ARS ON ARS.RequestStatusID = AR.RequestStatusID INNER JOIN
                         dbo.LocationFloor AS LF ON AR.FloorID = lf.FloorID INNER JOIN
                         dbo.LocationBuilding AS LB ON LF.BuildingID = lb.BuildingID INNER JOIN
                         dbo.LocationCity AS LC ON lb.CityID = lc.CityID INNER JOIN
                         dbo.LocationCountry AS LCC ON Lc.CountryID = lcc.CountryID INNER JOIN
                         dbo.LocationRegion AS LR ON LCC.RegionID = lr.RegionID LEFT OUTER JOIN
                         hrPerson AS Sponsor ON ARM.SponsorUserID = Sponsor.dbPeopleID
WHERE        AR.Disabled = 0

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MyRequestsDetail]') )
	DROP VIEW [dbo].[vw_MyRequestsDetail]
GO

/****** Object:  View [dbo].[vw_MyRequestsDetail]    Script Date: 3/9/2017 11:39:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MyRequestsDetail]
AS
SELECT        *, CASE WHEN
                             ((SELECT        TOP 1 TaskStatusId
                                 FROM            Tasks
                                 WHERE        RequestID = T1.[RequestID]) = 2) THEN [dbo].[GetRequestHistory](ApproverName, AccessApproverName, RequestTypeID, Approved, AccessApproved, AwaitingActionFrom, ApprovedDate, 
                         AccessApprovedDate, '\r') + '\r' + ' Task Comments: ' +
                             (SELECT        TOP 1 TaskComment
                               FROM            Tasks
                               WHERE        RequestID = T1.[RequestID]) ELSE [dbo].[GetRequestHistory](ApproverName, AccessApproverName, RequestTypeID, Approved, AccessApproved, AwaitingActionFrom, ApprovedDate, 
                         AccessApprovedDate, '\r') END AS [History]
FROM            (SELECT        arm.requestMasterID, arm.CreatedByID, arm.Created, AR.RequestTypeID, ART.Name AS RequestType, arp.Forename, arp.Surname, ARM.RequestStatusID, ARS.Name AS 'StatusDesc', 
                                                    lf.name AS 'FloorName', lb.name AS 'BuildingName', lc.name AS 'CityName', lcc.name AS 'CountryName', lr.Name AS 'RegionName', AA.Name AS 'AccessAreaName', 
                                                    AA.AccessAreaID AS 'AccessAreaID', AA.AccessAreaTypeID, arp.Forename + ' ' + arp.Surname AS 'ApplicantName', LB.LandlordID, arp.VendorName, AR.StartDate, AR.EndDate, NULL 
                                                    AS SponsorUserID, NULL AS SponsorName, NULL AS SponsorEmail, AR.Approved, AR.ApprovedDate, AR.ApprovedByID, AR.AccessApproved, AR.AccessApprovedDate, AR.AccessApprovedByID, 
                                                    Approver.Forename + ' ' + Approver.Surname ApproverName, AccessApprover.Forename + ' ' + AccessApprover.Surname AccessApproverName,
                                                        /*got rid of the while loop logic in function*/ (SELECT        TOP 1 CASE WHEN ((AR.Approved IS NULL OR
                                                                                                                                                                                 AR.AccessApproved IS NULL) AND (TasksUsers.mp_SecurityGroupID = 6 AND
                                                                                                                                                                                     (SELECT        COUNT(*)
                                                                                                                                                                                       FROM            mp_SecurityGroupUser
                                                                                                                                                                                       WHERE        SecurityGroupId = 6 AND dbPeopleID = hrPerson.dbPeopleID) > 0)) THEN
                                                                                                                                                                                     (SELECT Name FROM LocationPassOffice WHERE passOfficeId = AR.PassOfficeID) /*else*/ WHEN (AR.Approved IS NULL OR
                                                                                                                                                                                 AR.AccessApproved IS NULL) THEN reverse(stuff(reverse(STUFF
                                                                                                                                                                                     ((SELECT        CASE WHEN len(hrPerson.EmailAddress) 
                                                                                                                                                                                                                  > 0 THEN ' ' + hrPerson.Forename + ' ' + hrPerson.Surname + ' (' + hrPerson.EmailAddress + ')' + '\r' /*+ Cast(TasksUsers.mp_SecurityGroupID as varchar(50)) + '-' + Cast(hrPerson.dbPeopleID as varchar(50))*/ ELSE
                                                                                                                                                                                                                   ' ' + hrPerson.Forename + ' ' + hrPerson.Surname + '\r' END
                                                                                                                                                                                         FROM            hrPerson INNER JOIN
                                                                                                                                                                                                                  TasksUsers ON hrPerson.dbPeopleID = TasksUsers.dbPeopleID RIGHT OUTER JOIN
                                                                                                                                                                                                                  Tasks ON TasksUsers.TaskId = Tasks.TaskId
                                                                                                                                                                                         WHERE        (Tasks.TaskStatusId = 1) AND (TasksUsers.IsCurrent = 1) AND (Tasks.RequestId = AR.RequestID) FOR XML PATH('')), 1, 1, '')), 
                                                                                                                                                                                 1, 2, '')) ELSE NULL END
                                                                                                                                                        FROM            hrPerson INNER JOIN
                                                                                                                                                                                 TasksUsers ON hrPerson.dbPeopleID = TasksUsers.dbPeopleID RIGHT OUTER JOIN
                                                                                                                                                                                 Tasks ON TasksUsers.TaskId = Tasks.TaskId
                                                                                                                                                        WHERE        (Tasks.TaskStatusId = 1) AND (TasksUsers.IsCurrent = 1) AND (Tasks.RequestId = AR.RequestID)
                                                                                                                                                        ORDER BY TasksUsers.TasksUsersId DESC) AS AwaitingActionFrom, LF.FloorID, AR.RequestID, AR.IsSmartCard
                          FROM            dbo.AccessRequestMaster AS ARM INNER JOIN
                                                    dbo.AccessRequestPerson AS ARP ON arm.RequestMasterID = arp.RequestMasterID INNER JOIN
                                                    dbo.AccessRequest AS AR ON arp.RequestPersonID = ar.RequestPersonID INNER JOIN
                                                    dbo.AccessRequestStatus AS ARS ON ARS.RequestStatusID = ARM.RequestStatusID INNER JOIN
                                                    dbo.AccessRequestType AS ART ON ART.RequestTypeID = AR.RequestTypeID LEFT OUTER JOIN
                                                    dbo.AccessArea AS AA ON ar.AccessAreaID = aa.AccessAreaID LEFT OUTER JOIN
                                                    dbo.LocationFloor AS LF ON aa.FloorID = lf.FloorID LEFT OUTER JOIN
                                                    dbo.LocationBuilding AS LB /*on LF.BuildingID = lb.BuildingID*/ ON aa.BuildingID = lb.BuildingID LEFT OUTER JOIN
                                                    dbo.LocationCity AS LC ON lb.CityID = lc.CityID LEFT OUTER JOIN
                                                    dbo.LocationCountry AS LCC ON Lc.CountryID = lcc.CountryID LEFT OUTER JOIN
                                                    dbo.LocationRegion AS LR ON LCC.RegionID = lr.RegionID LEFT OUTER JOIN
                                                    hrPerson AS Approver ON AR.ApprovedByID = Approver.dbPeopleID LEFT OUTER JOIN
                                                    hrPerson AS AccessApprover ON AR.AccessApprovedByID = AccessApprover.dbPeopleID) AS T1
UNION ALL

SELECT        arm.requestMasterID, arm.CreatedByID, arm.Created, 4 AS RequestTypeID, 'Visitor' AS RequestType, arp.Forename, arp.Surname, ARM.RequestStatusID, ARS.Name AS 'StatusDesc', lf.name AS 'FloorName', 
                         lb.name AS 'BuildingName', lc.name AS 'CityName', lcc.name AS 'CountryName', lr.Name AS 'RegionName', NULL AS 'AccessAreaName', 0 AS 'AccessAreaID', NULL AS AccessAreaTypeID, 
                         arp.Forename + ' ' + arp.Surname AS 'ApplicantName', LB.LandlordID, arp.VendorName, AR.StartDate, AR.EndDate, ARM.SponsorUserID, Sponsor.Forename + ' ' + Sponsor.Surname AS SponsorName, 
                         Sponsor.EmailAddress AS SponsorEmail, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, LF.FloorID, AR.VisitorRequestID, 0 as IsSmartCard, '' History
FROM            dbo.AccessRequestVisitorRequest AS AR INNER JOIN
                         dbo.AccessRequestPerson AS ARP ON AR.RequestPersonID = ARP.RequestPersonID INNER JOIN
                         dbo.AccessRequestMaster AS ARM ON ARP.RequestMasterID = ARM.RequestMasterID INNER JOIN
                         dbo.AccessRequestStatus AS ARS ON ARS.RequestStatusID = AR.RequestStatusID INNER JOIN
                         dbo.LocationFloor AS LF ON AR.FloorID = lf.FloorID INNER JOIN
                         dbo.LocationBuilding AS LB ON LF.BuildingID = lb.BuildingID INNER JOIN
                         dbo.LocationCity AS LC ON lb.CityID = lc.CityID INNER JOIN
                         dbo.LocationCountry AS LCC ON Lc.CountryID = lcc.CountryID INNER JOIN
                         dbo.LocationRegion AS LR ON LCC.RegionID = lr.RegionID LEFT OUTER JOIN
                         hrPerson AS Sponsor ON ARM.SponsorUserID = Sponsor.dbPeopleID
WHERE        AR.Disabled = 0
GO

