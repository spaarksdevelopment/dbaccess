IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_CreateChangeStartDateTask]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_CreateChangeStartDateTask]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeed_CreateChangeStartDateTask]    Script Date: 6/1/2015 5:28:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--================================================================
-- History:
--			26/05/2015 by Asanka
--			Modified the logic of taking pass offices based on building pass office.
----================================================================


CREATE PROCEDURE [dbo].[HRFeed_CreateChangeStartDateTask] 
	@dbPeopleId int,
	@NewStartDate datetime
as 
begin 

--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int

exec [dbo].[up_InsertRequestMaster] null, @RequestMasterID output
exec [dbo].[up_InsertRequestPerson]	null, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

--find out the person's home pass office
declare @homePassOfficeID int

select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = 1

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = null,
			@RequestTypeID = 18,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = @NewStartDate,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
			
		fetch next from badgesCursor into @badgeId
	end
end

close badgesCursor



--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for
	select distinct ISNULL(blp.PassOfficeID, c.PassOfficeID) as PassOfficeID from HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		left outer join LocationPassOffice blp on  blp.PassOfficeID = lb.PassOfficeID and blp.Enabled = 1
		where (p.EndDate is null or p.EndDate > getdate())
			and ISNULL(blp.PassOfficeID, c.PassOfficeID) <> @homePassOfficeID
			and dbo.HasActivePassOfficers(ISNULL(blp.PassOfficeID, c.PassOfficeID)) = 1
			and p.dbPeopleId = @dbPeopleId

declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = null,
			@RequestTypeID = 18,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = @NewStartDate,
			@EndDate = null,
			@BadgeID = null,
			@DivisionID = null,
			@iPassOfficeID = @passOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
		
		fetch next from passOfficeCursor into @passOfficeID
	end

close passOfficeCursor
DEALLOCATE passOfficeCursor



--submit the request
declare @success bit
exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

end





GO




--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_CreateChangeStartDateTask]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_CreateChangeStartDateTask]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeed_CreateChangeStartDateTask]    Script Date: 5/26/2015 4:21:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[HRFeed_CreateChangeStartDateTask] 
	@dbPeopleId int,
	@NewStartDate datetime
as 
begin 

--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int

exec [dbo].[up_InsertRequestMaster] null, @RequestMasterID output
exec [dbo].[up_InsertRequestPerson]	null, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

--find out the person's home pass office
declare @homePassOfficeID int

select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = 1

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = null,
			@RequestTypeID = 18,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = @NewStartDate,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
			
		fetch next from badgesCursor into @badgeId
	end
end

close badgesCursor



--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for
	select distinct c.PassOfficeID from HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		where (p.EndDate is null or p.EndDate > getdate())
			and c.PassOfficeID <> @homePassOfficeID
			and p.dbPeopleId = @dbPeopleId

declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
	begin
		exec dbo.up_InsertRequest
			@CreatedByUserID = null,
			@RequestTypeID = 18,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = @NewStartDate,
			@EndDate = null,
			@BadgeID = null,
			@DivisionID = null,
			@iPassOfficeID = @passOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
		
		fetch next from passOfficeCursor into @passOfficeID
	end

close passOfficeCursor
DEALLOCATE passOfficeCursor



--submit the request
declare @success bit
exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

end





GO

