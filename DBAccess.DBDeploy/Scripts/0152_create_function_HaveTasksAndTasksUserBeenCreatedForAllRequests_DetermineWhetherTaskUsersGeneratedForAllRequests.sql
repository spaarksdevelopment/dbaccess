IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HaveTasksAndTasksUserBeenCreatedForAllRequests]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[HaveTasksAndTasksUserBeenCreatedForAllRequests]
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Asanka
-- Create date: 30/11/2015
-- Description:	Returns whether tasks and task users created for all requests in a master request
-- =============================================
CREATE FUNCTION [dbo].[HaveTasksAndTasksUserBeenCreatedForAllRequests]
(
	@RequestMasterID int
)
RETURNS bit
AS
BEGIN
	
	DECLARE @HasTaskUsersCreatedForAllRequest bit = 1
	DECLARE @RequestID int
	DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT AccessRequest.RequestID 
		FROM AccessRequestMaster 
			INNER JOIN AccessRequestPerson ON AccessRequestMaster.RequestMasterID = AccessRequestPerson.RequestMasterID
			INNER JOIN AccessRequest ON AccessRequestPerson.RequestPersonID = AccessRequest.RequestPersonID
		WHERE AccessRequestMaster.RequestMasterID = @RequestMasterID


	OPEN myCursor
 
	FETCH NEXT FROM myCursor INTO @RequestID 
 
	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF NOT EXISTS(SELECT * FROM Tasks INNER JOIN TasksUsers ON Tasks.TaskId = TasksUsers.TaskId WHERE Tasks.RequestId = @RequestID)
		BEGIN
			SET @HasTaskUsersCreatedForAllRequest = 0
			BREAK
		END

	FETCH NEXT FROM myCursor INTO @RequestID
	END
 
	CLOSE myCursor
	DEALLOCATE myCursor

			
	RETURN @HasTaskUsersCreatedForAllRequest

	END






GO


--//@UNDO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HaveTasksAndTasksUserBeenCreatedForAllRequests]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[HaveTasksAndTasksUserBeenCreatedForAllRequests]
END
GO
