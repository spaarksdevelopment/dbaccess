﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateUnsuspendAccessRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[GenerateUnsuspendAccessRequests]
END
GO


/****** Object:  StoredProcedure [dbo].[GenerateUnsuspendAccessRequests]    Script Date: 21/10/2015 18:46:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GenerateUnsuspendAccessRequests]
AS
BEGIN 
	DECLARE @TransactionID int
	DECLARE @dbPeopleID INT
	DECLARE @TerminationDate datetime

	DECLARE UnsuspendTriggerCursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT TransactionId, md.dbPeopleId, TerminationDate 
	FROM HRFeedMessage hrm INNER JOIN 
		 MasterData md ON hrm.HRID = md.HR_ID INNER JOIN 
		 HRPerson hp ON md.dbPeopleID=hp.dbPeopleID
	WHERE TriggerType = 'JOIN' AND 
	ReactivationFlag = 'Y' AND 
	hp.Suspended = 1 AND 
	(SuspensionProcessed IS NULL OR SuspensionProcessed = 0)

	OPEN UnsuspendTriggerCursor
	FETCH NEXT FROM UnsuspendTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate

	WHILE @@FETCH_STATUS = 0
	BEGIN
	--Don't create suspension if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN
		EXEC UnsuspendAccessForUser @dbPeopleId = @dbPeopleId, @Valid = 1, @CreatedByID  = NULL

		--Send emails to pass officers
		EXEC dbo.QueueEmailsForUser @dbPeopleId = @dbPeopleId, @EmailTypeID = 40
	END

	UPDATE HRFeedMessage SET SuspensionProcessed = 1 , SuspensionProcessedDate = GETDATE() WHERE TransactionId = @TransactionId

	FETCH NEXT FROM UnsuspendTriggerCursor INTO @TransactionId, @dbPeopleId, @TerminationDate
	END
 
	CLOSE UnsuspendTriggerCursor
	DEALLOCATE UnsuspendTriggerCursor
 END

 GO

 --//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateUnsuspendAccessRequests]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[GenerateUnsuspendAccessRequests]
END
GO