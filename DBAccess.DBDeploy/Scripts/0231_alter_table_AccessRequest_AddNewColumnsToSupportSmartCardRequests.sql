IF COL_LENGTH('AccessRequest', 'IsSmartCard') IS NULL
BEGIN
	ALTER TABLE AccessRequest 
	ADD IsSmartCard BIT
END

IF COL_LENGTH('AccessRequest', 'SmartCardJustification') IS NULL
BEGIN
	ALTER TABLE AccessRequest 
	ADD SmartCardJustification NVARCHAR(MAX)
END

GO 

--//@UNDO

IF COL_LENGTH('AccessRequest', 'IsSmartCard') IS NOT NULL
BEGIN
	ALTER TABLE AccessRequest 
	DROP COLUMN IsSmartCard
END

IF COL_LENGTH('AccessRequest', 'SmartCardJustification') IS NOT NULL
BEGIN
	ALTER TABLE AccessRequest 
	DROP COLUMN SmartCardJustification
END

GO 
