IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedRevoke]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeedRevoke]    Script Date: 11/24/2015 4:15:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--		 22/10/2015 Asanka - Fixed issue of not sending emails to user's home country's pass officers, when the default pass office is outside of home country
--		 02/11/2015 Asanka - Enable revocation for already suspended users
--       11/11/2015 Wijitha - Modifed SP to use helper SPs for request creation and email sending
--		 24/11/2015 Wijitha - Modifed logic to revoke a person based on termination date if termination date is before the revoke date
--======================================================================
 CREATE PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime
 DECLARE @RevokeDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
	 SELECT TransactionId, HRID, TerminationDate, RevokeAccessDateTimestamp
	 FROM HRFeedMessage hrm 
		JOIN HRPerson hp ON CAST(REPLACE(hrm.HRID, 'C', '') as int) = hp.dbpeopleid
	 WHERE RevokeProcessed = 0 
		AND TriggerType = 'LVER' AND TriggerAction <> 'DEL'
		AND (RevokeAccessDateTimestamp <= GETDATE() OR TerminationDate <= GETDATE())
		AND (hp.Revoked IS NULL OR hp.Revoked = 0 OR hp.Suspended = 1)

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate, @RevokeDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int 
	SELECT @ReplacedHRID = REPLACE(@HRID, 'C', '')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)

	--If Termination Date is before the revoke date, then consider the temination date and do the revoke. Just after that perform Offboarding. 
	IF (@TerminationDate < @RevokeDate)
	BEGIN

		EXEC HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL

		EXEC PassOfficeUserOffBoarding @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID = NULL

		UPDATE HRFeedMessage SET RevokeProcessed = 1, Processed = 1, RevokeProcessedDate = GETDATE(), ProcessedDate = GETDATE()  WHERE TransactionId = @TransactionId

	END
	ELSE
	BEGIN

		EXEC HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL

		UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId

	END	
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate, @RevokeDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
  

GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedRevoke]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedRevoke]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeedRevoke]    Script Date: 11/24/2015 12:40:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--		 22/10/2015 Asanka - Fixed issue of not sending emails to user's home country's pass officers, when the default pass office is outside of home country
--		 02/11/2015 Asanka - Enable revocation for already suspended users
--       11/11/2015 Wijitha - Modifed SP to use helper SPs for request creation and email sending
--======================================================================
 CREATE PROCEDURE [dbo].[HRFeedRevoke]
 AS
 
 DECLARE @TransactionID int
 DECLARE @HRID nvarchar(11)
 DECLARE @TerminationDate datetime

 DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
	 SELECT TransactionId, HRID, TerminationDate 
	 FROM HRFeedMessage hrm 
		JOIN HRPerson hp ON CAST(REPLACE(hrm.HRID, 'C', '') as int) = hp.dbpeopleid
	 WHERE RevokeProcessed = 0 
		AND TriggerType = 'LVER' AND TriggerAction <> 'DEL'
		AND RevokeAccessDateTimestamp <= GETDATE() 
		AND (hp.Revoked IS NULL OR hp.Revoked = 0 OR hp.Suspended = 1)

 OPEN myCursor
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 
 WHILE @@FETCH_STATUS = 0
 BEGIN
 
	DECLARE @ReplacedHRID nvarchar(11)
	DECLARE @dbPeopleIdConvert int 
	SELECT @ReplacedHRID = REPLACE(@HRID, 'C', '')
	SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
	
	--Don't create revoke if termination date is greater than 7 days in the past, because offboard should already be created
	IF @TerminationDate is null OR (@TerminationDate > DATEADD(d,-7,GETDATE()))
	BEGIN

		EXEC HRFeed_RevokeAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL

	END
	 
	UPDATE HRFeedMessage SET RevokeProcessed = 1 , RevokeProcessedDate = GETDATE() WHERE TransactionId = @TransactionId
 
 FETCH NEXT FROM myCursor INTO @TransactionId, @HRID, @TerminationDate
 END
 
 CLOSE myCursor
 DEALLOCATE myCursor
  

GO


