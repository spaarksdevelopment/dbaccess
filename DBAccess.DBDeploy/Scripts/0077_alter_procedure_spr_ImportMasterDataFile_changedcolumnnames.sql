IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spr_ImportMasterDataFile]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[spr_ImportMasterDataFile]
END
GO

/****** Object:  StoredProcedure [dbo].[spr_ImportMasterDataFile]    Script Date: 21/10/2015 15:09:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--     Logic to do the data import
CREATE PROCEDURE [dbo].[spr_ImportMasterDataFile]
AS
BEGIN
--     WE COULD TRANSACTION THIS BUT WE ARE BETTER FAILING THEN IT WILL GET NOTICED
       TRUNCATE TABLE MasterData;
 
       INSERT INTO MasterData
       SELECT [HR_ID]
              ,[LEGAL_FIRST_NAME]
              ,[LEGAL_MIDDLE_NAME]
              ,[LEGAL_LAST_NAME]
              ,[PREFERRED_NAME]
              ,[EMPLOYEE_EMAIL_ID]
              ,[EMPLOYEE_CLASS]
              ,[EMPLOYEE_STATUS]
              ,[ORGANIZATIONAL_RELATIONSHIP]
              ,[CORPORATE_TITLE]
              ,[POSITION_NUMBER]
              ,[JOB_CODE]
              ,[TELEPHONE_NO]
              ,[LOCATION_ID]
              ,[LOCATION_COUNTRY]
              ,[LOCATION_CITY]
              ,[COST_CENTER]
              ,[COST_CENTER_DESCR]
              ,[LEGAL_ENTITY]
              ,[GR_DIVISION_CODE]
              ,[UBR_PRODUCT_CODE]
              ,[UBR_PRODUCT_DESCR]
              ,[UBR_SUB_PRODUCT_CODE]
              ,[UBR_SUB_PRODUCT_DESCR]
              ,[EFFECTIVE_DATE_JOB_START]
              ,[EFFECTIVE_DATE_JOB_END]
              ,[CREATE ACCESS DATE TIMESTAMP]
              ,[REVOKE_ACCESS_DATE_TIMESTAMP]
              ,[MANAGER_HR_ID]
              ,[MANAGER_EMAIL_ADDRESS]
              ,[BADGE_ID]
              ,[HR_ID] AS [dbPeopleId],
              dbo.[ComputeUBRPath]([GR_DIVISION_CODE])
       FROM MasterDataImport
END

GO

--//@UNDO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spr_ImportMasterDataFile]') AND type in (N'P', N'PC'))
--BEGIN
--	DROP PROCEDURE [dbo].[spr_ImportMasterDataFile]
--END
--GO


--/****** Object:  StoredProcedure [dbo].[spr_ImportMasterDataFile]    Script Date: 21/10/2015 15:09:23 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
  
----     Logic to do the data import
--CREATE PROCEDURE [dbo].[spr_ImportMasterDataFile]
--AS
--BEGIN

----     WE COULD TRANSACTION THIS BUT WE ARE BETTER FAILING THEN IT WILL GET NOTICED
--       TRUNCATE TABLE MasterData;
 
--INSERT INTO MasterData
--SELECT [HR_ID]
--        ,[FIRST_NAME]
--        ,[MIDDLE_NAME]
--        ,[LAST_NAME]
--        ,[PREFERRED_NAME]
--        ,[EMPLOYEE_EMAIL_ID]
--        ,[EMPLOYEE_CLASS]
--        ,[EMPLOYEE_STATUS]
--        ,[ORGANIZATIONAL_RELATIONSHIP]
--        ,[CORPORATE_TITLE]
--        ,[POSITION_NUMBER]
--        ,[JOB_CODE]
--        ,[TELEPHONE_NO]
--        ,[LOCATION_ID]
--        ,[LOCATION_COUNTRY]
--        ,[LOCATION_CITY]
--        ,[COST_CENTER]
--        ,[COST_CENTER_DESCR]
--        ,[LEGAL_ENTITY]
--        ,[GR_DIVISION_CODE]
--        ,[UBR_PRODUCT_CODE]
--        ,[UBR_PRODUCT_DESCR]
--        ,[UBR_SUB_PRODUCT_CODE]
--        ,[UBR_SUB_PRODUCT_DESCR]
--        ,[EFFECTIVE_DATE_JOB_START]
--        ,[EFFECTIVE_DATE_JOB_END]
--        ,[CREATE ACCESS DATE TIMESTAMP]
--        ,[REVOKE_ACCESS_DATE_TIMESTAMP]
--        ,[MANAGER_HR_ID]
--        ,[MANAGER_EMAIL_ADDRESS]
--        ,[BADGE_ID]
--        ,[HR_ID] AS [dbPeopleId],
--        dbo.[ComputeUBRPath]([GR_DIVISION_CODE])
--FROM MasterDataImport
--END

--GO