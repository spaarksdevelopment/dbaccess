IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_InsertRequest]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_InsertRequest]
END
GO


/****** Object:  StoredProcedure [dbo].[up_InsertRequest]    Script Date: 5/19/2015 3:24:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- Author:        <AIS> 
-- Create date: <09/02/2011> 
-- Description:   <Insert the Request> 
-- History:
--     11/04/2014 (CM) - Remove old code which fills in BusinessAreaID and DivisionID
--     19/04/2015 (Wijitha) - Modifed the pass office retrieving logic based on building pass office
-- ============================================= 

CREATE PROCEDURE [dbo].[up_InsertRequest] 
( 
      @CreatedByUserID Int 
      ,@RequestTypeID Int 
      ,@AccessAreaID Int 
      ,@RequestPersonID Int 
      ,@StartDate DateTime 
      ,@EndDate DateTime 
      ,@BadgeID Int 
      ,@DivisionID Int 
      ,@iPassOfficeID Int 
      ,@BusinessAreaID Int 
      ,@RequestID Int Output 
      ,@ExternalSystemRequestID Int = null 
      ,@ExternalSystemID Int = null 
      ,@ClassificationID Int 
) 
AS 
Begin 
                
          --If PassOfficeID is null 
          --        Calculate PassOfficeID 
          --                If AccessArea is not null then use that country as the pass office 
          --                Otherwise use the home pass office 
          --                        --find out the person's home pass office 
                                        --declare @homePassOfficeID int 
                                        --select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp 
                                        --inner join LocationCountry lc on lc.CountryID = arp.CountryID 
                                        --where arp.RequestPersonID = @RequestPersonID 
          --        Set @iPassOfficeID 
          IF @iPassOfficeID IS NULL 
          BEGIN 
                  IF @AccessAreaID IS NOT NULL 
                  BEGIN 
                          SELECT  @iPassOfficeID= ISNULL(BPO.PassOfficeID, LCC.PassOfficeID) -- First try to get Pass office assigned to then building, if not fall back country's default one
                          FROM dbo.AccessArea 
							  INNER JOIN dbo.LocationBuilding LB ON AccessArea.BuildingID = LB.BuildingID 
							  INNER JOIN dbo.LocationCity  LC  ON LB.CityID  =LC.CityID 
							  INNER JOIN dbo.LocationCountry LCC  ON LC.CountryID =LCC.COuntryID 
							  LEFT OUTER JOIN dbo.LocationPassOffice BPO ON BPO.PassOfficeID = LB.PassOfficeID and BPO.[Enabled] = 1
						  WHERE dbo.AccessArea.AccessAreaID=@AccessAreaID
                  END                   
                  ELSE 
                  BEGIN 
                        select @iPassOfficeID = PassOfficeID
						from 
						AccessRequestPerson arp 
						inner join LocationCountry lc on lc.CountryID = arp.CountryID 
						where arp.RequestPersonID = @RequestPersonID 
                  END 
          END 


      Set NoCount On; 

      Set @RequestID = 0; 

      Declare @dtNow DateTime2 = SysDateTime(); 

      Begin Try 

            Declare @UBR VarChar(6); 

            If @DivisionID Is Null 
            Begin 
                  If (@RequestTypeID In (2, 3))       -- Access, Service 
                  Begin 

                        -- use the Access Area (where they want Access) to determine the Division 
                        -- CorporateDivisionAccessArea (a many-many table) should only ever contain 
                        -- one value of Division per Access Area except when there is also a Compliance division 

                        Select Top 1 
                              @DivisionID = CorporateDivision.DivisionID 
                        From 
                              CorporateDivision 
                        Inner Join 
                              CorporateDivisionAccessArea 
                              On 
                              CorporateDivisionAccessArea.DivisionID = CorporateDivision.DivisionID 
                        Where 
                              --CorporateDivision.UBR != 'G_6520'         -- ignore Compliance divisions 
                              --And 
                              CorporateDivisionAccessArea.[Enabled] = 1 
                              And 
                              CorporateDivisionAccessArea.AccessAreaID = @AccessAreaID; 

                        DECLARE @errorMessage VARCHAR(100) 
                        SET @errorMessage = 'Division entry does not exist for Access Area. Access Area ID:' + CAST(@AccessAreaID as NVARCHAR(5)) 
                        
                        If (@DivisionID Is Null) RaisError (@errorMessage, 11, 2); 

                  End
            End

			If (@RequestTypeID In (1)) --New Pass
			BEGIN
				DECLARE @dbpeopleid int
				SELECT @dbpeopleid=dbpeopleid FROM AccessRequestPerson WHERE RequestPersonID=@RequestPersonID
				SELECT @StartDate=dbo.GetStartDateForBadgeRequest(@dbpeopleid)
			END

            Insert Into 
                  AccessRequest 
            ( 
                  RequestTypeID 
                  ,AccessAreaID 
                  ,RequestPersonID 
                  ,StartDate 
                  ,EndDate 
                  ,RequestStatusID 
                  ,Approved 
                  ,ApprovedDate 
                  ,BadgeID 
                  ,DivisionID 
                  ,PassOfficeID 
                  ,BusinessAreaID 
                  ,CreatedByID 
                  ,Created 
                  ,ExternalSystemRequestID 
                  ,ExternalSystemID 
                  ,Classification 
            ) 
            Values 
            ( 
                  @RequestTypeID 
                  ,@AccessAreaID 
                  ,@RequestPersonID 
                  ,@StartDate 
                  ,@EndDate 
                  ,1 
                  ,null 
                  ,null 
                  ,@BadgeID 
                  ,@DivisionID 
                  ,@iPassOfficeID 
                  ,@BusinessAreaID 
                  ,@CreatedByUserID 
                  ,@dtNow 
                  ,@ExternalSystemRequestID 
                  ,@ExternalSystemID 
                  ,@ClassificationID 
            ); 

            Set @RequestID = Scope_Identity(); 


      End Try 
      Begin Catch 


            -- raise an error with the details of the exception 

            Declare @iState Int; 
            Declare @iSeverity Int; 
            Declare @sMessage NVarChar(4000); 

            Select 
                  @iState = Error_State (), 
                  @sMessage = Error_Message (), 
                  @iSeverity = Error_Severity () 

            RaisError (@sMessage, @iSeverity, @iState); 

      End Catch 
End;

GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_InsertRequest]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_InsertRequest]
END
GO

/****** Object:  StoredProcedure [dbo].[up_InsertRequest]    Script Date: 5/19/2015 3:28:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ============================================= 
-- Author:        <AIS> 
-- Create date: <09/02/2011> 
-- Description:   <Insert the Request> 
-- History:
--     11/04/2014 (CM) - Remove old code which fills in BusinessAreaID and DivisionID
-- ============================================= 

CREATE PROCEDURE [dbo].[up_InsertRequest] 
( 
      @CreatedByUserID Int 
      ,@RequestTypeID Int 
      ,@AccessAreaID Int 
      ,@RequestPersonID Int 
      ,@StartDate DateTime 
      ,@EndDate DateTime 
      ,@BadgeID Int 
      ,@DivisionID Int 
      ,@iPassOfficeID Int 
      ,@BusinessAreaID Int 
      ,@RequestID Int Output 
      ,@ExternalSystemRequestID Int = null 
      ,@ExternalSystemID Int = null 
      ,@ClassificationID Int 
) 
AS 
Begin 
                
          --If PassOfficeID is null 
          --        Calculate PassOfficeID 
          --                If AccessArea is not null then use that country as the pass office 
          --                Otherwise use the home pass office 
          --                        --find out the person's home pass office 
                                        --declare @homePassOfficeID int 
                                        --select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp 
                                        --inner join LocationCountry lc on lc.CountryID = arp.CountryID 
                                        --where arp.RequestPersonID = @RequestPersonID 
          --        Set @iPassOfficeID 
          IF @iPassOfficeID IS NULL 
          BEGIN 
                  IF @AccessAreaID IS NOT NULL 
                  BEGIN 
                          SELECT  @iPassOfficeID=LCC.PassOfficeID 
                          FROM dbo.AccessArea 
                          INNER JOIN dbo.LocationBuilding LB ON AccessArea.BuildingID = LB.BuildingID 
                          INNER JOIN dbo.LocationCity  LC  ON LB.CityID  =LC.CityID 
                          INNER JOIN dbo.LocationCountry LCC  ON LC.CountryID =LCC.COuntryID 
						  WHERE dbo.AccessArea.AccessAreaID=@AccessAreaID
                  END                   
                  ELSE 
                  BEGIN 
                        select @iPassOfficeID = PassOfficeID
						from 
						AccessRequestPerson arp 
						inner join LocationCountry lc on lc.CountryID = arp.CountryID 
						where arp.RequestPersonID = @RequestPersonID 
                  END 
          END 


      Set NoCount On; 

      Set @RequestID = 0; 

      Declare @dtNow DateTime2 = SysDateTime(); 

      Begin Try 

            Declare @UBR VarChar(6); 

            If @DivisionID Is Null 
            Begin 
                  If (@RequestTypeID In (2, 3))       -- Access, Service 
                  Begin 

                        -- use the Access Area (where they want Access) to determine the Division 
                        -- CorporateDivisionAccessArea (a many-many table) should only ever contain 
                        -- one value of Division per Access Area except when there is also a Compliance division 

                        Select Top 1 
                              @DivisionID = CorporateDivision.DivisionID 
                        From 
                              CorporateDivision 
                        Inner Join 
                              CorporateDivisionAccessArea 
                              On 
                              CorporateDivisionAccessArea.DivisionID = CorporateDivision.DivisionID 
                        Where 
                              --CorporateDivision.UBR != 'G_6520'         -- ignore Compliance divisions 
                              --And 
                              CorporateDivisionAccessArea.[Enabled] = 1 
                              And 
                              CorporateDivisionAccessArea.AccessAreaID = @AccessAreaID; 

                        DECLARE @errorMessage VARCHAR(100) 
                        SET @errorMessage = 'Division entry does not exist for Access Area. Access Area ID:' + CAST(@AccessAreaID as NVARCHAR(5)) 
                        
                        If (@DivisionID Is Null) RaisError (@errorMessage, 11, 2); 

                  End
            End

			If (@RequestTypeID In (1)) --New Pass
			BEGIN
				DECLARE @dbpeopleid int
				SELECT @dbpeopleid=dbpeopleid FROM AccessRequestPerson WHERE RequestPersonID=@RequestPersonID
				SELECT @StartDate=dbo.GetStartDateForBadgeRequest(@dbpeopleid)
			END

            Insert Into 
                  AccessRequest 
            ( 
                  RequestTypeID 
                  ,AccessAreaID 
                  ,RequestPersonID 
                  ,StartDate 
                  ,EndDate 
                  ,RequestStatusID 
                  ,Approved 
                  ,ApprovedDate 
                  ,BadgeID 
                  ,DivisionID 
                  ,PassOfficeID 
                  ,BusinessAreaID 
                  ,CreatedByID 
                  ,Created 
                  ,ExternalSystemRequestID 
                  ,ExternalSystemID 
                  ,Classification 
            ) 
            Values 
            ( 
                  @RequestTypeID 
                  ,@AccessAreaID 
                  ,@RequestPersonID 
                  ,@StartDate 
                  ,@EndDate 
                  ,1 
                  ,null 
                  ,null 
                  ,@BadgeID 
                  ,@DivisionID 
                  ,@iPassOfficeID 
                  ,@BusinessAreaID 
                  ,@CreatedByUserID 
                  ,@dtNow 
                  ,@ExternalSystemRequestID 
                  ,@ExternalSystemID 
                  ,@ClassificationID 
            ); 

            Set @RequestID = Scope_Identity(); 


      End Try 
      Begin Catch 


            -- raise an error with the details of the exception 

            Declare @iState Int; 
            Declare @iSeverity Int; 
            Declare @sMessage NVarChar(4000); 

            Select 
                  @iState = Error_State (), 
                  @sMessage = Error_Message (), 
                  @iSeverity = Error_Severity () 

            RaisError (@sMessage, @iSeverity, @iState); 

      End Catch 
End;

GO