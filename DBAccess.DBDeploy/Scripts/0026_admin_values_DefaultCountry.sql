
/*** Adding Default Country Admin Value ***/
if(Not Exists(select * from AdminValues where [Key] = 'DefaultCountry'))
begin
	insert into AdminValues ([Key], Value) 
	values ('DefaultCountry', (Select top 1 CountryID from LocationCountry where Name = 'United States of America')) /*** Add the correct ID before you run this ***/
end
else
	print 'Already Exists'
GO

--//@UNDO

/*** Delete Default Country Admin Value ***/
if(Exists(select * from AdminValues where [Key] = 'DefaultCountry'))
begin
	delete AdminValues 
	where [Key] = 'DefaultCountry'
end
else
	print 'Not Exist'

GO