IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_CreatePassOfficeRequestsForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_CreatePassOfficeRequestsForUser]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeed_CreatePassOfficeRequestsForUser]    Script Date: 11/11/2015 12:29:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==============================================================================
-- Author: Wijitha Wijenyake
-- Create Date: 05/11/2015
-- Description: This is a generic SP to create all the type of request, centralising the 
--				common logic
-- History:  
--==============================================================================

CREATE PROCEDURE [dbo].[HRFeed_CreatePassOfficeRequestsForUser]

@dbPeopleId int,
@requestTypeId int,
@createdById int

AS
BEGIN

--Create request master / person
DECLARE @RequestMasterID int
DECLARE @RequestPersonID int
DECLARE @RequestID int

EXEC [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
EXEC [dbo].[up_InsertRequestPerson]	@CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

DECLARE @homePassOfficeID int
DECLARE @homeCountryID int
DECLARE @OfficerID varchar(2)

--Get person's office ID
SELECT @OfficerID = OfficerID 
FROM HRPerson
WHERE dbPeopleID = @dbPeopleId

--Get Person's home pass office ID and home country ID
SELECT @homePassOfficeID = PassOfficeID, @homeCountryID=lc.CountryID 
FROM AccessRequestPerson arp
	INNER JOIN LocationCountry lc on lc.CountryID = arp.CountryID
WHERE arp.RequestPersonID = @RequestPersonID

--====================================================================================================================
--First, get the the user's home pass office. If user has mutiple badges, a request has to be created for each badge.
--====================================================================================================================

declare @passOffices table
(
	PassOfficeID int,
	BadgeID int
)

delete @passOffices --Just in case

INSERT INTO @passOffices
SELECT @homePassOfficeID, PersonBadgeID 
FROM HRPersonBadge 
WHERE dbPeopleID = @dbPeopleId and Valid = 1

IF(NOT EXISTS (SELECT * FROM @passOffices))
BEGIN

INSERT INTO @passOffices
VALUES (@homePassOfficeID, NULL)

END

--====================================================================================================================
-- Secondly,
-- If user is a MD or D then requests should be created for all the pass offices.
-- If not, requests has to be created for each building pass office which users has got access and all the pass offices 
-- of user's home country
--====================================================================================================================

IF (@OfficerID = 'D' OR @OfficerID = 'MD')
BEGIN
	
	INSERT INTO @passOffices
	SELECT DISTINCT PassOfficeID, NULL 
	FROM LocationPassOffice
	WHERE PassOfficeID <> @homePassOfficeID	AND [Enabled]=1
		AND dbo.HasActivePassOfficers(PassOfficeID) = 1

END
ELSE
BEGIN
	
	INSERT INTO @passOffices
	SELECT DISTINCT PassOfficeID, NULL
	FROM LocationPassOffice --All the pass office located in home country
	WHERE CountryID = @homeCountryID 
		AND PassOfficeID <> @homePassOfficeID 
		AND [Enabled] = 1 
		AND dbo.HasActivePassOfficers(PassOfficeID) = 1
	UNION
	SELECT PassOfficeID, NULL
	FROM dbo.fGetAccessAreaPassOfficesPerPerson(@dbPeopleId) --Building pass offices user has access
	WHERE PassOfficeID <> @homePassOfficeID
		 AND dbo.HasActivePassOfficers(PassOfficeID) = 1		

END

--====================================================================================================================
--Once collected all the pass offices, then create requests.
--====================================================================================================================

DECLARE @passOfficeID int
DECLARE @badgeID int

DECLARE cur_passOffice CURSOR FOR 
	SELECT DISTINCT PassOfficeID, BadgeID
	FROM @passOffices

OPEN cur_passOffice

FETCH NEXT FROM cur_passOffice INTO @passOfficeID, @badgeID

WHILE (@@FETCH_STATUS = 0)
BEGIN

	EXEC dbo.up_InsertRequest
		@CreatedByUserID = @createdById,
		@RequestTypeID = @requestTypeId,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = @badgeID,
		@DivisionID = null,
		@iPassOfficeID = @passOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null

FETCH NEXT FROM cur_passOffice INTO @passOfficeID, @badgeID
END

CLOSE cur_passOffice
DEALLOCATE cur_passOffice

--Submit the request
DECLARE @success bit
EXEC up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

END
GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeed_CreatePassOfficeRequestsForUser]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeed_CreatePassOfficeRequestsForUser]
END
GO