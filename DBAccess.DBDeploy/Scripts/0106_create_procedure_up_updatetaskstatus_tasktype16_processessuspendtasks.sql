IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_UpdateTaskStatus_TaskType16]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType16]
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType16]
@TaskId INT, 
@TaskStatusId INT, 
@CompletedById INT,
@Comment VarChar(500),
@Result int Output

	AS

	-- Change start date for badge				

	SET NoCount On;
	SET @Result = 0;

	DECLARE @dtNow DateTime2 = GetDate();
	DECLARE @doToday Date = @dtNow;
	DECLARE @iReferences INT = 0;
	DECLARE @NumberOfApprovals INT = 0;

	DECLARE @TaskTypeId INT;
    DECLARE @DivisionId INT;
    DECLARE @BusinessAreaId INT;
    DECLARE @RequestId INT;
    DECLARE @AccessAreaId INT;
    DECLARE @iPassOfficeId INT;
	DECLARE @ickCompletedByID int;
	
    SELECT
          @TaskTypeId = TaskTypeId,
          @DivisionId = DivisionId,
          @BusinessAreaId = BusinessAreaId,
          @RequestId = RequestId,
          @AccessAreaId = AccessAreaId,
          @iPassOfficeId = PassOfficeId,
          @ickCompletedByID = CompletedByID
    FROM
          Tasks
    WHERE
          TaskId = @TaskId;          
   
	--Check if the Task has CompletedByID NULL
	--IF it is null Go Ahead and process it if not then by pass all the processing
	IF(@ickCompletedByID is NULL)
	BEGIN
	
	exec up_UpdateTaskStatus_CommonUpper
		@TaskId,
		@TaskTypeId, 
		@TaskStatusId, 
		@CompletedById,
		@Comment
		

		IF (@TaskStatusId = 3) --Task Approved
		BEGIN	
			UPDATE
				  [AccessRequest]
			SET
				  [Approved] = 1,
				  RequestStatusID = 5, --Actioned
				  [ApprovedDate] = @dtNow,
				  [ApprovedByID] = @CompletedById
			WHERE
				  RequestID = @RequestId;
			
			--2- GET PASS/BADGE DETAILS:
			DECLARE @BadgeType NChar(2)
			DECLARE @DBPeopleID INT
			DECLARE @LandlordID INT
			DECLARE @BuildingID INT
			DECLARE @BadgeID INT
			
			SELECT
				@BadgeID = BadgeID,
				@BadgeType = BadgeType,
				@DBPeopleID = dbPeopleID,
				@LandlordID = LandlordID,
				@BuildingID = BuildingID
			FROM
				  vw_BadgeRequest
			WHERE RequestID = @RequestId;
				
			DECLARE @BadgeTypeD NChar (2)
			DECLARE @DBPeopleIDD INT
			DECLARE @LandlordIDD INT
			DECLARE @BuildingIDD INT
			DECLARE @BadgeIDD INT
			
			
			
			DECLARE @RequestTypeIDForDeactivation INT
			
			SELECT @RequestTypeIDForDeactivation = RequestTypeID
			FROM AccessRequest
			WHERE RequestId=@RequestId;
			
			IF @RequestTypeIDForDeactivation = 19 -- Suspend Access
			BEGIN
				SELECT
					@BadgeIDD = BadgeID,
					@BadgeType = BadgeType,
					@DBPeopleID = dbPeopleID,
					@LandlordID = LandlordID,
					@BuildingID = BuildingID
				FROM
					  vw_BadgeRequestForDeactivation
				WHERE RequestID = @RequestId;
			END
			ELSE
			BEGIN
				SELECT
					@BadgeID = BadgeID,
					@BadgeType = BadgeType,
					@DBPeopleID = dbPeopleID,
					@LandlordID = LandlordID,
					@BuildingID = BuildingID
				FROM
					  vw_BadgeRequest
				WHERE
					  RequestID = @RequestId;
			END
			
		END
		
		ELSE
		BEGIN
			UPDATE
                  [AccessRequest]
            SET
                  [Approved] = 0,
                  RequestStatusID = 10, --Task Rejected
                  [ApprovedDate] = @dtNow,
                  [ApprovedByID] = @CompletedById
            WHERE
                  RequestID = @RequestId;
		END
		
		exec up_UpdateTaskStatus_Common
		@RequestId,
		@TaskId,
		@TaskTypeId, 
		@TaskStatusId, 
		@CompletedById,
		@Comment		
		
		SET @Result = 1
	END
	ELSE
	BEGIN
		SET @Result = 0;
	END
				


GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_UpdateTaskStatus_TaskType16]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[up_UpdateTaskStatus_TaskType16]
END
GO

