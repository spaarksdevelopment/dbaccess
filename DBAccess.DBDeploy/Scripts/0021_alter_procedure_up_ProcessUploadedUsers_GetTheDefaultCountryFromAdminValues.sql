IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_ProcessUploadedUsers]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_ProcessUploadedUsers]
END
GO

/****** Object:  StoredProcedure [dbo].[up_ProcessUploadedUsers]    Script Date: 5/18/2015 3:09:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<AC>
-- Create date: <25/02/2011>
-- Description:	Gets all uploaded users for addition to the UI grid.
-- =============================================
CREATE PROCEDURE [dbo].[up_ProcessUploadedUsers]
	@dbPeopleID int,
	@Users nvarchar(max),
	@MasterId int,
	@RequestType int,
	@RequestMasterId int out
AS
BEGIN

BEGIN TRAN

DECLARE @NewMasterID int

--If we need to generate a new master, go do it...
IF (@MasterId = 0)
BEGIN
EXEC dbo.up_InsertRequestMaster @dbPeopleID, @NewMasterID OUTPUT
END
ELSE
BEGIN
SELECT @NewMasterID = @MasterId
END

INSERT INTO dbo.AccessRequestPerson
(
			   [dbPeopleID]
			   ,[IsExternal]
			   ,[EmailAddress]
			   ,[Forename]
			   ,[Surname]
			   ,[Telephone]
			   ,[CostCentre]
			   ,[VendorID]
			   ,[VendorName]
			   ,[RequestMasterID]
			   ,[DivisionID]
			   ,[BusinessAreaID]
			   ,[CreatedByID]
			   ,[Created]
)
SELECT distinct
				m.[dbPeopleID]
				,m.[IsExternal]
				,m.[EmailAddress]
				,m.[Forename]
				,m.[Surname]
				,m.[Telephone]
				,m.[CostCentre]
				,m.[VendorId]
			,m.[VendorName]
			  ,@NewMasterID
			  ,NULL
			  ,NULL
			  ,@dbPeopleID
			  ,GETDATE()

	FROM HRPerson AS m
	   JOIN dbo.Split( ',', @Users ) AS l
		  ON m.[dbPeopleID] = l.[Value]
	 
	 WHERE NOT EXISTS(SELECT RequestPersonID  FROM AccessRequestPerson t2  WHERE t2.RequestMasterID = @NewMasterID
	 and t2.dbPeopleID = l.[Value])
	 
	 --loop through the users and update the country id, business area id and divisionid
	 
	 DECLARE @IN_COUNT int = 0
	 SET @IN_COUNT = (SELECT COUNT(*) FROM  Split (',', @Users) As UBRPath)
--SELECT @IN_COUNT
	CREATE TABLE #TempFirst                                                       
	( 
					  RowID int,
					  dbPeopleID int
	)

	INSERT INTO #TempFirst(RowID,dbPeopleID)
	(
		SELECT * FROM [dbo].[Split] (',',@Users)
	)

--SELECT * FROM #TempFirst

	DECLARE @IN_dbPeopleID int
	DECLARE @IN_Counter int = 0
	While(@IN_Counter < @IN_COUNT)
	BEGIN
		  SET @IN_Counter = @IN_Counter + 1
		  SET @IN_dbPeopleID = (SELECT dbPeopleID FROM #TempFirst where RowID = @IN_Counter)
	      
	      
	 
	 
				 -- we need to calculate the person's Country ID from the name in the HRPerson table
					Declare @iCountryID Int;

					Set @iCountryID =
					(
						Select Top 1
							CountryID
						From
							HRPerson
						Inner Join
							LocationCountry
							On
							LocationCountry.Name = HRPerson.CountryName
						Where
							HRPerson.dbPeopleID = @IN_dbPeopleID
							And
							LocationCountry.[Enabled] = 1
					);

					Set @iCountryID = coalesce(@iCountryID, (select top 1 Value from AdminValues where [Key]='DefaultCountry'), 58); --Set the default country from Admin Values or take the US (id: 58) 
					
					
					-- we need to calculate the Business Area for this person for this Country
					-- work our way up the UBR path until we find the first one that is Enabled (therefore has approvers)

					Declare @sUBR NVarChar(6);
					Declare @sUBRPath NVarChar(100) = (Select DivPath From HRPerson Where dbPeopleID = @IN_dbPeopleID);

					-- split out the UBRs and find an enabled business area that we can assign to

					Declare @iDivisionID Int;
					Declare @iBusinessAreaID Int;

					With wUBRs As
					(
						Select
							UBRPath.RowID,
							UBRPath.Value As UBR,
							CorporateBusinessArea.DivisionID,
							CorporateBusinessArea.BusinessAreaID
						From
							dbo.Split ('/', @sUBRPath) As UBRPath
						Inner Join
							CorporateBusinessArea
							On
							CorporateBusinessArea.UBR = UBRPath.Value
						Inner Join
							CorporateDivision
							On
							CorporateDivision.DivisionID = CorporateBusinessArea.DivisionID
						Where
							0 != Len (RTrim (LTrim (UBRPath.Value)))		-- empty field
							And
							CorporateDivision.CountryID = @iCountryID
							And
							CorporateDivision.[Enabled] = 1
							And
							CorporateBusinessArea.[Enabled] = 1
					)
					Select Top 1
						@sUBR = UBR,
						@iDivisionID = DivisionID,
						@iBusinessAreaID = BusinessAreaID
					From
						wUBRs
					Order By
						RowID Desc;
						
						
					If (@iBusinessAreaID Is Not Null)
					Begin

						Update
							AccessRequestPerson
						Set
							 UBR = @sUBR
							,CountryID = @iCountryID
							,DivisionID = @iDivisionID
							,BusinessAreaID = @iBusinessAreaID
						Where
							 RequestMasterID = @NewMasterID
						AND  dbPeopleID = @IN_dbPeopleID
							--RequestPersonID = @RequestPersonID;

					End
		
			END
	 DROP table #TempFirst
	 
	 SELECT @RequestMasterId = @NewMasterID
	      
	      
	-- Now insert into AccessRequest table only if Request type =1(for new pass Request)
	--This below insert into access request is applicable only new pass request
	--The same insert for new Access request is implemented in a different stored proc
	--This is to avoid changing vw_Badgerequests as this view pulls the data onto thes screen and this is 
	--used in several places in the application
	
	IF @RequestType = 1
	BEgin     
	INSERT INTO dbo.AccessRequest
	(
		RequestTypeID,RequestPersonID,BadgeID,RequestStatusID,CreatedByID,Created
	)
	SELECT distinct @RequestType,arp.RequestPersonID,m.PersonBadgeID,1,@dbPeopleID,GETDATE()

	FROM vw_HRPersonBadge AS m
	
		RIGHT JOIN dbo.Split( ',', @Users ) AS l
		  ON m.[dbPeopleID] = l.[Value]
		  
		LEFT JOIN AccessRequestPerson arp
		  ON l.[Value] = arp.[dbPeopleID]
		  
		WHERE RequestMasterID = @NewMasterID
		  
		AND NOT EXISTS(SELECT RequestPersonID  FROM AccessRequest t2  WHERE t2.RequestPersonID = arp.RequestPersonID)
END
	IF @@ERROR>0
	BEGIN
		SELECT @RequestMasterId = -1
		ROLLBACK TRAN
	END
	ELSE
		COMMIT TRAN
END

--GRANT EXECUTE ON up_ProcessUploadedUsers TO dbAccessUser


GO

--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_ProcessUploadedUsers]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[up_ProcessUploadedUsers]
END
GO
/****** Object:  StoredProcedure [dbo].[up_ProcessUploadedUsers]    Script Date: 5/18/2015 3:10:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<AC>
-- Create date: <25/02/2011>
-- Description:	Gets all uploaded users for addition to the UI grid.
-- =============================================
CREATE PROCEDURE [dbo].[up_ProcessUploadedUsers]
	@dbPeopleID int,
	@Users nvarchar(max),
	@MasterId int,
	@RequestType int,
	@RequestMasterId int out
AS
BEGIN

BEGIN TRAN

DECLARE @NewMasterID int

--If we need to generate a new master, go do it...
IF (@MasterId = 0)
BEGIN
EXEC dbo.up_InsertRequestMaster @dbPeopleID, @NewMasterID OUTPUT
END
ELSE
BEGIN
SELECT @NewMasterID = @MasterId
END

INSERT INTO dbo.AccessRequestPerson
(
			   [dbPeopleID]
			   ,[IsExternal]
			   ,[EmailAddress]
			   ,[Forename]
			   ,[Surname]
			   ,[Telephone]
			   ,[CostCentre]
			   ,[VendorID]
			   ,[VendorName]
			   ,[RequestMasterID]
			   ,[DivisionID]
			   ,[BusinessAreaID]
			   ,[CreatedByID]
			   ,[Created]
)
SELECT distinct
				m.[dbPeopleID]
				,m.[IsExternal]
				,m.[EmailAddress]
				,m.[Forename]
				,m.[Surname]
				,m.[Telephone]
				,m.[CostCentre]
				,m.[VendorId]
			,m.[VendorName]
			  ,@NewMasterID
			  ,NULL
			  ,NULL
			  ,@dbPeopleID
			  ,GETDATE()

	FROM HRPerson AS m
	   JOIN dbo.Split( ',', @Users ) AS l
		  ON m.[dbPeopleID] = l.[Value]
	 
	 WHERE NOT EXISTS(SELECT RequestPersonID  FROM AccessRequestPerson t2  WHERE t2.RequestMasterID = @NewMasterID
	 and t2.dbPeopleID = l.[Value])
	 
	 --loop through the users and update the country id, business area id and divisionid
	 
	 DECLARE @IN_COUNT int = 0
	 SET @IN_COUNT = (SELECT COUNT(*) FROM  Split (',', @Users) As UBRPath)
--SELECT @IN_COUNT
	CREATE TABLE #TempFirst                                                       
	( 
					  RowID int,
					  dbPeopleID int
	)

	INSERT INTO #TempFirst(RowID,dbPeopleID)
	(
		SELECT * FROM [dbo].[Split] (',',@Users)
	)

--SELECT * FROM #TempFirst

	DECLARE @IN_dbPeopleID int
	DECLARE @IN_Counter int = 0
	While(@IN_Counter < @IN_COUNT)
	BEGIN
		  SET @IN_Counter = @IN_Counter + 1
		  SET @IN_dbPeopleID = (SELECT dbPeopleID FROM #TempFirst where RowID = @IN_Counter)
	      
	      
	 
	 
				 -- we need to calculate the person's Country ID from the name in the HRPerson table
					Declare @iCountryID Int;

					Set @iCountryID =
					(
						Select Top 1
							CountryID
						From
							HRPerson
						Inner Join
							LocationCountry
							On
							LocationCountry.Name = HRPerson.CountryName
						Where
							HRPerson.dbPeopleID = @IN_dbPeopleID
							And
							LocationCountry.[Enabled] = 1
					);

					Set @iCountryID = IsNull (@iCountryID, 58);		-- set to UK for now if not available
					
					
					-- we need to calculate the Business Area for this person for this Country
					-- work our way up the UBR path until we find the first one that is Enabled (therefore has approvers)

					Declare @sUBR NVarChar(6);
					Declare @sUBRPath NVarChar(100) = (Select DivPath From HRPerson Where dbPeopleID = @IN_dbPeopleID);

					-- split out the UBRs and find an enabled business area that we can assign to

					Declare @iDivisionID Int;
					Declare @iBusinessAreaID Int;

					With wUBRs As
					(
						Select
							UBRPath.RowID,
							UBRPath.Value As UBR,
							CorporateBusinessArea.DivisionID,
							CorporateBusinessArea.BusinessAreaID
						From
							dbo.Split ('/', @sUBRPath) As UBRPath
						Inner Join
							CorporateBusinessArea
							On
							CorporateBusinessArea.UBR = UBRPath.Value
						Inner Join
							CorporateDivision
							On
							CorporateDivision.DivisionID = CorporateBusinessArea.DivisionID
						Where
							0 != Len (RTrim (LTrim (UBRPath.Value)))		-- empty field
							And
							CorporateDivision.CountryID = @iCountryID
							And
							CorporateDivision.[Enabled] = 1
							And
							CorporateBusinessArea.[Enabled] = 1
					)
					Select Top 1
						@sUBR = UBR,
						@iDivisionID = DivisionID,
						@iBusinessAreaID = BusinessAreaID
					From
						wUBRs
					Order By
						RowID Desc;
						
						
					If (@iBusinessAreaID Is Not Null)
					Begin

						Update
							AccessRequestPerson
						Set
							 UBR = @sUBR
							,CountryID = @iCountryID
							,DivisionID = @iDivisionID
							,BusinessAreaID = @iBusinessAreaID
						Where
							 RequestMasterID = @NewMasterID
						AND  dbPeopleID = @IN_dbPeopleID
							--RequestPersonID = @RequestPersonID;

					End
		
			END
	 DROP table #TempFirst
	 
	 SELECT @RequestMasterId = @NewMasterID
	      
	      
	-- Now insert into AccessRequest table only if Request type =1(for new pass Request)
	--This below insert into access request is applicable only new pass request
	--The same insert for new Access request is implemented in a different stored proc
	--This is to avoid changing vw_Badgerequests as this view pulls the data onto thes screen and this is 
	--used in several places in the application
	
	IF @RequestType = 1
	BEgin     
	INSERT INTO dbo.AccessRequest
	(
		RequestTypeID,RequestPersonID,BadgeID,RequestStatusID,CreatedByID,Created
	)
	SELECT distinct @RequestType,arp.RequestPersonID,m.PersonBadgeID,1,@dbPeopleID,GETDATE()

	FROM vw_HRPersonBadge AS m
	
		RIGHT JOIN dbo.Split( ',', @Users ) AS l
		  ON m.[dbPeopleID] = l.[Value]
		  
		LEFT JOIN AccessRequestPerson arp
		  ON l.[Value] = arp.[dbPeopleID]
		  
		WHERE RequestMasterID = @NewMasterID
		  
		AND NOT EXISTS(SELECT RequestPersonID  FROM AccessRequest t2  WHERE t2.RequestPersonID = arp.RequestPersonID)
END
	IF @@ERROR>0
	BEGIN
		SELECT @RequestMasterId = -1
		ROLLBACK TRAN
	END
	ELSE
		COMMIT TRAN
END

--GRANT EXECUTE ON up_ProcessUploadedUsers TO dbAccessUser


GO
