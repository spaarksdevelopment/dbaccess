IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'CATDeleteOffboard'))
BEGIN

/****** Object:  Table [dbo].[CATDeleteOffboard]    Script Date: 10/18/2015 10:17:07 AM ******/

CREATE TABLE [dbo].[CATDeleteOffboard](
	[CATDeleteOffboardID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DBPeopleID] [int] NOT NULL,
	[Processed] [bit] NOT NULL,
	[ProcessedDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CATDeleteOffboard] PRIMARY KEY CLUSTERED 
(
	[CATDeleteOffboardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
	

END

GO

--//@UNDO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'CATDeleteOffboard'))
BEGIN

DROP TABLE CATDeleteOffboard

END



