/****** Object:  UserDefinedFunction [dbo].[GetEmployeeClass]    Script Date: 21/04/2015 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeClass]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
DROP FUNCTION [dbo].[GetEmployeeClass]
'
END

GO

--//@UNDO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeClass]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[GetEmployeeClass]
(
    @ORGANIZATIONAL_RELATIONSHIP NVARCHAR(3),
    @EMPLOYEE_CLASS NVARCHAR(3)
)
RETURNS VARCHAR(255)
AS
BEGIN
RETURN CASE WHEN [dbo].[IsEmployeeExternal](@ORGANIZATIONAL_RELATIONSHIP)=1 THEN ''Class_''+ @EMPLOYEE_CLASS ELSE NULL END;
END
' 
END

GO