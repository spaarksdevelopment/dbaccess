/****** Object:  StoredProcedure [dbo].[PassOfficeUserOffBoarding]    Script Date: 21/04/2015 12:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PassOfficeUserOffBoarding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




ALTER PROCEDURE [dbo].[PassOfficeUserOffBoarding] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 

--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int
declare @success bit

--find out the person''s home pass office
declare @homePassOfficeID int
declare @OfficerID varchar(2)

--first, make sure the OfficerID is available here, so it can be used later
select @OfficerID = OfficerID 
from HRPerson 
where dbpeopleid=@dbPeopleId


--commented out by BH 21/1/14, as PassOfficeID is not in AccessRequestPerson, have replaced this code below
--MCS - rolled back this change, as it''s not correct





--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
	     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

		select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
            inner join LocationCountry lc on lc.CountryID = arp.CountryID
		  where arp.RequestPersonID = @RequestPersonID

		exec dbo.up_InsertRequest
			@CreatedByUserID = @CreatedByID,
			@RequestTypeID = 15,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = null,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
		
		exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output
			
		fetch next from badgesCursor into @badgeId
	end
end
else
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

      	select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
            inner join LocationCountry lc on lc.CountryID = arp.CountryID
		  where arp.RequestPersonID = @RequestPersonID

	--we still want to send an offboarding request even if there are no badges
	--create it with null for BadgeID
	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @homePassOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
    
    	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output
end

close badgesCursor

if (@OfficerID is null) or (@OfficerID<>''D'' and @OfficerID<>''MD'')
begin
--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for
	select distinct c.PassOfficeID from HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		where (p.EndDate is null or p.EndDate > getdate())
			and c.PassOfficeID <> @homePassOfficeID
			and p.dbPeopleId = @dbPeopleId

declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @passOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
	
	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

	fetch next from passOfficeCursor into @passOfficeID
end

close passOfficeCursor
end

else
begin
--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for

	select PassOfficeID from LocationPassOffice l
		where l.Enabled=1
			
--declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @passOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
	
	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

	fetch next from passOfficeCursor into @passOfficeID
end

close passOfficeCursor
end




--submit the request



--delete the person''s requests
exec dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @HasBadges

end




' 
END
GO



--//@UNDO
ALTER PROCEDURE [dbo].[PassOfficeUserOffBoarding] 
          @dbPeopleId int, 
          @Valid bit, 
          @CreatedByID int 
as 
begin 

--create request master / person
declare @RequestMasterID int
declare @RequestPersonID int
declare @RequestID int
declare @success bit

--find out the person's home pass office
declare @homePassOfficeID int
declare @OfficerID varchar(2)

--first, make sure the OfficerID is available here, so it can be used later
select @OfficerID = OfficerID from HRPerson hr1
inner join AccessRequestPerson arp2 on hr1.dbPeopleID=arp2.dbPeopleID
where arp2.RequestPersonID = @RequestPersonID


--commented out by BH 21/1/14, as PassOfficeID is not in AccessRequestPerson, have replaced this code below
--MCS - rolled back this change, as it's not correct
select @homePassOfficeID = PassOfficeID from AccessRequestPerson arp
inner join LocationCountry lc on lc.CountryID = arp.CountryID
where arp.RequestPersonID = @RequestPersonID

----code to replace the above BH 21/2/14
--select @homePassOfficeID = ar.PassOfficeID from AccessRequest ar
--inner join AccessRequestPerson arp on ar.RequestPersonID=arp.RequestPersonID
--inner join LocationCountry lc on lc.CountryID = arp.CountryID
--where arp.RequestPersonID = @RequestPersonID




--set up a cursor to iterate over the badges
declare badgesCursor cursor local fast_forward for
SELECT PersonBadgeID 
FROM HRPersonBadge
WHERE dbPeopleID = @dbPeopleId and Valid = @Valid

declare @badgeId int
open badgesCursor
fetch next from badgesCursor into @badgeId

--check if there are any rows
declare @HasBadges bit
set @HasBadges = case @@FETCH_STATUS when 0 then 1 else 0 end

if @HasBadges = 1
begin
	--create a request for each badge
	while (@@FETCH_STATUS = 0)
	begin
		exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
	     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

		exec dbo.up_InsertRequest
			@CreatedByUserID = @CreatedByID,
			@RequestTypeID = 15,
			@AccessAreaID = null,
			@RequestPersonID = @RequestPersonID,
			@StartDate = null,
			@EndDate = null,
			@BadgeID = @badgeId,
			@DivisionID = null,
			@iPassOfficeID = @homePassOfficeID,
			@BusinessAreaID = null,
			@RequestID = @RequestID output,
			@ExternalSystemRequestID = null,
			@ExternalSystemID = null,
			@ClassificationID = null
		
		exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output
			
		fetch next from badgesCursor into @badgeId
	end
end
else
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

	--we still want to send an offboarding request even if there are no badges
	--create it with null for BadgeID
	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @homePassOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
    
    	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output
end

close badgesCursor

if (@OfficerID is null) or (@OfficerID<>'D' and @OfficerID<>'MD')
begin
--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for
	select distinct c.PassOfficeID from HRPersonAccessArea p
		inner join AccessArea aa on aa.AccessAreaID = p.AccessAreaID
		inner join LocationBuilding lb on lb.BuildingID = aa.BuildingID
		inner join LocationCity lc on lc.CityID = lb.CityID
		inner join LocationCountry c on c.CountryID = lc.CountryID
		where (p.EndDate is null or p.EndDate > getdate())
			and c.PassOfficeID <> @homePassOfficeID
			and p.dbPeopleId = @dbPeopleId

declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @passOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
	
	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

	fetch next from passOfficeCursor into @passOfficeID
end

close passOfficeCursor
end

else
begin
--make a cursor to iterate through the pass offices for the areas that the person has access to
--(besides their home pass office)
declare passOfficeCursor cursor local fast_forward for

	select PassOfficeID from LocationPassOffice l
		where l.Enabled=1
			
--declare @passOfficeID int
open passOfficeCursor
fetch next from passOfficeCursor into @passOfficeID

--make requests for the pass offices
while (@@FETCH_STATUS = 0)
begin
     exec [dbo].[up_InsertRequestMaster] @CreatedByID, @RequestMasterID output
     exec [dbo].[up_InsertRequestPerson] @CreatedByID, @RequestMasterID, @dbPeopleId, @RequestPersonID output, 0

	exec dbo.up_InsertRequest
		@CreatedByUserID = @CreatedByID,
		@RequestTypeID = 15,
		@AccessAreaID = null,
		@RequestPersonID = @RequestPersonID,
		@StartDate = null,
		@EndDate = null,
		@BadgeID = null,
		@DivisionID = null,
		@iPassOfficeID = @passOfficeID,
		@BusinessAreaID = null,
		@RequestID = @RequestID output,
		@ExternalSystemRequestID = null,
		@ExternalSystemID = null,
		@ClassificationID = null
	
	exec up_SubmitRequestMaster @dbPeopleId, @RequestMasterID, @success output

	fetch next from passOfficeCursor into @passOfficeID
end

close passOfficeCursor
end




--submit the request



--delete the person's requests
exec dbo.DeactivatePassOfficeandAccessRequests @dbPeopleId, @Valid, @CreatedByID, @HasBadges

end




