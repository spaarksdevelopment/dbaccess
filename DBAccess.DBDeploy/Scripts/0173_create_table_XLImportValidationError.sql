IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportValidationError')
BEGIN

CREATE TABLE [dbo].[XLImportValidationError](
	[FileID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[ErrorType] [nvarchar](50) NOT NULL,
	[ErrorMessage] [nvarchar](500) NOT NULL,
	[RowNumbers] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



ALTER TABLE [dbo].[XLImportValidationError]  WITH CHECK ADD  CONSTRAINT [FK_XLImportValidationError_XLImportEntity] FOREIGN KEY([EntityID])
REFERENCES [dbo].[XLImportEntity] ([ID])


ALTER TABLE [dbo].[XLImportValidationError] CHECK CONSTRAINT [FK_XLImportValidationError_XLImportEntity]


ALTER TABLE [dbo].[XLImportValidationError]  WITH CHECK ADD  CONSTRAINT [FK_XLImportValidationError_XLImportFile] FOREIGN KEY([FileID])
REFERENCES [dbo].[XLImportFile] ([ID])


ALTER TABLE [dbo].[XLImportValidationError] CHECK CONSTRAINT [FK_XLImportValidationError_XLImportFile]


END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportValidationError')
BEGIN
	
	DROP TABLE [dbo].[XLImportValidationError]
	
END

GO