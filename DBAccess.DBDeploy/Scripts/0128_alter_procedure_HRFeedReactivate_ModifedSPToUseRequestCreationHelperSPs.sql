IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedReactivate]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedReactivate]
END
GO


/****** Object:  StoredProcedure [dbo].[HRFeedReactivate]    Script Date: 11/11/2015 6:21:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--		 15/10/2015 Asanka - Changed the logic to reactive aceess only if reactivation raised within a week after revocation.
--		 22/10/2015 Asanka - Fixed issue of not sending emails to user's home country's pass officers, when the default pass office is outside of home country
--		 11/11/2015 Wijitha - Modified SP to user generic helper methods for request creation and email sending
--======================================================================

CREATE PROCEDURE [dbo].[HRFeedReactivate]
AS

DECLARE @TransactionID int
DECLARE @HRID nvarchar(11)

DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
SELECT TransactionId, HRID  FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, 'C', '') as int)=hp.dbpeopleid
WHERE ReactivateProcessed = 0 AND TriggerType = 'JOIN'
AND ReactivationFlag = 'Y' AND
(hp.Suspended=0 or hp.Suspended is NULL)

OPEN myCursor

FETCH NEXT FROM myCursor INTO @TransactionId, @HRID

WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @ReplacedHRID nvarchar(11)
DECLARE @dbPeopleIdConvert int
DECLARE @DaysSinceRevoked int
DECLARE @RevokedDate datetime

SELECT @ReplacedHRID = REPLACE(@HRID, 'C', '')
SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
SELECT @RevokedDate = MAX(AccessRequest.Created) 
FROM AccessRequest 
	INNER JOIN AccessRequestPerson ON AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID 
WHERE AccessRequest.RequestTypeID = 16 AND AccessRequestPerson.dbPeopleID = @dbPeopleIdConvert

SET @DaysSinceRevoked = DATEDIFF(d, @RevokedDate, GETDATE())

IF(@DaysSinceRevoked > 7 AND @DaysSinceRevoked <= 30)
BEGIN

	UPDATE HRPerson SET Revoked = NULL WHERE dbPeopleID = @dbPeopleIdConvert --make person searchable 

END
ELSE IF(@DaysSinceRevoked <= 7)
BEGIN 

	exec HRFeed_ReactivateAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL

END

UPDATE HRFeedMessage SET ReactivateProcessed = 1 , ReactivateProcessedDate = GETDATE() WHERE TransactionId = @TransactionId

FETCH NEXT FROM myCursor INTO @TransactionId, @HRID
END

CLOSE myCursor
DEALLOCATE myCursor


GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HRFeedReactivate]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[HRFeedReactivate]
END
GO

/****** Object:  StoredProcedure [dbo].[HRFeedReactivate]    Script Date: 11/11/2015 4:15:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- History:
--		 27/05/2015 Wijitha - Modifed the logic of retrieving pass offices considering builing pass offices and based on the Email configuration of pass offices
--		 15/10/2015 Asanka - Changed the logic to reactive aceess only if reactivation raised within a week after revocation.
--		 22/10/2015 Asanka - Fixed issue of not sending emails to user's home country's pass officers, when the default pass office is outside of home country
--======================================================================

CREATE PROCEDURE [dbo].[HRFeedReactivate]
AS

DECLARE @TransactionID int
DECLARE @HRID nvarchar(11)

DECLARE myCursor CURSOR FAST_FORWARD READ_ONLY FOR
SELECT TransactionId, HRID  FROM HRFeedMessage hrm JOIN HRPerson hp
 ON CAST(REPLACE(hrm.HRID, 'C', '') as int)=hp.dbpeopleid
WHERE ReactivateProcessed = 0 AND TriggerType = 'JOIN'
AND ReactivationFlag = 'Y' AND
(hp.Suspended=0 or hp.Suspended is NULL)

OPEN myCursor

FETCH NEXT FROM myCursor INTO @TransactionId, @HRID

WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @ReplacedHRID nvarchar(11)
DECLARE @dbPeopleIdConvert int
DECLARE @DaysSinceRevoked int
DECLARE @RevokedDate datetime

SELECT @ReplacedHRID = REPLACE(@HRID, 'C', '')
SELECT @dbPeopleIdConvert = CAST(@ReplacedHRID as int)
 
SELECT @RevokedDate = MAX(AccessRequest.Created) 
FROM AccessRequest 
	INNER JOIN AccessRequestPerson ON AccessRequest.RequestPersonID = AccessRequestPerson.RequestPersonID 
WHERE AccessRequest.RequestTypeID = 16 AND AccessRequestPerson.dbPeopleID = @dbPeopleIdConvert

SET @DaysSinceRevoked = DATEDIFF(d, @RevokedDate, GETDATE())

IF(@DaysSinceRevoked > 7 AND @DaysSinceRevoked <= 30)
BEGIN
	UPDATE HRPerson SET Revoked = NULL WHERE dbPeopleID = @dbPeopleIdConvert --make person searchable 
END

ELSE IF(@DaysSinceRevoked <= 7)
BEGIN 

exec HRFeed_ReactivateAccessForUser @dbPeopleId = @dbPeopleIdConvert, @Valid = 1, @CreatedByID  = NULL

--TABLE TO STORE Recipients
		DECLARE @PassOfficeMembers TABLE
		(
			RowNumber int,
			PassOfficeUserEmail nvarchar(200),
			PassOfficeUserName nvarchar(200)
		)

		DELETE FROM @PassOfficeMembers

		DECLARE @UserCountryID int
		SET @UserCountryID = 59 --Default to UK, in case person's country cannot be found

		SELECT @UserCountryID=CountryID
		FROM HRPerson Inner Join LocationCountry On LocationCountry.Name = HRPerson.CountryName 
		WHERE dbPeopleID=@dbPeopleIdConvert
		
		DECLARE @OfficerID varchar(2)
		select @OfficerID = OfficerID from HRPerson
		where HRPerson.dbPeopleID= @dbPeopleIdConvert
		
		if (@OfficerID='D' or @OfficerID='MD')
		begin
		INSERT INTO @PassOfficeMembers
				SELECT ROW_NUMBER() OVER (ORDER BY hr2.dbPeopleID) AS RowNumber, 
					hr2.EmailAddress AS PassOfficeUserEmail, 
					hr2.Forename + ' ' + hr2.Surname AS PassOfficeUserName
				FROM LocationPassOffice 
					INNER JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
					JOIN HRPerson as hr2 ON LocationPassOfficeUser.dbPeopleID=hr2.dbPeopleID
				where LocationPassOffice.Enabled = 1
					AND LocationPassOfficeUser.IsEnabled = 1
					AND LocationPassOffice.EmailEnabled = 1
		end
		else
		begin
		INSERT INTO @PassOfficeMembers
			 SELECT ROW_NUMBER() OVER (ORDER BY HRPerson.dbPeopleID) AS RowNumber, 
				HRPerson.EmailAddress AS PassOfficeUserEmail, 
				HRPerson.Forename + ' ' + HRPerson.Surname AS PassOfficeUserName
			 FROM LocationPassOffice
				JOIN LocationPassOfficeUser ON LocationPassOffice.PassOfficeID = LocationPassOfficeUser.PassOfficeID
				JOIN HRPerson ON LocationPassOfficeUser.dbPeopleID=HRPerson.dbPeopleID
			 WHERE LocationPassOffice.Enabled = 1  
				AND LocationPassOffice.EmailEnabled = 1
				AND LocationPassOfficeUser.IsEnabled = 1 
				AND LocationPassOffice.PassOfficeID in 
			 	(
				    SELECT distinct PassOfficeID from LocationPassOffice where CountryID = @UserCountryID --Get all pass offices in the user's home country
				    UNION
					SELECT PassOfficeID from LocationCountry where CountryID = @UserCountryID --Get user's home country's default pass office
					UNION
				    SELECT IsNUll(bpo.PassOfficeID, c.PassOfficeID) as PassOfficeID --Get pass offices of buildings where user has access to outside of their home country
				    FROM HRPersonAccessArea p
					   JOIN AccessArea aa on aa.AccessAreaID = p.AccessAreaID
					   JOIN LocationBuilding lb on lb.BuildingID = aa.BuildingID
					   JOIN LocationCity lc on lc.CityID = lb.CityID
					   JOIN LocationCountry c on c.CountryID = lc.CountryID
					   LEFT OUTER JOIN LocationPassOffice bpo on bpo.PassOfficeID = lb.PassOfficeID AND bpo.[Enabled] = 1
				    WHERE (p.EndDate IS NULL OR p.EndDate > getdate())
					   AND p.dbPeopleId = @dbPeopleIdConvert AND c.CountryID <> @UserCountryID
				)					
		end


		DECLARE @EmailForename nvarchar(255)
		DECLARE @EmailSurname nvarchar(255)
        SELECT @EmailForename = FirstName , @EmailSurname = LastName FROM HRFeedMessage WHERE TransactionId = @TransactionId

		DECLARE @HttpRoot nvarchar(100)
		SELECT @HttpRoot=Value
		FROM AdminValues
		WHERE [Key]='HttpRoot'

		--Loop through each member of the pass office
		Declare @NumberofPassOfficePeople int
		Select @NumberofPassOfficePeople = Count(*) From @PassOfficeMembers
		DECLARE @loopCounter INT
		SET @loopCounter = 1

		-- Table to allow us to not send duplicate emails
		DECLARE @EmailSentTo TABLE
		(
			PassOfficeUserEmail nvarchar(200)
		)

		DELETE FROM @EmailSentTo

		WHILE (@loopCounter <= @NumberofPassOfficePeople)
		BEGIN
		 	DECLARE @To nvarchar(255)
			DECLARE @ToName nvarchar(255)
			DECLARE @Subject nvarchar(255)
			DECLARE @Body nvarchar(1000)
			DECLARE @Guid nvarchar(255)
			SELECT @Subject = [Subject] , @Body = [Body] FROM EmailTemplates WHERE Type = 38


			SELECT @To=PassOfficeUserEmail, @ToName=PassOfficeUserName
			FROM @PassOfficeMembers
			WHERE RowNumber = @loopCounter
			
			DECLARE @BatchGuid uniqueidentifier
			SET @BatchGuid = NEWID()
	
			IF (@To <> '' AND @To IS NOT NULL AND NOT EXISTS(SELECT PassOfficeUserEmail FROM @EmailSentTo WHERE PassOfficeUserEmail = @To))
			BEGIN 
					SET @Body = REPLACE ( @Body , '[$$UserEmail$$]' , @EmailForename + ' ' + @EmailSurname ); 
					SET @Body = REPLACE ( @Body , '[$$HTTP_ROOT$$]' , @HttpRoot ); 
					SET @Body = REPLACE ( @Body , '[$$NAME$$]' , @ToName ); 
					SET @Body = REPLACE ( @Body , '[$$FILE_PATH$$]' , 'Pages/MyTasks.aspx' ); 

					EXEC [up_InsertEmailLog] 
							@To 
							, 'dbAccess_DO_NOT_REPLY@db.com' 
							, 'ryan.sheehan@db.com'
							, @Subject 
							, @Body 
							, 0 
							, @BatchGuid
							, 38 
							, NULL 
			END 

			INSERT INTO @EmailSentTo SELECT @To			

			SET @loopCounter = @loopCounter + 1
		END
END


UPDATE HRFeedMessage SET ReactivateProcessed = 1 , ReactivateProcessedDate = GETDATE() WHERE TransactionId	=   @TransactionId

FETCH NEXT FROM myCursor INTO @TransactionId, @HRID
END

CLOSE myCursor
DEALLOCATE myCursor


GO


