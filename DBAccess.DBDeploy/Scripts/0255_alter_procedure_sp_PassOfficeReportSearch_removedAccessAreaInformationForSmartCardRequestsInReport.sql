IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PassOfficeReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_PassOfficeReportSearch]
END
GO

/****** Object:  StoredProcedure [dbo].[sp_PassOfficeReportSearch]    Script Date: 09/03/17 18:14:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================
-- History:
-- 09/03/2017 Adheeb - Modified SP to show details about smart card requests

--======================================================================

CREATE PROCEDURE [dbo].[sp_PassOfficeReportSearch]
(
	@LanguageID INT=1,
	@TaskCreatedStartDate datetime2 = NULL,
	@TaskCreatedEndDate datetime2 = NULL,
	@TaskCompletedStartDate datetime2 = NULL,
    @TaskCompletedEndDate datetime2 = NULL,
    
    @PersonNameSearchType nvarchar(50) = 'Contains',
	@PersonNameValueStart nvarchar(150) = '',
	@PersonNameValueEnd nvarchar(150) = '',
	@PersonEmpIdSearchType nvarchar(50) = 'Contains',
	@PersonEmpIdValueStart nvarchar(150) = '',
	@PersonEmpIdValueEnd nvarchar(150) = '',
	@PersonEmailSearchType nvarchar(50) = 'Contains',
	@PersonEmailValueStart nvarchar(150) = '',
	@PersonEmailalueEnd nvarchar(150) = '',
	@PersonDbDirIdSearchType nvarchar(50) = 'Contains',
	@PersonDbDirIdValueStart nvarchar(150) = '',
	@PersonDbDirIdValueEnd nvarchar(150) = '',
	
	---requestor
	@RequestorNameSearchType nvarchar(50) = 'Contains'	,
	@RequestorNameValueStart nvarchar(150) = '',
	@RequestorNameValueEnd nvarchar(150) = '',	
	@RequestorEmpIdSearchType nvarchar(50) = 'Contains',
	@RequestorEmpIdValueStart nvarchar(150) = '',
	@RequestorEmpIdValueEnd nvarchar(150) = '',
	@RequestorEmailSearchType nvarchar(50) = 'Contains',
	@RequestorEmailValueStart nvarchar(150) = '',
	@RequestorEmailalueEnd nvarchar(150) = '',
	@RequestorDbDirIdSearchType nvarchar(50) = 'Contains',
	@RequestorDbDirIdValueStart nvarchar(150) = '',
	@RequestorDbDirIdValueEnd nvarchar(150) = '',
	
	@countryID int = NULL , 
	@cityID int = NULL ,
	@buildingID int = NULL ,
	@floorID int = NULL ,
	@RequestTypeID int = NULL ,
	@RequestStatusId int = NULL ,
	@PassOffice nvarchar(250) = '',	
	@UserdbPeopleID INT,
	@RequestStatusFullId int = NULL,
	@AccessAreaId int = NULL
)

AS
BEGIN
SET NOCOUNT ON

  IF(@TaskCreatedEndDate IS NOT NULL)
  BEGIN
	SET @TaskCreatedEndDate = DATEADD(second, 86399, @TaskCreatedEndDate);
  END
  
  DECLARE @IsUserAdmin bit
  SET @IsUserAdmin=0
  
  IF EXISTS(SELECT 1 FROM dbo.mp_SecurityGroupUser WHERE SecurityGroupId=2 and dbPeopleID=@UserdbPeopleID)
  BEGIN 
	SET @IsUserAdmin=1
  END
  
  
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' '+ HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator
        
  ,CASE WHEN AccessRequest.IsSmartCard = 1 THEN NULL
		ELSE vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName END AS AccessArea,
  CASE
	WHEN AccessRequest.IsSmartCard IS NULL AND Tasks.TaskTypeId = 1 THEN 'Standard' 
	WHEN  AccessRequest.IsSmartCard = 1 AND Tasks.TaskTypeId = 1 THEN 'Smart' 
	END AS CardType
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON  AccessRequest.CreatedByID=HR2.dbPeopleId
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
WHERE
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
  AND
( (@TaskCompletedStartDate IS NULL) OR (@TaskCompletedStartDate IS NOT NULL AND CompletedDate >= CAST(@TaskCompletedStartDate AS DATE)))
AND
( (@TaskCompletedEndDate IS NULL) OR (@TaskCompletedEndDate IS NOT NULL AND CompletedDate <= CAST(@TaskCompletedEndDate AS DATE)))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
---- requestor
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
--------
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))

AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))

AND 
( (@RequestStatusId IS NULL) OR (@RequestStatusId IS NOT NULL AND Tasks.TaskStatusID = @RequestStatusId))
  
AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@PassOffice = '') OR (@PassOffice <> '' AND 

CHARINDEX(
	',' + CAST(AccessRequest.PassOfficeId AS NVARCHAR(10)) + ',', 
	',' + @PassOffice + ','
	) > 0
))

AND
(
  @IsUserAdmin=1  OR (@IsUserAdmin=0 AND AccessRequest.PassOfficeID  IN (SELECT LU.PassOfficeID FROM dbo.LocationPassOfficeUser LU WHERE LU.dbPeopleID=@UserdbPeopleID AND LU.IsEnabled=1) ) 
)

--ORDER BY CreatedDate DESC
ORDER BY AccessRequest.RequestID,Tasks.TaskID DESC

END


GO



--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PassOfficeReportSearch]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[sp_PassOfficeReportSearch]
END
GO

/****** Object:  StoredProcedure [dbo].[sp_PassOfficeReportSearch]    Script Date: 09/03/17 17:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_PassOfficeReportSearch]
(
	@LanguageID INT=1,
	@TaskCreatedStartDate datetime2 = NULL,
	@TaskCreatedEndDate datetime2 = NULL,
	@TaskCompletedStartDate datetime2 = NULL,
    @TaskCompletedEndDate datetime2 = NULL,
    
    @PersonNameSearchType nvarchar(50) = 'Contains',
	@PersonNameValueStart nvarchar(150) = '',
	@PersonNameValueEnd nvarchar(150) = '',
	@PersonEmpIdSearchType nvarchar(50) = 'Contains',
	@PersonEmpIdValueStart nvarchar(150) = '',
	@PersonEmpIdValueEnd nvarchar(150) = '',
	@PersonEmailSearchType nvarchar(50) = 'Contains',
	@PersonEmailValueStart nvarchar(150) = '',
	@PersonEmailalueEnd nvarchar(150) = '',
	@PersonDbDirIdSearchType nvarchar(50) = 'Contains',
	@PersonDbDirIdValueStart nvarchar(150) = '',
	@PersonDbDirIdValueEnd nvarchar(150) = '',
	
	---requestor
	@RequestorNameSearchType nvarchar(50) = 'Contains'	,
	@RequestorNameValueStart nvarchar(150) = '',
	@RequestorNameValueEnd nvarchar(150) = '',	
	@RequestorEmpIdSearchType nvarchar(50) = 'Contains',
	@RequestorEmpIdValueStart nvarchar(150) = '',
	@RequestorEmpIdValueEnd nvarchar(150) = '',
	@RequestorEmailSearchType nvarchar(50) = 'Contains',
	@RequestorEmailValueStart nvarchar(150) = '',
	@RequestorEmailalueEnd nvarchar(150) = '',
	@RequestorDbDirIdSearchType nvarchar(50) = 'Contains',
	@RequestorDbDirIdValueStart nvarchar(150) = '',
	@RequestorDbDirIdValueEnd nvarchar(150) = '',
	
	@countryID int = NULL , 
	@cityID int = NULL ,
	@buildingID int = NULL ,
	@floorID int = NULL ,
	@RequestTypeID int = NULL ,
	@RequestStatusId int = NULL ,
	@PassOffice nvarchar(250) = '',	
	@UserdbPeopleID INT,
	@RequestStatusFullId int = NULL,
	@AccessAreaId int = NULL
)

AS
BEGIN
SET NOCOUNT ON

  IF(@TaskCreatedEndDate IS NOT NULL)
  BEGIN
	SET @TaskCreatedEndDate = DATEADD(second, 86399, @TaskCreatedEndDate);
  END
  
  DECLARE @IsUserAdmin bit
  SET @IsUserAdmin=0
  
  IF EXISTS(SELECT 1 FROM dbo.mp_SecurityGroupUser WHERE SecurityGroupId=2 and dbPeopleID=@UserdbPeopleID)
  BEGIN 
	SET @IsUserAdmin=1
  END
  
  
  SELECT   TOP 100 PERCENT
  AccessRequest.RequestID ,
  Tasks.TaskId,
  Tasks.CreatedDate,
  [dbo].[ComputeReportTaskStatus](AccessRequest.RequestTypeID, Tasks.TaskTypeId, TaskTypeLookup.Type) AS TaskType,
  HRPerson.Forename + ' '+ HRPerson.Surname  AS PersonName,
  HRPerson.dbPeopleID   AS PersonEmployeeId,
  HRPerson.EmailAddress AS PersonEmail,
  HRPerson.DBDirID AS PersonDBDirID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN 'HR' ELSE  HR2.Forename + ' '+ HR2.Surname END  AS RequestorName,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.dbPeopleId  END AS RequestorEmployeeID,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.EmailAddress END  AS RequestorEmail,
  CASE WHEN  AccessRequest.RequestTypeID=15 THEN Null ELSE HR2.DBDirID END AS RequestorDBDirID,
  Tasks.CompletedDate,
  CASE @LanguageID 
	  WHEN 1 THEN AccessRequestStatus.[Name] 
	  WHEN 2 THEN COALESCE(AccessRequestStatus.GermanName,AccessRequestStatus.[Name]) 
	  WHEN 3 THEN COALESCE(AccessRequestStatus.ItalyName,AccessRequestStatus.[Name]) 
	  ELSE AccessRequestStatus.[Name] 
  END AS TaskStatus,
  ISNULL(
  CASE
  WHEN NOT Tasks.TaskStatusId = 1 THEN HR3.Emailaddress
  WHEN Tasks.TaskStatusId = 1 AND NOT TaskTypeLookup.TaskTypeId IN (3) THEN (SELECT Name FROM  dbo.LocationPassOffice WHERE PassOfficeID=AccessRequest.PassOfficeID)
  WHEN Tasks.TaskStatusId = 1 AND TaskTypeLookup.TaskTypeId IN (3) THEN [dbo].[ComputeTaskApproverList](Tasks.TaskId)
  END, '') AS Operator
        
  , vw_AccessArea.RegionCode+'\' + vw_AccessArea.CityName+'\'+ ISNULL(vw_AccessArea.BuildingName,'') +'\'+ ISNULL(vw_AccessArea.FloorName,'')  +'\' + vw_AccessArea.AccessAreaName AccessArea,
  CASE
	WHEN AccessRequest.IsSmartCard IS NULL AND Tasks.TaskTypeId = 1 THEN 'Standard' 
	WHEN  AccessRequest.IsSmartCard = 1 AND Tasks.TaskTypeId = 1 THEN 'Smart' 
	END AS CardType
 
FROM
  (SELECT * FROM Tasks WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE))) UNION SELECT * FROM TasksArchive WHERE ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))) Tasks
  INNER JOIN dbo.TaskTypeLookup ON Tasks.TaskTypeId=TaskTypeLookup.TaskTypeId  
  INNER JOIN (SELECT * FROM AccessRequest UNION SELECT * FROM AccessRequestArchive) AccessRequest ON AccessRequest.RequestID = Tasks.RequestId    
  INNER JOIN (SELECT * FROM AccessRequestPerson UNION SELECT * FROM AccessRequestPersonArchive) AccessRequestPerson ON AccessRequestPerson.RequestPersonId = AccessRequest.RequestPersonId
  INNER JOIN dbo.HRPerson ON HRPerson.dbPeopleId = AccessRequestPerson.dbPeopleId
  LEFT JOIN dbo.HRPerson HR2 ON  AccessRequest.CreatedByID=HR2.dbPeopleId
  INNER JOIN dbo.TaskStatusLookup ON TaskStatusLookup.TaskStatusId = Tasks.TaskStatusId
  INNER JOIN dbo.AccessRequestStatus ON AccessRequest.RequestStatusID = AccessRequestStatus.RequestStatusID
  LEFT JOIN dbo.HRPerson AS HR3 ON Tasks.CompletedByID = HR3.dbPeopleId
  LEFT JOIN vw_AccessArea ON AccessRequest.AccessAreaID=vw_AccessArea.AccessAreaID
WHERE
  ( (@TaskCreatedStartDate IS NULL) OR (@TaskCreatedStartDate IS NOT NULL AND CreatedDate >= CAST(@TaskCreatedStartDate AS DATE)))
 AND
  ( (@TaskCreatedEndDate IS NULL) OR (@TaskCreatedEndDate IS NOT NULL AND CreatedDate <= CAST(@TaskCreatedEndDate AS DATE)))
  AND
( (@TaskCompletedStartDate IS NULL) OR (@TaskCompletedStartDate IS NOT NULL AND CompletedDate >= CAST(@TaskCompletedStartDate AS DATE)))
AND
( (@TaskCompletedEndDate IS NULL) OR (@TaskCompletedEndDate IS NOT NULL AND CompletedDate <= CAST(@TaskCompletedEndDate AS DATE)))


AND 
(
	(@PersonNameSearchType = 'Contains' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE ('%' + @PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Begins' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) LIKE (@PersonNameValueStart + '%')) ) )
	OR (@PersonNameSearchType = 'Equals' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'NotEqual' AND ( (@PersonNameValueStart = '' ) OR (@PersonNameValueStart <> '' AND NOT (HRPerson.Forename + ' ' + HRPerson.Surname) = (@PersonNameValueStart)) ) )
	OR (@PersonNameSearchType = 'Between' AND ( (@PersonNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@PersonNameValueStart <> '' AND @PersonNameValueEnd <> '' AND  (HRPerson.Forename + ' ' + HRPerson.Surname) BETWEEN @PersonNameValueStart AND @PersonNameValueEnd) ) )
			
)

AND 
(
	(@PersonEmpIdSearchType = 'Contains' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150)) ) LIKE ('%' + @PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Begins' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) LIKE (@PersonEmpIdValueStart + '%')) ) )
	OR (@PersonEmpIdSearchType = 'Equals' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'NotEqual' AND ( (@PersonEmpIdValueStart = '' ) OR (@PersonEmpIdValueStart <> '' AND NOT ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) = (@PersonEmpIdValueStart)) ) )
	OR (@PersonEmpIdSearchType = 'Between' AND ( (@PersonEmpIdValueStart = '' OR @PersonEmpIdValueEnd = '' ) 
			OR (@PersonEmpIdValueStart <> '' AND @PersonEmpIdValueEnd <> '' AND  ( CAST(HRPerson.dbPeopleId AS nvarchar(150))) BETWEEN @PersonEmpIdValueStart AND @PersonEmpIdValueEnd) ) )
			
)

AND 
(
	(@PersonEmailSearchType = 'Contains' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE ('%' + @PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Begins' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) LIKE (@PersonEmailValueStart + '%')) ) )
	OR (@PersonEmailSearchType = 'Equals' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'NotEqual' AND ( (@PersonEmailValueStart = '' ) OR (@PersonEmailValueStart <> '' AND NOT (  HRPerson.EmailAddress) = (@PersonEmailValueStart)) ) )
	OR (@PersonEmailSearchType = 'Between' AND ( (@PersonEmailValueStart = '' OR @PersonEmailalueEnd = '' ) 
			OR (@PersonEmailValueStart <> '' AND @PersonEmailalueEnd <> '' AND  ( HRPerson.EmailAddress) BETWEEN @PersonEmailValueStart AND @PersonEmailalueEnd) ) )
			
)
AND 
(
	(@PersonDbDirIdSearchType = 'Contains' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE ('%' + @PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Begins' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) LIKE (@PersonDbDirIdValueStart + '%')) ) )
	OR (@PersonDbDirIdSearchType = 'Equals' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'NotEqual' AND ( (@PersonDbDirIdValueStart = '' ) OR (@PersonDbDirIdValueStart <> '' AND NOT ( HRPerson.DBDirId) = (@PersonDbDirIdValueStart)) ) )
	OR (@PersonDbDirIdSearchType = 'Between' AND ( (@PersonDbDirIdValueStart = '' OR @PersonDbDirIdValueEnd = '' ) 
			OR (@PersonDbDirIdValueStart <> '' AND @PersonDbDirIdValueEnd <> '' AND  ( HRPerson.DBDirId) BETWEEN @PersonDbDirIdValueStart AND @PersonDbDirIdValueEnd) ) )
			
)
---- requestor
AND 
(
	(@RequestorNameSearchType = 'Contains' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE ('%' + @RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Begins' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) LIKE (@RequestorNameValueStart + '%')) ) )
	OR (@RequestorNameSearchType = 'Equals' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'NotEqual' AND ( (@RequestorNameValueStart = '' ) OR (@RequestorNameValueStart <> '' AND NOT (HR2.Forename + ' ' + HR2.Surname) = (@RequestorNameValueStart)) ) )
	OR (@RequestorNameSearchType = 'Between' AND ( (@RequestorNameValueStart = '' OR @PersonNameValueEnd = '' ) 
			OR (@RequestorNameValueStart <> '' AND @RequestorNameValueEnd <> '' AND  (HR2.Forename + ' ' + HR2.Surname) BETWEEN @RequestorNameValueStart AND @RequestorNameValueEnd) ) )
			
)
AND 
(
	(@RequestorEmpIdSearchType = 'Contains' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE ('%' + @RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Begins' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) LIKE (@RequestorEmpIdValueStart + '%')) ) )
	OR (@RequestorEmpIdSearchType = 'Equals' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'NotEqual' AND ( (@RequestorEmpIdValueStart = '' ) OR (@RequestorEmpIdValueStart <> '' AND NOT ( HR2.dbPeopleId) = (@RequestorEmpIdValueStart)) ) )
	OR (@RequestorEmpIdSearchType = 'Between' AND ( (@RequestorEmpIdValueStart = '' OR @RequestorEmpIdValueEnd = '' ) 
			OR (@RequestorEmpIdValueStart <> '' AND @RequestorEmpIdValueEnd <> '' AND  ( HR2.dbPeopleId) BETWEEN @RequestorEmpIdValueStart AND @RequestorEmpIdValueEnd) ) )
			
)

AND 
(
	(@RequestorEmailSearchType = 'Contains' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE ('%' + @RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Begins' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) LIKE (@RequestorEmailValueStart + '%')) ) )
	OR (@RequestorEmailSearchType = 'Equals' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'NotEqual' AND ( (@RequestorEmailValueStart = '' ) OR (@RequestorEmailValueStart <> '' AND NOT (  HR2.EmailAddress) = (@RequestorEmailValueStart)) ) )
	OR (@RequestorEmailSearchType = 'Between' AND ( (@RequestorEmailValueStart = '' OR @RequestorEmailalueEnd = '' ) 
			OR (@RequestorEmailValueStart <> '' AND @RequestorEmailalueEnd <> '' AND  ( HR2.EmailAddress) BETWEEN @RequestorEmailValueStart AND @RequestorEmailalueEnd) ) )
			
)
AND 
(
	(@RequestorDbDirIdSearchType = 'Contains' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE ('%' + @RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Begins' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) LIKE (@RequestorDbDirIdValueStart + '%')) ) )
	OR (@RequestorDbDirIdSearchType = 'Equals' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'NotEqual' AND ( (@RequestorDbDirIdValueStart = '' ) OR (@RequestorDbDirIdValueStart <> '' AND NOT ( HR2.DBDirId) = (@RequestorDbDirIdValueStart)) ) )
	OR (@RequestorDbDirIdSearchType = 'Between' AND ( (@RequestorDbDirIdValueStart = '' OR @RequestorDbDirIdValueEnd = '' ) 
			OR (@RequestorDbDirIdValueStart <> '' AND @RequestorDbDirIdValueEnd <> '' AND  ( HR2.DBDirId) BETWEEN @RequestorDbDirIdValueStart AND @RequestorDbDirIdValueEnd) ) )
			
)
--------
AND 
( (@RequestTypeID IS NULL) OR (@RequestTypeID IS NOT NULL AND AccessRequest.RequestTypeID = @RequestTypeID))

AND 
( (@RequestStatusFullId IS NULL) OR (@RequestStatusFullId IS NOT NULL AND AccessRequest.RequestStatusId = @RequestStatusFullId))

AND 
( (@RequestStatusId IS NULL) OR (@RequestStatusId IS NOT NULL AND Tasks.TaskStatusID = @RequestStatusId))
  
AND
(
   (@countryID IS NULL) OR (@countryID IS NOT NULL AND vw_AccessArea.CountryID IS NOT NULL AND vw_AccessArea.CountryID=@countryID)
)

AND
(
  (@cityID IS NULL) OR (@cityID IS NOT NULL AND vw_AccessArea.CityID IS NOT NULL AND vw_AccessArea.CityID=@cityID)
)
AND
(
  (@buildingID IS NULL) OR (@buildingID IS NOT NULL AND vw_AccessArea.BuildingID IS NOT NULL AND vw_AccessArea.BuildingID=@buildingID)
)
AND
(
  (@floorID IS NULL) OR (@floorID IS NOT NULL AND vw_AccessArea.FloorID IS NOT NULL AND vw_AccessArea.FloorID=@floorID)
)
AND
(
  (@AccessAreaId IS NULL) OR (@AccessAreaId IS NOT NULL AND vw_AccessArea.AccessAreaID IS NOT NULL AND vw_AccessArea.AccessAreaID=@AccessAreaId)
)

AND 
( (@PassOffice = '') OR (@PassOffice <> '' AND 

CHARINDEX(
	',' + CAST(AccessRequest.PassOfficeId AS NVARCHAR(10)) + ',', 
	',' + @PassOffice + ','
	) > 0
))

AND
(
  @IsUserAdmin=1  OR (@IsUserAdmin=0 AND AccessRequest.PassOfficeID  IN (SELECT LU.PassOfficeID FROM dbo.LocationPassOfficeUser LU WHERE LU.dbPeopleID=@UserdbPeopleID AND LU.IsEnabled=1) ) 
)

--ORDER BY CreatedDate DESC
ORDER BY AccessRequest.RequestID,Tasks.TaskID DESC

END

GO