IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccessAreaName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetAccessAreaName]
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetAccessAreaName]    Script Date: 11/17/2015 1:24:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=======================================================================
-- Create By: Wijtiha
-- Created Date: 17/11/2015	
-- Description: Retuns the full name (city/building/floor/Access Area Name) of a Access area
--=======================================================================

CREATE FUNCTION [dbo].[GetAccessAreaName]
(
@corporateDivAccessAreaId int
)
RETURNS varchar(max)
AS
BEGIN

DECLARE @accessAreaName varchar(max)

SELECT @accessAreaName =  ISNULL(city.Name + '/', '') + ISNULL(b.Name + '/', '') + ISNULL(f.Name + '/', '') + ISNULL(aa.Name, '')
FROM CorporateDivisionAccessArea caa
	LEFT OUTER JOIN AccessArea aa on aa.AccessAreaID = caa.AccessAreaID
	LEFT OUTER JOIN LocationFloor f on f.FloorID = aa.FloorID
	LEFT OUTER JOIN LocationBuilding b on aa.BuildingID = b.BuildingID
	LEFT OUTER JOIN LocationCity city on city.CityID = b.CityID
WHERE
	caa.CorporateDivisionAccessAreaID = @corporateDivAccessAreaId

RETURN @accessAreaName

END



GO


--//@UNDO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccessAreaName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
	DROP FUNCTION [dbo].[GetAccessAreaName]
END
GO