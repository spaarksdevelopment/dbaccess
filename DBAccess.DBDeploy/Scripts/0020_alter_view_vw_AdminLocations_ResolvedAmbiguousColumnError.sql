/****** Object:  View [dbo].[vw_AdminLocations]    Script Date: 5/18/2015 4:11:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_AdminLocations]
AS
SELECT        NULL AS parentId, CONVERT(varchar(255), regionID) + 'R' AS 'itemId', code, name, enabled, NULL AS CostCentreCode, NULL AS PassOffice, NULL AS TimeZone, NULL AS PassOfficeGerman, NULL 
                         AS PssOfficeItalyName
FROM            dbo.locationregion AS lr
UNION
SELECT        CONVERT(varchar(255), regionID) + 'R' AS parentId, CONVERT(varchar(255), lc.countryID) + 'C' AS 'itemId', lc.code, lc.name, lc.enabled, CostCentreCode, PO.Name AS PassOffice, NULL AS TimeZone, 
                         ISNULL(PO.GermanName, PO.Name) AS PassOfficeGerman, ISNULL(PO.ItalyName, PO.Name) AS PssOfficeItalyName
FROM            dbo.locationcountry AS lc LEFT OUTER JOIN
                         dbo.LocationPassOffice AS PO ON lc.PassOfficeID = PO.PassOfficeID
UNION
SELECT        CONVERT(varchar(255), countryID) + 'C' AS parentId, CONVERT(varchar(255), cityID) + 'Ci' AS 'itemId', lcc.code, lcc.name, enabled, NULL AS CostCentreCode, NULL AS PassOffice, TZ.name AS TimeZone, NULL 
                         AS PassOfficeGerman, NULL AS PssOfficeItalyName
FROM            dbo.locationCity AS lcc LEFT JOIN
                         dbo.TimeZone AS TZ ON lcc.TimeZoneID = TZ.TimeZoneID

GO


--//@UNDO

/****** Object:  View [dbo].[vw_AdminLocations]    Script Date: 5/18/2015 4:12:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_AdminLocations]
AS


select null  as parentId, convert(varchar(255),regionID) + 'R' as 'itemId', code, name, enabled,
null as CostCentreCode, null as PassOffice, null as TimeZone, null AS PassOfficeGerman,null AS PssOfficeItalyName
	from 	dbo.locationregion as lr
	
	
UNION

select convert(varchar(255),regionID) + 'R' as parentId, convert(varchar(255),countryID) + 'C' as 'itemId', lc.code, lc.name, lc.enabled,
CostCentreCode, PO.Name as PassOffice, null as TimeZone, ISNULL(PO.GermanName,PO.Name) AS PassOfficeGerman,ISNULL(PO.ItalyName,PO.Name) AS PssOfficeItalyName
 from dbo.locationcountry as lc
left outer join dbo.LocationPassOffice as PO
	on lc.PassOfficeID = PO.PassOfficeID
UNION

select  convert(varchar(255),countryID) + 'C' as parentId, convert(varchar(255),cityID) + 'Ci' as 'itemId',lcc.code, lcc.name,enabled,
null as CostCentreCode, null as PassOffice, TZ.name as TimeZone, null AS PassOfficeGerman,null AS PssOfficeItalyName
from dbo.locationCity as lcc
left join dbo.TimeZone as TZ
on lcc.TimeZoneID = TZ.TimeZoneID


GO



