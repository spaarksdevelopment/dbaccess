/****** Object:  StoredProcedure [dbo].[xp_DataLoad_ImportDBMovesBuildings]    Script Date: 17/04/2015 16:39:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[xp_DataLoad_ImportDBMovesBuildings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[xp_DataLoad_ImportDBMovesBuildings]
GO

--//@UNDO
/****** Object:  StoredProcedure [dbo].[xp_DataLoad_ImportDBMovesBuildings]    Script Date: 17/04/2015 16:42:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<AIS>
-- Create date: <09/11/2010>
-- Description:	<Update Buildings from DBMoves Data>
-- =============================================
CREATE PROCEDURE [dbo].[xp_DataLoad_ImportDBMovesBuildings]
	@DBMLocationID INT,
	@DBMRegionID INT,
	@DBMLocationName VARCHAR(255),
	@DBMCity VARCHAR(50)
AS
BEGIN
	
	DECLARE @CityID int
	DECLARE @CountryID int
	DECLARE @TimeZoneID int

	--first see if building is already listed
		
	IF (SELECT COUNT(*) FROM [LocationBuilding] WHERE [DBMovesLocationID]=@DBMLocationID) > 0
	BEGIN
		PRINT 'Building Already Listed: ' + CAST(@DBMLocationID AS VARCHAR(5)) + ' ' + @DBMLocationName
		RETURN 0
	END

	-- Now check if City is listed, if not, add it
	
	SELECT @CityID=[CityID] FROM [LocationCity] WHERE [Name] = @DBMCity
	IF @CityID IS NULL
	BEGIN
		--GET Country ID
		SELECT @CountryID=[CountryID] FROM [LocationCountry] WHERE [DBMovesRegionID]=@DBMRegionID
		
		IF @CountryID IS NULL
		BEGIN
			PRINT 'Country Not Found: RegionID= ' + CAST(@DBMRegionID AS VARCHAR(5))
			RETURN 0
		END
	
		-- GET Timezone from another city in that country (it can be corrected later if necessary)
		SELECT @TimeZoneID=[TimeZoneID] FROM [dbo].[LocationCity] WHERE CountryID=@CountryID
		IF @TimeZoneID IS NULL
		BEGIN
			PRINT 'Time Zone Not Identified: City= ' + @DBMCity
			-- Just set to GMT
			SET @TimeZoneID=16
		END

		INSERT INTO [dbo].[LocationCity]
           ([CountryID]
           ,[Code]
           ,[Name]
           ,[Enabled]
           ,[TimeZoneID])
		VALUES
           (@CountryID
           ,NULL
           ,@DBMCity
           ,1
           ,@TimeZoneID)

		SET @CityID=Scope_Identity()
	END

	-- Now Insert Building
	
	INSERT INTO [dbo].[LocationBuilding]
           ([CityID]
           ,[Code]
           ,[Name]
           ,[DBMovesLocationID]
           ,[StreetAddress]
           ,[Enabled]
           ,[Complex]
           ,[LocationURL]
           ,[BSPAARSLocationID])
     VALUES
           (@CityID
           ,NULL
           ,@DBMLocationName
           ,@DBMLocationID
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,NULL)

	IF @@Error=0
	BEGIN	
		PRINT 'Location Added with ID ' + CAST(Scope_Identity() AS VARCHAR(5))
		RETURN 1
	END
	ELSE 
		RETURN 0
END

GO

