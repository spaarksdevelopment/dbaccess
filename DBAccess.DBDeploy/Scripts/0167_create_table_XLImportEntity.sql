IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportEntity')
BEGIN

CREATE TABLE [dbo].[XLImportEntity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Entity] [nvarchar](100) NOT NULL,
	[SheetName] [nvarchar](100) NOT NULL,
	[StagingTable] [nvarchar](100) NOT NULL,
	[DestinationTable] [nvarchar](100) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_XLImportEntity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO


--//@UNDO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'XLImportEntity')
BEGIN
	
	DROP TABLE [dbo].[XLImportEntity]
	
END

GO