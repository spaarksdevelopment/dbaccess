﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace RevokeSmartcardService
{
	/// <summary>
	/// Summary description for Service1
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class RevokeSmartcardService : System.Web.Services.WebService
	{
		//Note this is a dummy webservice which is used to allow testing of the DBSmartcardWindowsService

		//The real service is part of the dbSmartcard/ PKI project and is not being written by us
		[WebMethod]
		public int RevokeSmartcard(string SmartcardIdentifier)
		{
			return 256;
		}
	}
}