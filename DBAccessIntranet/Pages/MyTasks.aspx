﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="True" CodeBehind="MyTasks.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyTasks" UICulture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register TagPrefix="uc1" Src="~/Controls/LoggedInUserDetails.ascx" TagName="LoggedInUserDetails" %>

<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">

    <script type="text/javascript">

        var strInputDescription = '<%= EscapeJavaScriptSpecialCharacters(GetResourceManger("Resources.CommonResource", "InputDescriptionMiFare")) %>';
        var myTasksRejectionReason = '<%= EscapeJavaScriptSpecialCharacters(MyTasksRejectionReason.Text) %>';
        var myTasksReason = '<%= EscapeJavaScriptSpecialCharacters(MyTasksReason.Text) %>';
        var myTasksErrorTask = '<%= EscapeJavaScriptSpecialCharacters(MyTasksErrorTask.Text) %>';
        var myTasksProcessTask = '<%= EscapeJavaScriptSpecialCharacters(MyTasksProcessTask.Text) %>';
        var myTasksUsedMiFare = '<%= EscapeJavaScriptSpecialCharacters(MyTasksUsedMiFare.Text) %>';
        var myTasksNoTaskDetails = '<%= EscapeJavaScriptSpecialCharacters(MyTasksNoTaskDetails.Text) %>';
        var myTasksDesc = '<%= EscapeJavaScriptSpecialCharacters(MyTasksDesc.Text) %>';
        var myTasksInputDesc = '<%= EscapeJavaScriptSpecialCharacters(MyTasksInputDesc.Text) %>';
        var myTasksInvalidMiFare = '<%= EscapeJavaScriptSpecialCharacters(MyTasksInvalidMiFare.Text) %>';

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">

    <h1>
        <asp:Literal ID="MyTasksDetail" runat="server" meta:resourceKey="MyTasks"></asp:Literal></h1>

    <p>
        <asp:Literal ID="MyTasksDescription" runat="server" meta:resourceKey="MyTasksDescription"></asp:Literal>
    </p>
    <br />

    <asp:Panel runat="server" ID="ChooseTaskType">
        <table>
            <tr>

                <td>
                    <asp:Literal ID="SelectStatus" runat="server" meta:resourceKey="SelectStatus"></asp:Literal>
                </td>
                <td>
                    <dx:ASPxComboBox ID="MyTaskStatus" ClientInstanceName="MyTaskStatus"
                        runat="server" TextField="Status" ValueField="TaskStatusId" Width="300px" ValueType="System.String">
                        <ClientSideEvents SelectedIndexChanged="function(s,e){ PerformTaskStatusCallback(); }" />
                    </dx:ASPxComboBox>
                </td>
            </tr>
        </table>
        <div style="margin-top: -30px; float: right;">
            <table>
                <tr>
                    <td>
                        <asp:Literal ID="SelectSearchOption" runat="server" meta:resourceKey="SelectSearchOption"></asp:Literal>
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="MySearchOption" ClientInstanceName="MySearchOption"
                            runat="server" TextField="TaskSearchOptionName" ValueField="TaskSearchOptionId"
                            Width="300px" ValueType="System.String">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="SearchText" runat="server" Width="120px">
                        </dx:ASPxTextBox>
                    </td>
                    <td>
                        <dx:ASPxButton ID="Search" runat="server" Text="" AutoPostBack="False"
                            meta:resourcekey="Search">
                            <ClientSideEvents Click="function(s,e){ PerformTaskStatusCallback(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <dx:ASPxButton ID="Reset" runat="server" Text="Reset" AutoPostBack="False"
                            meta:resourcekey="Reset">
                            <ClientSideEvents Click="function(s,e){ window.location.href= window.location;  }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </asp:Panel>
    <div id="divRequestPersons">
        <p>
            <asp:Literal ID="ExportToExcel" runat="server" meta:resourceKey="ExportToExcel"></asp:Literal>
            <asp:ImageButton ID="ASPxButton2"
                AlternateText="Click to export to Excel" runat="server"
                OnClick="btnXlsxExport_Click" SkinID="FileXLS"/>
        </p>
        <dx:ASPxGridView ID="Tasks" ClientInstanceName="Tasks"
            runat="server" AutoGenerateColumns="False"
            KeyFieldName="TaskId"
            OnStartRowEditing="Tasks_StartRowEditing"
            ClientIDMode="AutoID"
            EnableViewState="False"
            OnHtmlRowCreated="Tasks_HtmlRowCreated" OnCustomCallback="Tasks_CustomCallback">

            <SettingsPager PageSize="25"></SettingsPager>

            <SettingsEditing Mode="PopupEditForm" PopupEditFormHorizontalAlign="Center"
                PopupEditFormVerticalAlign="Middle" PopupEditFormModal="True"></SettingsEditing>

            <SettingsText PopupEditFormCaption="<%$ Resources:CommonResource, EditTask %>" EmptyDataRow="<%$ Resources: NoTaskMessage %>"></SettingsText>

            <ClientSideEvents EndCallback="
								function (s, e)
								{						
                                    popupLoading.Hide();
								}
							" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="" FieldName="RequestMasterID"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="0"
                    meta:resourcekey="GridViewDataTextColumnRequestID">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="" VisibleIndex="1" Name="Approval"
                    meta:resourcekey="GridViewDataTextColumnApprove">
                    <DataItemTemplate>
                        <dx:ASPxRadioButtonList ID="rblTaskAction" runat="server"
                            RepeatDirection="Horizontal"
                            ClientSideEvents-SelectedIndexChanged='<%#  "function(s, e) { SelectTask(s, " + Container.KeyValue + "," + Eval("TaskTypeId") + "," + "s.GetValue())}"  %>'>
                            <Items>
                                <dx:ListEditItem Text="<%$ Resources:CommonResource, Yes %>" Value="3" />
                                <dx:ListEditItem Text="<%$ Resources:CommonResource, No %>" Value="2" />
                            </Items>
                        </dx:ASPxRadioButtonList>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn Caption="" VisibleIndex="2"
                    meta:resourcekey="GridViewDataColumnDetails">
                    <DataItemTemplate>
                        <a href="javascript:void(0);" class="clickable" onclick="ShowDetails(this, '<%# Container.KeyValue %>')">
                            <asp:Literal ID="Details" runat="server" meta:resourceKey="Details"></asp:Literal>
                        </a>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>

                <dx:GridViewCommandColumn Caption="" EditButton-Visible="true"
                    VisibleIndex="3" meta:resourcekey="GridViewCommandColumnEdit">
                    <EditButton Visible="True"></EditButton>
                </dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn Caption="" FieldName="Descript"
                    ShowInCustomizationForm="true" ReadOnly="True" VisibleIndex="4"
                    meta:resourcekey="GridViewDataTextColumnTask">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataMemoColumn Caption="" FieldName="TaskComment"
                    ShowInCustomizationForm="True" ReadOnly="false" VisibleIndex="5"
                    meta:resourcekey="GridViewDataMemoColumnComment">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataMemoColumn>
                <dx:GridViewDataTextColumn Caption="Applicant" FieldName="Applicant"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="6"
                    meta:resourcekey="GridViewDataTextColumnApplicant">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="RequestStartDate"
                    PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="7"
                    meta:resourcekey="GridViewDataDateColumnStartDate">
                    <Settings AllowHeaderFilter="True" />
                    <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="RequestEndDate"
                    PropertiesDateEdit-AllowNull="true" VisibleIndex="8"
                    EditFormSettings-Visible="True"
                    meta:resourcekey="GridViewDataDateColumnEndDate">
                    <PropertiesDateEdit AllowUserInput="true" runat="server" AllowNull="true" DisplayFormatString="dd-MMM-yyyy" EditFormat="Date" />
                    <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

                    <EditFormSettings Visible="True"></EditFormSettings>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="BuildingName"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="9"
                    meta:resourcekey="GridViewDataTextColumnBuilding">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="FloorName"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="10"
                    meta:resourcekey="GridViewDataTextColumnFloor">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="AccessAreaName"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="11"
                    meta:resourcekey="GridViewDataTextColumnAccessArea">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="RequestBy"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="12"
                    meta:resourcekey="GridViewDataTextColumnRequester">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="CreatedDate"
                    PropertiesDateEdit-AllowNull="true" VisibleIndex="13"
                    EditFormSettings-Visible="True"
                    meta:resourcekey="GridViewDataDateColumnDateCreated">
                    <EditFormSettings Visible="True"></EditFormSettings>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="TaskId"
                    ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="14"
                    Visible="false" meta:resourcekey="GridViewDataTextColumnTaskID">
                    <Settings AllowHeaderFilter="True" />
                    <Settings AllowHeaderFilter="True"></Settings>
                </dx:GridViewDataTextColumn>
            </Columns>

            <SettingsPager PageSize="25" />
            <SettingsEditing Mode="PopupEditForm" PopupEditFormHorizontalAlign="Center" PopupEditFormVerticalAlign="Middle" PopupEditFormModal="true" />

            <SettingsText EmptyDataRow="You currently have no outstanding tasks." />
            <SettingsLoadingPanel Mode="Disabled" />
            <settingsdatasecurity allowdelete="False" allowinsert="False" />

            <Templates>
                <EditForm>
                    <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                        <PanelCollection>
                            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                                <div style="width: 600px; height: 230px; margin-left: 20px; margin-top: 20px;">
                                    <asp:Table ID="Table1" runat="server">
                                        <asp:TableRow ID="TopRow" runat="server">
                                            <asp:TableCell ID="TopRowCell1" runat="server">
                                                <div style="width: 250px;">
                                                    <asp:Table ID="Table2" runat="server">
                                                        <asp:TableRow ID="TableRow15" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel runat="server" ID="lblApplicantID" Text=""
                                                                    meta:resourcekey="lblApplicantID">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxRequestID" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="RequestMasterID"/>
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow0" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="lblApplicant" Text="" Style="margin-left: 9px;"
                                                                    meta:resourcekey="lblApplicant">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxApplicant" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="Applicant"/>
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow1" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="ASPxLabel1" Text="" Style="margin-left: 18px;"
                                                                    meta:resourcekey="ASPxLabel1Building">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxBuildingName" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="BuildingName" />
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow2" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="ASPxLabel2" Text="" Style="margin-left: 30px;"
                                                                    meta:resourcekey="ASPxLabel2Floor">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxFloorName" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="FloorName"/>
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow3" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="ASPxLabel3" Style="margin-left: 30px;"
                                                                    meta:resourcekey="ASPxLabelArea">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxAccessAreaName" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="AccessAreaName" />
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow4" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="ASPxLabel4" Text=""
                                                                    meta:resourcekey="ASPxLabel4Requester">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="LeftAlignElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxRequestBy" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="RequestBy" />
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </asp:TableCell>
                                            <asp:TableCell ID="TopRowCell2" runat="server">
                                                <div style="width: 300px;">
                                                    <asp:Table ID="Table3" runat="server">
                                                        <asp:TableRow ID="TableRow5" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <div class="RightAlignmentElement" style="margin-left: 30px;">
                                                                    <dx:ASPxLabel ID="lblComment" runat="server" Text=""
                                                                        meta:resourcekey="lblComment">
                                                                    </dx:ASPxLabel>
                                                                </div>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div class="RightAlignmentElement">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="AspxRequestComment" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="TaskComment" Width="190px" />
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="MiddleRow" runat="server">
                                            <asp:TableCell ID="MiddleRowCell1" runat="server">
                                                <div style="width: 250px; height: 170px;">
                                                    <asp:Table ID="Table4" runat="server">
                                                        <asp:TableRow ID="TableRow10" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="lblStartDate" Text=""
                                                                    meta:resourcekey="lblStartDate">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div style="margin-left: 19px;">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="dtStartDate" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="RequestStartDate"/>
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow11" runat="server">
                                                            <asp:TableCell runat="server">
                                                                <dx:ASPxLabel
                                                                    runat="server" ID="lblEndDate" Text="" Style="margin-left: 5px;"
                                                                    meta:resourcekey="lblEndDate">
                                                                </dx:ASPxLabel>
                                                            </asp:TableCell>
                                                            <asp:TableCell runat="server">
                                                                <div style="margin-left: 19px;">
                                                                    <dx:ASPxGridViewTemplateReplacement ID="dtEndDate" runat="server"
                                                                        ReplacementType="EditFormCellEditor" ColumnID="RequestEndDate" />
                                                                </div>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </asp:TableCell>
                                            <asp:TableCell ID="MiddleRowCell2" runat="server">
                                                <div style="width: 300px; height: 170px;">
                                                    <asp:Table ID="Table5" runat="server">
                                                        <asp:TableRow ID="TableRow12" runat="server">
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="BottomRow" runat="server">
                                            <asp:TableCell ID="BottomRowCell1" runat="server">
                                                <div style="width: 250px; margin-top: 20px;">
                                                    <asp:Table ID="Table6" runat="server">
                                                        <asp:TableRow ID="TableRow14" runat="server">
                                                            <asp:TableCell runat="server"></asp:TableCell>
                                                            <asp:TableCell runat="server"></asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                    <div style="margin-left: 20px; padding: 2px 2px 2px 2px; margin-bottom: 10px; margin-top: 15px;">
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnUpdate" runat="server" AutoPostBack="False" Text=""
                                        ClientSideEvents-Click='<%#  "function(s, e) { " + Container.UpdateAction + " }"  %>'
                                        meta:resourcekey="ASPxAddInput" />
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" Text=""
                                        ClientSideEvents-Click='<%#  "function(s, e) { " + Container.CancelAction + " }"  %>'
                                        meta:resourcekey="ASPxCancelInput" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </EditForm>
            </Templates>

        </dx:ASPxGridView>
    </div>
    <br />
    <dx:ASPxButton ID="ASPxButtonConfirm" Text=""
        ClientInstanceName="buttonConfirm" runat="server" AutoPostBack="False"
        meta:resourcekey="ASPxButtonConfirm">
        <ClientSideEvents Click="ConfirmTasks" />
    </dx:ASPxButton>

    <uc1:LoggedInUserDetails ID="LoggedInUserDetailsControl" runat="server"/>

    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="Tasks"></dx:ASPxGridViewExporter>


    <dx:ASPxHiddenField ID="ASPxHiddenFieldTasks" ClientInstanceName="hfTasks" OnCustomCallback="ASPxHiddenFieldTasks_Callback" runat="server">
        <ClientSideEvents EndCallback="OnSubmitTasks" />
    </dx:ASPxHiddenField>

    <dx:ASPxHiddenField ID="ASPxHiddenFieldInputs" ClientInstanceName="hfInputs" runat="server"/>
    <dx:ASPxHiddenField ID="ASPxHiddenFieldMFN" ClientInstanceName="hfMFN" runat="server"/>
    <dx:ASPxHiddenField ID="ASPxHiddenFieldInputsTaskRadioButton" ClientInstanceName="hfInputsTaskRadioButton" runat="server"/>
    
    <div id="popupTaskDetails">
      <div id="taskDetailsPopupContent"></div>
    </div>
    
    <dx:ASPxPopupControl ID="popupDetails" ClientInstanceName="popupDetails" runat="server"
        CloseAction="CloseButton"
        AllowDragging="True"
        HeaderText="Task Details"
        DragElement="Window"
        Width="500px"
        Height="500px"
        PopupHorizontalAlign="OutsideRight"
        PopupVerticalAlign="TopSides" meta:resourcekey="popupDetailsResource1">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server"
                SupportsDisabledAttribute="True">
                <h2>
                    <asp:Literal ID="MyTasksTaskDetails" meta:resourcekey="MyTasksTaskDetails" runat="server"></asp:Literal></h2>
                <br />
                <div id="TaskDescription"></div>

            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popupRejectReason"
        ClientInstanceName="popupRejectReason" runat="server"
        CloseAction="CloseButton"
        AllowDragging="True"
        ShowHeader="False"
        DragElement="Window"
        Width="400px"
        Height="160px"
        Modal="True"
        PopupHorizontalAlign="OutsideRight"
        PopupVerticalAlign="TopSides" meta:resourcekey="popupRejectReasonResource1">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server"
                SupportsDisabledAttribute="True">
                <p>
                    <asp:Literal ID="ReasonforRejection" runat="server" meta:resourceKey="ReasonforRejection"></asp:Literal>
                </p>

                <dx:ASPxMemo ID="ASPxMemoReason" ClientInstanceName="memoReason"
                    runat="server" Height="80px" Width="380px">
                </dx:ASPxMemo>

                <div style="float: left; margin: 5px" id="divReasonButton0">
                    <dx:ASPxButton ID="ASPxAddReason" Text="" runat="server"
                        AutoPostBack="false" meta:resourcekey="SaveReason">
                        <ClientSideEvents Click="SaveReason" />
                    </dx:ASPxButton>
                </div>
                <div style="float: left; margin: 5px" id="divReasonButton1">
                    <dx:ASPxButton ID="ASPxCancelReason" Text="" runat="server"
                        AutoPostBack="false" meta:resourcekey="ASPxCancelInput">
                        <ClientSideEvents Click="CancelPopup" />
                    </dx:ASPxButton>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <!-- This popup is used for collecting MiFare on 'yes' vote for new DB pass.  It could potentially be used in any other situations where additional info is required on a 'yes' vote. -->
    <dx:ASPxPopupControl ID="popupInput" ClientInstanceName="popupInput" runat="server"
        CloseAction="CloseButton"
        AllowDragging="True"
        ShowHeader="False"
        DragElement="Window"
        Width="250px"
        Height="120px"
        Modal="True"
        PopupHorizontalAlign="OutsideRight"
        PopupVerticalAlign="TopSides" meta:resourcekey="popupInputResource1">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server"
                SupportsDisabledAttribute="True">
                <p><span id="spanInputDescription"></span></p>

                <dx:ASPxTextBox ID="ASPxTxtInput" ClientInstanceName="txtInput" runat="server"
                    Width="220px">
                </dx:ASPxTextBox>

                <div style="float: left; margin: 5px 8px 5px 0px; width: 88px">
                    <dx:ASPxButton ID="ASPxAddInput" Text="Save" runat="server" AutoPostBack="false"
                        meta:resourcekey="ASPxAddInput" Width="88px">
                        <ClientSideEvents Click="SaveInput" />
                    </dx:ASPxButton>
                </div>
                <div style="float: right; margin: 5px 5px 5px 0px; width: 88px">
                    <dx:ASPxButton ID="ASPxCancelInput" Text="Cancel" runat="server"
                        AutoPostBack="false" meta:resourcekey="ASPxCancelInput" Width="88px">
                        <ClientSideEvents Click="CancelPopup" />
                    </dx:ASPxButton>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <ppc:Popups ID="PopupTemplates" runat="server" />

    <!--Literals-->

    <%-- <asp:Literal ID="DeletePersonLiteral"  text="<%$ Resources:CommonResource, DeletePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>--%>
    <asp:Literal ID="MyTasksNoTaskDetails" meta:resourcekey="MyTasksNoTaskDetails" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksUsedMiFare" meta:resourcekey="MyTasksUsedMiFare" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksErrorTask" meta:resourcekey="MyTasksErrorTask" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="MyTasksProcessTask" meta:resourcekey="MyTasksProcessTask" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksRejectionReason" meta:resourcekey="MyTasksRejectionReason" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksReason" meta:resourcekey="MyTasksReason" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="MyTasksDesc" meta:resourcekey="MyTasksDesc" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksInputDesc" meta:resourcekey="MyTasksInputDesc" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="MyTasksInvalidMiFare" meta:resourcekey="MyTasksInvalidMiFare" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="MyTasksMiFareExist" meta:resourcekey="MyTasksMiFareExist" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>


</asp:Content>
