﻿using System;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.Services;
using System.Web.Script.Services;
using DevExpress.Web.ASPxGridView;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class DivisionAdministratorRecertification : BaseSecurityPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            GetData();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void GetData()
        {
            DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Grid.DataSource = Director.GetDARolesForRecertification(currentUser.dbPeopleID);
            Grid.DataBind();

            Grid.Selection.UnselectAll();
        }

        protected void CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GetData();
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }

        [WebMethod, ScriptMethod]
        public static int Recertify(string[] selections)
        {
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            int? certifiedUsersOnAdd = Director.GetCertifiedDACount(CurrentUser.dbPeopleID, 0, selections);

            if (!certifiedUsersOnAdd.HasValue)
                return 0;

            if (certifiedUsersOnAdd.Value > 5)
                return -1;

            //action=0 [renew]
            if (UpdateCertification(selections, 0, CurrentUser))
                return 1;
            else
                return 0;
        }

        [WebMethod, ScriptMethod]
        //Returns 0 - Unknown Error
        //        1 - Success
        //        -1 - Not enough certified users after removal
        public static int Remove(string[] selections)
        {
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            int? certifiedUsersOnRemove = Director.GetCertifiedDACount(CurrentUser.dbPeopleID, 1, selections);

            if (!certifiedUsersOnRemove.HasValue)
                return 0;

            if (certifiedUsersOnRemove.Value < 2)
                return -1;

            //action=1 [remove]
            if (UpdateCertification(selections, 1, CurrentUser))
                return 1;
            else
                return 0;
        }

        private static bool UpdateCertification(string[] selections, int action, DBAppUser CurrentUser)
        {
            bool success = true;

            foreach (string g in selections)
            {
                Director d = new Director(CurrentUser.dbPeopleID);

                if (!d.UpdateDARoleCertification(Convert.ToInt32(g), action))
                    success = false;
            }

            return success;
        }
    }
}