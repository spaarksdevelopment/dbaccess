﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class ViewAllMyRequests : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadMyRequests();
        }

        protected void LoadMyRequests()
        {
            int ncurrentUserId = base.CurrentUser.dbPeopleID;
            List<DBAccessController.DAL.ViewAllMyRequests> lstmyRequests = Director.GetViewAllMyRequests(ncurrentUserId);

            ASPxGridViewRequestDetailsDetails.DataSource = lstmyRequests;
            ASPxGridViewRequestDetailsDetails.DataBind();
        }

        #region
        /// <summary>
        /// Method to export the po grid data to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnXlsxExportPO_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();

        }
        #endregion
    }
}