﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="VendorFileManagement.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.VendorFileManagement" uiculture="auto" %>

<%@ Register assembly="Brettle.Web.NeatUpload" namespace="Brettle.Web.NeatUpload" tagprefix="Upload" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">
		function ShowSmartcardUpload() {
			popupSmartcardNumbersUpload.Show();
		}
		
		function ValidateUploadFile(Source, args) {
			var chkFile = document.getElementById('<%= inputFileId.ClientID %>');

			if (chkFile.value == "") {
				args.IsValid = false;
				popupSmartcardNumbersUpload.Show();
			}
		}

		var currentAuditID;

		function RecallFileClick(auditID, recallDate) {
			if (recallDate != "") {
			    popupAlertTemplateContent.SetText("<%=VendorFileJava1.Text %>"); //This file has already been recalled.
			    popupAlertTemplate.SetHeaderText("<%=RecallFile1.Text %>"); //Recall File
				popupAlertTemplate.Show();
				return;
			};

			currentAuditID = auditID;

			PageMethods.GetNumberOfMatchingBadges(currentAuditID, OnGetNumberOfMatchingBadges);
		}

		function OnGetNumberOfMatchingBadges(matchingBadges) {
			popupConfirmTemplate.SetHeaderText("");
			popupConfirmTemplateContentHead.SetText("<%=ConfirmRecall.Text %>"); //Confirm File Recall

			var question = "<%=ConfirmSureRecall.Text %>"; //Are you sure you want to recall the file?
			if (matchingBadges > 0) {
			    question = "<%=RecallPromptPart1.Text %> " + matchingBadges + "<%=RecallPromptPart2.Text %>";
			}

			popupConfirmTemplateContentBody.SetText(question);
			popupConfirmTemplateHiddenField.Set("function_Yes", "DoRecallFile();");
			popupConfirmTemplate.Show();
		}

		function DoRecallFile() {
			PageMethods.RecallFile(currentAuditID, OnRecallFile);
		}

		function OnRecallFile(result) {
			if (!result) {
			    popupAlertTemplateContent.SetText("<%=RecallError.Text %>");
			    popupAlertTemplate.SetHeaderText("<%=RecallFile1.Text %>");
				popupAlertTemplate.Show();
				return;
			}

		    popupAlertTemplateContent.SetText(" <%=RecallFileSuccess.Text %>");
			popupAlertTemplate.SetHeaderText("<%=RecallFile1.Text %>");
			popupAlertTemplateHiddenField.Set("function_OK", "RefreshPage();");
			popupAlertTemplate.Show();
			return;
		}

		function ShowRecallButton(recallDate) { 
			return (recallDate == "")
		}

		function ExportRecalledFile() {
		    popupAlertTemplateContent.SetText("<%=CannotExport.Text %>");
		    popupAlertTemplate.SetHeaderText("<%=ExportFile.Text %>");
			popupAlertTemplate.Show();

		}

		function RefreshPage() {
			window.location.reload();
		}

		function ResetControls() {
		    ComboBoxSmartcardProviders.SetValue("<%=GetResourceManager("Resources.CommonResource", "SelectProvider")%>");
		}
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<h1><asp:Literal ID="VendorFileHead" meta:resourcekey="VendorFileHead" runat="server"></asp:Literal></h1>

	<dx:ASPxGridView ID="ASPxGridViewFileUploads" ClientInstanceName="gvFileUploads"
             runat="server" AutoGenerateColumns="False" KeyFieldName="ID" 
        ViewStateMode="Enabled" >
		<Columns>
			<dx:GridViewDataTextColumn FieldName="ID" Visible="false" ShowInCustomizationForm="True" 
				VisibleIndex="0" >
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataDateColumn FieldName="DateUploaded"  VisibleIndex="1"
				ShowInCustomizationForm="True" Width="120" 
                meta:resourcekey="UploadDate">
				<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy HH:mm"></PropertiesDateEdit>
			</dx:GridViewDataDateColumn>
			<dx:GridViewDataTextColumn FieldName="Vendor" ShowInCustomizationForm="True" 
				VisibleIndex="2" meta:resourcekey="Vendor" >
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataTextColumn FieldName="Identifier" 
				ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="Identifier">
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataTextColumn FieldName="Operator" ShowInCustomizationForm="True" 
				VisibleIndex="4" meta:resourcekey="Operator" >
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataColumn  VisibleIndex="5" 
                meta:resourcekey="NumberRowsHead" >
                <DataItemTemplate>
					<dx:ASPxButton ID="btnExportNumbers" runat="server" 
                        Text='<%# Eval("NumberOfRows") %>' OnCommand="btnExportNumbers_Command" 
                        CommandName="AuditID" 
                        CommandArgument='<%# Container.KeyValue + "," + Container.VisibleIndex %>' 
                         />
                </DataItemTemplate>
            </dx:GridViewDataColumn>
			<dx:GridViewDataColumn  VisibleIndex="6" 
                meta:resourcekey="RecallHead">
                <DataItemTemplate>
					<dx:ASPxButton ID="btnRecallFile" runat="server" AutoPostBack="False" 
					
                        ClientSideEvents-Click='<%#  "function(s, e){RecallFileClick(" + Container.KeyValue + ",\"" + Eval("RecallDate") + "\");}"  %>' 
                        meta:resourcekey="Recall"/>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
			<dx:GridViewDataDateColumn FieldName="RecallDate" VisibleIndex="7"
				ShowInCustomizationForm="True" Width="120" 
                meta:resourcekey="RecallDate">
				<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy HH:mm"></PropertiesDateEdit>
			</dx:GridViewDataDateColumn>
		</Columns>
		<Templates>
				<StatusBar>
					<dx:ASPxButton ID="btnUploadFile" runat="server" 
                        AutoPostBack="False" meta:resourcekey="UploadVendorFile" >
				        <ClientSideEvents Click="function(s, e){ ShowSmartcardUpload();}" />
                    </dx:ASPxButton>
				</StatusBar>
			</Templates>
		<Settings ShowStatusBar="Visible"/> 
		<SettingsPager PageSize="25" />                
	</dx:ASPxGridView>

	
	<dx:ASPxPopupControl ID="PopupSmartcardNumbersUpload" 
        ClientInstanceName="popupSmartcardNumbersUpload" runat="server" 
        SkinID="PopupBasic" style="height:150px;" 
        meta:resourcekey="UploadSmartCard">
		<%--<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />--%>
        <ClientSideEvents PopUp="function(s, e){ ResetControls(); }" />
		<ContentCollection>
            <dx:PopupControlContentControl ID="PopupContentControl" runat="server" 
                SupportsDisabledAttribute="True">
                <div style="width: 280px; margin-left: auto; margin-right: auto; clear: both">
                    <div style="float: left; padding: 5px">
						<dx:ASPxComboBox ID="ComboBoxSmartcardProviders" runat="server" 
                            ClientIDMode="Static" TextField="TypeString" ValueField="ID" 
                            AutoPostBack="false" Width="300px">
						</dx:ASPxComboBox>
						<br />
                         <div style="position:relative;">
                            <Upload:InputFile ID="inputFileId" ClientIDMode="Static" runat="server" 
							meta:resourcekey="inputFileId"/>
                        </div>
						<br />
						<asp:CustomValidator ID="CustomValidatorVendor" runat="server" 
                            ControlToValidate="ComboBoxSmartcardProviders" 
                            OnServerValidate="ValidateVendor" 
                            ErrorMessage="<%$ Resources:CommonResource, SelectVendor%>" 
                            ValidationGroup="UploadGroup">
						</asp:CustomValidator>
						<div>
							<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateUploadFile"
								ErrorMessage="<%$ Resources:CommonResource, PleaseSelectFileForUpload%>"   ValidationGroup="UploadGroup">
							</asp:CustomValidator>
						</div>
						<br /><br />
                        <Upload:ProgressBar ID="progressBarId" runat="server" Inline="false" />
						<asp:Label runat="server" ID="LabelDuplicateSerialError" style="color: Red;" 
                            Visible="false"></asp:Label>
						<br />
						<span style="margin: 2px; float: left;">
                            <dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel %>"
                                ForeColor="#0098db" ValidationGroup="UploadGroup">
                                <ClientSideEvents Click="function(s,e){ popupSmartcardNumbersUpload.Hide();}" />
                            </dx:ASPxButton>
                        </span>
                        <span style="margin: 2px; float: left;">
                            <dx:ASPxButton ID="btnSubmitSmartcardNumbers" runat="server" 
                            AutoPostBack="false" Text="<%$ Resources:CommonResource, Validate %>"
                                OnClick="btnSubmitSmartcardNumbers_Click" 
                            ValidationGroup="UploadGroup">
                            </dx:ASPxButton>
                        </span>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

	<ppc:Popups ID="PopupTemplates" runat="server" />
    <!--literals-->
    
    <asp:Literal ID="VendorFileJava1"  meta:resourcekey="VendorFileJava1" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="RecallFile1"  meta:resourcekey="RecallFile" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="ConfirmRecall"  meta:resourcekey="ConfirmRecall" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="ConfirmSureRecall"  meta:resourcekey="ConfirmSureRecall" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="RecallPromptPart1"  meta:resourcekey="RecallPromptPart1" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="RecallPromptPart2"  meta:resourcekey="RecallPromptPart2" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="CannotExport"  meta:resourcekey="CannotExport" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="ExportFile"  meta:resourcekey="ExportFile" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="RecallError"  meta:resourcekey="RecallError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="RecallFileSuccess"  meta:resourcekey="RecallFileSuccess" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    
    

       
    
    
    
    

</asp:Content>
