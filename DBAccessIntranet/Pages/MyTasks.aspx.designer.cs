﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages {
    
    
    public partial class MyTasks {
        
        /// <summary>
        /// MyTasksDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksDetail;
        
        /// <summary>
        /// MyTasksDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksDescription;
        
        /// <summary>
        /// ChooseTaskType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ChooseTaskType;
        
        /// <summary>
        /// SelectStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SelectStatus;
        
        /// <summary>
        /// MyTaskStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxComboBox MyTaskStatus;
        
        /// <summary>
        /// SelectSearchOption control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SelectSearchOption;
        
        /// <summary>
        /// MySearchOption control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxComboBox MySearchOption;
        
        /// <summary>
        /// SearchText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox SearchText;
        
        /// <summary>
        /// Search control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton Search;
        
        /// <summary>
        /// Reset control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton Reset;
        
        /// <summary>
        /// ExportToExcel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExportToExcel;
        
        /// <summary>
        /// ASPxButton2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ASPxButton2;
        
        /// <summary>
        /// Tasks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView.ASPxGridView Tasks;
        
        /// <summary>
        /// ASPxButtonConfirm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxButtonConfirm;
        
        /// <summary>
        /// LoggedInUserDetailsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.LoggedInUserDetails LoggedInUserDetailsControl;
        
        /// <summary>
        /// gridExport control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
        
        /// <summary>
        /// ASPxHiddenFieldTasks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxHiddenField.ASPxHiddenField ASPxHiddenFieldTasks;
        
        /// <summary>
        /// ASPxHiddenFieldInputs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxHiddenField.ASPxHiddenField ASPxHiddenFieldInputs;
        
        /// <summary>
        /// ASPxHiddenFieldMFN control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxHiddenField.ASPxHiddenField ASPxHiddenFieldMFN;
        
        /// <summary>
        /// ASPxHiddenFieldInputsTaskRadioButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxHiddenField.ASPxHiddenField ASPxHiddenFieldInputsTaskRadioButton;
        
        /// <summary>
        /// popupDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.ASPxPopupControl popupDetails;
        
        /// <summary>
        /// PopupControlContentControl4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.PopupControlContentControl PopupControlContentControl4;
        
        /// <summary>
        /// MyTasksTaskDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksTaskDetails;
        
        /// <summary>
        /// popupRejectReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.ASPxPopupControl popupRejectReason;
        
        /// <summary>
        /// PopupControlContentControl5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.PopupControlContentControl PopupControlContentControl5;
        
        /// <summary>
        /// ReasonforRejection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ReasonforRejection;
        
        /// <summary>
        /// ASPxMemoReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxMemo ASPxMemoReason;
        
        /// <summary>
        /// ASPxAddReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxAddReason;
        
        /// <summary>
        /// ASPxCancelReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxCancelReason;
        
        /// <summary>
        /// popupInput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.ASPxPopupControl popupInput;
        
        /// <summary>
        /// PopupControlContentControl6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.PopupControlContentControl PopupControlContentControl6;
        
        /// <summary>
        /// ASPxTxtInput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox ASPxTxtInput;
        
        /// <summary>
        /// ASPxAddInput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxAddInput;
        
        /// <summary>
        /// ASPxCancelInput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxCancelInput;
        
        /// <summary>
        /// PopupTemplates control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Popups PopupTemplates;
        
        /// <summary>
        /// MyTasksNoTaskDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksNoTaskDetails;
        
        /// <summary>
        /// MyTasksUsedMiFare control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksUsedMiFare;
        
        /// <summary>
        /// MyTasksErrorTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksErrorTask;
        
        /// <summary>
        /// MyTasksProcessTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksProcessTask;
        
        /// <summary>
        /// MyTasksRejectionReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksRejectionReason;
        
        /// <summary>
        /// MyTasksReason control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksReason;
        
        /// <summary>
        /// MyTasksDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksDesc;
        
        /// <summary>
        /// MyTasksInputDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksInputDesc;
        
        /// <summary>
        /// MyTasksInvalidMiFare control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksInvalidMiFare;
        
        /// <summary>
        /// MyTasksMiFareExist control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal MyTasksMiFareExist;
    }
}
