﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class AccessAreaRecertificationStatus : BaseSecurityPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetGridDataSource();
		}

		void SetGridDataSource()
		{
			if (this.CurrentUser.RolesShortNamesList.Contains("ADMIN"))
			{
				var accessAreas = Director.GetAdminRecertifyAccessAreas(this.CurrentUser.dbPeopleID);

				ASPxGridViewAccessAreas.DataSource = accessAreas;
			}

			ASPxGridViewAccessAreas.DataBind();
		}


		/// <summary>
		/// If todays date is greater than Next Recertification date
		/// then red
		/// </summary>
		/// <param name="LandlordID"></param>
		/// <returns></returns>
		public bool setImageRedVisible(object LastCertificationDate, object NextCertificationDate)
		{
			
			return (DateTime.Now > Convert.ToDateTime(NextCertificationDate)) ? true : false;
			
		}

		/// <summary>
		/// method to set the visibility for the amber traffic light image
		/// </summary>
		/// <param name="LandlordID"></param>
		/// <returns></returns>
		public bool setImageAmberVisible(object LastCertificationDate, object NextCertificationDate)
		{
			DateTime Date_NextRecert = Convert.ToDateTime(NextCertificationDate);
			if (Date_NextRecert > DateTime.Now)
			{
				Double ndays = (Date_NextRecert - DateTime.Now).TotalDays;
				return (ndays <= 7)? true:false;			
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// method to set the visibility for the green traffic light image
		/// </summary>
		/// <param name="LandlordID"></param>
		/// <returns></returns>
		public bool setImageGreenVisible(object LastCertificationDate, object NextCertificationDate)
		{
			DateTime Date_NextRecert = Convert.ToDateTime(NextCertificationDate);
			if (Date_NextRecert > DateTime.Now)
			{
				Double ndays = (Date_NextRecert - DateTime.Now).TotalDays;
				return (ndays > 7) ? true : false;
			}
			else
			{
				return false;
			}
		}
	}
}