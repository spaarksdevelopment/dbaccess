<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="TaskTypeId" />
  <xsl:template match="/">

    <xsl:if test="$TaskTypeId = '1' or $TaskTypeId = '12' or $TaskTypeId = '13' or $TaskTypeId = '14' or $TaskTypeId = '15' or $TaskTypeId = '16' or $TaskTypeId = '17'">
      <xsl:for-each select="ROOT/row">

        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="dbPeopleID != ''">
          <div>
            <span class="TaskDetailLabel">dbPeopleID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="dbPeopleID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DBDirID != ''">
          <div>
            <span class="TaskDetailLabel">DBDirID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DBDirID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="RequestedBy != ''">
          <div>
            <span class="TaskDetailLabel">Requested By:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestedBy"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="ContactTelephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="ContactTelephone"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Justification != ''">
          <div>
            <span class="TaskDetailLabel">Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Justification"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="LesserApprovalWarning != ''">
          <div>
            <span class="TaskDetailLabel">Access Areas having fewer Approvers:</span>
            <span class="TaskDetailContent">
              <xsl:value-of disable-output-escaping="yes" select="LesserApprovalWarning"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="CardType != ''">
          <div>
            <span class="TaskDetailLabel">Card Type:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="CardType"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="SmartCardJustification != ''">
          <div>
            <span class="TaskDetailLabel">Smart Card Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="SmartCardJustification"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '2'">
      <xsl:for-each select="ROOT/row">

        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="EmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="EmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Telephone"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="(StartDate != '' or EndtDate != '')">
          <div>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                Start:
                <xsl:value-of select="StartDate"/>
              </span>
            </xsl:if>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                End:
                <xsl:value-of select="EndDate"/>
              </span>
            </xsl:if>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Justification != ''">
          <div>
            <span class="TaskDetailLabel">Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Justification"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '3'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="EmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="EmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Telephone"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <br />
        <xsl:if test="(StartDate != '' or EndtDate != '')">
          <div>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                Start:
                <xsl:value-of select="StartDate"/>
              </span>
            </xsl:if>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                End:
                <xsl:value-of select="EndDate"/>
              </span>
            </xsl:if>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Category != ''">
          <div>
            <span class="TaskDetailLabel">Category:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Category"/>
            </span>
          </div>
        </xsl:if>
        <xsl:if test="Justification != ''">
          <div>
            <span class="TaskDetailLabel">Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Justification"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '7'">
      <xsl:for-each select="ROOT/row">

        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="dbPeopleID != ''">
          <div>
            <span class="TaskDetailLabel">dbPeopleID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="dbPeopleID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DBDirID != ''">
          <div>
            <span class="TaskDetailLabel">DBDirID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DBDirID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="EmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="EmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Telephone"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <br />
        <xsl:if test="(StartDate != '' or EndtDate != '')">
          <div>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                Start:
                <xsl:value-of select="StartDate"/>
              </span>
            </xsl:if>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                End:
                <xsl:value-of select="EndDate"/>
              </span>
            </xsl:if>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Category != ''">
          <div>
            <span class="TaskDetailLabel">Category:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Category"/>
            </span>
          </div>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>


    <xsl:if test="$TaskTypeId = '4'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test = "DivisionName != ''">
          <div>
            <span class="TaskDetailLabel">Division:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DivisionName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test ="DivisionAdmin != ''">
          <div>
            <span class="TaskDetailLabel">Administrator:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DivisionAdmin"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="BusinessAreaName != ''">
          <div>
            <span class="TaskDetailLabel">Business Area Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="BusinessAreaName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '5'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="EmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="EmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Telephone"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <br />

        <xsl:if test="PassOfficeName != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="PassOfficeName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="PassOfficeEmail != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Email:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="PassOfficeEmail"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="PassOfficePhone != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Phone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="PassOfficePhone"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '6'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test ="DivisionName != ''">
          <div>
            <span class="TaskDetailLabel">Division:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DivisionName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test ="BusinessAreaName != ''">
          <div>
            <span class="TaskDetailLabel">Business Area Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="BusinessAreaName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test ="DAName != ''">
          <div>

            <span class="TaskDetailLabel">Administrator:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test ="DAEmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAEmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Telephone Number:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DATelephoneNumber"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <br />

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>

      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '8' or $TaskTypeId = '10'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test = "DivisionName != ''">
          <div>
            <span class="TaskDetailLabel">Division:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DivisionName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DAName != ''">
          <div>
            <span class="TaskDetailLabel">Administrator:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DAEmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAEmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Telephone Number:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DATelephoneNumber"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <br />

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>

      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '11'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="MSPResourceName != ''">
          <div>
            <span class="TaskDetailLabel">MSP Resource Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="MSPResourceName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="MSPCountry != ''">
          <div>
            <span class="TaskDetailLabel">MSP Resource Country:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="MSPCountry"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="POName != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Operative:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="POName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="POEmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Operative Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="POEmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office Operative Telephone Number:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="POTelephoneNumber"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <br />

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>

      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '9'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="PassOfficeName != ''">
          <div>
            <span class="TaskDetailLabel">Pass Office:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="PassOfficeName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DAName != ''">
          <div>
            <span class="TaskDetailLabel">Administrator:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAName"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="DAEmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DAEmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Administrator Telephone Number:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="DATelephoneNumber"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <br />

        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="$TaskTypeId = '18'">
      <xsl:for-each select="ROOT/row">
        <xsl:if test="RequestMasterID != ''">
          <div>
            <span class="TaskDetailLabel">Request ID:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="RequestMasterID"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Name != ''">
          <div>
            <span class="TaskDetailLabel">Name:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Name"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="EmailAddress != ''">
          <div>
            <span class="TaskDetailLabel">Email Address:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="EmailAddress"/>
            </span>
          </div>
          <br />
        </xsl:if>

        <xsl:if test="Telephone != ''">
          <div>
            <span class="TaskDetailLabel">Telephone:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Telephone"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Description != ''">
          <div>
            <span class="TaskDetailLabel">Description:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Description"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <br />
        <xsl:if test="(StartDate != '' or EndtDate != '')">
          <div>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                Start:
                <xsl:value-of select="StartDate"/>
              </span>
            </xsl:if>
            <xsl:if test="StartDate != ''">
              <span class="TaskDetailLabel">
                End:
                <xsl:value-of select="EndDate"/>
              </span>
            </xsl:if>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Comment != ''">
          <div>
            <span class="TaskDetailLabel">Comment:</span>
            <span class="TaskDetailContentComment">
              <xsl:value-of select="Comment"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="Category != ''">
          <div>
            <span class="TaskDetailLabel">Category:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Category"/>
            </span>
          </div>
        </xsl:if>
        <xsl:if test="Justification != ''">
          <div>
            <span class="TaskDetailLabel">Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="Justification"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="CardType != ''">
          <div>
            <span class="TaskDetailLabel">Card Type:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="CardType"/>
            </span>
          </div>
          <br />
        </xsl:if>
        <xsl:if test="SmartCardJustification != ''">
          <div>
            <span class="TaskDetailLabel">Smart Card Justification:</span>
            <span class="TaskDetailContent">
              <xsl:value-of select="SmartCardJustification"/>
            </span>
          </div>
          <br />
        </xsl:if>        
      </xsl:for-each>
    </xsl:if>

  </xsl:template>
</xsl:stylesheet>