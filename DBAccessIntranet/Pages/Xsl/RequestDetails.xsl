﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:for-each select="ROOT/row">

		<xsl:if test="RequestID != ''">
			<div>
				<span class="RequestDetailContent">
					<h2>Request ID <xsl:value-of select="RequestID"/></h2>
				</span>
				
			</div>
			<br />
		</xsl:if>

		<xsl:if test="Task != ''">
			<div>
				<span class="RequestDetailLabel">Task:</span>
				<span class="RequestDetailContent">
					<xsl:value-of select="Task"/>
				</span>
			</div>
			<br />
		</xsl:if>

		<xsl:if test="Applicant != ''">
			<div>
				<span class="RequestDetailLabel">Applicant:</span>
				<span class="RequestDetailContent">
					<xsl:value-of select="Applicant"/>
				</span>
			</div>
			<br />
		</xsl:if>

		<xsl:if test="RequestorName != ''">
			<div>
				<span class="RequestDetailLabel">Requestor:</span>
				<span class="RequestDetailContent">
					<xsl:value-of select="RequestorName"/>
				</span>
			</div>
			<br />
		</xsl:if>

		<xsl:if test="DateCreated != ''">
			<div>
				<span class="RequestDetailLabel">Date Created:</span>
				<span class="RequestDetailContent">
					<xsl:value-of select="DateCreated"/>
				</span>
			</div>
			<br />
		</xsl:if>
			
		<xsl:if test="Status != ''">
			<div>
				<span class="RequestDetailLabel">Status:</span>
				<span class="RequestDetailContent">
					<xsl:value-of select="Status"/>
				</span>
			</div>
			<br />
		</xsl:if>

		<xsl:if test="Status != ''">
			<div>
				<span class="TaskDetailLabel">Request History:</span>
				<span class="TaskDetailContent">
					<xsl:value-of disable-output-escaping="yes" select="History"/>
				</span>
			</div>
			<br />
		</xsl:if>

	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
