﻿using DevExpress.Web.ASPxGridView;
using Excel;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Helpers;
using Spaarks.Common.UtilityManager;
using Spaarks.CustomMembershipManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Resources;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DBAccess.Model.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class AccessRequest : BaseSecurityPage
    {
        private int _RequestMasterId;
        private int _PersonCount;
        private int _AccessCount;

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
            SetGridDataSource();
        }

        private void SetGridDataSource()
        {         
            ASPxGridView gv = ASPxGridViewRequestPersons;
            gv.DataSource = ObjectDataSourceRequestPersons;            
            ASPxGridView gva = ASPxGridViewAccessAreas;
            gva.DataSource = ObjectDataSourceAccessAreas;
        }
                
        protected void Page_Load(object sender, EventArgs e)
        {
            //page load will run on DevExpress callbacks too
            if (!IsPostBack)
            {
                ProduceRedirect();
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    _RequestMasterId = Helper.SafeInt(Request.QueryString["id"]);
                    //check master id is valid
                    if (!Director.CheckRequestMaster(_RequestMasterId))
                        _RequestMasterId = 0;
                    else
                    {
                        _PersonCount = Helper.SafeInt(Director.GetAccessRequestPersonCount(_RequestMasterId));
                        _AccessCount = Helper.SafeInt(Director.GetAccessAreasForRequestCount(_RequestMasterId));
                    }
                }

                ASPxHiddenFieldRequestDetail.Add("RequestMasterID", _RequestMasterId);
                ASPxHiddenFieldPersonDetail.Add("PersonCount", _PersonCount);
                ASPxHiddenFieldAccessDetail.Add("AccessCount", _AccessCount);
                ObjectDataSourceRequestPersons.SelectParameters["RequestMasterId"].DefaultValue = _RequestMasterId.ToString();
            }

			ASPxGridViewRequestPersons.CustomColumnDisplayText += ASPxGridViewRequestPersons_CustomColumnDisplayText;
			ASPxGridViewAccessAreas.CustomColumnDisplayText += ASPxGridViewAccessAreas_CustomColumnDisplayText;
			ASPxGridViewRequestPersons.PageIndexChanged += ASPxGridViewRequestPersons_PageIndexChanged;
			ASPxGridViewAccessAreas.PageIndexChanged += ASPxGridViewAccessAreas_PageIndexChanged; 
        }

		void ASPxGridViewAccessAreas_PageIndexChanged(object sender, EventArgs e)
		{
			ObjectDataSourceAccessAreas.SelectParameters["RequestMasterId"].DefaultValue = ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString();
			
			var accessAreas = ObjectDataSourceAccessAreas.Select();
		    if (accessAreas == null) return;

		    ASPxGridViewAccessAreas.DataSource = accessAreas;
		    ASPxGridViewAccessAreas.DataBind();
		}

		void ASPxGridViewRequestPersons_PageIndexChanged(object sender, EventArgs e)
		{
			ObjectDataSourceRequestPersons.SelectParameters["RequestMasterId"].DefaultValue = ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString();
			
			//convert the below var to datatbale and add a new column
			var nInfo = ObjectDataSourceRequestPersons.Select();
		    if (nInfo != null)
		    {
		        List<DBAccessController.DAL.vw_AccessRequestPerson> info =
		            (List<DBAccessController.DAL.vw_AccessRequestPerson>) nInfo;

		        if (ASPxHiddenFieldRequestDetail.Get("RequestMasterID") != null)
		            GetRequestApprovers(info);
		    }
		}

        private string GetRequestApprovers(List<DBAccessController.DAL.vw_AccessRequestPerson> info)
        {
            StringBuilder noApproversList = new StringBuilder();

            DataTable theSource = GenericHelpers.ToDataTable(info);
            theSource.Columns.Add("Approvers");
            theSource.Columns.Add("Country");
            theSource.Columns.Add("UBR");

            DataTable dtApproved = theSource.Clone();

            string strApprovers = string.Empty;
            //Director director = new Director(base.CurrentUser.dbPeopleID);
            foreach (DataRow item in theSource.Rows)
            {
                if (strApprovers != "" || strApprovers != "Error")
                {
                    item["Approvers"] = System.DBNull.Value;

                    dtApproved.ImportRow(item);
                }
            }
            ASPxGridViewRequestPersons.DataSource = dtApproved;
            ASPxGridViewRequestPersons.DataBind();

            return noApproversList.ToString();
        }

		void ASPxGridViewAccessAreas_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName == "Approvers")
			{
				e.DisplayText = e.Value.ToString().Replace(Environment.NewLine, "<br />");
			}
		}

		void ASPxGridViewRequestPersons_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if ((e.Column.FieldName == "Approvers") || (e.Column.FieldName == "Country") || (e.Column.FieldName == "UBR"))
			{
				e.DisplayText = e.Value.ToString().Replace(Environment.NewLine, "<br />");
			}
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ASPxDateEditFromDate.MinDate = DateTime.Today;
            ASPxDateEditFromDate.Date = DateTime.Today;

            //add necessary scripts
            base.AddHeaderScriptInclude("~/scripts/ValidationService.js");

            //if there is no request master, then hide the grids and access selection
            if (_RequestMasterId == 0)
                base.RegisterStartupScript("togglePage", "togglePage();");

            base.RegisterStartupScript("BindEvents", "BindEvents();");
        }

        #endregion

        #region WebMethods
        [WebMethod, ScriptMethod]
        public static bool DeletePersonGridRow(int requestPersonID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            bool success = director.DeleteAccessRequestPerson(requestPersonID);

            return success;
        }

        [WebMethod, ScriptMethod]
        public static bool DeleteGridRow(int accessAreaID, int requestMasterID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            bool success = director.DeleteAccessAreaRequest(accessAreaID, requestMasterID);

            return success;
        }

		[WebMethod, ScriptMethod]
		public static int AddPersonToRequest(int personID, int requestMasterID)
		{
			//no base class available when using a static method, so need to get the user
			DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            IAccessRequestService accessRequestService = new AccessRequestService(currentUser.dbPeopleID);
            bool success = accessRequestService.CreateNewRequestForPerson(personID, ref requestMasterID);

            if(success)
                return requestMasterID;

            //TODO refactor out the -1
		    return (int)accessRequestService.AccessRequestFailureReason * -1;
		}

        [WebMethod, ScriptMethod]
        public static int AddAccessAreaToRequest(int cityId, int divisionID, int accessAreaID, int requestMasterID, DateTime? startDate, DateTime? endDate)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
            IAccessRequestService accessRequestService = new AccessRequestService(currentUser.dbPeopleID);

            bool hasAccessAreaAlreadyBeenAdded = accessRequestService.CheckAccessAreaExistsForRequest(requestMasterID, accessAreaID);
            if (hasAccessAreaAlreadyBeenAdded)
                return 0;

            bool success = accessRequestService.CreateAccessAreaRequest(accessAreaID, requestMasterID, startDate, endDate);

            if (!success)
                return -2;

            return Convert.ToInt32(true);
        }

        [WebMethod, ScriptMethod]
		public static bool SubmitRequest(int requestMasterId, string commentText, int justificationID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
            Director director = new Director(currentUser.dbPeopleID);

            int? errorCode = null;

            bool success = director.SubmitRequest(requestMasterId, commentText, currentUser.Email, currentUser.Forename + ' ' +currentUser.Surname, ref errorCode);

			director.UpdateAccessJustification(requestMasterId, justificationID);

            return success;
        }

        /// <summary>
        /// Method to get the default date for the access area calculate then return a specific end date
        /// </summary>
        [WebMethod, ScriptMethod]
        public static DateTime? GetAccessEndDate(int accessAreaID)
        {
            ////no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            //get the default seetings based on the accessAreaID

            List<DBAccess.DBAccessController.DAL.AccessArea> items = director.GetAccessAreaDetails(accessAreaID);

            if (items == null) return null;

            int? duration = items[0].defaultAccessPeriod;
            string durationType = items[0].duration;

            if (duration == null || durationType == null) return null;

            DateTime accessEndDate = DateTime.Now;
            //now we have the duration Int and the type lets work out whether we are dealing with days, weeks,months, years
            switch (durationType)
            {
                case "day":
                    accessEndDate = DateTime.Today.AddDays(duration.Value);
                    break;
                case "weeks":
                    accessEndDate = DateTime.Today.AddDays(duration.Value * 7);
                    break;
                case "months":
                    accessEndDate = DateTime.Today.AddMonths(duration.Value);
                    break;
                case "years":
                    accessEndDate = DateTime.Today.AddYears(duration.Value);
                    break;
            }
            return accessEndDate;
        }
        #endregion
    
        #region ASPx Methods

        protected void ASPxGridViewRequestPersons_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ObjectDataSourceRequestPersons.SelectParameters["RequestMasterId"].DefaultValue = e.Parameters;

			//convert the below var to datatbale and add a new column
			var nInfo = ObjectDataSourceRequestPersons.Select();
			if (nInfo != null)
			{
				List<DBAccessController.DAL.vw_AccessRequestPerson> info = (List<DBAccessController.DAL.vw_AccessRequestPerson>)nInfo;

				if (ASPxHiddenFieldRequestDetail.Get("RequestMasterID") != null)
				{
					GetRequestApprovers(info);
				}
			}
        }

        protected void ASPxGridViewAccessAreas_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ObjectDataSourceAccessAreas.SelectParameters["RequestMasterId"].DefaultValue = e.Parameters;
       
			//convert the below var to datatbale and add a new column
			var nInfo = ObjectDataSourceAccessAreas.Select();
			if (nInfo != null)
			{
				ASPxGridViewAccessAreas.DataSource = nInfo;
				ASPxGridViewAccessAreas.DataBind();
			}
        }

        protected void ASPxButtonCancel_click(object sender, EventArgs e)
        {
            _RequestMasterId = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            bool success = false;

            if (_RequestMasterId > 0)
            {
                Director director = new Director(base.CurrentUser.dbPeopleID);
                success = director.CancelRequest(_RequestMasterId);
            }

            if (success)
            {
                ASPxHiddenFieldRequestDetail.Set("RequestMasterID", 0);
                ASPxHiddenFieldPersonDetail.Set("PersonCount", 0);
                ASPxHiddenFieldAccessDetail.Set("AccessCount", 0);
                RadioButtonListRequestType.SelectedIndex = -1;
                ASPxMemoNote.Text = string.Empty;               
                base.RegisterStartupScript("toggleGrid1", "togglePage();");
            }
        }

        protected void ASPxButtonNewPass_click(object sender, EventArgs e)
        {
            _RequestMasterId = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            bool success = false;
            int? newRequestmasterId = null;

            if (_RequestMasterId > 0)
            {
                Director director = new Director(base.CurrentUser.dbPeopleID);
                newRequestmasterId = director.CreateNewRequestForTransferredPersons(_RequestMasterId);

                success = newRequestmasterId.HasValue;
            }

            if (success)
            {
                Response.Redirect("NewPassRequest.aspx?id=" + newRequestmasterId.Value);
            }
            else
            {        
                ASPxHiddenFieldRequestDetail.Set("RequestMasterID", 0);
                ASPxHiddenFieldPersonDetail.Set("PersonCount", 0);
                ASPxHiddenFieldAccessDetail.Set("AccessCount", 0);
                RadioButtonListRequestType.SelectedIndex = -1;
                ASPxMemoNote.Text = string.Empty;               
                base.RegisterStartupScript("toggleGrid1", "togglePage();");
            }
        }

        #endregion

        #region Bulk Upload

        protected void btnSubmitExcelSheet_Click(object sender, EventArgs e)
        {
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(inputFileId.FileContent);

            inputFileId.FileContent.Close();

            int counter = 0;
            int maxRows = Convert.ToInt32(ConfigurationManager.AppSettings["FileUploadMaxRowCount"].ToString());

            StringBuilder sb = new StringBuilder();

            while (excelReader.Read())
            {
				if (excelReader.GetValue(0) != null)
				{
					if (IsValidDbPeopleID(excelReader.GetValue(0).ToString()))
					{
						counter++;
						string dbpValue = excelReader.GetValue(0).ToString().Replace("C", "");
						dbpValue = dbpValue.Replace("c", "");
						sb.Append(dbpValue);
						sb.Append(",");
					}
				}

                // Stop the upload at 500 rows.
                if (counter > maxRows) break;
            }

            //Check if the users exist in the dbdatabse if not display an erroe message
            if (Director.CheckIfTheUsersExist(Convert.ToString(sb)))
                ProcessUploadedUserList(sb);
            else
            {
                //display a message saying that the document has users that doesn't exist in db
                base.RegisterStartupScript("errorBoxScript", "OnCheckExistingRequests();");
            }

            base.RegisterStartupScript("togglePage", "togglePage();");
        }

        /// <summary>
        /// Check to see if we are getting valid ids in from the selected excel sheet.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool IsValidDbPeopleID(string item)
        {
            item = item.Replace("C", "");
            item = item.Replace("c", "");
            if (item.Length != 7) return false;

            double Num;

            bool isDBPeopleIDNumeric = double.TryParse(item, out Num);

            return isDBPeopleIDNumeric;
        }

        private string ProcessUploadedUserList(StringBuilder users)
        {          
            IAccessRequestService accessRequestService = new AccessRequestService(CurrentUser.dbPeopleID);

            string napproverList = string.Empty;
            _RequestMasterId = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            int newMasterId = accessRequestService.ProcessUploadedUsers(users.ToString(), _RequestMasterId, (int)DBAccessEnums.RequestType.Access);


            if (_RequestMasterId == 0)
            {
                ASPxHiddenFieldRequestDetail.Remove("RequestMasterID");
                ASPxHiddenFieldRequestDetail.Add("RequestMasterID", newMasterId);
            }

            ObjectDataSourceRequestPersons.SelectParameters["RequestMasterId"].DefaultValue = ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString();

			//convert the below var to datatbale and add a new column
			var nInfo = ObjectDataSourceRequestPersons.Select();

			if (nInfo != null)
			{
			    List<DBAccessController.DAL.vw_AccessRequestPerson> info = (List<DBAccessController.DAL.vw_AccessRequestPerson>)nInfo;

			    if (ASPxHiddenFieldRequestDetail.Get("RequestMasterID") != null)
			        napproverList = GetRequestApprovers(info);
			}

            // need to update the count of request people displayed in the grid
            int? count = Director.GetAccessRequestPersonCount(newMasterId);
            ASPxHiddenFieldPersonDetail.Set("PersonCount", (null == count) ? 0 : (int)count);

            return napproverList;
		}

        #endregion

        private void ProduceRedirect()
        {
            List<string> roleList = new List<string>();
            roleList = base.CurrentUser.RolesShortNamesList;

            DBAccessController.Managers.LoginRedirectByRoleSection roleRedirectSection = (DBAccessController.Managers.LoginRedirectByRoleSection)ConfigurationManager.GetSection("LoginRedirectByRole");

            foreach (DBAccessController.Managers.RoleRedirect roleRedirect in roleRedirectSection.RoleRedirects)
            {
                if (roleList.Contains(roleRedirect.Role))
                {
                    string[] g = roleRedirect.Url.Split('/');

                    hdnRedirect.Value = g[2];
                }
            }
        }

        public string SetLanguageText(object text)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

            return resourceManager.GetString(Convert.ToString(text));
        }
              
    }
}
