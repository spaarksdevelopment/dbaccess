﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="True" CodeBehind="MyDivisions.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyDivisions" ValidateRequest="false" uiculture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="../Controls/Selectors/UBRSelector.ascx" TagName="UBRSelector" TagPrefix="uc1" %>
<%@ Register Src="../Controls/Editors/EditBusinessAreaControl.ascx" TagName="EditBusinessAreaControl"
	TagPrefix="uc2" %>
<%@ Register Src="../Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector"
	TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">

	    var isDirty;

	    function setDirty(dirty) {
	        isDirty = dirty;
	    };

		$(function () 
		{
		    isDirty = false;

			$('#details').hide();

			var values = new Array();
			 hfAccessRequestExArray.Set("AccessRequestExArray", values);
			 hfAccessRequestArray.Set("AccessRequestArray", values);
			 hfAccessRequestArrayDA.Set("AccessRequestArrayDA", values);
			});

			function SetValuesForValidation()
			{
				var HeaderText = '<%= SetLanguageText("AddAdministrator") %>';
				var HeaderContent = '<%= SetLanguageText("SelectValidPerson") %>';
				var values = HeaderText + '|' + HeaderContent;
				hfSetValuesForValidation.Set("ValuesForValidation", values)
			}

		function PerformBusinessAreasCallback(value)
		{
			var divisionID = MyDdlDivisions.GetValue();

			if (divisionID != null) {
			    PageMethods.RemoveDraftRequestsForThisDivision(divisionID);
			    
				hdDivision.Set("DivisionID", divisionID);
				if (value == null)
				{
					cbBusinessAreas.PerformCallback(divisionID);
				}
				else if(value == 'Cancel')
				{
				    PageMethods.RemoveDivisionRoleUsersFromCache();
				}
			}
		}
		
		var postponedCallbackValue = null;
		function GetBusinessAreaValue(value)
		{
			//We are deleting a business area so don't want to bind business area detail
			//for a deleted business area and no need to toggle the panels

			if (value != null)
			{
				if (cbBusinessAreaDetail.InCallback())
					postponedCallbackValue = value;
				else
				{
					cbBusinessAreaDetail.PerformCallback(value);
					ShowBusinessAreaDetail();
				}
			}
		}

		function OnEndBusinessAreasCallback(s, e)
		{
			var BusinessAreaID = hdDivision.Get("BusinessAreaID");
			if (BusinessAreaID != null)
			{
				GetBusinessAreaValue(BusinessAreaID);
			}
		}

		function OnEndBusinessAreaDetailCallback(s, e) 
		{		
			$('#divEditBusinessArea').tabs();
			//$('#divUpdateBusinessAreabutton').hide();

			if (postponedCallbackValue != null) {
				cbBusinessAreaDetail.PerformCallback(postponedCallbackValue);
				postponedCallbackValue = null;
				ShowBusinessAreaDetail();
			}

			//Set the header value to include business area details
			try {
				labelBADetailHeader.SetText(hdBADetailHeader.Get("HeaderText"));
			}
			catch (ex) { }
		}
        
		function ShowBusinessAreaDetail() {
			$('#BusinessAreaContent').css('display', '');
			$('#BusinessAreasContent').css('display', 'none');

			$('#BusinessAreasHeader').css('display', 'none');
			$('#BusinessAreaDetailHeader').css('display', '');
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<div>
		<h1 id="BusinessAreasHeader">
			<asp:Literal ID="MyDivisions1" runat="server" meta:resourceKey="MyDivisions"></asp:Literal>
		</h1>
		<h1 style="display: none;" id="BusinessAreaDetailHeader">
			<asp:Literal ID="MyDivisions2" runat="server" meta:resourceKey="MyDivisions"></asp:Literal>
				<dx:ASPxLabel runat="server" ID="LabelBusinessAreaDetailHeader" 
				ClientInstanceName="labelBADetailHeader" style="font-size: 22px;"/>
		</h1>
	</div>
	<div id="footerLower">
	</div>
	<div id="BusinessAreasContent">
		<p>
			<asp:Literal ID="MyDivisionsTitle" runat="server" meta:resourceKey="MyDivisionsTitle"></asp:Literal>
		</p>
		<br />
		<asp:Panel runat="server" ID="ChooseDivisionPanel" SkinID="GreyBorder">
			<table>
				<tr>
					<td>
						<asp:Literal ID="SelectDivision" runat="server" meta:resourceKey="SelectDivision"></asp:Literal>
					</td>
					<td>
						<dx:ASPxComboBox ID="MyDdlDivisions" ClientInstanceName="MyDdlDivisions" runat="server"
							TextField="Name" ValueField="DivisionId" Width="300px" DropDownHeight="250px" ValueType="System.String">
							<ClientSideEvents SelectedIndexChanged="function(s,e){ PerformBusinessAreasCallback(null); }" />
						</dx:ASPxComboBox>
					</td>
				</tr>
			</table>
			<br />
		</asp:Panel>
		<dx:ASPxCallbackPanel ID="ASPxCallbackPanelBusinessAreas" ClientInstanceName="cbBusinessAreas"
			runat="server" Width="800px" OnCallback="ASPxCallbackPanelBusinessAreas_Callback">
			<ClientSideEvents EndCallback="OnEndBusinessAreasCallback" />
			<PanelCollection>
				<dx:PanelContent>
					<asp:Panel runat="server" ID="DivisionPanel">
						
					</asp:Panel>
					<dx:ASPxHiddenField ID="ASPxHiddenFieldDivision" ClientInstanceName="hdDivision"
						runat="server" />
				</dx:PanelContent>
			</PanelCollection>
		</dx:ASPxCallbackPanel>
	</div>
	<br />
	<div id="BusinessAreaContent" class='Content' style="display: none;">
		<div>
			<h3>
				<b><asp:Literal ID="DivisionDetails" runat="server" meta:resourceKey="DivisionDetails"></asp:Literal></b>
			</h3>
		</div>
		<dx:ASPxCallbackPanel ID="ASPxCallbackPanelEditBusinessArea" ClientInstanceName="cbBusinessAreaDetail"
			runat="server" Width="1200px" 
			OnCallback="ASPxCallbackPanelEditBusinessArea_Callback">
			<ClientSideEvents EndCallback="OnEndBusinessAreaDetailCallback" />
			<PanelCollection>
				<dx:PanelContent>
					<uc2:EditBusinessAreaControl ID="EditBusinessAreaControl1" runat="server" />
				
					<dx:ASPxHiddenField ID="ASPxHiddenBADetailHeader" ClientInstanceName="hdBADetailHeader" runat="server" />
				</dx:PanelContent>
			</PanelCollection>
		</dx:ASPxCallbackPanel>
	</div>
	
	<dx:ASPxPopupControl ID="PopupAddDivisionAdmin" ClientInstanceName="popupAddDivisionAdmin"
		runat="server" SkinID="PopupCustomSize" Width="500px" Height="250px" 
		HeaderText="" 
		meta:resourcekey="PopUpAddDivisionAdminstrator">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
				<h2>
					<asp:Literal ID="AddDivisionAdministrator" runat="server"></asp:Literal>
				</h2>
				<div><asp:Literal ID="EnterPersonDetails" runat="server"></asp:Literal>
				</div>
				<br />
				<uc3:PersonSelector ID="PersonSelector3" ServiceMethod="GetPersonCompletionListWithValidIds"
					ControlIndex="3" runat="server" />
				<br />
				<br />
				<br />
				<div style="text-align: left; clear: both;">
					<div id="divSaveDA" style="float:left;">
						<dx:ASPxButton ID="btnAddDivisionAdmin" runat="server" AutoPostBack="false" 
							Text="" meta:resourcekey="btnConfirmSave">
							<ClientSideEvents Click="function(s,e){ popupAddDivisionAdmin.Hide(); SetValuesForValidation(); AddDivisionAdmin(); }" />
						</dx:ASPxButton>
					</div>
					<div id="divCancelDA" style="margin-left: 5px; float:left;">
						<dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="false" Text="" 
							ForeColor="#0098db" meta:resourcekey="btnConfirmCancel">
							<ClientSideEvents Click="function(s,e){ popupAddDivisionAdmin.Hide(); }" />
						</dx:ASPxButton>
					</div>
				</div>
				<div style="clear: both;">
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<ppc:Popups ID="PopupTemplates" runat="server" />
	<dx:ASPxHiddenField ID="ASPxHiddenFieldRequestDetail" ClientInstanceName="hfRequestDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFiedPeopleDetail" ClientInstanceName="hfPeoleDetail"
		runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestExId" ClientInstanceName="hfAccessRequestExId"
		runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestId" ClientInstanceName="hfAccessRequestId"
		runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestIdDA" ClientInstanceName="hfAccessRequestIdDA"
		runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestExArray" ClientInstanceName="hfAccessRequestExArray" runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestArray" ClientInstanceName="hfAccessRequestArray" runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestArrayDA" ClientInstanceName="hfAccessRequestArrayDA" runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldValuesForValidation" ClientInstanceName="hfSetValuesForValidation" runat="server">
	</dx:ASPxHiddenField>

</asp:Content>
