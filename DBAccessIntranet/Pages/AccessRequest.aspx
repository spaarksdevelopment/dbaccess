﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
    AutoEventWireup="true" CodeBehind="AccessRequest.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.AccessRequest"
    EnableViewState="false" UICulture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="../Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector"
	TagPrefix="uc1" %>
<%@ Register Src="../Controls/Selectors/BusinessAccessSelector.ascx" TagName="BusinessAccessSelector"
	TagPrefix="uc4" %>
<%@ Register Src="../Controls/Selectors/ServiceAccessSelector.ascx" TagName="ServiceAccessSelector"
	TagPrefix="uc5" %>
<%@ Register Src="../Controls/Selectors/ProfileAccessSelector.ascx" TagName="ProfileAccessSelector"
	TagPrefix="uc6" %>
<%@ Register Src="~/Controls/Selectors/MapAndLocationSelector.ascx" TagName="MapLocation"
	TagPrefix="uc8" %>
<%@ Register Src="../Controls/Selectors/AccessRequestJustificationSelector.ascx"
	TagName="AccessRequestJustificationSelector" TagPrefix="uc9" %>
<%@ Register Assembly="Brettle.Web.NeatUpload" Namespace="Brettle.Web.NeatUpload"
	TagPrefix="Upload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">
        $(document).ready(function () {

            // Add a method to the date object to create a string that ASP can recognise
		    (function () {

		        function pad(number) {
		            var r = String(number);
		            if (r.length === 1) {
		                r = '0' + r;
		            }
		            return r;
		        }

		        Date.prototype.toASPString = function () {
		            return this.getFullYear()
                        + '-' + pad(this.getMonth() + 1)
                        + '-' + pad(this.getDate());
		        };

		    }());

			hfPersonDetail.Set("Visitor", "false");
			//persons help
            $('#PersonsHelp').bind('click', function () {
				$("#PersonsHelpInfo").slideToggle("slow");
			});
			$("#PersonsHelpInfo").hide();

			//help section access selection
            $('#HelpAccessSelection').bind('click', function () {
				$("#HelpSelection").slideToggle("slow");
			});
			$("#HelpSelection").hide();

			// Get the person count if we have transferred here from pass request.
			var personCount = hfPersonDetail.Get("PersonCount");
            if (personCount > 0) {
				lblPersonsHeading.SetText(personCount + " selected");
			}
			//hide the content sections
			$('#AccessContent').hide();
			$('#LocationContent').hide();
			$('#SelectionContent').hide();
			$('#NoteContent').hide();

			$('#MapImages').hide();
			$('#BreadCrumbsMap').hide();

			//toggle the class for persons to highlight the section
			$('#PersonsContentHeader').toggleClass("Selected");

			//toggle events
            $('#PersonsContentHeader').bind('click', function () {
				$("#PersonsContent").slideToggle("slow");
				$('#PersonsContentHeader').toggleClass("Selected");
			});
            $('#AccessContentHeader').bind('click', function () {
				$("#AccessContent").slideToggle("slow");
				$("#AccessContentHeader").toggleClass('Selected');
			});
            $('#LocationContentHeader').bind('click', function () {
				$("#LocationContent").slideToggle("slow");
				$('#MapImages').slideToggle("slow");
				$('#BreadCrumbsMap').slideToggle("slow");
				$("#LocationContentHeader").toggleClass('Selected');
				$('#SelectionContentAccessSelection').slideToggle("slow");
			});
            $('#SelectionContentHeader').bind('click', function () {
				$("#SelectionContent").slideToggle("slow");
				$("#SelectionContentHeader").toggleClass('Selected');
			});
            $('#NoteContentHeader').bind('click', function () {
				$("#NoteContent").slideToggle("slow");
				$("#NoteContentHeader").toggleClass('Selected');
			});

                        
		});

		function BindEvents() {
			$('#cityDropDownList').bind('change', function () {

				var index = this.selectedIndex;
				if (index > 0) {
					var locationId = this.options[index].value;
					var location = this.options[index].text;

					//set hidden field
					hfLocationDetail.Set("CityID", locationId);
					hfLocationDetail.Set("CityString", location);

					//set heading
					lblLocationHeading.SetText(location + " is selected");

					//set building list
					SetAccessSelector(locationId);
				} else {
					//set hidden field
					hfLocationDetail.Set("CityID", 0);
					hfLocationDetail.Set("CityString", '');

					//clear heading
					lblLocationHeading.SetText('');

					//clear building list
					SetAccessSelector('');
				}
			});
		}

		function ShowExcelUpload() {
			popupExcelUpload.Show();
		}

		function ShowPostSubmit() {
		    if (showSubmitFlag)
                popupPostSubmit.Show();
		}

		function ValidateFileType(data) {
			data = data.replace(/^\s|\s$/g, ""); //trims string

			if (data.match(/([^\/\\]+)\.(xlsx)$/i)) {
				return true;
			}
			else {
				return false;
			}
		}
		function ValidateUploadFile(Source, args) {
			var chkFile = document.getElementById('<%= inputFileId.ClientID %>');

			if (chkFile.value == "") {
				args.IsValid = false;
				popupExcelUpload.Show();
			}
			else {
				if (ValidateFileType(chkFile.value)) {
					args.IsValid = true;
					popupExcelUpload.Hide();
				}
				else {
					args.IsValid = false;
					popupExcelUpload.Show();
				}
			}
		}

		function DeletePerson(personId) {
			PageMethods.DeletePersonGridRow(personId, OnDeletePerson);
		}

		function OnGetApprovers() {
		    popupAlertTemplateContent.SetText("<%=AccessRequestNotFindApprovers.Text %>");
		    popupAlertTemplate.SetHeaderText("<%=AccessRequestApprover.Text%>");
			popupAlertTemplate.Show();
			return;
		}

		function OnDeletePerson(result) {
			if (!result) {
				popupAlertTemplateContent.SetText("<%=AccessRequestPersonNotDeleted.Text%>");
				popupAlertTemplate.SetHeaderText("<%=DeletePersonLiteral.Text%>");
				popupAlertTemplate.Show();
				return;
			}

			var personCount = hfPersonDetail.Get("PersonCount") - 1;
			hfPersonDetail.Set("PersonCount", personCount);

			if (personCount > 0) {
				lblPersonsHeading.SetText(personCount + "  " + "<%=AccessRequestSelected.Text%>");
			}
			else {
				lblPersonsHeading.SetText("");
			}

			gvRequestPersons.PerformCallback(hfRequestDetail.Get("RequestMasterID"));
			togglePage();
		}

		function Delete(areaId) {
			PageMethods.DeleteGridRow(areaId, hfRequestDetail.Get("RequestMasterID"), OnDelete);
		}

		function OnDelete(result) {
			var requestId = hfRequestDetail.Get("RequestMasterID");
			gvAccessAreas.PerformCallback(requestId);

			var accessCount = hfAccessDetail.Get("AccessCount");
			accessCount--;
		}

		function AddPerson(s, e) {

			//first validate the selected person
			var idstring = $get("PersonSelectorNameTextBox").value;

			var personId = 0;
			if (idstring.length > 0) {
				personId = GetPersonIdFromIdentifier(idstring);
			}

			if (personId == 1) {
			    popupAlertTemplateContent.SetText("<%=AccessRequestPersonInvalidEmail.Text %>");
			    popupAlertTemplate.SetHeaderText("<%=AddPersonLiteral.Text %>");
				popupAlertTemplate.Show();
				return;
			}

			if (personId == 0) {
			    popupAlertTemplateContent.SetText("<%=SelectValidPerson.Text %>");
			    popupAlertTemplate.SetHeaderText("<%=AddPersonLiteral.Text %>");
				popupAlertTemplate.Show();
				return;
			}

			//save details temporarily before proceeding
			hfPersonDetail.Set("PersonID", personId);
			hfPersonDetail.Set("PersonString", idstring);

			//Clear the text box
			$get("PersonSelectorNameTextBox").value = '';

			//if not, add to request
			var requestId = hfRequestDetail.Get("RequestMasterID");

			PageMethods.AddPersonToRequest(personId, requestId, OnAddPersonToRequest);
		}

		var p_result = "";
		function OnAddPersonToRequest(result) {
			if (result < 0) {
				var sText = '';
				switch (result) {
				    case -1://Exception
                    case -2://Invalid RequestMasterID
					    sText = "<%=AccessRequestAccessNotAdded.Text %>";
						break;
				    case -3://Already on Request
					    sText = "<%=AccessRequestAlreadyAddedError.Text%>";
						break;
				}

				popupAlertTemplateContent.SetText(sText);
				popupAlertTemplate.SetHeaderText("<%=AccessRequestAddPerson.Text%>"); 
				popupAlertTemplate.Show();
				return;
			}

			p_result = result;
			hfRequestDetail.Set("RequestMasterID", result);

			//get the current person count and store in case needed elsewhere
			var personCount = GetRequestPersonCount(result);
			hfPersonDetail.Set("PersonCount", personCount);

			lblPersonsHeading.SetText(personCount + "  " + "<%=AccessRequestSelected.Text%>");

			gvRequestPersons.PerformCallback(p_result);
			togglePage();
		}

		function OnCheckRequestApprover(result) {
			gvRequestPersons.PerformCallback(p_result);
			togglePage();
		}


		function togglePage() {
			var requestId = hfRequestDetail.Get("RequestMasterID");

			if (requestId > 0) {

				$("#divRequestPersons").show();

				$("#divAccess0").hide();
				$("#divAccess1").hide();
				$("#divAccess2").hide();
				$("#divAddAccess").hide();
				$("#divAccessGrid").hide();
			}
			else {
				$("#divRequestPersons").hide();
				$("#divAccess0").hide();
				$("#divAccess1").hide();
				$("#divAccess2").hide();
				$("#divAddAccess").hide();
				$("#divAccessGrid").hide();
			}
		}

		function SetRequestType(s, e) {
			var requestType = s.GetSelectedIndex();
			hfAccessDetail.Set("RequestType", requestType);

			locationId = hfLocationDetail.Get("CityID");

			var requestTypeText = '';

			switch (requestType) {
				case 0:
                    requestTypeText = ('<%= SetLanguageText("BusinessArea") %>');
					$("#divAccess0").show();
					$("#divAccess1").hide();
					$("#divAccess2").hide();
					$("#divAddAccess").hide();
					break;
				case 1:
                    requestTypeText = '<%= SetLanguageText("ServiceAccess") %>';
					$("#divAccess0").hide();
					$("#divAccess1").show();
					$("#divAccess2").hide();
					$("#divAddAccess").hide();
					break;
				case 2:
                    requestTypeText = '<%= SetLanguageText("DivisionProfile") %>';
					$("#divAccess0").hide();
					$("#divAccess1").hide();
					$("#divAccess2").show();
					$("#divAddAccess").show();
					break;
			}

			if (!parseInt(locationId) > 0) {
				$("#divAccess0").hide();
				$("#divAccess1").hide();
				$("#divAccess2").hide();
				$("#divAddAccess").hide();
			}
			lblTypeHeading.SetText(requestTypeText);

			//check if a city was selected and set the access selector if so

			if (parseInt(locationId) > 0) SetAccessSelector(locationId);
		}

		function SetAccessSelector(cityId) {
			var requestType = hfAccessDetail.Get("RequestType");
			switch (requestType) {
				case 0:
					setBusinessAccessSelector(cityId);
					break;
				case 1:
					setServiceAccessSelector(cityId);
					break;
				case 2:
					setProfileAccessSelector(cityId);
					break;
			}

			$("#divAddAccess").hide();
		}



		var a_AccessAreaID = "";
        function AddAccess(s, e) {
			ASPxCallbackPanel1.PerformCallback();

			//get masterid and request type
			var requestId = hfRequestDetail.Get("RequestMasterID");
			var requestType = hfAccessDetail.Get("RequestType");
			var personCount = hfPersonDetail.Get("PersonCount");
			var startDate = deFromDate.GetValue();
			var endDate = deToDate.GetValue();
			var success = false;
			var accessAreaId = 0;
			var divisionId = -1;
			var cityId = hfLocationDetail.Get("CityID");

		    ///Mahmoud.Ali, Check that endDate is greater than startDate///
		
			    if (endDate < startDate && endDate != null) {
			        popupAlertTemplateContent.SetText("<%=AccessRequestEndDateError.Text %>");
			        popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddAccess") %>');
			        popupAlertTemplate.Show();
			        return;
			    }

			if (personCount < 1) {
				popupAlertTemplateContent.SetText("<%=AccessRequestNeedAddPers.Text %>"); //Please add at least one person before adding access.
			    popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddAccess") %>');
				popupAlertTemplate.Show();
				return;
			}

			switch (requestType) {
				case 0:
					accessAreaId = lbAccessArea.GetValue();

					if (accessAreaId == null) {
						popupAlertTemplateContent.SetText("<%=AccessRequestSelectArea.Text %>"); // Please select an Access Area to add.
					    popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddAccess") %>');
						popupAlertTemplate.Show();
						return;
					}

					break;
				case 1:

					accessAreaId = lbServiceAccess.GetValue();

					if (accessAreaId == null) {
						popupAlertTemplateContent.SetText("<%=AccessRequestNeedServiceArea.Text %>");  //Please select a Service Access Area to add.
					    popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddAccess") %>');
						popupAlertTemplate.Show();
						return;
					}

					break;
				case 2:
					cityId = hfLocationDetail.Get("CityID");
					divisionId = lbProfile.GetValue();
					break;
			}

		    var startDateString = startDate.toASPString();
		    var endDateString = endDate == null ? null : endDate.toASPString();
            
			a_AccessAreaID = accessAreaId;
			PageMethods.AddAccessAreaToRequest(cityId, divisionId, accessAreaId, requestId, startDateString, endDateString, OnAddAccessToRequest);
		}

		function OnAddAccessToRequest(result) {
			switch (result) {
				case 0: //success
					popupAlertTemplateContent.SetText("<%=AccessRequestAccessNotAdded.Text %>"); // The access could not be added.
					popupAlertTemplate.SetHeaderText("<%=AccessRequestAddAccess.Text%>");
					popupAlertTemplate.Show();
					return;
				case -2: //insufficient approvers for this area
					popupAlertTemplateContent.SetText("<%=AccessRequestNotFindAccessApprover.Text %>");
					popupAlertTemplate.SetHeaderText("<%=AccessRequestAddAccess.Text%>");
					popupAlertTemplate.Show();
					return;

			}
			//Lock request type selection
			rblRequestType.SetEnabled(false);
			//show access grid
			$("#divAccessGrid").show();
			//refresh access grid   
			var requestId = hfRequestDetail.Get("RequestMasterID");

			gvAccessAreas.PerformCallback(requestId);

			var accessCount = GetRequestAccessCount(requestId);
			hfAccessDetail.Set("AccessCount", accessCount);

		}

		function OnCheckAccessApprover(result) {
			var requestId = hfRequestDetail.Get("RequestMasterID");
			if (result == false) {
			    var sText = "<%=AccessRequestNotFindAccessApprovers.Text %>";//  Sorry! cannot find Access Approvers."

				popupAlertTemplateContent.SetText(sText);
				popupAlertTemplate.SetHeaderText("<%=AccessRequestAddArea.Text %>"); //Add Area to Request
				popupAlertTemplate.Show();
				return;
			}
			else {
				gvAccessAreas.PerformCallback(requestId);
				var accessCount = GetRequestAccessCount(requestId);
				hfAccessDetail.Set("AccessCount", accessCount);
			}
		}

		function OnCheckExistingRequests() {
		    popupAlertTemplateContent.SetText("<%=AccessRequestUsersNotAdded.Text %>"); //Some of the users in the spread sheet don't exist, Please ensure all users exist in the Database
		    popupAlertTemplate.SetHeaderText("<%=AccessRequestExcelUpload.Text %>");  //Excell Upload Access Request
			popupAlertTemplate.Show();
			return;
		}

		function OnCheckExistingListRequests(neAproverslist) {
		    popupAlertTemplateContent.SetText(neAproverslist + "<%=AccessRequestSupportText.Text %>");  //have no approvers
		    popupAlertTemplate.SetHeaderText("<%=AccessRequestExcelUpload.Text %>");
			popupAlertTemplate.Show();
			return;
		}

		function bulkSuccess() {
			$("#divAccess0").hide();
			$("#divAccess1").hide();
			$("#divAccess2").hide();
			$("#divAddAccess").hide();
			$("#divAccessGrid").hide();
		}

		function submitRequest() {
			var requestId = hfRequestDetail.Get("RequestMasterID");
			var doSubmit = true;
			var accessRequestJustificationID = $("#AccessRequestJustificationSelectorDropDownList").attr("selectedIndex");
			var additionalInfo = memoNote.GetValue();
			var requestType = rblRequestType.GetSelectedIndex();
			var errorMessage = "";
			var personCount = GetRequestPersonCount(requestId);
			var accessCount = GetRequestAccessCount(requestId);

			if (accessRequestJustificationID == 0) {
			    errorMessage += '<%= AccessRequestOptionJustification.Text %>'// "Please select an option for justification of access.<br />\n";
				doSubmit = false;
			}


			if (accessRequestJustificationID == 8) {
				if (additionalInfo == null) {
                    popupAlertTemplate.SetHeaderText('<%= SetLanguageText("Submit")%>');
					popupAlertTemplateContent.SetText("<%=AccessRequestEnterComment.Text %>"); //Please enter comments for the justification of access.
					popupAlertTemplate.Show();
					return;

				}
			}

			if (personCount == 0) {
			    errorMessage += "<%=AccessRequestAddPersonError.Text %>"; //Please add at least one Person<br />\n
				doSubmit = false;
			}

			if (requestType < 0) {
			    errorMessage += "<%=AccessRequestRequestTypeError.Text %>"; //Please select the type of request<br />\n
				doSubmit = false;
			}

			if (accessCount == 0) {
			    errorMessage += "<%=AccessRequestAccessAreaError.Text %>"; //Please add at least one Access Area<br />\n
				doSubmit = false;
			}

			if (doSubmit) {

				$("#divSubmit").hide();
				$("#divCancel").hide();
				ShowPostSubmit();

				PageMethods.SubmitRequest(requestId, additionalInfo, accessRequestJustificationID, OnSubmitRequest);


			}
			else {
				popupAlertTemplateContent.SetText(errorMessage);
                popupAlertTemplate.SetHeaderText('<%=SetLanguageText("AddAccess") %>');
				popupAlertTemplate.Show();
			}
		}

	    //The popupPostSubmit control was intermittently displaying after the request submitted popup.
        //Added a flag in order to prevent this from happening again
	    var showSubmitFlag = true;

	    function OnSubmitRequest(result) {
	        showSubmitFlag = false;

			if (!result)
			{
				popupPostSubmit.Hide();
				popupAlertTemplateContent.SetText("<%=AccessRequestAccessNotSubmitted.Text %>");
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddAccess") %>');
				popupAlertTemplate.Show();
				$("#divSubmit").show();
				$("#divCancel").show();
				return;
			}

			//check if any persons have no badge
			var requestId = hfRequestDetail.Get("RequestMasterID");
			var personCount = GetAccessRequestPersonsWithoutBadgeCount(requestId);

			if (personCount > 0)
				$("#divPersonsNoBadge").show();
			else
				$("#divPersonsNoBadge").hide();

			//hide inProgress popup
			popupPostSubmit.Hide();

			//show popup
			popupSubmitted.Show();
		}

		function ConfirmCancel() {
			popupConfirmCancel.Show();
		}
		function RedirectToHomePage() {
			window.location = $("#<%=hdnRedirect.ClientID %>").val();
		}

		function onGetAccessEndDate(result) {
			deToDate.SetValue(result);

			deToDate.SetEnabled(true);
			$('#divAddAccess').show();
		}

		function SetFile() {
		    var fileInput = document.getElementById("inputFileId");
		    TextBoxFile.SetValue(fileInput.value);
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<h1>
		<asp:Literal ID="AccessRequestLabel" runat="server" meta:ResourceKey="AccessRequestLabel"></asp:Literal>
	</h1>
	<p>
		<asp:Literal ID="RequestAccessDescription" runat="server" meta:ResourceKey="RequestAccessDescription"></asp:Literal>
	</p>
	<br />
	<div id="PersonsContentHeader" class='Header'>
		<asp:Literal ID="WhoIsTheAccess" runat="server" meta:ResourceKey="WhoIsTheAccess"></asp:Literal>
		<dx:ASPxLabel runat="server" ID="ASPxLabelPersonsHeading" ClientInstanceName="lblPersonsHeading"
			CssClass="lightblue_8" />
	</div>
	<div id="PersonsContent" class="Content">
		<div style="margin: 10px; width: 100%; float: left;">
			<asp:Table runat="server" ID="LocationTable" SkinID="FormTable">
				<asp:TableRow ID="TableRow0" runat="server">
					<asp:TableCell ID="TableCell11" runat="server">
                        <div>
                            <asp:Literal ID="BulkRequestStartText" runat="server" meta:resourcekey="BulkRequestStartText"></asp:Literal>
                            <a onclick="ShowExcelUpload()" href="javascript:void(0);">
                                <asp:Literal ID="BulkRequestHereText" runat="server" meta:resourcekey="BulkRequestHereText"></asp:Literal></a><asp:Literal ID="BulkRequestEndText" runat="server" meta:resourcekey="BulkRequestEndText"> </asp:Literal>
						</div>
                        <br />
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow ID="TableRow6" runat="server" SkinID="FormTableItem">
					<asp:TableCell ID="TableCell1" runat="server">
						<div style="margin-bottom: 5px;">
							<div style="float: left; margin-top: 8px;">
                                <asp:Literal ID="lookUpPerson" meta:resourcekey="lookUpPerson" runat="server"></asp:Literal>
                                &nbsp;
                            </div>
							<div style="float: left;">
								<uc1:PersonSelector ID="PersonSelector1" ServiceMethod="GetPersonCompletionList"
									ShowTerminated="false" runat="server" />
							

							</div>
							<dx:ASPxButton ID="ASPxButtonAddPerson" AutoPostBack="false" runat="server" Text=""
								Style="margin-top: 3px; float: left;" meta:resourcekey="ASPxButtonAddPerson">
								
<ClientSideEvents Click="AddPerson" />
							
</dx:ASPxButton>
							

							<div style="margin-left: 670px;">
							</div>
						</div>
					</asp:TableCell>
				</asp:TableRow>
			</asp:Table>
		</div>
	</div>
	<br />
	<br />
	<div id="divRequestPersons">
		<dx:ASPxGridView ID="ASPxGridViewRequestPersons" ClientInstanceName="gvRequestPersons"
			runat="server" AutoGenerateColumns="False" KeyFieldName="RequestPersonID" OnCustomCallback="ASPxGridViewRequestPersons_CustomCallback"
			ClientIDMode="AutoID" Width="70%">
			<Columns>
				<dx:GridViewDataImageColumn VisibleIndex="0">
					<DataItemTemplate>
                        <img src="../App_Themes/DBIntranet2010/images/trash.gif" onclick='<%# "DeletePerson("+Container.KeyValue+")" %>' alt="<%$ Resources:CommonResource, Delete%>" title="<%$ Resources:CommonResource, Delete%>" runat="server" />
					</DataItemTemplate>
				</dx:GridViewDataImageColumn>
				<dx:GridViewDataTextColumn FieldName="Forename" ShowInCustomizationForm="True"
					VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnForename">
				</dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Surname" ShowInCustomizationForm="True"
					VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnSurname">
				</dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PersonUBR" ShowInCustomizationForm="True"
					VisibleIndex="9" meta:resourcekey="GridViewDataTextColumnUBR">
				</dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CountryName" ShowInCustomizationForm="True"
					VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnPersonCountry">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn FieldName="EmailAddress" VisibleIndex="13" 
					meta:resourcekey="GridViewDataTextColumnEmailAddress">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn FieldName="Approvers"  
					VisibleIndex="15" Visible="false" 
					meta:resourcekey="GridViewDataTextColumnPersonApprovers">
				</dx:GridViewDataTextColumn>
			</Columns>
            <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>" />
		</dx:ASPxGridView>
	</div>
	<div id="AccessContentHeader" class='Header'>
		<asp:Literal ID="WhatType" runat="server" meta:ResourceKey="WhatType"></asp:Literal>
		<dx:ASPxLabel runat="server" ID="ASPxLabelTypeHeading" ClientInstanceName="lblTypeHeading"
			CssClass="lightblue_8" />
	</div>
	<div id="AccessContent" class='Content'>
		<p>
            &nbsp;
            <asp:Literal ID="AccessRequired" runat="server" meta:resourcekey="AccessRequired"></asp:Literal>
		</p>
		<asp:Table runat="server" ID="AccessList">
			<asp:TableRow ID="TableRow5" runat="server">
				<asp:TableCell ID="TableCell7" runat="server">
					<dx:ASPxRadioButtonList ID="RadioButtonListRequestType" ClientInstanceName="rblRequestType"
						runat="server">
						
<Items>
							
<dx:ListEditItem Selected="false" Value="0" 
		Text="" 
		meta:resourcekey="ListEditItemBusinessAccess" />
							
<dx:ListEditItem Selected="false" Value="1" 
		Text="" 
		meta:resourcekey="ListEditItemServiceAccess" />
						
</Items>
						
<ClientSideEvents ValueChanged="SetRequestType" />
					
</dx:ASPxRadioButtonList>
				
</asp:TableCell>
			</asp:TableRow>
		</asp:Table>
	</div>
	<div id="LocationContentHeader" class='Header'>
		<asp:Literal ID="WhereWould" runat="server" meta:ResourceKey="WhereWould"></asp:Literal>		
		<dx:ASPxLabel runat="server" ID="ASPxLocationHeading" ClientInstanceName="lblLocationHeading"
			CssClass="lightblue_8" />
	</div>
	<div id="LocationContent" class='Content'>
		<uc8:MapLocation ID="Map3" runat="server" />
	</div>
	<div id="SelectionContentAccessSelection" class='Content'>
		<div class="ClearBoth">
			<div id="divAccess0" style="float: left">
				<uc4:BusinessAccessSelector ID="BusinessAccessSelector1" runat="server" />
			</div>
			<div id="divAccess1" style="float: left">
				<uc5:ServiceAccessSelector ID="ServiceAccessSelector1" runat="server" />
			</div>
			<div id="divAccess2" style="float: left">
				<uc6:ProfileAccessSelector ID="ProfileAccessSelector1" runat="server" />
			</div>
			<div id="divAddAccess" style="float: left;">
			<div class="clearBoth">
                    <div id="float:left;width:200px;" style="float: left; width: 400px; margin-top: 6px;" class="marginB10 marginL10">
						<div class="form_label">
							<asp:Literal ID="AccessDates" meta:resourcekey="AccessDates" runat="server"></asp:Literal>
						</div>
						<br />
						<asp:Table runat="server" ID="Table1" SkinID="FormTable" Width="190px">
					<asp:TableRow ID="TableRow2" runat="server">
						<asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableLabel">
							<asp:Literal ID="From" runat="server" meta:resourceKey="From"></asp:Literal>
							
							<dx:ASPxDateEdit ID="ASPxDateEditFromDate" ClientInstanceName="deFromDate" runat="server"
								EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy">
							</dx:ASPxDateEdit>
						
</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="TableRow1" runat="server">
						<asp:TableCell ID="TableCell5" runat="server" SkinID="FormTableLabel">
							<asp:Literal ID="To" runat="server" meta:resourceKey="To"></asp:Literal> 
							<dx:ASPxDateEdit ID="ASPxDateEditToDate" ClientInstanceName="deToDate" runat="server"
								EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy">
							</dx:ASPxDateEdit>
							

							<br />
							<p class="grey_8">
								<asp:Literal ID="LeaveBlank" runat="server" meta:resourceKey="LeaveBlank"></asp:Literal> 
							</p>
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="TableRow11" runat="server">
						<asp:TableCell ID="TableCell6" runat="server" Width="30px" 
							SkinID="FormTableLabel">
							<dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" 
							ClientInstanceName="ASPxCallbackPanel1" runat="server" Width="200px">
								<PanelCollection>
									
<dx:PanelContent>
										<dx:ASPxButton ID="ASPxButtonAddAccess" 
		AutoPostBack="false" runat="server" Text="" 
		meta:resourcekey="ASPxButtonAddAccess">
											
<ClientSideEvents Click="AddAccess" />
										
</dx:ASPxButton>
									

									</dx:PanelContent>
								
</PanelCollection>
							
</dx:ASPxCallbackPanel>
						
</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
					</div>
			</div>
			</div>
		</div>
	</div>
	<br />

    <div id="divAccessGrid" class="ClearBoth margin10">
		<!-- Grid: Access Areas -->
		<dx:ASPxGridView ID="ASPxGridViewAccessAreas" 
			ClientInstanceName="gvAccessAreas" Width="70%"
			runat="server" AutoGenerateColumns="False" KeyFieldName="AccessAreaID" 
			OnCustomCallback="ASPxGridViewAccessAreas_CustomCallback">
			<SettingsBehavior AllowFocusedRow="True" />
			<Columns>
				<dx:GridViewDataImageColumn FieldName="" VisibleIndex="0">
					<DataItemTemplate>
                        <img src="../App_Themes/DBIntranet2010/images/trash.gif" onclick='<%# "Delete("+Container.KeyValue+")" %>' alt="<%$ Resources:CommonResource, Delete%>" title="<%$ Resources:CommonResource, Delete%>" runat="server">
					</DataItemTemplate>
				</dx:GridViewDataImageColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="CountryName" ShowInCustomizationForm="True"
					VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnCountry">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="CityName" ShowInCustomizationForm="True"
					VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnCity">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="BuildingName" ShowInCustomizationForm="True"
					VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnBuilding">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="FloorName" ShowInCustomizationForm="True"
					VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnFloor">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="AccessAreaName" ShowInCustomizationForm="True"
					VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnAccessArea">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="" FieldName="AccessAreaTypeName" ShowInCustomizationForm="True"
					VisibleIndex="7" meta:resourcekey="GridViewDataTextColumnType">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataDateColumn Caption="" FieldName="StartDate" ShowInCustomizationForm="True"
					VisibleIndex="8" PropertiesDateEdit-DisplayFormatString="{0:dd-MMM-yyyy}" 
					meta:resourcekey="GridViewDataDateColumnAccessFrom">
<PropertiesDateEdit DisplayFormatString="{0:dd-MMM-yyyy}"></PropertiesDateEdit>
				</dx:GridViewDataDateColumn>
				<dx:GridViewDataDateColumn Caption="" FieldName="EndDate" ShowInCustomizationForm="True"
					VisibleIndex="9" PropertiesDateEdit-DisplayFormatString="{0:dd-MMM-yyyy}" 
					meta:resourcekey="GridViewDataDateColumnAccessTo">
<PropertiesDateEdit DisplayFormatString="{0:dd-MMM-yyyy}"></PropertiesDateEdit>
				</dx:GridViewDataDateColumn>
				<dx:GridViewDataTextColumn FieldName="Approvers" Caption="Access Approvers" 
					VisibleIndex="10" meta:resourcekey="GridViewDataTextColumnApprovers">
					<DataItemTemplate>
						<div style="height: 100px; overflow-y: scroll">
                            <%# Container.Text %>
                        </div>
					</DataItemTemplate>
				</dx:GridViewDataTextColumn>
			</Columns>

              <SettingsBehavior AllowFocusedRow="True"></SettingsBehavior>
            <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>" />
		</dx:ASPxGridView>		
	</div>
	
	<div id="NoteContentHeader" class='Header'>
		<asp:Literal ID="Justification" runat="server" meta:ResourceKey="Justification"></asp:Literal>
	</div>
	<div id="NoteContent" class='Content'>
        <div style="margin: 10px">
			<asp:Table runat="server" ID="Table2" SkinID="FormTable">
				<asp:TableRow ID="TableRow7" runat="server">
					<asp:TableCell ID="TableCell4" runat="server" SkinID="FormTableLabel">
                        <asp:Literal ID="Justification1" runat="server" meta:resourcekey="Justification1"></asp:Literal>
                        &nbsp;
                    </asp:TableCell>
					<asp:TableCell ID="TableCell99" runat="server" SkinID="FormTableItemWide">
						<uc9:AccessRequestJustificationSelector ID="AccessRequestJustificationSelector" runat="server" />					
					</asp:TableCell>
					<asp:TableCell ID="TableCell14" SkinID="FormTableHelp" runat="server">
						        <div class="HelpPopUp">
						        </div>
					</asp:TableCell>
				</asp:TableRow>
			</asp:Table>
		</div>
		<asp:Table runat="server" ID="TableAddNotes">
			<asp:TableRow ID="TableRow3" runat="server">
				<asp:TableCell ID="TableCell8" runat="server">
					<div id="MemoNote">
						<dx:ASPxMemo ID="ASPxMemoNote" ClientInstanceName="memoNote" runat="server" Height="150px"
							Width="650px">
						</dx:ASPxMemo>
					

					</div>
				</asp:TableCell>
				<asp:TableCell ID="TableCell9" runat="server" VerticalAlign="Top">                
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>
	</div>
	<br />
	<div style="clear: both;">
	</div>
	<div id="Buttons">
		<div style="float: left; margin: 5px" id="divCancel">
			<dx:ASPxButton ID="ASPxButtonCancelConfirm" runat="server"
				ClientInstanceName="cancelButton" ForeColor="#0098DB" AutoPostBack="False" 
				meta:resourcekey="ASPxButtonCancelConfirm">
				<ClientSideEvents Click="function(s,e){ ConfirmCancel(); }" />
			</dx:ASPxButton>
		</div>		
		<div style="float: left; margin: 5px" id="divSubmit">
			<dx:ASPxButton ID="ASPxButtonSubmit" runat="server"
				AutoPostBack="False" meta:resourcekey="ASPxButtonSubmit">
				<ClientSideEvents Click="function(s,e){ submitRequest(); }" />
			</dx:ASPxButton>
		</div>		
		<div style="clear: both;">
		</div>
	</div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<asp:ObjectDataSource ID="ObjectDataSourceRequestPersons" runat="server" TypeName="spaarks.DB.CSBC.DBAccess.DBAccessController.Director"
		SelectMethod="GetAccessRequestPersons">
		<SelectParameters>
            <asp:Parameter DefaultValue="0" Name="requestMasterId" Type="Int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="ObjectDataSourceAccessAreas" runat="server" TypeName="spaarks.DB.CSBC.DBAccess.DBAccessController.Director"
		SelectMethod="GetMasterAreasForRequest">
		<SelectParameters>
            <asp:Parameter DefaultValue="0" Name="requestMasterId" Type="Int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldPersonDetail" ClientInstanceName="hfPersonDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldRequestDetail" ClientInstanceName="hfRequestDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldLocationDetail" ClientInstanceName="hfLocationDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessDetail" ClientInstanceName="hfAccessDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxPopupControl ID="ASPxPopupControlSubmitted" ClientInstanceName="popupSubmitted"
		runat="server" SkinID="PopupNoHeader" Width="400px" Height="250px" 
		CloseAction="None" meta:resourcekey="ASPxPopupControlSubmittedResource1">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
					<asp:Literal ID="AccessRequestRequestSubmitted" meta:resourcekey="AccessRequestRequestSubmitted" runat="server" ClientIDMode="Static"></asp:Literal></h2>
				<p>
					<asp:Literal ID="AccessRequestSubmitted" runat="server" meta:resourceKey="AccessRequestSubmitted"></asp:Literal>
					</p>
				<p>
					<asp:Literal ID="Notified" runat="server" meta:resourcekey="Notified"></asp:Literal>					
				</p>
                <div id="divPersonsNoBadge" class="highlightedPanel" style="background-color: #CFDAEB; padding: 10px; margin: 10px; border: 1px solid #0018A8;">
					<p>
                        <asp:Literal ID="AccessRequestnoDBPass" meta:resourcekey="AccessRequestnoDBPass" runat="server" ClientIDMode="Static"></asp:Literal>
                    </p>
					<p>
                        <asp:Literal ID="AccessRequestNewPass" meta:resourcekey="AccessRequestNewPass" runat="server" ClientIDMode="Static"></asp:Literal>
                    </p>
					<dx:ASPxButton ID="ASPxButtonNewPass" runat="server" AutoPostBack="false" 
						Width="240" OnClick="ASPxButtonNewPass_click" Style="margin: 0 auto;" 
						meta:resourcekey="ASPxButtonNewPass">
					</dx:ASPxButton>
				</div>
				<p>
					<asp:Literal ID="Continue" runat="server" meta:resourceKey="Continue"></asp:Literal>
				</p>
				<br />
				<div>
					<dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" 
						Width="240" Style="margin: 0 auto;" meta:resourcekey="AccessRequestAnotherAccess">
						<ClientSideEvents Click="function(s,e){ window.location=location.pathname; }" />
<ClientSideEvents Click="function(s,e){ window.location=location.pathname; }"></ClientSideEvents>
					</dx:ASPxButton>
					<dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="false" ForeColor="#0098db" Width="240" Style="margin: 0 auto;" 
						meta:resourcekey="AccessRequestToHome">
						<ClientSideEvents Click="function(s,e){ RedirectToHomePage(); }" />
<ClientSideEvents Click="function(s,e){ RedirectToHomePage(); }"></ClientSideEvents>
					</dx:ASPxButton>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="ConfirmCancel" ClientInstanceName="popupConfirmCancel" runat="server"
		SkinID="PopupBasic"  
		meta:resourcekey="ConfirmCanAccessRequest">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup('UploadGroup'); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(&#39;UploadGroup&#39;); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
                    <asp:Literal ID="AccessRequestCancel" Text="<%$ Resources:CommonResource, Cancel%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal></h2>
				<p>
                    <asp:Literal ID="AccessRequestCancelConfirm" Text="<%$ Resources:CommonResource, CancelConfirm%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
                </p>
				<br />
				<div style="width: 220px; margin-left: auto; margin-right: auto; clear: both">
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Yes%>"
                            Width="100px" OnClick="ASPxButtonCancel_click">
							<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="ASPxButton5" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, No%>"
							Width="100px">
							<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="PopupExcelUpload" ClientInstanceName="popupExcelUpload"
		runat="server" SkinID="PopupBasic" meta:resourcekey="AccessRequestPopupExcelUpload">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup('UploadGroup'); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(&#39;UploadGroup&#39;); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" 
				SupportsDisabledAttribute="True">
				<div style="width: 300px; margin-left: auto; margin-right: auto; clear: both">
					<div style="float: left; padding: 5px">
						<div>
                            <asp:Literal ID="ExcelFileText" runat="server" meta:resourcekey="ExcelFileText"></asp:Literal>
                        </div>
						<div style="margin-left: 40px">
                            -
                            <asp:Literal ID="SavedExcel" runat="server" meta:resourcekey="SavedExcel"></asp:Literal>
                        </div>
						<div style="margin-left: 40px">
                            -
                            <asp:Literal ID="NoHeadings" runat="server" meta:resourcekey="NoHeadings"></asp:Literal>
                        </div>
						<div style="margin-left: 40px">
                            -
                            <asp:Literal ID="OnlyEmployIDS" runat="server" meta:resourcekey="OnlyEmployIDS"></asp:Literal>
                        </div>
						<div style="margin-left: 80px">
                            -
                            <asp:Literal ID="ExternalIDS" runat="server" meta:resourcekey="ExternalIDS"></asp:Literal>
                        </div>
						<br />
                        <Upload:InputFile ID="inputFileId" ClientIDMode="Static" runat="server" 
							meta:resourcekey="inputFileId" />
						<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateUploadFile"
							ErrorMessage="Please select an Excel file to upload." ValidationGroup="UploadGroup"></asp:CustomValidator>
						<Upload:ProgressBar ID="progressBarId" runat="server" Inline="false" />

                        <div style="position: relative;">
                            <span style="margin: 2px;">
							    <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="false" Text=""
								    OnClick="btnSubmitExcelSheet_Click" ValidationGroup="UploadGroup" 
							    meta:resourcekey="ASPxButton2">
							    </dx:ASPxButton>
						    </span>
                            <span style="margin: 2px; position: absolute; top: -2px; left: 90px;">
							    <dx:ASPxButton ID="ASPxButton6" runat="server" AutoPostBack="false" Text=""
								    ForeColor="#0098db" ValidationGroup="UploadGroup" meta:resourcekey="ASPxButton6">
								    <ClientSideEvents Click="function(s,e){ popupExcelUpload.Hide(); }" />
                        	    </dx:ASPxButton>
						    </span>
                        </div>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
		<dx:ASPxPopupControl ID="PopupPostSubmit" 
		ClientInstanceName="popupPostSubmit" runat="server"
			SkinID="PopupCustomSize" HeaderText="" Height="180px" Width="350px" 
		ShowHeader="False" meta:resourcekey="PopupPostSubmitResource1">
			<ContentCollection>
				<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" 
					SupportsDisabledAttribute="True">
					<div style="text-align: center">
						<div style="padding-top: 20px">
							<h5>
							<asp:Literal ID="RequestSubmittedMessage1" runat="server" meta:resourceKey="RequestSubmittedMessage1"></asp:Literal>
								</h5>
						</div>
						<br />
                    <img src="../App_Themes/DBIntranet2010/images/indicator.gif" alt="In progress" />
						<br />
						<p style="color: Red; padding-top: 20px">
							<asp:Literal ID="RequestSubmittedMessage2" runat="server" meta:resourceKey="RequestSubmittedMessage2"></asp:Literal>
						</p>
					</div>
				</dx:PopupControlContentControl>
			</ContentCollection>
		</dx:ASPxPopupControl>
	<asp:HiddenField ID="hdnRedirect" runat="server" />
	<dx:ASPxHiddenField ID="ASPxHiddenFieldShowAccessRequestJustification" ClientInstanceName="hfshowAccessJustification"
		runat="server" />
	<ppc:Popups ID="PopupTemplates" runat="server" />

    <!--literals-->
    <asp:Literal ID="AccessRequestAddPerson" meta:resourcekey="AccessRequestAddPerson" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestAlreadyAddedError" meta:resourcekey="AccessRequestAlreadyAddedError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestAddAccess" meta:resourcekey="AccessRequestAddAccess" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     
    <asp:Literal ID="AccessRequestAccessNotAdded" meta:resourcekey="AccessRequestAccessNotAdded" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestAccessNotSubmitted" meta:resourcekey="AccessRequestAccessNotSubmitted" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    
    <asp:Literal ID="AccessRequestNotFindApprovers" meta:resourcekey="AccessRequestNotFindApprovers" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestApprover" meta:resourcekey="AccessRequestApprover" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestPersonNotDeleted" meta:resourcekey="AccessRequestPersonNotDeleted" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="DeletePersonLiteral" Text="<%$ Resources:CommonResource, DeletePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestSelected" meta:resourcekey="AccessRequestSelected" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestPersonInvalidEmail" meta:resourcekey="AccessRequestPersonInvalidEmail" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

 
    <asp:Literal ID="AddPersonLiteral" Text="<%$ Resources:CommonResource, AddPerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="SelectValidPerson" Text="<%$ Resources:CommonResource, SelectValidPerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestOnAddPerson" meta:resourcekey="AccessRequestOnAddPerson" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="AccessRequestApproversNotFound" meta:resourcekey="AccessRequestApproversNotFound" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="CannotAddInactivePerson" Text="<%$ Resources:CommonResource, CannotAddInactivePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestOptionJustification" meta:resourcekey="AccessRequestOptionJustification" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="AccessRequestEndDateError" meta:resourcekey="AccessRequestEndDateError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestNeedAddPers" meta:resourcekey="AccessRequestNeedAddPers" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestSelectArea" meta:resourcekey="AccessRequestSelectArea" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>


    <asp:Literal ID="AccessRequestNeedServiceArea" meta:resourcekey="AccessRequestNeedServiceArea" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestNotFindAccessApprover" meta:resourcekey="AccessRequestNotFindAccessApprover" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestUsersNotAdded" meta:resourcekey="AccessRequestUsersNotAdded" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

                     
    <asp:Literal ID="AccessRequestExcelUpload" meta:resourcekey="AccessRequestExcelUpload" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestSupportText" meta:resourcekey="AccessRequestSupportText" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestNotFindRequestApprovers" meta:resourcekey="AccessRequestNotFindRequestApprovers" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     
    <asp:Literal ID="AccessRequestNotFindAccessApprovers" meta:resourcekey="AccessRequestNotFindAccessApprovers" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestAddArea" meta:resourcekey="AccessRequestAddArea" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestEnterComment" meta:resourcekey="AccessRequestEnterComment" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="AccessRequestAddPersonError" meta:resourcekey="AccessRequestAddPersonError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestRequestTypeError" meta:resourcekey="AccessRequestRequestTypeError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AccessRequestAccessAreaError" meta:resourcekey="AccessRequestAccessAreaError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
</asp:Content>
