﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="true" CodeBehind="NewPassRequest.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.NewPassRequest"
	EnableViewState="false"uiculture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="../Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector"
	TagPrefix="uc1" %>
<%@ Register Src="../Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector"
	TagPrefix="uc2" %>
<%@ Register Src="../Controls/Selectors/BadgeJustificationSelector.ascx" TagName="BadgeJustificationSelector"
	TagPrefix="uc3" %>

<%@ Register Assembly="Brettle.Web.NeatUpload" Namespace="Brettle.Web.NeatUpload"
	TagPrefix="Upload" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">
	    CardType =
            {
                STANDARD_CARD: 0,
                SMART_CARD: 1
            };

	    PassCollection =
            {
                PASS_OFFICE: 0,
                PASS_OFFICE_CONTACT: 1
            };

	    revertToStandardCard_decesion = true;

	    function UpdatePositionOfButtons() {
	        //Get the position of the badge grid

	        var grid = $('#ctl00_DBMainContent_ASPxCallbackPanelBadgeRequests_ASPxGridViewBadgeRequests');
	        if (grid == null)
	            return;

	        var buttons = $('#Buttons');

	        var pos = grid.position();
	        var widthOfGrid = grid.width();

	        var adjustment = 204;
	        if (!$('#divSubmit').is(':visible'))
	            adjustment = 74;

	        var newPosOfButtons = (pos.left + widthOfGrid) - adjustment;

	        buttons.css({ position: "absolute", left: newPosOfButtons });
	    }


	    var FirstPerson = true;

	    function ValidateFileType(data) {
	        data = data.replace(/^\s|\s$/g, ""); //trims string

	        if (data.match(/([^\/\\]+)\.(xlsx)$/i)) { //eg: xlsx|html|htm|shtml|php
	            return true;
	        }
	        else {
	            return false;
	        }
	    }


	    function ValidateUploadFile(Source, args) {
	        var chkFile = document.getElementById('<%= inputFileId.ClientID %>');

	        if (chkFile.value == "") {
	            args.IsValid = false;
	            popupExcelUpload.Show();
	        }
	        else {
	            if (ValidateFileType(chkFile.value)) {
	                args.IsValid = true;
	                popupExcelUpload.Hide();
	            }
	            else {
	                args.IsValid = false;
	                popupExcelUpload.Show();
	            }
	        }
	    }

	    function AddPerson(s, e) {
	        //first validate the selected person
	        var idstring = $get("PersonSelectorNameTextBox").value;

	        var personId = 0;

	        if (idstring.length > 0) {
	            personId = GetPersonIdFromIdentifier(idstring);
	        }

	        if (personId == 1) {
	            popupAlertTemplateContent.SetText("<%=newPassRequestEmailValidator.Text%>");
	            popupAlertTemplate.SetHeaderText("<%=AddPersonLiteral.Text%>");
	            popupAlertTemplate.Show();
	            return;
	        }

	        if (personId == 0) {
	            popupAlertTemplate.SetHeaderText("<%=AddPersonLiteral.Text%>");
	            popupAlertTemplateContent.SetText("<%=SelectValidPerson.Text%>");
	            popupAlertTemplate.Show();
	            return;
	        }

	        //save details temporarily before proceeding
	        hfPersonDetail.Set("PersonID", personId);
	        hfPersonDetail.Set("PersonString", idstring);

	        //Clear the text box
	        $get("PersonSelectorNameTextBox").value = '';

	        //now check if they have badges already
	        var NumBadges = GetPersonBadgeCount(personId);

	        //if they have multiple badges, show popup screen
	        if (NumBadges > 1) {
	            lbBadges.PerformCallback(personId);
	            popupBadges.Show();
	        } else {
	            //if not, add to request
	            var requestId = hfRequestDetail.Get("RequestMasterID");
	            PageMethods.AddPersonToRequest(personId, requestId, (NumBadges > 0), OnAddPersonToRequest);
	        }
	    }

	    function OnAddPersonToRequest(result) { 
	        if (result == 0) {
	            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "<%=RequestCouldNotBeCreated.Text%>";
	            popupNewPassRequestError.Show();
	        }
	        else if (result == -1) {
	            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "<%=NewRequestExistingPersonRequestError.Text%>";
	            popupNewPassRequestError.Show();
	        }
	        else if (result == -2) {
	            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "<%=NewRequestRepPassError.Text%>";
	            popupNewPassRequestError.Show();
	        }
	        else if (result == -3) {
	            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "<%=CannotAddInactivePerson.Text%>";
	            popupNewPassRequestError.Show();
	        }
	        else {
	            hfRequestDetail.Set("RequestMasterID", result);
	            //gvBadgeRequests.PerformCallback(result);
	            cbBadgeRequests.PerformCallback();

	            toggleGrid();
               
	            //setTimeout("UpdatePositionOfButtons()", 600);
	        }
	    }

	    function replaceBadges() {

	        var selectedbadges = lbBadges.GetSelectedValues();
	        if (selectedbadges.length == 0) {
	            popupAlertTemplate.SetHeaderText("<%=NewRequestRepBadges.Text%>");
	            popupAlertTemplateContent.SetText("<%=NewRequestNoExistingBadgeError.Text%>");
	            popupAlertTemplate.Show();
	        }
	        else {
	            //badges to replace were selected - add them to the request
	            var personId = hfPersonDetail.Get("PersonID");
	            var requestId = hfRequestDetail.Get("RequestMasterID");
	            PageMethods.AddPersonWithBadgesToRequest(personId, requestId, selectedbadges, OnAddPersonToRequest);
	        }

	        popupBadges.Hide();
	    }

	    function toggleGrid() {
	        var requestId = hfRequestDetail.Get("RequestMasterID");
	        if (requestId > 0)
	            $("#divBadgeRequests").show();
	        else
	            $("#divBadgeRequests").hide();
	    }

	    function toggleCollectionDetails(decision) {

	        switch (decision) {
	            case 0:
	                $("#divPassOfficeSelector").show();
	                $("#divContactDetails").hide();
	                setSubmitBtnVisibility();
	                $("#pCollect").show();
	                $("#pContact").hide();
	                break;
	            case 1:
	                $("#divContactDetails").show();
	                //$("#divPassOfficeSelector").hide();
	                setSubmitBtnVisibility();
	                $("#pCollect").hide();
	                $("#pContact").show();
	                break;
	            default:
	                $("#divContactDetails").hide();
	                //$("#divPassOfficeSelector").hide();
	                $("#divSubmit").hide();	                
	                $("#pCollect").hide();
	                $("#pContact").hide();
	                $("#divCardTypeJustification").hide();
	                break;
	        }


	        //UpdatePositionOfButtons();
	    }

	    function toggleCardType(cardType)
	    {
	        setSubmitBtnVisibility();
	        switch (cardType)
	        {
	            case CardType.STANDARD_CARD:
	                $(JustificationReasonTextAreaJQID).val("");
	                $("#divCardTypeJustification").hide();
	                break;
	            case CardType.SMART_CARD:
	                SmartPassConfirmPopup.Show();
	                $("#divCardTypeJustification").show();
	                break;
	        }
	    }

	    function setSubmitBtnVisibility()
	    {
	        var reason = $(JustificationReasonTextAreaJQID).val();
	        var selectedCardType = RadioButtonListSelectCardType.GetSelectedIndex();
	        var collectionMethod = rblSelectCollectionMethod.GetSelectedIndex();

	        if (collectionMethod == PassCollection.PASS_OFFICE || collectionMethod == PassCollection.PASS_OFFICE_CONTACT) {
	            if (selectedCardType == CardType.STANDARD_CARD) {
	                $("#divSubmit").show();
	            }
	            else if (selectedCardType == CardType.SMART_CARD) {
	                if ($(JustificationReasonTextAreaJQID).val().length != 0) {
	                    $("#divSubmit").show();
	                }
	                else {
	                    $("#divSubmit").hide();	                                            	                    
	                }
	            }
	            else {
	                alert("Unknown Error! The selected card type cannot be detrmined");
	            }
	        }
	        else
	        {
	            $("#divSubmit").hide();                
	        }
	    }    

	    $(function () {
            
	        JustificationReasonTextAreaJQID = "#" + "<%=JustificationReason.ClientID%>";

	        $(JustificationReasonTextAreaJQID).keyup(function () {
	            setSubmitBtnVisibility();
	        });
	    });

	    function showJustificationAlert(selectJustificationAlert, justficationReasonAlert)
	    {
	        popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");

	        if (selectJustificationAlert && justficationReasonAlert)
	            popupAlertTemplateContent.SetText("<%=NewPassRequestBRJustification.Text%>" + "<br>" + "<%=SmartPassJustificationBody.Text%>");

	        else if (selectJustificationAlert)
	            popupAlertTemplateContent.SetText("<%=NewPassRequestBRJustification.Text%>");

	        else if (justficationReasonAlert)
	            popupAlertTemplateContent.SetText("<%=SmartPassJustificationBody.Text%>");

	        popupAlertTemplate.Show();
	        return;            
	    }

	    function revertToStandardCard()
	    {        
	        if (revertToStandardCard_decesion)
	        {
                RadioButtonListSelectCardType.SetSelectedIndex(CardType.STANDARD_CARD);
                toggleCardType(CardType.STANDARD_CARD);                
	        }
	        revertToStandardCard_decesion = true;
	    }

	    function submitRequest()
	    {
	        var showJustification = hfShowJustification.Get("ShowJustification");
	        var justificationID = -1;
	        if (showJustification)
	            justificationID = $("#BadgeJustificationSelectorDropDownList").attr("selectedIndex");

	        if ((RadioButtonListSelectCardType.GetSelectedIndex() == CardType.SMART_CARD) && ($(JustificationReasonTextAreaJQID).val().length == 0) || (showJustification && justificationID == 0))
	        {
	            var selectJustificationAlert = false;
	            var justficationReasonAlert = false;

	            if (showJustification && justificationID == 0)
	                selectJustificationAlert = true;

	            if ((RadioButtonListSelectCardType.GetSelectedIndex() == CardType.SMART_CARD) && $(JustificationReasonTextAreaJQID).val().length == 0)
	                justficationReasonAlert = true;

	            showJustificationAlert(selectJustificationAlert, justficationReasonAlert);
	        }
            else
	        {	            
                var requestId = hfRequestDetail.Get("RequestMasterID");

	            //check if there are replacements, and show the warning

	            var personCount = GetRequestPersonCount(requestId);

	            if (personCount == 0) {
	                popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");
	                popupAlertTemplateContent.SetText("<%=NewPassRequestPAP.Text%>");
	                popupAlertTemplate.Show();
	                return;
	            }	            

	            var NumReplacementBadges = GetReplacementBadgeCount(requestId);

	            if (NumReplacementBadges == 0) {
	                DoSubmitRequest();
	                return;
	            }
	            else (NumReplacementBadges >= 1)
	            {
	                var sHeader = "";
	                var sContentBody = "";

	                if (NumReplacementBadges == 1) {
	                    sHeader = "<%=ReplacementPass.Text%>";
	                    sContentBody = "<%=NewPassRequestDuplicateRequest.Text%>";
	                }
	                else if (NumReplacementBadges > 1) {
	                    sHeader = "<%=NewPassRequestPasses.Text%>";
	                    sContentBody = "<%=NewPassRequestDuplicatePasses.Text%>";
	                }
	                popupConfirmTemplate.SetHeaderText(sHeader);
	                popupConfirmTemplateContentHead.SetText(sHeader);
	                popupConfirmTemplateContentBody.SetText(sContentBody);
	                popupConfirmTemplateHiddenField.Set("function_Yes", "DoSubmitRequest();");
	                popupConfirmTemplate.Show();
	                return;
	            }
	        }
	    }

	    function DoSubmitRequest() {
	        popupLoading.Show();

	        // need to pick up these values again (as in submitRequest)

	        var requestId = hfRequestDetail.Get("RequestMasterID");

	        //check if there are replacements, and show the warning

	        var personCount = GetRequestPersonCount(requestId);

	        // move on to the rest of the functionality

	        var decision = 0;
	        var additionalInfo = '';
	        var commentNote = '';
	        var isSmartCard = false;
	        var smartCardJustificationReason = "";

	        //Check which collection method was specified and if all info was supplied
	        decision = rblSelectCollectionMethod.GetSelectedIndex();

	        switch (decision) {
	            case 0:
	                var passofficesddl = $get("passofficesDropDownList");
	                var passOfficeselectedIndex = passofficesddl.selectedIndex;

	                if (passOfficeselectedIndex < 1) {
	                    popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");
	                    popupAlertTemplateContent.SetText("<%=NewPassRequestSelectPassOff.Text%>");
	                    popupAlertTemplate.Show();
	                    return;
	                }
	                additionalInfo = passofficesddl.options[passOfficeselectedIndex].value;
	                break;
	            case 1:
	                var tbemail = $get("ctl00_DBMainContent_TextBoxEmail_I");
	                var tbtel = $get("ctl00_DBMainContent_TextBoxTelephone_I");
	                //debugger;

	                if (tbtel.value == '') {
	                    popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");
	                    popupAlertTemplateContent.SetText('<%=NewPassRequestPhone.Text%>');
	                    popupAlertTemplate.Show();
	                    return;
	                }
	                additionalInfo = tbemail.value + ',' + tbtel.value;
	                break;
	            default:
	                popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");
	                popupAlertTemplateContent.SetText("<%=NewPassRequestCollectContact.Text%>");
	                popupAlertTemplate.Show();
	                return;
	        }

	        var showJustification = hfShowJustification.Get("ShowJustification");
	        var justificationID = null;

	        if (showJustification) {
	            justificationID = $("#BadgeJustificationSelectorDropDownList").attr("selectedIndex");
	        }

	        //get information realted to pass type
	        
	        if (RadioButtonListSelectCardType.GetSelectedIndex() == CardType.SMART_CARD)
	        {
	            isSmartCard = true;
	            smartCardJustificationReason = $(JustificationReasonTextAreaJQID).val();
	        }            

	        PageMethods.SubmitRequest(requestId, decision, additionalInfo, commentNote, isSmartCard, smartCardJustificationReason, justificationID, OnSubmitRequest);
	    }

	    function OnSubmitRequest(result) {
	        //check if any persons have no badge
	        var requestId = hfRequestDetail.Get("RequestMasterID");
	        var personCount = GetAccessRequestPersonsWithoutBadgeCount(requestId);

	        if (personCount > 0)
	            $("#divPersonsNoBadge").show();
	        else
	            $("#divPersonsNoBadge").hide();

	        popupLoading.Hide();

	        if (result) {
	            popupSubmitted.Show();
	        }
	        else {
	            popupAlertTemplate.SetHeaderText("<%=NewPassRequestSubmit.Text%>");
	            popupAlertTemplateContent.SetText("<%=NewPassRequestNotSubmitted.Text %>");
	            popupAlertTemplate.Show();
	        }
	    }

	    function ConfirmCancel() {
	        popupConfirmCancel.Show();
	    }

	    function ShowExcelUpload() {
	        popupExcelUpload.Show();
	    }

	    function DeletePerson(personId) {
	        PageMethods.DeletePerson(personId, OnDeletePerson);
	    }
	    function OnDeletePerson(result) {
	        if (!result) {
	            popupAlertTemplate.SetHeaderText("<%=DeletePersonLiteral.Text%>");
	            popupAlertTemplateContent.SetText("<%=NewPassRequestPersNotDeleted.Text%>");
	            popupAlertTemplate.Show();
	        }
	        else {
	            //gvBadgeRequests.PerformCallback(hfRequestDetail.Get("RequestMasterID"));
	            cbBadgeRequests.PerformCallback();
	        }
	    }

	    function GetJustificationReason() {
	        var justificationReason = $("#BadgeJustificationSelectorDropDownList").attr("selectedIndex");
	        return justificationReason;
	    }

	    function cbBadgeRequestsEndCallback(s, e) {
	        toggleJustification();
	        SetDefaultPassOffice();
	        revertToStandardCard();
	    }

	    function toggleJustification() {
	        var showJustification = hfShowJustification.Get("ShowJustification");

	        if (showJustification)
	            $('#DivJustification').css('display', '');
	        else
	            $('#DivJustification').css('display', 'none');
	    }

	    function SetDefaultPassOffice() {
	        var passofficesddl = $get("passofficesDropDownList");
	        var defaultPassOffice = hfDefaultPassOffice.Get("DefaultPassOffice");

	        var exists = (0 != $('#passofficesDropDownList option[value=' + defaultPassOffice + ']').length); //Check whether default pass office is exist in the list
	        if (exists) {	            
	            passofficesddl.value = defaultPassOffice;
	        }
	        else {
	            passofficesddl.value = 0;
	        }
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<h1><asp:Literal ID="NewPassRequest1" runat="server"	meta:resourcekey="NewPassRequest"></asp:Literal></h1>
	<asp:Panel runat="server" ID="personTitlePanel" SkinID="CollapsiblePanelTitle">
	</asp:Panel>
	<div style="margin: 10px">
		<asp:Table runat="server" ID="LocationTable" SkinID="FormTable">
			<asp:TableHeaderRow Style="border-bottom-style: none;">
				<asp:TableCell ID="TableCellLocationTableColumn1" runat="server" SkinID="FormTableLabel"
					Style="border-bottom-style: none;" >
				</asp:TableCell>
				<asp:TableCell ID="TableCellLocationTableColumn2" runat="server" SkinID="FormTableItemWide"
					Style="border-bottom-style: none;">
				</asp:TableCell>
				<asp:TableCell ID="TableCellLocationTableColumn3" runat="server" Width="30px" SkinID="FormTableLabel"
					Style="border-bottom-style: none;"></asp:TableCell>
				<asp:TableCell ID="TableCellLocationTableColumn4" SkinID="FormTableHelp" runat="server"
					Style="border-bottom-style: none;"></asp:TableCell>
			</asp:TableHeaderRow>
			<asp:TableRow ID="TableRow4" runat="server" Height="30px">
				<asp:TableCell ColumnSpan="2" ID="TableCell15" runat="server" 
					class="form_label">
                <asp:Literal ID="BulkRequestStartText" runat="server"	meta:resourcekey="BulkRequestStartText"></asp:Literal><a onclick="ShowExcelUpload()" href="javascript:void(0);"><asp:Literal ID="BulkRequestHereText" runat="server" meta:resourcekey="BulkRequestHereText"></asp:Literal></a><asp:Literal ID="BulkRequestEndText" runat="server" meta:resourcekey="BulkRequestEndText"> </asp:Literal>
				</asp:TableCell>
				<asp:TableCell ID="TableCell17" runat="server" Width="30px" 
					SkinID="FormTableLabel"></asp:TableCell>
				<asp:TableCell ID="TableCell20" SkinID="FormTableHelp" runat="server">
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow ID="TableRow0" runat="server">
				<asp:TableCell ID="TableCell0" runat="server" SkinID="FormTableLabel" 
					Style="vertical-align: middle;">
					<asp:Literal ID="Persons" runat="server" meta:resourcekey="Persons"></asp:Literal>&nbsp;</asp:TableCell>
				<asp:TableCell ID="TableCell1" runat="server" SkinID="FormTableItemWide">
					<uc1:PersonSelector ID="PersonSelector1" ServiceMethod="GetPersonCompletionList"
						runat="server" />
				</asp:TableCell>
				<asp:TableCell ID="TableCell2" runat="server" Width="30px" 
					SkinID="FormTableLabel">
					<dx:ASPxButton ID="ASPxButtonAddPerson" AutoPostBack="false" runat="server" Text=""
						Style="margin-top: 3px;" meta:resourcekey="ASPxButtonAddPerson">
							<ClientSideEvents Click="AddPerson" />	
					</dx:ASPxButton>
				</asp:TableCell>
				<asp:TableCell ID="TableCell13" SkinID="FormTableHelp" runat="server">
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>
	</div>
    
	<div style="margin: 10px" id="divBadgeRequests">
		<div style="margin-bottom: 6px; padding: 5px" id="divBadgeRequestsKey" class="GreyBorder">
			<b><asp:Literal ID="Key" runat="server" meta:resourceKey="Key"></asp:Literal></b>&nbsp;&nbsp;
			<asp:Image runat="server" SkinID="New" ID="newKeyImage" ImageAlign="Middle"  
			  ToolTip="<%$ Resources:CommonResource, newKeyImageResource1AlternateText%>"   AlternateText="<%$ Resources:CommonResource, newKeyImageResource1AlternateText%>" />
			<asp:Literal ID="NewIssue" runat="server" meta:resourceKey="NewIssue"></asp:Literal>&nbsp;&nbsp;
			<asp:Image runat="server" SkinID="Renew" ID="renewKeyImage" ImageAlign="Middle" 
			   ToolTip="<%$ Resources:CommonResource, renewKeyImageResource1AlternateText%>"  AlternateText="<%$ Resources:CommonResource, renewKeyImageResource1AlternateText%>"  />
			<asp:Literal ID="ReplacementPass" runat="server" meta:resourceKey="ReplacementPass"></asp:Literal>&nbsp;&nbsp;
		</div>
		<dx:ASPxCallbackPanel ID="ASPxCallbackPanelBadgeRequests" ClientInstanceName="cbBadgeRequests"
			runat="server" OnCallback="ASPxCallbackPanelBadgeRequests_Callback">
			<Clientsideevents EndCallback="function(s, e) {cbBadgeRequestsEndCallback (s,e);}" />
			<PanelCollection>
				<dx:PanelContent>
					<dx:ASPxGridView ID="ASPxGridViewBadgeRequests" ClientInstanceName="gvBadgeRequests"
						runat="server" AutoGenerateColumns="False" KeyFieldName="RequestID" OnHtmlRowCreated="ASPxGridViewBadgeRequests_OnHtmlRowCreated"
						ClientIDMode="AutoID">
						<Columns>
							<dx:GridViewDataImageColumn VisibleIndex="0">
								<DataItemTemplate>
									<img src="../App_Themes/DBIntranet2010/images/trash.gif" alt="<%$ Resources:CommonResource, Delete%>"  onclick='<%# "DeletePerson("+Container.KeyValue+")" %>'  title="<%$ Resources:CommonResource, Delete%>" runat="server"/>
								</DataItemTemplate>
							</dx:GridViewDataImageColumn>
							<dx:GridViewDataColumn VisibleIndex="1" Caption=" " FieldName="Issue">
								<DataItemTemplate>
									<asp:Image ID="ImageIssueType" runat="server"/>
								</DataItemTemplate>
							</dx:GridViewDataColumn>
							<dx:GridViewDataTextColumn FieldName="Forename" Caption="" ShowInCustomizationForm="True"
								VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnFirstName">
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn FieldName="Surname" Caption="" ShowInCustomizationForm="True"
								VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnSurname">
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn Caption="" FieldName="CountryName" ShowInCustomizationForm="True"
								VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnCountry">
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn Caption="" FieldName="UBR_Code" ShowInCustomizationForm="True"
								VisibleIndex="9" meta:resourcekey="GridViewDataTextColumnUBRCode">
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn Caption="" FieldName="UBR_Name" ShowInCustomizationForm="True"
								VisibleIndex="10" meta:resourcekey="GridViewDataTextColumnUBRDescription">
							</dx:GridViewDataTextColumn>
							
							<dx:GridViewDataTextColumn FieldName="EmailAddress" VisibleIndex="13" 
								meta:resourcekey="GridViewDataTextColumnEmailAddress">
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn FieldName="BadgeTag" Caption="" ShowInCustomizationForm="True"
								VisibleIndex="13" meta:resourcekey="GridViewDataTextColumnBadgeToReplace">
							</dx:GridViewDataTextColumn>
						</Columns>

                         <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>"/>
					</dx:ASPxGridView>
					<dx:ASPxHiddenField ID="ASPxHiddenFieldShowJustification" ClientInstanceName="hfShowJustification"
						runat="server">
					</dx:ASPxHiddenField>
                   <dx:ASPxHiddenField ID="ASPxHiddenFieldDefaultPassOffice" ClientInstanceName="hfDefaultPassOffice"
						runat="server">
					</dx:ASPxHiddenField>
				</dx:PanelContent>
			</PanelCollection>
		</dx:ASPxCallbackPanel>
		<!-- /Mahmoud.Ali, Changes to Employees' Information Display -->
		<br />
		<br />

        <asp:Panel runat="server" ID="PanelPassType" 
			SkinID="CollapsiblePanelTitle">
			<asp:Literal ID="PassType" runat="server"  meta:resourceKey="CardType"></asp:Literal>
		</asp:Panel>
		<asp:Table runat="server" ID="PassTypeTable">
			<asp:TableRow ID="TableRow8" runat="server">
				<asp:TableCell ID="TableCell16" runat="server">
					<p><asp:Literal ID="CardTypeSelection" runat="server" meta:resourceKey="CardTypeSelection"></asp:Literal>&nbsp;</p>
				</asp:TableCell>
				<asp:TableCell ID="TableCell18" runat="server" VerticalAlign="Top">
                <div id="CardTupeHelpInfo" class="HelpPopUp">
                </div>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow ID="TableRow9" runat="server">
				<asp:TableCell ID="TableCell19" runat="server">
					<dx:ASPxRadioButtonList ID="RadioButtonListSelectCardType" ClientInstanceName="RadioButtonListSelectCardType"
						runat="server">		
						<Items>						
							<dx:ListEditItem Selected="True" Value="0"	Text="" meta:resourceKey="ListEditItemStandardPass" />							
							<dx:ListEditItem Selected="false" Value="1" Text=""  meta:resourceKey="ListEditItemSmartPass"/>						
						</Items>						
						<ClientSideEvents ValueChanged="function(s,e){toggleCardType(s.GetSelectedIndex());}" />					
					</dx:ASPxRadioButtonList>				
				</asp:TableCell>
			</asp:TableRow>            
		</asp:Table>
        <div id="divCardTypeJustification">
            <asp:Table ID="CardTYpeJustificationTable" runat="server">
                <asp:TableRow ID="JustificationLabelRow" runat="server">
                    <asp:TableCell ID="JustificationLabel" runat="server">
                    <p><asp:Literal ID="JustificationLabelText" runat="server" meta:resourceKey="JustificationLabel"></asp:Literal>:&nbsp;</p>
                </asp:TableCell>
                </asp:TableRow>
            <asp:TableRow ID="JustificationReasonRow" runat="server">                
                <asp:TableCell ID="JustificationReasonCell" runat="server">
                    <asp:TextBox ID="JustificationReason" runat="server" TextMode="MultiLine" Rows="4" Columns="50"></asp:TextBox>                  
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        </div>
        <br />
		<br />

		<asp:Panel runat="server" ID="PanelCollectionTitle" 
			SkinID="CollapsiblePanelTitle">
			<asp:Literal ID="Collection" runat="server" meta:resourceKey="Collection"></asp:Literal>
		</asp:Panel>
		<asp:Table runat="server" ID="TablePassCollection">
			<asp:TableRow ID="TableRow5" runat="server">
				<asp:TableCell ID="TableCell8" runat="server">
					<p><asp:Literal ID="PassCollection" runat="server" meta:resourceKey="PassCollection"></asp:Literal>&nbsp;</p>
				</asp:TableCell>
				<asp:TableCell ID="TableCell9" runat="server" VerticalAlign="Top">
                <div id="CollectionHelpInfo" class="HelpPopUp">
                </div>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow ID="TableRow6" runat="server">
				<asp:TableCell ID="TableCell21" runat="server">
					<dx:ASPxRadioButtonList ID="RadioButtonListSelectCollectionMethod" ClientInstanceName="rblSelectCollectionMethod"
						runat="server">		
						<Items>						
							<dx:ListEditItem Selected="false" Value="0"	Text="" meta:resourcekey="ListEditItemPassOffice" />							
							<dx:ListEditItem Selected="false" Value="1" Text="" meta:resourcekey="ListEditItemPassOfficeContact" />						
						</Items>						
						<ClientSideEvents ValueChanged="function(s,e){ toggleCollectionDetails(s.GetSelectedIndex()) }" />					
					</dx:ASPxRadioButtonList>				
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>
		<br />
		<div style="margin: 10px" id="divPassOfficeSelector">
			<asp:Table runat="server" ID="TablePassOffice" SkinID="FormTable">
				<asp:TableRow ID="TableRow2" runat="server">
					<asp:TableCell ID="TableCell22" runat="server" SkinID="FormTableLabel">
						<asp:Literal ID="PassOffice" runat="server" meta:resourceKey="PassOffice"></asp:Literal>
						 &nbsp;</asp:TableCell>
					<asp:TableCell ID="TableCell23" runat="server" SkinID="FormTableItem">
						<uc2:PassOfficeSelector ID="PassOfficeSelector1" runat="server" />				
					</asp:TableCell>
					<asp:TableCell ID="TableCell24" SkinID="FormTableHelp" runat="server">
						<div id="PassOfficeHelpInfo" class="HelpPopUp">
						</div>
					</asp:TableCell>
				</asp:TableRow>
			</asp:Table>
		</div>
		<div style="margin: 10px" id="divContactDetails">
			<asp:Table runat="server" ID="TableContactDetails" SkinID="FormTable">
				<asp:TableRow ID="TableRow1" runat="server">
					<asp:TableCell ID="TableCell10" runat="server" SkinID="FormTableLabel"><asp:Literal ID="EmailLabel" text="<%$ Resources:CommonResource, EmailLabel%>" runat="server"></asp:Literal>: &nbsp;</asp:TableCell>
					<asp:TableCell ID="TableCell11" runat="server" SkinID="FormTableItem">
						<dx:ASPxTextBox ID="TextBoxEmail" runat="server" ClientInstanceName="TextBoxEmail"
							Width="280px">
							
<ValidationSettings CausesValidation="true" ErrorDisplayMode="ImageWithText">
								<RequiredField IsRequired="false" />
							</ValidationSettings>
						
</dx:ASPxTextBox>
					
</asp:TableCell>
					<asp:TableCell ID="TableCell12" SkinID="FormTableHelp" runat="server">
					<div id="EmailHelpInfo" class="HelpPopUp">
					</div>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow ID="TableRow3" runat="server">
					<asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableLabel"><asp:Literal ID="TelephoneLiteral" text="<%$ Resources:CommonResource, TelephoneLiteral%>" runat="server"></asp:Literal>: &nbsp;</asp:TableCell>
					<asp:TableCell ID="TableCell5" runat="server" SkinID="FormTableItem">
						<dx:ASPxTextBox ID="TextBoxTelephone" runat="server" ClientInstanceName="TextBoxTelephone"
							Width="180px">
							
<ValidationSettings CausesValidation="true" ErrorDisplayMode="ImageWithText">
								<RequiredField IsRequired="false" />
								
<RegularExpression ValidationExpression="^[0-9\-\+\(\)\ ]{1,50}$" ErrorText="<%$ Resources:CommonResource, NewPassRequestInvalidNumberText%>" />
							
</ValidationSettings>
						
</dx:ASPxTextBox>
					
</asp:TableCell>
					<asp:TableCell ID="TableCell6" SkinID="FormTableHelp" runat="server"> 
						<div id="TelephoneHelpInfo" class="HelpPopUp">
						</div>
					</asp:TableCell>
				</asp:TableRow>
			</asp:Table>
		</div>
		<div id="DivJustification">
			<asp:Panel runat="server" ID="PanelJustificationTitle" 
				SkinID="CollapsiblePanelTitle">
				<asp:Literal ID="Justification1" runat="server" meta:resourcekey="Justification"></asp:Literal>
			</asp:Panel>
			<div style="margin: 10px" id="divBadgeRequestJustificationSelector">
				<asp:Table runat="server" ID="Table1" SkinID="FormTable">
					<asp:TableRow ID="TableRow7" runat="server">
						<asp:TableCell ID="TableCell4" runat="server" SkinID="FormTableLabel">
							<asp:Literal ID="Justification2" runat="server" meta:resourcekey="Justification"></asp:Literal>: &nbsp;</asp:TableCell>
						<asp:TableCell ID="TableCell7" runat="server" SkinID="FormTableItemWide">
							<uc3:BadgeJustificationSelector ID="BadgeJustificationSelector" runat="server" />
						
</asp:TableCell>
						<asp:TableCell ID="TableCell14" SkinID="FormTableHelp" runat="server">
							<div id="Div2" class="HelpPopUp">
							</div>
						</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</div>
		</div>

		<div id= "Buttons">
			
			<div style="margin: 5px;float:left;" id="divCancel">
				<dx:ASPxButton ID="ASPxButtonCancelConfirm" ClientInstanceName="cancelButton" runat="server"
					Text="" ForeColor="#0098DB" AutoPostBack="False" 
					meta:resourcekey="ASPxButtonCancelConfirm">
					<ClientSideEvents Click="function(s,e){ ConfirmCancel(); }" />
				</dx:ASPxButton>
			</div>
			<div style="margin: 5px; float:left;" id="divSubmit">
				<dx:ASPxButton ID="ASPxButtonSubmit" ClientInstanceName="submitButton" runat="server"
					AutoPostBack="False" Text="" meta:resourcekey="ASPxButtonSubmit">
					<ClientSideEvents Click="function(s,e){ var editor = ASPxClientControl.GetControlCollection().Get(s.cpEditorClientName);
																	 if(editor.GetIsValid()) submitRequest(); }" />
				</dx:ASPxButton>
			</div>

			<div style="clear: both;"></div>
		</div>
	</div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<asp:ObjectDataSource ID="ObjectDataSourceBadges" runat="server" SelectMethod="GetPersonBadges"
		TypeName="spaarks.DB.CSBC.DBAccess.DBAccessController.Director">
		<SelectParameters>
			<asp:Parameter DefaultValue="0" Name="dbPeopleId" Type="Int32" />
			<asp:Parameter DefaultValue="true" Name="valid" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldPersonDetail" ClientInstanceName="hfPersonDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldRequestDetail" ClientInstanceName="hfRequestDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldNumberOfFailedUploads" ClientInstanceName="hfNumberOfFailedUploads"
		runat="server">
	</dx:ASPxHiddenField>

<dx:ASPxPopupControl ID="SmartPassConfirmPopup" ClientInstanceName="SmartPassConfirmPopup"
runat="server" HeaderText="<%$ Resources:SmartPassAlertHeader.Text%>" AllowResize="True" 
AllowDragging="True" Width="400px" Height="150px" Modal="True" PopupHorizontalAlign="WindowCenter"
PopupVerticalAlign="WindowCenter" >
    <ClientSideEvents CloseUp="revertToStandardCard" />
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server" 
				SupportsDisabledAttribute="True">
				<p>
                    <asp:Literal ID="JustificationRequiredMessage" meta:resourcekey="SmartPassAlertBody" runat="server"></asp:Literal>
                    <asp:HyperLink ID="hlDemo" runat="server"  NavigateUrl="https://mydb.intranet.db.com/groups/dbsmartcard" Target="_blank" style="cursor:pointer; text-decoration:underline;"><asp:Literal ID="linLiteral" runat="server" meta:resourcekey="linLiteral"></asp:Literal>                        
                    </asp:HyperLink>
				</p>
                <div style="width: 220px; margin-left: auto; margin-right: auto; clear: both">
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="OkSmartCard" runat="server" AutoPostBack="false" meta:resourcekey="OkSmartCard" Width="100">
							<ClientSideEvents Click="function(s,e){revertToStandardCard_decesion = false; SmartPassConfirmPopup.Hide();}" />
						</dx:ASPxButton>
					</div>
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="CancelSmartCard" runat="server" AutoPostBack="false" Width="100" meta:resourcekey="CancelSmartCard">
							<ClientSideEvents Click="function(s,e){ revertToStandardCard_decesion = true; SmartPassConfirmPopup.Hide();}" />
						</dx:ASPxButton>
					</div>
				</div>			
			</dx:PopupControlContentControl>
		</ContentCollection>
</dx:ASPxPopupControl>

	<dx:ASPxPopupControl ID="ASPxPopupControlBadges" ClientInstanceName="popupBadges"
		runat="server" HeaderText="<%$ Resources:ExistingBadgePopHeading%>" AllowResize="True" 
		AllowDragging="True" Width="400px" Height="150px" Modal="True" PopupHorizontalAlign="WindowCenter"
		PopupVerticalAlign="WindowCenter" 
		meta:resourcekey="ASPxPopupControlBadgesResource1">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" 
				SupportsDisabledAttribute="True">
				<p><asp:Literal ID="NewPassRequestTextLostStolen" meta:resourcekey="NewPassRequestTextLostStolen" runat="server"></asp:Literal></p>
				<p>
					<asp:Literal ID="NewPassRequestTextCancel" meta:resourcekey="NewPassRequestTextCancel" runat="server"></asp:Literal></p>
				<span style="margin: 5px">
					<dx:ASPxListBox ID="ASPxListBoxBadges" ClientInstanceName="lbBadges" runat="server"
						SelectionMode="CheckColumn" OnCallback="ASPxListBoxBadges_Callback" ValueField="PersonBadgeID"
						TextField="BadgeTag">
					</dx:ASPxListBox>
				</span>
				<br />
				<span style="margin: 2px; float: right;">
					<dx:ASPxButton ID="ASPxButtonReplaceSelectedBadges" runat="server" AutoPostBack="false"
						Text="Replace Badges" meta:resourcekey="ASPxButtonReplaceSelectedBadgesResource1">
						<ClientSideEvents Click="function(s,e){ replaceBadges(); }" />
<ClientSideEvents Click="function(s,e){ replaceBadges(); }"></ClientSideEvents>
					</dx:ASPxButton>
				</span>
				<span style="margin: 2px; float: right;">
					<dx:ASPxButton ID="ASPxButtonCancelReplaceSelectedBadges" runat="server" AutoPostBack="false"
						Text="Cancel" ForeColor="#0098db" 
					meta:resourcekey="ASPxButtonCancelReplaceSelectedBadgesResource1">
						<ClientSideEvents Click="function(s,e){ popupBadges.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupBadges.Hide(); }"></ClientSideEvents>
					</dx:ASPxButton>
				</span>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="ASPxPopupControlSubmitted" ClientInstanceName="popupSubmitted"
		runat="server" SkinID="PopupNoHeader" Width="400px" Height="250px" 
		CloseAction="None" meta:resourcekey="ASPxPopupControlSubmittedResource1">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup('UploadGroup'); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(&#39;UploadGroup&#39;); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
				<asp:Literal ID="RequestSubmitted" runat="server" meta:resourceKey="RequestSubmitted"></asp:Literal>
				</h2>
				<p>
				<asp:Literal ID="NewPassRequestSubmitted" runat="server" meta:resourceKey="NewPassRequestSubmitted"></asp:Literal>
				</p>
				<p id="pCollect">
				<asp:Literal ID="ReadyForCollection" runat="server" meta:resourceKey="ReadyForCollection"></asp:Literal>
				</p>
				<p id="pContact">
				<asp:Literal ID="CollectionOfNewPasses" runat="server" meta:resourceKey="CollectionOfNewPasses"></asp:Literal>
				</p>
				<div id="divPersonsNoBadge" class="highlightedPanel" style="background-color: #CFDAEB;
					padding: 10px; margin: 10px; border: 1px solid #0018A8;">
					<p><asp:Literal ID="NewPassRequestPersonNoDBAccess" meta:resourcekey="NewPassRequestPersonNoDBAccess" runat="server"></asp:Literal>
						</p>
					<p>
                      <asp:Literal ID="NewPassRequestPersonCreatePrompt" meta:resourcekey="NewPassRequestPersonCreatePrompt" runat="server"></asp:Literal>
						</p>
					<dx:ASPxButton ID="ASPxButtonNewPass" runat="server" AutoPostBack="false" Text="Create New Access Request"
						Width="240" OnClick="ASPxButtonNewAccess_click" Style="margin: 0 auto;">
					</dx:ASPxButton>
				</div>
				<p> <asp:Literal ID="NewPassRequestContinue" meta:resourcekey="NewPassRequestContinue" runat="server"></asp:Literal>
					</p>
				<br />
				<div style="margin-left: auto; margin-right: auto;">
					<dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" Text=""
						Width="240" Style="margin: 0 auto;" meta:resourcekey="ASPxButton1">
						<ClientSideEvents Click="function(s,e){ window.location=location.pathname; }" />
<ClientSideEvents Click="function(s,e){ window.location=location.pathname; }"></ClientSideEvents>
					</dx:ASPxButton>
					<dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="false" Text=""
						ForeColor="#0098db" Width="240" Style="margin: 0 auto;" 
						meta:resourcekey="ASPxButton3">
						<ClientSideEvents Click='function(s,e){ window.location="/Pages/MyTasks.aspx"; }' />
<ClientSideEvents Click="function(s,e){ window.location=&quot;/Pages/MyTasks.aspx&quot;; }"></ClientSideEvents>
					</dx:ASPxButton>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="ConfirmCancel" ClientInstanceName="popupConfirmCancel" runat="server"
		SkinID="PopupBasic"  
		meta:resourcekey="ConfirmCancelResource1">
		<ClientSideEvents PopUp="function(s, e){ /* return ASPxClientEdit.ClearGroup(); */ }" />
<ClientSideEvents PopUp="function(s, e){ /* return ASPxClientEdit.ClearGroup(); */ }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
					<asp:Literal ID="NewPassRequestCancel"  text="<%$ Resources: NewPassRequestCancelCancel%>" Visible="true" runat="server" ClientIDMode="Static"> </asp:Literal></h2>
				<p>
					<asp:Literal ID="NewPassRequestCancelConfirm"  text="<%$ Resources:CommonResource, CancelConfirm%>" Visible="true" runat="server" ClientIDMode="Static"> </asp:Literal></h2></p>
				<br />
				<div style="width: 220px; margin-left: auto; margin-right: auto; clear: both">
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="Yes"
							Width="100" OnClick="ASPxButtonCancel_click" meta:resourcekey="ASPxButtonCancelResource1">
							<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div style="float: left; padding: 5px">
						<dx:ASPxButton ID="ASPxButton5" runat="server" AutoPostBack="false" Text="No" 
							Width="100" meta:resourcekey="ASPxButton5Resource1">
							<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmCancel.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="PopupExcelUpload" ClientInstanceName="popupExcelUpload"
		runat="server" SkinID="PopupBasic" HeaderText="Bulk Request" 
		meta:resourcekey="PopupExcelUploadResource1">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup('UploadGroup'); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(&#39;UploadGroup&#39;); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" 
				SupportsDisabledAttribute="True">
				<div style="width: 350px; height: 220px; margin-left: auto; margin-right: auto; clear: both">
					<div style="float: left; padding: 5px">
						<div>
							<asp:Literal ID="ExcelFileText" runat="server" meta:resourcekey="ExcelFileText"></asp:Literal>
						</div>
						<div style="margin-left: 40px">
							- <asp:Literal ID="SavedExcel" runat="server" meta:resourcekey="SavedExcel"></asp:Literal>						
						</div>
						<div style="margin-left: 40px">
							- <asp:Literal ID="NoHeadings" runat="server" meta:resourcekey="NoHeadings"></asp:Literal>
						</div>
						<div style="margin-left: 40px">
							- <asp:Literal ID="OnlyEmployIDS" runat="server" meta:resourcekey="OnlyEmployIDS"></asp:Literal>
						</div>
						<div style="margin-left: 80px">
							- <asp:Literal ID="ExternalIDS" runat="server" meta:resourcekey="ExternalIDS"></asp:Literal>
						</div>
						<br />
						<Upload:InputFile ID="inputFileId" ClientIDMode="Static" runat="server" 
							meta:resourcekey="inputFileId"/>
						<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateUploadFile"
							ErrorMessage="<%$ Resources:CommonResource, SelectValidExcelFile %>" ValidationGroup="UploadGroup"></asp:CustomValidator>
						<Upload:ProgressBar ID="progressBarId" runat="server" Inline="false"></Upload:ProgressBar>
						<br />
						<br />
						<span style="margin: 2px; float: left;">
							<dx:ASPxButton ID="btnSubmitExcelSheet" runat="server" AutoPostBack="false" Text=""
								OnClick="btnSubmitExcelSheet_Click" ValidationGroup="UploadGroup" 
							meta:resourcekey="btnSubmitExcelSheetUpload">
							</dx:ASPxButton>
						</span>
						<span style="margin: 2px; float: left;">
							<dx:ASPxButton ID="ASPxButton4" runat="server" AutoPostBack="false" Text=""
								ForeColor="#0098db" ValidationGroup="UploadGroup" meta:resourcekey="ASPxButton4Cancel">
								<ClientSideEvents Click="function(s,e){ popupExcelUpload.Hide(); }" />
                                    <ClientSideEvents Click="function(s,e){ popupExcelUpload.Hide(); }"></ClientSideEvents>
							</dx:ASPxButton>
						</span>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="PopupNewPassRequestError" ClientInstanceName="popupNewPassRequestError"
		runat="server" SkinID="PopupCustomSize" Width="300px" Height="150px" 
		HeaderText="<%$ Resources: CreateNewPassHeading %>" 
		meta:resourcekey="PopupNewPassRequestErrorResource1">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />
<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }"></ClientSideEvents>
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" 
				SupportsDisabledAttribute="True">
				<div style="width: 250px; margin-left: auto; margin-right: auto; clear: both">
					<asp:TextBox ID="textBoxPopupErrorMessage" runat="server" Width="250" BorderStyle="None"
						BackColor="#FFFFFF" Style="overflow-y: hidden" TextMode="MultiLine" Rows="5" 
						meta:resourcekey="NewPassRequestNotCreated"></asp:TextBox>
					<div align="center">
						<dx:ASPxButton ID="btnRequestFailOk" runat="server" AutoPostBack="false" 
							Text="Ok">
							<ClientSideEvents Click="function(s,e){ popupNewPassRequestError.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupNewPassRequestError.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<ppc:Popups ID="PopupTemplates" runat="server" />

        <!--literals-->

    <asp:Literal ID="newPassRequestEmailValidator"  text="<%$ Resources:CommonResource, InvalidEmail%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="AddPersonLiteral"  text="<%$ Resources:CommonResource, AddPerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="SelectValidPerson" text="<%$ Resources:CommonResource, SelectValidPerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="RequestCouldNotBeCreated"  text="<%$ Resources:CommonResource, RequestCouldNotBeCreated%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewRequestExistingPersonRequestError"  meta:resourcekey="NewRequestExistingPersonRequestError"   Visible="false"    runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewRequestRepPassError" meta:resourcekey="NewRequestRepPassError"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="CannotAddInactivePerson"   text="<%$ Resources:CommonResource, CannotAddInactivePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewRequestRepBadges"  meta:resourcekey="NewRequestRepBadges"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewRequestNoExistingBadgeError"  meta:resourcekey="NewRequestNoExistingBadgeError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="NewPassRequestSubmit" meta:resourcekey="NewPassRequestSubmit"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestPAP"  meta:resourcekey="NewPassRequestPAP"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    
    <asp:Literal ID="NewPassRequestBRJustification"  meta:resourcekey="NewPassRequestBRJustification" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestDuplicateRequest"  meta:resourcekey="NewPassRequestDuplicateRequest" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestDuplicatePasses"  meta:resourcekey="NewPassRequestDuplicatePasses" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="NewPassRequestSelectPassOff"  meta:resourcekey="NewPassRequestSelectPassOff" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestPhone"  meta:resourcekey="NewPassRequestPhone" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestCollectContact"  meta:resourcekey="NewPassRequestCollectContact" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestNotSubmitted"  meta:resourcekey="NewPassRequestNotSubmitted" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestNotExistBulk"  meta:resourcekey="NewPassRequestNotExistBulk" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="NewPassRequestPendReq"  meta:resourcekey="NewPassRequestPendReq" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="DeletePersonLiteral"   text="<%$ Resources:CommonResource, DeletePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="NewPassRequestPersNotDeleted"  meta:resourcekey="NewPassRequestPersNotDeleted" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="NewPassRequestPasses"  meta:resourcekey="NewPassRequestPasses" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="SmartPassAlertHeader" meta:resourcekey="SmartPassAlertHeader"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="SmartPassAlertBody"  meta:resourcekey="SmartPassAlertBody"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="SmartPassJustificationBody"  meta:resourcekey="SmartPassJustificationBody"  Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

</asp:Content>
