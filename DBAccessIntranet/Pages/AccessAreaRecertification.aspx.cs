﻿using System;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.Services;
using System.Web.Script.Services;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Collections.Generic;
using DevExpress.Web.ASPxGridView;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class AccessAreaRecertification : BaseSecurityPage
    {

        private string CacheKey
        {
            get
            {
                if (ASPxHiddenFieldDivision.Contains("CacheKey"))
                    return ASPxHiddenFieldDivision.Get("CacheKey").ToString();
                else
                {
                    //cache key not created yet, so create it
                    string cacheKey = "AccessAreas/" + base.CurrentUser.dbPeopleID.ToString() + "/" + DateTime.Now.ToOADate().ToString();
                    ASPxHiddenFieldDivision.Set("CacheKey", cacheKey);
                    return cacheKey;
                }
            }
        }
		 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                SetGridDataSource();
            }
            else
            {
                SetCachedGridDataSource();
            }                       
        }

        /// <summary>
        /// get the cached data and assign to grid
        /// </summary>
        void SetCachedGridDataSource()
        {
            if (Cache[this.CacheKey] != null)
            {
                if (this.CurrentUser.RolesShortNamesList.Contains("ADMIN"))
                {
                    List<DBAccessController.DAL.vw_AdminRecertifierAccessAreas> accessAreas = (List<DBAccessController.DAL.vw_AdminRecertifierAccessAreas>)Cache[this.CacheKey];
                    ASPxGridViewAccessAreas.DataSource = accessAreas;
                }
                else
                {
                     List<DBAccessController.DAL.vw_RecertifierAccessAreas> accessAreas = (List<DBAccessController.DAL.vw_RecertifierAccessAreas>)Cache[this.CacheKey];
                     ASPxGridViewAccessAreas.DataSource = accessAreas;
                }

                ASPxGridViewAccessAreas.DataBind();                
            }
        }

        void SetGridDataSource()
        {
            if (this.CurrentUser.RolesShortNamesList.Contains("ADMIN"))
            {
                var accessAreas = Director.GetAdminRecertifyAccessAreas(this.CurrentUser.dbPeopleID);
                Cache.Insert(this.CacheKey, accessAreas);

                ASPxGridViewAccessAreas.DataSource = accessAreas;                  
            }
            else
            {
                var accessAreas = Director.GetMyRecertifyAccessAreas(this.CurrentUser.dbPeopleID);
                Cache.Insert(this.CacheKey, accessAreas);

                ASPxGridViewAccessAreas.DataSource = accessAreas;
            }
            
            ASPxGridViewAccessAreas.DataBind();
        }


        void SetAccessAreaDataSource(int AccessAreaID, int divisionID)
        {
            ASPxGridViewAccessDetails.DataSource = Director.GetRecertifierPeople(AccessAreaID, divisionID);
            ASPxGridViewAccessDetails.DataBind();            
        }

        #region ASPX methods

		/// <summary>
        /// Method called by the actio nbutton on the access area dg.
        /// E.Parker 
        /// March 2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void aspxButtonAcessAreaClick(object sender, EventArgs e)
        {
            string paramsInput = ((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument;

            char splitString = Convert.ToChar("|");

            string[] paramsInputArray = paramsInput.Split(splitString);
            


            int accessAreaID = Convert.ToInt32(paramsInputArray[0]);
            int divisionID = Convert.ToInt32(paramsInputArray[1]);

            SetAccessAreaDataSource(accessAreaID, divisionID);

            //set up hidden field with AccessAreaID & DivisonID
            HFDivision.Value = divisionID.ToString();
            HFArea.Value = accessAreaID.ToString();
        
            string tempBreadCrumbs = paramsInputArray[2].ToString() + "&nbsp;>>&nbsp;" + paramsInputArray[3].ToString() + "&nbsp;>>&nbsp;" + paramsInputArray[4].ToString() + "&nbsp;>>&nbsp;" + paramsInputArray[5].ToString() + "&nbsp;>>&nbsp;" + paramsInputArray[6].ToString() + "&nbsp;>>&nbsp;" + paramsInputArray[7].ToString();
            AccessAreaName.Text = tempBreadCrumbs;
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }

        protected void btnXlsxExportPeople_Click(object sender, EventArgs e)
        {
            int AccessArea = Convert.ToInt32(HFArea.Value);
            int DivisionArea = Convert.ToInt32(HFDivision.Value);

            SetAccessAreaDataSource(AccessArea, DivisionArea);

            ASPxGridViewAccessDetails.BeginUpdate();
            ASPxGridViewAccessDetails.DataBind();
            ASPxGridViewAccessDetails.EndUpdate();

            gridExportTwo.DataBind();

            gridExportTwo.WriteXlsxToResponse();
        }

        protected void gvAccessAreaPeople_Callback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] parms = e.Parameters.Split('|');

            Int32 iDivisionId = Convert.ToInt32(parms[0]);
            Int32 iAccessAreaId = Convert.ToInt32(parms[1]);

            SetAccessAreaDataSource(iAccessAreaId, iDivisionId);
        }
    #endregion

    #region WebMethods

        [WebMethod, ScriptMethod]
        public static bool RejectedAccessAreaRecertify(string[] dbPeopleIDs, int AccessAreaID, int DivisionID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(CurrentUser.dbPeopleID);
            int ApproverID = CurrentUser.dbPeopleID;

            string users = string.Join(",", dbPeopleIDs);

            bool success = director.RejectAccessAreaRecertify(users, AccessAreaID, DivisionID, ApproverID);
         
            return success;
        }

        [WebMethod, ScriptMethod]
        public static bool ApprovedAccessAreaRecertify(string[] dbPeopleIDs, int AccessAreaID, int DivisionID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(CurrentUser.dbPeopleID);
            int ApproverID = CurrentUser.dbPeopleID;

            string users = string.Join(",", dbPeopleIDs);

            bool success = director.ApprovedAccessAreaRecertify(users,DivisionID, AccessAreaID, ApproverID);

            return success;
        }


        #endregion

       
      
    }
}
