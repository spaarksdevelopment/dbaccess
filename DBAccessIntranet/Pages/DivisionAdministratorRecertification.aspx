﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="DivisionAdministratorRecertification.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.DivisionAdministratorRecertification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">
    var selections = new Array();
    $(document).ready(function () {
        //hide the help function
        $('#RoleHelp').bind('click', function () {
            $("#RoleRecertInfo").slideToggle("slow");
        });
        $('#RoleRecertInfo').hide();
    });
    function SelectedDivisionIndexChanged() {
        Grid.PerformCallback();
    }
    function Recertify() {
        PageMethods.Recertify(selections, OnRecertify);
    }
    function OnRecertify(result) {
        if (result==0) {
            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "There was an unknown problem recertifying these selections";
            popupRecertificationError.Show();
        }
        else if(result==-1){
            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "You cannot recertify these administrators because there would be more than 5 administrators in the division";
            popupRecertificationError.Show();
        }
        else {
            Grid.PerformCallback();
        }
    }
    function Remove() {
        PageMethods.Remove(selections, OnRemove);
    }
    function OnRemove(result) {
        if (result == 0) {
            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "There was an unknown problem removing certification for these selections";
            popupRecertificationError.Show();
        }
        else if (result == -1) {
            $get("<%=textBoxPopupErrorMessage.ClientID%>").value = "You cannot remove these approvers because there would be less than 2 administrators in the division";
            popupRecertificationError.Show();
        }
        else {
            Grid.PerformCallback();
        }
    }
    function GridSelectionChanged(s, e) {
        s.GetSelectedFieldValues("ColumnId", GetSelectedFieldValuesCallback);
    }
    function GetSelectedFieldValuesCallback(values) {
        selections = [];
        for (var i = 0; i < values.length; i++) {
            selections.push(values[i]);
        }
    }

 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <h1>Division Administrator Recertification</h1>

     <p>
        Here you can recertify Divisional Administrators to administer access requests.<img id="RoleHelp" src="../App_Themes/DBIntranet2010/images/question.gif" title="Click here for help" alt="Click here for help"/>
    </p>

    <div id="RoleRecertInfo" class="HelpPopUp">
       <p>Division Administrator Recertification is designed to show only the Divisional Administrators in your division.      
       </p>
       <p>Where you see the pin image, clicking on this will allow you to filter the data within the grid.</p>
        <p> Where you see the following icon, you can export the data.
             <asp:Image ID="Help" SkinID="FileXLS"  AlternateText="Export data" title="Export Data" runat="server" />
        </p>        
    </div>
    <div style="margin: 10px" id="divRequestPersons">
        <p>Click here to export this data to Excel.
            <asp:ImageButton AlternateText="Click to export grid data to an Excel sheet" runat="server" ImageUrl="~/App_Themes/DBIntranet2010/images/file_xls.gif"  title="Click to export grid data to an Excel sheet" onclick="btnXlsxExport_Click" onmouseover="this.src=this.src.replace('.gif','_hl.gif');" onmouseout="this.src=this.src.replace('_hl.gif','.gif');"/>
        </p>

        <dx:ASPxGridView ID="Grid" ClientInstanceName="Grid"
            runat="server" AutoGenerateColumns="False"
            KeyFieldName="ColumnId"
            OnCustomCallback="CustomCallback" 
            ClientIDMode="AutoID" Width="1000px" >
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">
                    <HeaderTemplate>
                        <input type="checkbox" onclick="Grid.SelectAllRowsOnPage(this.checked);" title="Select/Unselect all rows on the page" />
                    </HeaderTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Country" FieldName="CountryName" 
                    ShowInCustomizationForm="True" VisibleIndex="0">
                <Settings AllowHeaderFilter="True"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Division" FieldName="DivisionName" 
                    ShowInCustomizationForm="True" VisibleIndex="1">
                <Settings AllowHeaderFilter="True"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Role Type" FieldName="RoleName" 
                    ShowInCustomizationForm="True" VisibleIndex="2">
                    <Settings AllowHeaderFilter="True"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Sequence" FieldName="SortOrder" 
                    ShowInCustomizationForm="True" VisibleIndex="3">
                <Settings AllowHeaderFilter="True"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Accepted" FieldName="DateAccepted" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}"
                    ShowInCustomizationForm="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Last Recertified" PropertiesDateEdit-DisplayFormatString="{0:dd-MMM-yyyy}" 
                    ShowInCustomizationForm="True" FieldName="DateRecertified" VisibleIndex="5">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="Users Email" FieldName="EmailAddress" 
                    ShowInCustomizationForm="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Users UBR" FieldName="UBR" 
                    ShowInCustomizationForm="True" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Certification Due" PropertiesDateEdit-DisplayFormatString="{0:dd-MMM-yyyy}" 
                    FieldName="DateNextCertification" ShowInCustomizationForm="True" 
                    VisibleIndex="8">
                </dx:GridViewDataDateColumn>
<%--                <dx:GridViewDataTextColumn Caption="Is Recertified"  FieldName="IsRecertified" 
                    VisibleIndex="9">
                </dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataTextColumn Caption="Users Host Country" FieldName="HostCountry" 
                    ShowInCustomizationForm="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description"  FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Settings ShowGroupPanel="true" />
            <SettingsPager PageSize="25" />
            <ClientSideEvents SelectionChanged="GridSelectionChanged" />
        </dx:ASPxGridView>
        <div style="float:left;">
        <dx:ASPxButton ID="btnConfirm" AutoPostBack="false" runat="server" Text="Recertify Selected Approvers">
            <ClientSideEvents Click="Recertify" />
        </dx:ASPxButton></div> <div style="float:left;">
        <dx:ASPxButton ID="btnRemove" AutoPostBack="false" runat="server" Text="Remove Selected Approvers">
            <ClientSideEvents Click="Remove" />
        </dx:ASPxButton></div>
        <br />
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="Grid"></dx:ASPxGridViewExporter>
    </div>
	<dx:ASPxPopupControl ID="PopupRecertificationError" ClientInstanceName="popupRecertificationError" runat="server"
		SkinID="PopupCustomSize"
		Width="300px"
		Height="150px"
		HeaderText="Problem Processing Request">
		<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />
		<ContentCollection>            
			<dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" SupportsDisabledAttribute="True">
			<div style="width:250px;margin-left:auto;margin-right:auto;clear:both">
				<asp:TextBox ID="textBoxPopupErrorMessage" runat="server" Width="250" BorderStyle="None" BackColor="#FFFFFF" Style="overflow-y:hidden" TextMode="MultiLine" Rows="5"></asp:TextBox>
				<div align="center">            
					<dx:ASPxButton ID="btnRequestFailOk" runat="server" AutoPostBack="false" Text="Ok" >
						<ClientSideEvents Click="function(s,e){ popupRecertificationError.Hide(); }" />
					</dx:ASPxButton>
				</div>
			</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
</asp:Content>
