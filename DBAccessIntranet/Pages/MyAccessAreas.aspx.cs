﻿using System;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Resources;
using System.Threading;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;
using Spaarks.Common.UtilityManager;
using DBAccess.Model;
using DBAccess.Model.Enums;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class MyAccessAreas : BaseSecurityPage
	{
		public const String AccessAreaApproverSessionKey = "MyAccessAreas/AccessAreaApprovers_{0}";
		public const String AccessAreaRecertifierSessionKey = "MyAccessAreas/AccessAreaRecertifiers_{0}";

        protected bool isAdmin = false;

        private enum SortType
	    {
            MoveUp,
            MoveDown,
            None
	    }

		private string CacheKey
		{
			get
			{
				if (ASPxHiddenFieldDivisionAccessArea.Contains("CacheKey"))
				{
					return ASPxHiddenFieldDivisionAccessArea.Get("CacheKey").ToString();
				}

				//cache key not created yet, so create it
                string cacheKey = "AccessAreas/" + CurrentUser.dbPeopleID + "/" + DateTime.Now.ToOADate();
				ASPxHiddenFieldDivisionAccessArea.Set("CacheKey", cacheKey);
				return cacheKey;
			}
		}

		private int languageID
		{
			get { return Helper.SafeInt(this.CurrentUser.LanguageId); }
			set { this.CurrentUser.LanguageId = value; }
		}

        public static int minimumNumberOfAccessApprovers
        {
            get { return (int)ConfigurationParameters.GetIntParameter("MinimumApprovers"); }
        }

		private int NumberOfRowsInAAGrid = 0;
		private int NumberOfRowsInARGrid = 0;

		protected void Page_Load(object sender, EventArgs e)
		{
			base.AddHeaderScriptInclude("~/scripts/validationservice.js");
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(languageID, currentUserLanguageCode);
			languageID = Convert.ToInt32(LanguageId);

			string sourceOfPostBack = Request.Form["__EventTarget"];

			if ((!Page.IsPostBack) || (sourceOfPostBack == Convert.ToString(DBAccessEnums.Language.English) || sourceOfPostBack == Convert.ToString(DBAccessEnums.Language.German) || sourceOfPostBack == Convert.ToString(DBAccessEnums.Language.Italian)))
			{
				GetAccessAreas();
				BindClassification();
			}
			else
				GetCachedData();

            isAdmin = CurrentUser.IsInRole("ADMIN");
        }

        #region Web Methods
        [WebMethod(EnableSession = true), ScriptMethod]
        public static void RemoveDraftRequestsForThisAccessArea(int corporateDivisionAccessAreaID)
		{
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            GenericRequestService genericService = new GenericRequestService();
            genericService.RemoveDraftAccessAreaRoleUserRequestsForCurrentUser(corporateDivisionAccessAreaID, currentUserID);

            //Clear the session to remove cancelled changes from the grids
            SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, null, AccessAreaApproverSessionKey);
            SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, null, AccessAreaRecertifierSessionKey);
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static string AddPersonAA(Int32 iPersonID, Int32 iRequestMasterID, Int32 iSortOrder, Int32 corporateDivisionAccessAreaID, Int32 iClassificationID, String sClassificationName)
		{
			int RequestId = 0;
			if (-1 == iClassificationID) return Convert.ToString(0);		// cannot add to 'all approvers' list

			var listAccessAreaApprovers = GetAccessAreaApprovers(corporateDivisionAccessAreaID);
            var approverCount = listAccessAreaApprovers.Count;

            if (approverCount >= MaximumApprovers) return Convert.ToString(0); // we've reached the maximum number of approvers

            if (approverCount == 0) iSortOrder = 1;
            else if (iSortOrder > approverCount + 1) iSortOrder = approverCount + 1;

			HRPerson person = Director.GetPersonById(iPersonID);

			if (person == null) return Convert.ToString(0);		// we cannot find the person

			// update the list with this person
			// add in details so that we can see what this user is in the grid
			AccessAreaApprover newPerson = new AccessAreaApprover();
			newPerson.dbPeopleID = iPersonID;
			newPerson.CorporateDivisionAccessAreaID = corporateDivisionAccessAreaID;
			newPerson.SortOrder = (byte)iSortOrder;
			newPerson.Enabled = false;
			newPerson.Name = person.Forename + ' ' + person.Surname;
			newPerson.EmailAddress = person.EmailAddress;
			newPerson.ClassificationID = iClassificationID;
			newPerson.Classification = (0 != iClassificationID) ? sClassificationName : "";

			// make sure it is not a duplicate
			if (0 != listAccessAreaApprovers.Where(a => a.dbPeopleID == iPersonID && a.ClassificationID == iClassificationID).Count()) return Convert.ToString(0);

            // Make room in the list for the new item
            listAccessAreaApprovers.Where(r => r.ClassificationID == iClassificationID && r.SortOrder >= iSortOrder).ToList().ForEach(r => r.SortOrder++);

			// okay, add it to the list
            listAccessAreaApprovers.Add(newPerson);
		
			// update the cache so that it is synchronised
			SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, listAccessAreaApprovers, AccessAreaApproverSessionKey);

			// no base class available when using a static method, so need to get the user
			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			// Now create the badge request
            Director director = new Director(currentUser.dbPeopleID);

			int bSuccess = director.CreateNewRequestForAccessApprover(iPersonID, ref RequestId, ref iRequestMasterID, corporateDivisionAccessAreaID, iClassificationID);

			if (bSuccess == 0 || bSuccess == -3)
			{
				// okay, remove the person from list
				listAccessAreaApprovers.Remove(newPerson);
			}

			return (bSuccess != 0 && bSuccess != -3) ? iRequestMasterID + "|" + RequestId + "|" + person.dbPeopleID : Convert.ToString(bSuccess);
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool DeletePersonAA(int? accessRequestID, Int32 personToDeleteDBPeopleID, Int32 corporateDivisionAccessAreaID, Int32 iClassificationID)
		{
			var listAccessAreaApprovers = GetAccessAreaApprovers(corporateDivisionAccessAreaID);

			// check the person is in the list
			if (0 == listAccessAreaApprovers.Select(a => a.dbPeopleID == personToDeleteDBPeopleID && a.ClassificationID == iClassificationID).Count()) return false;
            
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();

            IGenericRequestService genericRequestService = new GenericRequestService();

            genericRequestService.DeleteAccessAreaRoleUser(corporateDivisionAccessAreaID, RoleType.AccessApprover, personToDeleteDBPeopleID);
            genericRequestService.RemoveDraftAndPendingAccessAreaRoleRequestForDeletedPerson(corporateDivisionAccessAreaID, RoleType.AccessApprover, personToDeleteDBPeopleID);

			// remove the person from the list
			listAccessAreaApprovers.Remove(listAccessAreaApprovers.Where(a => a.dbPeopleID == personToDeleteDBPeopleID && a.ClassificationID == iClassificationID).First());

			//rebuild the sort order in list per classificationID basing on the list entries
            Director director = new Director(currentUserID);
			director.UpdateSortOrder(listAccessAreaApprovers, iClassificationID, DBAccessEnums.RequestType.AccessAA);

			return true;
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool ConfirmChangesAA(Int32 corporateDivisionAccessAreaID, Int32 iRequestMasterID)
		{
			var listAccessAreaApprovers = GetAccessAreaApprovers(corporateDivisionAccessAreaID);

            if (listAccessAreaApprovers.Count < minimumNumberOfAccessApprovers)
            {
                return false;
            }

			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);

			// update the AccessAreaApprovers table to add/remove people
			if (!director.UpdateAccessAreaApprovers(corporateDivisionAccessAreaID, listAccessAreaApprovers)) return false;

			if (iRequestMasterID != 0)
			{
				// create any tasks necessary
				if (!director.SubmitRequestGeneric(iRequestMasterID)) return false;
			}

			// clear the session state
			SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, null, AccessAreaApproverSessionKey);
			return true;
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static string AddPersonAR(Int32 newAccessRecertifierDBPeopleID, Int32 iRequestMasterID, Int32 iSortOrder, Int32 corporateDivisionAccessAreaID)
		{
			int RequestId = 0;
			var listAccessAreaRecertifiers = GetAccessAreaRecertifiers(corporateDivisionAccessAreaID);
            var recertifierCount = listAccessAreaRecertifiers.Count;

            if (recertifierCount >= MaximumRecertifiers) return Convert.ToString(0); // we've reached the maximum number of recertifiers

            if (recertifierCount == 0) iSortOrder = 1;
            else if (iSortOrder > recertifierCount + 1) iSortOrder = recertifierCount + 1;

			HRPerson person = Director.GetPersonById(newAccessRecertifierDBPeopleID);

			if (person == null) return Convert.ToString(0);		// we cannot find the person

			// update the list with this person
			// add in details so that we can see what this user is in the grid
			AccessAreaApprover newPerson = new AccessAreaApprover();
            newPerson.dbPeopleID = newAccessRecertifierDBPeopleID;
			newPerson.CorporateDivisionAccessAreaID = corporateDivisionAccessAreaID;
			newPerson.SortOrder = (byte)iSortOrder;
			newPerson.Enabled = false;
			newPerson.Name = person.Forename + ' ' + person.Surname;
			newPerson.EmailAddress = person.EmailAddress;

			// make sure it is not a duplicate
			if (0 != listAccessAreaRecertifiers.Where(a => a.dbPeopleID == newAccessRecertifierDBPeopleID && a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID).Count()) return Convert.ToString(0);

            // Make room in the list for the new item
            listAccessAreaRecertifiers.Where(r => r.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID && r.SortOrder >= iSortOrder).ToList().ForEach(r => r.SortOrder++);

            // okay, add it to the list
            listAccessAreaRecertifiers.Add(newPerson);

			// update the cache so that it is synchronised
            SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, listAccessAreaRecertifiers, AccessAreaRecertifierSessionKey);

			// no base class available when using a static method, so need to get the user
			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			// Now create the badge request
            Director director = new Director(currentUser.dbPeopleID);

			int bSuccess = director.CreateNewRequestForAr(newAccessRecertifierDBPeopleID, ref RequestId, ref iRequestMasterID, corporateDivisionAccessAreaID);

			if (bSuccess == 0 || bSuccess == -3)
			{
				// okay, remove the person from list
				listAccessAreaRecertifiers.Remove(newPerson);
			}

			return (bSuccess != 0 && bSuccess != -3) ? iRequestMasterID + "|" + RequestId + "|" + person.dbPeopleID : Convert.ToString(bSuccess);
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool DeletePersonAR(int? accessRequestID, int accessRecertifierToDeleteDBPeopleID, int corporateDivisionAccessAreaID)
		{
			var listAccessAreaRecertifiers = GetAccessAreaRecertifiers(corporateDivisionAccessAreaID);

			// check the person is in the list
			if (0 == listAccessAreaRecertifiers.Select(a => a.dbPeopleID == accessRecertifierToDeleteDBPeopleID && a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID).Count()) return false;

            IGenericRequestService genericRequestService = new GenericRequestService();

            genericRequestService.DeleteAccessAreaRoleUser(corporateDivisionAccessAreaID, RoleType.AccessRecertifier, accessRecertifierToDeleteDBPeopleID);
            genericRequestService.RemoveDraftAndPendingAccessAreaRoleRequestForDeletedPerson(corporateDivisionAccessAreaID, RoleType.AccessRecertifier, accessRecertifierToDeleteDBPeopleID);

			// remove the person from the list
			listAccessAreaRecertifiers.Remove(listAccessAreaRecertifiers.Where(a => a.dbPeopleID == accessRecertifierToDeleteDBPeopleID && a.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID).First());

			//rebuild the sort order in list per classificationID basing on the list entries
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            Director director = new Director(currentUserID);
			director.UpdateSortOrder(listAccessAreaRecertifiers, corporateDivisionAccessAreaID, DBAccessEnums.RequestType.AccessAREC);
			return true;
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool ConfirmChangesAR(Int32 corporateDivisionAccessAreaID, Int32 iRequestMasterID)
		{
			var listAccessAreaRecertifiers = GetAccessAreaRecertifiers(corporateDivisionAccessAreaID);

			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);

			// update the AccessAreaRecertifiers table to add/remove people
			if (!director.UpdateAccessAreaRecertifiers(corporateDivisionAccessAreaID, listAccessAreaRecertifiers)) return false;

			if (iRequestMasterID != 0)
			{
				// create any tasks necessary
				if (!director.SubmitRequestGeneric(iRequestMasterID)) return false;
			}

			// clear the session state
			SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, null, AccessAreaRecertifierSessionKey);
			return true;
		}

		[WebMethod, ScriptMethod]
		public static bool UpdateArea(Int32 iAccessAreaId, Int32 iFrequency, Int32 iApprovalType, Int32 iApprovals,
									   Boolean bEnabled, String sName)
		{
            if (iApprovals > MaximumApprovals) return false; // Code for too many approvals

			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
			bool success = director.UpdateMyAccessArea(iAccessAreaId, bEnabled, iApprovalType, iApprovals, iFrequency, sName);

			return success;
		}
        #endregion

        #region Events
        protected void LocationsGridCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GetAccessAreas();
        }

        protected void Approvers_CustomJSProperties(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs e)
        {
            e.Properties["cpMaxOrder"] = NumberOfRowsInAAGrid;
        }

        protected void ApproversCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] parms = e.Parameters.Split('|');

            if (parms.Length < 3) return;

            bool bUpdate = Convert.ToBoolean(parms[0]);
            int corporateDivisionAccessAreaID = Convert.ToInt32(parms[1]);
            int iClassificationId = Convert.ToInt32(parms[2]);


            Boolean bShowAll = (-1 == iClassificationId);
            List<AccessAreaApprover> listAccessAreaApprovers = null;

            listAccessAreaApprovers = GetAccessAreaRoleUsers(corporateDivisionAccessAreaID, bUpdate, DBAccessEnums.RoleType.AccessApprover, AccessAreaApproverSessionKey);
            
            // get and sort the list we are going to display locally (by classification then sort order)
            // show all people (-1 == iClassificationId) or the people for a specific classification (0 represents unclassified)
            listAccessAreaApprovers = listAccessAreaApprovers.Where(a => -1 == iClassificationId || a.ClassificationID == iClassificationId).OrderBy(a => a.ClassificationID).ThenBy(a => a.SortOrder).ToList();

            //5 parameters => sorting
            //iClassificationId = -1 => all selected so no sorting
            if (5 == parms.Length && -1 != iClassificationId)
            {
                int direction = Convert.ToInt32(parms[3]);
                int dbPeopleID = Convert.ToInt32(parms[4]);

                AccessAreaApprover approver = listAccessAreaApprovers.Where(a => a.dbPeopleID == dbPeopleID).SingleOrDefault();

                if (approver != null)
                {
                    int indexOfItemToSwap = listAccessAreaApprovers.IndexOf(approver);
                    SwapListItems.SwapSortOrders<AccessAreaApprover>(direction, indexOfItemToSwap, ref listAccessAreaApprovers);
                }
            }

            //  Check for the sorting
            if (parms.Length > 5)
            {
                //  The current start of the grid
                string approverDBPeopleIDsInOrderBeforeSortWithCommas = parms[4];

                SortType sortType = GetSortType(parms[3]);
                int accessAreaRoleUserToMoveDBPeopleID = int.Parse(parms[5]);

                listAccessAreaApprovers = MovePersonUpOrDownListAndUpdateSessionList(approverDBPeopleIDsInOrderBeforeSortWithCommas, sortType,
                    accessAreaRoleUserToMoveDBPeopleID, listAccessAreaApprovers, corporateDivisionAccessAreaID, AccessAreaApproverSessionKey);

            }

            Approvers.DataSource = listAccessAreaApprovers;
            Approvers.DataBind();

            // these columns have the same parameters, so use the index to get them
            Approvers.Columns[1].Visible = !bShowAll;
            Approvers.Columns[2].Visible = bShowAll;
            Approvers.Columns["Classification"].Visible = bShowAll;

            NumberOfRowsInAAGrid = listAccessAreaApprovers.Count();

            SetStatusBarButtonVisibility(Approvers, "btnAdd", bShowAll);
            SetStatusBarButtonVisibility(Approvers, "btnConfirm", bShowAll);
            SetStatusBarButtonVisibility(Approvers, "buttonCancelBusinessAreaApprovers", bShowAll);
        }

	    private static void SetStatusBarButtonVisibility(ASPxGridView grid, string buttonID, bool showAll)
	    {
            ASPxButton button = grid.FindStatusBarTemplateControl(buttonID) as ASPxButton;
            if (button != null) 
                button.Enabled = !showAll;
	    }

	    protected void Recertifiers_CustomJSProperties(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs e)
        {
            e.Properties["cpMaxOrderAR"] = NumberOfRowsInARGrid;
        }

        protected void RecertifiersCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] parms = e.Parameters.Split('|');

            if (parms.Length < 2) return;

            bool bUpdate = Convert.ToBoolean(parms[0]);
            int corporateDivisionAccessAreaID = Convert.ToInt32(parms[1]);


            List<AccessAreaApprover> listAccessAreaRecertifiers = null;

            listAccessAreaRecertifiers = GetAccessAreaRoleUsers(corporateDivisionAccessAreaID, bUpdate, DBAccessEnums.RoleType.AccessRecertifier
                    , AccessAreaRecertifierSessionKey);
            
            if (listAccessAreaRecertifiers != null)
                listAccessAreaRecertifiers = listAccessAreaRecertifiers.OrderBy(a => a.SortOrder).ToList();

            if (4 == parms.Length)
            {
                int direction = Convert.ToInt32(parms[2]);
                int dbPeopleID = Convert.ToInt32(parms[3]);

                AccessAreaApprover recertifier = listAccessAreaRecertifiers.Where(a => a.dbPeopleID == dbPeopleID).SingleOrDefault();

                if (recertifier != null)
                {
                    int indexOfItemToSwap = listAccessAreaRecertifiers.IndexOf(recertifier);
                    SwapListItems.SwapSortOrders<AccessAreaApprover>(direction, indexOfItemToSwap, ref listAccessAreaRecertifiers);
                }
            }

            //  Check for the sorting
            if (parms.Length > 4)
            {
                SortType sortType = GetSortType(parms[2]);
                string approverDBPeopleIDsInOrderBeforeSortWithCommas = parms[3];
                int accessAreaRoleUserToMoveDBPeopleID = int.Parse(parms[4]);

                listAccessAreaRecertifiers =
                    MovePersonUpOrDownListAndUpdateSessionList(approverDBPeopleIDsInOrderBeforeSortWithCommas, sortType,
                    accessAreaRoleUserToMoveDBPeopleID, listAccessAreaRecertifiers, corporateDivisionAccessAreaID, AccessAreaRecertifierSessionKey);
            }

            NumberOfRowsInARGrid = listAccessAreaRecertifiers.Count();

            Recertifiers.DataSource = listAccessAreaRecertifiers;
            Recertifiers.DataBind();
        }

		protected void btnXlsxExport_Click(object sender, EventArgs e)
		{
			List<vw_DivisionAccessAreasWithApprovers> exportData = Director.GetAccessAreasAndApproversByDA(CurrentUser.dbPeopleID);
			List<string> columnsToExport = GetColumnNamesForExport();

			List<List<string>> exportDataStrings = ExcelExport.GetRowData(exportData, columnsToExport);

			List<string> columnHeaders = GetColumnHeaders();

			ExcelExport.ExportToExcel("MyAccessAreas.xlsx", columnHeaders, exportDataStrings);
		}

		protected void LocationsGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6'; this.style.cursor = 'hand';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}

		protected void Approvers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			this.HighlightGridRowsOnMouseOver(e);
		}

		protected void Recertifiers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			this.HighlightGridRowsOnMouseOver(e);
		}
        #endregion events

        #region Private helpers
        private static void SetAccessAreaRoleUsersSessionList(int corporateDivisionAccessAreaID, List<AccessAreaApprover> listAccessAreaRoleUsers, string sessionKey)
        {
            string sessionKeyForAccessArea = GetSessionKeyForAccessArea(corporateDivisionAccessAreaID, sessionKey);
            HttpContext.Current.Session[sessionKeyForAccessArea] = listAccessAreaRoleUsers;
        }

        private static List<AccessAreaApprover> GetAccessAreaApprovers(Int32 corporateDivisionAccessAreaID)
        {
            return GetAccessAreaRoleUsers(corporateDivisionAccessAreaID, false, DBAccessEnums.RoleType.AccessApprover, AccessAreaApproverSessionKey);
        }

        private static List<AccessAreaApprover> GetAccessAreaRoleUsers(int corporateDivisionAccessAreaID, Boolean bUpdate,
            DBAccessEnums.RoleType accessAreaRoleUserType, string sessionKey)
        {
            string accessAreaRoleUserSessionKey = GetSessionKeyForAccessArea(corporateDivisionAccessAreaID, sessionKey);

            var listAccessAreaRoleUsers = new List<AccessAreaApprover>();

            // pick up the list from the cache if it exists
            if (HttpContext.Current.Session[accessAreaRoleUserSessionKey] == null | bUpdate)
            {
                listAccessAreaRoleUsers = Director.GetMyAccessAreaApprovers(corporateDivisionAccessAreaID, (int)accessAreaRoleUserType);
                SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, listAccessAreaRoleUsers, accessAreaRoleUserSessionKey);
            }
            listAccessAreaRoleUsers = (List<AccessAreaApprover>)HttpContext.Current.Session[accessAreaRoleUserSessionKey];

            if (bUpdate)
            {
                var listDatabaseAccessAreaApprovers = Director.GetMyAccessAreaApprovers(corporateDivisionAccessAreaID, (int)accessAreaRoleUserType);

                UpdateOutOfOfficeDatesFromDatabase(listDatabaseAccessAreaApprovers, listAccessAreaRoleUsers);
            }

            return listAccessAreaRoleUsers;
        }

        private static List<AccessAreaApprover> GetAccessAreaRecertifiers(Int32 corporateDivisionAccessAreaID)
        {
            return GetAccessAreaRoleUsers(corporateDivisionAccessAreaID, false, DBAccessEnums.RoleType.AccessRecertifier, AccessAreaRecertifierSessionKey);
        }

        private void GetAccessAreas()
        {
            var accessAreas = Director.GetAccessAreasByDA(base.CurrentUser.dbPeopleID, languageID);

            if (accessAreas != null)
            {
                if (base.IsRestrictedUser)
                {
                    accessAreas = accessAreas.Where(e => e.CountryID == RestrictedCountryId).ToList();
                }

                Cache.Insert(this.CacheKey, accessAreas);

                BindGrid(accessAreas);
            }
        }

        private void GetCachedData()
        {
            if (Cache[this.CacheKey] != null)
            {
                List<vw_DistinctDivisionAccessAreasWithApprovers> accessAreas = (List<vw_DistinctDivisionAccessAreasWithApprovers>)Cache[this.CacheKey];
                BindGrid(accessAreas);
            }
        }

        private void BindGrid(List<vw_DistinctDivisionAccessAreasWithApprovers> accessAreas)
        {
            LocationsGrid.DataSource = accessAreas;
            LocationsGrid.DataBind();
            LocationsGrid.FocusedRowIndex = -1;
            LocationsGrid.SettingsText.GroupPanel = SetLanguageText("DragText");
        }

        private void BindClassification()
        {
            var Classes = Director.GetClassificationsEnabled(languageID);

            exClassifications.Items.Clear();
            foreach (var clas in Classes.AsEnumerable().ToList())
            {
                ListEditItem lstItem = new ListEditItem();
                lstItem.Text = clas.Classification + " - " + clas.Description;
                lstItem.Value = clas.HRClassificationID;
                exClassifications.Items.Add(lstItem);
            }

            exClassifications.Items.Insert(0, new ListEditItem(SetLanguageText("ShowAllApprovers"), "-1"));
            exClassifications.Items.Insert(1, new ListEditItem(SetLanguageText("FTEAndExtClass"), "0"));

            exClassifications.SelectedIndex = 0;
        }

        private static List<AccessAreaApprover> MovePersonUpOrDownListAndUpdateSessionList(string approverDBPeopleIDsInOrderBeforeSortWithCommas,
            SortType sortType, int accessAreaRoleUserToMoveDBPeopleID, List<AccessAreaApprover> listAccessAreaRecertifiers,
            int corporateDivisionAccessAreaID, string sessionKey)
        {
            List<int> currentSortOrder = GetListOfIntsFromCommaDelimitedString(approverDBPeopleIDsInOrderBeforeSortWithCommas);

            MovePersonUpOrDownTheList(sortType, currentSortOrder, accessAreaRoleUserToMoveDBPeopleID);

            listAccessAreaRecertifiers = ReorderListAfterMovingAccessAreaRoleUser(currentSortOrder, listAccessAreaRecertifiers);

            SetAccessAreaRoleUsersSessionList(corporateDivisionAccessAreaID, listAccessAreaRecertifiers,
                sessionKey);
            return listAccessAreaRecertifiers;
        }

        private static string GetSessionKeyForAccessArea(int corporateDivisionAccessAreaID, string sessionKey)
        {
            return string.Format(sessionKey, corporateDivisionAccessAreaID);
        }

        private static void MovePersonUpOrDownTheList(SortType sortType, List<int> currentSortOrder,
            int accessAreaRoleUserToMoveDBPeopleID)
        {
            if (sortType != SortType.None)
            {
                if (sortType == SortType.MoveUp && IsFirstInList(currentSortOrder, accessAreaRoleUserToMoveDBPeopleID))
                    ChangeSortOrder(currentSortOrder, accessAreaRoleUserToMoveDBPeopleID, sortType);
                else if (sortType == SortType.MoveDown && IsLastInList(currentSortOrder, accessAreaRoleUserToMoveDBPeopleID))
                    ChangeSortOrder(currentSortOrder, accessAreaRoleUserToMoveDBPeopleID, sortType);
            }
        }

        private static List<int> GetListOfIntsFromCommaDelimitedString(string commaDelimitedString)
        {
            List<int> listOfInts = new List<int>();
            string[] intStringArray = GetStringArrayFromCommaDelimitedString(commaDelimitedString);

            foreach (string currentIntString in intStringArray)
            {
                int currentInt = int.Parse(currentIntString);
                listOfInts.Add(currentInt);
            }
            return listOfInts;
            ;
        }

        private static string[] GetStringArrayFromCommaDelimitedString(string commaDelimitedString)
        {
            return commaDelimitedString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        }

        private static List<AccessAreaApprover> ReorderListAfterMovingAccessAreaRoleUser(List<int> currentSortOrder, List<AccessAreaApprover> listAccessAreaApprovers)
        {
            List<AccessAreaApprover> sortedList = new List<AccessAreaApprover>();

            int sortCounter = 1;

            foreach (int sortedItem in currentSortOrder)
            {
                AccessAreaApprover currentApprover = listAccessAreaApprovers.Find(f => f.dbPeopleID == sortedItem);
                currentApprover.SortOrder = (byte)sortCounter;

                sortedList.Add(currentApprover);

                sortCounter++;
            }
            return sortedList;
        }

        private static bool IsLastInList(List<int> currentSortOrder, int accessAreaRoleUserToMoveDBPeopleID)
        {
            return currentSortOrder.Last() != accessAreaRoleUserToMoveDBPeopleID;
        }

        private static bool IsFirstInList(List<int> currentSortOrder, int accessAreaRoleUserToMoveDBPeopleID)
        {
            return currentSortOrder.First() != accessAreaRoleUserToMoveDBPeopleID;
        }

        private SortType GetSortType(string sortType)
        {
            switch (sortType)
            {
                case "moveup":
                    return SortType.MoveUp;
                case "movedown":
                    return SortType.MoveDown;
                default:
                    return SortType.None;
            }
        }
		
        private static void ChangeSortOrder(List<int> currentSortOrder, int dbpeopleClicked, SortType sortType)
        {
            int changeInIndex = GetChangeInIndexFromSortType(sortType);
            int currentIndex = currentSortOrder.IndexOf(dbpeopleClicked);
            currentSortOrder.RemoveAt(currentIndex);
            currentSortOrder.Insert(currentIndex + changeInIndex, dbpeopleClicked);
        }

        private static int GetChangeInIndexFromSortType(SortType sortType)
        {
            switch (sortType)
            {
                case SortType.MoveUp:
                    return -1;
                case SortType.MoveDown:
                    return 1;
                default:
                    return 0;
            }
        }

        private static void UpdateOutOfOfficeDatesFromDatabase(List<AccessAreaApprover> listSourceDatabaseAccessAreaRoleUser,
                                                                                            List<AccessAreaApprover> listTargetAccessAreaRoleUsers)
        {
            foreach (var sourceAccessAreaRoleUser in listSourceDatabaseAccessAreaRoleUser)
            {
                foreach (var targetAccessAreaRoleUser in listTargetAccessAreaRoleUsers.Where(aaa => aaa.CorporateDivisionAccessAreaApproverId == sourceAccessAreaRoleUser.CorporateDivisionAccessAreaApproverId))
                {
                    targetAccessAreaRoleUser.LeaveDate = sourceAccessAreaRoleUser.LeaveDate;
                    targetAccessAreaRoleUser.ReturnDate = sourceAccessAreaRoleUser.ReturnDate;
                }
            }
        }

		private void HighlightGridRowsOnMouseOver(ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}

		private List<string> GetColumnNamesForExport()
		{
			return new List<string>()
			{
				"DivisionName", 
				"CountryName", 
				"CityName", 
				"BuildingName", 
				"FloorName", 
				"AccessAreaName", 
				"AccessAreaTypeName",
				"Approval", 
				"ApprovalType", 
				"Frequency", 
				"LastCertifiedDate",
				"RoleName", 
				"EmailAddress1",
				"EmailAddress2", 
				"EmailAddress3",
				"EmailAddress4", 
				"EmailAddress5" 
			};
		}

		private List<string> GetColumnHeaders()
		{
			return new List<string>()
			{
				"Division",
				"Country",
				"City",
				"Building",
				"Floor",
				"Access Area",
				"Access Type",
				"Approval",
				"Approval Type",
				"Recert Freq",
				"Last Recert",
				"RoleType",
				"Primary",
				"Secondary",
				"Third",
				"Fourth",
				"Fifth"
			};
		}
        #endregion

        #region public Pages helpers
		public string SetLanguageText(object text)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			
			return resourceManager.GetString(Convert.ToString(text));
		}

        public static int MaximumApprovers
        {
            get { return Int32.Parse(ConfigurationManager.AppSettings["MaximumApprovers"]); }
        }

        public static int MaximumRecertifiers
        {
            get { return Int32.Parse(ConfigurationManager.AppSettings["MaximumRecertifiers"]); }
        }

        public static int MaximumApprovals
        {
            get { return Int32.Parse(ConfigurationManager.AppSettings["MaximumApprovals"]); }
        }
        #endregion
	}
}

