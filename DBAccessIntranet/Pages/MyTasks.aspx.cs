﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Configuration;
using Microsoft.Security.Application;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxGridView;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using DevExpress.Web.ASPxEditors;
using System.Resources;
using Spaarks.Common.UtilityManager;
using Spaarks.CustomMembershipManager;
using System.Threading;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class MyTasks : BaseSecurityPage
	{
		//property makes the list available while processing all the tasks in a single go
		public List<spaarks.DB.CSBC.DBAccess.DBAccessController.DAL.vw_MyTasksDetails> GetTaskInfo { get; set; }

		private int languageID
		{
			get { return Helper.SafeInt(this.CurrentUser.LanguageId); }
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            base.RegisterScriptInclude("MyTasks", "~/scripts/Specific/MyTasks.js");
        }

		protected void Page_Load(object sender, EventArgs e)
		{
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? languageId = Director.GetLanguageID(languageID, currentUserLanguageCode);

            string language = Request.Form["__EventTarget"];
            bool doReloadForLanguageChange = false;

            //if (language != null && language != "")
            if (language == Convert.ToString(DBAccessEnums.Language.English) || language == Convert.ToString(DBAccessEnums.Language.German) || language == Convert.ToString(DBAccessEnums.Language.Italian))
            {
                doReloadForLanguageChange = true;
            }


            if ((!IsCallback && !IsPostBack) || doReloadForLanguageChange)
				LoadDropDown(languageId);

            if (!IsPostBack || doReloadForLanguageChange)
				LoadSearchOption(languageId);

			GetData(Convert.ToInt32(MyTaskStatus.SelectedItem.Value), Convert.ToInt32(MySearchOption.SelectedItem.Value), SearchText.Text);		

			Tasks.CellEditorInitialize +=new ASPxGridViewEditorEventHandler(Tasks_CellEditorInitialize);
			Tasks.RowValidating += new DevExpress.Web.Data.ASPxDataValidationEventHandler(Tasks_RowValidating);
			Tasks.RowUpdating += new DevExpress.Web.Data.ASPxDataUpdatingEventHandler(Tasks_RowUpdating);
		}
		
		void LoadDropDown(int? languageId)
		{
			var TaskStatus = Director.LoadTaskStatus(languageId);
			MyTaskStatus.DataSource = TaskStatus;
			MyTaskStatus.DataBind();

			//set the Pending requests
			MyTaskStatus.SelectedIndex = 0;
        }

		void LoadSearchOption(int? LanguageId)
		{
			var SearchOption = Director.LoadSearchOption(LanguageId);
			MySearchOption.DataSource = SearchOption;
			MySearchOption.DataBind();

			SetLanguageText();
            MySearchOption.SelectedIndex = 0;
		}

		private void SetLanguageText()
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			MySearchOption.Items.Insert(0, new ListEditItem(resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.PleaseSelect)), "0"));
		}

        void Tasks_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            DateTime startDate = DateTime.MinValue;
            DateTime.TryParse(e.NewValues["RequestStartDate"] == null ? string.Empty : e.NewValues["RequestStartDate"].ToString(), out startDate);

            //validation for start date
            DateTime dtFormatToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            //validation to check if the start date is modified
            if (e.NewValues["RequestStartDate"] == null)
            {
                AddError(e.Errors, Tasks.Columns["RequestStartDate"], "Start date cannot be empty.");
            }
            else
            {
                DateTime dtFormatstartdate = new DateTime(startDate.Year, startDate.Month, startDate.Day);

                int ncomp = dtFormatstartdate.CompareTo(dtFormatToday);
                if (ncomp == -1)
                {
                    AddError(e.Errors, Tasks.Columns["RequestStartDate"], "Start date must be greater than or equal to Today.");
                }
            }

            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(e.NewValues["RequestEndDate"] == null ? string.Empty : e.NewValues["RequestEndDate"].ToString(), out endDate);

            if (e.NewValues["RequestEndDate"] == null)
            {
                //AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date cannot be empty.");
            }
            else
            {
                DateTime dtFormatEnddate = new DateTime(endDate.Year, endDate.Month, endDate.Day);
                int ncompend = dtFormatEnddate.CompareTo(dtFormatToday);
                if (ncompend == -1 || ncompend == 0)
                {
                    AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date must be greater than Today.");
                    return;
                }

                //TimeSpan stspan = endDate.Subtract(startDate);
                if (endDate.Year > startDate.Year)
                {
                    int nenddiff = endDate.Month;
                    int nstartDiff = 12 - startDate.Month;
                    if (nstartDiff + nenddiff > 6)
                    {
                        AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date cannot be greater than six months from start.");
                    }
                }
                else
                    if (endDate.Year == startDate.Year)
                    {
                        if (endDate.CompareTo(startDate) == 1)
                        {
                            if (endDate.Month - startDate.Month > 6)
                            {
                                AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date cannot be greater than six months from start.");
                            }
                        }
                        else
                        {
                            AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date must be greater than start Date.");
                        }
                    }
                    else
                        if (endDate.Year < startDate.Year)
                        {
                            AddError(e.Errors, Tasks.Columns["RequestEndDate"], "End date must be greater than Today.");
                        }
            }
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        void Tasks_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            //throw new NotImplementedException();
            int recordId = (int)e.Keys[0];
            String txtComment = (String)e.NewValues["TaskComment"];

            //check for start date to be null
            DateTime? dtStartDate = null;
            if (e.NewValues["RequestStartDate"] != null)
            {
                dtStartDate = (DateTime)e.NewValues["RequestStartDate"];
            }

            //check for end date to be null
            DateTime? dtEndDate = null;
            if (e.NewValues["RequestEndDate"] != null)
            {
                dtEndDate = (DateTime)e.NewValues["RequestEndDate"];
            }


            int i = Director.UpdateTaskInfo(recordId, txtComment, dtEndDate, dtStartDate);
            //successsfully updated the current row
            if (i == 2)
            {
                e.Cancel = true;
                ASPxGridView grid = (ASPxGridView)sender;
                grid.CancelEdit();
                GetData((int)DBAccessEnums.TaskStatus.Pending, Convert.ToInt32(MySearchOption.SelectedItem.Value), SearchText.Text);
            }
        }

        private void GetData(int taskstatusid, int searchOption, string searchValue)
        {
            var dataSource = Director.GetMyTasksDetails(base.CurrentUser.dbPeopleID, taskstatusid, searchOption, searchValue);

            Tasks.DataSource = dataSource;
            Tasks.DataBind();

			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
           
            //pending request
			if (taskstatusid != 1)
			{
				Tasks.Columns[resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.GridViewCommandColumnEdit))].Visible = false;
				Tasks.Columns[resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.GridViewDataTextColumnApprove))].Visible = false;
			}
			else
			{
				Tasks.Columns[resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.GridViewCommandColumnEdit))].Visible = true;
				Tasks.Columns[resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.GridViewDataTextColumnApprove))].Visible = true;
			}

            //ASPxButtonConfirm.ClientVisible = true;
            Tasks.JSProperties["cpRows"] = dataSource.Count;
            Tasks.JSProperties["cptaskstatus"] = taskstatusid;
            Tasks.Settings.ShowColumnHeaders = (0 != dataSource.Count);
        }

        protected void Tasks_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GetData(Convert.ToInt32(MyTaskStatus.SelectedItem.Value), Convert.ToInt32(MySearchOption.SelectedItem.Value), SearchText.Text);
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }

        [WebMethod, ScriptMethod]
        public static string GetMiFareNumber(string MiFareNumber)
        {
            string toReturn = string.Empty;

            int num = Director.GetMiFareNumber(MiFareNumber);

            //  Lets build the string message to display
            if (num > 0)
            {
                HRPerson badgeUser = Director.GetMiFareNumberAssignment(MiFareNumber);

                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
                string message = resourceManager.GetString("AssignedMyFareMessage");

                //  Check for the date and by user
                Dictionary<string, string> badgeRequest = Director.GetMiFareNumberAssignmentDateUser(MiFareNumber);
                string date = "Archived";
                string approvedBy = "Archived";

                if (badgeRequest != null && badgeRequest.Count > 0)
                {
                    approvedBy = badgeRequest.First().Key;
                    date = badgeRequest.First().Value;
                }

                if (badgeUser != null)
                {
                    toReturn = String.Format(message, badgeUser.dbPeopleID, badgeUser.EmailAddress, date, approvedBy);
                }
                else
                {
                    toReturn = String.Format(message, "Archived", "Archived", date, approvedBy);
                }
            }

            return toReturn;
        }

        [WebMethod, ScriptMethod]
        public static string GetTaskDetails(int taskID)
        {
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            string taskDetailXml = Director.GetTaskDetails(taskID);

            if (taskDetailXml == string.Empty) return string.Empty;

            XPathDocument xmlDoc = new XPathDocument(new StringReader(taskDetailXml));

            // See what type of task we have here
            XPathNavigator nav = xmlDoc.CreateNavigator();
            int taskType = Convert.ToInt32(nav.SelectSingleNode("/ROOT/row/TaskTypeId").Value);

            XsltArgumentList argsList = new XsltArgumentList();
            argsList.AddParam("TaskTypeId", "", taskType);

            XmlWriterSettings writerSettings = new XmlWriterSettings
                                                   {
                                                       OmitXmlDeclaration = true,
                                                       ConformanceLevel = ConformanceLevel.Fragment,
                                                       CloseOutput = false
                                                   };

            MemoryStream ms = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(ms, writerSettings);

            XslCompiledTransform transform = new XslCompiledTransform();

            XsltSettings settings = new XsltSettings();
            settings.EnableScript = true;

            string xslPath = ConfigurationManager.AppSettings["TaskDetailsXslPathRoot"].ToString();

            if (CurrentUser.LanguageId == (int)DBAccessEnums.Language.German)
            {
                xslPath = xslPath.Replace(".xsl", "_de.xsl");
            }
            else if (CurrentUser.LanguageId == (int)DBAccessEnums.Language.Italian)
            {
                xslPath = xslPath.Replace(".xsl", "_it.xsl");
            }

            xslPath = HttpContext.Current.Server.MapPath(xslPath);

            transform.Load(xslPath, settings, null);
            transform.Transform(xmlDoc, argsList, writer);

            ms.Position = 0;
            StreamReader r = new StreamReader(ms);
            string taskDetails = r.ReadToEnd().ToString();
                    
            return taskDetails;
        }

     

        public void ASPxHiddenFieldTasks_Callback(object sender, CallbackEventArgsBase e)
        {
            var tasks = (DevExpress.Web.ASPxHiddenField.ASPxHiddenField)sender;

            var director = new Director(base.CurrentUser.dbPeopleID);

            //a bool to record any faiures
            bool anyFailed = false;

            foreach (var task in tasks.AsEnumerable().ToList())
            {
                try
                {
                    string taskKey = task.Key;

                    // there are other things in the collection, so ignore them

                    if (taskKey.StartsWith("task_"))
                    {
                        int? taskId = Helper.SafeIntNullable(taskKey.Remove(0, 5));
                        int taskStatusID = Helper.SafeInt(task.Value);

                        if (taskId.HasValue)
                        {
                            //check if there was a reason or other input
                            string inputKey = "Input_" + taskId.ToString();
                            string comment = (ASPxHiddenFieldInputs.Contains(inputKey)) ? ASPxHiddenFieldInputs.Get(inputKey).ToString() : string.Empty;
                            comment = Encoder.HtmlEncode(comment);

                            int success = 0;

							success = director.UpdateTaskStatus(taskId.Value, (DBAccessEnums.TaskStatus)taskStatusID, comment);
                        }
                    }
                }
                catch (Exception ex)
                {
                    anyFailed = true;
                    //just in case key or value contains invalid data
                    var u = base.CurrentUser;
                    LogHelper.LogException(ExceptionSeverity.Low, u.UserName, u.RolesNamesList, ex);
                }
            }

            ASPxHiddenFieldTasks.Clear();
            ASPxHiddenFieldTasks.Set("Success", !anyFailed);
        }

        /// <summary>
        /// Method to evaluate the accessareaid of the editing row and if there is a default value assigned disable the ability to edit the end date.
        /// E.Parker
        /// 28.09.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Tasks_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            int RequestMasterID = Convert.ToInt32(Tasks.GetRowValuesByKeyValue(e.EditingKeyValue, "RequestMasterID"));
            //get the accessarea based on this. 
            DBAccess.DBAccessController.DAL.AccessArea area = Director.GetAccessAreaDetailsFromRequestMaster(RequestMasterID);
            if (area != null && area.defaultAccessPeriod != null && area.duration != null)
            {
                //now stop the edit of the end date               
                (Tasks.Columns["End Date"] as GridViewDataColumn).ReadOnly = true;
            }
        }
        
		protected void Tasks_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
        {
            if (Tasks.IsNewRowEditing) return;

            if (e.Column.FieldName == "TaskComment")
            {
                e.Editor.Width = Unit.Pixel(190);
                e.Editor.Height = Unit.Pixel(80);
                e.Editor.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Editor.Width = Unit.Pixel(150);
            }
        }

        protected void Tasks_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6'; this.style.cursor = 'hand';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
        }

        public string GetResourceManger(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }

        protected string EscapeJavaScriptSpecialCharacters(string input)
        {
            return spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers.Utility.EscapeJavaScriptSpecialCharacters(input);
        }
    }
}
