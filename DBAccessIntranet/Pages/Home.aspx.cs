﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Web.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class Home : BaseSecurityPage
	{
		protected void Page_PreInit (object sender, EventArgs e)
		{
			try
			{
				// return URL messes with auto redirect based on role type
				// so only redirect to login page if user is not logged in

				String sRedirect = "";

				if (Context.User.Identity.IsAuthenticated)
				{
					// we need to redirect to user's default home page
					// if we have navigated here from the portal

					Boolean bNavigatedFromPortal = !String.IsNullOrEmpty (Request.QueryString["Portal"]);

					if (bNavigatedFromPortal)
					{
						String s = Context.User.Identity.AuthenticationType;

						// this should reflect what is defined in the web.config file for roles

						if (Context.User.IsInRole ("USER")) sRedirect = "~/Pages/MyDBAccess.aspx";
						else if (Context.User.IsInRole ("ADMIN")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("DA")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("RA")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("AA")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("PO")) sRedirect = "~/Pages/ViewAllMyTasks.aspx";
						else if (Context.User.IsInRole ("DO")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("AR")) sRedirect = "~/Pages/MyTasks.aspx";
						else if (Context.User.IsInRole ("MSP")) sRedirect = "~/Pages/MyMSPResources.aspx";
					}
				}
				else
				{
					// we need to force the unauthenticated user to log in to the system

					sRedirect = "~/Pages/Login.aspx";
				}
				if (0 != sRedirect.Length) Response.Redirect (sRedirect, true);
			}
			catch
			{
				// do not do anything
			}
		}



        /// <summary>
        /// We need to do this in a page so that we can access the session object to set the language, as this STATIC it requires it seperate thread context set to culture
        /// </summary>
        /// <param name="resourceName">The resource file name</param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCommonTextTranslation(string resourceName)
        {
            if (HttpContext.Current.Session["Language"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo((string)HttpContext.Current.Session["Language"]);
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture((string)HttpContext.Current.Session["Language"]);
            }

            string textValue = Director.GetResourceManager("Resources.CommonResource", resourceName);

            return textValue;
        }

		protected void Page_Load (object sender, EventArgs e)
		{
            string username = Context.User.Identity.Name.ToString();

            DBSqlMembershipProvider DBSqlMembershipProvider = new DBSqlMembershipProvider();
            DBAppUser User = DBSqlMembershipProvider.GetDBAppUser(username);
            DBAppUser byUser = DBSqlMembershipProvider.GetDBAppUser(User.CreatedByUserId);

            if ((username != null) && (byUser != null))
            {

                StringBuilder userDetails = new StringBuilder();                             

                userDetails.Append("\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeFullName")).
                Append(": ").
                Append(User.FullName).
                Append("\n\n-----------------------------------------------\n\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeEmail") + ": ").
                Append(User.Email).
                Append("\n\n-----------------------------------------------\n\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeEmployeeId") + ": ").
                Append(User.dbPeopleID.ToString()).
                Append("\n\n-----------------------------------------------\n\n").
                //Append("DBDirId: ").
                //Append(User.GDInfo.dbdirid).
                //Append("\n\n-----------------------------------------------\n\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeCostCentre") + ": ").
                Append(User.CostCentre).
                Append("\n\n-----------------------------------------------\n\n").
                //Append("Cost Center Desc: ").
                //Append(User.GDInfo.costcenterdesc).
                // Append("\n\n-----------------------------------------------\n\n").
                //Append("DBLocation(country): ").
                //Append(User.GDInfo.dblocationcountry).
                // Append("\n\n-----------------------------------------------\n\n").
                //Append("DBLocation(street): ").
                //Append(User.GDInfo.dblocationstreet).
                // Append("\n\n-----------------------------------------------\n\n").
                //Append("DBNT Login Id: ").
                //Append(User.GDInfo.dbntloginid).
                // Append("\n\n-----------------------------------------------\n\n").
                //Append("DB Trader Intercom: ").
                //Append(User.GDInfo.dbtraderintercom).
                // Append("\n\n-----------------------------------------------\n\n").
                //Append("Directory Fields:\n\n");
                //foreach (string item in User.GDInfo.DirectoryFields.Values)
                //{
                //    userDetails.Append(item).
                //    Append("\n");
                //}
                //userDetails.Append("\n\n-----------------------------------------------\n\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeCreatedBy") + ": ").
                Append(byUser.FullName).
                Append("\n\n-----------------------------------------------\n\n").
                Append(GetGlobalResourceObject("CommonResource", "HomeCreationDate") + ": ").
                Append(User.CreationDate.ToString("dd/MM/yyyy")).
                Append("\n\n");


                string usData = userDetails.ToString();
                txtUserDetails.Text = usData;
            }
            else
            {
                txtUserDetails.Text = GetGlobalResourceObject("CommonResource", "NouserdetailsfoundHome").ToString();
            }
		}

	}
}
