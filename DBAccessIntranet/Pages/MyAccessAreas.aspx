﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="true" CodeBehind="MyAccessAreas.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyAccessAreas" uiculture="auto" %>

<%@ Import Namespace="spaarks.DB.CSBC.DBAccess.DBAccessController" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="~/Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector"
	TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">
                
	    var isAdmin = '<%= isAdmin %>';

	    function RaiseBasicPopupAlert(contentText, headerText) {
	        popupAlertTemplateContent.SetText(contentText);
	        popupAlertTemplate.SetHeaderText(headerText);
	        popupAlertTemplate.Show();
	    }
        
		var popupOutOfOfficeURL;
		$(document).ready(function () 
		{
			popupOutOfOfficeURL = '<%= Page.ResolveUrl("~/Pages/Popup/OutOfOfficePopup.aspx") %>';

			var values = new Array();
			hfAccessRequestArrayAA.Set("AccessRequestArrayAA", values);
			hfAccessRequestArrayAR.Set("AccessRequestArrayAR", values);

		});

	    function handleupclick(elementId) {
	        setDirty(true);
	        var outputOrder = "";
            
	        $('[class*=" Approvers_OrderingClass"]').each(function (index) {
	            outputOrder += "," + $(this).find("div.dbpeopleId").text();
	        });

	        var classificationId = classifications.GetValue();	        

	        Approvers.PerformCallback(true + '|' + selectedAreaId + '|' + classificationId + '|' + 'moveup' + '|' + outputOrder + '|' + elementId);	        
	    }

	    function handledownclick(elementId) {
	        setDirty(true);
	        var outputOrder = "";

	        $('[class*=" Approvers_OrderingClass"]').each(function (index) {    
	            outputOrder += "," + $(this).find("div.dbpeopleId").text();
	        });

	        var classificationId = classifications.GetValue();

	        Approvers.PerformCallback(true + '|' + selectedAreaId + '|' + classificationId + '|' + 'movedown' + '|' + outputOrder + '|' + elementId);
	    }

	    function handleupclickrecertifier(elementId) {
	        setDirty(true);
	        var outputOrder = "";

	        $('[class*=" Recertifier_OrderingClass"]').each(function (index) {
	            outputOrder += "," + $(this).find("div.dbpeopleId").text();
	        });

	        Recertifiers.PerformCallback(true + '|' + selectedAreaId + '|' + 'moveup' + '|' + outputOrder + '|' + elementId);
	    }

	    function handledownclickrecertifier(elementId) {
	        setDirty(true);
	        var outputOrder = "";

	        $('[class*=" Recertifier_OrderingClass"]').each(function (index) {
	            outputOrder += "," + $(this).find("div.dbpeopleId").text();
	        });

	        Recertifiers.PerformCallback(true + '|' + selectedAreaId + '|' + 'movedown' + '|' + outputOrder + '|' + elementId);
	    }

		$(function () {
	        isDirty = false;
	       
			$("#tabs").tabs({
				select: function (event, ui) {
			        switchTabs(event, ui);
				}
			});

			$('#tabs input').change(function() { 
			    handleChangeOnTab();
			});

			$('#externals').hide();
			$('#externalBut').hide();
		});

	    function switchTabs(event, ui) {
	        if (isDirty) {
	            event.preventDefault();//Prevent the tab from switching
	            popupAlertTemplateContent.SetText('<%= SetLanguageText("SaveChangesWarning") %>');
	            popupAlertTemplate.SetHeaderText('<%= SetLanguageText("Error") %>');//Please save or cancel your changes before proceeding
	            popupAlertTemplate.Show();
	        } 
	    }

	    function handleChangeOnTab() {
	        isDirty = true;
	    }

	    var isDirty;

	    function setDirty(dirty) {
	        isDirty = dirty;
	    };

		var selectedAreaId;
		var selectedRoleTypeId;

		function LocationsGridSelectedIndexChanged(args) {
			if (-1 != args.visibleIndex) {
			    LocationsGrid.GetRowValues(args.visibleIndex, 'CorporateDivisionAccessAreaID;DivisionName;CountryName;CityName;BuildingName;FloorName;AccessAreaName;AccessAreaEnabled;NumberOfApprovals;FrequencyValue;ApprovalTypeID', OnGetAccessAreaRowValue);
			}
		}

		function OnGetAccessAreaRowValue(value) {
		    
			if (value[0] != null) {
				selectedAreaId = parseInt(value[0]);

			    PageMethods.RemoveDraftRequestsForThisAccessArea(selectedAreaId);

				var headerText = value[1] + ' - ' + value[2] + ' - ' + value[3] + ' - ' + value[4];
				if (value[5])
					headerText += ' - ' + value[5]

				if (value[6])
					headerText += ' - ' + value[6]

				$("#SectionTwoHeader").text('<%= SetLanguageText("AccessArea") %>' + ': ' + headerText);
				$("#labelAADetailHeader").text(headerText);

				var classificationId = classifications.GetValue();

				Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
				Recertifiers.PerformCallback(false + '|' + selectedAreaId);

				PopulateAreaDetails(value[6], value[7], value[8], value[9], value[10]);

				ShowAccessAreaDetail();
				checkPermission();
			}
		}

		function PopulateAreaDetails(sName, bEnabled, iApproval, iFrequency, iApprovalType) {
			var ctl_tbName = ASPxClientTextBox.Cast("txtAccessAreaName");
			var ctl_rbEnabled = ASPxClientRadioButtonList.Cast("rbEnabled");
			var ctl_rbApprovalType = ASPxClientRadioButtonList.Cast("rbApprovalType");
			var ctl_cbRecertFrequency = ASPxClientComboBox.Cast("cbRecertFrequency");

			if (iFrequency != 1 &&
				iFrequency != 3 &&
				iFrequency != 6 &&
				iFrequency != 12) {
				iFrequency = 6;
			}

			ctl_tbName.SetText(sName);
			ctl_rbEnabled.SetValue(bEnabled ? 1 : 0);
			ctl_rbApprovalType.SetValue(iApprovalType);
			$("#txtNumApprovals").val(iApproval);
			ctl_cbRecertFrequency.SetValue(iFrequency);
		}

		// ----------------------------------------------------------------------
		// ----------------------------------------------------------------------

		var g_popup_personId = 0;
		var g_popup_classificationId = 0;

		// ----------------------------------------------------------------------
		// ----------------------------------------------------------------------

		function ShowAddPersonAA() {
			popupAddApprover.SetHeaderText('<%= SetLanguageText("AccessAreaApprover") %>');
			$("#popupHeaderAA").text('<%= SetLanguageText("AddAccessAreaApprover") %>');

			$("#PersonSelectorNameTextBox").val('');
			$("#SortOrderTextBoxAA").val(Approvers.cpMaxOrder+1);

			popupAddApprover.Show();
		}

		// ----------------------------------------------------------------------

	    function AddPersonAA(s, e) {
			var idstring = $get("PersonSelectorNameTextBox1").value;

			var personId = 0;

			if (idstring.length > 0) {
				personId = GetPersonIdFromIdentifier(idstring);
			}

			if (personId == 1) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("InvalidEmail") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddPerson") %>');
				popupAlertTemplate.Show();
				return;
			}

			if (personId == 0) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("SelectValidPerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddPerson") %>');
				popupAlertTemplate.Show();
				return;
			}

			var classificationId = classifications.GetValue();
			var classificationName = classifications.GetText();

			var iSortOrder = $("#SortOrderTextBoxAA").val();
			var iRequestMasterId = hfRequestDetail.Get("RequestMasterID_AA");
			if (iRequestMasterId == null) iRequestMasterId = 0;

			PageMethods.AddPersonAA(personId, iRequestMasterId, iSortOrder, selectedAreaId, classificationId, classificationName, OnAddPersonAA);
		}

		function OnAddPersonAA(result)
		{
			var substr = result.split('|');
			if (substr[0].toString() == 0) 
			{
				popupAlertTemplateContent.SetText('<%= SetLanguageText("RequestCouldNotBeCreated") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddApprover") %>');
				popupAlertTemplate.Show();
				return;
			}

			if (substr[0].toString() == -3)
			{
				popupAlertTemplateContent.SetText('<%= SetLanguageText("CannotAddInactivePerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddApprover") %>');
				popupAlertTemplate.Show();
				return;
			}
			hfRequestDetail.Set("RequestMasterID_AA", substr[0].toString());
			HiddenArrayDataAA(result);

			$get("PersonSelectorNameTextBox1").value = '';
			$('#SortOrderTextBoxAA').val(Approvers.cpMaxOrder+1);

			LocationsGrid.PerformCallback();

			var classificationId = classifications.GetValue();
			Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
		}

		function HiddenArrayDataAA(result)
		{
			var substr = result.split('|');
			var values = hfAccessRequestArrayAA.Get("AccessRequestArrayAA");
			if (values.length == 0)
			{
				values[0] = substr[1] + "-" + substr[2];
			}
			else
			{
				var index = values.length;
				values[index] = substr[1] + "-" + substr[2];
			}

			hfAccessRequestArrayAA.Set("AccessRequestArrayAA", values);
		}

		// ----------------------------------------------------------------------

		function DeletePersonAA(personId, classificationId) {

		    if (isDirty) {
		        RaiseBasicPopupAlert('<%= SetLanguageText("SaveChangesWarning") %>', '<%= SetLanguageText("Error") %>');
		        return;
		    } 

		    if (null == classificationId) classificationId = 0;
			var classValue = classifications.GetValue();
			var numOfApprovalRows = $("div[class*='approverRowNumber']").length;
            
			if(numOfApprovalRows <= <%= minimumNumberOfAccessApprovers %> && (classValue == -1 || classValue == 0))
			{
			    RaiseBasicPopupAlert('<%= SetLanguageText("AALessThanTwoApprovers") %>', '<%= SetLanguageText("ConfirmChanges") %>');
		        return;
		    }

			g_popup_personId = personId;
			g_popup_classificationId = classificationId;

			popupConfirmTemplate.SetHeaderText('<%= SetLanguageText("RemovePersonHeader") %>');
			popupConfirmTemplateContentHead.SetText('<%= SetLanguageText("Person") %>');
			popupConfirmTemplateContentBody.SetText('<%= SetLanguageText("RemovePersonContent") %>');
			popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemovePersonAA();");
			popupConfirmTemplate.Show();
		}

		function OnRemovePersonAA() 
		{
			var personId = g_popup_personId;
			var classificationId = g_popup_classificationId;

			CheckAndSetAccessRequestIdAA(personId);
			var accessRequestIDAA = hfAccessRequestIdAA.Get("AccessRequestIdAA");
			if (accessRequestIDAA == undefined) accessRequestIDAA = null;

			PageMethods.DeletePersonAA(accessRequestIDAA, personId, selectedAreaId, classificationId, OnDeletePersonAA);
		}

		function CheckAndSetAccessRequestIdAA(personId)
		{
			var bval = false;
			var values = hfAccessRequestArrayAA.Get("AccessRequestArrayAA");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == personId.toString())
				{
					bval = true;
					break;
				}
			}

			if (bval)
			{
				hfAccessRequestIdAA.Set("AccessRequestIdAA", rval[0].toString());
			}
			else
			{
				hfAccessRequestIdAA.Set("AccessRequestIdAA", undefined);
			}
		}

		function OnDeletePersonAA(result) {
			if (!result) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("ProblemDeletingPerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("DeletePerson") %>');
				popupAlertTemplate.Show();
				return;
			}
			LocationsGrid.PerformCallback();

			var classificationId = classifications.GetValue();
			Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
			
			HiddenArrayDataUpdateAA(g_popup_personId);
		}

		function HiddenArrayDataUpdateAA(g_popup_personId)
		{
			var values = hfAccessRequestArrayAA.Get("AccessRequestArrayAA");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == g_popup_personId.toString())
				{
					values.splice(i, 1);
					hfAccessRequestArrayAA.Set("AccessRequestArrayAA", values);
					break;
				}
			}
		}

		// ----------------------------------------------------------------------

		function ConfirmChangesAA() {
		    popupLoading.Show();
		    var iRequestMasterId = hfRequestDetail.Get("RequestMasterID_AA");
		    if (null == iRequestMasterId) iRequestMasterId = 0;

		    var classValue = classifications.GetValue();
		    var numOfApprovalRows = $("div[class*='approverRowNumber']").length;
            
		    if(numOfApprovalRows < <%= minimumNumberOfAccessApprovers %> && (classValue == -1 || classValue == 0))
			{
			    RaiseBasicPopupAlert('<%= SetLanguageText("AALessThanTwoApprovers") %>', '<%= SetLanguageText("ConfirmChanges") %>');
			    return;
			}

			PageMethods.ConfirmChangesAA(selectedAreaId, iRequestMasterId, OnConfirmChangesAA);
		}

	    function OnConfirmChangesAA(result) {
			popupLoading.Hide();

			if (!result) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("ProblemConfirmChanges") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
				popupAlertTemplate.Show();
				return;
			}
			hfRequestDetail.Set("RequestMasterID_AA", 0);

			popupAlertTemplateContent.SetText('<%= SetLanguageText("AAUpdatedSuccessfully") %>');
			popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
			popupAlertTemplate.Show();

			// we need to reload the grid

			var classificationId = classifications.GetValue();

			Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
		}

	    function ConfirmCancellingChangedAA() {
	        popupAlertTemplateContent.SetText('<%= SetLanguageText("AAUpdateCancelled") %>');
	        popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
	        popupAlertTemplate.Show();

	        var classificationId = classifications.GetValue();
	        Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
	    }

		// ----------------------------------------------------------------------
		// ----------------------------------------------------------------------

		function ShowAddPersonAR() {
			popupAddRecertifier.SetHeaderText('<%= SetLanguageText("AccessAreaRecertifier") %>');
			$("#popupHeaderAR").text('<%= SetLanguageText("AddAccessAreaRecertifier") %>');

			$("#PersonSelectorNameTextBox").val('');
			$("#SortOrderTextBoxAR").val(Recertifiers.cpMaxOrderAR+1);

			popupAddRecertifier.Show();
		}

		// ----------------------------------------------------------------------

		function AddPersonAR(s, e) {
			var idstring = $get("PersonSelectorNameTextBox2").value;

			var personId = 0;

			if (idstring.length > 0) {
				personId = GetPersonIdFromIdentifier(idstring);
			}

			if (personId == 1) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("InvalidEmail") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddPerson") %>');
				popupAlertTemplate.Show();
				return;
			}

			if (personId == 0) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("SelectValidPerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddPerson") %>');
				popupAlertTemplate.Show();
				return;
			}

		    var iSortOrder = $("#SortOrderTextBoxAR").val();
			var iRequestMasterId = hfRequestDetail.Get("RequestMasterID_AR");
			if (iRequestMasterId == null) iRequestMasterId = 0;

			PageMethods.AddPersonAR(personId, iRequestMasterId, iSortOrder, selectedAreaId, OnAddPersonAR);
		}

		function OnAddPersonAR(result)
		{
			var substr = result.split('|');
			if (substr[0].toString() == 0)
			{
				popupAlertTemplateContent.SetText('<%= SetLanguageText("RequestCouldNotBeCreated") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddRecertifier") %>');
				popupAlertTemplate.Show();
				return;
			}

			if (substr[0].toString() == -3)
			{
				popupAlertTemplateContent.SetText('<%= SetLanguageText("CannotAddInactivePerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("AddRecertifier") %>');
				popupAlertTemplate.Show();
				return;
			}


			hfRequestDetail.Set("RequestMasterID_AR", substr[0].toString());
			HiddenArrayDataAR(result);

			$get("PersonSelectorNameTextBox2").value = '';
			$('#SortOrderTextBoxAR').val(Recertifiers.cpMaxOrderAR+1);

			LocationsGrid.PerformCallback();

			Recertifiers.PerformCallback(false + '|' + selectedAreaId);
		}

		function HiddenArrayDataAR(result)
		{
			var substr = result.split('|');
			var values = hfAccessRequestArrayAR.Get("AccessRequestArrayAR");
			if (values.length == 0)
			{
				values[0] = substr[1] + "-" + substr[2];
			}
			else
			{
				var index = values.length;
				values[index] = substr[1] + "-" + substr[2];
			}

			hfAccessRequestArrayAR.Set("AccessRequestArrayAR", values);
		}

		// ----------------------------------------------------------------------

		function DeletePersonAR(personId) {
		    if (isDirty) {
		        RaiseBasicPopupAlert('<%= SetLanguageText("SaveChangesWarning") %>', '<%= SetLanguageText("Error") %>');
            	return;
            } 
            
			g_popup_personId = personId;

			popupConfirmTemplate.SetHeaderText('<%= SetLanguageText("RemovePersonHeader") %>');
			popupConfirmTemplateContentHead.SetText('<%= SetLanguageText("Person") %>');
			popupConfirmTemplateContentBody.SetText('<%= SetLanguageText("RemovePersonContent") %>');
			
			popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemovePersonAR();");
			popupConfirmTemplate.Show();
		}

		function OnRemovePersonAR() {
			var personId = g_popup_personId;

			CheckAndSetAccessRequestIdAR(personId);
			var accessRequestIDAR = hfAccessRequestIdAR.Get("AccessRequestIdAR");
			if (accessRequestIDAR == undefined) accessRequestIDAR = null;

			PageMethods.DeletePersonAR(accessRequestIDAR, personId, selectedAreaId, OnDeletePersonAR);
		}

		function OnDeletePersonAR(result) {
			if (!result) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("ProblemDeletingPerson") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("DeletePerson") %>');
				popupAlertTemplate.Show();
				return;
			}
			LocationsGrid.PerformCallback();

			Recertifiers.PerformCallback(false + '|' + selectedAreaId);
			HiddenArrayDataUpdateAR(g_popup_personId);
		}

		function CheckAndSetAccessRequestIdAR(personId)
		{
			var bval = false;
			var values = hfAccessRequestArrayAR.Get("AccessRequestArrayAR");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == personId.toString())
				{
					bval = true;
					break;
				}
			}

			if (bval)
			{
				hfAccessRequestIdAR.Set("AccessRequestIdAR", rval[0].toString());
			}
			else
			{
				hfAccessRequestIdAR.Set("AccessRequestIdAR", undefined);
			}
		}

		function HiddenArrayDataUpdateAR(g_popup_personId)
		{
			var values = hfAccessRequestArrayAR.Get("AccessRequestArrayAR");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == g_popup_personId.toString())
				{
					values.splice(i, 1);
					hfAccessRequestArrayAR.Set("AccessRequestArrayAR", values);
					break;
				}
			}
		}

		// ----------------------------------------------------------------------

		function ConfirmChangesAR() {
			popupLoading.Show();
			var iRequestMasterId = hfRequestDetail.Get("RequestMasterID_AR");
			if (null == iRequestMasterId) iRequestMasterId = 0;

			PageMethods.ConfirmChangesAR(selectedAreaId, iRequestMasterId, OnConfirmChangesAR);
		}

		function OnConfirmChangesAR(result) {
			popupLoading.Hide();
			if (!result) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("ProblemConfirmChanges") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
				popupAlertTemplate.Show();
				return;
			}
			hfRequestDetail.Set("RequestMasterID_AR", 0);

			popupAlertTemplateContent.SetText('<%= SetLanguageText("ARUpdatedSuccessfully") %>');
			popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
			popupAlertTemplate.Show();

			// we need to reload the grid
			Recertifiers.PerformCallback(false + '|' + selectedAreaId);

			//HideAccessAreaDetail();
		}

	    function ConfirmCancellingChangedAR() {
	        popupAlertTemplateContent.SetText('<%= SetLanguageText("ARUpdateCancelled") %>');
	        popupAlertTemplate.SetHeaderText('<%= SetLanguageText("ConfirmChanges") %>');
	        popupAlertTemplate.Show();

	        var classificationId = classifications.GetValue();
	        Recertifiers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
	    }


		// ----------------------------------------------------------------------

	    function ValidateApprovals() {
	        var txtNumApprovals = $("#txtNumApprovals");
	        var iApprovals = txtNumApprovals.val();
	        if(iApprovals == '' || iApprovals > <%= MaximumApprovals %>) {
                txtNumApprovals.addClass('ui-state-error');
	            return false;
	        } else {
                txtNumApprovals.removeClass("ui-state-error");
                return true;
            }
	    }

		function UpdateArea() {
			var ctl_tbName = ASPxClientTextBox.Cast("txtAccessAreaName");
			var ctl_rbEnabled = ASPxClientRadioButtonList.Cast("rbEnabled");
			var ctl_rbApprovalType = ASPxClientRadioButtonList.Cast("rbApprovalType");
			var ctl_cbRecertFrequency = ASPxClientComboBox.Cast("cbRecertFrequency");

			var sName = ctl_tbName.GetText();
			var bEnabled = (1 == ctl_rbEnabled.GetValue());
			var iApprovalType = ctl_rbApprovalType.GetValue();
			var iApprovals = $("#txtNumApprovals").val();
			var iFrequency = ctl_cbRecertFrequency.GetValue();

			var valid = true;
			valid &= ValidateApprovals();

            if(valid) {
			    PageMethods.UpdateArea(selectedAreaId, iFrequency, iApprovalType, iApprovals, bEnabled, sName, OnUpdateArea);
            }

			//HideAccessAreaDetail();
		}

		function OnUpdateArea(result) {
			if (!result) {
				popupAlertTemplateContent.SetText('<%= SetLanguageText("AccessAreaCouldntUpdate") %>');
				popupAlertTemplate.SetHeaderText('<%= SetLanguageText("UpdateArea") %>');
				popupAlertTemplate.Show();
				return;
			}

			popupAlertTemplateContent.SetText('<%= SetLanguageText("AccessAreaUpdated") %>');
			popupAlertTemplate.SetHeaderText('<%= SetLanguageText("UpdateArea") %>');
			popupAlertTemplate.Show();

			var classificationId = classifications.GetValue();

			switch (selectedRoleTypeId) {
				case 5:
					{
						Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
						break;
					}
				case 8:
					{
						Recertifiers.PerformCallback(false + '|' + selectedAreaId);
						break;
					}
			}
			LocationsGrid.PerformCallback();
		}

		function SortApprover(iDirection, iKeyValue) {
			var classificationId = classifications.GetValue();
			Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId + '|' + iDirection + '|' + iKeyValue);
		}

		function SortRecertifier(iDirection, iKeyValue) {
			Recertifiers.PerformCallback(false + '|' + selectedAreaId + '|' + iDirection + '|' + iKeyValue);
		}

		function ClassificationChanged(classificationId) {
			var classificationId = classifications.GetValue();

			Approvers.PerformCallback(false + '|' + selectedAreaId + '|' + classificationId);
		}

		// ----------------------------------------------------------------------

		function ShowOutOfOfficePopup(userID) {
		    setDirty(true);
			popupUrlOutOfOffice = popupOutOfOfficeURL + "?dbPeopleID=" + userID;
			popupOutOfOffice.SetContentUrl(popupUrlOutOfOffice);
			popupOutOfOffice.Show();
		}

		function CloseOutOfOfficePopup() {
			popupOutOfOffice.Hide();

			var classificationId = classifications.GetValue();

			// we don't know where this was called from, so refresh both

			Approvers.PerformCallback(true + '|' + selectedAreaId + '|' + classificationId);
			Recertifiers.PerformCallback(true + '|' + selectedAreaId);
		}

		// ----------------------------------------------------------------------

        // Limit user input on the sort order and approvals required textboxes
		$(document).ready(function () {
		    $('#SortOrderTextBoxAR').keyup(function () {
		        this.value = this.value.replace(/^[^1-9]/, '').replace(/[^0-9]/g, '').substring(0, <%= MaximumRecertifiers.ToString().Length %>);
		    });

		    $('#SortOrderTextBoxAA').keyup(function () {
		        this.value = this.value.replace(/^[^1-9]/, '').replace(/[^0-9]/g, '').substring(0, <%= MaximumApprovers.ToString().Length %>);
		    });

		    $('#txtNumApprovals').keyup(function () {
		        this.value = this.value.replace(/^[^1-9]/, '').replace(/[^0-9]/g, '').substring(0, <%= MaximumApprovals.ToString().Length %>);
		        ValidateApprovals();
		        handleChangeOnTab();
		    });
		});

		function ShowAccessAreaDetail() {
			$('#AccessAreaContent').css('display', '');
			$('#AccessAreasContent').css('display', 'none');

			$('#AccessAreasHeader').css('display', 'none');
			$('#AccessAreaDetailHeader').css('display', '');
			
			$("#tabs").tabs("select", 0);
		}

		function CancelAccessAreaRoleUserChanges(isAccessApproverTab) {
		    RemoveDraftRequests();
		    
		    if (isAccessApproverTab) {
		        var classificationId = classifications.GetValue();
		        Approvers.PerformCallback(true + '|' + selectedAreaId + '|' + classificationId);
		    } else {
		        Recertifiers.PerformCallback(true + '|' + selectedAreaId);
		    }

            setDirty(false);
		}

		function RemoveDraftRequests() {
		    //Need to clear request master ID or subsequent requests will fail
		    hfRequestDetail.Set("RequestMasterID_AA", 0);
		    hfRequestDetail.Set("RequestMasterID_AR", 0);

		    PageMethods.RemoveDraftRequestsForThisAccessArea(selectedAreaId);
		}

		function HideAccessAreaDetail() {
		    RemoveDraftRequests();

		    setDirty(false);

			$('#AccessAreaContent').css('display', 'none');
			$('#AccessAreasContent').css('display', '');

			$('#AccessAreasHeader').css('display', '');
			$('#AccessAreaDetailHeader').css('display', 'none');

			//Need to call this or event handlers are lost on the grid
			//Particularly if you click on a row no event is fired.
			LocationsGrid.PerformCallback();
		}

		function checkPermission() {
		    if (isAdmin == 'True')
		        $('#ctl00_DBMainContent_ASPxTextBoxAccessAreaName_I').removeAttr("disabled");
		    else
		        $('#ctl00_DBMainContent_ASPxTextBoxAccessAreaName_I').attr("disabled", "true");
		}

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<div>
		<h1 id="AccessAreasHeader"><asp:Literal ID="MyAccessAreas1" runat="server" meta:resourcekey="MyAccessAreasHeader"></asp:Literal></h1>
		<h1 style="display: none;" id="AccessAreaDetailHeader">
		<asp:Literal ID="MyAccessAreas2" runat="server" meta:resourcekey="MyAccessAreasHeader"></asp:Literal>			
			<dx:ASPxLabel runat="server" ID="LabelAccessAreaDetailHeader" ClientInstanceName="labelAADetailHeader"
				Style="font-size: 22px;" />
		</h1>
	</div>
	<div id="footerLower">
	</div>
	<br />
	<div id="AccessAreasContent" class='Content'>
		<p>
			<asp:Literal ID="MyAccessArea3" runat="server" meta:resourcekey="MyAccessAreaDescription"></asp:Literal>
		</p>
		<asp:Panel ID="AccessAreaPanel" runat="server">
			<p> <asp:Literal ID="ExportToExcel" runat="server" meta:resourceKey="ExportToExcel"></asp:Literal>
				<asp:ImageButton ID="ASPxButton2" runat="server"
					OnClick="btnXlsxExport_Click" SkinID="FileXLS"/></p>
			<br />
			<dx:ASPxGridView ID="LocationsGrid" ClientInstanceName="LocationsGrid" runat="server"
				AutoGenerateColumns="False" KeyFieldName="CorporateDivisionAccessAreaID" OnCustomCallback="LocationsGridCustomCallback"
				OnHtmlRowCreated="LocationsGrid_HtmlRowCreated" ClientIDMode="AutoID">
				<ClientSideEvents SelectionChanged="function(s,e){ LocationsGridSelectedIndexChanged(e); }" />
				<Columns>
					<dx:GridViewDataTextColumn Caption="" FieldName="DivisionName" ShowInCustomizationForm="True"
						VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnDivision">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="CountryName" ShowInCustomizationForm="True"
						VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnCountry">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="CityName" ShowInCustomizationForm="True"
						VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnCity">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="BuildingName" ShowInCustomizationForm="True"
						VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnBuilding">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="FloorName" ShowInCustomizationForm="True"
						VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnFloor">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="AccessAreaName" ShowInCustomizationForm="True"
						VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnAccessArea">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="AccessAreaTypeName" ShowInCustomizationForm="True"
						VisibleIndex="7" meta:resourcekey="GridViewDataTextAccessType">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="Approval" ShowInCustomizationForm="True"
						VisibleIndex="8" meta:resourcekey="GridViewDataTextColumnApproval">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="ApprovalType" ShowInCustomizationForm="True"
						VisibleIndex="9" meta:resourcekey="GridViewDataTextColumnApprovalType">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataTextColumn Caption="" FieldName="Frequency" ShowInCustomizationForm="True"
						VisibleIndex="10" meta:resourcekey="GridViewDataTextColumnRecertFrequency">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn>
					<dx:GridViewDataDateColumn Caption="" FieldName="LastCertifiedDate" 
						ShowInCustomizationForm="True" VisibleIndex="11" 
						meta:resourcekey="GridViewDataDateColumnLastRecert">
						<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy">
						</PropertiesDateEdit>
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataDateColumn>
					<dx:GridViewDataTextColumn FieldName="AccessAreaEnabled" Visible="False" 
						meta:resourcekey="GridViewDataTextColumnAccessAreaEnabled" 
						ShowInCustomizationForm="True">
						<Settings AllowHeaderFilter="True" />
					</dx:GridViewDataTextColumn> 
                   <dx:GridViewDataTextColumn FieldName="FrequencyValue" Visible="False" 
						ShowInCustomizationForm="True">
					</dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="NumberOfApprovals" Visible="False" 
						ShowInCustomizationForm="True">
					</dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ApprovalTypeID" Visible="False" 
						ShowInCustomizationForm="True">
				</dx:GridViewDataTextColumn>
				</Columns>
				<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" 
					AllowSelectSingleRowOnly="True" />
				<SettingsPager PageSize="25" />
				<Settings ShowStatusBar="Visible" ShowGroupPanel="True" />
				<SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRowAccessAreas %>"/>
				<GroupSummary></GroupSummary>
			</dx:ASPxGridView>
			<dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="LocationsGrid">
			</dx:ASPxGridViewExporter>
		</asp:Panel>
	</div>
	<div id="AccessAreaContent" class='Content' style="display: none;">
		<div id="AccessAreaHeader">
			<h3 style="margin-bottom: 0px;">
				<b>
				<asp:Literal ID="AccessAreaDetails" runat="server" meta:resourceKey="AccessAreaDetails"></asp:Literal>
				</b>
			</h3>
                <div id="BackToListEnclosure" style="width: 1160px; margin-bottom: 0px;">
							    <div id="BackToList" style="float: right; padding-bottom: 10px;">
								    <dx:ASPxButton ForeColor="#0098DB" ID="btnBackToList" ClientInstanceName="buttonCancelAccessAreaGeneral"
									    AutoPostBack="False" runat="server" Text="" 
									    meta:resourcekey="btnBackToList">
									    <ClientSideEvents Click="function(s,e){ HideAccessAreaDetail(); }" />
								    </dx:ASPxButton>
							    </div>		
                </div>
		</div>
		<div id="accessareadetails">
			<div id="tabs" class='Content' style="width: 1160px;">
				<div>
					<div class="shaded">
						<h3>
							<span id="SectionTwoHeader"></span>
						</h3>
					</div>
					<ul>
						<li><a href="#tabs1" id="a1">
						<asp:Literal ID="General" runat="server" meta:resourceKey="General"></asp:Literal></a></li>
						<li><a href="#tabs-2" id="a2">
						<asp:Literal ID="AccessApprovers" runat="server" meta:resourceKey="AccessApprovers"></asp:Literal>
						</a></li>
						<li><a href="#tabs-3" id="a3">
						<asp:Literal ID="AccessRecertifiers" runat="server" meta:resourceKey="AccessRecertifiers"></asp:Literal>
						</a></li>
					</ul>
					<div id="tabs1">
						<asp:Table runat="server" ID="LocationTable" SkinID="FormTable">
							<asp:TableRow ID="TableRow0" runat="server">
								<asp:TableCell ID="TableCell0" runat="server" SkinID="FormTableLabel" 
									Style="width: 200px;">
									<asp:Literal ID="Name" runat="server" meta:resourceKey="Name"></asp:Literal>&nbsp;
								</asp:TableCell>
								<asp:TableCell ID="TableCell1" runat="server" SkinID="FormTableItem" 
									Style="width: 450px;">
									<dx:ASPxTextBox ID="ASPxTextBoxAccessAreaName" ClientInstanceName="txtAccessAreaName"
										runat="server" Width="300px">
										
<ValidationSettings RequiredField-IsRequired="true" ValidateOnLeave="true">
										
<RequiredField IsRequired="True"></RequiredField>
</ValidationSettings>
									
</dx:ASPxTextBox>
								
</asp:TableCell>
								<asp:TableCell ID="TableCell4" SkinID="FormTableHelp" runat="server"> 
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="TableRow1" runat="server">
								<asp:TableCell ID="TableCell2" runat="server" SkinID="FormTableLabel" 
									Style="vertical-align: middle;"> 
									<asp:Literal ID="Enabled" runat="server" meta:resourceKey="Enabled"></asp:Literal></asp:TableCell>
								<asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableItem" 
									Style="width: 450px;">
									<dx:ASPxRadioButtonList Paddings-PaddingLeft="0px" ID="rbEnabled" ClientEnabled="true"
										ClientInstanceName="rbEnabled" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
										RepeatLayout="Flow">
										<clientsideevents selectedindexchanged="handleChangeOnTab" />
<Paddings PaddingLeft="0px"></Paddings>
<Items>
											
<dx:ListEditItem Text="" Value="1" meta:resourcekey="Yes" />
											
<dx:ListEditItem Text="" Value="0" meta:resourcekey="No" />
										
</Items>
									
</dx:ASPxRadioButtonList>
								
</asp:TableCell>
								<asp:TableCell ID="TableCell5" SkinID="FormTableHelp" runat="server"> 
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="TableRow2" runat="server">
								<asp:TableCell ID="TableCell6" runat="server" SkinID="FormTableLabel" 
									Style="vertical-align: middle;">
									<asp:Literal ID="ApprovalType" runat="server" meta:resourceKey="ApprovalType"></asp:Literal>
									</asp:TableCell>
								<asp:TableCell ID="TableCell7" runat="server" SkinID="FormTableItem" 
									Style="width: 450px;">
									<dx:ASPxRadioButtonList Paddings-PaddingLeft="0px" ID="rbApprovalType" ClientEnabled="true"
										ClientInstanceName="rbApprovalType" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
										RepeatLayout="Flow" RepeatColumns="2"
                                        >
										<clientsideevents selectedindexchanged="handleChangeOnTab" />
<Paddings PaddingLeft="0px"></Paddings>
<Items>
											
<dx:ListEditItem Text="" Value="1" 
		meta:resourcekey="SequentialEsculation" />
											
<dx:ListEditItem Text="" Value="2" 
		meta:resourcekey="ConcurrentNotification" />
										
</Items>
									
</dx:ASPxRadioButtonList>
								
</asp:TableCell>
								<asp:TableCell ID="TableCell8" SkinID="FormTableHelp" runat="server"> 
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="TableRow3" runat="server">
								<asp:TableCell ID="TableCell9" runat="server" SkinID="FormTableLabel" Style="width: 200px;
									vertical-align: middle;">
									<asp:Literal ID="ApprovalsRequired" runat="server" meta:resourceKey="ApprovalsRequired"></asp:Literal> (1-<%= MaximumApprovals %>)
									</asp:TableCell>
								<asp:TableCell ID="TableCell10" runat="server" SkinID="FormTableItem">
                                    <input id="txtNumApprovals" type="text" />
</asp:TableCell>
								<asp:TableCell ID="TableCell11" SkinID="FormTableHelp" runat="server"> 
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="TableRow4" runat="server">
								<asp:TableCell ID="TableCell12" runat="server" SkinID="FormTableLabel">
									<asp:Literal ID="RecertFrequency" runat="server" meta:resourceKey="RecertFrequency"></asp:Literal>
									</asp:TableCell>
								<asp:TableCell ID="TableCell13" runat="server" SkinID="FormTableItem">
									<dx:ASPxComboBox ID="ASPxComboBoxRecertFrequency" ClientInstanceName="cbRecertFrequency"
										runat="server" ClientEnabled="true">
									    <clientsideevents selectedindexchanged="handleChangeOnTab" />
										<Items>
											
<dx:ListEditItem Text="" Value="1" meta:resourcekey="Monthly" />											
<dx:ListEditItem Text="" Value="3" meta:resourcekey="Quarterly" />											
<dx:ListEditItem Text="" Value="6" meta:resourcekey="BiAnnually" />											
<dx:ListEditItem Text="" Value="12" meta:resourcekey="Annually" />
										
</Items>
									
</dx:ASPxComboBox>
								
</asp:TableCell>
								<asp:TableCell ID="TableCell14" SkinID="FormTableHelp" runat="server"> 
								</asp:TableCell>
							</asp:TableRow>
						</asp:Table>
						<br />
						<div id="divSaveAccessAreaGeneral" style="margin-top: 10px; margin-bottom: 10px;">
							<div id="divSaveAccessAreaGeneralButton" style="float: left;">
								<dx:ASPxButton ID="buttonUpdateAccessArea" ClientInstanceName="buttonUpdateAccessArea"
									AutoPostBack="False" runat="server" Text="" meta:resourcekey="Save">
									<ClientSideEvents Click="function(s,e){ UpdateArea(); setDirty(false);}" />
								</dx:ASPxButton>
							</div>
							<div id="div3" style="margin-left: 5px; float: left;">
								<dx:ASPxButton ForeColor="#0098DB" ID="ButtonCancelAccessAreaGeneral" ClientInstanceName="buttonCancelAccessAreaGeneral"
									AutoPostBack="False" runat="server" Text="" 
									meta:resourcekey="Cancel">
									<ClientSideEvents Click="function(s,e){ HideAccessAreaDetail(); }" />
								</dx:ASPxButton>
							</div>
							<div id="divClearAccessAreaGeneralSaveButton" style="clear: both;">
							</div>
						</div>
						<div style="clear: both;">
						</div>
						<span class="TaskDetailLabel" style="width: 150px;">&nbsp;</span>
						<span class="TaskDetailContent" style="width: 250px;"></span>
					</div>
					<div id="tabs-2">
						<!--classificationsDropdown-->
						<dx:ASPxComboBox ID="exClassifications" ClientInstanceName="classifications" runat="server"
							TextField="Classification" ValueField="HRClassificationID"
							Width="300px" ValueType="System.String">
							<ClientSideEvents SelectedIndexChanged="function (s,e) { ClassificationChanged(); }" />
						</dx:ASPxComboBox>
						<div class="displaytext">
							<asp:Literal ID="AddOrAmend" runat="server" meta:resourceKey="AddOrAmend"></asp:Literal>							
						</div>
						
						<br />
						<dx:ASPxGridView ID="Approvers" ClientInstanceName="Approvers" runat="server" AutoGenerateColumns="False"
							KeyFieldName="CorporateDivisionAccessAreaApproverId" OnCustomCallback="ApproversCustomCallback" OnCustomJSProperties="Approvers_CustomJSProperties"
							ClientIDMode="AutoID" OnHtmlRowCreated="Approvers_HtmlRowCreated" SettingsPager-Mode="ShowAllRecords" 
							meta:resourcekey="ApproversResource1">
							<Settings ShowStatusBar="Visible" />
                            <Styles Row-CssClass="dxgvDataRow_Office2010Silver Approvers_OrderingClass" />
							<SettingsBehavior AllowFocusedRow="false" AllowMultiSelection="true" AllowSort="false" />
							<Columns>
								<dx:GridViewDataImageColumn VisibleIndex="1">
									<DataItemTemplate>
										<asp:Image runat="server" ID="removeImage" SkinID="Trash" 
											AlternateText="" onclick='<%# "DeletePersonAA(" + Eval("dbPeopleID") + ", " + Eval("ClassificationID") + ");" %>' meta:resourcekey="removeImageText" />
                                        <div style="display: none;" class="approverRowNumber">&nbsp;</div>
									</DataItemTemplate>
								</dx:GridViewDataImageColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="SortOrder" ShowInCustomizationForm="True"
									VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnOrder">
									<DataItemTemplate>
										<asp:Label runat="server" ID="LabelSortOrder" Text='<%# Eval("SortOrder") %>'/>
										<asp:Image ID="ImageSortUp" runat="server" SkinID="SortUp" onclick=<%# string.Format("handleupclick({0});", Eval("dbPeopleID")) %>
											meta:resourcekey="ImageSortUpResource1"  CssClass="up" />
										<asp:Image ID="ImageSortDown" runat="server" SkinID="SortDown" onclick=<%# string.Format("handledownclick({0});", Eval("dbPeopleID")) %>
											meta:resourcekey="ImageSortDownResource1" CssClass="down" />
                                        <div style="display: none;" class="dbpeopleId"><%# Eval("dbPeopleID")  %></div>
									</DataItemTemplate>
								</dx:GridViewDataTextColumn>
	<%--							<dx:GridViewDataTextColumn Caption="" FieldName="SortOrder" ShowInCustomizationForm="True"
									VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnOrder">
								</dx:GridViewDataTextColumn>--%>
								<dx:GridViewDataCheckColumn Caption="" FieldName="Enabled" ShowInCustomizationForm="True"
									VisibleIndex="3" meta:resourcekey="GridViewDataCheckColumnEnabled">
								</dx:GridViewDataCheckColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="Name" ShowInCustomizationForm="True"
									VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnName">
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="EmailAddress" ShowInCustomizationForm="True"
									VisibleIndex="5" Width="140px" meta:resourcekey="GridViewDataTextColumnEmail">
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="DateAccepted" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
									ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewDataDateColumnDateAccepted">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="DateNextCertification"
									PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="7" 
									meta:resourcekey="GridViewDataDateColumnRecertificationDue">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="LeaveDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
									ShowInCustomizationForm="True" VisibleIndex="8" meta:resourcekey="GridViewDataDateColumnOutOfOfficeStart">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="ReturnDate"
									PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" ShowInCustomizationForm="True"
									VisibleIndex="9" meta:resourcekey="GridViewDataDateColumnOutOfOfficeReturn">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="Classification" ShowInCustomizationForm="True"
									VisibleIndex="11" Width="110px" meta:resourcekey="GridViewDataTextColumnClassification">
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataTextColumn VisibleIndex="12" Width="120px">
									<DataItemTemplate>
										<asp:HyperLink ID="HyperLinkSetOutOfOffice" NavigateUrl="javascript:{};" 
											runat="server" onclick='<%# "ShowOutOfOfficePopup(" + Eval("dbPeopleID") + ");"%>' Visible='<%# Convert.ToBoolean(Eval("Enabled")) %>'
											Text="" meta:resourcekey="HyperLinkSetOutOfOffice" />
									</DataItemTemplate>
								</dx:GridViewDataTextColumn>
							</Columns>
							<Templates>
								<StatusBar>
									<dx:ASPxButton ID="btnAdd" AutoPostBack="False" runat="server" 
										Text="" meta:resourcekey="AddApprover">
										<ClientSideEvents Click='function(s,e){ ShowAddPersonAA(); }' />
									</dx:ASPxButton>
						<div id="divSaveAccessAreaApprovers" style="float: right;">
							<div id="divSaveAccessAreaApproversButton" style="float:left;">
								<dx:ASPxButton ID="btnConfirm" AutoPostBack="False" Style="float: left;" 
									runat="server" Text="" meta:resourcekey="btnConfirmSave">
									<ClientSideEvents Click='function(s,e){ ConfirmChangesAA(); setDirty(false);}' />
								</dx:ASPxButton>
							</div>
							<div id="divCancelButtonAAAprovers" style="margin-left: 5px; float:left;">
								<dx:ASPxButton  ForeColor="#0098DB" ID="ButtonCancelBusinessAreaApprovers" 
									ClientInstanceName="buttonCancelBusinessAreaApprovers" AutoPostBack="False" 
									runat="server" Text="Cancel" 
									meta:resourcekey="btnConfirmCancel">
									<ClientSideEvents Click="function(s,e){ CancelAccessAreaRoleUserChanges(true); }" />
								</dx:ASPxButton>
							</div>

							<div style="clear: both;">
							</div>
						</div>
								</StatusBar>
							</Templates>
							<SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyApproverForMyAccessAreaDetails %>"/>
						</dx:ASPxGridView>
						<br />
						<div style="float: left;">
						</div>
						<div class="displaytext" style="margin: 0px; width: 100%;">
							<asp:Literal ID="AdditionsConfirmed" runat="server" meta:resourceKey="AdditionsConfirmed"></asp:Literal>							
						</div>
						<br />
						<div style="clear: both;">
						</div>
					</div>
					<div id="tabs-3">
						<dx:ASPxGridView ID="Recertifiers" ClientInstanceName="Recertifiers" runat="server"
							AutoGenerateColumns="False" KeyFieldName="CorporateDivisionAccessAreaApproverId"  OnCustomJSProperties="Recertifiers_CustomJSProperties"
							OnCustomCallback="RecertifiersCustomCallback" ClientIDMode="AutoID"  SettingsPager-Mode="ShowAllRecords" 
							OnHtmlRowCreated="Recertifiers_HtmlRowCreated">
							<Settings ShowStatusBar="Visible" />
							<SettingsBehavior AllowFocusedRow="False" AllowMultiSelection="true" AllowSort="false" />
                            <Styles Row-CssClass="dxgvDataRow_Office2010Silver Recertifier_OrderingClass" />
							<Columns>
								<dx:GridViewDataImageColumn VisibleIndex="1">
									<DataItemTemplate>
										<asp:Image runat="server" ID="removeImage" SkinID="Trash" 
											AlternateText="" onclick='<%# "DeletePersonAR(" + Eval("dbPeopleID") + ");" %>' meta:resourcekey="removeImageText" />
									</DataItemTemplate>
								</dx:GridViewDataImageColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="SortOrder" ShowInCustomizationForm="True"
									VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnOrder">
									<DataItemTemplate>
                                        <asp:Label runat="server" ID="LabelSortOrder" Text='<%# Eval("SortOrder") %>' />
										<asp:Image ID="Image1" runat="server" SkinID="SortUp" onclick=<%# string.Format("handleupclickrecertifier({0});", Eval("dbPeopleID")) %>
											meta:resourcekey="ImageSortUpResource1"  CssClass="up" />
										<asp:Image ID="Image2" runat="server" SkinID="SortDown" onclick=<%# string.Format("handledownclickrecertifier({0});", Eval("dbPeopleID")) %>
											meta:resourcekey="ImageSortDownResource1" CssClass="down" />
                                        <div style="display: none;" class="dbpeopleId"><%# Eval("dbPeopleID")  %></div>																				
									</DataItemTemplate>
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataCheckColumn Caption="" FieldName="Enabled" ShowInCustomizationForm="True"
									VisibleIndex="3" meta:resourcekey="GridViewDataCheckColumnEnabled">
								</dx:GridViewDataCheckColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="Name" ShowInCustomizationForm="True"
									VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnName">
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataTextColumn Caption="" FieldName="EmailAddress" ShowInCustomizationForm="True"
									VisibleIndex="5" Width="140px" meta:resourcekey="GridViewDataTextColumnEmail">
								</dx:GridViewDataTextColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="DateAccepted" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
									ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewDataDateColumnDateAccepted">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="DateNextCertification"
									PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="7" 
									meta:resourcekey="GridViewDataDateColumnRecertificationDue">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="LeaveDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
									ShowInCustomizationForm="True" VisibleIndex="8" meta:resourcekey="GridViewDataDateColumnOutOfOfficeStart">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataDateColumn Caption="" FieldName="ReturnDate"
									PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" ShowInCustomizationForm="True"
									VisibleIndex="9" meta:resourcekey="GridViewDataDateColumnOutOfOfficeReturn">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
								</dx:GridViewDataDateColumn>
								<dx:GridViewDataTextColumn VisibleIndex="10" Width="120px">
									<DataItemTemplate>
										<asp:HyperLink ID="HyperLinkSetOutOfOffice" NavigateUrl="javascript:{};" 
											runat="server" onclick='<%# "ShowOutOfOfficePopup(" + Eval("dbPeopleID") + ");"%>' Visible='<%# Convert.ToBoolean(Eval("Enabled")) %>' 
											Text="" meta:resourcekey="HyperLinkSetOutOfOffice" />
									</DataItemTemplate>
								</dx:GridViewDataTextColumn>
							</Columns>
							<Templates>
								<StatusBar>
									<dx:ASPxButton ID="btnAdd" AutoPostBack="False" runat="server" 
										Text="" meta:resourcekey="btnAddRecertifier">
										<ClientSideEvents Click='function(s,e){ ShowAddPersonAR(); }' />
									</dx:ASPxButton>
						<div id="divSaveAccessAreaRecertifiers" style="float:right;">
							<div id="divSaveAccessAreaRecertifiersButton" style="float:left;">
								<dx:ASPxButton ID="ButtonSaveAccessAreaRecertifiers" AutoPostBack="False" 
									Style="float: left;" runat="server"
									Text="" meta:resourcekey="btnConfirmSave">
									<ClientSideEvents Click='function(s,e){ ConfirmChangesAR();  setDirty(false);}' />
								</dx:ASPxButton>
							</div>
							
							<div id="divCancelAccessRecertifiersButton" style="margin-left: 5px; float:left;">
								<dx:ASPxButton  ForeColor="#0098DB" ID="ButtonCancelAccessRecertifiers" 
									ClientInstanceName="buttonCancelAccessRecertifiers" AutoPostBack="False" 
									runat="server" Text="" 
									meta:resourcekey="btnConfirmCancel">
									<ClientSideEvents Click="function(s,e){ CancelAccessAreaRoleUserChanges(false); }" />
								</dx:ASPxButton>
							</div>

							<div style="clear: both;">
							</div>
						</div>
								</StatusBar>
							</Templates>
						</dx:ASPxGridView>
						<br />
						<p>
							<asp:Literal ID="AdditionsConfirmedRecertifier" runat="server" meta:resourceKey="AdditionsConfirmedRecertifier"></asp:Literal>
							
						</p>

					</div>
				</div>
			</div>
		</div>
	</div>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldDivisionAccessArea" ClientInstanceName="hdDivisionAccessArea"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxPopupControl ID="PopupAddApprover" ClientInstanceName="popupAddApprover"
		runat="server" SkinID="PopupCustomSize" Width="500px" Height="250px" 
		HeaderText="" 
		meta:resourcekey="PopupAddApprover">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControlAA" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
					<span id="popupHeaderAA"></span>
				</h2>
				<div>
					<asp:Literal ID="EnterPersonsName1" runat="server" meta:resourceKey="EnterPersonsName"></asp:Literal>
				</div>
				<br />
				<uc3:PersonSelector ID="PersonSelectorAA" ServiceMethod="GetPersonCompletionListWithValidIds"
					ControlIndex="1" runat="server" />
				<br />
				<br />
				<asp:Literal ID="Ordering1" runat="server" meta:resourceKey="Ordering"></asp:Literal> (1-<%= MaximumApprovers %>)
				
				<input id="SortOrderTextBoxAA" type="text" />
				<br /><br />
				<div id="ButtonsAddAA" style="clear: both; margin-top: 10px; margin-bottom: 10px;">
					<div id="divAddAASaveButton" style="float:left;">
						<dx:ASPxButton ID="btnAddApprover" runat="server" AutoPostBack="False" 
							Text="" meta:resourcekey="btnAddApprover">
							<ClientSideEvents Click="function(s,e){ popupAddApprover.Hide(); AddPersonAA(); setDirty(true);}">
							</ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div id="divAddAACancelButton" style="margin-left: 5px; float:left;">
						<dx:ASPxButton ID="ASPxButtonCancelAA" runat="server" AutoPostBack="False" 
							Text="" ForeColor="#0098db" 
							meta:resourcekey="btnConfirmCancel">
							<ClientSideEvents Click="function(s,e){ popupAddApprover.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="PopupAddRecertifier" ClientInstanceName="popupAddRecertifier"
		runat="server" SkinID="PopupCustomSize" Width="500px" Height="250px" 
		HeaderText="" 
		meta:resourcekey="PopupAddRecertifier">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControlAR" runat="server" 
				SupportsDisabledAttribute="True">
				<h2>
					<span id="popupHeaderAR"></span>
				</h2>
				<p>
				<asp:Literal ID="EnterPersonsName" runat="server" meta:resourceKey="EnterPersonsName"></asp:Literal>
					</p>
				<br />
				<uc3:PersonSelector ID="PersonSelectorAR" ServiceMethod="GetPersonCompletionListWithValidIds"
					ControlIndex="2" runat="server" />
				<br />
				<br />
				<asp:Literal ID="Ordering" runat="server" meta:resourceKey="Ordering"></asp:Literal> (1-<%= MaximumRecertifiers %>)
				<input id="SortOrderTextBoxAR" class="order-recertifiers" type="text" />
				<br /><br />
				<div id="ButtonsAddAR" style="clear: both; margin-top: 10px; margin-bottom: 10px;">
					<div id="divAddARSaveButton" style="float:left;">
					<dx:ASPxButton ID="btnAddRecertifier" runat="server" AutoPostBack="False" 
							Text="" meta:resourcekey="btnConfirmAddRecertifier">
						<ClientSideEvents Click="function(s,e){ popupAddRecertifier.Hide(); AddPersonAR();  setDirty(true);}">
						</ClientSideEvents>
					</dx:ASPxButton>
					</div>
					<div id="divAddARCancelButton" style="margin-left: 5px; float:left;">
						<dx:ASPxButton ID="ASPxButtonCancelAR" runat="server" AutoPostBack="False" Text=""
							ForeColor="#0098db" meta:resourcekey="btnConfirmCancel">
						    <ClientSideEvents Click="function(s,e){ popupAddRecertifier.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
	<dx:ASPxPopupControl ID="PopupOutOfOffice" ClientInstanceName="popupOutOfOffice"
		runat="server" SkinID="PopupCustomSize" HeaderText="Out Of Office" Width="580px"
		Height="470px" PopupVerticalAlign="TopSides" ContentUrl="~/Pages/Popup/OutOfOfficePopup.aspx"
		ContentUrlIFrameTitle="OutOfOfficePopup" 
		meta:resourcekey="PopupOutOfOfficeResource1">
		<ContentCollection>
			<dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
			</dx:PopupControlContentControl>
		</ContentCollection>
	    <ClientSideEvents CloseUp="function(s, e) { CloseOutOfOfficePopup(); }"/>
	</dx:ASPxPopupControl>
	<ppc:Popups ID="PopupTemplates" runat="server" />
	<dx:ASPxHiddenField ID="ASPxHiddenFieldRequestDetail" ClientInstanceName="hfRequestDetail"
		runat="server">
	</dx:ASPxHiddenField>
	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestArrayAA" ClientInstanceName="hfAccessRequestArrayAA" runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestArrayAR" ClientInstanceName="hfAccessRequestArrayAR" runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestIdAA" ClientInstanceName="hfAccessRequestIdAA"
		runat="server">
	</dx:ASPxHiddenField>

	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessRequestIdAR" ClientInstanceName="hfAccessRequestIdAR"
		runat="server">
	</dx:ASPxHiddenField>
</asp:Content>
