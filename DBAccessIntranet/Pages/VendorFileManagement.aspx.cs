﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Reflection;
using System.Data;
using System.Text;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class VendorFileManagement : BaseSecurityPage
	{
		#region properties
		private List<vw_SmartcardProviderDetails> SmartcardProviders
		{
			get
			{
				if (null == Session["SmartcardProviders"])
                    return Director.GetSmartCardProvidersExtended(false, GetResourceManager("Resources.CommonResource", "SelectProvider"));

				return (List<vw_SmartcardProviderDetails>)Session["SmartcardProviders"];
			}
		}
		#endregion

		#region events
		protected void Page_Load(object sender, EventArgs e)
		{
			BindData();
        }

		protected void btnSubmitSmartcardNumbers_Click(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				int selectedProviderID = 0;

				if (ComboBoxSmartcardProviders.SelectedItem == null || !int.TryParse(ComboBoxSmartcardProviders.SelectedItem.Value.ToString(), out selectedProviderID))
					return;

				string message = string.Empty;

				using (StreamReader reader = new StreamReader(inputFileId.FileContent))
				{
					message = Director.UploadSmartcardNumbers(reader, selectedProviderID);
				}

				if (String.IsNullOrEmpty(message))
				{
					Response.Clear();
					Server.Transfer(@"~/Pages/UploadValidationResults.aspx");
				}
				else
				{
					LabelDuplicateSerialError.Visible = true;
					LabelDuplicateSerialError.Text = message;
				}
			}
		}

		protected void ValidateVendor(object source, ServerValidateEventArgs args)
		{
			int selectedProviderID = ComboBoxSmartcardProviders.SelectedIndex;
			args.IsValid = selectedProviderID > 0;
		}

		protected void btnExportNumbers_Command(object sender, CommandEventArgs e)
		{
			string[] args = e.CommandArgument.ToString().Split(',');
			if (args == null || args.Length < 2)
				return;

			int auditID = Convert.ToInt32(args[0]);
			int visibleIndex = Convert.ToInt32(args[1]);
			vw_SmartcardUploadAudit row = (vw_SmartcardUploadAudit)ASPxGridViewFileUploads.GetRow(visibleIndex);

			if (row.RecallDate != null)
			{
				LoadJQuery();
				return;
			}

			ExportToExcel("SmartcardNumbers", auditID);
		}
		
		protected bool GetRecallButtonVisible(int rowVisibleIndex) 
		{
			vw_SmartcardUploadAudit row = (vw_SmartcardUploadAudit)ASPxGridViewFileUploads.GetRow(rowVisibleIndex);

			if (row != null)
				return row.RecallDate == null;

			return false;
        }

		[WebMethod, ScriptMethod]
		public static int GetNumberOfMatchingBadges(int auditID)
		{
			int matches = Director.GetNumberOfMatchingBadges(auditID);
			if (matches < 0)
				return 0;

			return matches;
		}

		[WebMethod, ScriptMethod]
		public static bool RecallFile(int auditID)
		{
			return Director.RecallFile(auditID);
		}
		#endregion events
		
		#region helper methods
		private void BindData()
		{
			if(ComboBoxSmartcardProviders.DataSource == null)
			{
				ComboBoxSmartcardProviders.DataSource = SmartcardProviders;
				ComboBoxSmartcardProviders.DataBind();

				if(ComboBoxSmartcardProviders.SelectedIndex == -1)
					ComboBoxSmartcardProviders.SelectedIndex = 0;
			}

			if(ASPxGridViewFileUploads.DataSource == null)
			{
			    ASPxGridViewFileUploads.DataSource = Director.GetVendorFiles();
			    ASPxGridViewFileUploads.DataBind();
			}
		}

		protected void ExportToExcel(string filename, int auditID)
		{
			HttpContext context = HttpContext.Current;

			context.Response.Clear();
			context.Response.Buffer = true;
			context.Response.ContentType = "text/plain";
			context.Response.AddHeader("Cache-Control", "no-store, no-cache");
			context.Response.AddHeader("Content-Disposition: attachment", string.Format("attachment; filename={0}.csv", filename));

			List<string> columnsToExport = GetColumnHeaders();

			if (columnsToExport.Count > 0)
			{
				bool firstColumn = true;

				foreach (string column in columnsToExport)
				{
					if (!firstColumn)
						context.Response.Write(",");

					firstColumn = false;

					context.Response.Write(column);
				}

				context.Response.Write(Environment.NewLine);


				List<vw_SmartcardNumbers> smartcardNumbers = Director.GetSmartcardNumbers(auditID);
				List<List<string>> rows = GetRowData<vw_SmartcardNumbers>(smartcardNumbers);

				foreach (List<string> row in rows)
				{
					firstColumn = true;

					foreach (string column in row)
					{
						if (!firstColumn)
							context.Response.Write(",");

						firstColumn = false;
						string columnValue = column.Replace(",", string.Empty); //remove commas as they are used to separate commas in csv and will cause extra columns
						context.Response.Write(columnValue);
					}
					context.Response.Write(Environment.NewLine);
				}
			}

			context.Response.Flush();
			context.Response.End();
		}

		private List<List<string>> GetRowData<T>(List<T> rows)
		{
			List<List<string>> data = new List<List<string>>();
			List<string> properties = GetPropertyNames();
			foreach (var row in rows)
			{
				List<string> rowData = new List<string>();
				foreach (string propName in properties)
				{
					PropertyInfo property = row.GetType().GetProperty(propName);
					if(property != null)
					{
						object value = property.GetValue(row, null) ?? string.Empty;
						rowData.Add(value.ToString());
					}
				}
				data.Add(rowData);
			}

			return data;
		}

		private List<string> GetColumnHeaders()
		{
			return new List<string> { "Date Uploaded", "Operator", "Mifare", "Smartcard Number", "PUK", "Vendor", "Type Identifier" };
		}

		private List<string> GetPropertyNames()
		{
			return new List<string> { "DateUploaded", "Operator", "Mifare", "SerialNumber", "PUK", "ProviderName", "TypeIdentifier" };
		}

		private void LoadJQuery()
		{
			ScriptManager requestSM = ScriptManager.GetCurrent(this);
			if (requestSM != null && requestSM.IsInAsyncPostBack)
			{
				ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), GetJQueryCode(), true);
			}
			else
			{
				ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), GetJQueryCode(), true);
			}
		}

		private string GetJQueryCode()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("$(document).ready(function() {");
			sb.AppendLine("ExportRecalledFile();");

			sb.AppendLine(" });");
			return sb.ToString();
		}

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
		#endregion 
	}
}