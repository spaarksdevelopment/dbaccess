﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyRequestDetailsPopup.aspx.cs"
	Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.MyRequestDetailsPopup" uiculture="auto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
	<div>
		<dx:ASPxGridView ID="ASPxGridViewDetail" runat="server" AutoGenerateColumns="False"
			KeyFieldName="requestMasterID" ClientIDMode="AutoID" 
			OnCustomColumnDisplayText="ASPxGridViewDetail_CustomColumnDisplayText" 
			Width="100%">
			<Columns>
				<dx:GridViewDataTextColumn Caption="" FieldName="ApplicantName" ShowInCustomizationForm="True"
					VisibleIndex="1" Width="10%" meta:resourcekey="GridViewDataTextColumnApplicantName">
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataDateColumn Caption="" FieldName="StartDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
					VisibleIndex="2" Width="10%" meta:resourcekey="GridViewDataDateColumnStartDate">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
				</dx:GridViewDataDateColumn>
				<dx:GridViewDataDateColumn Caption="" FieldName="EndDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
					Width="10%" VisibleIndex="3" meta:resourcekey="GridViewDataDateColumnEndDate">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
				</dx:GridViewDataDateColumn>
				<dx:GridViewDataTextColumn Caption="" 
					FieldName="Access Required" CellStyle-BorderRight-BorderStyle="Solid"
					ShowInCustomizationForm="True" VisibleIndex="4" Width="30%" 
					meta:resourcekey="GridViewDataTextColumnAccessRequired">
					<DataItemTemplate>
						<asp:Label ID="Access" Text='<%# GetAccessAreaText(Eval("CityName"), Eval("BuildingName"), Eval("FloorName"), Eval("AccessAreaName"), Eval("IsSmartCard")) %>'
							runat="server"></asp:Label>
					</DataItemTemplate>

<CellStyle>
<BorderRight BorderStyle="Solid"></BorderRight>
</CellStyle>
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataMemoColumn Caption="" FieldName="History" 
					VisibleIndex="5" Width = "40%" meta:resourcekey="GridViewDataMemoColumnHistory">
				</dx:GridViewDataMemoColumn>
			</Columns>
             <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>"/>
            <SettingsLoadingPanel  Text="<%$ Resources:CommonResource, Loading %>"/> 
		</dx:ASPxGridView>
	</div>
	</form>
</body>
</html>
