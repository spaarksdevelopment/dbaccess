﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Resources;
using System.Globalization;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
	public partial class MyRequestDetailsPopup : BaseSecurityPage //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int masterRequestID = Convert.ToInt32(Request.QueryString["masterRequestID"]);


            ASPxGridViewDetail.DataSource = Director.GetMyRequestDetails(masterRequestID);
            ASPxGridViewDetail.DataBind();
        }

		protected void ASPxGridViewDetail_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
		{
            string histroyText = string.Empty; 
            string date = string.Empty;
            string histroyTextAfter=  string.Empty;

			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
		    if (e.Column.FieldName == "History")
		    {
				if (e.Value != null)
				{
                    //if (e.Value.ToString().Contains("\\n") || e.Value.ToString().Contains("\\r"))
                    //{
                    //    e.DisplayText = e.Value.ToString().Replace("\\n", "<br>");
                    //    e.DisplayText = e.Value.ToString().Replace("\\r", "<br>");
                    //}

                    if (e.Value.ToString().Contains("#"))
                    {
                        string[] history = e.Value.ToString().Split(Convert.ToChar("#"));
                        histroyText = history[0];
                        date = history[1];
                        histroyTextAfter = history[2];
                        histroyText = histroyText + " - ## " + histroyTextAfter;
                    }
                    else
                    {
                        histroyText = e.Value.ToString().Trim();
                    }

                    //if (histroyText.Contains("\\n") || histroyText.Contains("\\r"))
                    //{
                        histroyText = histroyText.Replace("\\n", "<br>");
                        histroyText = histroyText.Replace("\\r", "<br>");
                   // }

                        //histroyTextAfter = histroyTextAfter.Replace("\\r", "<br>");

                    if (histroyText.Contains("Awaiting action from"))
					{
						histroyText= histroyText.Replace("Awaiting action from", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AwaitingActionFrom)));
					}

                    if (histroyText.Contains("Request approved by"))
					{
						histroyText= histroyText.Replace("Request approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.RequestApprovedBy)));
					}

                    if (histroyText.Contains("Request rejected by"))
					{
						histroyText= histroyText.Replace("Request rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.RequestRejectedBy)));
					}

                    if (histroyText.Contains("Access approved by"))
					{
						histroyText= histroyText.Replace("Access approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessApprovedBy)));
					}

                    if (histroyText.Contains("Access rejected by"))
					{
						histroyText= histroyText.Replace("Access rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessRejectedBy)));
					}


                    if (histroyText.Contains("Pass Request approved by"))
					{
						histroyText= histroyText.Replace("Pass Request approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.PassRequestApprovedBy)));
					}

                    if (histroyText.Contains("Pass Request rejected by"))
					{
						histroyText= histroyText.Replace("Pass Request rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.PassRequestRejectedBy)));
					}

                    if (histroyText.Contains("Division Admin role approved by"))
					{
						histroyText= histroyText.Replace("Division Admin role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.DivisionAdminRoleApprovedBy)));
					}

                    if (histroyText.Contains("Division Admin role rejected by"))
					{
						histroyText= histroyText.Replace("Division Admin role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.DivisionAdminRoleRejectedBy)));
					}

                    if (histroyText.Contains("Access Area Approver role approved by"))
					{
						histroyText= histroyText.Replace("Access Area Approver role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessAreaApproverRoleApprovedBy)));
					}

                    if (histroyText.Contains("Access Area Approver role rejected by"))
					{
						histroyText= histroyText.Replace("Access Area Approver role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessAreaApproverRoleRejectedBy)));
					}

                    if (histroyText.Contains("Access Area Recertifier role approved by"))
					{
						histroyText= histroyText.Replace("Access Area Recertifier role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessAreaRecertifierRoleApprovedBy)));
					}

                    if (histroyText.Contains("Access Area Recertifier role rejected by")) 
					{
						histroyText= histroyText.Replace("Access Area Recertifier role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.AccessAreaRecertifierRoleRejectedBy)));
					}

                    if (histroyText.Contains("Request Approver role approved by"))
					{
						histroyText= histroyText.Replace("Request Approver role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.RequestApproverRoleApprovedBy)));
					}

					if (histroyText.Contains("Request Approver role rejected by")) 
					{
						histroyText= histroyText.Trim().Replace("Request Approver role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.RequestApproverRoleRejectedBy)));
					}

                    if (histroyText.Contains("Pass Office role approved by"))
                    {
                        histroyText= histroyText.Trim().Replace("Pass Office role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.PassOfficeRoleApprovedBy)));
                    }

                    if (histroyText.Contains("Pass Office role rejected by"))
					{
                        histroyText= histroyText.Trim().Replace("Pass Office role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.PassOfficeRoleRejectedBy)));
					}

                    if (histroyText.Contains("Division Owner role approved by"))
					{
                        histroyText= histroyText.Trim().Replace("Division Owner role approved by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.DivisionOwnerRoleApprovedBy)));
					}

                    if (histroyText.Contains("Division Owner role rejected by"))
					{
                        histroyText= histroyText.Trim().Replace("Division Owner role rejected by", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnumsRequestStatuses.DivisionOwnerRoleRejectedBy)));
					}

                    if (!string.IsNullOrEmpty(histroyText) && !string.IsNullOrEmpty(date))
                    {
                        histroyText = histroyText.Replace(@"##", Convert.ToDateTime(date).ToString("dd MMM yyyy", CultureInfo.CurrentUICulture));// +" - " + Convert.ToDateTime(date).ToString("dd MMM yyyy", CultureInfo.CurrentUICulture);
                    }

                    e.DisplayText = histroyText;

              

				}
		    }
		}

		public string GetAccessAreaText(object city, object building, object floor, object accessArea, object isSmartCard)
		{
			string text = string.Empty;

            if (isSmartCard != null && isSmartCard.ToString() == "1")
            {
                text = "Smart Card Request";
            }
            else
            {
                if (city != null)
                    text += city;

                if (building != null)
                    text += " \\ " + building.ToString();

                if (floor != null)
                    text += " \\ " + floor.ToString();

                if (accessArea != null)
                    text += " \\ " + accessArea.ToString();
            }
			
			return text;
		}

    }
}
