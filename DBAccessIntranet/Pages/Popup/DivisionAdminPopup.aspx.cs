﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using DBAccess.BLL.Abstract;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.CustomMembershipManager;
using DBAccess.BLL.Abstract.Helpers;
using DBAccess.BLL.Concrete;
using DBAccess.BLL.Concrete.Helpers;
using DBAccess.Model.Enums;
using DBAccess.Model.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class DivisionAdminPopup : BasePage
    {
        private const string DefaultRecertPeriod = "6";
		private const string DivisionRoleUsersCacheKey = @"Divisions/DivisionRoleUserList";

        private int _CurrentDivisionID;

        #region event handlers
        protected void Page_PreRender(object sender, EventArgs e)
        {
            DBAppUser theUserSession = new DBSqlMembershipProvider().GetUser();
            spaarks.DB.CSBC.DBAccess.DBAccessController.DAL.mp_User theUser = Director.GetUserBydbPeopleId(theUserSession.dbPeopleID);

            if (theUser != null && theUser.RestrictedCountryId.HasValue)
            {
                var restrictedCountryId = theUser.RestrictedCountryId.Value;
                BindRestrictedUserCountries(restrictedCountryId);
            }

            ObjectDataSourceRecertPeriods.SelectParameters["languageID"].DefaultValue = GetLanguage().ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			base.AddHeaderScriptInclude ("~/scripts/validationservice.js");

			if (!IsPostBack)
            {
                Session[DivisionRoleUsersCacheKey] = null;

                string pageType = GetQueryStringParameter("type");
                bool isSubdivision = GetIsSubdivision();

                string ubrCode = GetQueryStringParameter("ubrCode");
                if (ubrCode.Length > 0)
                {
                    ASPxHiddenFieldDivision.Set("ubrCode", ubrCode);
                }

                SetPageTypeSettings(pageType, isSubdivision);

				ASPxHiddenFieldRequestDetail.Set("RequestMasterID_DA", 0);
				ASPxHiddenFieldRequestDetail.Set("RequestMasterID_DO", 0);
                    
                ASPxHiddenFieldDivision.Add("pageType", pageType);
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
            }
        }

		protected void EditDivision(object sender, EventArgs e)
		{
		    OnEditDivision();
		}

        private void OnEditDivision()
        {
			int iDivisionID = Convert.ToInt32(ASPxHiddenFieldDivision.Get("DivisionID"));
            int requestMasterIDDA = Convert.ToInt32(ASPxHiddenFieldRequestDetail.Get("RequestMasterID_DA"));
            int requestMasterIDDO = Convert.ToInt32(ASPxHiddenFieldRequestDetail.Get("RequestMasterID_DO"));

            bool success = true;

            DivisionAdminPopupSettings divisionAdminPopupSettings = GetPageTypeSettings();
            if (divisionAdminPopupSettings.ShouldCheckForDuplicateCountry)
            {
                if (DoesDivisionAlreadyExistForThisCountry())
                {
                    success = false;
                    GenericRequestService genericRequestService = new GenericRequestService();
                    genericRequestService.RemoveDraftDivisionRequests(requestMasterIDDA, requestMasterIDDO);
                }
            }

            if (success)
                success = ConfirmChanges(iDivisionID, requestMasterIDDA, requestMasterIDDO);

            ShowChangesSavedMessage(success);
		}

        protected void ASPxGridViewDivisionRoleUsers_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            BindDivisionAdminGrid();
        }
        #endregion

        #region WebMethods
		[WebMethod(EnableSession = true), ScriptMethod]
		public static int AddPerson(int requestPersonDBPeopleID, int? divisionID, int requestMasterIDDA, int requestMasterIDDO, string roleString)
		{
            var listDivisionRoleUsers = GetDivisionRoleUsersFromCache();

            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);

		    RoleType roleType = divisionService.GetRoleFromString(roleString);

            AddDivisionRoleUserResult canWeAddDivisionRoleUserResult = divisionService.CanAddDivisionRoleUser(roleType, requestPersonDBPeopleID, listDivisionRoleUsers);
		    if (canWeAddDivisionRoleUserResult != AddDivisionRoleUserResult.Success)
		        return (int)canWeAddDivisionRoleUserResult;

		    int requestMasterID = GetRequestMasterID(requestMasterIDDA, requestMasterIDDO, roleType);

		    RequestType requestType = divisionService.GetRequestTypeFromRole(roleType);

            if (!divisionService.CreateNewRequestForDivisionRoleUser(requestPersonDBPeopleID, requestType, ref requestMasterID, divisionID, null))
		        return (int) AddDivisionRoleUserResult.ProblemCreatingRequest;

            AddRequestMasterIDToSession(requestMasterID, roleType);

            DivisionRoleUser newPerson = divisionService.CreateNewDivisionRoleUser(requestPersonDBPeopleID, currentUserID, roleType);

            listDivisionRoleUsers = divisionService.AddNewDivisionRoleUserToList(listDivisionRoleUsers, newPerson);

            AddDivisionRoleUsersToCache(listDivisionRoleUsers);

		    return requestMasterID;
        }

        private static void AddRequestMasterIDToSession(int requestMasterID, RoleType roleType)
        {
            if (roleType == RoleType.DivisionAdministrator)
            {
                HttpContext.Current.Session["requestMasterIDDA"] = requestMasterID;
                return;
            }

            HttpContext.Current.Session["requestMasterIDDO"] = requestMasterID;
        }

        [WebMethod(EnableSession = true), ScriptMethod]
		public static int DeletePerson(int personToDeleteDBPeopleID, int roleID, int divisionID)
        {
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);

            if(divisionID!=0) //Can't add new user's if division hasn't been created yet
                AddNewDivisionRoleUsers(divisionService, divisionID, currentUserID);
            
            RoleType roleType = (RoleType)roleID;

            if (divisionService.ShouldCheckNumberOfAdministrators(roleType, divisionID))
			{
                if (!divisionService.AreThereEnoughDivisionAdministratorsToDelete(divisionID, roleID))
			        return (int)DeleteDivisionRoleResult.InsufficientDivisionAdministratorsToDelete;
			}

            if (!divisionService.DeleteDivisionRoleUser(divisionID, (int)roleType, personToDeleteDBPeopleID))
            {
                return (int)DeleteDivisionRoleResult.DivisionOwnerCouldNotBeDeleted;
            }

            bool shouldResetMasterID = divisionService.RemoveDraftAndPendingDivisionRoleRequestForDeletedPerson(divisionID, roleType, personToDeleteDBPeopleID);

            RemoveDivisionRoleUserFromSession(personToDeleteDBPeopleID, roleID);

            return GetDeletePersonResult(shouldResetMasterID, roleType);
		}

        private static int GetDeletePersonResult(bool shouldResetMasterID, RoleType roleType)
        {
            if (!shouldResetMasterID)
                return (int) DeleteDivisionRoleResult.Success;

            if (roleType == RoleType.DivisionAdministrator)
                return (int)DeleteDivisionRoleResult.SuccessRemoveRequestMasterIDDA;

            return (int)DeleteDivisionRoleResult.SuccessRemoveRequestMasterIDDO; 
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static void RemoveDraftRequests(int? requestMasterIDDA, int? requestMasterIDDO)
        {
            GenericRequestService genericService = new GenericRequestService();
            genericService.RemoveDraftDivisionRequests(requestMasterIDDA, requestMasterIDDO);
        }

        #endregion

        #region public page methods
        public int GetLanguage()
        {
            int? retVal = null;

            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
            retVal = currentUser.LanguageId;
            return Convert.ToInt32(retVal);
        }

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
        #endregion

        #region private helpers

        private static void AddDivisionRoleUsersToCache(List<DivisionRoleUser> listDivisionRoleUsers)
        {
            HttpContext.Current.Session[DivisionRoleUsersCacheKey] = listDivisionRoleUsers;
        }

        private static List<DivisionRoleUser> GetDivisionRoleUsersFromCache()
        {
            List<DivisionRoleUser> listDivisionRoleUsers = new List<DivisionRoleUser>();

            // pick up the list from the cache if it exists
            if (HttpContext.Current.Session[DivisionRoleUsersCacheKey] != null)
            {
                listDivisionRoleUsers = (List<DivisionRoleUser>)HttpContext.Current.Session[DivisionRoleUsersCacheKey];
            }
            return listDivisionRoleUsers;
        }

        private static int GetRequestMasterID(int requestMasterIDDA, int requestMasterIDDO, RoleType roleType)
        {
            return roleType == RoleType.DivisionAdministrator ? requestMasterIDDA : requestMasterIDDO;
        }

        private static int GetDeleteDivisionRoleUserErrorCode(int roleID)
        {
            if (roleID == (int)RoleType.DivisionAdministrator)
                return (int)DeleteDivisionRoleResult.DivisionAdministratorCouldNotBeDeleted;

            return (int)DeleteDivisionRoleResult.DivisionOwnerCouldNotBeDeleted;
        }

        private void SetParentDivisionID(int divisionID)
        {
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);
            CorporateDivision division = divisionService.GetDivision(divisionID);

            if (divisionService.IsSubdivision(division))
                HttpContext.Current.Session["ParentDivisionID"] = division.SubdivisionParentDivisionID;
            else
                HttpContext.Current.Session["ParentDivisionID"] = division.UBR;
        }

        private string GetSelectedCountry()
        {
            string currentCountry = Convert.ToString(ddlCountry.SelectedValue);
            return currentCountry;
        }

        private bool DoesDivisionAlreadyExistForThisCountry()
        {
            string selectedCountry = GetSelectedCountry();
            string[] countryIDsForExistingDivisions = GetQueryStringParameter("CountryIDS").Split('|');

            return countryIDsForExistingDivisions.Contains(selectedCountry);
        }

        private void ShowChangesSavedMessage(bool success)
        {
            string divisionIdentifier = GetDivisionIdentifier();

            string changesSavedMessage = GetJavascriptToShowChangesSavedMessage(success, divisionIdentifier);
            LoadJQuery(changesSavedMessage);
        }

        private string GetDivisionIdentifier()
        {
            if (_CurrentDivisionID == 0)
                return null;

            if (GetIsSubdivision())
                return _CurrentDivisionID + "|S";

            return _CurrentDivisionID + "|D";
        }
         
        private DivisionAdminPopupSettings GetPageTypeSettings()
        {
            string pageType = GetQueryStringParameter("type");
            bool isSubdivision = GetIsSubdivision();
            return DivisionAdminPopupSettingsFactory.GetDivisionAdminPopupSettings(pageType, isSubdivision, GetResourceManager);
        }

        private bool GetIsSubdivision()
        {
            return Convert.ToBoolean(GetQueryStringParameter("isSubdivision"));
        }

        private bool ConfirmChanges(int divisionID, int requestMasterID_DA, int requestMasterID_DO)
        {
            bool newDivision = IsThisDivisionNew();

            string ubrCode = GetQueryStringParameter("ubrCode");

            UBRDivision division = GetDivisionFromControls(divisionID, ubrCode);

            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);

            bool isSubdivision = GetIsSubdivision();
            int? subdivisionParentDivisionID = GetSubdivisionParentDivisionID(isSubdivision);

            divisionID = divisionService.CreateEditDivision(division, newDivision, requestMasterID_DA, requestMasterID_DO, isSubdivision, subdivisionParentDivisionID);
            _CurrentDivisionID = divisionID;

            bool success = true;

            success = IsDivisionIDValid(divisionID);
            if (!success)
                return false;

            success = AddNewDivisionRoleUsers(divisionService, divisionID, currentUserID);

            if (!success)
                return false;

            RemoveDivisionRoleUsersFromSession();

            SetParentDivisionID(divisionID);

            return true;
        }

        private static bool AddNewDivisionRoleUsers(IDivisionService divisionService, int divisionID, int currentUserID)
        {
            List<DivisionRoleUser> listDivisionRoleUsers = GetDivisionRoleUsersFromCache();

            bool success = divisionService.AddNewDivisionRoleUsers(divisionID, listDivisionRoleUsers, currentUserID);
            if (!success)
                return false;

            Director director = new Director(currentUserID);
            return director.SubmitDivisionRoleRequests(divisionID);
        }

        private bool IsThisDivisionNew()
        {
            return GetQueryStringParameter("type") == "add";
        }

        private int? GetSubdivisionParentDivisionID(bool isSubdivision)
        {
            int? subdivisionParentDivisionID = null;

            if (isSubdivision)
                subdivisionParentDivisionID = GetParentDivisionIDFromQueryString();

            return subdivisionParentDivisionID;
        }

        private static void RemoveDivisionRoleUserFromSession(int dbpeopleID, int roleID)
        {
            var listDivisionRoleUsers = GetDivisionRoleUsersFromCache();
            var divisionRoleUserToRemove =
                listDivisionRoleUsers.FirstOrDefault(a => a.DBPeopleID == dbpeopleID && a.SecurityGroupID == roleID);

            if (divisionRoleUserToRemove != null)
            {
                listDivisionRoleUsers.Remove(divisionRoleUserToRemove);
                AddDivisionRoleUsersToCache(listDivisionRoleUsers);
            }
        }

        private void RemoveDivisionRoleUsersFromSession()
        {
            Session[DivisionRoleUsersCacheKey] = null;
        }

        private UBRDivision GetDivisionFromControls(int divisionID, string ubrCode)
        {
            UBRDivision division = new UBRDivision
            {
                Id = divisionID.ToString(),
                ChildNodeCount = 0,
                Name = txtDivisionName.Text,
                UBRCode = ubrCode,
                Enabled = Convert.ToBoolean(ddlDivisionEnabled.SelectedValue),
                CountryID = Convert.ToInt32(ddlCountry.SelectedValue),
                RecertPeriod = Convert.ToInt32(ddlRecertPeriod.SelectedValue)
            };

            return division;
        }

        private void DataBindPopup()
        {
            CorporateDivision division = GetDivision();

            txtDivisionName.Text = Server.HtmlDecode(division.Name);

            ddlDivisionEnabled.SelectedValue = Convert.ToString(division.Enabled).ToLower();

            SetSelectedCountry(division.CountryID);

            ddlRecertPeriod.SelectedValue = division.RecertPeriod.ToString();

            BindDivisionAdminGrid();
        }

        private void SetSelectedCountry(int countryID)
        {
            if(Director.IsCountryEnabled(countryID))
                ddlCountry.SelectedValue = Convert.ToString(countryID);
        }

        private bool IsDivisionIDValid(int iDivisionID)
        {
            return iDivisionID > 0;
        }

        private void SetPageTypeSettings(string pageType, bool isSubdivision)
        {
            DivisionAdminPopupSettings divisionAdminPopupSettings =
                DivisionAdminPopupSettingsFactory.GetDivisionAdminPopupSettings(pageType, isSubdivision, GetResourceManager);

            ddlRecertPeriod.SelectedValue = DefaultRecertPeriod;

            SetDivisionID(divisionAdminPopupSettings);
            SetColumnsVisibleForPopupSettings(divisionAdminPopupSettings);
            SetControlTextForPopupSettings(divisionAdminPopupSettings);
            SetHelpVisibleForPopupSettings(divisionAdminPopupSettings);

            if (divisionAdminPopupSettings.ShouldSetDefaultDivisionName)
                SetDefaultDivisionName(isSubdivision);
            else
                DataBindPopup();
        }

        private void SetDefaultDivisionName(bool isSubdivision)
        {
            if (isSubdivision)
                SetSubdivisionDefaultName();
            else SetDivisionDefaultName();
        }

        private void SetDivisionDefaultName()
        {
            string ubrName = GetQueryStringParameter("ubrName");
            txtDivisionName.Text = ubrName;
        }

        private void SetSubdivisionDefaultName()
        {
            string ubrCode = GetQueryStringParameter("ubrCode");
            txtDivisionName.Text = ubrCode + " - Subdivision";
        }

        private void SetColumnsVisibleForPopupSettings(DivisionAdminPopupSettings divisionAdminPopupSettings)
        {
            ASPxGridViewDivisionRoleUsers.Columns[6].Visible = divisionAdminPopupSettings.ShowDateAcceptedColumn;
            ASPxGridViewDivisionRoleUsers.Columns[7].Visible = divisionAdminPopupSettings.ShowDateRecertifyColumn;
        }

        private void SetControlTextForPopupSettings(DivisionAdminPopupSettings divisionAdminPopupSettings)
        {
            divisionTitle.Text = divisionAdminPopupSettings.PopupTitle;
            ButtonEditDivision.Text = divisionAdminPopupSettings.ButtonEditDivisionText;

            ASPxButtonAddDA.Text = divisionAdminPopupSettings.ButtonAddDivisionAdministratorText;
            ASPxButtonAddDO.Text = divisionAdminPopupSettings.ButtonAddDivisionOwnerText;
        }

        private void SetHelpVisibleForPopupSettings(DivisionAdminPopupSettings divisionAdminPopupSettings)
        {
            literalAddDivisionNameInfo.Visible = divisionAdminPopupSettings.IsLiteralAddDivisionNameInfoVisible;
            literalAddSubdivisionNameInfo.Visible = divisionAdminPopupSettings.IsLiteralAddSubdivisionNameInfoVisible;
            literalEditDivisionNameInfo.Visible = divisionAdminPopupSettings.IsLiteralEditDivisionNameInfoVisible;
            literalEditSubdivisionNameInfo.Visible = divisionAdminPopupSettings.IsLiteralEditSubdivisionNameInfoVisible;
            literalAddDivisionEnabled.Visible = divisionAdminPopupSettings.IsLiteralAddDivisionEnabledVisible;
            literalSubdivisionEnabledInfo.Visible = divisionAdminPopupSettings.IsLiteralSubdivisionEnabledInfoVisible;
            literalSelectCountryInfo.Visible = divisionAdminPopupSettings.IsLiteralSelectCountryInfoVisible;
            literalSelectCountrySubdivisionInfo.Visible = divisionAdminPopupSettings.IsLiteralSelectCountrySubdivisionInfoVisible;
            literalSelectRecert.Visible = divisionAdminPopupSettings.IsLiteralSelectRecertVisible;
            literalSelectRecertSubdivisionInfo.Visible = divisionAdminPopupSettings.IsLiteralSelectRecertSubdivisionInfoVisible;
        }

        private void BindRestrictedUserCountries(int restrictedCountryID)
        {
            ddlCountry.Items.Clear();

            ILocationService locationService = new LocationService();

            List<LocationCountry> filterCountry = new List<LocationCountry> { locationService.GetCountry(restrictedCountryID)};

            ddlCountry.DataSourceID = "";
            ddlCountry.DataSource = filterCountry;
            ddlCountry.DataBind();
        }

        private CorporateDivision GetDivision()
        {
            int divisionID = GetDivisionIDFromQueryString();

            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);
            return divisionService.GetDivision(divisionID);
        }

        private void SetDivisionID(DivisionAdminPopupSettings settings)
        {
            if (settings.ShouldSetDefaultDivisionID)
                ASPxHiddenFieldDivision.Set("DivisionID", 0);
            else
            {
                int divisionID = GetDivisionIDFromQueryString();
                ASPxHiddenFieldDivision.Set("DivisionID", divisionID);
            }
        }

        private int GetDivisionIDFromQueryString()
        {
            return GetDivisionIDParameterFromQueryString("divisionID");
        }

        private int GetParentDivisionIDFromQueryString()
        {
            if (GetQueryStringParameter("subdivisionParentDivisionID") == null)
                return GetDivisionIDFromQueryString();

            return GetDivisionIDParameterFromQueryString("subdivisionParentDivisionID");
        }

        private int GetDivisionIDParameterFromQueryString(string parameter)
        {
            string divisionID = GetQueryStringParameter(parameter);
            divisionID = divisionID.Split('|')[0];
            return Convert.ToInt32(divisionID);
        }

		private void BindDivisionAdminGrid()
		{
			int divisionID = Convert.ToInt32(ASPxHiddenFieldDivision.Get("DivisionID"));

            if (!AreDivisionRoleUsersCached())
                CacheDivisionRoleUsers(divisionID);

            var listDivisionRoleUsers = GetDivisionRoleUsersFromCache();
			
			ASPxGridViewDivisionRoleUsers.DataSource = listDivisionRoleUsers;
			ASPxGridViewDivisionRoleUsers.DataBind();
		}

        private void CacheDivisionRoleUsers(int divisionID)
        {
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            IDivisionService divisionService = new DivisionService(currentUserID);
            List<DivisionRoleUser> listDivisionRoleUsers = divisionService.GetDivisionRoleUsers(divisionID);

            Session[DivisionRoleUsersCacheKey] = listDivisionRoleUsers;
        }

        private bool AreDivisionRoleUsersCached()
        {
            return Session[DivisionRoleUsersCacheKey] != null;
        }

        private void LoadJQuery(string script)
        {
            ScriptManager requestScriptManager = ScriptManager.GetCurrent(this);

            if (requestScriptManager != null && requestScriptManager.IsInAsyncPostBack)
				ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), script, true);
			else
				ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), script, true);
        }

        private String GetJavascriptToShowChangesSavedMessage(bool results, string divisionIdentifier)
        {
            StringBuilder sb = new StringBuilder();

			sb.Append("$(document).ready(function() { ");
            sb.AppendFormat("OnEditDivisionRequest({0}, \"{1}\");", results.ToString().ToLower(), divisionIdentifier);
            sb.AppendLine(" });");

			return sb.ToString();
        }

        private string GetQueryStringParameter(string paramName)
        {
            return Request.QueryString[paramName];
        }

        #endregion

        public static int MaximumDAs
        {
            get { return Int32.Parse(ConfigurationManager.AppSettings["MaximumDAs"]); }
        }
    }
}