﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class OutOfOfficePopup : BasePage
    {
        public void Page_Load(object sender, EventArgs e)
        {
            ;
        }

        #region Web Methods
        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool SetOutOfOffice(DateTime LeaveDate, DateTime ReturnDate)
        {
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(CurrentUser.dbPeopleID);

            //note the substitute UserID is 0 as not implemented
            bool success = director.CreateNewOutOfOffice(LeaveDate, ReturnDate, Convert.ToInt32(HttpContext.Current.Session["dbPeopleID"]), 0);

            return success;
        }
        #endregion
    }
}