﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using MSA = Microsoft.Security.Application;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class RegionAdmin : BasePage
    {
        /// <summary>
        /// set the page title and button title on load
        /// allows for further expansion of page if the need to add a region happens
        /// E.Parker 
        /// 13.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
				ResourceManager resourceManager = Director.GetResourceManager("Resources.SiteMapLocalizations");
				string strResourceValue = resourceManager.GetString("EditRegionTitle");

				regionTitle.Text = strResourceValue; //Edit Region

				strResourceValue = resourceManager.GetString("SaveTitle");

				ASPxButRegion.Text = strResourceValue;// "Save";

                //now get the region ID
                string regionID = Request.QueryString["regionID"].ToString();
                ASPxHiddenRegion.Add("regionID", regionID);
                getRegionData(regionID);

                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
            }

        }


      /// <summary>
      /// Method to add /edit a Region
      /// E.Parker
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
        protected void AddRegion(object sender, EventArgs e)
        {
            DBAccessController.DAL.LocationRegion editedRegion = new DBAccessController.DAL.LocationRegion();
            //set the values on the object
            int newRegionID = Convert.ToInt32(ASPxHiddenRegion.Get("regionID").ToString().Replace("R", ""));
            editedRegion.RegionID = newRegionID;
			editedRegion.Name = MSA.Encoder.HtmlEncode(txtEditRegionName.Text);
			editedRegion.Code = MSA.Encoder.HtmlEncode(txtRegionCode.Text);
            editedRegion.Enabled = Convert.ToBoolean(ddlEditRegionEnabled.SelectedValue); 
          

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateRegion(editedRegion, false);
            loadJQuery(success);
            
        }

        

         #region BindData
         /// <summary>
        /// Method to return the region data from the director
        /// E.Parker 
        /// 13.04.11
        /// </summary>
        /// <param name="regionID"></param>
        public void getRegionData(string regionID)
        {
            //turn the region ID into an int
            int editRegionID = Convert.ToInt32(regionID.Replace("R", ""));

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(CurrentUser.dbPeopleID);
            int dbPeopleID = CurrentUser.dbPeopleID;

            DBAccessController.DAL.LocationRegion region = Director.GetRegion(editRegionID);

            BindData(region);
        }

        /// <summary>
        /// method to bind the page elements to the region data
        /// E.Parker 
        ///13.04.11
        /// </summary>
        /// <param name="region"></param>
        public void BindData(DBAccessController.DAL.LocationRegion region)
        {
            ////now bind the data 
            txtEditRegionName.Text = region.Name;
            ddlEditRegionEnabled.SelectedValue = Convert.ToString(region.Enabled).ToLower() ;
            txtRegionCode.Text = region.Code;
           
        }
        #endregion
        #region jQueryCallMethods
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }


        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditRegionRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditRegionRequest(false);");
            }
           
            
            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion
    }
}