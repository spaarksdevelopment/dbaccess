﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class UBRPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			base.AddHeaderScriptInclude ("~/scripts/validationservice.js");

			if (!IsCallback && !IsPostBack)
            {
                string UBRStartNode = (Request.QueryString["ubr"]==null) ? "G_0095" : Request.QueryString["ubr"].ToString();
                
                UBRSelector1.StartNode = UBRStartNode;
            }
        }
    }
}