﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CityAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.CityAdmin" %>

<%@ Register Src="~/Controls/Selectors/TimeZone.ascx" TagName="TimeZone" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="CityAdminLiteral" meta:resourcekey="CityAdmin" runat="server" ClientIDMode="Static"></asp:Literal></title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>


     <script type="text/javascript" language="javascript">
         function Cancel() {
             window.parent.CancelEditCity();
         }

         function OnEditCityRequest(result) {
             //cancel and refresh the grid
             if (!result) {
                 window.parent.ErrorShowPopup('city', false);
             }
             else {
                 window.parent.RefreshLocationGrid('city');
             }
         }
     </script>

</head>
<body>
<form id="form1" runat="server">

            <h2><asp:Literal id="cityTitle" runat="server" /></h2>
            <table>
                <tr>
                    <td><p>
                            <asp:Literal ID="Literal1" Text="<%$ Resources:CommonResource, Name%>" runat="server"></asp:Literal>
                        </p>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCityName" ClientIDMode="Static" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><asp:Literal ID="Name" text="<%$ Resources:CommonResource, Enabled%>" runat="server"></asp:Literal></p>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEditCityEnabled" runat="server"  ClientIDMode="Static">
                            <asp:ListItem Text="<%$ Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><asp:Literal ID="TimeZone" meta:resourcekey="TimeZone" runat="server"></asp:Literal></p>
                    </td>
                    <td>
                        <uc1:TimeZone ID="tzEditCity" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
			<br />
			<div style="float:left;">
                <div style="float: left; margin: 5px" id="div11">
					<asp:Button ID="ButtonAddCity" runat="server" Width="150" OnClick="addCity" Text = "<%$ Resources:CommonResource, SaveTitle%>"/>              
                </div>
                <div style="float: left; margin: 5px" id="div12">
						<dx:ASPxButton ID="ASPxButton4" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>"  ForeColor="#0098db" Width="150">                    
                            <ClientSideEvents Click="Cancel" />
                        </dx:ASPxButton>
                </div>
            </div>   

            <!--hidden field-->

            <dx:ASPxHiddenField ID="ASPxHiddenFieldCity" ClientInstanceName="hdCity" runat="server">
            </dx:ASPxHiddenField>
     
    </form>
</body>
</html>
