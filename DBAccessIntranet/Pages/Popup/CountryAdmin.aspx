﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.CountryAdmin" %>

<%@ Register Src="~/Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="CountryAdminLiteral" meta:resourcekey="CountryAdmin" runat="server" ClientIDMode="Static"></asp:Literal></title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>


     <script type="text/javascript" language="javascript">
         function Cancel() {
             window.parent.CancelEditCountry();
         }

         function OnEditCountryRequest(result, valid) {
             //cancel and refresh the grid
             if (!result && valid == "yes") {
                 window.parent.ErrorShowPopup('country', false);
             }
             else if (!result && valid == "no") {
                 window.parent.validation();
             }
             else {
                 window.parent.RefreshLocationGrid('country');
             }
         }
     </script>
</head>
<body>
      <form id="form1" runat="server">
               <h2><asp:Literal ID="countryTitle" runat="server" /></h2>           
            <table width="70%">
                <tr>
                    <td><p><asp:Literal ID="Name" text="<%$ Resources:CommonResource, Name%>" runat="server"></asp:Literal></p></td>
                    <td>
                         <asp:TextBox width="100%" ID="txtNameCountryEdit" runat="server" />        
                    </td>
                    <td>
                        <div id="countryNameHelpInfo" class="HelpPopUp">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><p><asp:Literal ID="Literal1" text="<%$ Resources:CommonResource, Enabled%>" runat="server"></asp:Literal></p></td>
                    <td>
                         <asp:DropDownList ID="ddlnewIDCountry" runat="server" >
                            <asp:ListItem Text="<%$ Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    <div id="countryEnabledHelpInfo" class="HelpPopUp">
                    </div>
                    </td>
                </tr>
                <tr>
                    <td><p><asp:Literal ID="CostCentreCode" meta:resourcekey="CostCentreCode" runat="server"></asp:Literal></p></td>
                    <td>                       
                         <asp:TextBox width="100%" ID="txtCostCentreCountryEdit" runat="server" />        
                    </td>
                    <td>
                    <div id="countryCCHelpInfo" class="HelpPopUp">                      
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><asp:Literal ID="PassOffice" meta:resourcekey="PassOffice" runat="server"></asp:Literal></p>
                    </td>
                    <td>
                       <uc2:PassOfficeSelector ID="POCountryEdit" runat="server" ClientIDMode="Static" />                       
                    </td>
                    <td>
                     <div id="countryPOHelpInfo" class="HelpPopUp">
                    </div>
                    </td>
                </tr>
            </table>
			<br />
            <div style="float:left;">
                <div style="float: left; margin: 5px" id="div11">
                    <asp:Button ID="ButtonCountry" runat="server" Width="150" OnClick="EditCountry" />                
                </div>
                <div style="float: left; margin: 5px" id="div12">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>"  ForeColor="#0098db" Width="150">                    
                         <ClientSideEvents Click="Cancel" />
                    </dx:ASPxButton>
                </div>
            </div>   
            <!--hidden field-->

            <dx:ASPxHiddenField ID="ASPxHiddenFieldCountry" ClientInstanceName="hdCountry" runat="server">
            </dx:ASPxHiddenField>
            </form>  
</body>
</html>
