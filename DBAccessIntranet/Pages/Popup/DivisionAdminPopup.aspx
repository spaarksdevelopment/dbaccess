﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisionAdminPopup.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.DivisionAdminPopup" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="~/Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector" TagPrefix="uc1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literaldivAdmin" Text="<%$Resources:CommonResource, DivisionAdministrator%>" runat="server"></asp:Literal>  </title>
    <asp:PlaceHolder runat="server">
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            
            setDirty(false);

            $('#divisionNameHelp').bind('click', function () {
                $('#divisionNameHelpInfo').slideToggle("slow");
            });

            $('#divisionEnabledHelp').bind('click', function () {
                $('#divisionEnabledHelpInfo').slideToggle("slow");
            });

            $('#divisionCountryHelp').bind('click', function () {
                $('#divisionCountryHelpInfo').slideToggle("slow");
            });

            $('#divisionRecertHelp').bind('click', function () {
                $('#divisionRecertHelpInfo').slideToggle("slow");
            });

            $('#divisionNameHelpInfo').hide();
            $('#divisionEnabledHelpInfo').hide();
            $('#divisionCountryHelpInfo').hide();
            $('#divisionRecertHelpInfo').hide();

            /////////////////
            $('#divisionNameHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
            $('#divisionNameHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');

            $('#divisionEnabledHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
            $('#divisionEnabledHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
           
            $('#divisionCountryHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
            $('#divisionCountryHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
          
            $('#divisionRecertHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
            $('#divisionRecertHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
                       
        });

        var isDirty;

        function setDirty(dirty) {
            isDirty = dirty;
        };

        function RaiseBasicPopupAlert(contentText, headerText) {
            popupAlertTemplateContent.SetText(contentText);
            popupAlertTemplate.SetHeaderText(headerText);
            popupAlertTemplate.Show();
        }

        function Cancel() {
            RemoveDraftRequests();

        	window.parent.CancelEditDivision();
        }

        function RemoveDraftRequests()
        {
            var requestMasterIDDA = hfRequestDetail.Get("RequestMasterID_DA");
            var requestMasterIDDO = hfRequestDetail.Get("RequestMasterID_DO");
            PageMethods.RemoveDraftRequests(requestMasterIDDA, requestMasterIDDO);
        }

        function OnEditDivisionRequest(result, divisionIdentifier)
        {
            setDirty(false);

        	//cancel and refresh the grid
        	if (!result)
        		window.parent.ErrorShowPopup('division', false);
        	else
        	    window.parent.RefreshDivisionGrid(divisionIdentifier);
        }

		var g_sRole = "";

		function AddPerson(sRole) {
		    g_sRole = sRole;
		    var idstring = $get("PersonSelectorNameTextBox").value;
		    var iPersonID = 0;

		    if (idstring.length > 0) {
		        SetServicesFolderPath('/../Services/');
		        iPersonID = GetPersonIdFromIdentifier(idstring);
		    }
            		 
		    if (iPersonID == 1) {
		        popupAlertTemplateContent.SetText('<%= AddPersonInvalidEmail.Text%>');//("The person you have selected has an invalid email address.");
		        popupAlertTemplate.SetHeaderText("<%= AddPersonLit.Text%>"); //("Add Person");
		        popupAlertTemplate.Show();
		        return;
		    }

		    if (iPersonID == 0) {
		        popupAlertTemplateContent.SetText('<%=AddPersonNoOne.Text%>'); //("Please select a valid Person to add.");
		        popupAlertTemplate.SetHeaderText("<%= AddPersonLit.Text%>"); //("Add Person");
		        popupAlertTemplate.Show();
		        return;
		    }

		    var iDivisionID = hfDivision.Get("DivisionID");
		    var iRequestMasterID_DA = hfRequestDetail.Get("RequestMasterID_DA");
		    var iRequestMasterID_DO = hfRequestDetail.Get("RequestMasterID_DO");

		    PageMethods.AddPerson(iPersonID, iDivisionID, iRequestMasterID_DA, iRequestMasterID_DO, sRole, OnAddPerson);
		}

        function OnAddPerson(result) {
            var errorMessage = GetAddPersonToDivisionErrorMessageFromResult(result);

            if (errorMessage != "") {
                popupAlertTemplateContent.SetText(errorMessage);
        	    popupAlertTemplate.SetHeaderText("<%= AddPersonLit.Text%>"); //("Add Person");
        	    popupAlertTemplate.Show();
        	    return;
        	}


        	switch (g_sRole)
        	{
        		case "DA":
        			{
        				hfRequestDetail.Set("RequestMasterID_DA", result);
        				break;
        			}
        		case "DO":
        			{
        				hfRequestDetail.Set("RequestMasterID_DO", result);
        				break;
        			}
        	}
        	$get("PersonSelectorNameTextBox").value = '';

        	setDirty(true);

        	// we need to reload the grid
        	gvDivisionAdmins.PerformCallback();
       }

        function GetAddPersonToDivisionErrorMessageFromResult(result)
        {
            var errorMessage = "";

            switch (result) {
                case 1:
                    {
                        errorMessage = '<%=InactivePerson.Text%>'; // "Sorry! cannot add Person as he is inactive";
                        break;
                    }
                case 2:
                    {
                        errorMessage = '<%=PersonHasNoEmailAddress.Text%>'; // "This person cannot be added as they have no email address";
                        break;
                    }
                case 3:
                    {
                        errorMessage = '<%=DiviAdminCantDelete.Text%>'; // "Division Administrator could not be added.";
                        break;
                    }
                case 4:
                    {
                        errorMessage = '<%=DiviAdminAlreadyExists.Text%>'; // "This person is already a division administrator for this division.";
                        break;
                    }
                case 5:
                    {
                        errorMessage = '<%=DiviAdminMaximum.Text.Replace("5", MaximumDAs.ToString()) %>'; //"You cannot have more than X division administrators in one division.";
                        break;
                    }
                case 6:
                    {
                        errorMessage = '<%=DivOwnerNotAdded.Text%>'; //"Division Owner could not be added.";
                        break;
                    }
                case 7:
                    {
                        errorMessage = '<%=DivOwnerAlreadyAssigned.Text%>'; // "There is already a division owner assigned to this division.";
                        break;
                    }
                case 8:
                    {
                        errorMessage = '<%=PersonCannotBeAddedToRequest.Text%>'; // "There was a problem adding this person.";
                        break;
                    }
            }

            return errorMessage;
        }

		// ----------------------------------------------------------------------

		var g_popup_roleId = 0;
		var g_popup_personId = 0;

		function DeletePerson(personId, roleId)
		{
		    if (isDirty) {
		        RaiseBasicPopupAlert('<%=SaveChangesWarning.Text %>', '<%= Error.Text %>');
                return;
		    }

    		g_popup_roleId = roleId;
    		g_popup_personId = personId;

    		popupConfirmTemplate.SetHeaderText('<%=RemovePerson.Text%>'); // ("Remove Person");
		    popupConfirmTemplateContentHead.SetText('<%=Person.Text%>'); // ("Person");
		    popupConfirmTemplateContentBody.SetText('<%=SavePersonMsg.Text%>'); // ("Are you sure you want to save this Person?");
    		popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemovePerson();");
    		popupConfirmTemplate.Show();
		}

		function OnRemovePerson()
		{
    		var roleId = g_popup_roleId;
    		var personId = g_popup_personId;
    		var divisionID = hfDivision.Get("DivisionID");

    		PageMethods.DeletePerson(personId, roleId, divisionID, OnDeletePerson);
		}

		function OnDeletePerson(result)
		{
		   
			var sError = "";

			switch (result)
    		{
    			case 3:
    				{
    				    sError ='<%=DivAdminCantDelete.Text%>'; //  "Division Administrator could not be deleted.";
    					break;
    				}
    			case 4:
    				{
    				    sError ='<%=DivAdminMinimum.Text%>'; //  "You cannot have less than 2 division administrators in one division.";
    					break;
    				}
    			case 5:
    				{
    				    sError ='<%=DivOwnerCantDelete.Text%>'; //  "Division Owner could not be deleted.";
    					break;
    				}
    		}

    		if (sError != "")
    		{
    			popupAlertTemplateContent.SetText(sError);
    			popupAlertTemplate.SetHeaderText("<%= DeletePersonLit.Text%>"); //("Delete Person");
    			popupAlertTemplate.Show();
				return;
    		}

		    ResetMasterID(result);

			// we need to reload the grid
			gvDivisionAdmins.PerformCallback();
		}

        function ResetMasterID(result) {
            if (result === 0)
                return;
            else if (result==1) //Remove DA Request MasterID
                hfRequestDetail.Set("RequestMasterID_DA", 0);
            else //Remove DO Request MasterID
                hfRequestDetail.Set("RequestMasterID_DO", 0);
       }

        // ----------------------------------------------------------------------

    </script>
    <style type="text/css">
        .style1
        {
            width: 163px;
        }
        .style2
        {
            width: 440px;
        }
    </style>
        </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <act:ToolkitScriptManager ID="Scriptmanager1" runat="server" enablepagemethods="true">
		</act:ToolkitScriptManager>
        <h2><asp:Literal ID="divisionTitle" runat="server" /></h2>           
        <table>
            <tr>
                <td class="style1">
                    <p>
                        <img id="divisionNameHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" />
                        &nbsp;
                        <asp:Literal ID="literal1" Text="<%$Resources:CommonResource, Name%>" runat="server"></asp:Literal> 
                    </p>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtDivisionName" runat="server" Width="250px"></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDivisionName" runat="server" ControlToValidate="txtDivisionName" meta:resourcekey="RequiredFieldValidatorDivisionName"  ></asp:RequiredFieldValidator>       
                </td>
                <td>
                    <div id="divisionNameHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddDivisionNameInfo" runat="server"  meta:resourcekey="literalAddDivisionNameInfo" />
                        <asp:Literal ID="literalEditDivisionNameInfo" runat="server"  meta:resourcekey="literalEditDivisionNameInfo" />
                        <asp:Literal ID="literalAddSubdivisionNameInfo" runat="server"  meta:resourcekey="literalAddSubdivisionNameInfo" />
                        <asp:Literal ID="literalEditSubdivisionNameInfo" runat="server"  meta:resourcekey="literalEditSubdivisionNameInfo" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <p>
                        <img id="divisionEnabledHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" />
                        &nbsp;
                        <asp:Literal ID="literal2" Text="<%$ Resources:CommonResource, Enabled%>" runat="server"></asp:Literal>?
                    </p>
                </td>
                <td class="style2">
                    <asp:DropDownList ID="ddlDivisionEnabled" runat="server" >
                        <asp:ListItem Text="<%$Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                <div id="divisionEnabledHelpInfo" class="HelpPopUp">
                    <asp:Literal ID="literalAddDivisionEnabled" runat="server"  meta:resourcekey="literalAddDivisionEnabled" />
                    <asp:Literal ID="literalSubdivisionEnabledInfo" runat="server" meta:resourcekey="literalSubdivisionEnabledInfo" />
                </div>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <p>
                        <img id="divisionCountryHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" />
                        &nbsp;
                        <asp:Literal ID="literal3" Text="<%$Resources:CommonResource, Country%>" runat="server"></asp:Literal>  
                    </p>
                </td>
                <td class="style2">                    
                    <asp:DropDownList ID="ddlCountry" runat="server" DataSourceID="ObjectDataSourceCountries" DataTextField="Name" DataValueField="CountryID" SkinID="DropDownList160" AppendDataBoundItems="true">
                        <asp:ListItem Value="0" meta:resourcekey="SelectCountry" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator id="RequiredFieldValidatorCountry"  
                        runat="server" meta:resourcekey="PleaseSelect"
                        ControlToValidate="ddlCountry" 
                        InitialValue="0">
                    </asp:RequiredFieldValidator>
                </td>
                <td>
                    <div id="divisionCountryHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalSelectCountryInfo" runat="server" meta:resourcekey="PleaseSelectCountry" />
                        <asp:Literal ID="literalSelectCountrySubdivisionInfo" runat="server" meta:resourcekey="PleaseSelectCountrySubdivision" />
                    </div>                    
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <p>
                        <img id="divisionRecertHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" />
                        &nbsp;
                        <asp:Literal ID="literal4" meta:resourcekey="RecertificationPeriod" runat="server"></asp:Literal> 
                    </p>
                </td>
                <td class="style2">                    
                    <asp:DropDownList ID="ddlRecertPeriod" runat="server" DataSourceID="ObjectDataSourceRecertPeriods" DataTextField="Value" DataValueField="Key" SkinID="DropDownList160" AppendDataBoundItems="true"  ClientIDMode="Static">
                        <asp:ListItem  Value="0"  meta:resourcekey="SelectRecertPeriod"/>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator id="RequiredFieldValidatorRecert"  
                        runat="server" meta:resourcekey="PleaseSelect"
                        ControlToValidate="ddlRecertPeriod" 
                        InitialValue="0">
                    </asp:RequiredFieldValidator>
                </td>
                <td>
                    <div id="divisionRecertHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalSelectRecert" runat="server" meta:resourcekey="literalSelectRecert" />
                        <asp:Literal ID="literalSelectRecertSubdivisionInfo" runat="server" meta:resourcekey="literalSelectRecertSubdivision" />
                    </div>                    
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <p>
                        <asp:Literal ID="literal5" Text="<%$Resources:CommonResource, DivisionAdministrators%>" runat="server"></asp:Literal>  /<br />
						 <asp:Literal ID="literal6" Text="<%$Resources:CommonResource, DivisionOwners%>" runat="server"></asp:Literal>  
                    </p>
                </td>
                <td class="style2">
                    <uc1:PersonSelector ID="PersonSelectorDAs" ServiceMethod="GetPersonCompletionListWithValidIds" runat="server" />
                </td>
                <td>
                    <dx:ASPxButton  ID="ASPxButtonAddDA" AutoPostBack="false" runat="server"   Text="<%$Resources:CommonResource, AddDivAdministrator%>">
                        <ClientSideEvents Click= "function(s, e) { AddPerson('DA');}" />
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td class="style1"/>
                <td class="style2"/>
                <td>
                    <dx:ASPxButton  ID="ASPxButtonAddDO" AutoPostBack="false" runat="server"   Text="<%$Resources:CommonResource, AddDivOwner%>">
                        <ClientSideEvents Click= "function(s, e) { AddPerson('DO');}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <div style="margin:10px;" id="divDivisionAdmins">
            <dx:ASPxGridView ID="ASPxGridViewDivisionRoleUsers" ClientInstanceName="gvDivisionAdmins"
                runat="server" AutoGenerateColumns="False" SettingsPager-Mode="ShowAllRecords" SettingsBehavior-AllowSort="false"
                KeyFieldName="DBPeopleID" 
                ClientIDMode="AutoID" oncustomcallback="ASPxGridViewDivisionRoleUsers_CustomCallback"
                >
                <Columns>
                    <dx:GridViewDataImageColumn VisibleIndex="0">
                        <DataItemTemplate>
                            <img src="../../App_Themes/DBIntranet2010/images/trash.gif" onclick='<%# "DeletePerson(\"" + Container.KeyValue + "\",\"" + DataBinder.Eval(Container, "DataItem.SecurityGroupID") + "\");" %>'  alt="<%$Resources:CommonResource, Delete%>" title="<%$Resources:CommonResource, Delete%>" runat="server"/>
                        </DataItemTemplate>
                    </dx:GridViewDataImageColumn>
                    <dx:GridViewDataTextColumn meta:resourcekey="DBPeopleID"  FieldName="DBPeopleID" ShowInCustomizationForm="True" VisibleIndex="1" CellStyle-HorizontalAlign="Right">
                        <CellStyle HorizontalAlign="Right"></CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, Forename%>" FieldName="Forename" ShowInCustomizationForm="True" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, Surname%>" FieldName="Surname" ShowInCustomizationForm="True" VisibleIndex="3">  
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataHyperLinkColumn Caption="<%$Resources:CommonResource, EmailAddress%>"  FieldName="EmailAddress" ShowInCustomizationForm="True" VisibleIndex="4">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="http://gd.intranet.db.com/iam/{0}" Target="_blank">
                        </PropertiesHyperLinkEdit>
                    </dx:GridViewDataHyperLinkColumn>
                    <dx:GridViewDataTextColumn meta:resourcekey="Role"  FieldName="SecurityGroup" ShowInCustomizationForm="True" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn meta:resourcekey="DateAccepted"  FieldName="DateAccepted" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" ShowInCustomizationForm="True" VisibleIndex="6" >
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn meta:resourcekey="RecertificationDue"  FieldName="DateNextCertification" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="7">                
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>                    
                    <dx:GridViewDataDateColumn meta:resourcekey="OutOfOfficeStart"  FieldName="LeaveDate"  PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" ShowInCustomizationForm="True" VisibleIndex="8">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>                
                    <dx:GridViewDataDateColumn meta:resourcekey="OutOfOfficeReturn" FieldName="ReturnDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" ShowInCustomizationForm="True" VisibleIndex="9">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="SecurityGroupID" ShowInCustomizationForm="True" Visible="false">
                    </dx:GridViewDataTextColumn>
                </Columns>

<SettingsPager Mode="ShowAllRecords"></SettingsPager>

                <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>"/>
                <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
            </dx:ASPxGridView>
        </div>
        <div style="float:left;">
            <div style="float: left; margin: 5px" id="div11">
                <asp:Button ID="ButtonEditDivision" runat="server" Width="150" Height="25" Text="Button" OnClick="EditDivision"/>
            </div>
            <div style="float: left; margin: 5px" id="div12">
                <dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$Resources:CommonResource, Cancel%>"  ForeColor="#0098db" Width="150">                    
                    <ClientSideEvents Click="Cancel" />
                </dx:ASPxButton>
            </div>
        </div>   

        <!--hidden field-->

        <dx:ASPxHiddenField ID="ASPxHiddenFieldDivision" ClientInstanceName="hfDivision" runat="server">
        </dx:ASPxHiddenField>

		<dx:ASPxHiddenField ID="ASPxHiddenFieldRequestDetail" ClientInstanceName="hfRequestDetail" runat="server">
		</dx:ASPxHiddenField>

        <asp:ObjectDataSource ID="ObjectDataSourceCountries" runat="server"
            TypeName="spaarks.DB.CSBC.DBAccess.DBAccessController.Director" SelectMethod="GetCountriesAllEnabled" >
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="ObjectDataSourceRecertPeriods" runat="server"
            TypeName="spaarks.DB.CSBC.DBAccess.DBAccessController.Director" SelectMethod="GetRecertPeriods" >
          <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="languageID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

	<ppc:Popups ID="PopupTemplates" runat="server" />
               
        <asp:Literal runat="server" ID="AddPersonLit" meta:resourcekey="AddPersonLit" Visible="false" ClientIDMode="Static"></asp:Literal> 
        <asp:Literal runat="server" ID="AddPersonInvalidEmail" meta:resourcekey="AddPersonInvalidEmail" Visible="false" ClientIDMode="Static"></asp:Literal>
        <asp:Literal runat="server" ID="AddPersonNoOne" meta:resourcekey="AddPersonNoOne" Visible="false" ClientIDMode="Static"></asp:Literal> 
        <asp:Literal runat="server" ID="DeletePersonLit" meta:resourcekey="DeletePersonLit" Visible="false" ClientIDMode="Static"></asp:Literal>
        <asp:Literal runat="server" ID="DiviAdminCantDelete" meta:resourcekey="DiviAdminCantDelete" Visible="false" ClientIDMode="Static"></asp:Literal>

        <asp:Literal runat="server" ID="DiviAdminAlreadyExists" meta:resourcekey="DiviAdminAlreadyExists" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DiviAdminMaximum" meta:resourcekey="DiviAdminMaximum" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivOwnerNotAdded" meta:resourcekey="DivOwnerNotAdded" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivOwnerAlreadyExists" meta:resourcekey="DivOwnerAlreadyExists" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivOwnerAlreadyAssigned" meta:resourcekey="DivOwnerAlreadyAssigned" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="InactivePerson" meta:resourcekey="InactivePerson" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="PersonHasNoEmailAddress" meta:resourcekey="PersonHasNoEmailAddress" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="PersonCannotBeAddedToRequest" meta:resourcekey="PersonCannotBeAddedToRequest" Visible="false"></asp:Literal>

        <asp:Literal runat="server" ID="RemovePerson" meta:resourcekey="RemovePerson" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="Person" meta:resourcekey="Person" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="SavePersonMsg" meta:resourcekey="SavePersonMsg" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivAdminCantDelete" meta:resourcekey="DivAdminCantDelete" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivAdminMinimum" meta:resourcekey="DivAdminMinimum" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="DivOwnerCantDelete" meta:resourcekey="DivOwnerCantDelete" Visible="false"></asp:Literal>
  
        <asp:Literal runat="server" ID="SaveChangesWarning" meta:resourcekey="SaveChangesWarning" Visible="false"></asp:Literal>
        <asp:Literal runat="server" ID="Error" meta:resourcekey="Error" Visible="false"></asp:Literal>

    </form>
</body>
</html>
