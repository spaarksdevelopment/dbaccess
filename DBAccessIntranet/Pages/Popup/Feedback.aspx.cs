﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Security.Application;

using DevExpress.Web.ASPxEditors;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
	public partial class Feedback : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// if both text and image are specified, only image is shown (a bug)
				// until it is fixed, manually override this and specify it explicitly

				var list = new List<ListEditItem>();

				list.Add(new ListEditItem(GetItemTextAndImage("~/Images/ic_vote1.gif", "very good"), 1));
				list.Add(new ListEditItem(GetItemTextAndImage("~/Images/ic_vote2.gif", "good"), 2));
				list.Add(new ListEditItem(GetItemTextAndImage("~/Images/ic_vote3.gif", "average"), 3));
				list.Add(new ListEditItem(GetItemTextAndImage("~/Images/ic_vote4.gif", "poor"), 4));
				list.Add(new ListEditItem(GetItemTextAndImage("~/Images/ic_vote5.gif", "very poor"), 5));

				ASPxRadioButtonListRating.DataSource = list;
				ASPxRadioButtonListRating.DataBind();
			}
		}

		/// <summary>
		/// formats a string that will show the text and the image
		/// </summary>
		/// <param name="sUrl"></param>
		/// <param name="sText"></param>
		/// <returns></returns>
		protected String GetItemTextAndImage(String sUrl, String sText)
		{
			return String.Format("<img src=\"{0}\" alt=\"{1}\" />&nbsp;&nbsp;{1}", Page.ResolveClientUrl(sUrl), sText);
		}

		[WebMethod, ScriptMethod]
		public static Boolean Send(Int32 iRating, String sSuggestions)
		{
			sSuggestions = Encoder.HtmlEncode(sSuggestions.Trim());

			if (iRating < 0 || iRating > 5 || 0 == sSuggestions.Length) return false;

			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			// now create the entry
            Director director = new Director(CurrentUser.dbPeopleID);

			return director.SubmitFeedback(iRating, sSuggestions);
		}
	}
}
