﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using MSA = Microsoft.Security.Application;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class CountryAdmin : BasePage
    {
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Response.Expires = 0;
				Response.Cache.SetNoStore();
				Response.AppendHeader("Pragma", "no-cache");

				string pageType = Request.QueryString["type"].ToString();
				if (pageType == "add")
				{
					string regionID = Request.QueryString["regionID"].ToString();
					if (regionID.Length > 0)
					{
						int newRegionID = Convert.ToInt32(regionID.Replace("R", ""));
						ASPxHiddenFieldCountry.Set("regionID", newRegionID);
						ASPxHiddenFieldCountry.Set("countryID", 0);
					}
				}
				else
				{
					string countryID = Request.QueryString["countryid"].ToString();
					if (countryID.Length > 0)
					{
						getCountry(countryID);
						int newCountryID = Convert.ToInt32(countryID.Replace("C", ""));
						ASPxHiddenFieldCountry.Set("countryID", newCountryID);
					}
				}
				setTitlePage(pageType);
				ASPxHiddenFieldCountry.Add("pageType", pageType);
			}
		}

		#region WebMethods

        /// <summary>
        /// Method to create / edit a country
        /// E.Parker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		protected void EditCountry(object sender, EventArgs e)
		{
			LocationCountry country = new LocationCountry();

			country.RegionID = Convert.ToInt32(ASPxHiddenFieldCountry.Get("regionID"));
			country.Name = MSA.Encoder.HtmlEncode(txtNameCountryEdit.Text);
			country.CostCentreCode = MSA.Encoder.HtmlEncode(txtCostCentreCountryEdit.Text);
			country.CountryID = Convert.ToInt32(ASPxHiddenFieldCountry.Get("countryID"));
			country.Enabled = Convert.ToBoolean(ddlnewIDCountry.SelectedValue);
			country.PassOfficeID = POCountryEdit.PassOfficeID;

			if (0 == country.PassOfficeID)
			{
				loadJQuery(false, "not valid");
				return;
			}

			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);

			bool isNew = (ASPxHiddenFieldCountry.Get("pageType").ToString() == "add");
			bool success = director.CreateCountry(country, isNew);

			loadJQuery(success, "valid");
		}
        #endregion

        /// <summary>
        /// method to get the country from the director
        /// E.Parker 
        /// 12.04.11
        /// </summary>
        /// <param name="countryID"></param>
            public void getCountry(object countryID)
            {

                //first remove the country nonsense
                int newCountryID = Convert.ToInt32(Convert.ToString(countryID).Replace("C",""));

                // new instance of the director

                LocationCountry country = new LocationCountry();
                country = Director.GetCountry(newCountryID);
                DataBind(country);
            }

       /// <summary>
       /// method to bind to the page controls 
       /// E.Parker
       /// 12.04.11
       /// </summary>
       /// <param name="country"></param>
        public void DataBind(LocationCountry country)
        {
            //now bind to the page controls
            txtNameCountryEdit.Text = country.Name;
            txtCostCentreCountryEdit.Text = country.CostCentreCode;
            ddlnewIDCountry.SelectedValue = Convert.ToString(country.Enabled).ToLower();
            
            POCountryEdit.PassOfficeID = country.PassOfficeID;
            ASPxHiddenFieldCountry.Add("regionID", country.RegionID);
        }



        /// <summary>
        /// method to set the page type depending on the value passed in
        /// E.Parker 12.04.11
        /// </summary>
        /// <param name="pageType"></param>
        public void setTitlePage(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            
            switch (pageType)
            {
                case "add":
                    countryTitle.Text = resourceManager.GetString("AddNewCountry");
                    ButtonCountry.Text = resourceManager.GetString("Save");
                    break;
                case "edit":
                    countryTitle.Text = resourceManager.GetString("EditCountry");
                    ButtonCountry.Text = resourceManager.GetString("Save");
                    break;
                default:
                    countryTitle.Text = "";
                    ButtonCountry.Text = resourceManager.GetString("AddCountry");
                    break;
            }
        }


        private void loadJQuery(bool results, string validation)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results, validation), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results, validation), true);
            }
        }


        private string getjQueryCode(bool results, string validation)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditCountryRequest(true, 'yes');");
            }
            else if (validation == "not valid")
            {
                sb.AppendLine("OnEditCountryRequest(false, 'no');");
            }
            else
            {
                sb.AppendLine("OnEditCountryRequest(false, 'yes');");
            }


            sb.AppendLine(" });");
            return sb.ToString();
        }
    }
}