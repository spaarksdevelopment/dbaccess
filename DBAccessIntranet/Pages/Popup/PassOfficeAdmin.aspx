﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassOfficeAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.PassOfficeAdmin" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="~/Controls/Selectors/CountrySelector.ascx" TagName="CountrySelector" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Selectors/CitySelector.ascx" TagName="CitySelector" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Literal ID="Literal6" Text="<%$Resources:CommonResource, PassOfficeAdmin%>" runat="server"></asp:Literal></title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js"></script>


    <script type="text/javascript" language="javascript">
        //flag to avoid button doubleclick postback.
        var isPostbackInitiated = false;
        var validationFailedControls = new Array();
        var index = 0;

        function Cancel() {
            window.parent.CancelPassOffice();
        }

        //validate whether add/edit pass office have a name and a country.
        function Validate(s, e) {

            if (!IsNameValid()) {

                AddToValidationFailedControls("Name");
            }
            if (!IsCountrySelectionValid()) {

                AddToValidationFailedControls("Country");
            }

            if (!IsFormValid()) {
                //call parent method to show popup and stop postbacking.
                window.parent.showPopupValidationError(validationFailedControls);
                e.processOnServer = false;
            }
            else {
                //avoiding button doubleclick postbacking.
                e.processOnServer = !isPostbackInitiated;
                isPostbackInitiated = true;
            }
        }

        function IsNameValid() {
            if ($("#txtName").val() == '') {
                return false;
            }
            else {
                return true;
            }
        }

        function IsCountrySelectionValid() {
            if ((parseInt($('#CountryDropDownList').val()) > 0)) {
                return true;
            }
            else {
                return false;
            }
        }

        function AddToValidationFailedControls(control) {

            validationFailedControls[index] = control;
            index++;
        }

        function IsFormValid() {

            if (validationFailedControls.length > 0) {
                return false;
            }
            else {
                return true;
            }
        }

        function OnEditPassOfficeRequest(result) {
            //cancel and refresh the grid
            if (!result) {
                window.parent.ErrorPOShowPopup();
            }
            else {
                window.parent.RefreshPOGrid();
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <h2>
            <asp:Literal ID="poTitle" runat="server" /></h2>
        <table width="70%">
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Name" Text="<%$ Resources:CommonResource, Name %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>

                    <asp:TextBox Width="100%" ID="txtName" runat="server" ClientIDMode="Static" />

                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Country" Text="<%$ Resources:CommonResource, Country %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <uc1:CountrySelector ID="ctrlCountry" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="City" Text="<%$ Resources:CommonResource, City %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <uc2:CitySelector ID="ctrlCity" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal1" Text="<%$ Resources:CommonResource, Code %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtCode" runat="server" ClientIDMode="Static" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal2" Text="<%$ Resources:CommonResource, Address %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtAddress" runat="server" ClientIDMode="Static" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal3" Text="<%$ Resources:CommonResource, EmailLabel %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtEmail" runat="server" ClientIDMode="Static" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal4" Text="<%$ Resources:CommonResource, TelephoneLiteral %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtTelephone" runat="server" ClientIDMode="Static" />
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal7" Text="<%$ Resources:CommonResource, EmailEnabledLabel %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEmailEnabledPOEdit" runat="server" ClientIDMode="Static">
                        <asp:ListItem Text="<%$Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:CommonResource, No%>" Value="false" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Literal ID="Literal5" Text="<%$ Resources:CommonResource, Enabled %>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEnabledPOEdit" runat="server" ClientIDMode="Static">
                        <asp:ListItem Text="<%$Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <div style="float: left;">
            <div style="float: left; margin: 5px" id="div11">
                <dx:ASPxButton ID="ASPxButPO" runat="server" Width="150px" ClientInstanceName="SaveButton" OnClick="AddPassOffice">
                    <ClientSideEvents Click="Validate" />
                </dx:ASPxButton>
            </div>
            <div style="float: left; margin: 5px" id="div12">
                <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="False" Text="<%$ Resources:CommonResource, Cancel %>" ForeColor="#0098DB" Width="150px">
                    <ClientSideEvents Click="Cancel" />
                </dx:ASPxButton>
            </div>
        </div>
        <!-- hiddenfields-->
        <dx:ASPxHiddenField ID="ASPxHiddenFieldPO" ClientInstanceName="hdPO" runat="server">
        </dx:ASPxHiddenField>
    </form>
</body>
</html>

