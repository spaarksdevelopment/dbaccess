﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.Feedback" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Deutsche Bank - Feedback</title>

	<!-- make sure intelli-sense works at design time -->
	<div runat="server">
		<% if (DesignMode) { %>
			<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
			<script type="text/javascript" src="/Scripts/ASPxScriptIntelliSense.js"></script>
		<% } else { %>
			<script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" ></script>
		<% } %>
	</div>

	<style type="text/css">

		form
		{
			height: 722px;
			background-color: White;
		}

	</style>

	<script type="text/javascript" language="javascript">

		function Send(s, e)
		{
			var ctl_rblRating = ASPxClientRadioButtonList.Cast("rblRating");
			var ctl_memoSuggestions = ASPxClientMemo.Cast("memoSuggestions");

			var iRating = ctl_rblRating.GetValue();
			var sSuggestions = ctl_memoSuggestions.GetText();

			if (null == iRating) iRating = 0;
			if (null == sSuggestions) sSuggestions = "";

			PageMethods.Send(iRating, sSuggestions, OnSend);
		}

		function Close(bClear)
		{
			window.parent.ClosePopup_Feedback(bClear);
		}

		function OnSend(result)
		{
			if (!result)
			{
				popupAlertTemplate.SetHeaderText("Header Text");
				popupAlertTemplateContent.SetText("Please provide your opinion and comments.");
				popupAlertTemplate.Show();
				return;
			}
			Close(true);
		}

	// ]]>
	</script>

</head>
<body>

	<form id="formFeedback" runat="server">
		<div id="divFeedback">
			<act:ToolkitScriptManager ID="ToolkitScriptManagerFeedback" runat="server" EnablePageMethods="true">
			</act:ToolkitScriptManager>

			<div id="topStage">
				<a id="clickLogo" href="#" title="" rel="logo"></a>
				<a href="/Default.aspx" target="_self" class="headerProjectName" title="">
					<asp:Image ID="headerImg" runat="server" ImageUrl="~/App_Themes/DBIntranet2010/img/name_db.gif" AlternateText="" />
				</a>
				<a href="/Pages/Home.aspx" target="_self" class="headerApplicationTitle" title="" runat="server" id="aTitle"></a>
			</div>

			<hr />

			<div id="wrapper">
				<h1 class="theTitle"><span>Feedback</span></h1>

				<p class="firstline">Please let us know what you think about this page.</p>

				<div class="leftX">
					<label class="small">Your opinion</label>
					<p class="small">How helpful is this page to you?</p>
				</div>
				<div class="rightX">
					<dx:ASPxRadioButtonList ID="ASPxRadioButtonListRating" runat="server" ClientInstanceName="rblRating" EncodeHtml="false" RepeatLayout="Flow">
						<Items>
							<dx:ListEditItem Value="1" ImageUrl="~/Images/ic_vote1.gif" Text="very good" />
							<dx:ListEditItem Value="2" ImageUrl="~/Images/ic_vote2.gif" Text="good" />
							<dx:ListEditItem Value="3" ImageUrl="~/Images/ic_vote3.gif" Text="average" />
							<dx:ListEditItem Value="4" ImageUrl="~/Images/ic_vote4.gif" Text="poor" />
							<dx:ListEditItem Value="5" ImageUrl="~/Images/ic_vote5.gif" Text="very poor" />
						</Items>
					</dx:ASPxRadioButtonList>
				</div>
				<br />
				<br />
				<div class="leftX">
					<label for="textInput" class="small">Your comments</label>
					<p class="small">Are there any suggestions for improvement you would like to make?</p>
				</div>
				<div class="rightX">
					<dx:ASPxMemo ID="MemoSuggestions" runat="server" ClientInstanceName="memoSuggestions" Rows="8" Width="100%"></dx:ASPxMemo>
				</div>
				<br />
				<div id="send">
					<dx:ASPxButton ID="ButtonSend" runat="server" AutoPostBack="false" Text="Send" CssClass="inline">
						<ClientSideEvents Click="function(s,e){ Send(s, e); }" />
					</dx:ASPxButton>
				</div>
				<p class="note">
					If you prefer to contact us personally, feel free to use our 
					<a href="http://www.db.com/en/content/contact_form.htm" onclick="openWin(this.href,803,550,0,0,0,0,1,1); return false;">[contact form]</a>.
					We  ensure a prompt reply.
				</p>
			</div>

			<hr style="clear: both;" />

			<div id="copy">
				<p>Copyright &copy; Deutsche Bank AG</p>
			</div>

			<div id="divClose">
				<img src="../../Images/bar_shadow.gif" alt="Divider" />
				<a href="javascript:Close(false);">close</a>
			</div>
		</div>

		<ppc:Popups ID="PopupTemplates" runat="server" />

	</form>

</body>
</html>
