﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxHiddenField;
using System.Text;
using MSA = Microsoft.Security.Application;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class CityAdmin : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsCallback && !IsPostBack)
            {
                string pageType = Request.QueryString["type"].ToString();
                if (pageType == "add")
                {
                    string countryID = Request.QueryString["countryID"].ToString();
                    if (countryID.Length > 0)
                    {
                        int newCountryID = Convert.ToInt32(countryID.Replace("C", ""));
                        ASPxHiddenFieldCity.Add("CountryID", newCountryID);
                        ASPxHiddenFieldCity.Set("CityID", 0);
                    }
                }
                else
                {
                    string cityID = Request.QueryString["cityID"].ToString();
                    if (cityID.Length > 0)
                    {
                        getCity(cityID);
                    }
                    ASPxHiddenFieldCity.Set("CityID", Convert.ToInt32(cityID.Replace("Ci","")));
                }
                ASPxHiddenFieldCity.Add("pageType", pageType);
               
                setTitlePage(pageType);
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");               
            }
        }

        /// <summary>
        /// method to create new city 
        /// E.Parker 
        /// 13.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addCity(object sender, EventArgs e)
        {
            DBAccessController.DAL.LocationCity newCity = new DBAccessController.DAL.LocationCity();
			newCity.Name = MSA.Encoder.HtmlEncode(txtEditCityName.Text);
            newCity.CountryID = Convert.ToInt32(ASPxHiddenFieldCity.Get("CountryID"));
            
            if (tzEditCity.TimeZoneID == null)
            {
                newCity.TimeZoneID = Convert.ToInt32(ASPxHiddenFieldCity.Get("TimeZoneID"));
            }
            else
            {
                 newCity.TimeZoneID = Convert.ToInt32(tzEditCity.TimeZoneID);
            }
            newCity.CityID = Convert.ToInt32(ASPxHiddenFieldCity.Get("CityID"));
            if (ddlEditCityEnabled.SelectedIndex == 0)
            {
                newCity.Enabled = true;
            }
            else
            {
                newCity.Enabled = false;
            }
            bool isNew;

            if (ASPxHiddenFieldCity.Get("pageType").ToString() == "add")
            {
                isNew = true;
            }
            else
            {
                isNew = false;               
            }
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateCity(newCity, isNew);
            loadJQuery(success);            
        }

        /// <summary>
        /// director call method to retrieve the city details from the db
        /// E.Parker 12.04.11
        /// </summary>
        /// <param name="cityID"></param>
        public void getCity(string cityID)
        {
            //remove the extra text from the id
            int newCityId = Convert.ToInt32(cityID.Replace("Ci", ""));
            //    //now call the director to return a list of 1 city with that id
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(CurrentUser.dbPeopleID);
            int dbPeopleID = CurrentUser.dbPeopleID;

            DBAccessController.DAL.LocationCity editCity = Director.GetCity(newCityId);

            BindData(editCity);
  
        }

        /// <summary>
        /// method to bind data to the page for the edit city
        /// E.Parker 12.04.11
        /// </summary>
        /// <param name="city"></param>
        public void BindData(DBAccessController.DAL.LocationCity city)
        {
            txtEditCityName.Text = city.Name;
            if (city.Enabled == true)
            {
                ddlEditCityEnabled.SelectedIndex = 0;
            }
            else
            {
                ddlEditCityEnabled.SelectedIndex = 1;
            }
            tzEditCity.TimeZoneID = city.TimeZoneID;
            ASPxHiddenFieldCity.Add("CountryID", city.CountryID);
            ASPxHiddenFieldCity.Set("TimeZoneID", city.TimeZoneID);
        }



        /// <summary>
        /// method to set the page type depending on the value passed in
        /// E.Parker 12.04.11
        /// </summary>
        /// <param name="pageType"></param>
        public void setTitlePage(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

            switch (pageType)
            {
                case "add":
                    cityTitle.Text =resourceManager.GetString("AddNewCity");// "Add New City";
                    break;
                case "edit":
                    cityTitle.Text = resourceManager.GetString("EditCity");
                    break;
                default:
                    cityTitle.Text = "";
                    break;
            }
        }

        #region jQueryCallMethods
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }


        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditCityRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditCityRequest(false);");
            }


            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion

    }
}
