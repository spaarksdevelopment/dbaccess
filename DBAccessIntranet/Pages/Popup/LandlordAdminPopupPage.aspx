﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LandlordAdminPopupPage.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.LandlordAdminPopupPage" %>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="Literal6" Text="<%$Resources:CommonResource, LandlordAdmin%>" runat="server"></asp:Literal></title>
     <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
     <script type="text/javascript" language="javascript">
         function OnEditLandlordRequest(success) {
             if (!success) {
                 window.parent.ErrorShowPopup();
             }
             else {
                 window.parent.RefreshLandlordGrid();
             }
         }
         function Cancel() {
             window.parent.CancelEditLandlord();
         }
</script>
</head>
<body>
<form runat="server">
<h2><asp:Literal id="landlordTitle" runat="server" /></h2>

<table>
    <tr>
        <td>
            <p><asp:Literal ID="Name" Text="<%$Resources:CommonResource, Name%>" runat="server"></asp:Literal></p>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtName" runat="server" Width="240px" />
        </td>
        <td>
            <div id="nameHelpInfo" class="HelpPopUp">                      

            </div>
        </td>
  </tr>
  <tr>    
        <td>
            <p><asp:Literal ID="Literal1" Text="<%$Resources:CommonResource, Address%>" runat="server"></asp:Literal></p>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtAddress" runat="server" Width="240px" />
        </td>
        <td>
            <div id="addressHelpInfo" class="HelpPopUp">                      

            </div>
        </td>
    </tr>
    <tr>
        <td>
            <p><asp:Literal ID="Literal2" Text="<%$Resources:CommonResource, TelephoneLiteral%>" runat="server"></asp:Literal></p>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtTelephone" runat="server" Width="240px" />
        </td>
        <td>
            <div id="telephoneHelpInfo" class="HelpPopUp">                      

            </div>
        </td>
    </tr>
    <tr>
        <td>
            <p><asp:Literal ID="Literal3" Text="<%$Resources:CommonResource, EmailLabel%>" runat="server"></asp:Literal> </p>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtEmail" runat="server" Width="240px" />
        </td>
        <td>
            <div id="emailHelpInfo" class="HelpPopUp">                      

            </div>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <p><asp:Literal ID="Literal4" Text="<%$Resources:CommonResource,  VisitorRequirements%>" runat="server"></asp:Literal> </p>
        </td>
        <td>
            <dx:ASPxCheckBox ID="chkPhoto" runat="server" Text="<%$Resources:CommonResource, Photo%>"  /> <br />
            <dx:ASPxCheckBox ID="chkMiFair" runat="server" Text="<%$Resources:CommonResource, MiFairNumber%>"   /><br />
            <dx:ASPxCheckBox ID="chkEmail" runat="server" Text="<%$Resources:CommonResource, EmailLabel%>" /><br />
        </td>
        <td>
            <div id="visitorHelpInfo" class="HelpPopUp">                      

            </div>
        </td>
    </tr>
    <tr>
        <td>
            <dx:ASPxButton ID="butLandlord" runat="server" OnClick="addLandlord" Width="150"/>
        </td>
        <td colspan="2">            
            <dx:ASPxButton ID="ASPxButton4" runat="server" AutoPostBack="false" Text="<%$Resources:CommonResource, Cancel%>" ForeColor="#0098db" Width="150" style="margin: 5px;">                    
                <ClientSideEvents Click="Cancel" />
            </dx:ASPxButton>
        </td>
    </tr>
</table>

<!--hidden fields-->
    <dx:ASPxHiddenField ID="ASPxHiddenFieldLandlord" ClientInstanceName="hdLandlord" runat="server">
    </dx:ASPxHiddenField>
</form>
</body>
</html>
