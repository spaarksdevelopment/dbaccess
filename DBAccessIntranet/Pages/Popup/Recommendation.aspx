﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recommendation.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.Recommendation" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Deutsche Bank - Recommendation</title>

	<!-- make sure intelli-sense works at design time -->
	<div runat="server">
		<% if (DesignMode) { %>
			<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
			<script type="text/javascript" src="/Scripts/ASPxScriptIntelliSense.js"></script>
		<% } else { %>
			<script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" ></script>
		<% } %>
	</div>

	<style type="text/css">

		form
		{
			height: 512px;
			background-color: White;
		}

	</style>

	<script type="text/javascript" language="javascript">

		function Send(s, e)
		{
			var ctl_memoMessage = ASPxClientMemo.Cast("memoMessage");
			var ctl_tbEmailAddress = ASPxClientTextBox.Cast("tbEmailAddress");

			var sMessage = ctl_memoMessage.GetText();
			var sFilePath = window.parent.GetCurrentFilePath();
			var sEmailAddress = ctl_tbEmailAddress.GetText();

			if (null == sMessage) sMessage = "";
			if (null == sFilePath) sFilePath = "";
			if (null == sEmailAddress) sEmailAddress = "";

			PageMethods.Send(sMessage, sFilePath, sEmailAddress, OnSend);
		}

		function Close(bClear)
		{
			window.parent.ClosePopup_Recommendation(bClear);
		}

		function OnSend(result)
		{
			if (!result)
			{
				popupAlertTemplate.SetHeaderText("Header Text");
				popupAlertTemplateContent.SetText("Please provide valid recipient and sender email addresses.");
				popupAlertTemplate.Show();
				return;
			}
			Close(true);
		}

	// ]]>
	</script>

</head>
<body>

	<form id="formRecommendation" runat="server">
		<div id="divRecommendation">
			<act:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
			</act:ToolkitScriptManager>

			<div id="topStage">
				<a id="clickLogo" href="#" title="" rel="logo"></a>
				<a href="/Default.aspx" target="_self" class="headerProjectName" title="">
					<asp:Image ID="headerImg" runat="server" ImageUrl="~/App_Themes/DBIntranet2010/img/name_db.gif" AlternateText="" />
				</a>
				<a href="/Pages/Home.aspx" target="_self" class="headerApplicationTitle" title="" runat="server" id="aTitle"></a>
			</div>

			<hr />

			<div id="wrapper">
				<h1 class="theTitle"><span>Recommend this page</span></h1>

				<p class="firstline">I would like to recommend this page.</p>

				<div class="leftX">
					<label class="small">Recipient</label>
					<p class="small">e-mail</p>
				</div>
				<div class="rightX" style="padding-top: 2.3em;">
					<dx:ASPxTextBox ID="ASPxTextBoxEmailAddress" runat="server" ClientInstanceName="tbEmailAddress"
						Width="100%"
						Text="name@provider.com">
					</dx:ASPxTextBox>
				</div>
				<div class="leftX">
					<label for="textInput" class="small">Message</label>
				</div>
				<div class="rightX">
					<dx:ASPxMemo ID="MemoMessage" runat="server" ClientInstanceName="memoMessage" Rows="6" Width="100%"></dx:ASPxMemo>
				</div>
				<br />
				<div id="send">
					<dx:ASPxButton ID="ButtonSend" runat="server" AutoPostBack="false" Text="Send" CssClass="inline">
						<ClientSideEvents Click="function(s,e){ Send(s, e); }" />
					</dx:ASPxButton>
				</div>
				<br />
			</div>

			<hr style="clear: both;" />

			<div id="copy">
				<p>Copyright &copy; Deutsche Bank AG</p>
			</div>

			<div id="divClose">
				<img src="../../Images/bar_shadow.gif" alt="Divider" />
				<a href="javascript:Close(false);">close</a>
			</div>
		</div>

		<ppc:Popups ID="PopupTemplates" runat="server" />

	</form>

</body>
</html>
