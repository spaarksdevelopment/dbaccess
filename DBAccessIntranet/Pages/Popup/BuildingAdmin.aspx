﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuildingAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.BuildingAdmin" %>

<%@ Register Src="~/Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Selectors/VisitorManagementControlSystemSelector.ascx" TagName="VMSelector" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/Selectors/LandlordSelector.ascx" TagName="LandlordSelector" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Selectors/DBMovesLocationSelector.ascx" TagName="DBMovesLocationSelector" TagPrefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="Literal2" meta:resourcekey="BuildingAdmin"  runat="server"> </asp:Literal>  </title>
     <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
     <script type="text/javascript" language="javascript">
         function Cancel() {
             window.parent.CancelEditBuilding();
         }
         function OnEditBuildingRequest(result) {
             if (!result) {
                 window.parent.ErrorShowPopup('building') 
             }
             else {
                 window.parent.refreshTree('building');
             }
         }
     </script>
</head>
<body>
    <form id="form1" runat="server">
         <h2><asp:Literal ID="buildingTitle" runat="server" /></h2>
        <table width="70%">
            <tr>            
                <td>
                    <p> <asp:Literal Text="<%$ Resources:CommonResource, Name%>" runat="server"> </asp:Literal> 
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtBuildingName" runat="server" ClientIDMode="Static" />
                </td>
                 <td>
                    <div id="buildingNameHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            <tr>            
                <td>
                    <p><asp:Literal ID="Literal1" meta:resourcekey="Code"   runat="server"> </asp:Literal>  
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtCode" runat="server" ClientIDMode="Static" />
                </td>
                 <td>
                    <div id="buildingCodeHelpInfo" class="HelpPopUp">
                    </div>
                </td>
            </tr>
            <tr>            
                <td>
                    <p><asp:Literal ID="StreetAddress" meta:resourcekey="StreetAddress"   runat="server"> </asp:Literal>    
                    </p>
                </td>
                <td>
                    <asp:TextBox Width="100%" ID="txtStreetAddress" runat="server" ClientIDMode="Static" />
                </td>
                 <td>
                    <div id="buildingSAHelpInfo" class="HelpPopUp">            
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p><asp:Literal ID="Literal3" Text="<%$ Resources:CommonResource, Enabled%>" runat="server"> </asp:Literal>  
                    </p>
                </td>
                <td>
                     <asp:DropDownList ID="ddlEnabledBuildEdit" runat="server"  ClientIDMode="Static">
                            <asp:ListItem Text="<%$ Resources:CommonResource, Yes%>" Value="true"  />
                            <asp:ListItem Text="<%$ Resources:CommonResource, No%>" Value="false"  />
                        </asp:DropDownList>
                </td>
                 <td>
                    <div id="buildingEnabledHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p><asp:Literal ID="PassOffice" Text="<%$ Resources:CommonResource, PassOffice%>" runat="server"> </asp:Literal>  
                    </p>
                </td>
                <td>
                        <uc2:PassOfficeSelector id="ctrlPassOffice" runat="server" />
                </td>
                 <td>
                    <div id="buildingPassOfficeHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p><asp:Literal ID="VisitorMgt" meta:resourcekey="VisitorMgt"   runat="server"> </asp:Literal>     
                    </p>
                </td>
                <td>
                     <uc3:VMSelector id="vmSelectorManagement" runat="server" />
                </td>
                 <td>
                    <div id="buildingVisitorHelpInfo" class="HelpPopUp">                                             
                    </div>
                </td>
            </tr>
            
            <tr class="landlordInfo">
                <td>
                    <p><asp:Literal ID="Landlord" meta:resourcekey="Landlord"   runat="server"> </asp:Literal>  
                    </p>
                </td>
                <td>
                     <uc4:LandlordSelector ID="landlordSelector1" runat="server" />
                </td>
                 <td>
                    <div id="landlordHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    <p><asp:Literal ID="DBMovesLocation" meta:resourcekey="DBMovesLocation"   runat="server"> </asp:Literal>  </p>
                </td>
                <td>
                    <uc5:DBMovesLocationSelector ID="DBMovesLocationSelector1" runat="server" ClientIDMode="Static" />                       
                </td>
                <td>
                    <div id="dbMovesLocationHelpInfo" class="HelpPopUp">
					</div>
                </td>
            </tr>
        </table>
		<br />
        <div style="float:left;">
                <div style="float: left; margin: 5px" id="div11">
                    <dx:ASPxButton ID="ASPxButBuilding" runat="server" AutoPostBack="True" Width="150" OnClick="AddBuilding">                
                    </dx:ASPxButton>              
                </div>
                <div style="float: left; margin: 5px" id="div12">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>" ForeColor="#0098db" Width="150">                    
                         <ClientSideEvents Click="Cancel" />
                    </dx:ASPxButton>
                </div>
            </div>   
            <!-- hiddenfields-->
            <dx:ASPxHiddenField ID="ASPxHiddenFieldBuilding" ClientInstanceName="hdBuilding" runat="server">
            </dx:ASPxHiddenField>
    </form>
</body>
</html>
