﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Text;
using MSA = Microsoft.Security.Application;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class FloorAdmin : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string pageType = Request.QueryString["type"].ToString();

                //Use this to filter the list of DB Moves Floors by DB Moves location (ie Building)
                int? dbMovesLocationID = null;

                if (pageType == "add")
                {
                    string buildingID = Request.QueryString["BuildingID"].ToString();
                    int newBuildingID = Convert.ToInt32(buildingID.Replace("B",""));
                    ASPxHiddenFieldFloor.Set("buildingID", newBuildingID);
                    ASPxHiddenFieldFloor.Set("floorID", 0);

                    dbMovesLocationID = Director.GetDBMovesLocationIDByBuildingID(newBuildingID);
                }
                else
                {
                    string floorID = Request.QueryString["floorID"].ToString();
                    int newFloorID = Convert.ToInt32(floorID.Replace("F", ""));
                    ASPxHiddenFieldFloor.Set("floorID", newFloorID);
                    ASPxHiddenFieldFloor.Set("buildingID", 0);
                    GetData(floorID);

                    dbMovesLocationID = Director.GetDBMovesLocationIDByFloorID(newFloorID);
                }

                DBMovesFloorSelector1.DBMovesLocationID = dbMovesLocationID;

                setPageText(pageType);
                ASPxHiddenFieldFloor.Add("pageType", pageType);
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
            }
        }


        #region WebMethods

        /// <summary>
        /// Method to add /edit floor
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addFloor(object sender, EventArgs e)
        {           
            DBAccessController.DAL.LocationFloor floor = new DBAccessController.DAL.LocationFloor();
            floor.BuildingID = Convert.ToInt32(ASPxHiddenFieldFloor.Get("buildingID"));
            floor.FloorID = Convert.ToInt32(ASPxHiddenFieldFloor.Get("floorID"));
			floor.Name = MSA.Encoder.HtmlEncode(txtFloorName.Text);
            floor.Enabled = Convert.ToBoolean(ddlEnabledFloorEdit.SelectedValue);       

            bool isNew;
            if (ASPxHiddenFieldFloor.Get("pageType").ToString() == "add")
            {
                isNew = true;
            }
            else
            {
                isNew = false;
            }
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateFloor(floor, isNew);
            loadJQuery(success);
        }
        #endregion
        #region DataBind
        /// <summary>
        /// method to bind the returned floor data to the page  controls
        /// E.Parker 14.04.11
        /// </summary>
        /// <param name="floorID"></param>
        void GetData(string floorID)
        {
            //convert to acutal ID
            int newFloorID = Convert.ToInt32(floorID.Replace("F", ""));
            DBAccessController.DAL.LocationFloor floor = Director.GetFloor(newFloorID);
            txtFloorName.Text = floor.Name;
            ddlEnabledFloorEdit.SelectedValue = Convert.ToString(floor.Enabled).ToLower();            
        }

        /// <summary>
        /// method to set the text for the page title and the button text
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="pageType"></param>
        void setPageText(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            switch (pageType)
            { 
                case "add":
                    floorTitle.Text =resourceManager.GetString("AddFloor");
                    ASPxButFloor.Text = resourceManager.GetString("Save");
                    break;
                case "edit":
                    floorTitle.Text = resourceManager.GetString("EditFloor");
                    ASPxButFloor.Text = resourceManager.GetString("Save");
                    break;               
            }
        }
        #endregion


        #region jQueryCallMethods
        /// <summary>
        /// Method to bind jQuery to page
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="results"></param>
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }

        /// <summary>
        /// Method to create jQuery code
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditFloorRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditFloorRequest(false);");
            }
            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion

    }
}