﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Text;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class AddEditSmartCardType : BasePage
	{

		#region Properties
		private int TypeId
		{
			get
			{
				if (Request.QueryString["ID"] != null && Convert.ToInt32(Request.QueryString["ID"]) > 0)
				{
					return Convert.ToInt32(Request.QueryString["ID"]);
				}
				else
				{
					return 0;
				}
			}
		}
		#endregion

        #region
        protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				Populate();

				ASPxDelete.Visible = ShowDeleteButton();
			}
		}

        protected void ValidateUniqueIdentifier(object sender, ServerValidateEventArgs args)
        {
            if (string.IsNullOrEmpty(txtKeyIdentifier.Text))
            {
                args.IsValid = true;
                return;
            }

            args.IsValid = Director.IsTypeNameUnique(TypeId, txtKeyIdentifier.Text);
        }
		
		protected void SaveSmartCardType(object sender, EventArgs e)
		{
            if (!Page.IsValid)
                return;

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);

            bool success = true;

			if (TypeId > 0)
                success = director.UpdateSmartCardType(txtboxSmartCardType.Text.Trim(), txtKeyIdentifier.Text.Trim(), ckbEnabled.Checked, Convert.ToInt32(ddVendor.SelectedValue), txtboxSmartcardLabel.Text.Trim(), TypeId);
			else
                success = director.UpdateSmartCardType(txtboxSmartCardType.Text.Trim(), txtKeyIdentifier.Text.Trim(), ckbEnabled.Checked, Convert.ToInt32(ddVendor.SelectedValue), txtboxSmartcardLabel.Text.Trim());
			
            LoadJQuery(GetJQueryCode(success));		
		}
        #endregion

		#region helper methods
		/// <summary>
		/// Load the controls and labels on the page accordingly for edit or Add
		/// </summary>
		private void Populate()
		{
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			if (TypeId > 0)
			{
                divisionTitle.Text = resourceManager.GetString("EditType");
				literalAddVendorInfo.Visible = false;
				
                literalEditSmartCardType.Visible = true;
				literalAddSmartCardType.Visible = false;

                literalEditSmartcardLabel.Visible = true;
                literalAddSmartcardLabel.Visible = false;
                
                literalEditSmartcardTypeIdentifier.Visible = true;
				literalAddSmartcardTypeIdentifier.Visible = false;
				ddVendor.Enabled = false;
			}
			else
			{
                divisionTitle.Text = resourceManager.GetString("AddType");
				literalAddVendorInfo.Visible = true;
				
                literalEditSmartCardType.Visible = false;
				literalAddSmartCardType.Visible = true;

                literalEditSmartcardLabel.Visible = false;
                literalAddSmartcardLabel.Visible = true;
				
                literalEditSmartcardTypeIdentifier.Visible = false;
                literalAddSmartcardTypeIdentifier.Visible = true;
				
                ckbEnabled.Checked = true;
			}
			PopulateData();
		}

		/// <summary>
		/// 
		/// </summary>
		private void PopulateData()
		{
			try
			{
				if (TypeId > 0)
				{
					vw_SmartcardTypeDetails smartcardTypes = Director.GetSmartCardTypes(true).Where(a => a.ID == TypeId).FirstOrDefault();
					if (smartcardTypes != null)
					{
						ddVendor.DataSource = Director.GetSmartCardProviders(false).ToList().Where(a => a.Enabled == true);
						ddVendor.DataBind();
                        ddVendor.SelectedValue = smartcardTypes.SmartCardProviderID.ToString();
						
                        txtboxSmartCardType.Text = smartcardTypes.Type;
						txtKeyIdentifier.Text = smartcardTypes.TypeIdentifier;
                        txtboxSmartcardLabel.Text = smartcardTypes.Label; 
						ckbEnabled.Checked = smartcardTypes.Enabled;
					}
				}
				else
				{
					ddVendor.DataSource = Director.GetSmartCardProviders(false).ToList().Where(a => a.Enabled == true);
					ddVendor.DataBind();
				}
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}

        private void LoadJQuery(string script)
		{
			ScriptManager requestSM = ScriptManager.GetCurrent(this);

			if (requestSM != null && requestSM.IsInAsyncPostBack)
			{
				ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), script, true);
			}
			else
			{
				ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), script, true);
			}
		}

		private String GetJQueryCode(Boolean bResults)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("$(document).ready(function() { ");
			sb.AppendFormat("OnAddEditSmartCardProviderRequest({0});", bResults.ToString().ToLower());
			sb.AppendLine(" });");

			return sb.ToString();
		}

		private bool ShowDeleteButton()
		{
			if (TypeId == 0)
				return false;

			try
			{
				return Director.GetCanDeleteSmartcardType(TypeId);
			}
			catch
			{
				return false;
			}
		}
	#endregion
	}
}