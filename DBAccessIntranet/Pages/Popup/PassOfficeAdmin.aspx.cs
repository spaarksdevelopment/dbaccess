﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Text;
using MSA = Microsoft.Security.Application;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class PassOfficeAdmin : BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ctrlCountry.CountryChanged += ctrlCountry_CountryChanged;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                string pageType = Request.QueryString["type"].ToString();
                ASPxHiddenFieldPO.Set("pageType", pageType);
                if (pageType == "add")
                {
                    ASPxHiddenFieldPO.Set("PassOfficeID", 0);
                }
                else
                {
                    string passOffice = Request.QueryString["pass"].ToString();
                    ASPxHiddenFieldPO.Set("PassOfficeID", Convert.ToInt32(passOffice));
                    DataBind(Convert.ToInt32(passOffice));
                }
                setPageText(pageType);
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
            }            
        }

        /// <summary>
        /// Rebind City Dropdown when Country Dropdown changes
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        protected void ctrlCountry_CountryChanged(object o, Controls.Selectors.CountryChangeEventArg e)
        {
            ctrlCity.CountryID = e.CountryID;
            ctrlCity.ReloadCities();
        }

        #region Add/Edit Methods
        /// <summary>
        /// Method to add a PO
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPassOffice(object sender, EventArgs e)
        {
            if (IsRequestValid())
            {               
                DBAccessController.DAL.LocationPassOffice newPassOffice = new DBAccessController.DAL.LocationPassOffice();
                newPassOffice.PassOfficeID = Convert.ToInt32(ASPxHiddenFieldPO.Get("PassOfficeID"));
                newPassOffice.Name = MSA.Encoder.HtmlEncode(txtName.Text);
                newPassOffice.Code = MSA.Encoder.HtmlEncode(txtCode.Text);
                newPassOffice.Address = MSA.Encoder.HtmlEncode(txtAddress.Text);
                newPassOffice.Enabled = Convert.ToBoolean(ddlEnabledPOEdit.SelectedValue);
                newPassOffice.EmailEnabled = Convert.ToBoolean(ddlEmailEnabledPOEdit.SelectedValue);
                newPassOffice.Email = MSA.Encoder.HtmlEncode(txtEmail.Text);
                newPassOffice.CountryID = ctrlCountry.CountryID;
                newPassOffice.Telephone = MSA.Encoder.HtmlEncode(txtTelephone.Text);
                newPassOffice.CityID = GetSelectedCityID();
               
                bool isNew = ASPxHiddenFieldPO.Get("pageType").ToString() == "add";

                DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                Director director = new Director(CurrentUser.dbPeopleID);
                bool success = director.CreatePassOffice(newPassOffice, isNew);
                loadJQuery(success);
            }
            else
            {
                loadJQuery(false);                
            }
        }

        private bool IsRequestValid()
        {
            return (ctrlCountry.CountryID.Value > 0 && !string.IsNullOrWhiteSpace(txtName.Text));           
        }

        private int? GetSelectedCityID()
        {
            if (ctrlCity.CityID.HasValue && ctrlCity.CityID.Value > 0)
            {
                return ctrlCity.CityID;
            }
            else
            {
                return null;
            }
        }

        #endregion
        #region DataBind
        /// <summary>
        /// Method to bind the data to the page controls.
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="passOfficeID"></param>
        void DataBind(int passOfficeID)
        {
            DBAccessController.DAL.LocationPassOffice po = Director.GetPassOffice(passOfficeID);
            //now bind to the page controls
            txtAddress.Text = po.Address;
            txtCode.Text = po.Code;
            txtEmail.Text = po.Email;
            txtName.Text = po.Name;
            txtTelephone.Text = po.Telephone;
            ddlEnabledPOEdit.SelectedValue = Convert.ToString(po.Enabled).ToLower();
            ctrlCountry.CountryID = po.CountryID;
            ctrlCity.CityID = (po.CityID.HasValue) ? po.CityID : 0;
            ctrlCity.CountryID = po.CountryID;
            ddlEmailEnabledPOEdit.SelectedValue = Convert.ToString(po.EmailEnabled).ToLower();       
        }

        /// <summary>
        /// method to set the text for the page title and the button text
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="pageType"></param>
        void setPageText(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

            switch (pageType)
            {
                case "add":
                    poTitle.Text = resourceManager.GetString("AddPassOffice"); 
                    ASPxButPO.Text = resourceManager.GetString("Save");
                    break;
                case "edit":
                    poTitle.Text =resourceManager.GetString("EditPassOffice"); 
                    ASPxButPO.Text = resourceManager.GetString("Save");
                    break;
            }
        }
        #endregion

        #region jQueryCallMethods
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }


        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditPassOfficeRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditPassOfficeRequest(false);");
            }
            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion

    }
}