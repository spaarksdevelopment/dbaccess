﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Microsoft.Security.Application;

using DevExpress.Web.ASPxEditors;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
	public partial class Recommendation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
			}
		}

		/// <summary>
		/// formats a string that will show the text and the image
		/// </summary>
		/// <param name="sUrl"></param>
		/// <param name="sText"></param>
		/// <returns></returns>
		protected String GetItemTextAndImage(String sUrl, String sText)
		{
			return String.Format("<img src=\"{0}\" alt=\"{1}\" />&nbsp;&nbsp;{1}", Page.ResolveClientUrl(sUrl), sText);
		}

		[WebMethod, ScriptMethod]
		public static Boolean Send(String sMessage, String sFilePath, String sEmailAddress)
		{
			sMessage = Encoder.HtmlEncode(sMessage.Trim());
			sFilePath = Encoder.HtmlEncode(sFilePath.Trim());
			sEmailAddress = Encoder.HtmlEncode(sEmailAddress.Trim());

			if (0 == sMessage.Length || 0 == sFilePath.Length || 0 == sEmailAddress.Length) return false;

			String sExpression = @"^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$";

			if (!Regex.IsMatch(sEmailAddress, sExpression)) return false;

			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			// now create the entry
            Director director = new Director(CurrentUser.dbPeopleID);

			return director.SubmitRecommendation(sMessage, sFilePath, sEmailAddress);
		}
	}
}
