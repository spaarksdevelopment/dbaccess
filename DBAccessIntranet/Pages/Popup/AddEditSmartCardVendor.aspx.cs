﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Text;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class AddEditSmartCardVendor : BasePage
	{
		#region Properties
		private int VendorID
		{
			get
			{
				if (Request.QueryString["ID"] != null && Convert.ToInt32(Request.QueryString["ID"]) > 0)
				{
					return Convert.ToInt32(Request.QueryString["ID"]);
				}
				else
				{
					return 0;
				}
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				Populate();

				ASPxDelete.Visible = ShowDeleteButton();
			}
		}

        protected void ValidateUniqueVendorName(object source, ServerValidateEventArgs args)
        {
            if(string.IsNullOrEmpty(txtVendorName.Text))
            {
                args.IsValid= true;
                return;
            }

            args.IsValid = Director.IsVendorNameUnique(VendorID, txtVendorName.Text);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void SaveSmartCardProvider(object sender, EventArgs e)
		{
            if (!Page.IsValid)
                return;

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);

            bool success = true;

            if (VendorID > 0)
                success = director.UpdateSmartCardProvider(txtVendorName.Text.Trim(), txtKeyIdentifier.Text.Trim(), ckbUseHex.Checked, ckbEnabled.Checked, VendorID);
			else
                success = director.UpdateSmartCardProvider(txtVendorName.Text.Trim(), txtKeyIdentifier.Text.Trim(), ckbUseHex.Checked, ckbEnabled.Checked);
			
            LoadJQuery(GetJavascriptOnSave(success));		
		}

		#region helper methods
        /// <summary>
        /// Load the controls and labels on the page accordingly for edit or Add
        /// </summary>
        private void Populate()
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            if (VendorID > 0)
            {
                PopupTitle.Text = resourceManager.GetString("EditVendor");//"Edit Vendor";
                literalEditVendorNameInfo.Visible = true;
                literalAddVendorNameInfo.Visible = false;
                literalEditKeyIdentifier.Visible = true;
                literalAddKeyIdentifier.Visible = false;

                PopulateData();
            }
            else
            {
                PopupTitle.Text = resourceManager.GetString("AddVendor");
                literalEditVendorNameInfo.Visible = false;
                literalAddVendorNameInfo.Visible = true;
                literalEditKeyIdentifier.Visible = false;
                literalAddKeyIdentifier.Visible = true;
                ckbUseHex.Checked = true;
                ckbEnabled.Checked = true;
            }
        }

        private void PopulateData()
        {
            try
            {
                SmartcardProvider smartcardProviders = Director.GetSmartCardProviders(true).Where(a => a.ID == VendorID).FirstOrDefault();
                if (smartcardProviders != null)
                {
                    txtVendorName.Text = smartcardProviders.ProviderName;
                    txtKeyIdentifier.Text = smartcardProviders.CurrentPUK;
                    ckbUseHex.Checked = smartcardProviders.UseHexMifare;
                    ckbEnabled.Checked = smartcardProviders.Enabled;
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
        }

		private void LoadJQuery(string script)
		{
			ScriptManager requestSM = ScriptManager.GetCurrent(this);

			if (requestSM != null && requestSM.IsInAsyncPostBack)
			{
				ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), script, true);
			}
			else
			{
				ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), script, true);
			}
		}

		private String GetJavascriptOnSave(Boolean bResults)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("$(document).ready(function() { ");
			sb.AppendFormat("OnAddEditSmartCardProviderRequest({0});", bResults.ToString().ToLower());
			sb.AppendLine(" });");

			return sb.ToString();
		}

		private bool ShowDeleteButton()
		{
			if (VendorID == 0)
				return false;

			try
			{
				return Director.GetCanDeleteVendor(VendorID);
			}
			catch
			{
				return false;
			}
		}
		#endregion
	}
}