﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Text;
using System.Resources;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class LandlordAdminPopupPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string pageType = Request.QueryString["type"].ToString();
                if (pageType == "edit")
                {
                    string tempLandlordID = Request.QueryString["landlordID"].ToString();
                    retrieveValuesForEdit(tempLandlordID);
                    ASPxHiddenFieldLandlord.Set("LandlordID", tempLandlordID);
                }
                ASPxHiddenFieldLandlord.Set("pageType", pageType);
                setPageText(pageType);
            }
        }

        /// <summary>
        /// Method 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addLandlord(object sender, EventArgs e)
        {
            string pageType = ASPxHiddenFieldLandlord.Get("pageType").ToString();
            bool isNew = false;
            if (pageType == "add")
            {
                isNew = true;
            }

            DBAccessController.DAL.ControlLandlord landlord = new DBAccessController.DAL.ControlLandlord();

            if (pageType == "edit")
                landlord.LandlordID = Convert.ToInt32(ASPxHiddenFieldLandlord.Get("LandlordID"));

            landlord.Name = txtName.Text;
            landlord.Address = txtAddress.Text;
            landlord.Email = txtEmail.Text;
            landlord.Telephone = txtTelephone.Text;
            landlord.RequiresEmail = chkEmail.Checked;
            landlord.RequiresMiFare = chkMiFair.Checked;
            landlord.RequiresPhoto = chkPhoto.Checked;

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateEditLandlords(landlord, isNew);
            loadJQuery(success); 
        }


        /// <summary>
        /// method to show / hide various controls on the page
        /// E.Parker 
        /// 13.05.11
        /// </summary>
        /// <param name="pageType"></param>
        void setPageText(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

            switch (pageType.ToLower())
                {
                case "add":
                        butLandlord.Text =resourceManager.GetString("Save"); 
                        landlordTitle.Text = resourceManager.GetString("AddLandlord");
                        break;
                case "edit":
                        butLandlord.Text = resourceManager.GetString("Save"); 
                        landlordTitle.Text = resourceManager.GetString("EditLandlord"); 
                        break;
                }
        }

        /// <summary>
        /// method to retrieve the landlord information based on id
        /// E.Parker
        /// 13.05.11
        /// </summary>
        /// <param name="tempLandlordID"></param>
        void retrieveValuesForEdit(string tempLandlordID)
        {
            int landlordID = Convert.ToInt32(tempLandlordID);

            DBAccessController.DAL.ControlLandlord landlord = new DBAccessController.DAL.ControlLandlord();
            landlord = Director.GetLandLord(landlordID);
            txtAddress.Text = landlord.Address;
            txtEmail.Text = landlord.Email;
            txtName.Text = landlord.Name;
            txtTelephone.Text = landlord.Telephone;
            chkEmail.Checked = Convert.ToBoolean(landlord.RequiresEmail);
            chkMiFair.Checked = Convert.ToBoolean(landlord.RequiresMiFare);
            chkPhoto.Checked = Convert.ToBoolean(landlord.RequiresPhoto);
        }

        #region jQueryCallMethods
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }


        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditLandlordRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditLandlordRequest(false);");
            }


            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion
    }
}