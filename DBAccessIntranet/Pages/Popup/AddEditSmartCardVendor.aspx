﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditSmartCardVendor.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.AddEditSmartCardVendor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vendor Admin</title>
	<script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function ()
		{
			$('#vendorNameHelp').bind('click', function ()
			{
				$('#vendorNameHelpInfo').slideToggle("slow");
			});

			$('#keyIdentifierHelp').bind('click', function ()
			{
				$('#keyIdentifierHelpInfo').slideToggle("slow");
			});

            $('#useHexHelp').bind('click', function () {
                $('#useHexHelpInfo').slideToggle("slow");
            });

			$('#vendorEnabledHelp').bind('click', function ()
			{
				$('#vendorEnabledHelpInfo').slideToggle("slow");
			});

			
			$('#vendorNameHelpInfo').hide();
			$('#keyIdentifierHelpInfo').hide();
			$('#useHexHelpInfo').hide();
			$('#vendorEnabledHelpInfo').hide();
		});


		function Cancel()
		{
			window.parent.CancelAddEditSmartCardVendor();
		}

       function OnAddEditSmartCardProviderRequest(result)
       {
       		if (result)
       		{
       			window.parent.RefreshSmartCardProviderGrid();
       		}
       	}
	</script>
</head>
<body>
   <form id="form1" runat="server">
   <div>
		<h2><asp:Literal ID="PopupTitle" runat="server" ></asp:Literal></h2>
        <table>
            <tr>
				<td class="style1">
                    <p>
					 <img id="vendorNameHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage %>" alt="<%$ Resources:CommonResource, divAdminAltImage %>" runat="server"/>
                        &nbsp;
                        <asp:Literal ID="Vendorliteral" meta:resourcekey="Vendor" Visible="true" runat="server"/>
                    </p>
                </td>
                <td class="style2">
                    <asp:TextBox style="float:left" ID="txtVendorName" runat="server" Width="250px"></asp:TextBox> 
                    <asp:RequiredFieldValidator style="float:left" ID="RequiredFieldValidatorDivisionName" ControlToValidate="txtVendorName" meta:resourcekey="RequiredFieldValidatorVendor" Visible="true" runat="server"></asp:RequiredFieldValidator>       
                    <asp:CustomValidator style="float:left" ID="CustomValidatorVendorName" runat="server" OnServerValidate="ValidateUniqueVendorName" ControlToValidate="txtVendorName" ErrorMessage="There is an existing Vendor with the same name"></asp:CustomValidator>
                </td>
                <td>
                    <div id="vendorNameHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddVendorNameInfo" meta:resourcekey="AddVendorNameInfo" Visible="true" runat="server"/>
                        <asp:Literal ID="literalEditVendorNameInfo" meta:resourcekey="EditVendorNameInfo" Visible="true" runat="server"/>
                    </div>
                </td>
            </tr>
			<tr>
				<td class="style1">
                    <p>
					 <img id="keyIdentifierHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage %>" alt="<%$ Resources:CommonResource, divAdminAltImage %>" runat="server"/>
                        &nbsp;
                       <asp:Literal ID="KeyIdentifierliteral" meta:resourcekey="KeyIdentifier" Visible="true" runat="server" ClientIDMode="Static"/>
                    </p>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtKeyIdentifier" runat="server" Width="250px"></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorKeyIdentifier" ControlToValidate="txtKeyIdentifier" meta:resourcekey="RequiredFieldValidatorKeyIdentifier" Visible="true" runat="server"></asp:RequiredFieldValidator>       
                </td>
                <td>
                    <div id="keyIdentifierHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddKeyIdentifier" meta:resourcekey="AddKeyIdentifierInfo" Visible="true" runat="server"/>
                        <asp:Literal ID="literalEditKeyIdentifier" meta:resourcekey="EditKeyIdentifierInfo" Visible="true" runat="server"/>
                    </div>
                </td>
            </tr>
			<tr>
				<td class="style1">
                    <p>
                        <img id="useHexHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage %>" alt="<%$ Resources:CommonResource, divAdminAltImage %>" runat="server"/>
                        &nbsp;

                        <asp:Literal ID="UseHexLiteral" meta:resourcekey="UseHexLiteral" Visible="true" runat="server" ClientIDMode="Static"/>
                        <%--Use Hex Mifare--%>
                    </p>
                </td>
                <td class="style2">                    
                    <dx:ASPxCheckBox ID="ckbUseHex" runat="server"></dx:ASPxCheckBox>
                </td>
                <td>
                    <div id="useHexHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalUseHexMifare" meta:resourcekey="EditHexMifareInfo" Visible="true" runat="server"/>
                    </div>                    
                </td>
			</tr>
            <tr>
				<td class="style1">
                    <p>
                        <img id="vendorEnabledHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage %>" alt="<%$ Resources:CommonResource, divAdminAltImage %>" runat="server"/>
                        &nbsp;
                        <asp:Literal ID="Enabled" meta:resourcekey="Enabled" Visible="true" runat="server" ClientIDMode="Static"/>
                    </p>
                </td>
                <td class="style2">                    
                    <dx:ASPxCheckBox ID="ckbEnabled" runat="server"></dx:ASPxCheckBox>
                </td>
                <td>
                    <div id="vendorEnabledHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalSelectEnabled" meta:resourcekey="EditEnabledInfo" Visible="true" runat="server"/>
                    </div>                    
                </td>
			</tr>
		 </table>
   </div>
   <div style="float:left;">
			<div style="float: left; margin: 5px" id="div1">
				<dx:ASPxButton ID="ASPxDelete" runat="server" Text="<%$ Resources:CommonResource, Delete %>" Width ="150" ClientSideEvents-Click='function(s, e){ window.parent.DeleteSmartcardProvider();}'></dx:ASPxButton>
            </div>            
            <div style="float: left; margin: 5px" id="div12">
                <dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel %>" ForeColor="#0098db" Width="150">                    
                    <ClientSideEvents Click="Cancel" />
                </dx:ASPxButton>
            </div>
			<div style="float: left; margin: 5px" id="div11">
				<dx:ASPxButton ID="ButtonEditDivision" runat="server" Text="<%$ Resources:CommonResource, Save %>" Width ="150" OnClick="SaveSmartCardProvider"></dx:ASPxButton>
            </div>
    </div>
   </form>
</body>
</html>

