﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditSmartCardType.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.AddEditSmartCardType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="VendorAdmin1" meta:resourcekey="VendorAdmin" runat="server"></asp:Literal></title>
	<script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function ()
		{
			$('#vendorNameHelp').bind('click', function ()
			{
				$('#vendorNameHelpInfo').slideToggle("slow");
			});

			$('#smartCardTypeHelp').bind('click', function ()
			{
				$('#smartCardTypeHelpInfo').slideToggle("slow");
			});

            $('#smartcardLabelHelp').bind('click', function () {
                $('#smartcardLabelHelpInfo').slideToggle("slow");
            });

			$('#keyIdentifierHelp').bind('click', function ()
			{
			    $('#keyIdentifierHelpInfo').slideToggle("slow");
			});

			$('#enabledHelp').bind('click', function ()
			{
				$('#enabledHelpInfo').slideToggle("slow");
			});


			$('#vendorNameHelpInfo').hide();
			$('#smartCardTypeHelpInfo').hide();
			$('#smartcardLabelHelpInfo').hide();
			$('#keyIdentifierHelpInfo').hide();
			$('#enabledHelpInfo').hide();
		});


		function Cancel()
		{
			window.parent.CancelAddEditSmartCardType();
		}

       function OnAddEditSmartCardProviderRequest(result)
       {
       		if (result)
       		{
       		    window.parent.RefreshSmartCardTypeGrid();
       		}
       }
	</script>
    <style type="text/css">
        .style1
        {
           width: 100px;
            height: 20px;
            vertical-align: top;
        }
        .style2
        {
            width: 280px;
            height: 20px;
            vertical-align: top;
           
        }
        .style3 {
            width: 140px;
            vertical-align: top;
            height: 20px;
            
        }
    </style>
</head>
<body>
   <form id="form1" runat="server">
   <div>
		<h2><asp:Literal ID="divisionTitle" runat="server"></asp:Literal></h2>
		<table>
            <tr>
				<td class="style1" >
                    <p>
					 <img id="vendorNameHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage%>" alt="<%$ Resources:CommonResource, divAdminAltImage%>" runat="server"/>
                        &nbsp;
                       <asp:Literal ID="Vendorliteral" meta:resourcekey="Vendor" Visible="true" runat="server"/>
                    </p>
                </td>
                <td class="style2" >
					<asp:DropDownList ID="ddVendor" runat="server" Width="180px" ClientIDMode="Static" DataValueField="ID" DataTextField="ProviderName" AutoPostBack="true">
					</asp:DropDownList>
					<%--<asp:CompareValidator ID="rqfddVendor" runat="server" ControlToValidate="ddVendor" Operator="notEqual" ValueToCompare="0" ErrorMessage="Please Choose a SmartCardType"/> --%>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorDivisionName" runat="server" ControlToValidate="ddVendor" ErrorMessage="Please enter a Vendor name"></asp:RequiredFieldValidator>--%>
                </td>
                <td class="style3">
                    <div id="vendorNameHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddVendorInfo"  meta:resourcekey="SelectVendorForAdd" runat="server"/>
                    </div>
                </td>
            </tr>
			<tr>
				<td class="style1">
                    <p>
					 <img id="smartCardTypeHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage%>" alt="<%$ Resources:CommonResource, divAdminAltImage%>" runat="server"/>
                        &nbsp;
                        <asp:Literal ID="SmartcardType" meta:resourcekey="SmartcardType" runat="server"/>
                    </p>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtboxSmartCardType" runat="server" Width="180px"></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorSmartCardType" runat="server" ControlToValidate="txtboxSmartCardType" ErrorMessage="<%$ Resources:CommonResource, SmartCardTypeError%>"></asp:RequiredFieldValidator>       
                </td>
                <td class="style3">
                    <div id="smartCardTypeHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddSmartCardType" runat="server" Visible ="False"  meta:resourcekey="SmartCardTypePrompt" />
                        <asp:Literal ID="literalEditSmartCardType" runat="server" Visible ="False" meta:resourcekey="SmartCardVendorPrompt" />
                    </div>
                </td>
            </tr>
			<tr>
				<td class="style1">
                    <p>
					 <img id="smartcardLabelHelp" src="../../App_Themes/DBIntranet2010/images/question.gif"  title="<%$ Resources:CommonResource, divAdminAltImage%>" alt="<%$ Resources:CommonResource, divAdminAltImage%>" runat="server"/>
                        &nbsp;
                       <asp:Literal ID="LabelLiteral1" meta:resourcekey="Label" runat="server"></asp:Literal>  
                    </p>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtboxSmartcardLabel" runat="server" Width="180px"></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorSmartcardLabel" runat="server" ControlToValidate="txtboxSmartcardLabel" ErrorMessage="<%$ Resources:CommonResource, SmartCardLabelError%>"></asp:RequiredFieldValidator>       
                </td>
                <td class="style3">
                    <div id="smartcardLabelHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddSmartcardLabel" meta:resourcekey="SmartCardEnterLabel" Visible="true" runat="server"/>
                        <asp:Literal ID="literalEditSmartcardLabel" meta:resourcekey="SmartCardEditLabel" Visible="true" runat="server"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="style1"><p>
					 <img id="keyIdentifierHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage%>" alt="<%$ Resources:CommonResource, divAdminAltImage%>" runat="server"/>
                        &nbsp;
                       <asp:Literal ID="Identifier" meta:resourcekey="Identifier" runat="server"></asp:Literal>    
                    </p>
                </td>
                <td class="style2" ><asp:TextBox  ID="txtKeyIdentifier" runat="server" Width="180px"  MaxLength="16"></asp:TextBox> 
                    <asp:RequiredFieldValidator style="float:left"  ID="RequiredFieldValidatorKeyIdentifier" runat="server" ControlToValidate="txtKeyIdentifier" ErrorMessage="<%$ Resources:CommonResource, SmartCardTypeIdentifierError%>"></asp:RequiredFieldValidator>       
                    <asp:RegularExpressionValidator style="float:left"  ID="RegularExpressionValidatorIdentifier" runat="server" ControlToValidate="txtKeyIdentifier" ErrorMessage="Identifier has some invalid characters" ValidationExpression="^[0-9a-zA-Z]+$"></asp:RegularExpressionValidator>
                    <asp:CustomValidator style="float:left" ID="CustomValidatorIdentifier" runat="server" OnServerValidate="ValidateUniqueIdentifier" ControlToValidate="txtKeyIdentifier" ErrorMessage="There is an existing Type Identifier with the same name"></asp:CustomValidator>
                 </td>
                <td class="style1"><div id="keyIdentifierHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalAddSmartcardTypeIdentifier"  meta:resourcekey="AddSmartcardTypeIdentifier" Visible="true" runat="server"/>
                        <asp:Literal ID="literalEditSmartcardTypeIdentifier" meta:resourcekey="EditSmartcardTypeIdentifier" Visible="true" runat="server"/>
                    </div>
                </td>
            </tr>

			
			<tr style="vertical-align: top">
				<td class="style1">
                    <p>
                        <img id="enabledHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminAltImage%>" alt="<%$ Resources:CommonResource, divAdminAltImage%>" runat="server"/>
                        &nbsp;
                        <asp:Literal ID="Enabled" Text="<%$ Resources:CommonResource, Enabled%>" runat="server"></asp:Literal>
                    </p>
                </td>
                <td class="style2">                    
                    <dx:ASPxCheckBox ID="ckbEnabled" runat="server"></dx:ASPxCheckBox>
                </td>
                <td class="style3">
                    <div id="enabledHelpInfo" class="HelpPopUp">
                        <asp:Literal ID="literalChkEnableType" runat="server" meta:resourcekey="SelectToEnable" />
                    </div>                    
                </td>
			</tr>
            
		 </table>
   </div>
   <div style="float:left;">
			<div style="float: left; margin: 5px" id="div1">
				<dx:ASPxButton ID="ASPxDelete" runat="server" Text="<%$ Resources:CommonResource, Delete %>" Width ="75" ClientSideEvents-Click='function(s, e){ window.parent.DeleteSmartcardType();}'></dx:ASPxButton>
            </div>            
            <div style="float: left; margin: 5px" id="div12">
                <dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>" ForeColor="#0098db" Width="75">                    
                    <ClientSideEvents Click="Cancel" />
                </dx:ASPxButton>
            </div>
			<div style="float: left; margin: 5px" id="div11">
				<dx:ASPxButton ID="ButtonEditDivision" runat="server" Text="<%$ Resources:CommonResource, Save %>" Width ="75" OnClick="SaveSmartCardType"></dx:ASPxButton>
            </div>
    </div>
   </form>
</body>
</html>

