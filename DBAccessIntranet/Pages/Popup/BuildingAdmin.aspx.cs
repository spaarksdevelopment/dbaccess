﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using MSA = Microsoft.Security.Application;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup
{
    public partial class BuildingAdmin : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string pageType = Request.QueryString["type"].ToString();

                int? dbMovesRegionID = null;

                if (pageType == "add")
                {
                    string cityID = Request.QueryString["CityID"].ToString();
                    int newCityID = Convert.ToInt32(cityID.Replace("Ci", ""));
                    ASPxHiddenFieldBuilding.Set("CityID", newCityID);
                    ASPxHiddenFieldBuilding.Set("BuildingID", 0);

                    dbMovesRegionID = Director.GetDBMovesRegionIDByCityID(newCityID);
                }
                else
                {
                    string buildingID = Request.QueryString["BuildingID"].ToString();
                    int newBuildingID = Convert.ToInt32(buildingID.Replace("B",""));
                    ASPxHiddenFieldBuilding.Set("BuildingID", newBuildingID);
                    
                    dbMovesRegionID = Director.GetDBMovesRegionIDByBuildingID(newBuildingID);
                    
                    getData(buildingID);
                }

                DBMovesLocationSelector1.DBMovesRegionID = dbMovesRegionID;
                
                setPageText(pageType);
                ASPxHiddenFieldBuilding.Add("pageType", pageType);
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
            }
        }


        /// <summary>
        /// Method to add /edit a building 
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddBuilding(object sender, EventArgs e)
        {
            DBAccessController.DAL.LocationBuilding newbuilding = new DBAccessController.DAL.LocationBuilding();
            newbuilding.BuildingID = Convert.ToInt32(ASPxHiddenFieldBuilding.Get("BuildingID"));
            newbuilding.CityID = Convert.ToInt32(ASPxHiddenFieldBuilding.Get("CityID"));
			newbuilding.Code = MSA.Encoder.HtmlEncode(txtCode.Text);
			newbuilding.Name = MSA.Encoder.HtmlEncode(txtBuildingName.Text);
			newbuilding.StreetAddress = MSA.Encoder.HtmlEncode(txtStreetAddress.Text);
            newbuilding.Enabled = Convert.ToBoolean(ddlEnabledBuildEdit.SelectedValue);
            newbuilding.LandlordID = landlordSelector1.LandlordID;
            newbuilding.VisitorControlSystem = vmSelectorManagement.VisitorAccessSysID;
            newbuilding.DBMovesLocationID = DBMovesLocationSelector1.DBMovesLocationID;           
            newbuilding.PassOfficeID = GetSelectedPassOfficeID();

            bool isNew = ASPxHiddenFieldBuilding.Get("pageType").ToString() == "add";

            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateBuilding(newbuilding, isNew);
            loadJQuery(success);            
        }

        private int? GetSelectedPassOfficeID(){

            if (ctrlPassOffice.PassOfficeID != 0)
            {
               return ctrlPassOffice.PassOfficeID;
            }
            else
            {
                return null;
            }               
        }
       
        #region BindData
        /// <summary>
        /// Method to get building object and bind to the page controls
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="buildingID"></param>
        void getData(string buildingID)
        {
            //convert to int
            int newBuildingID = Convert.ToInt32(buildingID.Replace("B", ""));
            DBAccessController.DAL.LocationBuilding building =  Director.GetBuilding(newBuildingID);
            //now bind to the page controls
            txtBuildingName.Text = building.Name;
            ddlEnabledBuildEdit.SelectedValue = building.Enabled.ToString().ToLower();
            landlordSelector1.LandlordID = building.LandlordID;
            vmSelectorManagement.VisitorAccessSysID = building.VisitorControlSystem;
            DBMovesLocationSelector1.DBMovesLocationID = building.DBMovesLocationID;
            txtStreetAddress.Text = building.StreetAddress;
            ctrlPassOffice.PassOfficeID = (building.PassOfficeID != null) ? building.PassOfficeID.Value : 0;
            ASPxHiddenFieldBuilding.Set("CityID", building.CityID);
        }
        
        /// <summary>
        /// method to set the text for the page title and the button text
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="pageType"></param>
        void setPageText(string pageType)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            switch (pageType)
            {
                case "add":
                    buildingTitle.Text = resourceManager.GetString("AddBuilding"); //"Add Building";
                    ASPxButBuilding.Text = resourceManager.GetString("Save");
                    break;
                case "edit":
                    buildingTitle.Text = resourceManager.GetString("EditBuilding");
                    ASPxButBuilding.Text = resourceManager.GetString("Save");
                    break;
            }
        }
        #endregion

        #region jQueryCallMethods
        /// <summary>
        /// Method to register the jQuery within the page 
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="results"></param>
        private void loadJQuery(bool results)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            if (requestSM != null && requestSM.IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(results), true);
            }
        }

        /// <summary>
        /// Method to create jQuery
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        private string getjQueryCode(bool results)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            if (results)
            {
                sb.AppendLine("OnEditBuildingRequest(true);");
            }
            else
            {
                sb.AppendLine("OnEditBuildingRequest(false);");
            }
            sb.AppendLine(" });");
            return sb.ToString();
        }
        #endregion


    }

}