﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OutOfOfficePopup.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.OutOfOfficePopup" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="~/Controls/OutOfOfficeControl.ascx" TagName="OutOfOffice" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <div id="Div1" runat="server">
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" ></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-plugins.js") %>" ></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-tablesorter.js") %>" ></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery-ui-1.8.10.custom.min.js") %>" ></script>
    </div>	

    <script type="text/javascript" language="javascript">
        function ClosePopup() {
            window.parent.CloseOutOfOfficePopup();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <act:ToolkitScriptManager ID="Scriptmanager1" runat="server" enablepagemethods="true">
		</act:ToolkitScriptManager>

        <uc1:OutOfOffice ID="OutOfOffice" runat="server" />
    
        <br />
		<div align="center">
            <!--Note ASPxButton causes the progress bar to stall and asp:Button causes postback-->
            <!-- there is a problem putting translation into <input> tag. setting UseSubmitBehavior="false" solves the problem of postback in <asp:Button -->
           <%-- <input id="ButtonAccept" type="button" size="100px" onclick="ClosePopup()" class="htmlButton"  meta:resourcekey="Done"/>--%>
            <asp:Button  ID="ButtonAccept" Width="100px" OnClientClick="ClosePopup()" CssClass="htmlButton" Text="<%$ Resources:CommonResource, Done%>" runat="server"  UseSubmitBehavior="false"/>
		</div>
    </form>
</body>
</html>