﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UBRPopup.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.UBRPopup" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register src="~/Controls/Selectors/UBRSelector.ascx" tagname="UBRSelector" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UBR Selection</title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
    <script type="text/javascript" language="javascript">
        function AddBusinessArea() {
            var ubr = $('#TextBoxUBRKey').val();
            var ubrName = $('#TextBoxUBRKeyValue').val();

            if (ubr == '')
                $('#result').html('Error - UBR has not been selected.');

            window.parent.SubmitCode(ubr, ubrName);
        }

        function Cancel() {
            window.parent.CancelAdd();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color:White">
        
    <p>Please select from the UBR structure below.</p>
   
        <uc1:UBRSelector ID="UBRSelector1" runat="server" />
   
  <br />
  <br />
  <div>
   <dx:ASPxButton ID="btnAddBA" runat="server" AutoPostBack="False" Text="Add Business Area" style="float:left;">
            <ClientSideEvents Click="AddBusinessArea" />
    </dx:ASPxButton>
    <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" Text="Cancel" ForeColor="#0098db" style="float:left;">
            <ClientSideEvents Click="Cancel" />
    </dx:ASPxButton>

    <div id="result" style="color:red;float:right;"></div>
   </div> 
   <br />
    <br />
    </div>

	<ppc:Popups ID="PopupTemplates" runat="server" />

    </form>
</body>
</html>
