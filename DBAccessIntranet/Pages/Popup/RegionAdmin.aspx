﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegionAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.RegionAdmin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Literal ID="RegionAdminLiteral" meta:resourcekey="RegionAdmin" runat="server" ClientIDMode="Static"></asp:Literal></title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>


     <script type="text/javascript" language="javascript">

         function Cancel() {
             window.parent.CancelEditRegion();
         }
                  
         function OnEditRegionRequest(result) {
             //cancel and refresh the grid
             if (!result) {
                 window.parent.ErrorShowPopup('region',false);
             }
             else {
                 window.parent.RefreshLocationGrid('region');
             }
         }


     </script>

</head>
<body>
<form id="form1" runat="server">
            <h2><asp:Literal id="regionTitle" runat="server" /></h2>
            <table>
                <tr>
                    <td><p><asp:Literal runat="server" Text="<%$Resources:CommonResource, Name%>" /></p></td>
                    <td>                        
                        <asp:TextBox ID="txtEditRegionName" runat="server" />                   
                    </td>
                </tr>
                <tr>
                    <td><p><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:CommonResource, Code%>" /></p></td>
                    <td>                        
                        <asp:TextBox ID="txtRegionCode" runat="server" />                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:CommonResource, Enabled%>" /></p>
                    </td>
                    <td>
                         <asp:DropDownList ID="ddlEditRegionEnabled" runat="server" >
                            <asp:ListItem Text="<%$Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
			<br />
			<div style="float:left;">
                <div style="float: left; margin: 5px" id="div11">
					<asp:Button ID="ASPxButRegion" runat="server" AutoPostBack="true" Width="150" OnClick="AddRegion" />            
                </div>
                <div style="float: left; margin: 5px" id="div12">
						<dx:ASPxButton ID="ASPxButton4" runat="server" AutoPostBack="false" Text="<%$Resources:CommonResource, Cancel%>" ForeColor="#0098db" Width="150">                    
                            <ClientSideEvents Click="Cancel" />
                        </dx:ASPxButton>
                </div>
            </div>   
            <!--hidden field-->

            <dx:ASPxHiddenField ID="ASPxHiddenRegion" ClientInstanceName="hdRegion" runat="server">
            </dx:ASPxHiddenField>
            </form>
</body>
</html>
