﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FloorAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Popup.FloorAdmin" %>

<%@ Register Src="~/Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Selectors/DBMovesFloorSelector.ascx" TagName="DBMovesFloorSelector" TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="Literal2" meta:resourcekey="FloorAdmin"  runat="server"> </asp:Literal> </title>
     <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js" ></script>
     <script type="text/javascript" language="javascript">
         function Cancel() {
             window.parent.CancelEditFloor();
         }    
         function OnEditFloorRequest(result) {
             //cancel and refresh the grid
             if (!result) {
                 window.parent.ErrorShowPopup('floor');
             }
             else {
                 window.parent.refreshTree('floor');
             }
         }
     </script>
</head>
<body>
    <form id="form1" runat="server">
        <h2><asp:Literal ID="floorTitle" runat="server" /></h2>
        <table>
            <tr>            
                <td>
                    <p><asp:Literal ID="Literal1" Text="<%$ Resources:CommonResource, Name%>" runat="server"> </asp:Literal> 
                    </p>
                </td>
                <td>
                    <asp:TextBox id="txtFloorName" ClientIDMode="Static" runat="server"  MaxLength="10" />
                </td>
                 <td>
                    <div id="floorNameHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p><asp:Literal ID="Literal3" Text="<%$ Resources:CommonResource, Enabled%>" runat="server"> </asp:Literal> 
                    </p>
                </td>
                <td>
                     <asp:DropDownList ID="ddlEnabledFloorEdit" runat="server"  ClientIDMode="Static">
                            <asp:ListItem Text="<%$ Resources:CommonResource, Yes%>" Value="true"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:CommonResource, No%>" Value="false"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                 <td>
                    <div id="floorEnabledHelpInfo" class="HelpPopUp">                      
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p><asp:Literal ID="DBMovesFloor" meta:resourcekey="DBMovesFloor"  runat="server"> </asp:Literal>
                    </p>
                </td>
                <td>
                    <uc3:DBMovesFloorSelector ID="DBMovesFloorSelector1" runat="server" ClientIDMode="Static" />                       
                </td>
                <td>
                    <div id="dbMovesFloorHelpInfo" class="HelpPopUp">
                </div>
                </td>
            </tr>
        </table>
		<br />
        <div style="float:left;">
                <div style="float: left; margin: 5px" id="div11">
                    <dx:ASPxButton ID="ASPxButFloor" runat="server" AutoPostBack="True" Width="150" OnClick="addFloor">                
                    </dx:ASPxButton>              
                </div>
                <div style="float: left; margin: 5px" id="div12">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>"  ForeColor="#0098db" Width="150">                    
                         <ClientSideEvents Click="Cancel" />
                    </dx:ASPxButton>
                </div>
            </div>   

<!-- hidden fields-->
 <dx:ASPxHiddenField ID="ASPxHiddenFieldFloor" ClientInstanceName="hdFloor" runat="server">
</dx:ASPxHiddenField>

    </form>
</body>
</html>
