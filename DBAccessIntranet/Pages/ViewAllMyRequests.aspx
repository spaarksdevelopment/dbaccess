﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ViewAllMyRequests.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.ViewAllMyRequests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
<h1>View All Requests</h1>
<div id="allrequests">
    <p>Click here to export this data to Excel.<asp:ImageButton ID="ImageButton2" AlternateText="Click here to export this data to Excel" SkinID="FileXLS" runat="server"  OnClick="btnXlsxExportPO_Click" />  </p> 
    <dx:ASPxGridView ID="ASPxGridViewRequestDetailsDetails" ClientInstanceName="gvViewAllMyREquests" runat="server">
     
            <Columns>
                 <dx:GridViewDataTextColumn Caption="RequestID" FieldName="RequestID" ShowInCustomizationForm="True" VisibleIndex="1"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Start Date" FieldName="StartDate" ShowInCustomizationForm="True" VisibleIndex="2" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="End Date" FieldName="EndDate" ShowInCustomizationForm="True" VisibleIndex="3" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RequestStatusID" FieldName="RequestStatusID" ShowInCustomizationForm="True" VisibleIndex="4"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="BadgeID" FieldName="BadgeID" ShowInCustomizationForm="True" VisibleIndex="5"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="BusinessAreaID" FieldName="BusinessAreaID" ShowInCustomizationForm="True" VisibleIndex="6"></dx:GridViewDataTextColumn>               
                <dx:GridViewDataTextColumn Caption="Created By" FieldName="CreatedBy" ShowInCustomizationForm="True" VisibleIndex="7"></dx:GridViewDataTextColumn>               
                <dx:GridViewDataTextColumn Caption="Created Date" FieldName="Created" ShowInCustomizationForm="True" VisibleIndex="8" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="9"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Approved" FieldName="Approved" ShowInCustomizationForm="True" VisibleIndex="10"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="AccessApprovedDate" FieldName="AccessApprovedDate" ShowInCustomizationForm="True" VisibleIndex="11" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="ApprovedBy" FieldName="ApprovedBy" ShowInCustomizationForm="True" VisibleIndex="12"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RequestName" FieldName="RequetName" ShowInCustomizationForm="True" VisibleIndex="13"></dx:GridViewDataTextColumn>                
            </Columns>
            <SettingsText EmptyDataRow="You don't have any tasks." />
            
    </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewRequestDetailsDetails">
        
    </dx:ASPxGridViewExporter>

</div>    
</asp:Content>
