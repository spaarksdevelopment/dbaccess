﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Collections.Generic;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class LoginPage : BaseSecurityPage
    {
        bool TestMode = (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE");
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);            

            string returnURL = Request.QueryString["ReturnURL"];

            if (returnURL == "/Pages/Home.aspx") returnURL = string.Empty;

            if ((ConfigurationManager.AppSettings["UseSSO"] != null)&&(ConfigurationManager.AppSettings["UseSSO"].ToUpper() == "TRUE"))
            {
                string username = Request.ServerVariables["HTTP_CT_REMOTE_USER"];

                DBSqlMembershipProvider dbSqlMembershipProvider = new DBSqlMembershipProvider();
                DBAppUser User = dbSqlMembershipProvider.GetDBAppUser(username);

                if ((User == null) || (!User.Enabled))
                {
                    PanelLogin.Visible = false;
                }
                else
                {
                    Session["FullNameOfCurrentUser"] = User.FullName;
                    Session["LastLoggedInDateTime"] = String.Format("{0:r}", User.LastLoginDate);

                    FormsAuthentication.SetAuthCookie(username, false);

                    dbSqlMembershipProvider.UpdateUserActivity(User.Email, User.dbPeopleID, true);

                    if (string.IsNullOrEmpty(returnURL))
                        RedirectLogin(User.RolesShortNamesList);
                    else
                        base.Redirect(returnURL);

                    PanelLogin.Visible = false;
                }
                //[/Mahmoud]    
                           
            }
            else if (ConfigurationManager.AppSettings["AutoLoginUsername"] != null)
            {
                string username = ConfigurationManager.AppSettings["AutoLoginUsername"].ToString();

                if (username != string.Empty)
                {
                    FormsAuthentication.SetAuthCookie(username, false);

                    if (string.IsNullOrEmpty(returnURL))
                    {
                        DBSqlMembershipProvider dbSqlMembershipProvider = new DBSqlMembershipProvider();
                        DBAppUser User = dbSqlMembershipProvider.GetDBAppUser(username);

                        Session["FullNameOfCurrentUser"] = User.FullName;
                        Session["LastLoggedInDateTime"] = String.Format("{0:r}", User.LastLoginDate);

                        RedirectLogin(User.RolesShortNamesList);
                    }
                    else
                        base.Redirect(returnURL);
                }
            }
		}

        /// <summary>
        /// Redirect the user to a specific URL, as specified in the web.config, depending on their role.
        /// If a user belongs to multiple roles, the first matching role in the web.config is used.
        /// Prioritize the role list by listing higher-level roles at the top.
        /// </summary>
        /// <param name="username">Username to check the roles for</param>
        private void RedirectLogin(List<string> RoleList)
        {
            LoginRedirectByRoleSection roleRedirectSection = (LoginRedirectByRoleSection)ConfigurationManager.GetSection("LoginRedirectByRole");

            foreach (RoleRedirect roleRedirect in roleRedirectSection.RoleRedirects)
            {
                if (RoleList.Contains(roleRedirect.Role))
                    base.Redirect(roleRedirect.Url);                   
                }
            }

        protected void Login1_OnLoggingIn(object sender, EventArgs e)
        {           
            string username = Login1.UserName;
            DBSqlMembershipProvider dbSqlMembershipProvider = new DBSqlMembershipProvider();
            DBAppUser user = dbSqlMembershipProvider.GetDBAppUser(username);

            if (user != null)
            {
                Session["FullNameOfCurrentUser"] = user.FullName;
                Session["LastLoggedInDateTime"] = String.Format("{0:r}", user.LastLoginDate);
            }            
        }

        protected void Login1_OnLoggedIn(object sender, EventArgs e)
        {
            string username = Login1.UserName;           

            //if login failed because user was not registered, add them...
            DBSqlMembershipProvider dbSqlMembershipProvider = new DBSqlMembershipProvider();

            DBAppUser User = dbSqlMembershipProvider.GetDBAppUser(username);
            mp_User theUser = Director.GetUserBydbPeopleId(User.dbPeopleID);
            if (theUser.Revoked == true)
                LogOutUser();

            string returnURL = Request.QueryString["ReturnURL"];

            if (returnURL == "/Pages/Home.aspx") returnURL = string.Empty;

            if (string.IsNullOrEmpty(returnURL))
                RedirectLogin(User.RolesShortNamesList);
            }

        private void LogOutUser()
        {
            FormsAuthentication.SignOut();
            Roles.DeleteCookie();
            Session.Clear();
        }


        protected override void OnPreRender(EventArgs e)
        {            
            LabelTestModeWarning.Visible = TestMode;
            
            base.OnPreRender(e);
        }
    }
}
