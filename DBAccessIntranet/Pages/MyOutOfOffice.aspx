﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="MyOutOfOffice.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyOutOfOffice" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="~/Controls/OutOfOfficeControl.ascx" TagName="OutOfOffice" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
     
                <uc1:OutOfOffice ID="OutOfOffice" runat="server" />
           
</asp:Content>