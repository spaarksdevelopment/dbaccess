﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="AccessAreaRecertification.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.AccessAreaRecertification" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">

<script type="text/javascript">
var selections = new Array();

    $(document).ready(function () {
        //hide the help function
        $('#AccessHelp').bind('click', function () {
            $("#AccessHelpInfo").slideToggle("slow");
        });
        $('#AccessHelpInfo').hide();
    });

    function ConfirmCancel()
    {
    	popupConfirmTemplate.SetHeaderText("Cancel Save Selection");
    	popupConfirmTemplateContentHead.SetText("Save Selection");
    	popupConfirmTemplateContentBody.SetText("Are you sure you want to save your selection?");
    	popupConfirmTemplateHiddenField.Set("function_Yes", "OnSaveSelection();");
    	popupConfirmTemplate.Show();
    }

    function OnSaveSelection()
    {
		// do we need to do something here? SC
    }


    function Recertify()
    {
    	var iArea = $("#<%=HFArea.ClientID %>").val();
    	var iDivision = $("#<%=HFDivision.ClientID %>").val();

    	PageMethods.ApprovedAccessAreaRecertify(selections, (iDivision != "") ? iDivision : 0, (iArea != "") ? iArea : 0, OnRecertify);
    }

    function OnRecertify(result)
    {
    	if (!result)
    	{
    		popupAlertTemplateContent.SetText("There was a problem recertifying these selections.");
    		popupAlertTemplate.SetHeaderText("Recertify");
    		popupAlertTemplate.Show();
    		return;
    	}
    	gvAccessAreaPeople.PerformCallback();
    }

    function Remove()
    {
    	var iArea = $("#<%=HFArea.ClientID %>").val();
    	var iDivision = $("#<%=HFDivision.ClientID %>").val();

    	PageMethods.RejectedAccessAreaRecertify(selections, (iDivision != "") ? iDivision : 0, (iArea != "") ? iArea : 0, OnRemove);
    }

    function OnRemove(result)
    {
    	if (!result)
    	{
    		popupAlertTemplateContent.SetText("There was a problem removing these selections.");
    		popupAlertTemplate.SetHeaderText("Remove");
    		popupAlertTemplate.Show();
    		return;
    	}
    	gvAccessAreaPeople.PerformCallback();
    }

    function GridSelectionChanged(s, e) {
        s.GetSelectedFieldValues("dbPeopleID", GetSelectedFieldValuesCallback);
    }
    function GetSelectedFieldValuesCallback(values) {
        selections = [];
        for (var i = 0; i < values.length; i++) {
            selections.push(values[i]);
        }
    }
</script>
<h1>Recertification of Access Areas</h1>
    <p>
        Recertification of Access Areas is where you can recertify user's access for a particular Access Area.<img id="AccessHelp" src="../App_Themes/DBIntranet2010/images/question.gif" title="Click here for help" alt="Click here for help"/>
    </p>
    <br />   
    <br />
    <div id="AccessHelpInfo" class="HelpPopUp">
       <p>Recertification of Access Areas is designed to show only the Access Areas to which you have permissions to recertify user's persmissions, the first grid on this page will show the Access Areas.       
       </p>
       <p>The second grid shows the people who currently have access within this Access Area and the original Approver and Date.</p>
        <p> Where you see the following icon, you can export the data.
             <asp:Image ID="Help" SkinID="FileXLS"  AlternateText="Export data" title="Export Data" runat="server" />
        </p>        
    </div>
    <br />
    <h3>My Access Areas </h3>
     <p>
    Click here to export this data to Excel. <asp:ImageButton ID="btnUploadExcel" AlternateText="Click to upload Excel sheet" SkinID="FileXLS" runat="server"  OnClick="btnXlsxExport_Click" />   
    </p>
  <dx:ASPxGridView ID="ASPxGridViewAccessAreas" ClientInstanceName="gvAccessAreas"
    runat="server" AutoGenerateColumns="False" KeyFieldName="AccessAreaID" Enabled="true" Visible="true" 
    ClientIDMode="AutoID" >
    <Columns>
        <dx:GridViewDataTextColumn Caption="&nbsp;" ShowInCustomizationForm="True" CellStyle-HorizontalAlign="Right" VisibleIndex="1" >
            <DataItemTemplate>
                <asp:LinkButton ID="Action" Text="Action" CommandArgument='<%#Container.KeyValue + "|" + Eval("CorporateDivisionAccessAreaID") + "|" + DataBinder.Eval(Container.DataItem, "RegionName") + "|" +  DataBinder.Eval(Container.DataItem, "CountryName")  + "|" + DataBinder.Eval(Container.DataItem, "CityName") + "|" + DataBinder.Eval(Container.DataItem, "BuildingName") + "|" + DataBinder.Eval(Container.DataItem, "FloorName") + "|" + DataBinder.Eval(Container.DataItem, "Name") %>'  runat="server" OnClick="aspxButtonAcessAreaClick"   >                   
                </asp:LinkButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        
        <dx:GridViewDataTextColumn Caption="Division Name"  FieldName="DivisionName" ShowInCustomizationForm="True" VisibleIndex="2" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Region"  FieldName="RegionName" ShowInCustomizationForm="True" VisibleIndex="2" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Country" FieldName="CountryName" ShowInCustomizationForm="True" VisibleIndex="3" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="City" FieldName="CityName" ShowInCustomizationForm="True" VisibleIndex="4" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Building" FieldName="BuildingName" ShowInCustomizationForm="True" VisibleIndex="5" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Floor" FieldName="FloorName" ShowInCustomizationForm="True" VisibleIndex="6" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Access Area" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="7" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Date Due"  FieldName="NextCertificationDate" ShowInCustomizationForm="True" VisibleIndex="8" CellStyle-HorizontalAlign="Right" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}" />
        
    </Columns>
  </dx:ASPxGridView>
  <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewAccessAreas" ></dx:ASPxGridViewExporter>
  <asp:HiddenField ID="HFDivision" runat="server" />
  <asp:HiddenField ID="HFArea" runat="server" />

  <dx:ASPxHiddenField ID="ASPxHiddenFieldDivision" ClientInstanceName="hdDivision" runat="server"></dx:ASPxHiddenField>


  <br />  
  <br />   
    <h3>People in "<asp:Literal ID="AccessAreaName" runat="server" />" Access Area for Recertification</h3>
     <p>
    Click here to export this data to Excel. <asp:ImageButton ID="ImgbuttonPeople" AlternateText="Click to upload Excel sheet" SkinID="FileXLS" runat="server"  OnClick="btnXlsxExportPeople_Click" />   
    </p>     
  <dx:ASPxGridView ID="ASPxGridViewAccessDetails" ClientInstanceName="gvAccessAreaPeople"
    runat="server" AutoGenerateColumns="False"  SettingsPager-PageSize="150" KeyFieldName="dbPeopleID"  
    ClientIDMode="AutoID">
    <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">
            <HeaderTemplate>
                <input type="checkbox" onclick="gvAccessAreaPeople.SelectAllRowsOnPage(this.checked);" title="Select/Unselect all rows on the page" />
            </HeaderTemplate>
            <HeaderStyle HorizontalAlign="Center" />
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn Caption="Forename" FieldName="Forename" ShowInCustomizationForm="True" VisibleIndex="2" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Surname" FieldName="Surname" ShowInCustomizationForm="True" VisibleIndex="3" CellStyle-HorizontalAlign="Right" />
        <%--<dx:GridViewDataTextColumn Caption="Original Approver Email" FieldName="AccessApproverEmail" ShowInCustomizationForm="True" VisibleIndex="4" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Original Approval Date" FieldName="ApprovalDate" ShowInCustomizationForm="True" VisibleIndex="6" CellStyle-HorizontalAlign="Right" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}" />
        <dx:GridViewDataTextColumn Caption="Due Date" FieldName="DateNextCertification" ShowInCustomizationForm="True" VisibleIndex="6" CellStyle-HorizontalAlign="Right" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy}" />--%>
    </Columns>
    <ClientSideEvents SelectionChanged="GridSelectionChanged" />
  </dx:ASPxGridView>
                <div style="float:left;">
    <dx:ASPxButton ID="btnConfirm" AutoPostBack="false" runat="server" Text="Recertify Selected Approvers">
        <ClientSideEvents Click="Recertify" />
    </dx:ASPxButton></div> <div style="float:left;">               
    <dx:ASPxButton ID="btnRemove" AutoPostBack="false" runat="server" Text="Remove Selected Approvers">
        <ClientSideEvents Click="Remove" />
    </dx:ASPxButton></div>
    <dx:ASPxGridViewExporter ID="gridExportTwo" runat="server" GridViewID="ASPxGridViewAccessDetails" ></dx:ASPxGridViewExporter>  
  <br />
<%--    <dx:ASPxButton ID="ASPxButtonSave" runat="server" Text="Recertify People"  ClientInstanceName="saveButton" ForeColor="#0098db" AutoPostBack="false">
                <ClientSideEvents Click="function(s,e){ ConfirmCancel(); }" />
    </dx:ASPxButton>--%>
    <!--hidden fields-->
    <dx:ASPxHiddenField ID="ASPxHiddenFieldYesDetails" ClientInstanceName="hfYesDetails"
        runat="server">        
    </dx:ASPxHiddenField>
    <dx:ASPxHiddenField ID="ASPxHiddenFieldNoDetails" ClientInstanceName="hfNoDetails"
        runat="server">
    </dx:ASPxHiddenField>
    <dx:ASPxHiddenField ID="ASPxHiddenFieldAccessDetails" ClientInstanceName="hfAccessDetails"
        runat="server">
    </dx:ASPxHiddenField>

    <!--popups-->

	<ppc:Popups ID="PopupTemplates" runat="server" />

</asp:Content>

