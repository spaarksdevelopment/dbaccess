﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="True" CodeBehind="MyRoles.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyRoles" uiculture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
<script type="text/javascript">
    var g_passoffice_Role = 0;
    function DeletePassOfficeRole(passofficeId) 
    {
    	g_passoffice_Role = passofficeId;
    	 popupConfirmTemplate.SetHeaderText('<%= GetPopupDisplayText("HeaderText") %>');
    	 popupConfirmTemplateContentHead.SetText('<%= GetPopupDisplayText("ContentHeadText") %>');
    	 popupConfirmTemplateContentBody.SetText('<%= GetPopupDisplayText("ContentBodyText") %>');
        popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmRemoveRole();");
        popupConfirmTemplate.Show();
    }

    function ConfirmRemoveRole() {
        if (g_passoffice_Role == 0)
            return;
        var passofficeID = g_passoffice_Role;
        PageMethods.DeletePassOfficeRoleUser(passofficeID, OnDeletePassOfficeRole);
    }

    function OnDeletePassOfficeRole(success) 
    {
        if (!success) {
            popupAlertTemplateContent.SetText("<%=MyRolesDeleteRolePrompt.Text %>"); //There was a problem deleting the role.
            popupAlertTemplate.SetHeaderText("<%=MyRoleDelete.Text %>");
            popupAlertTemplate.Show();
            return;
        }

		gvAccessAreaRole.PerformCallback();
    }

    var g_Msp_Role = 0;
    function DeleteMspRole(mspID) {
    	g_Msp_Role = mspID;
        popupConfirmTemplate.SetHeaderText('<%= GetPopupDisplayText("HeaderText") %>');
        popupConfirmTemplateContentHead.SetText('<%= GetPopupDisplayText("ContentHeadText") %>');
        popupConfirmTemplateContentBody.SetText('<%= GetPopupDisplayText("ContentBodyText") %>');
        popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmRemoveMspRole();");
        popupConfirmTemplate.Show();
    }

    function ConfirmRemoveMspRole() {
        if (g_Msp_Role == 0)
            return;

        PageMethods.DeleteMspRoleUser(g_Msp_Role, OnDeleteMspRole);
    }

    function OnDeleteMspRole(success) {
        if (!success) {
            popupAlertTemplateContent.SetText("<%=MyRolesDeleteRolePrompt.Text %>");
            popupAlertTemplate.SetHeaderText("<%=MyRoleDelete.Text %>");
            popupAlertTemplate.Show();
            return;
        }

		gvAccessAreaRole.PerformCallback();
    }

    var _DivisionRoleUserID = 0;

    function DeleteDivisionRole(divisionRoleUserID) {
		_DivisionRoleUserID = divisionRoleUserID
		popupConfirmTemplate.SetHeaderText('<%= GetPopupDisplayText("HeaderText") %>');
		popupConfirmTemplateContentHead.SetText('<%= GetPopupDisplayText("ContentHeadText") %>');
		popupConfirmTemplateContentBody.SetText('<%= GetPopupDisplayText("ContentBodyText") %>');
        popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmDeleteDivisionRole();");
        popupConfirmTemplate.Show();
    }

    function ConfirmDeleteDivisionRole() 
	{
		if (_DivisionRoleUserID == 0)
            return;
		PageMethods.DeleteDivisionRoleUser(_DivisionRoleUserID, OnDeleteDivisionRole);

        _DivisionRoleUserID = 0;
    }

    function OnDeleteDivisionRole(success) {
        if (!success) {
            popupAlertTemplateContent.SetText("<%=MyRolesDeleteRolePrompt.Text %>");
            popupAlertTemplate.SetHeaderText("<%=MyRoleDelete.Text %>");
            popupAlertTemplate.Show();
            return;
        }

		gvAccessAreaRole.PerformCallback();
    }

    _AccessAreaApproverID = 0;
    _HRClassificationID = 0;
    _corportateDivisionAccessAreaId = 0;
    _RoleID =0;
    function DeleteAARole(AccessAreaApproverID, HRClassificationID, corportateDivisionAccessAreaId, RoleID) {
        _AccessAreaApproverID = AccessAreaApproverID;
        _HRClassificationID = HRClassificationID;
        _corportateDivisionAccessAreaId = corportateDivisionAccessAreaId;
        _RoleID = RoleID;
        popupConfirmTemplate.SetHeaderText('<%= GetPopupDisplayText("HeaderText") %>');
        popupConfirmTemplateContentHead.SetText('<%= GetPopupDisplayText("ContentHeadText") %>');
        popupConfirmTemplateContentBody.SetText('<%= GetPopupDisplayText("ContentBodyText") %>');
        popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmDeleteAARole();");
        popupConfirmTemplate.Show();
    }

    function ConfirmDeleteAARole() {        
        if (_AccessAreaApproverID == 0)
            return;
        PageMethods.DeleteAARoleUser(_AccessAreaApproverID, _HRClassificationID, _corportateDivisionAccessAreaId, _RoleID, OnDeleteAARole);

        _AccessAreaApproverID = 0;
        _HRClassificationID = 0;
        _corportateDivisionAccessAreaId = 0;
        _RoleID = 0;
    }

    function OnDeleteAARole(success) {
        if (success == 0) {
            popupAlertTemplateContent.SetText("<%=MyRolesDeleteRolePrompt.Text %>");
            popupAlertTemplate.SetHeaderText("<%=MyRoleDelete.Text %>");
            popupAlertTemplate.Show();
            return;
        }
        else if (success == 2) {
            popupAlertTemplateContent.SetText("<%=MyTasksFewApprovers.Text %>");
            popupAlertTemplate.SetHeaderText("<%=MyRoleDelete.Text %>");
            popupAlertTemplate.Show();
            return;
        }
        gvAccessAreaRole.PerformCallback();
    }


    function DeleteRole(AccessAreaApproverID, HRClassificationID, corportateDivisionAccessAreaId, RoleID, DivisionRoleUserID, PassOfficeUserID) {
       
        switch (RoleID) {
    		//case "Access Approver":
            //case "Access Recertification":
            case 5:
            case 8:
                DeleteAARole(AccessAreaApproverID, HRClassificationID, corportateDivisionAccessAreaId, RoleID);
    			break;
           // case "Divisional Administrator":
            case 3:
				DeleteDivisionRole(DivisionRoleUserID);
				break;
            case 4: //case "Request Approver":
				break;
            case 6://case "Pass Office Operator":
				DeletePassOfficeRole(PassOfficeUserID);
				break;
		}
	}
</script>

<h1>
	<asp:Literal ID="MyRolesHeader" runat="server"></asp:Literal>
</h1>
 <div id="footerLower"></div>
<div>
	<asp:Literal ID="MyRolesDetails" runat="server" meta:resourceKey="MyRolesDetails"></asp:Literal>
</div>
<br />
    <br />

    <asp:Panel ID="AccesAreaPanel" runat="server">
    <h3> <asp:Literal ID="Roles" runat="server" meta:resourceKey="Roles"></asp:Literal> </h3>

    <div> <asp:Literal ID="ExportToExcel" runat="server" meta:resourceKey="ExportToExcel"></asp:Literal>&nbsp;<asp:ImageButton 
			ID="btnUploadExcel" AlternateText="Click here to export this data to Excel" 
			SkinID="FileXLS" runat="server"  OnClick="btnXlsxExport_Click" 
			meta:resourcekey="btnUploadExcelResource1" />   </div>
    <br />
	<dx:ASPxGridView ID="ASPxGridViewAccessAreaRoles" 
			ClientInstanceName="gvAccessAreaRole" OnCustomCallback="ASPxGridViewAccessAreaRoles_CustomCallback"
        runat="server" AutoGenerateColumns="False" 
			meta:resourcekey="ASPxGridViewAccessAreaRolesResource1">
            <Columns>       
				<dx:GridViewDataImageColumn VisibleIndex="0" ShowInCustomizationForm="True">
					<DataItemTemplate>                     
						 <asp:Image runat="server" ID="removeImage" SkinID="Trash" 
							 AlternateText="Delete" onclick='<%# "DeleteRole("+ Eval("CorporateDivisionAccessAreaApproverId") +","+ Eval("HRClassificationID") + "," + Eval("CorporateDivisionAccessAreaID") + "," + Eval("RoleID") + ","+ Eval("DivisionRoleUserID") + "," + Eval("PassOfficeUserID")  + ")"%>' meta:resourcekey="removeImageResource1" />
					</DataItemTemplate>
				</dx:GridViewDataImageColumn>                
				<dx:GridViewDataTextColumn Caption="Role" FieldName="RoleName" 
					ShowInCustomizationForm="True" VisibleIndex="1" 
					meta:resourcekey="GridViewDataTextColumnResource1">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Date Accepted" FieldName="DateAccepted" 
					ShowInCustomizationForm="True" VisibleIndex="2" 
					meta:resourcekey="GridViewDataTextColumnResource2">
					<PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
					</PropertiesTextEdit>
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Last Recertified" 
					FieldName="DateRecertified" ShowInCustomizationForm="True" VisibleIndex="3" 
					meta:resourcekey="GridViewDataTextColumnResource3">
					<PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
					</PropertiesTextEdit>
					<Settings AllowHeaderFilter="True" />
					<CellStyle HorizontalAlign="Right">
					</CellStyle>
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Division" FieldName="DivisionName" 
					ShowInCustomizationForm="True" VisibleIndex="4" 
					meta:resourcekey="GridViewDataTextColumnResource4">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Classification" FieldName="Classification" 
					ShowInCustomizationForm="True" VisibleIndex="5" 
					meta:resourcekey="GridViewDataTextColumnResource5">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Region" FieldName="RegionName" 
					ShowInCustomizationForm="True" VisibleIndex="6" 
					meta:resourcekey="GridViewDataTextColumnResource6">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Country" FieldName="CountryName" 
					ShowInCustomizationForm="True" VisibleIndex="7" 
					meta:resourcekey="GridViewDataTextColumnResource7">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="City" FieldName="CityName" 
					ShowInCustomizationForm="True" VisibleIndex="8" 
					meta:resourcekey="GridViewDataTextColumnResource8">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>            
				<dx:GridViewDataTextColumn Caption="Building" FieldName="BuildingName" 
					ShowInCustomizationForm="True" VisibleIndex="9" 
					meta:resourcekey="GridViewDataTextColumnResource9">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Floor" FieldName="FloorName" 
					ShowInCustomizationForm="True" VisibleIndex="10" 
					meta:resourcekey="GridViewDataTextColumnResource10">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Access Area" FieldName="AccessAreaName" 
					ShowInCustomizationForm="True" VisibleIndex="12" 
					meta:resourcekey="GridViewDataTextColumnResource12">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Access Type" FieldName="AccessAreaType" 
					ShowInCustomizationForm="True" VisibleIndex="13" 
					meta:resourcekey="GridViewDataTextColumnResource13">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VendorName" 
					ShowInCustomizationForm="True" VisibleIndex="14" 
					meta:resourcekey="GridViewDataTextColumnResource14">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Pass Office Name" 
					FieldName="PassOfficeName" ShowInCustomizationForm="True" VisibleIndex="15" 
					meta:resourcekey="GridViewDataTextColumnResource15">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Pass Office Email" 
					FieldName="PassOfficeEmail" ShowInCustomizationForm="True" VisibleIndex="16" 
					meta:resourcekey="GridViewDataTextColumnResource16">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
               
                <dx:GridViewDataTextColumn Caption="" 
					FieldName="RoleID"   VisibleIndex="20" 
					 Visible="false">
					<Settings AllowHeaderFilter="True" />
				</dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="25" />
          <SettingsLoadingPanel  Text="<%$ Resources:CommonResource, Loading %>"/> 
    </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewAccessAreaRoles" ></dx:ASPxGridViewExporter>
    <br />
    <br />
   </asp:Panel>
    
    <ppc:Popups ID="PopupTemplates" runat="server" />
    <!--literals-->
         <asp:Literal ID="MyRolesDeleteRolePrompt"  meta:resourcekey="MyRolesDeleteRolePrompt" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
         <asp:Literal ID="MyRoleDelete"  meta:resourcekey="MyRoleDelete" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
         <asp:Literal ID="MyTasksFewApprovers"  meta:resourcekey="MyTasksFewApprovers" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     
    
</asp:Content>
