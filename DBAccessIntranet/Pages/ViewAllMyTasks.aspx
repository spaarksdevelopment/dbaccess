﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ViewAllMyTasks.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.ViewAllMyTasks" %>
<%@ Register TagPrefix="uc1" Src="~/Controls/LoggedInUserDetails.ascx" TagName="LoggedInUserDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
<h1>View All Tasks</h1>
<div id="alltasks">
    <p>Click here to export this data to Excel.<asp:ImageButton ID="ImageButton2" AlternateText="Click here to export this data to Excel" SkinID="FileXLS" runat="server"  OnClick="btnXlsxExportPO_Click" />  </p> 
    <dx:ASPxGridView ID="ASPxGridViewTasks" ClientInstanceName="gvViewAllTasks" runat="server">
        <Columns>
                <dx:GridViewDataTextColumn Caption="TaskId" FieldName="TaskId" ShowInCustomizationForm="True" VisibleIndex="1"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="TaskDescription" FieldName="TaskDescription" ShowInCustomizationForm="True" VisibleIndex="2"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CreatedBy" FieldName="CreatedBy" ShowInCustomizationForm="True" VisibleIndex="3"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CreatedDate" FieldName="CreatedDate" ShowInCustomizationForm="True" VisibleIndex="4" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy HH:mm:ss}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CompletedDate" FieldName="CompletedDate" ShowInCustomizationForm="True" VisibleIndex="5" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy HH:mm:ss}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="TaskType" FieldName="TaskType" ShowInCustomizationForm="True" VisibleIndex="6"></dx:GridViewDataTextColumn>               
                <dx:GridViewDataTextColumn Caption="TaskStatus" FieldName="TaskStatus" ShowInCustomizationForm="True" VisibleIndex="7"></dx:GridViewDataTextColumn>               
            </Columns>
            <SettingsText EmptyDataRow="You don't have any tasks." />
    </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewTasks">
    </dx:ASPxGridViewExporter>
    <uc1:LoggedInUserDetails ID="LoggedInUserDetailsControl" runat="server"/>
</div>    
</asp:Content>

