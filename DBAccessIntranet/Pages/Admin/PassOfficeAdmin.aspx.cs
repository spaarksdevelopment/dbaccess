﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.CustomMembershipManager;
using System.Threading;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class PassOfficeAdmin : BaseSecurityPage
    {
		protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //add necessary scripts
            base.AddHeaderScriptInclude("~/scripts/ValidationService.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            	LoadPassOffice();
            
            BindGrid();
        }

        private void BindGrid()
		{
			int passOfficeID = Convert.ToInt32(ASPxPassOffice.SelectedItem.Value.ToString().Trim());

			var listPassOfficeUsers = new List<vw_PassOfficeDetails>();

            if (passOfficeID > 0)
                listPassOfficeUsers = Director.GetPassOfficeDetails(passOfficeID);
            		
			ASPxGridViewPassOfficeDetails.DataSource = listPassOfficeUsers.ToList();
            ASPxGridViewPassOfficeDetails.DataBind();
		}

        private void LoadPassOffice()
        {           
            ListEditItem lsttop1 = new ListEditItem();

            lsttop1.Text = GetResourceManager("Resources.CommonResource", "PleaseSelect") ;
            lsttop1.Value = "-1";
            ASPxPassOffice.Items.Add(lsttop1);

            string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
            int? LanguageId = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);

            if (!IsRestrictedUser)
            {
                List<GetAllPassOffices> listPassOffice = Director.GetAllPassOfficesEnabled(LanguageId);

                //set the value to of passoffice user
                for (int i = 0; i <= listPassOffice.Count - 1; i++)
                {
                    if (!String.IsNullOrEmpty(listPassOffice[i].Name))
                    {
                        ListEditItem lsttop0 = new ListEditItem();
                        lsttop0.Value = Convert.ToString(listPassOffice[i].PassOfficeID);
                        lsttop0.Text = Convert.ToString(listPassOffice[i].DisplayName);
                        ASPxPassOffice.Items.Add(lsttop0);
                    }
                }
                ASPxPassOffice.SelectedIndex = 0;
            }
            else
            {
                List<GetAllPassOffices> listPassOffices = Director.GetEnabledPassOfficeDetailsByCountryID(LanguageId, RestrictedCountryId);
               
                foreach (GetAllPassOffices listItem in listPassOffices)
                {
                    if (!String.IsNullOrEmpty(listItem.Name))
                    {
                        ListEditItem lsttop0 = new ListEditItem();
                        lsttop0.Value = Convert.ToString(listItem.PassOfficeID);
                        lsttop0.Text = Convert.ToString(listItem.DisplayName);
                        ASPxPassOffice.Items.Add(lsttop0);
                    }
                }
                ASPxPassOffice.SelectedIndex = 0;
            }
        }

        protected void btnXlsxExportPO_Click(object sender, EventArgs e)
        {
			BindGrid();
			ASPxGridViewPassOfficeDetails.BeginUpdate();
			ASPxGridViewPassOfficeDetails.DataBind();
			ASPxGridViewPassOfficeDetails.EndUpdate();

			gridExport.DataBind();

			gridExport.WriteXlsxToResponse();
		}

        protected void PassOfficeRole_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
             BindGrid();
        }

		[WebMethod(EnableSession = true), ScriptMethod]
		public static int AddPerson(int personID, int requestMasterID, int passOfficeID)
		{
			HRPerson person = Director.GetPersonById(personID);

			if (person == null) return 0;		// we cannot find the person

            int dbPeopleID = person.dbPeopleID;

			// update the list with this person
			// add in details so that we can see what this user is in the grid
			vw_PassOfficeDetails newPerson = new vw_PassOfficeDetails();

            newPerson.dbPeopleID = dbPeopleID;
			newPerson.PassOfficeID = passOfficeID;
			newPerson.mp_SecurityGroupID = (Int32)DBAccessEnums.RoleType.PassOfficeOperator;
			newPerson.IsEnabled = false;
			newPerson.SurName = person.Surname;
			newPerson.ForeName = person.Forename;
			newPerson.Email = person.EmailAddress;
			newPerson.CreatedDateTime = DateTime.Now;

			// no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = new DBSqlMembershipProvider().GetUser();

			// Now create the badge request
            Director director = new Director(CurrentUser.dbPeopleID);
            bool bsuccesRequest = false;

			int bSuccess = director.CreateNewRequestForPassOffice(personID, ref requestMasterID, passOfficeID);

            if(bSuccess != 0 && bSuccess != -3)
            bsuccesRequest = director.SubmitRequestGeneric(requestMasterID);

            if (bsuccesRequest)
               director.InsertNewPassOfficeUser(personID, passOfficeID);

            return (bsuccesRequest) ? requestMasterID : bSuccess;
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static Boolean DeletePerson(int userID, int passOfficeId)
		{
            DBAppUser CurrentUser = new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.DeletePassOfficeUserByUserId(userID, passOfficeId);
            return success;
		}

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
	}
}
