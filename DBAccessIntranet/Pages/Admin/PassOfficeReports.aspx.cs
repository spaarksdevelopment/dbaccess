﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;
using Spaarks.Common.UtilityManager;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class PassOfficeReports : BaseSecurityPage
    {
        
        protected void Page_Init(object sender, EventArgs e)
        {
            PopulatePassOfficeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isPagerCallback = Request.Form["__CALLBACKID"] != null && Request.Form["__CALLBACKID"].Contains("SearchResults");

            if (!Page.IsPostBack && !isPagerCallback)
            {
                PopulateCountryDropDown();
                PopulateStatusDropDown();
                PopulateTypeDropDown();
                PopulatePassOfficeList();
                PopulateRequestStatusDropDown();
                m_taskDateStartDate.Date = DateTime.Now.AddMonths(-1);
                m_taskDateEndDate.Date = DateTime.Now;

            }
            else if (!Page.IsCallback || isPagerCallback)
            {
                //  Check for the between visible
                if (m_personDetailsNameSelector.Value.ToString() == "Between")
                {
                    m_personDetailsNameBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsNameBetweenText.Text = "";
                }

                if (m_personDetailsEmployeeIdSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmployeeIdBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmployeeIdBetweenText.Text = "";
                }

                if (m_personDetailsEmailSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmailBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmailBetweenText.Text = "";
                }

                if (m_personDetailsDbDirSelector.Value.ToString() == "Between")
                {
                    m_personDetailsDbDirBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsDbDirBetweenText.Text = "";
                }

                if (m_requestorDetailsNameSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsNameBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsNameBetweenText.Text = "";
                }

                if (m_requestorDetailsEmployeeIdSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsEmployeeIdBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsEmployeeIdBetweenText.Text = "";
                }

                if (m_requestorDetailsEmailSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsEmailBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsEmailBetweenText.Text = "";
                }

                if (m_requestorDetailsDbDirSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsDbDirBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsDbDirBetweenText.Text = "";
                }                
            }

            if (isPagerCallback)
            {
                PerformSearch();
            }

            m_passOfficeholder.Visible =  base.CurrentUser.RolesShortNamesList.Contains("ADMIN") == true;
        }

        #region click events
        protected void m_searchSubmit_click(object sender, EventArgs e)
        {
            PerformSearch();
        }

        protected void m_seearchReset_click(object sender, EventArgs e)
        {
            //  Redirect to the original search page to reset all inputs and output grid view status
           Response.Redirect("/Pages/Admin/PassOfficeReports.aspx", true);
        }

        #endregion

        #region callbacks
        protected void m_city_Callback(object sender, CallbackEventArgsBase e) 
        {
            int countryID = Convert.ToInt32(Number.NullableInt32(e.Parameter.ToString()));
            PopulateCityDropDown(countryID);
        }

        protected void m_Building_Callback(object sender, CallbackEventArgsBase e) 
        {          
            int cityID =Convert.ToInt32( Number.NullableInt32(e.Parameter.ToString()));
            PopulateBuildingDropDown(cityID);
        }

        protected void m_Floor_Callback(object sender, CallbackEventArgsBase e) 
        {
            int buildingID = Convert.ToInt32(Number.NullableInt32(e.Parameter.ToString()));
            PopulateFloorDropDown(buildingID);
        }

        protected void m_accessArea_Callback(object sender, CallbackEventArgsBase e) 
        {
            int floorId = Convert.ToInt32(Number.NullableInt32(e.Parameter.ToString()));
            PopulateAccessAreaDropDown(floorId);
        }
        #endregion

        #region Population Methods
        
        private void PopulateStatusDropDown()
        {
            List<TaskStatusLookup> requestStatus;
            using (var context = new DBAccessEntities())
            {
                var items = from t in context.TaskStatusLookups
                            select t;

                requestStatus = items.ToList();
            }

            m_passOfficeStatus.DataSource = requestStatus;
            m_passOfficeStatus.DataBind();
        }

        private void PopulateRequestStatusDropDown()
        {
            List<AccessRequestStatu> requestStatus;
            using (var context = new DBAccessEntities())
            {
                var items = from t in context.AccessRequestStatus
                            select t;

                requestStatus = items.ToList();
            }

            m_requestStatus.DataSource = requestStatus;
            m_requestStatus.DataBind();
        }
     
        private void PopulateTypeDropDown()
        {
            var reqTypes = StoredProcedureManager.GetRequestTypesForReportSearching().OrderBy(e => e.RequestType);
            m_requestType.DataSource = reqTypes.ToList();
            m_requestType.DataBind();
         
        }

        public bool IsSwissPassOfficeUser
        {

            get{

                bool toReturn = false;

                //  Get the pass office for the user
                int dbPeopleId = 0;

                if (base.CurrentUser == null)
                {
                    DBAppUser theCurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                    dbPeopleId = theCurrentUser.dbPeopleID;
                }
                else
                {
                    dbPeopleId = base.CurrentUser.dbPeopleID;
                }

                List<LocationPassOffice> usersPassOffices = Director.GetUserPassOffice(dbPeopleId);

                 string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];

                 List<LocationPassOffice> countryPassOffice = Director.GetPassOfficesByCountryId(Int32.Parse(switzerlandStringId));

                //  does it only contain 1 and is it switzerland?
                 if (usersPassOffices != null && countryPassOffice != null && usersPassOffices.Count == 1 && countryPassOffice.Count == 1 && countryPassOffice.First().PassOfficeID == usersPassOffices.First().PassOfficeID)
                {
                    toReturn = true;
                }


                return toReturn;
            }

        }

        private void PopulateCountryDropDown()
        {
           List<LocationCountry> countries = new List<LocationCountry>();

           if (IsRestrictedUser)
            {
                countries = new List<LocationCountry>();
                countries.Add(Director.GetCountry(RestrictedCountryId));
            }
           else if (IsSwissPassOfficeUser)
           {
               string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
               countries = new List<LocationCountry>();
               countries.Add(Director.GetCountry(Int32.Parse(switzerlandStringId)));
           }
           else if (countries != null)
           {
               countries = Director.GetCountriesByDbPeopleID(base.CurrentUser.dbPeopleID, base.CurrentUser.LanguageId);

               if (countries != null && countries.Count > 0)
               {
                   countries.OrderBy(e => e.Name);
               }
           }

            m_countryArea.Items.Add("--Select--", 0).Selected = true;

            foreach (var country in countries)
            {
                m_countryArea.Items.Add(country.Name, country.CountryID);
            }
        }

        private void PopulateCityDropDown(int countryID)
        {
            var cities = Director.GetCitiesForCountry(countryID, true).OrderBy(e => e.Name);

            if (m_cityArea.Items.Count >= 1)
            {
                m_cityArea.Items.Clear();
            }

            if (m_countryArea.Value == null || Convert.ToInt32(Number.NullableInt32(m_countryArea.Value.ToString())) == 0) return;

            m_cityArea.Items.Add("--Select--",0);

                foreach(var city in cities)
                {
                    m_cityArea.Items.Add(city.Name, city.CityID);
                }                     
        }
      
        
        private void PopulateBuildingDropDown(int cityID)
        {
            var buildings = Director.GetBuildingsEnabledForCity(cityID, false).OrderBy(e => e.Name);

            if (m_buildingArea.Items.Count >= 1)
            {
                m_buildingArea.Items.Clear();
            }
          
            m_buildingArea.Items.Add(" --Select-- ", 0);
            foreach (var bld in buildings) 
            {
                m_buildingArea.Items.Add(bld.Name, bld.BuildingID);
            }     
        }
      
        private void PopulateFloorDropDown(int buildingID)
        {
            var floors = Director.GetFloorsForBuilding(buildingID,true,false,false).OrderBy(e => e.Name);

            if (m_floorArea.Items.Count >= 1)
            {
                m_floorArea.Items.Clear();
            }

            if (m_buildingArea.Value == null || Convert.ToInt32(Number.NullableInt32(m_buildingArea.Value.ToString())) == 0) return;
        
            m_floorArea.Items.Add(" --Select-- ", 0);
            foreach (var flr in floors)
            {
                m_floorArea.Items.Add(flr.Name, flr.FloorID);
            } 
        }

        private void PopulateAccessAreaDropDown(int floorID)
        {
            var floors = Director.GetAccessAreasForFloor(floorID, true, false).OrderBy(e => e.Name);

            if (m_accessArea.Items.Count >= 1)
            {
                m_accessArea.Items.Clear();
            }

            if (m_floorArea.Value == null || Convert.ToInt32(Number.NullableInt32(m_floorArea.Value.ToString())) == 0) return;

            m_accessArea.Items.Add(" --Select-- ", 0);
            foreach (var flr in floors)
            {
                m_accessArea.Items.Add(flr.Name, flr.AccessAreaID);
            } 
        }

        
        private void PopulatePassOfficeList()
        {
            if (IsRestrictedUser)
            {
                List<GetAllPassOffices> passOffices = Director.GetEnabledPassOfficeDetailsByCountryID(1, RestrictedCountryId);
                m_passOfficeList.DataSource = passOffices;
                m_passOfficeList.DataBind();                              
            }
            else if (IsSwissPassOfficeUser)
            {
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                List<GetAllPassOffices> switzerlandPassOffice = Director.GetEnabledPassOfficeDetailsByCountryID(1, Int32.Parse(switzerlandStringId));
                m_passOfficeList.DataSource = switzerlandPassOffice;
                m_passOfficeList.DataBind();
            }
            else
            {
                m_passOfficeList.DataSource = Director.GetAllPassOfficesEnabled(1).OrderBy(e => e.DisplayName);
                m_passOfficeList.DataBind();                
            }           
        }

        #endregion

        public int? GetLanguageID()
        {
            return base.CurrentUser.LanguageId;

        }
        private void PerformSearch()
        {
            DateTime? TaskCreatedStartDate = m_taskDateStartDate.Date != DateTime.MinValue ? m_taskDateStartDate.Date : new Nullable<DateTime>();
            DateTime? TaskCreatedEndDate = m_taskDateEndDate.Date != DateTime.MinValue ? m_taskDateEndDate.Date : new Nullable<DateTime>();
            DateTime? TaskCompletedStartDate = m_taskDateCompletedStartDate.Date != DateTime.MinValue ? m_taskDateCompletedStartDate.Date : new Nullable<DateTime>();
            DateTime? TaskCompletedEndDate = m_taskDateCompletedEndDate.Date != DateTime.MinValue ? m_taskDateCompletedEndDate.Date : new Nullable<DateTime>();

            bool performSearch = true;

            if (!TaskCreatedStartDate.HasValue || !TaskCreatedEndDate.HasValue)
            {
                m_voidDateErrorMessage.Visible=true;
                m_dateSpanErrorMessage.Visible = false;
                performSearch = false;
            }
            else if ((TaskCreatedEndDate.GetValueOrDefault() - TaskCreatedStartDate.GetValueOrDefault()).Days > 31)
            {
                m_dateSpanErrorMessage.Visible = true;
                m_voidDateErrorMessage.Visible = false;
                performSearch = false;
            }
            else
            {
                m_dateSpanErrorMessage.Visible = false;
                m_voidDateErrorMessage.Visible = false;
            }

            int? fullRequestStatusId = SafelyGetComboBoxValue(m_requestStatus.Value);
            int? statusId = SafelyGetComboBoxValue(m_passOfficeStatus.Value);
            int? typeID = SafelyGetComboBoxValue(m_requestType.Value);
            int? countryID = SafelyGetComboBoxValue(m_countryArea.Value);
            int? cityID = SafelyGetComboBoxValue(m_cityArea.Value);
            int? buildingID = SafelyGetComboBoxValue(m_buildingArea.Value);
            int? floorID = SafelyGetComboBoxValue(m_floorArea.Value);
            int? accessAreaId = SafelyGetComboBoxValue(m_accessArea.Value);


            if (performSearch)
            {
                string personNameSearchType = m_personDetailsNameSelector.Value.ToString();
                string personNameValueStart = m_personDetailsNameText.Text;
                string personNameValueEnd = m_personDetailsNameBetweenText.Text;

                string personEmployeeIdSelector = m_personDetailsEmployeeIdSelector.Value.ToString();
                string personEmployeeIdText = m_personDetailsEmployeeIdText.Text;
                string personEmployeeIdTextBetween = m_personDetailsEmployeeIdBetweenText.Text;

                string personEmailSelector = m_personDetailsEmailSelector.Value.ToString();
                string personEmailText = m_personDetailsEmailText.Text;
                string personEmailTextBetween = m_personDetailsEmailBetweenText.Text;

                string personDbDirSelector = m_personDetailsDbDirSelector.Value.ToString();
                string personDbDireText = m_personDetailsDbDirText.Text;
                string personDbDirTextBetween = m_personDetailsDbDirBetweenText.Text;

                //requestor
                string requestorNameSearchType = m_requestorDetailsNameSelector.Value.ToString();
                string requestorNameValueStart = m_requestorDetailsNameText.Text;
                string requestorNameValueEnd = m_requestorDetailsNameBetweenText.Text;


                string requestorEmployeeIdSelector = m_requestorDetailsEmployeeIdSelector.Value.ToString();
                string requestorEmployeeIdText = m_requestorDetailsEmployeeIdText.Text;
                string requestorEmployeeIdTextBetween = m_requestorDetailsEmployeeIdBetweenText.Text;

                string requestorEmailSelector = m_requestorDetailsEmailSelector.Value.ToString();
                string requestorEmailText = m_requestorDetailsEmailText.Text;
                string requestorEmailTextBetween = m_requestorDetailsEmailBetweenText.Text;

                string requestorDbDirSelector = m_requestorDetailsDbDirSelector.Value.ToString();
                string requestorDbDireText = m_requestorDetailsDbDirText.Text;
                string requestorDbDirTextBetween = m_requestorDetailsDbDirBetweenText.Text;

                string passOffices = "";

                foreach (ListEditItem item in m_passOfficeList.SelectedItems)
                {
                    passOffices += "," + item.Value.ToString();
                }

                if (passOffices.Length > 0)
                {
                    passOffices = passOffices.Substring(1);
                }

                if (IsRestrictedUser)
                {
                    countryID = RestrictedCountryId;
                }
                else if (IsSwissPassOfficeUser)
                {
                    string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                    countryID = Int32.Parse(switzerlandStringId);
                }

                List<sp_PassOfficeReportSearch_Result> results = StoredProcedureManager.PassOfficeReportSearch(base.CurrentUser.LanguageId, TaskCreatedStartDate, TaskCreatedEndDate,
                    TaskCompletedStartDate, TaskCompletedEndDate, personNameSearchType, personNameValueStart, personNameValueEnd,
                    personEmployeeIdSelector, personEmployeeIdText, personEmployeeIdTextBetween, personEmailSelector, personEmailText, personEmailTextBetween
                    , personDbDirSelector, personDbDireText, personDbDirTextBetween
                    , requestorNameSearchType, requestorNameValueStart, requestorNameValueEnd, requestorEmployeeIdSelector, requestorEmployeeIdText, requestorEmployeeIdTextBetween
                    , requestorEmailSelector, requestorEmailText, requestorEmailTextBetween, requestorDbDirSelector, requestorDbDireText, requestorDbDirTextBetween
                    , countryID, cityID, buildingID, floorID, typeID, statusId, passOffices, base.CurrentUser.dbPeopleID, fullRequestStatusId, accessAreaId
                   );

                SearchResults.DataSource = results;
                SearchResults.DataBind();
                m_searchOutputHolder.Visible = true;
            }
            else
            {
                m_searchOutputHolder.Visible = false;
            }

        }

        
        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            PerformSearch();
            gridExport.WriteXlsxToResponse();
        }

        private int? SafelyGetComboBoxValue(object selectedValue)
        {

            object passOffiveObjectValue = selectedValue;
            int workingId = 0;
            int? Id = new Nullable<int>();

            if (selectedValue != null && !String.IsNullOrEmpty(selectedValue.ToString()) && Int32.TryParse(selectedValue.ToString(), out workingId))
            {
                Id = workingId;
            }

            return Id == 0 ? null:Id ;
        }


    }
}