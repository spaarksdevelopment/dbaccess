﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="PassOfficeAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.PassOfficeAdmin"%>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register src="~/Controls/Selectors/PersonSelector.ascx" tagname="PersonSelector" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <script type="text/javascript">

    	var g_iUserId = 0;
    	var g_iPassOfficeId = 0;

    	function DeletePerson(iUserId, iPassOfficeId)
    	{
    		g_iUserId = iUserId;
    		g_iPassOfficeId = iPassOfficeId;

    		popupConfirmTemplate.SetHeaderText("<%=PassOfficeRemovePerson.Text %>");  //Remove Person
    		popupConfirmTemplateContentHead.SetText('<%= GetResourceManager("Resources.CommonResource", "Person") %>');
    		popupConfirmTemplateContentBody.SetText('<%= GetResourceManager("Resources.CommonResource", "RemovePersonContent") %>');  //Are you sure you want to remove this Person?
    		popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemovePerson();");
    		popupConfirmTemplate.Show();
    	}

    	function OnRemovePerson()
    	{
    		PageMethods.DeletePerson(g_iUserId, g_iPassOfficeId, OnDeletePerson);
    	}

    	function OnDeletePerson(result)
    	{
    		if (!result)
    		{
    		    popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "DeletePerson") %>');
    		    popupAlertTemplateContent.SetText('<%= GetResourceManager("Resources.CommonResource", "ProblemDeletingPerson") %>');   //"Sorry, there was a problem deleting the Person."
    			popupAlertTemplate.Show();
    			return;
    		}
			PassOfficeRole.PerformCallback();		// reload the grid
		}

    	function AddPerson()
    	{
    		var iPassOfficeId = ASPxPassOffice.GetValue();

    		if (iPassOfficeId == -1)
    		{
    		    popupAlertTemplateContent.SetText("<%=PassOfficeSelectType.Text %>"); //Please slect the type of Pass Office.
    		    popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "SubmitAlternate") %>');
    			popupAlertTemplate.Show();
    			return;
    		}

    		var idstring = $get("PersonSelectorNameTextBox").value;
    		var personId = 0;

    		if (idstring.length > 0)
    		{
    			SetServicesFolderPath("/../Services/");
    			personId = GetPersonIdFromIdentifier(idstring);
    		}

    		if (personId == 0)
    		{
    		    popupAlertTemplateContent.SetText("<%=PassOfficiceAddPerson.Text %>");  //Please add at least one Person.
    			popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "SubmitAlternate") %>');
    			popupAlertTemplate.Show();
    			return;
    		}

    		var iRequestMasterId = 0;

    		PageMethods.AddPerson(personId, iRequestMasterId, iPassOfficeId, OnAddPerson);
    	}

    	function OnAddPerson(result)
    	{
			if (result == 0)
			{
			    popupAlertTemplateContent.SetText("<%=PassOfficePersonNotAdded.Text %>");  //Person could not be added.
			    popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "AddPerson") %>'); 
				popupAlertTemplate.Show();
				return;
			}

			if (result == -3)
			{
			    popupAlertTemplateContent.SetText('<%= GetResourceManager("Resources.CommonResource", "CannotAddInactivePerson") %>');   //"Sorry! cannot add Person as he is inactive"
			    popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "AddPerson") %>');
				popupAlertTemplate.Show();
				return;
			}

			$get("PersonSelectorNameTextBox").value = '';

			// we need to reload the grid
			PassOfficeRole.PerformCallback();
    	}

    	function PassOfficeChanged()
    	{
			// update the grid (or make it empty if no pass office selected)
   			PassOfficeRole.PerformCallback();
   		}

   		function PassOffice_EndCallback()
   		{
   			// set the state of the Add Person button on the grid
   			btnAdd.SetEnabled(-1 != ASPxPassOffice.GetValue());
   		}

    	$(document).ready(function ()
    	{
    	    $('#AccessHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
    	    $('#AccessHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');


    		$('#AccessHelp').bind('click', function ()
    		{
    			$("#AccessHelpInfo").slideToggle("slow");
    		});
    		$('#AccessHelpInfo').hide();

    		// set the state of the Add Person button on the grid
    		btnAdd.SetEnabled(-1 != ASPxPassOffice.GetValue());
    	});

	</script>

<h1><asp:Literal ID="PassOfficeAdminLiteral" meta:resourcekey="PassOfficeAdmin" runat="server"></asp:Literal></h1>
<p> <asp:Literal ID="PassOfficeAdminS" Text="<%$ Resources:CommonResource, PassOfficeAdmin%>" runat="server"></asp:Literal><img id="AccessHelp" src="../../App_Themes/DBIntranet2010/images/question.gif"  /></p>
    <br />
    <div id="AccessHelpInfo" class="HelpPopUp">
        <p><asp:Literal ID="PassOfficeGetCurrent" meta:resourcekey="PassOfficeGetCurrent" runat="server"></asp:Literal></p>       
        <p> <asp:Literal ID="PassOfficeUseIcon" meta:resourcekey="PassOfficeUseIcon" runat="server"></asp:Literal>
                <asp:Image ID="Help" SkinID="FileXLS" AlternateText="<%$ Resources:CommonResource, ExportData%>" runat="server"/>
        </p>      
    </div>
    <br /> 
    <div>
        <asp:Table runat="server">
                <asp:TableRow runat="server">
                        <asp:TableCell>
                                <asp:Label ID="lblPassOfficeAdmin" runat="server" meta:resourcekey="PassOfficeSelect"></asp:Label>
                         </asp:TableCell>
                         <asp:TableCell>
                                <dx:ASPxComboBox ID="ASPxPassOffice" ClientInstanceName="ASPxPassOffice" runat="server"
                                    ClientSideEvents-SelectedIndexChanged="PassOfficeChanged" AutoPostBack="false" Width="280px">
                                </dx:ASPxComboBox>
                          </asp:TableCell>
                 </asp:TableRow>                 
        </asp:Table>
        <br />
    </div>
    <div id="passOfficePanel">
        <p>
			<asp:Literal ID="ClickToExportToExcel" Text="<%$ Resources:CommonResource, ClickToExportToExcel%>" runat="server"></asp:Literal>
			<asp:ImageButton ID="btnUploadExcel" AlternateText="<%$ Resources:CommonResource, ClickToExportToExcel%>" SkinID="FileXLS" runat="server" OnClick="btnXlsxExportPO_Click" />
		</p>
        <dx:ASPxGridView ID="ASPxGridViewPassOfficeDetails" ClientInstanceName="PassOfficeRole" runat="server"
			KeyFieldName="PassOfficeUserID"
			AutoGenerateColumns="false"
			OnCustomCallback="PassOfficeRole_CustomCallback">
			<ClientSideEvents EndCallback="function (s,e) { PassOffice_EndCallback(); }" />
            <Columns>
				<dx:GridViewDataImageColumn VisibleIndex="0">
					<DataItemTemplate>
						<asp:Image runat="server" ID="removeImage" SkinID="Trash" AlternateText="<%$ Resources:CommonResource, Remove%>" style="cursor: pointer;" onclick='<%# "DeletePerson(" + Eval("dbPeopleID") + "," + Eval("PassOfficeID") + ");" %>' />
					</DataItemTemplate>
				</dx:GridViewDataImageColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, ForeName%>" FieldName="ForeName" ShowInCustomizationForm="True" VisibleIndex="3"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, SurName%>" FieldName="SurName" ShowInCustomizationForm="True" VisibleIndex="4"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, UserEmail%>" FieldName="Email" ShowInCustomizationForm="True" VisibleIndex="5"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Created%>" FieldName="CreatedDateTime" ShowInCustomizationForm="True" VisibleIndex="6" PropertiesTextEdit-DisplayFormatString="{0:dd-MMM-yyyy HH:mm:ss}"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Enabled%>" FieldName="IsEnabled" ShowInCustomizationForm="True" VisibleIndex="7"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, TelephoneLiteral%>" FieldName="PassOfficeTelephone" ShowInCustomizationForm="True" VisibleIndex="8"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, PassOfficeEmail%>" FieldName="PassOfficeEmail" ShowInCustomizationForm="True" VisibleIndex="9"></dx:GridViewDataTextColumn>
            </Columns>
			<Settings ShowStatusBar="Visible" />
            <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>"/>
			<Templates>
				<StatusBar>
					<dx:ASPxButton ID="btnAdd" ClientInstanceName="btnAdd" runat="server"
						Text="<%$ Resources:CommonResource, AddPerson%>"
						AutoPostBack="false">
						<ClientSideEvents Click="function(s,e){ popupAddPerson.Show(); }" />
					</dx:ASPxButton>
				</StatusBar>
			</Templates>
        </dx:ASPxGridView>
		<br />
	<%--	<dx:ASPxButton ID="btnConfirm" AutoPostBack="false" runat="server" Text="<%$ Resources:CommonResource, ConfirmChanges%>">
			<ClientSideEvents Click="ConfirmChanges" />
		</dx:ASPxButton>--%>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewPassOfficeDetails" ></dx:ASPxGridViewExporter>
    </div>

	<dx:ASPxPopupControl ID="ASPxAddPerson" ClientInstanceName="popupAddPerson" runat="server"
		SkinID="PopupCustomSize"
		Width="500px"
		Height="250px"
		meta:resourcekey="PassOfficiceAddPersonHead">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
				<h2><asp:Literal ID="PassOfficiceAddPersonH" meta:resourcekey="PassOfficiceAddPersonH" runat="server"></asp:Literal></h2>
				<p><asp:Literal ID="PassOfficeAddPersonDetails" meta:resourcekey="PassOfficeAddPersonDetails" runat="server"></asp:Literal></p>
				<br />
				<uc1:PersonSelector ID="PersonSelector1" ServiceMethod="GetPersonCompletionListWithValidIds" runat="server" /> 
				<br />
				<br />
				<br />
				<div class="PopupConfirm">
					<div>
						<dx:ASPxButton ID="ASPxButtonAdd" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Add%>">
							<ClientSideEvents Click="function(s,e){ popupAddPerson.Hide(); AddPerson(); }" />
						</dx:ASPxButton>
						<dx:ASPxButton ID="ASPxButtonCancel" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Cancel%>" ForeColor="#0098db">
							<ClientSideEvents Click="function(s,e){ popupAddPerson.Hide(); }" />
						</dx:ASPxButton>
					</div>
				</div>            
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
    
	<ppc:Popups ID="PopupTemplates" runat="server" />

    <!--literals-->

    <asp:Literal ID="PassOfficeRemovePerson"  meta:resourcekey="PassOfficeRemovePerson" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="PassOfficeSelectType"  meta:resourcekey="PassOfficeSelectType" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="PassOfficePersonNotAdded"  meta:resourcekey="PassOfficePersonNotAdded" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

    <asp:Literal ID="PassOfficeUserUpdated"  meta:resourcekey="PassOfficeUserUpdated" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    <asp:Literal ID="PassOfficiceAddPerson"  meta:resourcekey="PassOfficiceAddPerson" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
          

</asp:Content>
