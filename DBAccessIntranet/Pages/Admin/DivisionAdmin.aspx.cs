﻿using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DBAccess.BLL.Concrete;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class DivisionAdmin : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			base.AddHeaderScriptInclude ("~/scripts/validationservice.js");
        }
             
        private static string GetData(string ParentNode, string countryName, bool IsRoot)
        {
            string id = ParentNode == null ? null : ParentNode.Split('|')[0]; 
            int? existingFilter = GetExistingCountryIDFromSession();
            List<DBAccessController.DAL.UBRDivision> divisions = Director.GetUBRDivisions(id, IsRoot, existingFilter);
                
            string countryIDSString = string.Empty;
            foreach (var division in divisions)
            {
                //Exclude the country we are editing, otherwise you won't be able to save changes when editing an existing division
                if(division.CountryName==countryName)
                    continue;
                countryIDSString += Convert.ToString(division.CountryID) + "|";
            }

            return countryIDSString;
        }

        private static int? GetExistingCountryIDFromSession()
        {
            return HttpContext.Current.Session["CountryID"] == null ? null : (int?)HttpContext.Current.Session["CountryID"];
        }

        private static void SetParentDivisionID(int divisionID)
        {
            //Need parent division ID if this is a subdivision
            CorporateDivision division = Director.GetDivision(divisionID);
            if (Director.IsSubdivision(division))
                HttpContext.Current.Session["ParentDivisionID"] = division.SubdivisionParentDivisionID;
            else
                HttpContext.Current.Session["ParentDivisionID"] = division.UBR;
        }

        //TODO - Node should be defined in it's own class(es) and a lot of code could be removed from DivisionSelectorAdmin
        private static int? GetDivisionIDFromNodeKey(string divisionIDString)
        {
            if (string.IsNullOrWhiteSpace(divisionIDString))
                return null;

            return Convert.ToInt32(divisionIDString.Split('|')[0]);
        }

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }

        #region Web Methods
        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool DeleteDivision(string divisionIDString)
		{
            int? divisionID = GetDivisionIDFromNodeKey(divisionIDString);

            if (!divisionID.HasValue)
                return false;

            SetParentDivisionID(divisionID.Value);
            bool success = Director.DeleteDivision(divisionID.Value) == 0;
            return success;
		}

        [WebMethod()]
        public static string GetCountriesForUBR(string ubrCode, string countryName)
        {
            return GetData(ubrCode, countryName, false);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static void RemoveDraftRequestsForThisDivision(string divisionIDString)
        {
            int? divisionID = GetDivisionIDFromNodeKey(divisionIDString);
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            GenericRequestService genericService = new GenericRequestService();
            genericService.RemoveDraftDivisionRequestsForCurrentUser(divisionID, currentUserID);
        }
        #endregion
    }
}