﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ManageSmartCardTypes.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.ManageSmartCardTypes" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
<script type="text/javascript">
$(document).ready(function ()
{
	hdSmartCardType.Set("smartCardTypeURL", '<%= Page.ResolveUrl("~/Pages/Popup/AddEditSmartCardType.aspx") %>');
});

var popupURLTypeID = "";
var popupTypeID = "";
function EditType(typeId)
{
	//Existing Record
	if (typeId != null) {
		popupTypeID = typeId;
		var popupUrlEditSmartCardType = hdSmartCardType.Get("smartCardTypeURL");
		popupUrlEditSmartCardType = popupUrlEditSmartCardType + "?ID=" + typeId;
		popupURLTypeID = popupUrlEditSmartCardType;

		popupEditSmartCardType.SetContentUrl(popupURLTypeID);
		popupEditSmartCardType.Show();
	}
}

function CancelAddEditSmartCardType()
{
	popupEditSmartCardType.Hide();
}

function RefreshSmartCardTypeGrid()
{
	popupEditSmartCardType.Hide();
	grdViewSmartCardTypes.PerformCallback();
}

function DeleteSmartcardType() {
    popupConfirmTemplate.SetHeaderText("<%=DeleteSmartCard.Text %>"); //Delete dbSmartcard Type
    popupConfirmTemplateContentHead.SetText("<%=SmartCardType.Text %>"); //dbSmartcard Type
    popupConfirmTemplateContentBody.SetText("<%=SmartCardRemoveConfirm.Text %>"); //Are you sure you want to remove this Smartcard Type?
	popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmDeleteSmartcardType();");
	popupConfirmTemplate.Show();
}

function ConfirmDeleteSmartcardType() {
	if (popupTypeID == 0 || popupTypeID == "")
		return;

	PageMethods.DeleteSmartcardType(popupTypeID, OnDeleteSmartcardType);
}

function OnDeleteSmartcardType() {
	popupEditSmartCardType.Hide();
	grdViewSmartCardTypes.PerformCallback();
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
<h1> <asp:Literal ID="SmartCardManageHead" meta:resourcekey="SmartCardManageHead" runat="server"></asp:Literal> </h1>
<dx:ASPxGridView ID="grdViewSmartCardTypes" runat="server" AutoGenerateColumns="false" KeyFieldName="ID" 
 ClientInstanceName="grdViewSmartCardTypes" OnCustomCallback="grdViewSmartCardTypes_CustomCallback">
 <Settings ShowStatusBar="Visible" />
 <Columns>
        <dx:GridViewDataTextColumn Caption="" FieldName="SmartCardProviderID" ShowInCustomizationForm="True" VisibleIndex="5" Width="150px" Visible="false"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn meta:resourcekey="Vendor" FieldName="ProviderName" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn meta:resourcekey="SmartCardTypeHead" FieldName="Type" ShowInCustomizationForm="True" VisibleIndex="2" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn meta:resourcekey="Label" FieldName="Label" ShowInCustomizationForm="True" VisibleIndex="3" Width="150px"></dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn meta:resourcekey="Identifier"  FieldName="TypeIdentifier" ShowInCustomizationForm="True" VisibleIndex="4" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Enabled%>" FieldName="Enabled" ShowInCustomizationForm="True" VisibleIndex="5" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" VisibleIndex="6" ShowInCustomizationForm="True">
                <DataItemTemplate>								
							<a href="javascript:{}" ID="lbBuildingEdit" runat="server" meta:resourcekey="SmartCardEdit" onclick='<%#"EditType(\"" + Eval("ID") + "\");"%>'><asp:Literal ID="EditLiteral" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" runat="server"></asp:Literal> </a>  
                </DataItemTemplate>
        </dx:GridViewDataColumn>
</Columns>
<Templates>
	<StatusBar>
			<dx:ASPxButton ID="dtnAddSmartCardProvider" AutoPostBack="false" runat="server" meta:resourcekey="SmartCardAdd">
				<ClientSideEvents Click='function(s,e){ EditType(0); }' />
			</dx:ASPxButton>
	</StatusBar>
</Templates>
</dx:ASPxGridView>
<dx:ASPxCheckBox ID="ckbEnabled" runat="server"  meta:resourcekey="SmartCardShowDisabled">
    <ClientSideEvents CheckedChanged='function(s,e){ RefreshSmartCardTypeGrid(); }' />
</dx:ASPxCheckBox>
<br />
<br />

<!-- edit division-->
<dx:ASPxPopupControl ID="ASPxPopupControlEditDivision" ClientInstanceName="popupEditSmartCardType" runat="server"
	SkinID="PopupNoHeader"
	 meta:resourcekey="SmartCardTypeHead"
	Width="600px"
	Height="350px"
	ContentUrl="~/Pages/Popup/AddEditSmartCardType.aspx"
	ContentUrlIFrameTitle="SmartCardPopup">
</dx:ASPxPopupControl>


<!--Hidden feid to hold the navigation URl-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldSmartCardVendor" ClientInstanceName="hdSmartCardType" runat="server">
</dx:ASPxHiddenField>

<ppc:Popups ID="PopupTemplates" runat="server" />

<!--Literals-->
 <asp:Literal ID="DeleteSmartCard"  meta:resourcekey="DeleteSmartCard" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
 <asp:Literal ID="SmartCardType"  meta:resourcekey="SmartCardType" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
 <asp:Literal ID="SmartCardRemoveConfirm"  meta:resourcekey="SmartCardRemoveConfirm" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
 

</asp:Content>
