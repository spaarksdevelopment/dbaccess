﻿using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;
using Spaarks.Common.UtilityManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class AccessReports : BaseSecurityPage
    {        
        protected void Page_Init(object sender, EventArgs e)
        {
            PopulateStatusDropDown();
        }

        public bool IsAdminPOuser
        {
            get
            {
                bool toReturn = false;

                List<string> rolesList = base.CurrentUser.RolesShortNamesList;

                if (rolesList.Contains("ADMIN") || rolesList.Contains("PO"))
                {
                    toReturn = true;
                }

                return toReturn;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isPagerCallback = Request.Form["__CALLBACKID"] != null && Request.Form["__CALLBACKID"].Contains("SearchResults");

            if (!Page.IsPostBack && !isPagerCallback)
            {
                PopulateCountryDropDown();
                PopulateStatusDropDown();
                PopulateRequestStatusDropDown();
                PopulateTypeDropDown();

                m_taskDateStartDate.Date = DateTime.Now.AddMonths(-1);
                m_taskDateEndDate.Date = DateTime.Now;                
            }
            else if (!Page.IsCallback || isPagerCallback)
            {
                //  Check for the between visible
                if (m_personDetailsNameSelector.Value.ToString() == "Between")
                {
                    m_personDetailsNameBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsNameBetweenText.Text = "";
                }

                if (m_personDetailsEmployeeIdSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmployeeIdBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmployeeIdBetweenText.Text = "";
                }

                if (m_personDetailsEmailSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmailBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmailBetweenText.Text = "";
                }

                if (m_personDetailsDbDirSelector.Value.ToString() == "Between")
                {
                    m_personDetailsDbDirBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsDbDirBetweenText.Text = "";
                }

                if (m_requestorDetailsNameSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsNameBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsNameBetweenText.Text = "";
                }

                if (m_requestorDetailsEmployeeIdSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsEmployeeIdBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsEmployeeIdBetweenText.Text = "";
                }

                if (m_requestorDetailsEmailSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsEmailBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsEmailBetweenText.Text = "";
                }

                if (m_requestorDetailsDbDirSelector.Value.ToString() == "Between")
                {
                    m_requestorDetailsDbDirBetweenText.ClientVisible = true;
                }
                else
                {
                    m_requestorDetailsDbDirBetweenText.Text = "";
                }
            }

            if (isPagerCallback)
            {
                PerformSearch();
            }
        }

        private void PopulateAccessAreaDropDown(int floorID)
        {
            var floors = Director.GetAccessAreasForFloor(floorID, true, false).OrderBy(e => e.Name);

            if (m_accessArea.Items.Count >= 1)
            {
                m_accessArea.Items.Clear();
            }

            if (m_floorArea.Value == null || Convert.ToInt32(Number.NullableInt32(m_floorArea.Value.ToString())) == 0) return;

            m_accessArea.Items.Add(" --Select-- ", 0);
            foreach (var flr in floors)
            {
                m_accessArea.Items.Add(flr.Name, flr.AccessAreaID);
            }
        }

        private void PopulateTypeDropDown()
        {
            var reqTypes = StoredProcedureManager.GetRequestTypesForReportSearching().OrderBy(e => e.RequestType);
            List<RequestTypesForReportSearching> list = reqTypes.ToList();
            RequestTypesForReportSearching offboardItem = list.Find(e => e.RequestType == "Offboarding");

            list.Remove(offboardItem);

            m_requestType.DataSource = list;
            m_requestType.DataBind();

        }

        protected void m_searchSubmit_click(object sender, EventArgs e)
        {
            PerformSearch();
        }

        protected void m_seearchReset_click(object sender, EventArgs e)
        {
            //  Redirect to the original search page to reset all inputs and output grid view status
            Response.Redirect("/Pages/Admin/AccessReports.aspx", true);
        }

        #region callbacks
        protected void m_city_Callback(object sender, CallbackEventArgsBase e)
        {
            int countryID = GetCallbackParameter(e.Parameter);
            if (countryID == 0)
                return;
            
            PopulateCityDropDown(countryID);
        }

        protected void m_Building_Callback(object sender, CallbackEventArgsBase e)
        {
            int cityID = GetCallbackParameter(e.Parameter);
            if (cityID == 0)
                return;

            PopulateBuildingDropDown(cityID);
        }

        protected void m_Floor_Callback(object sender, CallbackEventArgsBase e)
        {
            int buildingID = GetCallbackParameter(e.Parameter);
            if (buildingID == 0)
                return;

            PopulateFloorDropDown(buildingID);
        }

        protected void m_accessArea_Callback(object sender, CallbackEventArgsBase e)
        {
            int floorID = GetCallbackParameter(e.Parameter);
            if (floorID == 0)
                return;

            PopulateAccessAreaDropDown(floorID);
        }

        private int GetCallbackParameter(string parameter)
        {
            if (string.IsNullOrEmpty(parameter))
                return 0;

            return Convert.ToInt32(parameter);
        }

        public bool IsSwissPassOfficeUser
        {
            get
            {
                bool toReturn = false;

                //  Get the pass office for the user
                List<LocationPassOffice> usersPassOffices = Director.GetUserPassOffice(base.CurrentUser.dbPeopleID);

                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];

                List<LocationPassOffice> countryPassOffice = Director.GetPassOfficesByCountryId(Int32.Parse(switzerlandStringId));

                //  does it only contain 1 and is it switzerland?
                if (usersPassOffices != null && countryPassOffice != null && usersPassOffices.Count == 1 && countryPassOffice.Count == 1 && countryPassOffice.First().PassOfficeID == usersPassOffices.First().PassOfficeID)
                {
                    toReturn = true;
                }


                return toReturn;
            }
        }

        #endregion

        private void PerformSearch()
        {
            //  Need to get all inputs the form our query collection            
           
            DateTime? TaskCreatedStartDate = m_taskDateStartDate.Date != DateTime.MinValue ? m_taskDateStartDate.Date : new Nullable<DateTime>();
            DateTime? TaskCreatedEndDate = m_taskDateEndDate.Date != DateTime.MinValue ? m_taskDateEndDate.Date : new Nullable<DateTime>();
           
            bool performSearch = true;

            //  Check the message dates
            if (!TaskCreatedStartDate.HasValue || !TaskCreatedEndDate.HasValue)
            {
                m_dateErrorMessage.Text = "Please choose a start and end date";
                performSearch = false;
            }
            else if ((TaskCreatedEndDate.GetValueOrDefault() - TaskCreatedStartDate.GetValueOrDefault()).Days > 31)
            {
                m_dateErrorMessage.Text = "Date range should be within maximum of 31 days";
                performSearch = false;
            }
            else
            {
                m_dateErrorMessage.Text = "";
            }
           
            if (performSearch)
            {
                string personNameSelector = m_personDetailsNameSelector.Value.ToString();
                string personNameText = m_personDetailsNameText.Text;
                string personNameTextBetween = m_personDetailsNameBetweenText.Text;

                string personEmployeeIdSelector = m_personDetailsEmployeeIdSelector.Value.ToString();
                string personEmployeeIdText = m_personDetailsEmployeeIdText.Text;
                string personEmployeeIdTextBetween = m_personDetailsEmployeeIdBetweenText.Text;

                string personEmailSelector = m_personDetailsEmailSelector.Value.ToString();
                string personEmailText = m_personDetailsEmailText.Text;
                string personEmailTextBetween = m_personDetailsEmailBetweenText.Text;

                string personDbDirSelector = m_personDetailsDbDirSelector.Value.ToString();
                string personDbDireText = m_personDetailsDbDirText.Text;
                string personDbDirTextBetween = m_personDetailsDbDirBetweenText.Text;

                string requestorNameSelector = m_requestorDetailsNameSelector.Value.ToString();
                string requestorNameText = m_requestorDetailsNameText.Text;
                string requestorNameTextBetween = m_requestorDetailsNameBetweenText.Text;

                string requestorEmployeeIdSelector = m_requestorDetailsEmployeeIdSelector.Value.ToString();
                string requestorEmployeeIdText = m_requestorDetailsEmployeeIdText.Text;
                string requestorEmployeeIdTextBetween = m_requestorDetailsEmployeeIdBetweenText.Text;

                string requestorEmailSelector = m_requestorDetailsEmailSelector.Value.ToString();
                string requestorEmailText = m_requestorDetailsEmailText.Text;
                string requestorEmailTextBetween = m_requestorDetailsEmailBetweenText.Text;

                string requestorDbDirSelector = m_requestorDetailsDbDirSelector.Value.ToString();
                string requestorDbDireText = m_requestorDetailsDbDirText.Text;
                string requestorDbDirTextBetween = m_requestorDetailsDbDirBetweenText.Text;

                int? taskStatusId = SafelyGetComboBoxValue(m_taskStatus.Value);
                int? countryId = SafelyGetComboBoxValue(m_countryArea.Value);
                int? cityId = SafelyGetComboBoxValue(m_cityArea.Value);
                int? buildingId = SafelyGetComboBoxValue(m_buildingArea.Value);
                int? floorId = SafelyGetComboBoxValue(m_floorArea.Value);
                int? fullRequestStatusId = SafelyGetComboBoxValue(m_requestStatus.Value);
                int? accessAreaId = SafelyGetComboBoxValue(m_accessArea.Value);
                int? typeID = SafelyGetComboBoxValue(m_requestType.Value);

                if (IsRestrictedUser)
                {
                    countryId = RestrictedCountryId;
                }
                else if (IsSwissPassOfficeUser)
                {
                    string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                    countryId = Int32.Parse(switzerlandStringId);
                }

                bool? isRestricted = IsAdminPOuser ? false : true;
                
                
                List<sp_DAAccessReportSearch_Result> results = StoredProcedureManager.PerformDAAccessSearch(GetLanguageID(), TaskCreatedStartDate, TaskCreatedEndDate,
                    personNameSelector, personNameText, personNameTextBetween,
                    personEmployeeIdSelector, personEmployeeIdText, personEmployeeIdTextBetween,
                    personEmailSelector, personEmailText, personEmailTextBetween,
                    personDbDirSelector, personDbDireText, personDbDirTextBetween,
                    requestorNameSelector, requestorNameText, requestorNameTextBetween,
                    requestorEmployeeIdSelector, requestorEmployeeIdText, requestorEmployeeIdTextBetween,
                    requestorEmailSelector, requestorEmailText, requestorEmailTextBetween,
                    requestorDbDirSelector, requestorDbDireText, requestorDbDirTextBetween,
                    taskStatusId,
                    countryId,
                    cityId,
                    buildingId,
                    floorId,
                    isRestricted,
                    base.CurrentUser.dbPeopleID, fullRequestStatusId, accessAreaId, typeID);
               
                SearchResults.DataSource = results;
                SearchResults.DataBind();

                m_searchOutputHolder.Visible = true;
            }
            else
            {
                m_searchOutputHolder.Visible = false;
            }
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            PerformSearch();
            gridExport.WriteXlsxToResponse();
        }

        #region Population Methods

        private void PopulateStatusDropDown()
        {
            List<TaskStatusLookup> requestStatus;
            using (var context = new DBAccessEntities())
            {
                var items = from t in context.TaskStatusLookups
                            select t;

                requestStatus = items.ToList();
            }

            m_taskStatus.DataSource = requestStatus;
            m_taskStatus.DataBind();
        }

        private void PopulateRequestStatusDropDown()
        {
            List<AccessRequestStatu> requestStatus;
            using (var context = new DBAccessEntities())
            {
                var items = from t in context.AccessRequestStatus
                            select t;

                requestStatus = items.ToList();
            }

            m_requestStatus.DataSource = requestStatus;
            m_requestStatus.DataBind();
        }

        private void PopulateCountryDropDown()
        {
            List<LocationCountry> countries = null;

            if (IsRestrictedUser)
            {
                countries = new List<LocationCountry>();
                countries.Add(Director.GetCountry(RestrictedCountryId));
            }
            else if (IsSwissPassOfficeUser)
            {
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                countries = new List<LocationCountry>();
                countries.Add(Director.GetCountry(Int32.Parse(switzerlandStringId)));
            }
            else if (IsAdminPOuser)
            {
                countries = Director.GetCountriesAllEnabled();
            }
            else
            {
                countries = Director.GetCountriesByDbPeopleID(base.CurrentUser.dbPeopleID, base.CurrentUser.LanguageId);
            }

            m_countryArea.Items.Add("--Select--", 0);

            if (countries != null)
            {
                foreach (var country in countries)
                {
                    m_countryArea.Items.Add(country.Name, country.CountryID);
                }
            }
        }

        private void PopulateCityDropDown(int countryID)
        {
            var cities = Director.GetCitiesForCountry(countryID, true);

            m_cityArea.Items.Clear();

            m_cityArea.Items.Add("--Select--", 0);

            foreach (var city in cities)
            {
                m_cityArea.Items.Add(city.Name, city.CityID);
            }
        }


        private void PopulateBuildingDropDown(int cityID)
        {
            var buildings = Director.GetBuildingsEnabledForCity(cityID, false).OrderBy(e => e.Name);

            if (m_buildingArea.Items.Count >= 1)
            {
                m_buildingArea.Items.Clear();
            }

            m_buildingArea.Items.Add(" --Select-- ", 0);
            foreach (var bld in buildings)
            {
                m_buildingArea.Items.Add(bld.Name, bld.BuildingID);
            }
        }

        private void PopulateFloorDropDown(int buildingID)
        {
            var floors = Director.GetFloorsForBuilding(buildingID, true, false, false);

            m_floorArea.Items.Clear();

            m_floorArea.Items.Add(" --Select-- ", 0);
            foreach (var flr in floors)
            {
                m_floorArea.Items.Add(flr.Name, flr.FloorID);
            }
        }

        public int? GetLanguageID()
        {
            return base.CurrentUser.LanguageId;

        }


        #endregion

        private int? SafelyGetComboBoxValue(object selectedValue)
        {

            object passOffiveObjectValue = selectedValue;
            int workingId = 0;
            int? Id = new Nullable<int>();

            if (selectedValue != null && !String.IsNullOrEmpty(selectedValue.ToString()) && Int32.TryParse(selectedValue.ToString(), out workingId))
            {
                Id = workingId;
            }

            return Id == 0 ? null : Id;
        }


    }
}