﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;
using Spaarks.Common.UtilityManager;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class SkippedRequestsReport : BaseSecurityPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
           // PopulatePassOfficeList();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isPagerCallback = Request.Form["__CALLBACKID"] != null && Request.Form["__CALLBACKID"].Contains("SearchResults");
            if (!Page.IsPostBack && !isPagerCallback)
            {
                PopulateCountryList();

                m_logDateStartDate.Date = DateTime.Now.AddMonths(-1);
                m_logDateEndDate.Date = DateTime.Now;
            }

            if (isPagerCallback)
            {
                PerformSearch();
            }

            m_countryholder.Visible = base.CurrentUser.RolesShortNamesList.Contains("ADMIN") == true;
        }

        public bool IsSwissPassOfficeUser
        {
            get
            {

                bool toReturn = false;

                //  Get the pass office for the user
                int dbPeopleId = 0;

                if (base.CurrentUser == null)
                {
                    DBAppUser theCurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                    dbPeopleId = theCurrentUser.dbPeopleID;
                }
                else
                {
                    dbPeopleId = base.CurrentUser.dbPeopleID;
                }

                List<LocationPassOffice> usersPassOffices = Director.GetUserPassOffice(dbPeopleId);

                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];

                List<LocationPassOffice> countryPassOffice = Director.GetPassOfficesByCountryId(Int32.Parse(switzerlandStringId));

                //  does it only contain 1 and is it switzerland?
                if (usersPassOffices != null && countryPassOffice != null && usersPassOffices.Count == 1 && countryPassOffice.Count == 1 && countryPassOffice.First().PassOfficeID == usersPassOffices.First().PassOfficeID)
                {
                    toReturn = true;
                }

                return toReturn;
            }
        }


        private void PopulateCountryList()
        {
            List<LocationCountry> countries = new List<LocationCountry>();

            if (IsRestrictedUser)
            {
                countries = new List<LocationCountry>();
                countries.Add(Director.GetCountry(RestrictedCountryId));
            }
            else if (IsSwissPassOfficeUser)
            {
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                countries = new List<LocationCountry>();
                countries.Add(Director.GetCountry(Int32.Parse(switzerlandStringId)));
            }
            else if (countries != null)
            {
                countries = Director.GetCountriesByDbPeopleID(base.CurrentUser.dbPeopleID, base.CurrentUser.LanguageId);

                if (countries != null && countries.Count > 0)
                {
                    countries.OrderBy(e => e.Name);
                }
            }

            //m_countryArea.Items.Add("--Select--", 0).Selected = true;

            foreach (var country in countries)
            {
                m_countryList.Items.Add(country.Name, country.CountryID);
            }
        }


        #region click events
        protected void m_searchSubmit_click(object sender, EventArgs e)
        {
            PerformSearch();
        }

        protected void m_seearchReset_click(object sender, EventArgs e)
        {
            //  Redirect to the original search page to reset all inputs and output grid view status
            Response.Redirect("/Pages/Admin/SkippedRequestsReport.aspx", true);
        }

        #endregion


        private void PerformSearch()
        {
            DateTime? LogCreatedStartDate = m_logDateStartDate.Date != DateTime.MinValue ? m_logDateStartDate.Date : new Nullable<DateTime>();
            DateTime? LogCreatedEndDate = m_logDateEndDate.Date != DateTime.MinValue ? m_logDateEndDate.Date : new Nullable<DateTime>();
          

            bool performSearch = true;

            if (!LogCreatedStartDate.HasValue || !LogCreatedEndDate.HasValue)
            {
                m_voidDateErrorMessage.Visible = true;
                m_dateSpanErrorMessage.Visible = false;
                performSearch = false;
            }
            else if ((LogCreatedEndDate.GetValueOrDefault() - LogCreatedStartDate.GetValueOrDefault()).Days > 31)
            {
                m_dateSpanErrorMessage.Visible = true;
                m_voidDateErrorMessage.Visible = false;
                performSearch = false;
            }
            else
            {
                m_dateSpanErrorMessage.Visible = false;
                m_voidDateErrorMessage.Visible = false;
            }


            if (performSearch)
            {              
                string countries = "";
                if (IsRestrictedUser)
                {
                    countries = RestrictedCountryId.ToString();
                }
                else if (IsSwissPassOfficeUser)
                {
                    string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                    countries = switzerlandStringId;
                }
                else
                {
                    foreach (ListEditItem item in m_countryList.SelectedItems)
                    {
                        countries += "," + item.Value.ToString();
                    }

                    if (countries.Length > 0)
                    {
                        countries = countries.Substring(1);
                    }
                }

                List<sp_SkippedRequestsReportSearch_Result> results = StoredProcedureManager.SkippedRequestsReportSearch(base.CurrentUser.LanguageId, LogCreatedStartDate, LogCreatedEndDate, countries, base.CurrentUser.dbPeopleID);

                SearchResults.DataSource = results;
                SearchResults.DataBind();
                m_searchOutputHolder.Visible = true;
            }
            else
            {
                m_searchOutputHolder.Visible = false;
            }
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            PerformSearch();
            gridExport.WriteXlsxToResponse();
        }

    }
}