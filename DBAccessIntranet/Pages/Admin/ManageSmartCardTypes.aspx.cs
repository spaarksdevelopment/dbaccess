﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

using DevExpress.Web.ASPxGridView;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
	public partial class ManageSmartCardTypes : BaseSecurityPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		    GetSmartCardTypes();
		}

		private void GetSmartCardTypes()
		{
			try
			{
                List<GetSmartCardTypes_Result> smartcardTypes = new List<GetSmartCardTypes_Result>();
                bool includeDisabled = ckbEnabled.Checked;
                smartcardTypes = Director.GetSmartCardTypes(CurrentUser.LanguageId,includeDisabled);

				if (smartcardTypes.Count > 1)
				{
					smartcardTypes.RemoveAt(0);
					grdViewSmartCardTypes.DataSource = smartcardTypes;
					grdViewSmartCardTypes.DataBind();
				}
			}
			catch (Exception ex)
			{
				int LoggedExceptionId = LogHelper.HandleException(ex);
			}
		}

		protected void grdViewSmartCardTypes_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			GetSmartCardTypes();
		}

		[WebMethod, ScriptMethod]
		public static void DeleteSmartcardType(int typeID)
		{
			int nrval = 0;
			try
			{
                DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                Director director = new Director(CurrentUser.dbPeopleID);

				if (typeID > 0)
					nrval = director.DeleteSmartCardType(typeID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}
	}
}