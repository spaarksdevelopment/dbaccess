﻿using System;
using System.Data;
//using Excel;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Models;
using Spaarks.CustomMembershipManager;
using System.IO;
using Excel;
using DevExpress.Web.ASPxTreeList;
using System.Data.SqlTypes;
using DevExpress.Web.ASPxEditors;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class BulkUpload : BaseSecurityPage
    {
        #region Properties

        private int FileID { get; set; }

        public bool ValidationPassed { get; set; }

        public bool UploadSuccess { get; set; }

        public bool ImportSuccess { get; set; }

        private string ErrorMessage { get; set; }

        private string SuccessMessage { get; set; }

        private string PopupInfoMessage { get; set; }

        #endregion

        #region Page Events

        protected override void OnPreRender(EventArgs e)
        {
            FormatUI();

            base.OnPreRender(e);
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            if (ViewState["FileID"] != null)
                FileID = (int)ViewState["FileID"];

            if (ViewState["ValidationPassed"] != null)
                ValidationPassed = (bool)ViewState["ValidationPassed"];

            if (ViewState["UploadSuccess"] != null)
                UploadSuccess = (bool)ViewState["UploadSuccess"];

            if (ViewState["ImportSuccess"] != null)
                ImportSuccess = (bool)ViewState["ImportSuccess"];
        }

        protected override object SaveViewState()
        {
            ViewState.Add("FileID", FileID);
            ViewState.Add("ValidationPassed", ValidationPassed);
            ViewState.Add("UploadSuccess", UploadSuccess);
            ViewState.Add("ImportSuccess", ImportSuccess);

            return base.SaveViewState();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadImportHistory();
            }

            pnlValidationSummary.Visible = false;
            pnlImportSummary.Visible = false;
        }       

        #endregion

        #region Private Methods

        private void FormatUI()
        {
            btnImport.Enabled = this.ValidationPassed;
            btnValidate.Enabled = this.UploadSuccess;

            imgStep1_Green.Visible = this.UploadSuccess;
            imgStep1_Gray.Visible = !this.UploadSuccess;

            imgStep2_Green.Visible = this.ValidationPassed;
            imgStep2_Gray.Visible = !this.ValidationPassed;

            lblErrorMsg.Text = string.IsNullOrEmpty(ErrorMessage) ? string.Empty : ErrorMessage.Replace("\n", "<br />");
            lblSuccessMsg.Text = SuccessMessage;

            litPopupInfo.Text = PopupInfoMessage;
            popupInfo.ShowOnPageLoad = !string.IsNullOrWhiteSpace(PopupInfoMessage);

            PopupPostSubmit.ShowOnPageLoad = false;

            btnValidate.ForeColor = System.Drawing.Color.Black;
            btnUpload.ForeColor = System.Drawing.Color.Black;
            btnImport.ForeColor = System.Drawing.Color.Black;

            if (this.ValidationPassed)
                SetInfoTextAndFocusButton("File is valid. Click 'Import' button to import data.", btnImport);            
            else if (this.UploadSuccess)
                SetInfoTextAndFocusButton("File uploaded successfully. Click 'Validate' button to validate the data.", btnValidate);
            else
                SetInfoTextAndFocusButton("Browse the data file and click 'Upload' button.", btnUpload);
        }

        private void SetInfoTextAndFocusButton(string message, ASPxButton focusButton)
        {
            lblInfo.Text = message;
            focusButton.ForeColor = System.Drawing.Color.FromArgb(0, 152, 219);
        }

        private void LoadImportHistory()
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            gvImportHistoty.DataSource = bulkUploadService.GetBulkUploadHistory();
            gvImportHistoty.DataBind();
        }

        private void DisplayImportSummary()
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            rptImportSummary.DataSource = bulkUploadService.GetImportSummary(FileID);
            rptImportSummary.DataBind();

            pnlImportSummary.Visible = true;
        }

        private string SanitizeValuesForCSV(object value)
        {
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";

            string output = value.ToString().Replace(System.Environment.NewLine, string.Empty).Replace(',', ';');

            if (output.Contains(",") || output.Contains("\""))
                output = "\"" + output.Replace("\"", "\"\"") + "\"";

            return output;
        }

        #endregion

        #region Click Events

        protected void btnSubmitExcelSheet_Click(object sender, EventArgs e)
        {
            if (!cusValidatorFile.IsValid)
                return;

            this.FileID = 0;
            this.UploadSuccess = false;
            this.ValidationPassed = false;
            this.ImportSuccess = false;

            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(inputFileID.FileContent);

            if (excelReader.IsValid)
            {
                //extract excel file data into a DataSet taking first row as column headers.
                excelReader.IsFirstRowAsColumnNames = true;
                var excelData = excelReader.AsDataSet();
                
                int importFileID = 0;
                string errors = string.Empty;

                if (bulkUploadService.StageData(excelData, inputFileID.FileName, ref importFileID, ref errors))
                {
                    this.UploadSuccess = true;
                    FileID = importFileID;
                    SuccessMessage = "File Uploaded Successfully!";
                }
                else
                {
                    ErrorMessage = errors;
                }
            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            List<XLImportValidateFile_Result> listErrors = new List<XLImportValidateFile_Result>();

            if (this.UploadSuccess)
            {
                if (bulkUploadService.ValidateData(FileID, ref listErrors))
                {
                    this.ValidationPassed = true;
                    this.ImportSuccess = false;

                    if (listErrors.Where(er => er.ErrorType.ToLower() == "warning").Count() > 0)
                    {
                        SuccessMessage = "Validation Passed with warnings!";
                        DisplayValidationErrors(listErrors);
                    }
                    else
                    {
                        SuccessMessage = "Validation Passed!";
                    }
                }
                else
                {
                    ErrorMessage = "Validation Failed. Please correct all the errors and upload the file again.";
                    DisplayValidationErrors(listErrors);
                }
            }
            else 
            {
                ErrorMessage = "Upload Failed";
            }
        }

        protected void ASPxButtonImport_Click(object sender, EventArgs e)
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            this.ImportSuccess = false;
            string errorMsg = string.Empty;

            if (ValidationPassed)
            {
                if (bulkUploadService.ImportData(FileID, ref errorMsg))
                {
                    this.ImportSuccess = true;
                    SuccessMessage = "Data Imported Successfully!";
                    DisplayImportSummary();
                    LoadImportHistory();

                    //Once file imported successfully reset all the values to defaults
                    this.FileID = 0;
                    this.UploadSuccess = false;
                    this.ValidationPassed = false;
                    this.ImportSuccess = false;
                }
                else
                {
                    ErrorMessage = errorMsg;
                }
            }
            else
            {
                ErrorMessage = "Validation Failed";                
            }
        }

        protected void btnDownloadTemplate_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + "BulkImportTemplate.xlsx");
            Response.TransmitFile(Server.MapPath("~/Templates/BulkImportTemplate.xlsx"));
            Response.End();
        }

        protected void btnDownloadValidationRules_Click(object sender, EventArgs e)
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);
            List<ValidationRuleDisplayModel> validationRules = new List<ValidationRuleDisplayModel>();
            StringBuilder sbValidationRules = new StringBuilder();

            validationRules = bulkUploadService.GetValidationRules();
            
            //Add column names first.
            sbValidationRules.AppendLine("Sheet, Type, Rule");
            
            foreach (var rule in validationRules)
            {
                sbValidationRules.AppendFormat("{0}, {1}, {2}\n", rule.SheetName, rule.ValidationType, SanitizeValuesForCSV(rule.BusinessRule));
            }

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "BulkImport_BusinessRules.csv");
            Response.Flush();
            Response.Write(sbValidationRules.ToString());
            Response.End();
        }

        protected void btnValidationSummary_Click(object sender, EventArgs e)
        {
            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            List<ValidationErrorDisplayModel> listErrors = new List<ValidationErrorDisplayModel>();
            StringBuilder sbValidationErrors = new StringBuilder();

            listErrors =  bulkUploadService.GetValidationErrors(FileID);

            //Add column names first.
            sbValidationErrors.AppendLine("Sheet, Type, Message, Rows");

            foreach (var error in listErrors)
            {
                sbValidationErrors.AppendFormat("{0}, {1}, {2}, {3}\n", error.SheetName, error.ErrorType, SanitizeValuesForCSV(error.ErrorMessage), error.RowNumbers.Replace(',', ';'));
            }

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "BulkImport_ValidationErrors.csv");
            Response.Flush();
            Response.Write(sbValidationErrors.ToString());
            Response.End();
        }        

        #endregion

        #region Grid Events

        protected void gvImportHistoty_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
        {
            LoadImportHistory();
        }
        
        protected void gvImportHistoty_PageIndexChanged(object sender, EventArgs e)
        {
            LoadImportHistory();
        }

        protected void btnRollback_Click(object sender, EventArgs e)
        {
            int fileID = 0;
            int.TryParse(((LinkButton)sender).CommandArgument, out fileID);
            string errorMsg = string.Empty;

            IBulkUploadService bulkUploadService = new BulkUploadService(CurrentUser.dbPeopleID);

            if (bulkUploadService.RollbackFile(fileID, ref errorMsg))
            {
                LoadImportHistory();
                PopupInfoMessage = "File Rollbacked Successfully";
            }
            else
            {
                PopupInfoMessage = errorMsg;
            }
        }

        #endregion

        #region Error Tree

        private void DisplayValidationErrors(List<XLImportValidateFile_Result> listErrors)
        {
            int index = 0;

            foreach (var entity in listErrors.Select(e => new { e.SheetName, e.Entity }).Distinct())
            {
                index++;
                TreeListNode parentNode = CreateNodeCore(index, "Sheet", string.Format("<b>{0}</b>", entity.SheetName), null);
                parentNode.Expanded = true;
                foreach (var error in listErrors.Where(e => e.Entity == entity.Entity))
                {
                    index++;
                    CreateNodeCore(index, error.ErrorType, string.Format("{0} [Row{1}: {2}]", error.ErrorMessage, error.RowNumbers.Count(e => e == ',') > 0 ? "s" : "", error.RowNumbers), parentNode);
                }
            }

            pnlValidationSummary.Visible = true;
        }

        private TreeListNode CreateNodeCore(object key, string iconName, string text, TreeListNode parentNode)
        {
            TreeListNode node = treeList.AppendNode(key, parentNode);
            node["IconName"] = iconName;
            node["Name"] = text;
            return node;
        }

        protected string GetIconUrl(TreeListDataCellTemplateContainer container)
        {
            switch (container.GetValue("IconName").ToString().ToLower())
            {
                case "error": return "~/App_Themes/DBIntranet2010/images/exclamation.gif";
                case "warning": return "~/App_Themes/DBIntranet2010/images/exclamation-icon-yellow.png";
                case "sheet": return "~/App_Themes/DBIntranet2010/images/bullet.gif";
                default: return string.Empty;
            }
        }

        #endregion        

        #region Validation

        protected void cusValidatorFile_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(args.Value).ToLower() == ".xlsx";
        }

        #endregion
    }
}