﻿using DevExpress.Web.ASPxEditors;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class AdminReport : BaseSecurityPage
    {
        

        protected void Page_Init(object sender, EventArgs e)
        {
            PopulatePassOfficeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isPagerCallback = Request.Form["__CALLBACKID"] != null && Request.Form["__CALLBACKID"].Contains("SearchResults");

            PopulatePassOfficeList();
            PopulateStatusDropDown();

            if (!Page.IsCallback || isPagerCallback)
            {
                //  Check for the between visible
                if (m_personDetailsNameSelector.Value.ToString() == "Between")
                {
                    m_personDetailsNameBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsNameBetweenText.Text = "";
                }

                if (m_personDetailsEmployeeIdSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmployeeIdBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmployeeIdBetweenText.Text = "";
                }

                if (m_personDetailsEmailSelector.Value.ToString() == "Between")
                {
                    m_personDetailsEmailBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsEmailBetweenText.Text = "";
                }

                if (m_personDetailsDbDirSelector.Value.ToString() == "Between")
                {
                    m_personDetailsDbDirBetweenText.ClientVisible = true;
                }
                else
                {
                    m_personDetailsDbDirBetweenText.Text = "";
                }
            }

            if (isPagerCallback)
            {
                PerformSearch();
            }
        }

        protected void m_searchSubmit_click(object sender, EventArgs e)
        {            
            PerformSearch();
        }

        protected void m_seearchReset_click(object sender, EventArgs e)
        {
            //  Redirect to the original search page to reset all inputs and output grid view status
            Response.Redirect("/Pages/Admin/AdminReport.aspx", true);
        }

        private void PerformSearch()
        {
            //  Need to get all inputs the form our query collection
            DateTime? RevokeAccessStartDate = m_revokeAccessStartDate.Date != DateTime.MinValue ? m_revokeAccessStartDate.Date : new Nullable<DateTime>();
            DateTime? RevokeAccessEndDate = m_revokeAccessEndDate.Date != DateTime.MinValue ? m_revokeAccessEndDate.Date : new Nullable<DateTime>();
            DateTime? TaskCreatedStartDate = m_taskDateStartDate.Date != DateTime.MinValue ? m_taskDateStartDate.Date : new Nullable<DateTime>();
            DateTime? TaskCreatedEndDate = m_taskDateEndDate.Date != DateTime.MinValue ? m_taskDateEndDate.Date : new Nullable<DateTime>();
            DateTime? TaskCompletedStartDate = m_taskDateCompletedStartDate.Date != DateTime.MinValue ? m_taskDateCompletedStartDate.Date : new Nullable<DateTime>();
            DateTime? TaskCompletedEndDate = m_taskDateCompletedEndDate.Date != DateTime.MinValue ? m_taskDateCompletedEndDate.Date : new Nullable<DateTime>();

            bool PerformSearch = true;
            bool revokeDateSupplied = true;
            bool taskCreatedDateSupplied = true;
            bool taskCompletedDateSupplied = true;
            bool revokeDateInRange = true;
            bool taskCreatedInRange = true;
            bool taskCompletedInRange = true;
            m_dateErrorMessage.Text = "";

            //  Check at least one date has been supplied
            if (!RevokeAccessStartDate.HasValue || !RevokeAccessEndDate.HasValue)
            {
                revokeDateSupplied = false;
            }
            else if ((RevokeAccessStartDate.GetValueOrDefault() - RevokeAccessEndDate.GetValueOrDefault()).Days > 31)
            {
                revokeDateInRange = false;
            }
            if (!TaskCreatedStartDate.HasValue || !TaskCreatedEndDate.HasValue)
            {
                taskCreatedDateSupplied = false;
            }
            else if ((TaskCreatedStartDate.GetValueOrDefault() - TaskCreatedEndDate.GetValueOrDefault()).Days > 31)
            {
                taskCreatedInRange = false;
            }
            if (!TaskCompletedStartDate.HasValue || !TaskCompletedEndDate.HasValue)
            {
                taskCompletedDateSupplied = false;
            }
            else if ((TaskCompletedStartDate.GetValueOrDefault() - TaskCompletedEndDate.GetValueOrDefault()).Days > 31)
            {
                taskCompletedInRange = false;
            }

            if (!revokeDateSupplied && !taskCreatedDateSupplied && !taskCompletedDateSupplied)
            {
                m_dateErrorMessage.Text = "Please choose a start and end date";
                PerformSearch = false;
            }
            else if (!revokeDateInRange || !taskCreatedInRange || !taskCompletedInRange)
            {
                m_dateErrorMessage.Text = "choose a shorter timespan";
                PerformSearch = false;
            }

            if (PerformSearch)
            {
                string personNameSelector = m_personDetailsNameSelector.Value.ToString();
                string personNameText = m_personDetailsNameText.Text;
                string personNameTextBetween = m_personDetailsNameBetweenText.Text;

                string personEmployeeIdSelector = m_personDetailsEmployeeIdSelector.Value.ToString();
                string personEmployeeIdText = m_personDetailsEmployeeIdText.Text;
                string personEmployeeIdTextBetween = m_personDetailsEmployeeIdBetweenText.Text;

                string personEmailSelector = m_personDetailsEmailSelector.Value.ToString();
                string personEmailText = m_personDetailsEmailText.Text;
                string personEmailTextBetween = m_personDetailsEmailBetweenText.Text;

                string personDbDirSelector = m_personDetailsDbDirSelector.Value.ToString();
                string personDbDireText = m_personDetailsDbDirText.Text;
                string personDbDirTextBetween = m_personDetailsDbDirBetweenText.Text;
                string passOffices = "";

                foreach (ListEditItem item in m_passOfficeList.SelectedItems)
                {
                    passOffices += "," + item.Value.ToString();
                }

                if (passOffices.Length > 0)
                {
                    passOffices = passOffices.Substring(1);
                }

                object passOffiveObjectValue = m_passOfficeStatus.Value;
                int workingStatusId = 0;
                int? statusId = new Nullable<int>();

                if (passOffiveObjectValue != null && !String.IsNullOrEmpty(passOffiveObjectValue.ToString()) && Int32.TryParse(passOffiveObjectValue.ToString(), out workingStatusId))
                {
                    statusId = workingStatusId;
                }

                int? countryId = new Nullable<int>();

                if (IsRestrictedUser)
                {
                    countryId = RestrictedCountryId;
                }

                //  Just need to write a view that represents the search query and do an entity update
                List<sp_PerformAdminSearch_Result> results = StoredProcedureManager.PerformSearch(RevokeAccessStartDate, RevokeAccessEndDate, TaskCreatedStartDate, TaskCreatedEndDate,
                    personNameSelector, personNameText, personNameTextBetween,
                    personEmployeeIdSelector, personEmployeeIdText, personEmployeeIdTextBetween,
                    personEmailSelector, personEmailText, personEmailTextBetween,
                    personDbDirSelector, personDbDireText, personDbDirTextBetween,
                    TaskCompletedStartDate, TaskCompletedEndDate, passOffices, statusId, countryId);

                SearchResults.DataSource = results;
                SearchResults.DataBind();

                m_searchOutputHolder.Visible = true;
            }
            else
            {
                m_searchOutputHolder.Visible = false;
            }
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            PerformSearch();
            gridExport.WriteXlsxToResponse();
        }

        #region Population Methods

        private void PopulateStatusDropDown()
        {
            List<TaskStatusLookup> requestStatus;
			using (var context = new DBAccessEntities())
			{
                var items = from t in context.TaskStatusLookups
							select t;

                requestStatus = items.ToList();
			}

            m_passOfficeStatus.DataSource = requestStatus;
            m_passOfficeStatus.DataBind();
        }

        private void PopulatePassOfficeList()
        {
            if (IsRestrictedUser)
            {
                List<GetAllPassOffices> passOffices = Director.GetEnabledPassOfficeDetailsByCountryID(1, RestrictedCountryId);
                m_passOfficeList.DataSource = passOffices;
                m_passOfficeList.DataBind();
                
            }
            else
            {
                m_passOfficeList.DataSource = Director.GetAllPassOfficesEnabled(1).OrderBy(e => e.DisplayName);
                m_passOfficeList.DataBind();
            }           
        }

        #endregion

        
    }
}