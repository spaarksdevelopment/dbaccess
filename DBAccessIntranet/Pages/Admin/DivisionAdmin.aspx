﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="DivisionAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.DivisionAdmin" uiculture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Src="~/Controls/AdminControls/DivisionSelectorAdmin.ascx" TagName="DivisionSelector" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">

<script type="text/javascript">
    $(document).ready(function () {
       
        $('#AccessHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
        $('#AccessHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');

        $('#AccessHelp').bind('click', function () {
                $("#AccessHelpInfo").slideToggle("slow");
               });
            $('#AccessHelpInfo').hide();
           });

    </script>
    <h1><asp:Literal runat="server" ID="dividAdmin" meta:resourcekey="dividAdmin"> </asp:Literal></h1>
    <p><asp:Literal runat="server" ID="dividAdmin2" meta:resourcekey="dividAdmin2"> </asp:Literal><img id="AccessHelp" src="../../App_Themes/DBIntranet2010/images/question.gif"  /></p>
    <br />
    <div id="AccessHelpInfo" class="HelpPopUp">
        <p><asp:Literal runat="server" ID="pdivAdminParagraph1" meta:resourcekey="pdivAdminParagraph1"></asp:Literal> </p>       
        <p><asp:Literal runat="server" ID="pdivAdminParagraph2" meta:resourcekey="pdivAdminParagraph2"> </asp:Literal> 
                     <asp:Image ID="Help" SkinID="FileXLS"  AlternateText="<%$ Resources:CommonResource, divAdminAltImage2 %>" runat="server"/>
        </p>      
    </div>
    <br /> 
    <div id="DivisionContent" class='Content'>
        <uc1:DivisionSelector ID="Division" runat="server" />
    </div>

	<ppc:Popups ID="PopupTemplates" runat="server" />
</asp:Content>
