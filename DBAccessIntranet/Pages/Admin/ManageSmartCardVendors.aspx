﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ManageSmartCardVendors.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.ManageSmartCardVendors" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">
$(document).ready(function ()
{
	hdSmartCardVendor.Set("smartCardVendorURL", '<%= Page.ResolveUrl("~/Pages/Popup/AddEditSmartCardVendor.aspx") %>');
});

var popupURLVendorID = "";
var popupVendorID = "";

function EditVendor(vendorId)
{    
	//Existing Record
	if (vendorId != null) {
		popupVendorID = vendorId;

		var popupUrlEditSmartCardVendor = hdSmartCardVendor.Get("smartCardVendorURL");
		popupUrlEditSmartCardVendor = popupUrlEditSmartCardVendor + "?ID=" + vendorId;
		popupURLVendorID = popupUrlEditSmartCardVendor;

		popupEditSmartCardVendor.SetContentUrl(popupURLVendorID);
		popupEditSmartCardVendor.Show();
	}
}

function CancelAddEditSmartCardVendor()
{
	popupEditSmartCardVendor.Hide();
}

function RefreshSmartCardProviderGrid()
{
	popupEditSmartCardVendor.Hide();
	grdViewSmartCardVendors.PerformCallback();
}

function DeleteSmartcardProvider() {
    popupConfirmTemplate.SetHeaderText("<%=DeleteVendorHead.Text %>"); //Delete Vendor
    popupConfirmTemplateContentHead.SetText("<%=Vendor.Text %>");
    popupConfirmTemplateContentBody.SetText("<%=VendorRemoveConfirm.Text %>"); //Are you sure you want to remove this vendor?
	popupConfirmTemplateHiddenField.Set("function_Yes", "ConfirmDeleteSmartcardProvider();");
	popupConfirmTemplate.Show();
}

function ConfirmDeleteSmartcardProvider() {
	if (popupVendorID == 0 || popupVendorID == "")
		return;

	PageMethods.DeleteSmartcardProvider(popupVendorID, OnDeleteSmartcardProvider);
}

function OnDeleteSmartcardProvider() {
    popupEditSmartCardVendor.Hide();
	grdViewSmartCardVendors.PerformCallback();
}

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <h1> <asp:Literal ID="ManageVendorHeader" meta:resourcekey="ManageVendorHeader" runat="server"></asp:Literal> </h1>
<dx:ASPxGridView ID="grdViewSmartCardVendors" runat="server" AutoGenerateColumns="false" KeyFieldName="ID" 
 ClientInstanceName="grdViewSmartCardVendors" OnCustomCallback="grdViewSmartCardVendors_CustomCallback">
 <Settings ShowStatusBar="Visible" />
 <Columns>
		<dx:GridViewDataTextColumn meta:resourcekey="VendorHead" FieldName="ProviderName" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn meta:resourcekey="KeyIdentifier" FieldName="CurrentPUK" ShowInCustomizationForm="True" VisibleIndex="2" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataTextColumn meta:resourcekey="HexMifare" FieldName="UseHexMifare" ShowInCustomizationForm="True" VisibleIndex="3" Width="150px"></dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Enabled%>" FieldName="Enabled" ShowInCustomizationForm="True" VisibleIndex="4" Width="150px"></dx:GridViewDataTextColumn>
		<dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" VisibleIndex="5" ShowInCustomizationForm="True">
                <DataItemTemplate>								
							<a href="javascript:{}" ID="lbBuildingEdit" runat="server" meta:resourcekey="EditAccesArea" onclick='<%#"EditVendor(\"" + Eval("ID") + "\");"%>'><asp:Literal ID="EditLiteral" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" runat="server"></asp:Literal></a>  
                </DataItemTemplate>
        </dx:GridViewDataColumn>
</Columns>
<Templates>
	<StatusBar>
			<dx:ASPxButton ID="dtnAddSmartCardProvider" AutoPostBack="false" runat="server" meta:resourcekey="AddSmartCard">
				<ClientSideEvents Click='function(s,e){ EditVendor(0); }' />
			</dx:ASPxButton>
	</StatusBar>
</Templates>
</dx:ASPxGridView>
<dx:ASPxCheckBox ID="ckbEnabled" runat="server"  meta:resourcekey="ShowDisabled">
    <ClientSideEvents CheckedChanged='function(s,e){ RefreshSmartCardProviderGrid(); }' />
</dx:ASPxCheckBox>
<br />
<br />

<!-- edit division-->
<dx:ASPxPopupControl ID="ASPxPopupControlEditDivision" ClientInstanceName="popupEditSmartCardVendor" runat="server"
	SkinID="PopupNoHeader"
	 meta:resourcekey="SmartCardVendor"
	Width="600px"
	Height="300px"
	ContentUrl="~/Pages/Popup/AddEditSmartCardVendor.aspx"
	ContentUrlIFrameTitle="SmartCardPopup">
</dx:ASPxPopupControl>


<!--Hidden feid to hold the navigation URl-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldSmartCardVendor" ClientInstanceName="hdSmartCardVendor" runat="server">
</dx:ASPxHiddenField>

    <ppc:Popups ID="PopupTemplates" runat="server" />
    <!--literals-->
    
     <asp:Literal ID="DeleteVendorHead"  meta:resourcekey="DeleteVendorHead" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="Vendor"  meta:resourcekey="Vendor" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="VendorRemoveConfirm"  meta:resourcekey="VendorRemoveConfirm" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     
     
</asp:Content>
