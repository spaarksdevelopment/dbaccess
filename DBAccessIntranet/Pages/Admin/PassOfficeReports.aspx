﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="PassOfficeReports.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.PassOfficeReports" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">

        function betweenInputToggleName()
        {
            var value = PersonDetailsNameSelector.GetValue();


            if (value == "Between") {
                PersonDetailsNameBetweenText.SetVisible(true);
            }
            else {
                PersonDetailsNameBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleEmployeeId()
        {
            var value = PersonDetailsEmployeeIdSelector.GetValue();


            if (value == "Between") {
                PersonDetailsEmployeeIdBetweenText.SetVisible(true);
            }
            else {
                PersonDetailsEmployeeIdBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleEmail()
        {
            var value = PersonDetailsEmailSelector.GetValue();


            if (value == "Between") {
                PersonDetailsEmailBetweenText.SetVisible(true);
            }
            else {
                PersonDetailsEmailBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleDbDir()
        {
            var value = PersonDetailsDbDirSelector.GetValue();


            if (value == "Between") {
                PersonDetailsDbDirBetweenText.SetVisible(true);
            }
            else {
                PersonDetailsDbDirBetweenText.SetVisible(false);
            }
        }

        //requestors functions
        function betweenInputToggleNameReq()
        {
            var value = RequestorDetailsNameSelector.GetValue();


            if (value == "Between") {
                RequestorDetailsNameBetweenText.SetVisible(true);
            }
            else {
                RequestorDetailsNameBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleEmployeeIdReq()
        {
            var value = RequestorDetailsEmployeeIdSelector.GetValue();


            if (value == "Between") {
                RequestorDetailsEmployeeIdBetweenText.SetVisible(true);
            }
            else {
                RequestorDetailsEmployeeIdBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleEmailReq()
        {
            var value = RequestorDetailsEmailSelector.GetValue();


            if (value == "Between") {
                RequestorDetailsEmailBetweenText.SetVisible(true);
            }
            else {
                RequestorDetailsEmailBetweenText.SetVisible(false);
            }
        }

        function betweenInputToggleDbDirReq()
        {
            var value = RequestorDetailsDbDirSelector.GetValue();


            if (value == "Between") {
                RequestorDetailsDbDirBetweenText.SetVisible(true);
            }
            else {
                RequestorDetailsDbDirBetweenText.SetVisible(false);
            }
        }
        //generic

        function onCountryChanged()
        {
            CityArea.PerformCallback(CountryArea.GetValue());
           
        }

       function onCityChanged()
            {
                BuildingArea.PerformCallback(CityArea.GetValue());
            }      
        
       function onBuildingChanged()
            {
                FloorArea.PerformCallback(BuildingArea.GetValue());
       }

       function onFloorChanged() {
           AccessArea.PerformCallback(FloorArea.GetValue());
       }


       function CountryChangeEnd()
            {

            }

       function CityChangeEnd()
            {
                BuildingArea.PerformCallback(CityArea.GetValue());
            }

       function BuildingChangeEnd()
            {
                FloorArea.PerformCallback(BuildingArea.GetValue());
       }

       function FloorChangeEnd() {

           AccessArea.PerformCallback(FloorArea.GetValue());
       }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
      <h1>Pass Office Report</h1>
    <asp:Panel runat="server" ID="searchFilters">  
          <table>
              <tr>
                  <td valign="top">
                   <table>
                      <tr>
                       <td valign="top">
                         <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Task Created</p>
                        <table>            
                            <tr>
                                <td><strong>Date &amp; Time</strong></td>
                                <td>Start: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_taskDateStartDate" 
					                ClientInstanceName="TaskDateStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                   
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>End: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_taskDateEndDate" 
					                ClientInstanceName="TaskDateEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                    
                                </td>
                            </tr>
                            <tr>
                              <td colspan="3"><p style="color: red;font-weight: bold;">
                                  <asp:Literal ID="m_voidDateErrorMessage" runat="server" Text="Please choose a start and end date" Visible="false" />
                                  <asp:Literal ID="m_dateSpanErrorMessage" runat="server"  Text="Date range should be within maximum of 31 days" Visible="false"/></p>
                              </td>
                           </tr>
                        </table>                     

                      </td>
                       <td style="width: 50px;">&nbsp;</td>
                       <td>

                           <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Task Completed</p>
                        <table>            
                            <tr>
                                <td><strong>Date &amp; Time</strong></td>
                                <td>Start: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_taskDateCompletedStartDate" 
					                ClientInstanceName="TaskDateCompletedStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                   
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>End: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_taskDateCompletedEndDate" 
					                ClientInstanceName="TaskDateCompletedEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                    
                                </td>
                            </tr>
                        </table>
                      </td>             
                             
                    </tr>
                 </table>
                </td>
              </tr>

              <tr>
                   <td valign="top">
                      <table>
                         <tr>
                           <td>
                             <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Person Details</p>
                             <table>            
                                 <tr>
                                    <td align="right" width="100px"><strong>Name</strong></td>
                                    <td><dx:ASPxComboBox ID="m_personDetailsNameSelector" ClientInstanceName="PersonDetailsNameSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                            <Items>
                                                <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                                <dx:ListEditItem Value="Begins" Text="Begins With" />
                                                <dx:ListEditItem Value="Equals" Text="Equals" />
                                                <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                                <dx:ListEditItem Value="Between" Text="Between" />
                                            </Items>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleName(); }" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsNameText" ClientInstanceName="PersonDetailsNameText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsNameBetweenText" ClientInstanceName="PersonDetailsNameBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" width="100px"><strong>Employee ID</strong></td>
                                    <td><dx:ASPxComboBox ID="m_personDetailsEmployeeIdSelector" ClientInstanceName="PersonDetailsEmployeeIdSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                            <Items>
                                                <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                                <dx:ListEditItem Value="Begins" Text="Begins With" />
                                                <dx:ListEditItem Value="Equals" Text="Equals" />
                                                <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                                <dx:ListEditItem Value="Between" Text="Between" />
                                            </Items>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmployeeId(); }" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsEmployeeIdText" ClientInstanceName="PersonDetailsEmployeeIdText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsEmployeeIdBetweenText" ClientInstanceName="PersonDetailsEmployeeIdBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" width="100px"><strong>Email</strong></td>
                                    <td><dx:ASPxComboBox ID="m_personDetailsEmailSelector" ClientInstanceName="PersonDetailsEmailSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                            <Items>
                                                <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                                <dx:ListEditItem Value="Begins" Text="Begins With" />
                                                <dx:ListEditItem Value="Equals" Text="Equals" />
                                                <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                                <dx:ListEditItem Value="Between" Text="Between" />
                                            </Items>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmail(); }" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsEmailText" ClientInstanceName="PersonDetailsEmailText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsEmailBetweenText" ClientInstanceName="PersonDetailsEmailBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" width="100px"><strong>dbDir</strong></td>
                                    <td><dx:ASPxComboBox ID="m_personDetailsDbDirSelector" ClientInstanceName="PersonDetailsDbDirSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                            <Items>
                                                <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                                <dx:ListEditItem Value="Begins" Text="Begins With" />
                                                <dx:ListEditItem Value="Equals" Text="Equals" />
                                                <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                                <dx:ListEditItem Value="Between" Text="Between" />
                                            </Items>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleDbDir(); }" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsDbDirText" ClientInstanceName="PersonDetailsDbDirText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_personDetailsDbDirBetweenText" ClientInstanceName="PersonDetailsDbDirBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                    </td>
                                </tr>
                              </table>
                           </td>
                           <%-- requestor --%>
                             <td style="width:100px"></td>
                            <td>
                             <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Requestor Details</p>
                             <table>            
                                 <tr>
                                <td align="right" width="100px"><strong>Name</strong></td>
                                <td><dx:ASPxComboBox ID="m_requestorDetailsNameSelector" ClientInstanceName="RequestorDetailsNameSelector" runat="server"  AutoPostBack="false" Width="100px">
                                        <Items>
                                            <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                            <dx:ListEditItem Value="Begins" Text="Begins With" />
                                            <dx:ListEditItem Value="Equals" Text="Equals" />
                                            <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                            <dx:ListEditItem Value="Between" Text="Between" />
                                        </Items>
                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleNameReq(); }" />
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsNameText" ClientInstanceName="RequestorDetailsNameText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsNameBetweenText" ClientInstanceName="RequestorDetailsNameBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                </td>
                            </tr>
                                 <tr>
                                <td align="right" width="100px"><strong>Employee ID</strong></td>
                                <td><dx:ASPxComboBox ID="m_requestorDetailsEmployeeIdSelector" ClientInstanceName="RequestorDetailsEmployeeIdSelector" runat="server"  AutoPostBack="false" Width="100px">
                                        <Items>
                                            <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                            <dx:ListEditItem Value="Begins" Text="Begins With" />
                                            <dx:ListEditItem Value="Equals" Text="Equals" />
                                            <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                            <dx:ListEditItem Value="Between" Text="Between" />
                                        </Items>
                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmployeeIdReq(); }" />
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsEmployeeIdText" ClientInstanceName="RequestorDetailsEmployeeIdText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsEmployeeIdBetweenText" ClientInstanceName="RequestorDetailsEmployeeIdBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                </td>
                            </tr>
                                 <tr>
                                <td align="right" width="100px"><strong>Email</strong></td>
                                <td><dx:ASPxComboBox ID="m_requestorDetailsEmailSelector" ClientInstanceName="RequestorDetailsEmailSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                        <Items>
                                            <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                            <dx:ListEditItem Value="Begins" Text="Begins With" />
                                            <dx:ListEditItem Value="Equals" Text="Equals" />
                                            <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                            <dx:ListEditItem Value="Between" Text="Between" />
                                        </Items>
                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmailReq(); }" />
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsEmailText" ClientInstanceName="RequestorDetailsEmailText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="m_requestorDetailsEmailBetweenText" ClientInstanceName="RequestorDetailsEmailBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                </td>
                            </tr>
                                 <tr>
                                    <td align="right" width="100px"><strong>dbDir</strong></td>
                                    <td><dx:ASPxComboBox ID="m_requestorDetailsDbDirSelector" ClientInstanceName="RequestorDetailsDbDirSelector" runat="server"  AutoPostBack="false" Width="100px">
                                            <Items>
                                                <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                                <dx:ListEditItem Value="Begins" Text="Begins With" />
                                                <dx:ListEditItem Value="Equals" Text="Equals" />
                                                <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                                <dx:ListEditItem Value="Between" Text="Between" />
                                            </Items>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleDbDirReq(); }" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_requestorDetailsDbDirText" ClientInstanceName="RequestorDetailsDbDirText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="m_requestorDetailsDbDirBetweenText" ClientInstanceName="RequestorDetailsDbDirBetweenText" runat="server" Width="150px" ClientVisible="false" />
                                    </td>
                              </tr>
                           </table>
                          </td>
                             <%-- passoffice --%>
                            <td style="width:100px"></td>
                           <td>
                          <asp:PlaceHolder runat="server" ID="m_passOfficeholder" Visible="false">
                            <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Pass Office:</p>
                          <table>            
                           <tr>
                             <td valign="top">
                                <dx:ASPxListBox ID="m_passOfficeList" ClientInstanceName="PassOfficeList" runat="server" SelectionMode="Multiple"
			                        Width="300px"
			                        Height="200px"
                                    TextField="DisplayName"
                                    ValueField="PassOfficeID"
			                        ItemStyle-Cursor="pointer">
		                        </dx:ASPxListBox>
                            </td>
                          </tr>
                           <tr></tr>
                        </table>
                        </asp:PlaceHolder>
                      </td>

                        </tr>
                     </table>
                 </td>

              </tr>
              <tr>
                  <td valign="top">
                       <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Area</p>
                      <table>
                          <tr>
                              <td align="right" width="100px">Country </td>
                              <td >
                                  <dx:ASPxComboBox ID="m_countryArea" ClientInstanceName="CountryArea" runat="server" TextField="Name" ValueField="CountryID" AutoPostBack="false" Width="200px">
                                      <ClientSideEvents  SelectedIndexChanged="onCountryChanged"  EndCallback="CountryChangeEnd"/>
                                  </dx:ASPxComboBox>    
                              </td>
                              <td align="right" width="100px">City </td>
                              <td >
                                  <dx:ASPxComboBox ID="m_cityArea" ClientInstanceName="CityArea" runat="server" TextField="Name" ValueField="CityID" AutoPostBack="false" Width="200px" OnCallback="m_city_Callback">
                                       <ClientSideEvents  SelectedIndexChanged="onCityChanged" EndCallback="CityChangeEnd"/>
                                  </dx:ASPxComboBox>      
                              </td>
                              <td align="right" width="100px">Building </td>
                              <td >
                                  <dx:ASPxComboBox ID="m_buildingArea" ClientInstanceName="BuildingArea" runat="server" TextField="Name" ValueField="BuildingID" AutoPostBack="false" Width="200px" OnCallback="m_Building_Callback">
                                      <ClientSideEvents  SelectedIndexChanged="onBuildingChanged"   EndCallback="BuildingChangeEnd"/>
                                  </dx:ASPxComboBox>     
                               </td>
                              <td align="right" width="100px">Floor </td>
                              <td >
                                  <dx:ASPxComboBox ID="m_floorArea" ClientInstanceName="FloorArea" runat="server" TextField="Name" ValueField="FloorID" AutoPostBack="false" Width="200px" OnCallback="m_Floor_Callback">
                                      <ClientSideEvents SelectedIndexChanged="onFloorChanged" EndCallback="FloorChangeEnd" />
                                  </dx:ASPxComboBox>          
                             </td>
                              <td align="right" width="100px">Access area </td>
                              <td >
                                  <dx:ASPxComboBox ID="m_accessArea" ClientInstanceName="AccessArea" runat="server" TextField="Name" ValueField="AccessAreaID" AutoPostBack="false" Width="200px" OnCallback="m_accessArea_Callback"></dx:ASPxComboBox>          
                             </td>
                          </tr>

                      </table>


                  </td>

             </tr>
              <tr>
                  <td valign="top">
                      <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Task</p>
                      <table>
                          <tr>
                             <td align="right" width="100px"><strong>Type:</strong></td>
                             <td>
                                <dx:ASPxComboBox ID="m_requestType" ClientInstanceName="RequestType" runat="server" TextField="RequestType" ValueField="RequestTypeID" AutoPostBack="false" Width="120px"></dx:ASPxComboBox>         
                             </td>
                              <td align="right" width="100px"><strong>Status:</strong></td>
                             <td>
                                <dx:ASPxComboBox ID="m_passOfficeStatus" ClientInstanceName="PassOfficeStatus" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="120px"></dx:ASPxComboBox>         
                             </td>
                          </tr>

                      </table>

                  </td>

              </tr>
              <tr>
                  <td valign="top">
                      <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Request</p>
                      <table>
                          <tr>
                              <td align="right" width="100px"><strong>Status:</strong></td>
                             <td>
                                <dx:ASPxComboBox ID="m_requestStatus" ClientInstanceName="RequestStatus" runat="server" TextField="Name" ValueField="RequestStatusId" AutoPostBack="false" Width="120px"></dx:ASPxComboBox>         
                             </td>
                          </tr>

                      </table>

                  </td>

              </tr>


          </table>
          <div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_searchSubmit" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Search%>"
			        Width="100px" OnClick="m_searchSubmit_click" >
		        </dx:ASPxButton>
	        </div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_seearchReset" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Reset%>"
			        Width="100px" OnClick="m_seearchReset_click">
		        </dx:ASPxButton>
	        </div>
        </div>
        <br />
        <br />
    </asp:Panel>
     <asp:PlaceHolder ID="m_searchOutputHolder" runat="server" Visible="false">
    <div id="divPassOfficeReports">
         <p>Click here to export this data to Excel. <asp:ImageButton ID="ASPxButton2" runat="server" OnClick="btnXlsxExport_Click" SkinID="FileXLS"/> </p>
		
        <dx:ASPxGridView ID="SearchResults" ClientInstanceName="SearchResults"
            runat="server" AutoGenerateColumns="False"
            KeyFieldName="TaskId" 
            ClientIDMode="AutoID" EnableViewState = "false" Settings-ShowFilterRowMenu="true" Settings-ShowFilterRow="true" >
			<SettingsLoadingPanel ShowImage="true" />
            <Columns>  
                 <dx:GridViewDataTextColumn Caption="Request ID" FieldName="RequestID"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="1" >
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>              

				<dx:GridViewDataTextColumn Caption="Task ID" FieldName="TaskId" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="1">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

				<dx:GridViewDataDateColumn Caption="Task Created Date" FieldName="CreatedDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="2">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataDateColumn Caption="Task Created Time" FieldName="CreatedDate" PropertiesDateEdit-DisplayFormatString="HH:mmm:ss" VisibleIndex="3">
				<Settings AllowHeaderFilter="True" AllowSort="False" AllowGroup="False" AllowAutoFilter="False" AllowDragDrop="False"/>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Task Type" FieldName="TaskType" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="4">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Access Area" FieldName="AccessArea"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="5" >
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person Name" FieldName="PersonName" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="6">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person Employee ID" FieldName="PersonEmployeeId" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="7">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person Email" FieldName="PersonEmail" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="8">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person DbDir" FieldName="PersonDBDirID" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="9">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Requestor  Name" FieldName="RequestorName" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="10">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Requestor Employee ID" FieldName="RequestorEmployeeID" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="11">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Requestor Email" FieldName="RequestorEmail" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="12">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Requestor DbDir" FieldName="RequestorDBDirID" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="13">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>


                <dx:GridViewDataDateColumn Caption="Task closed Date" FieldName="CompletedDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="14">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataDateColumn Caption="Task closed Time" FieldName="CompletedDate" PropertiesDateEdit-DisplayFormatString="HH:mmm:ss" VisibleIndex="15">
                    <Settings AllowHeaderFilter="True" AllowSort="False" AllowGroup="False" AllowAutoFilter="False" AllowDragDrop="False"/>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Status" FieldName="TaskStatus"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="16">
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Operator" FieldName="Operator"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="17">
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="CardType" FieldName="CardType"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="18">
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>
                
				
				</Columns>
            <SettingsText EmptyDataRow="No results could be found for your criteria" />
				<SettingsPager PageSize="25"   />
        </dx:ASPxGridView>

    </div>

       <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="SearchResults"></dx:ASPxGridViewExporter>
     </asp:PlaceHolder>



    <ppc:Popups ID="PopupTemplates" runat="server" AutoPostBack="false" />

    <dx:ASPxHiddenField ID="HiddenFieldIDs" ClientInstanceName="hiddenFieldIDs" runat="server"></dx:ASPxHiddenField>
</asp:Content>
