﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/DBIntranet.master" CodeBehind="Login.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.LoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" Runat="Server">
         
       <table>
       <tr>
        <td valign="top">
             <p>To log in as another user, enter their email address below and click 'Log In'.</p>
             <p>Alternatively, select the user from the table on the right.</p>
                   
            <asp:Panel runat="server" id="PanelLogin" SkinID="GreyBorder320">
                   <asp:Panel runat="server" id="Panel4" SkinID="Shaded">
                        <asp:Label ID="Label3" runat="server" Text="Log In" SkinID="Title"></asp:Label>
                   </asp:Panel>
                  
                    <table border="0" cellpadding="1" cellspacing="0" 
                            style="border-collapse:collapse;">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0">                           
                                        <tr>
                                            <td align="right" class="form_label">
                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Email:</asp:Label>
                                            </td>
                                            <td class="form_item">
                                                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                                    ControlToValidate="UserName" ErrorMessage="User Email is required." 
                                                    ToolTip="Please enter the Group Directory Email address." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td align="right" colspan="2">
                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
                                                    ValidationGroup="Login1" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
              </asp:Panel>
        </td>
        <td>
            <asp:Panel runat="server" id="usersPanel" SkinID="GreyBorder">
           <asp:Panel runat="server" id="Panel1" SkinID="Shaded">
                <asp:Label ID="headingLabel" runat="server" Text="Registered Users"></asp:Label>
           </asp:Panel>
           <p class="grey_11" style="text-align:left;">Select a row from the users listed below to log in as another user.<br /></p>
           <div class="padding10" style="text-align:left;">
           <asp:UpdatePanel ID="UpdatePanelUsers" runat="server">
           <ContentTemplate>  
               
               <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="true"
                    AutoGenerateColumns="False"  PageSize="200" PagerSettings-Position="Bottom">
                   <Columns>        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="LoginButton" CommandName="Select" CommandArgument='<%# Eval("EmailAddress") %>' Text="Select" OnCommand="LoginButton_Command"/>
                            </ItemTemplate>
                        </asp:TemplateField>            
                       <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress" 
                           SortExpression="EmailAddress" />
                       <asp:BoundField DataField="Role" HeaderText="Role" 
                           SortExpression="Role" />
                   </Columns>
               </asp:GridView>
           </ContentTemplate>
           </asp:UpdatePanel>
           </div>
           </asp:Panel>  
        
        </td>        
       </tr>       
       </table>        
</asp:Content>    
 
