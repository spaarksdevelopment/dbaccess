﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="AccessAreaAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.AccessAreaAdmin" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">
        var areaType; //either floor or building
        var selectedBuildingId = -1;
        var selectedFloorId = -1;
        var selectedAreaId = -1;
        var isAdmin = '<%= isAdmin %>';
        $(function () {
            $('#add').click(function () {
                $('#AvailableLinkedAreas option:selected').appendTo('#SelectedLinkedAreas');
                return false;
            });
            $('#remove').click(function () {
                $('#SelectedLinkedAreas option:selected').appendTo('#AvailableLinkedAreas');
                return false;
            });
            $("#addAll").click(function () {
                $("#AvailableLinkedAreas option").appendTo("#SelectedLinkedAreas");
            });
            $("#removeAll").click(function () {
                $("#SelectedLinkedAreas option").appendTo("#AvailableLinkedAreas");
            });
            HideFloorGrid(); HideBuildingGrid(); $("#DivisionSelector").hide();
            $('#<%=ddlCountries.ClientID %>').change(function () {
                GetCities();
                HideFloorGrid();
                HideBuildingGrid();
            });
            $('#<%=ddlCities.ClientID %>').change(function () {
                GetBuildings();
                HideFloorGrid();
                HideBuildingGrid();
            });
            $('#<%=ddlBuildings.ClientID %>').change(function () {
                GetFloors();
                GetDivisions();
                GetBuildingAccessAreas();
                HideFloorGrid();
            });
            $('#<%=ddlFloors.ClientID %>').change(function () {
                GetFloorAccessAreas();
            });
            $('#AreaType').change(function () {
                if ($('#AreaType option:selected').val() == 4) {
                    $("#DivisionSelector").show();
                }
                else if (areaType != 'Floor') {
                    $("#DivisionSelector").hide();
                }
            });
        });
        function DisplayFloorGrid() {
            $("#FloorAccessAreaAdminGrid").show();
        }
        function HideFloorGrid() {
            $("#FloorAccessAreaAdminGrid").hide();
        }
        function DisplayBuildingGrid() {
            $("#BuildingAccessAreaAdminGrid").show();
        }
        function HideBuildingGrid() {
            $("#BuildingAccessAreaAdminGrid").hide();
        }

        function Reload() {
            ReloadCategoryName();
        }

        function ShowAdd(type) {//type here indicates if this is an access area for a building or a floor.
            areaType = type;
            //unbind the on click event
            $("#<%= ControlSystem.ClientID %>").unbind('change', Reload);

        if (areaType == 'Floor') {
            $("#DivisionSelector").show();
        }
        else {
            $("#DivisionSelector").hide();
        }
        ClearDetails();
        SetAreaTypes(type);
        $("select[id$=Category] > option").remove();
        LoadAvailableCategory();
        PopupAdd.Show();
        checkPermission(false);
    }

    function ReloadCategoryName() {
        $("select[id$=Category] > option").remove();

        GetCategoryName(selectedAreaId, $("#<%= ControlSystem.ClientID %>").val());
    }

    function EditArea(areaId, controlsystemid, areaTypeId, type) {
        areaType = type;
        if (areaType == 'Floor') {
            $("#DivisionSelector").show();
        }
        else {
            $("#DivisionSelector").hide();
        }
        selectedAreaId = areaId;
        ClearDetails();
        if (controlsystemid == '') {
            controlsystemid = 0;
        }
        var controlsystemval = controlsystemid;

        //BIND
        $("#<%= ControlSystem.ClientID %>").bind('change', Reload);

      //Error here 
      GetCategoryName(areaId, controlsystemval);
      PopulateDetails(areaId, areaTypeId, type);
      SetAreaTypes(type);

      PopupAdd.Show();
      checkPermission(true);
  }
  function ClearDetails() {
      $("#txtAccessArea").val('');
      $("#RecertPeriods").val('0').attr('selected', true);
      $("#AreaType").val(1);
      $("#<%= ControlSystem.ClientID %>").val('0');
        $("#Divisions").val('0');
        $("select[id$=Category] > option").remove();
        $("#Enabled").val('-');
        $("#btnAddArea").val('<%= GetResourceManager("Resources.CommonResource", "AddArea") %>');
    }
    function CancelAdd() {
        PopupAdd.Hide();
    }
    function AddArea() {

        if ($("#txtAccessArea").val() == '') {
            popupAlertTemplateContent.SetText("<%=AccessAdminEnterName.Text%> "); //Please enter an area name.
            popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>"); //Add Area
            popupAlertTemplate.Show();
            return;
        }

        if ($("#RecertPeriods").val() == 0) {
            popupAlertTemplateContent.SetText("<%=AccessAdminSelectRecert.Text %>"); //Please select a recert period.
            popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>");
            popupAlertTemplate.Show();
            return;
        }

        if ($("#AreaType").val() == 0) {
            popupAlertTemplateContent.SetText("<%=AccessAdminSelectAreaType.Text %>"); //Please select an area type.
            popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>");
            popupAlertTemplate.Show();
            return;
        }

        if (areaType == "Building" && $("#AreaType").val() == 4) {
            if ($("#Divisions").val() == 0) {
                popupAlertTemplateContent.SetText("<%=AccessAdminSelectDivision.Text %>"); //Please select a division.
                popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>");
                popupAlertTemplate.Show();
                return;
            }
        }

        else if (areaType == "Floor" && $("#Divisions").val() == 0) {
            popupAlertTemplateContent.SetText("<%=AccessAdminSelectDivision.Text %>");
            popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>");
            popupAlertTemplate.Show();
            return;
        }
        else if ($("#<%= ControlSystem.ClientID %>").val() == 0 || $("#Category").val() == null) {
            popupAlertTemplateContent.SetText("<%=AccessAdminSelectControl.Text %>");//  Please select a control system.
    	    popupAlertTemplate.SetHeaderText("<%=AccessAdminEnterArea.Text %>");
    	    popupAlertTemplate.Show();
    	    return;
    	}
    	else if ($("#Category").val() == 0) {
    	    popupAlertTemplateContent.SetText("<%=AccessAdminSelectCategory.Text %>"); //Please select a valid category.
    	    popupAlertTemplate.SetHeaderText("<%=AccessAdminCategory.Text %>");//category
    	    popupAlertTemplate.Show();
    	    return;
    	}
    	else if (($("#txtDefaultAccessPeriod").val() == '' || $("#txtDefaultAccessPeriod").val() == 0) && $("#ddlDefaultAccess").val() != '-') {
    	    popupAlertTemplateContent.SetText("<%=AccessAdminSelectDefaultAccess.Text %>"); //Please select a default access period.
    	    popupAlertTemplate.SetHeaderText("<%=AccessAdminDefaultAccessPeriod.Text %>"); //default access period
    	    popupAlertTemplate.Show();
    	    return;

    	}
        //    	else if (($("#txtDefaultAccessPeriod").val() != '' || $("#txtDefaultAccessPeriod").val() != 0) && $("#ddlDefaultAccess").val() == '-') {
        //    	    popupAlertTemplateContent.SetText("Please select a default access period.");
        //    	    popupAlertTemplate.SetHeaderText("default access period");
        //    	    popupAlertTemplate.Show();
        //    	    return;

        //    	}
        // we need to set the floor to zero (null) if this is a building that is being accessed

    var iFloor = $("#<%=ddlFloors.ClientID %>").val();
        if ("Building" == areaType) iFloor = 0;

        var k = $("#RecertPeriods").val();

        //(Mahmoud) SET THE DefaultAccessPeriod & DefaultAccessDuration

        var DefaultAccessPeriod = ($("#txtDefaultAccessPeriod").val() == undefined) ? null : $("#txtDefaultAccessPeriod").val();
        var DefaultAccessDuration = ($("#ddlDefaultAccess").val() == undefined) ? null : $("#ddlDefaultAccess").val();

        switch ($("#btnAddArea").val()) {
            case '<%= GetResourceManager("Resources.CommonResource", "AddArea") %>':
                {
                    PageMethods.AddArea($("#AreaType").val(), iFloor, $("#<%=ddlBuildings.ClientID %>").val(), $("#txtAccessArea").val(), $("#Divisions").val(), $("#<%= ControlSystem.ClientID %>").val(), $("#RecertPeriods").val(), $("#Category").val(), $("#Enabled").val(), DefaultAccessPeriod, DefaultAccessDuration, OnAdd);
    	            break;
    	        }
            case '<%= GetResourceManager("Resources.CommonResource", "UpdateArea") %>':
                {
                    var check = $("#Category").val();
                    var corporateDivisionaccessAreaId = hfCorporateDivisionId.Get("CorporateDivisionAccessAreaId");
                    if (corporateDivisionaccessAreaId == "") {
                        corporateDivisionaccessAreaId = 0;
                    }

                    PageMethods.UpdateArea(selectedAreaId, $("#AreaType").val(), iFloor, $("#<%=ddlBuildings.ClientID %>").val(), $("#txtAccessArea").val(), $("#Divisions").val(), $("#<%= ControlSystem.ClientID %>").val(), $("#RecertPeriods").val(), $("#Category").val(), $("#Enabled").val(), corporateDivisionaccessAreaId, DefaultAccessPeriod, DefaultAccessDuration, OnUpdate);
    	            break;
                }
        }
        PopupAdd.Hide();
    }

    function OnAdd(result) {
        if (result > 0) {
            switch (areaType) {
                case "Floor":
                    FloorGrid.PerformCallback(selectedFloorId);
                    break;
                case "Building":
                    BuildingGrid.PerformCallback(selectedBuildingId);
                    break;
            }
        }
    }

    function OnUpdate(result) {
        if (result == "Error") {
            popupAlertTemplateContent.SetText("<%=AccessAdminProblemUpdating.Text %>"); //Sorry, there was a problem updating the access area.
            popupAlertTemplate.SetHeaderText("<%=AccessAdminUpdateArea.Text %>"); //Update Area
            popupAlertTemplate.Show();
            return;
        }

        switch (areaType) {
            case "Floor":
                FloorGrid.PerformCallback(selectedFloorId);
                break;
            case "Building":
                BuildingGrid.PerformCallback(selectedBuildingId);
                break;
        }
    }

    // ----------------------------------------------------------------------

    var g_popup_areaId = 0;

    function DeleteArea(areaId) {

        selectedAreaId = areaId;
        popupConfirmTemplate.SetHeaderText("<%=AccessAdminRemoveAccess.Text %>"); //Remove Access Area
        popupConfirmTemplateContentHead.SetText("<%=AccessAdminAccessArea.Text %>"); //Access Area
        popupConfirmTemplateContentBody.SetText("<%=AccessAdminAccessRemovalConfirm.Text %>"); //Are you sure you want to remove this Access Area?
        popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemoveArea();");
        popupConfirmTemplate.Show();
    }

    function OnRemoveArea() {
        PageMethods.DeleteArea(selectedAreaId, OnDeleteArea);
    }

    function OnDeleteArea(result) {
        var alertText = '';
        if (result == 1) {
            alertText = "<%=AccessAdminAccessDeleteError.Text %>"; //Access Area can not be deleted due to pending / previous requests and tasks, the access area has been disabled instead.
		    }
		    else if (result == 2) {
		        alertText = "<%=AccessAdminAccessDeleteSuccess.Text %>"; //Access Area has been successfully deleted
		        BuildingGrid.PerformCallback(selectedBuildingId);
		    }
		    else if (result == 3) {
		        alertText = "<%=AccessAdminAccessDeleteError2.Text %>"; //'An error occured, unable to delete the access area.';
               }

       popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Show") %>'); //Show
		    popupAlertTemplateContent.SetText(alertText);
		    popupAlertTemplate.Show();
		    FloorGrid.PerformCallback(selectedFloorId);
		}

		// ----------------------------------------------------------------------

		function PopulateDetails(areaId, areaTypeId, areaType) {
		    PageMethods.PopulateDetails(areaId, areaType, areaTypeId, OnPopulateDetails);
		}
		function OnPopulateDetails(result) {
		    var res = result.split("|");

		    if (res.length < 4) {
		        popupAlertTemplateContent.SetText("<%=AccessAdminAccessAreaRetrievalEror.Text %>"); //Sorry, there was a problem retrieving this access area.Sorry, there was a problem retrieving this access area.
		        popupAlertTemplate.SetHeaderText("<%=AccessAdminPopulateArea.Text %>"); //Populate Area
		        popupAlertTemplate.Show();
		        return;
		    }

            $("#txtAccessArea").val(res[0]);
            $("#AreaType").val(res[1]);
            $("#<%= ControlSystem.ClientID %>").val(res[2]);
        $("#RecertPeriods").val(res[3]);
        $("#Enabled").val(res[6]);
        if ((areaType == "Building" && $("#AreaType").val() == 4) || (areaType == "Floor")) {
            $("#Divisions").val(res[4])
            $("#DivisionSelector").show();
        }

        if (res[5] != -100) {
            $("#Category").val(res[5]);
        }

		    //set the corporatedivisionAccess area is on editing the record
        hfCorporateDivisionId.Set("CorporateDivisionAccessAreaId", res[7]);
		    // hfRequestDetail.Set("RequestMasterID_AA", result);

        $("#btnAddArea").val('<%= GetResourceManager("Resources.CommonResource", "UpdateArea") %>');
    }



    function GetDivisions() {
        var languageID = '<%= GetLanguageID() %>';

        if ($('#Divisions option').size() > 0) return;
        var myval = $("#<%=ddlCountries.ClientID%>").val();
        if (myval == "-1") return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetDivisionsByCountry",
            data: "{countryId:'" + myval + "',languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateDivisions(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText("Divisions");
                popupAlertTemplate.Show();
            }
        });
    }



    function GetCities() {
        var languageID = '<%= GetLanguageID() %>';

        $("select[id$=<%=ddlCities.ClientID%>] > option").remove();
        $("select[id$=<%=ddlBuildings.ClientID%>] > option").remove();
        $("select[id$=<%=ddlFloors.ClientID%>] > option").remove();
        var myval = $("#<%=ddlCountries.ClientID%>").val();
        if (myval == "-1") return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetCitiesByCountryId",
            data: "{countryId:'" + myval + "',languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateCities(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Cities") %>');
                popupAlertTemplate.Show();
            }
        });
    }

    function GetBuildings() {
        var languageID = '<%= GetLanguageID() %>';

        $("select[id$=<%=ddlBuildings.ClientID%>] > option").remove();
        $("select[id$=<%=ddlFloors.ClientID%>] > option").remove();
        var myval = $("#<%=ddlCities.ClientID%>").val();
        if (myval == "0") return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetBuildingsByCityId",
            data: "{cityId:'" + myval + "',languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateBuildings(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Building") %>');
                popupAlertTemplate.Show();
            }
        });
    }

    function GetFloors() {
        var languageID = '<%= GetLanguageID() %>';

        $("select[id$=<%=ddlFloors.ClientID%>] > option").remove();
        var myval = $("#<%=ddlBuildings.ClientID%>").val();
        if (myval == "0") return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetFloorsByBuildingId",
            data: "{buildingId:'" + myval + "',languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateFloors(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Floors") %>');
                popupAlertTemplate.Show();
            }
        });
    }

    function GetAccessAreaTypes() {
        var languageID = '<%= GetLanguageID() %>';

        if ($('#AreaType option').size() > 0) return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetAccessAreaTypes",
            data: "{languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateAccessAreaTypes(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "AccessAreaTypes") %>');
                popupAlertTemplate.Show();
            }
        });
    }

    function LoadAvailableCategory() {
        var languageID = '<%= GetLanguageID() %>';

        if ($('#Category option').size() > 0) return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetAvailableCategoryName",
            data: "{languageID:'" + languageID + "'}",
            dataType: "json",
            success: function (data) { PopulateCategoryName(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Category") %>');
                popupAlertTemplate.Show();
            }
        });
    }

    function GetCategoryName(areaid, controlsystemid) {
        if ($('#Category option').size() > 0) return;
        var myval = areaid;
        if (myval == "-1") return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Services/ListBuilder.asmx/GetControlSystemCategoryName",
            data: "{areaId: '" + myval + "', controlsystemid: '" + controlsystemid + "'}",
            dataType: "json",
            success: function (data) { PopulateCategoryName(data.d); },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                popupAlertTemplateContent.SetText(textStatus + " " + errorThrown);
                popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Category") %>');
                popupAlertTemplate.Show();
            }
        });

    }


    function GetBuildingAccessAreas() {
        selectedBuildingId = $("#<%=ddlBuildings.ClientID%>").val();
            hiddenFieldIDs.Set("BuildingID", selectedBuildingId);
            if (selectedBuildingId == "-1") return;
            BuildingGrid.PerformCallback(selectedBuildingId);
            DisplayBuildingGrid();
        }
        function GetFloorAccessAreas() {
            selectedFloorId = $("#<%=ddlFloors.ClientID%>").val();
        hiddenFieldIDs.Set("FloorID", selectedFloorId);
        if (selectedFloorId == "-1") return;
        FloorGrid.PerformCallback(selectedFloorId);
        DisplayFloorGrid();
    }
    function PopulateCities(items) {
        $("select[id$=<%=ddlCities.ClientID%>] > option").remove();
        var options = $("#<%=ddlCities.ClientID%>");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function PopulateBuildings(items) {
        $("select[id$=<%=ddlBuildings.ClientID%>] > option").remove();
        var options = $("#<%=ddlBuildings.ClientID%>");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function PopulateFloors(items) {
        $("select[id$=<%=ddlFloors.ClientID%>] > option").remove();
        var options = $("#<%=ddlFloors.ClientID%>");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function PopulateAccessAreaTypes(items) {
        var options = $("#AreaType");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function PopulateDivisions(items) {
        var options = $("#Divisions");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function PopulateCategoryName(items) {
        var options = $("#Category");
        $.each(items, function (index, value) {
            var city = value.split("|");
            options.append($("<option />").val(city[0]).text(city[1]));
        });
    }
    function SetAreaTypes(type) {

        if (type == "Building") {
            $("#PopupHeader").text("<%=AccessAreaBuildingPhrase.Text %>");
        }

        else if (type == "Floor") {
            $("#PopupHeader").text("<%=AccessAreaFloorPhrase.Text %>");
        }

    var options = $("#AreaType");
    $("select[id$=AreaType] > option").remove();
    switch (type) {
        case "Floor":
            options.append($('<option>').text("- " + '<%=GetResourceManager("Resources.CommonResource","Select")%>' + "  -").val("0"));
            options.append($('<option>').text('<%=GetResourceManager("Resources.CommonResource","Business")%>').val("1"));
            options.append($('<option>').text('<%=GetResourceManager("Resources.CommonResource","Service")%>').val("4"));
            break;
        case "Building":
            options.append($('<option>').text("- " + '<%=GetResourceManager("Resources.CommonResource","Select")%>' + "  -").val("0"));
                options.append($('<option>').text('<%=GetResourceManager("Resources.CommonResource","General")%>').val("3"));
                options.append($('<option>').text('<%=GetResourceManager("Resources.CommonResource","Service")%>').val("4"));
                break;
        }
    }
    function checkPermission(isEdit) {
        if (isAdmin == 'True' || !isEdit)
            $('#txtAccessArea').removeAttr("disabled");
        else
            $('#txtAccessArea').attr("disabled", "true");
    }


    $(document).ready(function () {

        $('#AccessHelp').bind('click', function () {
            $("#AccessHelpInfo").slideToggle("slow");
        });
        $('#AccessHelpInfo').hide();

        $('#AccessHelp').attr('title', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
        $('#AccessHelp').attr('alt', '<%= GetResourceManager("Resources.CommonResource", "divAdminAltImage") %>');
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <h1>
        <asp:Literal ID="AccessAreaAdminTitle" meta:resourcekey="AccessAreaAdminTitle" runat="server" ClientIDMode="Static"></asp:Literal></h1>

    <p>
        <asp:Literal ID="AccessAreaAdmin1" meta:resourcekey="AccessAreaAdmin" runat="server" ClientIDMode="Static"></asp:Literal>
        <img id="AccessHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" />
    </p>
    <br />
    <div id="AccessHelpInfo" class="HelpPopUp">
        <p>
            <asp:Literal ID="AccessAreaGetBuilding" meta:resourcekey="AccessAreaGetBuilding" runat="server" ClientIDMode="Static"></asp:Literal>
        </p>

        <p>
            <asp:Literal ID="AccessAreaIcon" meta:resourcekey="AccessAreaIcon" runat="server" ClientIDMode="Static"></asp:Literal>
            <asp:Image ID="Help" SkinID="FileXLS" AlternateText="<%$ Resources:CommonResource, ExportData%>" title="<%$ Resources:CommonResource, ExportData%>" runat="server" />
        </p>
    </div>
    <br />

    <div id="AccessAreaHeader" class="Header">
        <asp:Literal ID="AccessAreaSelectArea" meta:resourcekey="AccessAreaSelectArea" runat="server" ClientIDMode="Static"></asp:Literal>
    </div>

    <div id="AccessAreaContent" class="Content">
        <br />
        <asp:Literal ID="CountryLiteral" Text="<%$ Resources:CommonResource, Country%>" runat="server" ClientIDMode="Static"></asp:Literal>:
    <asp:DropDownList ID="ddlCountries" runat="server" DataTextField="Name" DataValueField="CountryId" AppendDataBoundItems="true" Width="200px" AutoPostBack="false">
        <asp:ListItem Text="<%$Resources:CommonResource, Select%>" Value="-1"></asp:ListItem>
    </asp:DropDownList>
        <asp:Literal ID="CityLiteral" Text="<%$ Resources:CommonResource, City%>" runat="server" ClientIDMode="Static"></asp:Literal>:
    <asp:DropDownList ID="ddlCities" runat="server" DataTextField="Name" DataValueField="CityId" AppendDataBoundItems="true" Width="200px" AutoPostBack="false"></asp:DropDownList>
        <asp:Literal ID="BuildingLiteral" Text="<%$ Resources:CommonResource, Building%>" runat="server" ClientIDMode="Static"></asp:Literal>:
    <asp:DropDownList ID="ddlBuildings" runat="server" DataTextField="Name" DataValueField="BuildingId" AppendDataBoundItems="true" Width="200px" AutoPostBack="false"></asp:DropDownList>
        <asp:Literal ID="FloorLiteral" Text="<%$ Resources:CommonResource, Floor%>" runat="server" ClientIDMode="Static"></asp:Literal>:
    <asp:DropDownList ID="ddlFloors" runat="server" DataTextField="Name" DataValueField="FloorId" AppendDataBoundItems="true" Width="200px" AutoPostBack="false"></asp:DropDownList><br />
        <br />
    </div>

    <div id="BuildingAccessAreaAdminGrid">
        <div id="BuildingAccessAreaHeader">
            <h1>
                <asp:Literal ID="AccessAreaBuildinHeader" meta:resourcekey="AccessAreaBuildinHeader" runat="server" ClientIDMode="Static"></asp:Literal>
            </h1>
            <p>
                <asp:Literal ID="AccessAreaClickExport" meta:resourcekey="AccessAreaClickExport" runat="server" ClientIDMode="Static"></asp:Literal>

                <asp:ImageButton ID="ImageButton1" meta:resourcekey="AccessAreaClickExportAlt" SkinID="FileXLS" runat="server" OnClick="btnXlsxExportAccessArea_Click" />
            </p>
        </div>


        <div id="BuildingAccessAreaContent" class="Content">
            <dx:ASPxGridView ID="BuildingGrid" ClientInstanceName="BuildingGrid"
                runat="server" AutoGenerateColumns="False" OnCustomColumnDisplayText="OnBuildingCustomColumnDisplayText"
                KeyFieldName="AccessAreaID"
                OnCustomCallback="BuildingGridCustomCallback" Width="700px">
                <Columns>
                    <dx:GridViewDataImageColumn VisibleIndex="0" ShowInCustomizationForm="True">
                        <DataItemTemplate>
                            <asp:Image runat="server" ID="removeImage" SkinID="Trash" AlternateText="Delete" meta:Resourcekey="AccessAreaDelete" onclick='<%# "DeleteArea("+Eval("AccessAreaID")+")" %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataImageColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Name%>" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, AccessType%>" FieldName="AccessAreaType.AccessAreaTypeName" ShowInCustomizationForm="True" VisibleIndex="3" Width="80px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, RecertificationPeriod%>" FieldName="RecertPeriod" ShowInCustomizationForm="True" VisibleIndex="4" Width="80px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, ControlSystem%>" FieldName="ControlSystem.Name" ShowInCustomizationForm="True" VisibleIndex="5"></dx:GridViewDataTextColumn>
                    <%--<dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Active%>" FieldName="Enabled" ShowInCustomizationForm="True" VisibleIndex="5"></dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, Active%>" ShowInCustomizationForm="True" VisibleIndex="5">
                        <DataItemTemplate>
                            <asp:Label Text="<%$ Resources:CommonResource, Enabled%>" runat="server"></asp:Label>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>

                    <dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, Edit%>" VisibleIndex="6" ShowInCustomizationForm="True">
                        <DataItemTemplate>
                            <a href="javascript:{}" id="lbBuildingEdit" runat="server" meta:resourcekey="AccessAreaEdit" onclick='<%#"EditArea(\"" + Eval("AccessAreaID") + "\",\"" + Eval("ControlSystemID") + "\", \"" + Eval("AccessAreaType.AccessAreaTypeID") + "\", \"" + "Building" + "\");"%>'>
                                <asp:Literal runat="server" Text="<%$ Resources:CommonResource, Edit%>"></asp:Literal></a>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn meta:resourcekey="AccessAreaGridHeader" FieldName="ControlSystem.ControlSystemTypeID" ShowInCustomizationForm="True" VisibleIndex="7" Visible="false"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="10" />
                <Settings ShowStatusBar="Visible" />
                <Templates>
                    <StatusBar>
                        <dx:ASPxButton ID="btnShowAdd" runat="server" AutoPostBack="False" meta:resourcekey="AccessAreaAddNew">
                            <ClientSideEvents Click="function(s, e) { ShowAdd('Building');}" />
                        </dx:ASPxButton>
                        <div id="result" style="color: red; float: right;"></div>
                    </StatusBar>
                </Templates>
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="BuildingGrid"></dx:ASPxGridViewExporter>
            <br />
            <br />
        </div>
    </div>

    <div id="FloorAccessAreaAdminGrid">
        <div id="FloorAccessAreaHeader">
            <h1>
                <asp:Literal ID="AccessAreaFloor" meta:resourcekey="AccessAreaFloor" runat="server" ClientIDMode="Static"></asp:Literal></h1>
            <p>
                <asp:Literal ID="AccessAreaClickExport2" meta:resourcekey="AccessAreaClickExport" runat="server" ClientIDMode="Static"></asp:Literal><asp:ImageButton ID="ImageButton2" meta:resourcekey="AccessAreaClickExportAlt" SkinID="FileXLS" runat="server" OnClick="btnXlsxExportFloorGrid_Click" />
            </p>
        </div>
        <div id="FloorAccessAreaContent" class="Content">
            <dx:ASPxGridView ID="FloorGrid" ClientInstanceName="FloorGrid"
                runat="server" AutoGenerateColumns="False" OnCustomColumnDisplayText="OnFloorCustomColumnDisplayText"
                KeyFieldName="AccessAreaID"
                OnCustomCallback="FloorGridCustomCallback" Width="700px">
                <Columns>
                    <dx:GridViewDataImageColumn VisibleIndex="0">
                        <DataItemTemplate>
                            <asp:Image runat="server" ID="removeImage" SkinID="Trash" AlternateText="<%$ Resources:CommonResource, Floor%>" meta:Resourcekey="AccessAreaDelete" onclick='<%# "DeleteArea("+Eval("AccessAreaID")+")" %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataImageColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, Name%>" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, AreaType%>" FieldName="AccessAreaType.AccessAreaTypeName" ShowInCustomizationForm="True" VisibleIndex="3" Width="80px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, RecertificationPeriod%>" FieldName="RecertPeriod" ShowInCustomizationForm="True" VisibleIndex="4" Width="80px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="<%$ Resources:CommonResource, ControlSystem%>" FieldName="ControlSystem.Name" ShowInCustomizationForm="True" VisibleIndex="5"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, Active%>" ShowInCustomizationForm="True" VisibleIndex="6">
                        <DataItemTemplate>
                            <%# (Convert.ToBoolean(Eval("Enabled"))) ? GetResourceManager("Resources.CommonResource", "Enabled") : GetResourceManager("Resources.CommonResource", "Disabled") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="<%$ Resources:CommonResource, Edit%>" VisibleIndex="7">
                        <DataItemTemplate>

                            <a href="javascript:{}" id="lbFloorEdit" runat="server" meta:resourcekey="AccessAreaEdit" onclick='<%#"EditArea(\"" + Eval("AccessAreaID") + "\",\"" + Eval("ControlSystemID") + "\",\"" + Eval("AccessAreaType.AccessAreaTypeID") + "\",\"" + "Floor"+ "\");"%>'>
                                <asp:Literal ID="GridViewCommandColumnEdit" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" runat="server"></asp:Literal></a>
                        </DataItemTemplate>

                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn meta:resourcekey="AccessAreaControlSystem" FieldName="ControlSystem.ControlSystemTypeID" ShowInCustomizationForm="True" VisibleIndex="8" Visible="false"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="10" />
                <Settings ShowStatusBar="Visible" />
                <Templates>
                    <StatusBar>
                        <dx:ASPxButton ID="btnShowAdd" runat="server" AutoPostBack="False" meta:resourcekey="AccessAreaAddNew">
                            <ClientSideEvents Click="function(s, e) { ShowAdd('Floor');}" />
                        </dx:ASPxButton>
                        <div id="result" style="color: red; float: right;"></div>
                    </StatusBar>
                </Templates>
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter ID="gridExport1" runat="server" GridViewID="FloorGrid"></dx:ASPxGridViewExporter>
            <br />
            <br />
        </div>
    </div>
    <br />

    <dx:ASPxPopupControl ID="PopupAdd" ClientInstanceName="PopupAdd" runat="server"
        SkinID="PopupCustomSize"
        Width="700px"
        Height="400px"
        meta:resourcekey="AccessAreaAddEditHead">
        <ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl" runat="server" SupportsDisabledAttribute="True">
                <h2><span id="PopupHeader"></span><%--&nbsp; <asp:Literal ID="AccessAdminAccessArea2" meta:resourcekey="AccessAdminAccessArea" runat="server"  ClientIDMode="Static"></asp:Literal>--%></h2>
                <br />

                <span class="TaskDetailLabelExtended">
                    <asp:Literal ID="AccessAreaName" meta:resourcekey="AccessAreaName" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent"><input type="text" id="txtAccessArea" style="width: 200px;"/></span>
                <span class="TaskDetailLabelExtended">
                    <asp:Literal ID="AccessAreaRecertPeriod" meta:resourcekey="AccessAreaRecertPeriod" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent"><select id="RecertPeriods" style="width: 200px;">
                        <option value="0">-
                            <asp:Literal ID="SelectLiteral" Text="<%$ Resources:CommonResource, Select%>" runat="server" ClientIDMode="Static"></asp:Literal>
                            -</option>
                        <option value="1">
                            <asp:Literal ID="AccessAreaMonthly" meta:resourcekey="AccessAreaMonthly" runat="server" ClientIDMode="Static"></asp:Literal></option>
                        <option value="3">
                            <asp:Literal ID="AccessAreaQuarterly" meta:resourcekey="AccessAreaQuarterly" runat="server" ClientIDMode="Static"></asp:Literal></option>
                        <option value="6">
                            <asp:Literal ID="AccessAreaBiAnnual" meta:resourcekey="AccessAreaBiAnnual" runat="server" ClientIDMode="Static"></asp:Literal></option>
                        <option value="12">
                            <asp:Literal ID="AccessAreaAnnual" meta:resourcekey="AccessAreaAnnual" runat="server" ClientIDMode="Static"></asp:Literal></option>
                    </select></span>
                <span class="TaskDetailLabelExtended">
                    <asp:Literal ID="AccessAreaAreaType" meta:resourcekey="AccessAreaAreaType" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent"><select id="AreaType" style="width: 200px;">
                        <option value="0">-
                            <asp:Literal ID="Literal1" Text="<%$ Resources:CommonResource, Select%>" runat="server" ClientIDMode="Static"></asp:Literal>
                            -</option>
                        <%--            <option value="1">Business</option>
                <option value="3">General</option>
                <option value="4">Service</option>--%>
                    </select></span>
                <div id="DivisionSelector">
                    <span class="TaskDetailLabelExtended">
                        <asp:Literal ID="DivisionLiteral" Text="<%$ Resources:CommonResource, Division%>" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent"><select id="Divisions" style="width: 300px;">
                        </select></span>
                </div>
                <div id="">
                    <span class="TaskDetailLabelExtended">
                        <asp:Literal ID="AccessAreaControl" meta:resourcekey="AccessAreaControl" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent">
                            <asp:DropDownList ID="ControlSystem" runat="server" Width="200px" DataTextField="Name" DataValueField="ControlSystemId"></asp:DropDownList>
                        </span>
                </div>
                <span class="TaskDetailLabelExtended">
                    <asp:Literal ID="AccessAreaControlCatgeoryName1" meta:resourcekey="AccessAreaControlCatgeoryName" runat="server"></asp:Literal>:</span><span class="TaskDetailContent"><select id="Category" style="width: 400px;">
                    </select></span>

                <span class="TaskDetailLabelExtended">
                    <asp:Literal ID="Literal3" Text="<%$ Resources:CommonResource,Enabled %>" runat="server" ClientIDMode="Static"></asp:Literal>:</span><span class="TaskDetailContent">
                        <select id="Enabled" style="width: 200px;">
                            <option value="-">-
                                <asp:Literal ID="Literal4" Text="<%$ Resources:CommonResource, Select%>" runat="server" ClientIDMode="Static"></asp:Literal>
                                -</option>
                            <option value="True">
                                <asp:Literal ID="Literal5" Text="<%$ Resources:CommonResource, Yes%>" runat="server" ClientIDMode="Static"></asp:Literal></option>
                            <option value="False">
                                <asp:Literal ID="Literal6" Text="<%$ Resources:CommonResource, No%>" runat="server" ClientIDMode="Static"></asp:Literal></option>
                        </select>
                    </span>

                <%--<span class="TaskDetailLabel">Default Access Period:</span><span class="TaskDetailContent"><input type="text" id="txtDefaultAccessPeriod" style="width:100px;" /></span>

                 <span class="TaskDetailLabel">Duration:</span><span class="TaskDetailContent">                
                   <select id="ddlDefaultAccess" style="width:200px;">
                        <option value="-">- Select -</option>
                        <option value="day">Day(s)</option>
                        <option value="weeks">Week(s)</option>
                        <option value="months">Month(s)</option>
                        <option value="years">Year(s)</option>
                    </select>
                 </span>--%>


                <span class="TaskDetailLabelExtended">&nbsp;</span>
                <span class="TaskDetailLabelExtended">&nbsp;</span>
                <span class="TaskDetailContent">
                    <input type="button" id="btnAddArea" onclick="AddArea()" class="htmlButton" style="width: auto" />
                    <input type="button" id="btnCancel" runat="server" value="<%$ Resources:CommonResource, Cancel%>" onclick="CancelAdd()" class="htmlButton" />
                </span>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxHiddenField ID="HiddenFieldIDs" ClientInstanceName="hiddenFieldIDs" runat="server"></dx:ASPxHiddenField>
    <dx:ASPxHiddenField ID="hfCorporateDivisionId" ClientInstanceName="hfCorporateDivisionId" runat="server"></dx:ASPxHiddenField>

    <ppc:Popups ID="PopupTemplates" runat="server" AutoPostBack="false" />

    <!--literals-->

    <asp:Literal ID="AccessAdminEnterName" meta:resourcekey="AccessAdminEnterName" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminEnterArea" meta:resourcekey="AccessAdminEnterArea" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminSelectRecert" meta:resourcekey="AccessAdminSelectRecert" Visible="false" runat="server"></asp:Literal>

    <asp:Literal ID="AccessAdminSelectAreaType" meta:resourcekey="AccessAdminSelectAreaType" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminSelectDivision" meta:resourcekey="AccessAdminSelectDivision" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminSelectControl" meta:resourcekey="AccessAdminSelectControl" Visible="false" runat="server"></asp:Literal>

    <asp:Literal ID="AccessAdminSelectCategory" meta:resourcekey="AccessAdminSelectCategory" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminCategory" meta:resourcekey="AccessAdminCategory" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminSelectDefaultAccess" meta:resourcekey="AccessAdminSelectDefaultAccess" Visible="false" runat="server"></asp:Literal>

    <asp:Literal ID="AccessAdminDefaultAccessPeriod" meta:resourcekey="AccessAdminDefaultAccessPeriod" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminProblemUpdating" meta:resourcekey="AccessAdminProblemUpdating" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminUpdateArea" meta:resourcekey="AccessAdminUpdateArea" Visible="false" runat="server"></asp:Literal>

    <asp:Literal ID="AccessAdminRemoveAccess" meta:resourcekey="AccessAdminRemoveAccess" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminAccessArea" meta:resourcekey="AccessAdminAccessArea" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminAccessRemovalConfirm" meta:resourcekey="AccessAdminAccessRemovalConfirm" Visible="false" runat="server"></asp:Literal>

    <asp:Literal ID="AccessAdminAccessDeleteError" meta:resourcekey="AccessAdminAccessDeleteError" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminAccessDeleteSuccess" meta:resourcekey="AccessAdminAccessDeleteSuccess" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminAccessDeleteError2" meta:resourcekey="AccessAdminAccessDeleteError2" Visible="false" runat="server"></asp:Literal>


    <asp:Literal ID="AccessAdminAccessAreaRetrievalEror" meta:resourcekey="AccessAdminAccessAreaRetrievalEror" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAdminPopulateArea" meta:resourcekey="AccessAdminPopulateArea" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAreaBuildingPhrase" meta:resourcekey="AccessAreaBuildingPhrase" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="AccessAreaFloorPhrase" meta:resourcekey="AccessAreaFloorPhrase" Visible="false" runat="server"></asp:Literal>






</asp:Content>



