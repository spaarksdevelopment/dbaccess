﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxGridView;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
	public partial class AccessAreaAdmin : BaseSecurityPage
	{
        protected bool isAdmin = false;

        protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				Int32 iFloorID = -1;
				Int32 iBuildingID = -1;

				iFloorID = Int32.Parse(HiddenFieldIDs.Get("FloorID").ToString());
				iBuildingID = Int32.Parse(HiddenFieldIDs.Get("BuildingID").ToString());

				if (-1 != iFloorID) GetFloorAccessAreas(iFloorID);
				if (-1 != iBuildingID) GetBuildingAccessAreas(iBuildingID);
			}
			else
			{
				HiddenFieldIDs.Set("FloorID", -1);
				HiddenFieldIDs.Set("BuildingID", -1);
				GetCountries();
                PopulateAccessAreas();
			}

            isAdmin = CurrentUser.IsInRole("ADMIN");

        }


        private void PopulateAccessAreas()
        {
            List<ControlSystem> controlSystems = Director.GetControlSystemsEnabled();

            controlSystems.Insert(0, new DBAccessController.DAL.ControlSystem() { Enabled = true, ControlSystemID = 0, Name = GetResourceManager("Resources.CommonResource", "PleaseSelect") });

            ControlSystem.DataSource = controlSystems;
            ControlSystem.DataBind();

        }


		private void GetCountries()
		{
            if (IsRestrictedUser)
            {
                List<LocationCountry> filterCountry = new List<LocationCountry>();
                filterCountry.Add(Director.GetCountry(RestrictedCountryId));

                ddlCountries.DataSource = filterCountry;
                ddlCountries.DataBind();
            }
            else
            {
                ddlCountries.DataSource = Director.GetCountriesByDbPeopleID(base.CurrentUser.dbPeopleID, base.CurrentUser.LanguageId);
                ddlCountries.DataBind();
            }
		}

		protected void FloorGridCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			Int32 iFloorID = Convert.ToInt32(e.Parameters);

			GetFloorAccessAreas(iFloorID);
		}

		protected void BuildingGridCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			Int32 iBuildingID = Convert.ToInt32(e.Parameters);

			GetBuildingAccessAreas(iBuildingID);
		}

		private void GetFloorAccessAreas(int floorId)
		{
			FloorGrid.DataSource = Director.GetAccessAreasForEdit(floorId, "Floor");
			FloorGrid.DataBind();
		}

		private void GetBuildingAccessAreas(int buildingId)
		{
			List<spaarks.DB.CSBC.DBAccess.DBAccessController.DAL.AccessArea> data= Director.GetAccessAreasForEdit(buildingId, "Building");

            //switch (CurrentUser.LanguageId)
            //{
            //    case 2:
            //        foreach (var dt in data)
            //        {
            //           dt.
            //        }
            //        break;
            //    case 3:
            //        break;
            //    default:
            //        break;
            //}
            BuildingGrid.DataSource = data;
			BuildingGrid.DataBind();
		}

      
		/// <summary>
		/// We need to translate the numeric value into a textual representation for RecertPeriod
		/// </summary>
		protected void OnBuildingCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName != "RecertPeriod") return;

			Int32 iMonths = Convert.ToInt32(e.Value);
			e.DisplayText = Director.GetRecertPeriodText(iMonths,CurrentUser.LanguageId);
		}

		/// <summary>
		/// We need to translate the numeric value into a textual representation for RecertPeriod
		/// </summary>
		protected void OnFloorCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName != "RecertPeriod") return;

			Int32 iMonths = Convert.ToInt32(e.Value);
			e.DisplayText = Director.GetRecertPeriodText(iMonths,CurrentUser.LanguageId);
		}

		/// <summary>
		/// Excell upload Access Area
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnXlsxExportAccessArea_Click(object sender, EventArgs e)
		{
			gridExport.WriteXlsxToResponse();
		}

		/// <summary>
		/// Excell upload Floor Area
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnXlsxExportFloorGrid_Click(object sender, EventArgs e)
		{
			gridExport1.WriteXlsxToResponse();
		}

        public int? GetLanguageID()
        {
            return  base.CurrentUser.LanguageId;
           
        }

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
       

        #region WebMethods
        [WebMethod, ScriptMethod]
        public static int AddArea(int areaTypeId, int floorId, int buildingId, string name, int divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int? defaultAccessPeriod, string defaultAccessDuration )
        {
            //no base class available when using a static method, so need to get the users
            var currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
			return director.CreateAccessArea(areaTypeId, floorId, buildingId, name, divisionId, controlSystemId, recertPeriod, categoryid, enabled, defaultAccessPeriod, defaultAccessDuration);
            
		}

		[WebMethod, ScriptMethod]
        public static string UpdateArea(int areaId, int areaTypeId, int floorId, int buildingId, string name, int divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int corporateDivisionaccessAreaId, int? defaultAccessPeriod, string defaultAccessDuration)
		{
			//no base class available when using a static method, so need to get the user
			var currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            			
            Director director = new Director(currentUser.dbPeopleID);
			if (director.UpdateAccessArea(areaId, areaTypeId, floorId, buildingId, name, divisionId, controlSystemId, recertPeriod, categoryid, enabled, corporateDivisionaccessAreaId, defaultAccessPeriod, defaultAccessDuration))
				return areaTypeId.ToString();
			else
				return "Error";
		}


        [WebMethod, ScriptMethod]
        public static int DeleteArea(int areaId)
        {
            //no base class available when using a static method, so need to get the user
            var currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
			return director.DeleteAccessArea(areaId);
		}

		[WebMethod, ScriptMethod]
		public static string PopulateDetails(int areaId, string areaType, int areaTypeId)
		{
			var sb = new StringBuilder();

			List<string> s = Director.GetAccessAreaForEdit(areaId, areaType, areaTypeId);

			if (s != null)
			{
				foreach (string g in s)
				{
					sb.Append(g + "|");
				}
			}

			return sb.ToString();
		}

        [WebMethod, ScriptMethod]
        public static string GetLinkedAccessAreas(int buildingId)
        {
            return Director.GetLinkedAccessAreas(buildingId);
        }

        [WebMethod, ScriptMethod]
        public static int DeleteAccessArea(int accessAreaID)
        {
            return 0;
        }
        #endregion
    }
}
