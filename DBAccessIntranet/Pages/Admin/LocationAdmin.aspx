﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" 
	CodeBehind="LocationAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.LocationAdmin"%>

<%@ Register Src="~/Controls/AdminControls/LocationSelectorAdmin.ascx" TagName="LocationSelector" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdminControls/BuildingFloorSelectorAdmin.ascx" TagName="PhysicalSelector" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdminControls/PassOfficeAdmin.ascx" TagName="PassOfficeAdmin" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdminControls/LandLordAdmin.ascx" TagName="LandlordAdmin" TagPrefix="uc4" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#CountryHeader").toggleClass('Selected');
            $('#BuildContent').hide();
            $('#PassContent').hide();
            $('#BuildingContent').hide();
            $('#LandLordContent').hide();

            $('#CountryHeader').bind('click', function () {
                $("#CountryContent").slideToggle("slow");
                $("#CountryHeader").toggleClass('Selected');
            });
            $('#BuildHeader').bind('click', function () {
                $("#BuildContent").slideToggle("slow");
                $("#BuildHeader").toggleClass('Selected');
            });
            $('#PassHeader').bind('click', function () {
                $("#PassContent").slideToggle("slow");
                $("#PassHeader").toggleClass('Selected');
            });
            $('#BuildingHeader').bind('click', function () {
                $("#BuildingContent").slideToggle("slow");
                $("#BuildingHeader").toggleClass('Selected');
            });

            $('#LandLordHeader').bind('click', function () {
                $("#LandLordContent").slideToggle("slow");
                $("#LandLordHeader").toggleClass('Selected');
            });
        });

        $(document).ready(function () {
            $('#AccessHelp').bind('click', function () {
                $("#AccessHelpInfo").slideToggle("slow");
            });
            $('#AccessHelpInfo').hide();
        });
    </script>

    <h1><asp:Literal runat="server" Text="<%$Resources:SiteMapLocalizations, LocationAdministrationTitle%>"></asp:Literal></h1>
    <div id="CountryHeader" class='Header'>              
    <asp:Literal runat="server" Text="<%$Resources:SiteMapLocalizations, RegionsCountriesCitiesTitle%>"></asp:Literal>
	</div>
    <div id="CountryContent" class='Content'>
        <uc1:LocationSelector ID="Location" runat="server" />
    </div>
    <div id="BuildHeader" class='Header'>
	<asp:Literal runat="server" Text="<%$Resources:SiteMapLocalizations, BuildingsAndFloorsTitle%>"></asp:Literal>
    </div>
    <div id="BuildContent" class='Content'>
        <uc2:PhysicalSelector ID="buildings" runat="server" />
    </div>
    <div id="PassHeader" class='Header'>
	<asp:Literal runat="server" Text="<%$Resources:SiteMapLocalizations, PassOfficeTitle%>"></asp:Literal>
    </div>
    <div id="PassContent" class='Content'>
        <uc3:PassOfficeAdmin id="PassOffice" runat="server" />
    </div>
    <div id="LandLordHeader" class="Header">
	<asp:Literal runat="server" Text="<%$Resources:SiteMapLocalizations, LandlordTitle%>"></asp:Literal>
    </div>
    <div id="LandLordContent" class="content">
        <uc4:LandlordAdmin id="LandLord" runat="Server" />
    </div>
 
	<ppc:Popups ID="PopupTemplates" runat="server" />

</asp:Content>
