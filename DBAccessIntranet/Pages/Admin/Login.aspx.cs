﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using Microsoft.Security.Application;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
    public partial class LoginPage : BasePage
    {
        protected void LoginButton_Command(object sender, CommandEventArgs e)
        {
            string email = e.CommandArgument.ToString();
            Login(email);
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            string email = Encoder.HtmlEncode(UserName.Text);
            Login(email);
		}

        protected void Login(string username)
        {
            DBSqlMembershipProvider DBSqlMembershipProvider = new DBSqlMembershipProvider();

            if (DBSqlMembershipProvider.ValidateEmailAddress(username))
            {
                DBAppUser User = DBSqlMembershipProvider.GetDBAppUser(username);

                if (User == null)
                {
                    base.DisplayMessage("Log In", "User is not registered to use the site: " + username);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(username, false);
                    Global.AdminLoginOverride = true;
                    base.Redirect("~/Pages/Home.aspx");
                }
            }
            else
            {
                base.DisplayMessage("Log In", "Invalid email address: " + username);
            }
        }
    }
}
