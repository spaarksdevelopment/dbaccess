﻿<%@ Page Language="C#"  MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="SkippedRequestsReport.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.SkippedRequestsReport" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
      <h1>Unactioned Triggers Report</h1>
    <asp:Panel runat="server" ID="searchFilters">  
          <table>
              <tr>
                  <td valign="top">
                   <table>
                      <tr>
                       <td valign="top">
                         <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Created</p>
                        <table>            
                            <tr>
                                <td><strong>Date &amp; Time</strong></td>
                                <td>Start: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_logDateStartDate" 
					                ClientInstanceName="TaskDateStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                   
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>End: </td>
                                <td>
                                    <dx:ASPxDateEdit ID="m_logDateEndDate" 
					                ClientInstanceName="TaskDateEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						                EditFormatString="dd-MMM-yyyy"
						                DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					                </dx:ASPxDateEdit>                    
                                </td>
                            </tr>
                            <tr>
                              <td colspan="3"><p style="color: red;font-weight: bold;">
                                  <asp:Literal ID="m_voidDateErrorMessage" runat="server" Text="Please choose a start and end date" Visible="false" />
                                  <asp:Literal ID="m_dateSpanErrorMessage" runat="server"  Text="Date range should be within maximum of 31 days" Visible="false"/></p>
                              </td>
                           </tr>
                        </table>                     

                      </td>
                             
                    </tr>
                 </table>
                </td>
              </tr>

              <tr>
                   <td valign="top">
                      <table>
                         <tr>
                          
                           <td>
                          <asp:PlaceHolder runat="server" ID="m_countryholder" Visible="true">
                            <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Country:</p>
                          <table>            
                           <tr>
                             <td valign="top">
                                <dx:ASPxListBox ID="m_countryList" ClientInstanceName="PassOfficeList" runat="server" SelectionMode="Multiple"
			                        Width="300px"
			                        Height="200px"
                                    TextField="DisplayName"
                                    ValueField="PassOfficeID"
			                        ItemStyle-Cursor="pointer">
		                        </dx:ASPxListBox>
                            </td>                          
                          </tr>
                           <tr></tr>
                        </table>
                        </asp:PlaceHolder>
                      </td>

                        </tr>
                     </table>
                 </td>

              </tr>
          </table>
          <div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_searchSubmit" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Search%>"
			        Width="100px" OnClick="m_searchSubmit_click" >
		        </dx:ASPxButton>
	        </div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_seearchReset" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Reset%>"
			        Width="100px" OnClick="m_seearchReset_click">
		        </dx:ASPxButton>
	        </div>
        </div>
        <br />
        <br />
        <br />
    </asp:Panel>
 <asp:PlaceHolder ID="m_searchOutputHolder" runat="server" Visible="false">

    <div id="divPassOfficeReports">
         <p>Click here to export this data to Excel. <asp:ImageButton ID="ASPxButton2" runat="server" OnClick="btnXlsxExport_Click" SkinID="FileXLS"/> </p>
		
        <dx:ASPxGridView ID="SearchResults" ClientInstanceName="SearchResults"
            runat="server" AutoGenerateColumns="False"
            KeyFieldName="PeopleID" 
            ClientIDMode="AutoID" EnableViewState = "false" Settings-ShowFilterRowMenu="true" Settings-ShowFilterRow="true" style="margin-left: 5px"  >
			<SettingsLoadingPanel ShowImage="true" />
            <Columns>  
                <dx:GridViewDataDateColumn Caption="Date" FieldName="CreatedDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="1">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn> 

                <dx:GridViewDataTextColumn Caption="Person Name" FieldName="PersonName" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="2">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person Employee ID" FieldName="PeopleID"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="3" >
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>  

                <dx:GridViewDataTextColumn Caption="Person Email" FieldName="EmailAddress" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="4">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>
  
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Message" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="5">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataDateColumn Caption="Trigger Date" FieldName="MessageTimestamp" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="6">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
				</dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Task ID" FieldName="TaskID" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="7">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

				</Columns>
            <SettingsText EmptyDataRow="No results could be found for your criteria" />
				<SettingsPager PageSize="25"   />
        </dx:ASPxGridView>
    </div>

       <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="SearchResults"></dx:ASPxGridViewExporter>
     </asp:PlaceHolder>

    <ppc:Popups ID="PopupTemplates" runat="server" AutoPostBack="false" />

    <dx:ASPxHiddenField ID="HiddenFieldIDs" ClientInstanceName="hiddenFieldIDs" runat="server"></dx:ASPxHiddenField>
</asp:Content>
