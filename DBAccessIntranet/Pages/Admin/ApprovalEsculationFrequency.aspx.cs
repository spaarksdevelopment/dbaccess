﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Web.Services;
using System.Web.Script.Services;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
	public partial class ApprovalEsculationFrequency : BaseSecurityPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			LoadEsculationFrequency();
		}

		protected void LoadEsculationFrequency()
		{
			//getting ra by passing tasktype 2
			int n_RATaskType = 2;
			int nRA_Frequencry = Director.GetApproverEsculationFrequency(n_RATaskType);

			ddlRADropDownList.Value = nRA_Frequencry;

			//getting ra by passing tasktype 2
			int n_AATaskType = 3;
			int nAA_Frequencry = Director.GetApproverEsculationFrequency(n_AATaskType);

			ddlAADropDownList.Value = nAA_Frequencry;
		}

		[WebMethod, ScriptMethod]
		public static bool UpdateApproverEsculation(int nRApprover, int nAApprover)
		{
			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			Director director = new Director(CurrentUser.dbPeopleID);
			bool success = director.UpdateApproverEsculation(nRApprover, nAApprover);

			return success;
		}
	}
}