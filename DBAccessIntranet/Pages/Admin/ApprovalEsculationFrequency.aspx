﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ApprovalEsculationFrequency.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.ApprovalEsculationFrequency" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
  <script type="text/javascript">

  	$(document).ready(function ()
  	{
  		$('#AccessHelp').bind('click', function ()
  		{
  			$("#AccessHelpInfo").slideToggle("slow");
  		});
  		$('#AccessHelpInfo').hide();
  	});

  	function UpdateApproverEsculation()
  	{
  		var n_RApprover = ddlRADropDownList.GetValue();
  		var n_AApprover = ddlAADropDownList.GetValue();
		
  		if (n_RApprover != null || n_AApprover != null)
  		{
  			if (n_RApprover != "Please Select" && n_AApprover != "Please Select")
  			{
  				n_RApprover = (n_RApprover == null) ? 0 : n_RApprover;
  				n_AApprover = (n_AApprover == null) ? 0 : n_AApprover;

  				PageMethods.UpdateApproverEsculation(n_RApprover, n_AApprover, OnUpdate);
  			}
  			else
  			{
  				popupAlertTemplateContent.SetText("Please Select  a valid Escalation Frequency");
  				popupAlertTemplate.SetHeaderText("Invalid Frequency");
  				popupAlertTemplate.Show();
  			}
		}
  	}

	function OnUpdate(bval)
	{
		if(!bval)
		{
			popupAlertTemplateContent.SetText("Sorry Escalation Update failed");
			popupAlertTemplate.SetHeaderText("Update Failed");
			popupAlertTemplate.Show();

		}
		else
		{
			popupAlertTemplateContent.SetText("Escalation Updated Successfully");
			popupAlertTemplate.SetHeaderText("Update Success");
			popupAlertTemplate.Show();

		}
	}
    </script>

 <h1>Approval  Escalation</h1>
 <p>Escalation Frequency. <img id="AccessHelp" src="../../App_Themes/DBIntranet2010/images/question.gif" title="Click here for help" alt="Click here for help"/></p>
    <br />
    <div id="AccessHelpInfo" class="HelpPopUp">
        <p>Update the Escalation frequency for Access Approval and Request Approval</p>            
    </div>

	 <div>
        <asp:Table ID="Table1" runat="server">
                <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell>
                                <asp:Label ID="lblRAApproverAdmin"  runat="server" Text="Select Request Approver Escalation  "></asp:Label>
                         </asp:TableCell>
                         <asp:TableCell>
                               <dx:ASPxComboBox ID="ddlRADropDownList" ClientInstanceName="ddlRADropDownList" runat="server" AutoPostBack="false">
									<Items>
										<dx:ListEditItem Text="Please Select" Value="Please Select" Selected="true" />
										<dx:ListEditItem Text="24" Value="24" />
										<dx:ListEditItem Text="48" Value="48" />
										<dx:ListEditItem Text="72" Value="72" />
									</Items>
                                </dx:ASPxComboBox>
                          </asp:TableCell>
                 </asp:TableRow>      
				<asp:TableRow ID="TableRow2" runat="server">
                        <asp:TableCell>
                                <asp:Label ID="lblAAApproverAdmin" runat="server" Text="Select Access Approver Escalation  "></asp:Label>
								<br />
                         </asp:TableCell>
                         <asp:TableCell>
								<dx:ASPxComboBox ID="ddlAADropDownList" ClientInstanceName="ddlAADropDownList" runat="server" AutoPostBack="false">
									<Items>
										<dx:ListEditItem Text="Please Select" Value="Please Select" Selected="true" />
										<dx:ListEditItem Text="24" Value="24" />
										<dx:ListEditItem Text="48" Value="48" />
										<dx:ListEditItem Text="72" Value="72" />
									</Items>
                                </dx:ASPxComboBox>
								<br />
                          </asp:TableCell>
                 </asp:TableRow>    
				 <asp:TableRow ID="TableRow3" runat="server">
					<asp:TableCell>
					</asp:TableCell>
					<asp:TableCell>
							<dx:ASPxButton ID="AspxUpdateEsculationApprover" ClientInstanceName="AspxUpdateEsculationApprover" AutoPostBack="false" runat="server" Text="Update">
								<ClientSideEvents Click="UpdateApproverEsculation" />
							</dx:ASPxButton>
					</asp:TableCell>
				 </asp:TableRow>                        
        </asp:Table>
        <br />
    </div>
	<ppc:Popups ID="PopupTemplates" runat="server" />

</asp:Content>

