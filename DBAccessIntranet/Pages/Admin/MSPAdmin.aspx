﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="MSPAdmin.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.MSPAdmin" %>

<%@ Register Src="~/Controls/Selectors/PersonSelector.ascx" TagName="PersonSelector" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
<h1>Assign Managed Service Providers to Country</h1>
<br />
<br />
<p>Please Select a Region from the drop down list.</p>
<dx:ASPxListBox ID="Region" ClientIDMode="Static" runat="server"></dx:ASPxListBox>
<p>Please Select a Country from the drop down list.</p>
<dx:ASPxListBox ID="Country" ClientIDMode="Static" runat="server"></dx:ASPxListBox>
<p>Please Select a Vendor from the drop down list. </p>
<dx:ASPxListBox ID="VendorName" ClientIDMode="Static" runat="server"></dx:ASPxListBox>
<p>Enter the person's name and select from the list.</p>
<uc1:PersonSelector ID="externalMSP" runat="server" ServiceMethod="GetPersonCompletionListWithValidIds" />
<dx:ASPxButton ID="addPerson" runat="server" >
</dx:ASPxButton>
  
<dx:ASPxGridView ID="externalPeople" runat="server" >

</dx:ASPxGridView>
</asp:Content>
