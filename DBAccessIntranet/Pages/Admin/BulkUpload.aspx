﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
    AutoEventWireup="true" CodeBehind="BulkUpload.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.BulkUpload" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Assembly="Brettle.Web.NeatUpload" Namespace="Brettle.Web.NeatUpload"
    TagPrefix="Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">

        function nodeExpandCollapse(obj, e) {
            e.cancel = true;
        }

        function ValidateFileType(fileName) {
            fileName = fileName.replace(/^\s|\s$/g, ""); //trims string

            if (fileName.match(/([^\/\\]+)\.(xlsx)$/i)) { //eg: xlsx|html|htm|shtml|php
                return true;
            }
            else {
                return false;
            }
        }

        function ValidateUploadFile(source, args) {
            var file = document.getElementById('<%= inputFileID.ClientID %>');

            if (file.value == "") {
                args.IsValid = false;
            }
            else {
                if (ValidateFileType(file.value)) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }

        function ImportClick(source, args) {
            popupPostSubmit.Show();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <h2>Import History  </h2>
    <dx:ASPxGridView runat="server" ID="gvImportHistoty" AutoGenerateColumns="false" KeyFieldName="ID"
        OnPageIndexChanged="gvImportHistoty_PageIndexChanged" OnBeforeColumnSortingGrouping="gvImportHistoty_BeforeColumnSortingGrouping">
        <Columns>
            <dx:GridViewDataTextColumn Caption="File Name" FieldName="Name">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Imported By" FieldName="ImportedByName">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn Caption="Imported Date" FieldName="ImportedDate" CellStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy hh:mm tt">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataColumn>
                <DataItemTemplate>
                    <asp:LinkButton runat="server" OnClientClick="return confirm('Are you sure you want to rollback this file?')"
                        OnClick="btnRollback_Click" CommandArgument='<%# Eval("ID") %>' meta:resourcekey="RollbackLink" ID="btnRollback"></asp:LinkButton>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
        </Columns>
        <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>" />
        <SettingsPager PageSize="10" />
    </dx:ASPxGridView>
    <br />
    <br />
    <h2>New Bulk Import</h2>
    <asp:Panel runat="server" ID="pnlNewImport" SkinID="GreyBorder720" Style="padding: 5px 0px;">
        <table style="width: 100%;">
            <tr>
                <td style="width: 65%;">
                    <Upload:InputFile ID="inputFileID" ClientIDMode="Static" runat="server"
                        meta:resourcekey="inputFileID" style="width: 500px;" />
                    <asp:CustomValidator ID="cusValidatorFile" ControlToValidate="inputFileID" runat="server"
                        ClientValidationFunction="ValidateUploadFile"
                        OnServerValidate="cusValidatorFile_ServerValidate" Display="Dynamic"
                        ErrorMessage="Please select an Excel file to upload." ValidationGroup="UploadGroup"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="ReqValidatorFile" runat="server" ControlToValidate="inputFileID"
                        ErrorMessage="Please select a file" ValidationGroup="UploadGroup" Display="Dynamic">  </asp:RequiredFieldValidator>
                    <Upload:ProgressBar ID="progressBarId" runat="server" Inline="false" />

                    <br />
                    <br />

                    <asp:Label runat="server" ID="lblInfo" />

                    <br />
                    <br />

                    <table style="border-spacing: 0px;">
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnUpload" runat="server"
                                    ClientInstanceName="uploadButton"
                                    OnClick="btnSubmitExcelSheet_Click"
                                    ValidationGroup="UploadGroup"
                                    meta:resourcekey="ASPxButtonUpload">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <asp:Image ID="imgStep1_Green" runat="server" Visible="false" ImageUrl="~/App_Themes/DBIntranet2010/images/arrow-right-green.png" />
                                <asp:Image ID="imgStep1_Gray" runat="server" Visible="true" ImageUrl="~/App_Themes/DBIntranet2010/images/arrow-right-gray.png" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnValidate" runat="server"
                                    AutoPostBack="False" OnClick="btnValidate_Click"
                                    meta:resourcekey="ASPxButtonValidate">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <asp:Image ID="imgStep2_Green" runat="server" Visible="false" ImageUrl="~/App_Themes/DBIntranet2010/images/arrow-right-green.png" />
                                <asp:Image ID="imgStep2_Gray" runat="server" Visible="true" ImageUrl="~/App_Themes/DBIntranet2010/images/arrow-right-gray.png" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnImport" runat="server"
                                    AutoPostBack="False" OnClick="ASPxButtonImport_Click"
                                    meta:resourcekey="ASPxButtonImport">
                                    <ClientSideEvents Click="ImportClick" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;">

                    <ul>
                        <li>
                            <asp:LinkButton ID="btnDownloadTemplate" runat="server" OnClick="btnDownloadTemplate_Click" meta:resourcekey="DownloadTemplateLink"></asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnDownloadValidationRules" runat="server" OnClick="btnDownloadValidationRules_Click" meta:resourcekey="DownloadValidationRulesLink"></asp:LinkButton>

                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblErrorMsg" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblSuccessMsg" runat="server" Text="" ForeColor="Green" Font-Size="Small"></asp:Label>

                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
    <asp:Panel runat="server" ID="pnlValidationSummary">

        <h3>Validation Summary </h3>
        [<asp:LinkButton ID="btnValidationSummary" runat="server" OnClick="btnValidationSummary_Click" meta:resourcekey="DownloadValidationSummaryLink"></asp:LinkButton>]
        <br />
        <br />

        <dx:ASPxTreeList ID="treeList" runat="server" SettingsBehavior-AutoExpandAllNodes="true">
            <Columns>
                <dx:TreeListTextColumn FieldName="Name">
                    <PropertiesTextEdit EncodeHtml="false" />
                </dx:TreeListTextColumn>
            </Columns>
            <Settings ShowColumnHeaders="False" />
            <ClientSideEvents NodeCollapsing="nodeExpandCollapse" />
            <ClientSideEvents NodeExpanding="nodeExpandCollapse" />
            <ClientSideEvents NodeDblClick="nodeExpandCollapse" />
            <Images ExpandedButton-Width="0" />
            <Styles>
                <Cell>
                    <Paddings PaddingLeft="1px" />
                </Cell>
            </Styles>
            <Templates>
                <DataCell>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl='<%# GetIconUrl(Container) %>'
                                    Width="16" Height="16" />
                            </td>
                            <td>&nbsp;
                            </td>
                            <td style="padding-bottom: 1px;">
                                <%# Container.Text %>
                            </td>
                        </tr>
                    </table>
                </DataCell>
            </Templates>
            <Border BorderWidth="0" />
            <SettingsBehavior ExpandCollapseAction="NodeDblClick" />
        </dx:ASPxTreeList>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlImportSummary">
        <h3>Import Summary </h3>
        <asp:Repeater runat="server" ID="rptImportSummary">
            <HeaderTemplate>
                <table>
                    <tr>
                        <th style="text-align: left;">Entity
                        </th>
                        <th>Record Count
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("SheetName")%> </td>
                    <td style="text-align: center;"><%# Eval("RecordCount") %> </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>    
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <dx:ASPxPopupControl ID="popupInfo" runat="server" SkinID="PopupCustomSize" HeaderText="Information" AllowDragging="true"
        Width="400" Height="200">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:Literal runat="server" ID="litPopupInfo" Text="Error Test" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="PopupPostSubmit" ClientInstanceName="popupPostSubmit" runat="server" SkinID="PopupCustomSize"
        HeaderText="" Height="180px" Width="350px" ShowHeader="False" meta:resourcekey="PopupPostSubmitResource1">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
                SupportsDisabledAttribute="True">
                <div style="text-align: center">
                    <div style="padding-top: 20px">
                        <h5>
                            <asp:Literal ID="RequestSubmittedMessage1" runat="server" Text="In progress..."></asp:Literal>
                        </h5>
                    </div>
                    <br />
                    <img src="../../App_Themes/DBIntranet2010/images/indicator.gif" alt="In progress" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
