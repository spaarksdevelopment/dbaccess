﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

using DevExpress.Web.ASPxGridView;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin
{
	public partial class ManageSmartCardVendors : BaseSecurityPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//if (!IsPostBack)
			//{
				GetSmartCardVendors();
			//}
		}

		private void GetSmartCardVendors()
		{
			try
			{
                List<GetSmartcardProviders_Result> smartcardProviders = new List<GetSmartcardProviders_Result>();
                bool showDisabled = ckbEnabled.Checked;
                smartcardProviders = Director.GetSmartCardProviders(CurrentUser.LanguageId,showDisabled);
				//if (smartcardProviders.Count > 1)
				//{
					smartcardProviders.RemoveAt(0);
					grdViewSmartCardVendors.DataSource = smartcardProviders;
					grdViewSmartCardVendors.DataBind();
				//}
			}
			catch (Exception ex)
			{
				int LoggedExceptionId = LogHelper.HandleException(ex);
			}
		}

		protected void grdViewSmartCardVendors_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			GetSmartCardVendors();
		}

		[WebMethod, ScriptMethod]
		public static void DeleteSmartcardProvider(int vendorID)
		{
			int nrval = 0;
			try
			{
                DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                Director director = new Director(CurrentUser.dbPeopleID);

				if (vendorID > 0)
                    nrval = director.DeleteSmartCardProvider(vendorID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}
	}
}