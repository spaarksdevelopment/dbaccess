﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="true" CodeBehind="AdminReport.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Admin.AdminReport"
	EnableViewState="false" uiculture="auto" %>


<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">

	    function betweenInputToggleName() {
	        var value = PersonDetailsNameSelector.GetValue();


	        if (value == "Between") {
	            PersonDetailsNameBetweenText.SetVisible(true);
	        }
	        else {
	            PersonDetailsNameBetweenText.SetVisible(false);
	        }
	    }

	    function betweenInputToggleEmployeeId() {
	        var value = PersonDetailsEmployeeIdSelector.GetValue();


	        if (value == "Between") {
	            PersonDetailsEmployeeIdBetweenText.SetVisible(true);
	        }
	        else {
	            PersonDetailsEmployeeIdBetweenText.SetVisible(false);
	        }
	    }

	    function betweenInputToggleEmail() {
	        var value = PersonDetailsEmailSelector.GetValue();


	        if (value == "Between") {
	            PersonDetailsEmailBetweenText.SetVisible(true);
	        }
	        else {
	            PersonDetailsEmailBetweenText.SetVisible(false);
	        }
	    }

	    function betweenInputToggleDbDir() {	       
	        var value = PersonDetailsDbDirSelector.GetValue();
	        

	        if (value == "Between") {
	            PersonDetailsDbDirBetweenText.SetVisible(true);
	        }
	        else {
	            PersonDetailsDbDirBetweenText.SetVisible(false);
	        }	       
	    }

	   
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">

    
    <h1>Admin Report</h1>

	 <asp:Panel runat="server" ID="searchFilters">    

         <table>
             <tr>
                 <td valign="top">
<p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Task Created</p>
                    <table>            
                        <tr>
                            <td><strong>Date &amp; Time</strong></td>
                            <td>Start: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_taskDateStartDate" 
					            ClientInstanceName="TaskDateStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                   
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>End: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_taskDateEndDate" 
					            ClientInstanceName="TaskDateEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                    
                            </td>
                        </tr>
                    </table>  

                     <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Task Completed</p>
                    <table>            
                        <tr>
                            <td><strong>Date &amp; Time</strong></td>
                            <td>Start: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_taskDateCompletedStartDate" 
					            ClientInstanceName="TaskDateCompletedStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                   
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>End: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_taskDateCompletedEndDate" 
					            ClientInstanceName="TaskDateCompletedEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                    
                            </td>
                        </tr>
                    </table>

                    <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Revoke Access Date</p>
                    <table>            
                        <tr>
                            <td><strong>Date &amp; Time</strong></td>
                            <td>Start: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_revokeAccessStartDate" enabled="false"
					            ClientInstanceName="RevokeAccessDateCompletedStartDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                   
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>End: </td>
                            <td>
                                <dx:ASPxDateEdit ID="m_revokeAccessEndDate" enabled="false"
					            ClientInstanceName="RevokeAccessDateCompletedEndDate" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						            EditFormatString="dd-MMM-yyyy"
						            DisplayFormatString="dd-MMM-yyyy" Font-Size="Larger">				
					            </dx:ASPxDateEdit>                    
                            </td>
                        </tr>
                        <tr>
                            <td><p style="color: red;font-weight: bold;"><asp:Literal ID="m_dateErrorMessage" runat="server" /></p></td>
                        </tr>
                    </table> 
                                                     
                 </td>
                 <td style="width: 50px;">&nbsp;</td>
                 <td valign="top">
                     <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Person Details</p>
                    <table>            
                        <tr>
                            <td align="right" width="100px"><strong>Name</strong></td>
                            <td><dx:ASPxComboBox ID="m_personDetailsNameSelector" ClientInstanceName="PersonDetailsNameSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                    <Items>
                                        <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                        <dx:ListEditItem Value="Begins" Text="Begins With" />
                                        <dx:ListEditItem Value="Equals" Text="Equals" />
                                        <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                        <dx:ListEditItem Value="Between" Text="Between" />
                                    </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleName(); }" />
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsNameText" ClientInstanceName="PersonDetailsNameText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsNameBetweenText" ClientInstanceName="PersonDetailsNameBetweenText" runat="server" Width="150px" ClientVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="100px"><strong>Employee ID</strong></td>
                            <td><dx:ASPxComboBox ID="m_personDetailsEmployeeIdSelector" ClientInstanceName="PersonDetailsEmployeeIdSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                    <Items>
                                        <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                        <dx:ListEditItem Value="Begins" Text="Begins With" />
                                        <dx:ListEditItem Value="Equals" Text="Equals" />
                                        <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                        <dx:ListEditItem Value="Between" Text="Between" />
                                    </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmployeeId(); }" />
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsEmployeeIdText" ClientInstanceName="PersonDetailsEmployeeIdText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsEmployeeIdBetweenText" ClientInstanceName="PersonDetailsEmployeeIdBetweenText" runat="server" Width="150px" ClientVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="100px"><strong>Email</strong></td>
                            <td><dx:ASPxComboBox ID="m_personDetailsEmailSelector" ClientInstanceName="PersonDetailsEmailSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                    <Items>
                                        <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                        <dx:ListEditItem Value="Begins" Text="Begins With" />
                                        <dx:ListEditItem Value="Equals" Text="Equals" />
                                        <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                        <dx:ListEditItem Value="Between" Text="Between" />
                                    </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleEmail(); }" />
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsEmailText" ClientInstanceName="PersonDetailsEmailText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsEmailBetweenText" ClientInstanceName="PersonDetailsEmailBetweenText" runat="server" Width="150px" ClientVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="100px"><strong>dbDir</strong></td>
                            <td><dx:ASPxComboBox ID="m_personDetailsDbDirSelector" ClientInstanceName="PersonDetailsDbDirSelector" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px">
                                    <Items>
                                        <dx:ListEditItem Value="Contains" Text="Contains" Selected="true" />
                                        <dx:ListEditItem Value="Begins" Text="Begins With" />
                                        <dx:ListEditItem Value="Equals" Text="Equals" />
                                        <dx:ListEditItem Value="NotEqual" Text="Does Not Equal" />
                                        <dx:ListEditItem Value="Between" Text="Between" />
                                    </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s,e){ betweenInputToggleDbDir(); }" />
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsDbDirText" ClientInstanceName="PersonDetailsDbDirText" runat="server" Width="150px"></dx:ASPxTextBox>                   
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="m_personDetailsDbDirBetweenText" ClientInstanceName="PersonDetailsDbDirBetweenText" runat="server" Width="150px" ClientVisible="false" />
                            </td>
                        </tr>
                    </table>
                 </td>
                 
                 <td style="width: 50px;">&nbsp;</td>
                 <td valign="top">
                     <p style="margin-left: 0; padding-left: 0;text-decoration: underline;">Pass Office Queue</p>
                    <table>            
                        <tr>
                            <td align="right" width="100px"><strong>Pass Office:</strong></td>
                            <td>
                                <dx:ASPxListBox ID="m_passOfficeList" ClientInstanceName="PassOfficeList" runat="server" SelectionMode="Multiple"
			                        Width="300px"
			                        Height="200px"
                                    TextField="DisplayName"
                                    ValueField="PassOfficeID"
			                        ItemStyle-Cursor="pointer">
		                        </dx:ASPxListBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="100px"><strong>Status:</strong></td>
                            <td>
                                <dx:ASPxComboBox ID="m_passOfficeStatus" ClientInstanceName="PassOfficeStatus" runat="server" TextField="Status" ValueField="TaskStatusId" AutoPostBack="false" Width="100px"></dx:ASPxComboBox>         
                            </td>
                        </tr>
                    </table>

                 </td>
             </tr>
         </table>
    <br />

        <div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_searchSubmit" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Search%>"
			        Width="100px" OnClick="m_searchSubmit_click" >
		        </dx:ASPxButton>
	        </div>
	        <div style="float: left; padding: 5px">
		        <dx:ASPxButton ID="m_seearchReset" runat="server" AutoPostBack="false" Text="<%$ Resources:CommonResource, Reset%>"
			        Width="100px" OnClick="m_seearchReset_click">
		        </dx:ASPxButton>
	        </div>
        </div>

         <br />
         <br />
         <br />
    </asp:Panel>


    <asp:PlaceHolder ID="m_searchOutputHolder" runat="server" Visible="false">
 <div id="divRequestPersons">
<p>Click here to export this data to Excel. <asp:ImageButton ID="ASPxButton2" runat="server" OnClick="btnXlsxExport_Click" SkinID="FileXLS"/> </p>
		
        <dx:ASPxGridView ID="SearchResults" ClientInstanceName="SearchResults"
            runat="server" AutoGenerateColumns="False"
            KeyFieldName="TaskId" 
            ClientIDMode="AutoID" EnableViewState = "false" Settings-ShowFilterRowMenu="true" Settings-ShowFilterRow="true" >
			<SettingsLoadingPanel ShowImage="true" />
            <Columns>
    
                <dx:GridViewDataDateColumn Caption="Revoke Access Date" FieldName="RevokeAccessDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="1">
					<Settings AllowHeaderFilter="True"/>

					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataDateColumn Caption="Revoke Access Time" FieldName="RevokeAccessDate" PropertiesDateEdit-DisplayFormatString="HH:mmm:ss" VisibleIndex="2">
					<Settings AllowHeaderFilter="True" AllowSort="False" AllowGroup="False" AllowAutoFilter="False" AllowDragDrop="False"/>
                </dx:GridViewDataDateColumn>

				<dx:GridViewDataTextColumn Caption="Task ID" FieldName="TaskId" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="3">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

				<dx:GridViewDataDateColumn Caption="Task Created Date" FieldName="CreatedDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="4">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataDateColumn Caption="Task Created Time" FieldName="CreatedDate" PropertiesDateEdit-DisplayFormatString="HH:mmm:ss" VisibleIndex="5">
					<Settings AllowHeaderFilter="True" AllowSort="False" AllowGroup="False" AllowAutoFilter="False" AllowDragDrop="False"/>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Person name" FieldName="PersonName" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="6">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person Employee ID" FieldName="EmployeeId" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="7">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person email" FieldName="EmailAddress" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="8">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Person dbDir" FieldName="DBDirId" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="9">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Task Type" FieldName="RequestType" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="10">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataDateColumn Caption="Task closed Date" FieldName="CompletedDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="11">
					<Settings AllowHeaderFilter="True"/>
					<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataDateColumn Caption="Task closed Time" FieldName="CompletedDate" PropertiesDateEdit-DisplayFormatString="HH:mmm:ss" VisibleIndex="12">
					<Settings AllowHeaderFilter="True" AllowSort="False" AllowGroup="False" AllowAutoFilter="False" AllowDragDrop="False"/>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Pass Office" FieldName="PassOfficeName" ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="13">
					<Settings AllowHeaderFilter="True"/>
				</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Status" FieldName="RequestName"  ShowInCustomizationForm="True" ReadOnly="true" VisibleIndex="14">
					<Settings AllowHeaderFilter="True"/>                    
				</dx:GridViewDataTextColumn>
				
				</Columns>
            <SettingsText EmptyDataRow="No results could be found for your criteria" />
				<SettingsPager PageSize="25"   />
        </dx:ASPxGridView>

    </div>

<dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="SearchResults"></dx:ASPxGridViewExporter>

        </asp:PlaceHolder>

</asp:Content>