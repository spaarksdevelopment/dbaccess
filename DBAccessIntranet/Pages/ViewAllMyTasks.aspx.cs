﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class ViewAllMyTasks : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadMyTasks();
        }

        protected void LoadMyTasks()
        {
            int ncurrentUserId = base.CurrentUser.dbPeopleID;
            List<DBAccessController.DAL.ViewAllTasks> lstMyTasks = Director.GetViewAllMyTasks(ncurrentUserId);


            ASPxGridViewTasks.DataSource = lstMyTasks;
            ASPxGridViewTasks.DataBind();
        }

        #region
        /// <summary>
        /// Method to export the po grid data to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnXlsxExportPO_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }
        #endregion
    }
}