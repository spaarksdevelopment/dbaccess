﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Home"  uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
    <h1>dbAccess</h1>
    <p><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:CommonResource, HomeContent%>" /></p>
	<h2 style="color:#80A9DB;padding:0 0 0 7px; font-weight:bold"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:CommonResource, HomeContentHeading%>" /></h2>
    <asp:TextBox ID="txtUserDetails"  runat="server" Text="Details Box:" ForeColor="#0018A8" Font-Size="1.5em" EnableViewState="true" TextMode="MultiLine" ReadOnly="True" style="background:#EAF0F4;width:600px;height:600px;overflow:auto;border:1px solid #B1B6B9;"></asp:TextBox>
						
    <br />
</asp:Content>
