﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" MasterPageFile="~/MasterPages/DBIntranet.master" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.Error" %>

<asp:Content ContentPlaceHolderID="DBMainContent" runat="server">
    <h1>Error</h1>

    <asp:Panel ID="PanelKnownError" runat="server" Visible="false">
        <p>An error has occurred.</p>
        <br />
        <asp:Label ID="LabelErrorText" runat="server" Text="" Font-Bold="true" Font-Size="14px"></asp:Label>
        <br />
    </asp:Panel>
    <asp:Panel ID="PanelUnknownError" runat="server">
        <p>An error has occurred in site.  We apologise for any inconvenience this may have caused.</p>
        <p>The details of this error have been sent to the technical support team for investigation.</p> 
        <p>The Error ID is <b><asp:Label runat="server" ID="LabelEventID" /></b></p>
    </asp:Panel>

    <asp:Label ID="LabelThanks" runat="server" Visible="false" Text="Thank you for your comments." Font-Size="Larger" ForeColor="red"></asp:Label>
    <asp:Label ID="LabelError" runat="server" Visible="false" Text="Thank you for your comments - unfortunately they could not be saved due to a system fault." Font-Size="Larger" ForeColor="red"></asp:Label>
    <asp:Panel ID="PanelComments" Visible="true" runat="server">
    <p>If you would like to add any additional comments which will help the team diagnose the problem, please do so below.</p>
         <p>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxComments" ErrorMessage="Please enter your comments before sending"></asp:RequiredFieldValidator></p>
         

    <asp:TextBox runat="server" ID="TextBoxComments" TextMode="MultiLine" Rows="6" Width="600px">
    </asp:TextBox>
        <br />
        <asp:Label ID="LabelUser" runat="server" Text="User:" Width="40px"></asp:Label>    
        <asp:TextBox ID="TextBoxUserName" runat="server" ReadOnly="True" Width="160px"></asp:TextBox>
        <asp:TextBox ID="TextBoxEmail" runat="server" ReadOnly="True" Width="220px"></asp:TextBox>
        <asp:TextBox ID="TextBoxUserType" runat="server" ReadOnly="True" Width="160px"></asp:TextBox>
        <br />

        &nbsp; &nbsp;
    <asp:Button ID="ButtonSubmit" runat="server" Text="Send comments" OnClick="ButtonSubmit_Click" />
    </asp:Panel>
</asp:Content>