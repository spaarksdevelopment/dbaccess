﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Services;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxGridView;
using System.Resources;
using Spaarks.Common.UtilityManager;
using System.Reflection;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class ViewAllRequests : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			if (!IsPostBack && !IsCallback)
			{
				SetDateTime();
			}
				LoadRequests();
				ApplyFilter();

	    }

        public string GetResourceManager(string resourseName)
        {
            try
            {
                return HttpContext.GetLocalResourceObject("~//Pages/ViewAllRequests.aspx", resourseName).ToString();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		private void SetDateTime()
		{
			DateTime dt = DateTime.Today;
			AspxStartDateFilter.Date = dt = dt - TimeSpan.FromDays(30);
			ASPxEndDateFilter.Date = DateTime.MinValue;
		}

		protected void requestsTabPage_ActiveTabChanging(object sender, EventArgs e)
		{
			ResetControls();
		}

		public void ApplyFilter()
		{
			string searchBox = SearchBox.Text;
			if (requestsTabPage.ActiveTabIndex == 0)
			{
				if (searchBox.Length == 0)
				{
					if (ASPxGridViewAllRequests.FilterExpression.Length > 0)
						ASPxGridViewAllRequests.FilterExpression = string.Empty;
					return;
				}
				switch (cbSearchType.SelectedItem.Value.ToString())
				{
					case null:
						break;
					case "RequestID":
						ASPxGridViewAllRequests.FilterExpression = "[RequestID] = " + searchBox;
						break;
					case "Applicant":
						ASPxGridViewAllRequests.FilterExpression = "Contains([Applicant],'" + searchBox + "')";
						break;
					case "Requestor":
						ASPxGridViewAllRequests.FilterExpression = "Contains([RequestorName],'" + searchBox + "')";
						break;
					case "Building":
						ASPxGridViewAllRequests.FilterExpression = "Contains([Building],'" + searchBox + "')";
						break;
					case "Area":
						ASPxGridViewAllRequests.FilterExpression = "Contains([Access_Area],'" + searchBox + "')";
						break;
					case "Task":
						ASPxGridViewAllRequests.FilterExpression = "Contains([Task],'" + searchBox + "')";
						break;
				}
			}
			else
			{
				if (searchBox.Length == 0)
				{
					if (ASPxGridView1.FilterExpression.Length > 0)
						ASPxGridView1.FilterExpression = string.Empty;
					return;
				}
				switch (cbSearchType.SelectedItem.Value.ToString())
				{
					case null:
						break;
					case "RequestID":

						ASPxGridView1.FilterExpression = "[RequestID] = " + searchBox;
						break;
					case "Applicant":
						ASPxGridView1.FilterExpression = "Contains([Applicant],'" + searchBox + "')";
						break;
					case "Requestor":
						ASPxGridView1.FilterExpression = "Contains([RequestorName],'" + searchBox + "')";
						break;
					case "Building":
						ASPxGridView1.FilterExpression = "Contains([Building],'" + searchBox + "')";
						break;
					case "Area":
						ASPxGridView1.FilterExpression = "Contains([Access_Area],'" + searchBox + "')";
						break;
					case "Task":
						ASPxGridView1.FilterExpression = "Contains([Task],'" + searchBox + "')";
						break;
				}
			}			
		}
		
        public bool IsSwissPassOfficeUser
        {

            get
            {
                bool toReturn = false;

                //  Get the pass office for the user
                List<LocationPassOffice> usersPassOffices = Director.GetUserPassOffice(base.CurrentUser.dbPeopleID);

                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];

                List<LocationPassOffice> countryPassOffice = Director.GetPassOfficesByCountryId(Int32.Parse(switzerlandStringId));

                //  does it only contain 1 and is it switzerland?
                if (usersPassOffices != null && countryPassOffice != null && usersPassOffices.Count == 1 && countryPassOffice.Count == 1 && countryPassOffice.First().PassOfficeID == usersPassOffices.First().PassOfficeID)
                {
                    toReturn = true;
                }


                return toReturn;
            }

        }

        protected void LoadRequests()
        {
            int ncurrentUserId = base.CurrentUser.dbPeopleID;
			DateTime StartDate = AspxStartDateFilter.Date;
			DateTime EndDate = (DateTime.MinValue == ASPxEndDateFilter.Date) ? DateTime.MinValue : ASPxEndDateFilter.Date;
            List<string> accessAreas = new List<string>();

            if (base.IsRestrictedUser)
            {
                List<DBAccessController.DAL.vw_AccessArea> accessAreasObjectList = Director.GetAccessAreasForCountry(base.RestrictedCountryId);

                if (accessAreasObjectList != null)
                {
                    foreach (DBAccessController.DAL.vw_AccessArea item in accessAreasObjectList)
                    {
                        accessAreas.Add(item.AccessAreaName);
                    }
                }
            }
            else if(IsSwissPassOfficeUser)
            {
                List<DBAccessController.DAL.vw_AccessArea> accessAreasObjectList = Director.GetAccessAreasForCountry(Int32.Parse(ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"]));

                if (accessAreasObjectList != null)
                {
                    foreach (DBAccessController.DAL.vw_AccessArea item in accessAreasObjectList)
                    {
                        accessAreas.Add(item.AccessAreaName);
                    }
                }
            }
           
            List<DBAccessController.DAL.GetViewAllRequests> lstAllRequests = Director.GetViewAllRequests(ncurrentUserId, StartDate, EndDate);

            if (base.IsRestrictedUser || IsSwissPassOfficeUser)
            {
                lstAllRequests = lstAllRequests.Where(e => accessAreas.Contains(e.Access_Area)).ToList();
            }

            ASPxGridViewAllRequests.DataSource = lstAllRequests;
            ASPxGridViewAllRequests.DataBind();
           
         
            List<DBAccessController.DAL.GetViewAllAdminRequests> lstAllAdminRequests = Director.GetViewAllAdminRequests(ncurrentUserId, StartDate, EndDate);

            if (base.IsRestrictedUser || IsSwissPassOfficeUser)
            {
                lstAllAdminRequests = lstAllAdminRequests.Where(e => accessAreas.Contains(e.Access_Area)).ToList();
            }

            ASPxGridView1.DataSource = lstAllAdminRequests;
            ASPxGridView1.DataBind();
        

            ASPxGridViewAllRequests.SettingsText.GroupPanel = GetResourceManager("DragText");
            ASPxGridView1.SettingsText.GroupPanel = GetResourceManager("DragText");

        }

       

        protected void Clear_Clicked(object sender, EventArgs e)
        {
			ResetControls();            
        }

		private void ResetControls()
		{
			string searchBox = SearchBox.Text;
			if (requestsTabPage.ActiveTabIndex == 0)
			{
				ASPxGridViewAllRequests.FilterExpression = string.Empty;
			}
			else
			{
				ASPxGridView1.FilterExpression = string.Empty;
			}

			SearchBox.Text = string.Empty;
			SetDateTime();
			LoadRequests();
		}

        protected void btnAllRequests_Click(object sender, EventArgs e)
        {
            ASPxGridViewAllRequests.Columns[11].Visible = true;
            ASPxGridViewAllRequests.Columns[12].Visible = true;
            gridExport.WriteXlsxToResponse();
            ASPxGridViewAllRequests.Columns[11].Visible = false;
            ASPxGridViewAllRequests.Columns[12].Visible = false;
        }

        protected void btnAllAdminRequests_Click(object sender, EventArgs e)
        {
            ASPxGridView1.Columns[11].Visible = true;
            ASPxGridView1.Columns[12].Visible = true;
            ASPxGridViewExporter1.WriteXlsxToResponse();
            ASPxGridView1.Columns[11].Visible = false;
            ASPxGridView1.Columns[12].Visible = false;
        }


		protected void ASPxGridViewAllRequests_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6'; this.style.cursor = 'hand';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}

		protected void ASPxGridView1_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6'; this.style.cursor = 'hand';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}

        

		[WebMethod, ScriptMethod]
		public static string GetRequestDetails(int requestID, string type)
		{
			//no base class available when using a static method, so need to get the user
			DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			int ncurrentUserId = CurrentUser.dbPeopleID;
			string requestDetailXml = Director.GetRequestDetails(ncurrentUserId, requestID, type);

			if (requestDetailXml == string.Empty) return string.Empty;

			XPathDocument xmlDoc = new XPathDocument(new StringReader(requestDetailXml));

			// See what type of task we have here
			//XPathNavigator nav = xmlDoc.CreateNavigator();
			//int taskType = Convert.ToInt32(nav.SelectSingleNode("/ROOT/row/TaskTypeId").Value);

			//XsltArgumentList argsList = new XsltArgumentList();
			//argsList.AddParam("TaskTypeId", "", taskType);

			XmlWriterSettings writerSettings = new XmlWriterSettings
			{
				OmitXmlDeclaration = true,
				ConformanceLevel = ConformanceLevel.Fragment,
				CloseOutput = false
			};

			MemoryStream ms = new MemoryStream();
			XmlWriter writer = XmlWriter.Create(ms, writerSettings);

			XslCompiledTransform transform = new XslCompiledTransform();

			XsltSettings settings = new XsltSettings();
			settings.EnableScript = true;

			string xslPath = ConfigurationManager.AppSettings["RequestDetailsXslPathRoot"].ToString();
            
            //  Need to check the language here to see if we are german or italian - need to use the language id as this is a different thread to website so culture hasnt copied over
            if (CurrentUser.LanguageId == (int)DBAccessEnums.Language.German)
            {
                xslPath = xslPath.Replace(".xsl", "_de.xsl");
            }
            else if (CurrentUser.LanguageId == (int)DBAccessEnums.Language.Italian)
            {
                xslPath = xslPath.Replace(".xsl", "_it.xsl");
            }
            

			xslPath = HttpContext.Current.Server.MapPath(xslPath);

			transform.Load(xslPath, settings, null);
			transform.Transform(xmlDoc, null, writer);
			//transform.Transform(xmlDoc, argsList, writer);

			ms.Position = 0;
			StreamReader r = new StreamReader(ms);
			return r.ReadToEnd();
		}
    }
}