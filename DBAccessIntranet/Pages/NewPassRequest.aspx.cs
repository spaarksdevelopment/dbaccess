﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHiddenField;
using Excel;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class NewPassRequest : BaseSecurityPage
    {
        private int _RequestMasterID;
		
        #region Page Events
                
        protected void Page_Load(object sender, EventArgs e)
        {
            //page load will run on DevExpress callbacks too
            if(!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                _RequestMasterID = Helper.SafeInt(Request.QueryString["id"]);
            }

            //language postback causing problems
            string language = Request.Form["__EventTarget"];
            bool doReloadForLanguageChange = false;

            if (language == Convert.ToString(DBAccessEnums.Language.English) || language == Convert.ToString(DBAccessEnums.Language.German) || language == Convert.ToString(DBAccessEnums.Language.Italian))
            {
                doReloadForLanguageChange = true;
            }
            
            if (!IsPostBack)
            {
                ASPxHiddenFieldRequestDetail.Add("RequestMasterID", _RequestMasterID);

                //DBAppUser user = base.CurrentUser;
                DBAppUser user = base.CurrentUser;
                TextBoxEmail.Text = user.Email;
                TextBoxTelephone.Text = user.Telephone;

                var po = Director.GetUserPassOffice(user.dbPeopleID);
                if (po.Count > 0)
                    PassOfficeSelector1.PassOfficeID = po.First().PassOfficeID;
            }
            else
            {
                if (doReloadForLanguageChange)
                 {
                    // SetJustificationVisible(BindGrid());
                     base.RegisterStartupScript("toggleJustification", "toggleJustification();");
                 }
            }

            BindGrid();

			TextBoxEmail.ClientInstanceName = TextBoxEmail.ClientID;
			TextBoxTelephone.ClientInstanceName = TextBoxTelephone.ClientID;
            ASPxButtonSubmit.JSProperties["cpEditorClientName"] = TextBoxTelephone.ClientInstanceName;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
                        
            //add necessary scripts
            AddHeaderScriptInclude("~/scripts/ValidationService.js");

            //if there is no request master, then hide the grid, and hide the collection details fields regardless
            if (_RequestMasterID == 0)
                RegisterStartupScript("toggleGrid", "toggleGrid(); toggleCollectionDetails(-1);");
            else
                RegisterStartupScript("toggleGrid", "toggleCollectionDetails(-1);");
        }
              
        #endregion

        #region WebMethods
        [WebMethod, ScriptMethod]
        public static bool DeletePerson(int requestPersonID)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            bool success = director.DeleteAccessRequest(requestPersonID);

            return success;
        }

        [WebMethod, ScriptMethod]
        public static int AddPersonToRequest(int personID, int requestMasterID, bool hasBadge)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
			
            List<int> badgeList = null;
			if (hasBadge)
			{
				//not passing in a list badges, so get the badge ID for replacement
				var personBadges = Director.GetPersonBadges(personID, true);

				badgeList = new List<int>();
				if (personBadges != null && personBadges.Count > 0)
					badgeList.Add(personBadges[0].PersonBadgeID);
				}

				//Now create the badge request
			IBadgeRequestService badgeRequestService = new BadgeRequestService(currentUser.dbPeopleID);

            bool success = badgeRequestService.CreateNewBadgeRequestForPerson(personID, badgeList, ref requestMasterID);

			if (success)
				return requestMasterID;

            if (badgeRequestService.FailureReason == FailureReason.PersonAlreadyOnRequest)
				return -1;

		    return 0;
		}

        [WebMethod, ScriptMethod]
        public static int AddPersonWithBadgesToRequest(int personID, int requestMasterID, object[] badgesToReplace)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            List<int> badgeList = new List<int>();

            GetListOfBadgeIDsFromArray(badgesToReplace, badgeList);

            IBadgeRequestService badgeRequestService = new BadgeRequestService(currentUser.dbPeopleID);

            bool success = badgeRequestService.CreateNewBadgeRequestForPerson(personID, badgeList, ref requestMasterID);
            
			if (success)
				return requestMasterID;

            if (badgeRequestService.FailureReason == FailureReason.PersonAlreadyOnRequest)
				return -2;

		    return 0;
		}

        [WebMethod, ScriptMethod]
        public static bool SubmitRequest(int requestMasterID, int decision, string additionalInfo, string commentNote, bool isSmartCard, string smartCardJustificationReason, int? justificationID)
        {
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            if (!Director.CheckRequestMasterForSubmission(requestMasterID))
            {
                //decision is the selected index, 0-1, delivery option id is 1-2, so just add 1
                DeliveryOption deliveryOption = (DeliveryOption)(decision + 1);

                IBadgeRequestService badgeRequestService = new BadgeRequestService(currentUser.dbPeopleID);                              

                bool success = badgeRequestService.SubmitBadgeRequest(requestMasterID, deliveryOption, additionalInfo,
                                                                    commentNote, currentUser.Forename, currentUser.Email, isSmartCard, smartCardJustificationReason);

                if (!justificationID.HasValue) return success;

                List<GetBadgeJustificationReasons_Result> reasons = badgeRequestService.GetBadgeJustificationReasons(null);

                if (reasons.Any(item => item.ID == justificationID))
                    success = badgeRequestService.UpdateBadgeJustification(requestMasterID, justificationID);
                
                return success;
            }
         
            LogHelper.LogException(ExceptionSeverity.Low, currentUser.UserName, currentUser.RolesNamesList, "Duplicate try for New Pass Request - Request Master ID: " + requestMasterID);
            return false;
        }

        #endregion

        #region Event Handlers

        protected void ASPxButtonNewAccess_click(object sender, EventArgs e)
        {
            _RequestMasterID = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            bool success = false;
            int? newRequestMasterID = null;

            if (_RequestMasterID > 0)
            {
                Director director = new Director(CurrentUser.dbPeopleID);
                
                newRequestMasterID = director.CreateNewRequestForTransferredPersons(_RequestMasterID);

                success = newRequestMasterID.HasValue;
            }

            if (success)
            {
                Response.Redirect("AccessRequest.aspx?id=" + newRequestMasterID.Value);
            }
        }

		protected void ASPxCallbackPanelBadgeRequests_Callback(object sender, CallbackEventArgsBase e)
		{
			List<vw_BadgeRequest> badgeRequests = BindGrid();

			SetJustificationVisible(badgeRequests);
		}

        protected void ASPxGridViewBadgeRequests_OnHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;

            Image imageIssueType = ASPxGridViewBadgeRequests.FindRowCellTemplateControl(e.VisibleIndex, null, "ImageIssueType") as Image;

            string issueType = e.GetValue("Issue").ToString();
            string imagename = "new.png";
            string imageResource = "newKeyImageResource1AlternateText";

            if (issueType == "R")
            {
                imagename = "renew.png";
                imageResource = "renewKeyImageResource1AlternateText";
            }

            imageIssueType.ImageUrl = base.PageThemeImagesFolder + imagename;

           
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            imageIssueType.ToolTip = resourceManager.GetString(imageResource);
        }

        protected void ASPxListBoxBadges_Callback(object sender, CallbackEventArgsBase e)
        {
            ObjectDataSourceBadges.SelectParameters["dbPeopleID"].DefaultValue = e.Parameter;
            ASPxListBoxBadges.DataSource = ObjectDataSourceBadges;
            ASPxListBoxBadges.DataBind();
        }

        protected void ASPxButtonCancel_click(object sender, EventArgs e)
        {            
            _RequestMasterID = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            bool success = false;

            if (_RequestMasterID > 0)
            {
                Director director = new Director(base.CurrentUser.dbPeopleID);
                success = director.CancelRequest(_RequestMasterID);
            }

            if (success)
            {
                ASPxHiddenFieldRequestDetail.Remove("RequestMasterID");
                ASPxHiddenFieldRequestDetail.Add("RequestMasterID", 0);
                RadioButtonListSelectCollectionMethod.SelectedIndex = -1;
                base.RegisterStartupScript("toggleGrid1", "toggleGrid(); toggleCollectionDetails(-1);");
            }
            else
            {
                base.RegisterStartupScript("toggleGrid1", "toggleGrid(); toggleCollectionDetails(" + RadioButtonListSelectCollectionMethod.SelectedIndex.ToString() + ");");
            }
        }
              
        protected void btnSubmitExcelSheet_Click(object sender, EventArgs e)
        {
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(inputFileId.FileContent);

            inputFileId.FileContent.Close();

            int counter = 0;
            int maxRows = Convert.ToInt32(ConfigurationManager.AppSettings["FileUploadMaxRowCount"].ToString());

            StringBuilder sb = new StringBuilder();

            while (excelReader.Read())
            {
                if (IsValidUserID(excelReader.GetValue(0).ToString()))
                {
                    counter++;

					string dbpValue = excelReader.GetValue(0).ToString().Replace("C", "");
					dbpValue = dbpValue.Replace("c", "");
					sb.Append(dbpValue);

                    sb.Append(",");
                }

                // Stop the upload at 500 rows.
                if (counter > maxRows) break;
            }
			
			//Check if the users exist in the dbdatabse if not display an erroe message
            if (Director.CheckIfTheUsersExist(Convert.ToString(sb)))
            {
                int numberNotProcessed = ProcessUploadedUserList(sb);

                if (numberNotProcessed > 0)
                {
                    PopupNewPassRequestError.ShowOnPageLoad = true;
                    textBoxPopupErrorMessage.Text = numberNotProcessed + GetLocalResource("NewPassRequestPendReq.Text");
                }
            }
            else
            {
                //display a message saying that the document has users that doesn't exist in db
                PopupNewPassRequestError.ShowOnPageLoad = true;
                textBoxPopupErrorMessage.Text = GetLocalResource("NewPassRequestNotExistBulk.Text");
            }
        }
		#endregion

		#region Private Methods
        private static void GetListOfBadgeIDsFromArray(object[] badgesToReplace, List<int> badgeList)
        {
            badgeList.AddRange(from badge in badgesToReplace select Helper.SafeIntNullable(badge) 
                                   into badgeId where badgeId.HasValue select badgeId.Value);
        }

		private List<vw_BadgeRequest> BindGrid()
		{
            int requestMasterID = GetIntFromASPXHiddenField(ASPxHiddenFieldRequestDetail, "RequestMasterID");

            IBadgeRequestService badgeRequestService = new BadgeRequestService(CurrentUser.dbPeopleID);
            List<vw_BadgeRequest> badgeRequests = badgeRequestService.GetBadgeRequests(requestMasterID);

			ASPxGridViewBadgeRequests.DataSource = badgeRequests;
			ASPxGridViewBadgeRequests.DataBind();

            SelectDefaultPassOffice(badgeRequests);

			return badgeRequests;
		}

        //TODO Promote this to generic method
        private int GetIntFromASPXHiddenField(ASPxHiddenField hiddenField, string identifier)
        {
            string value = hiddenField.Get(identifier).ToString();

            return Convert.ToInt32(value);
        }

        //TODO Possibly promote to generic method
		/// <summary>
        /// AC: test to see if we are getting valid ids in from the selected excel sheet.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool IsValidUserID(string dbPeopleIDString)
        {
            dbPeopleIDString = dbPeopleIDString.Replace("C", "");
            if (dbPeopleIDString.Length != 7) return false;

            double dbPeopleIDDouble;

            bool isNum = double.TryParse(dbPeopleIDString, out dbPeopleIDDouble);

		    return isNum;
        }

        private int ProcessUploadedUserList(StringBuilder users)
        {
            IBadgeRequestService badgeRequestService = new BadgeRequestService(CurrentUser.dbPeopleID);

            _RequestMasterID = Helper.SafeInt(ASPxHiddenFieldRequestDetail.Get("RequestMasterID").ToString());

            int numberNotProcessed;

            int newMasterId = badgeRequestService.ProcessUploadedUsers(users.ToString(), _RequestMasterID, (int)DBAccessEnums.RequestType.NewPass, out numberNotProcessed);
                        
            if (_RequestMasterID == 0)
            {
                ASPxHiddenFieldRequestDetail.Remove("RequestMasterID");
                ASPxHiddenFieldRequestDetail.Add("RequestMasterID", newMasterId);
            }

			List<vw_BadgeRequest> badgeRequests = BindGrid();

			SetJustificationVisible(badgeRequests);

			RegisterStartupScript("toggleJustification", "toggleJustification();");
            RegisterStartupScript("selectDefaultPassOffice", "SetDefaultPassOffice();");

            return numberNotProcessed;
        }

		private void SetJustificationVisible(List<vw_BadgeRequest> badgeRequests)
		{
			if (badgeRequests != null && badgeRequests.Count > 0 && badgeRequests.Count(a => a.Issue == "R") > 0)
				ASPxHiddenFieldShowJustification.Set("ShowJustification", true);
			else
				ASPxHiddenFieldShowJustification.Set("ShowJustification", false);
		}

        private string GetLocalResource(string text)
        {
            object localResources = GetLocalResourceObject(text);
            if (localResources == null)
                return string.Empty;

            return localResources.ToString();
        }

        private void SelectDefaultPassOffice(List<vw_BadgeRequest> badgeRequests)
        {
            IBadgeRequestService badgeRequestService = new BadgeRequestService(CurrentUser.dbPeopleID);

            int defaultPassOfficeId = badgeRequestService.GetDefaultPassOffice(badgeRequests);
            
            ASPxHiddenFieldDefaultPassOffice.Set("DefaultPassOffice", defaultPassOfficeId);
        }

		#endregion
	}
}
