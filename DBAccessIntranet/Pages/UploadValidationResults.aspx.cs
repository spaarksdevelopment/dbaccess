﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
	public partial class UploadValidationResults : BaseSecurityPage
	{
		#region events
		protected void Page_Load(object sender, EventArgs e)
        {
			if(!Page.IsPostBack)
				BindSmartcardNumberGrid();
        }

		protected void btnCancelUpload_Click(object sender, EventArgs e)
		{
			Server.Transfer("~/Pages/VendorFileManagement.aspx");
		}

		protected void btnLoadSmartcardNumbers_Click(object sender, EventArgs e)
		{
			string message = Director.ConfirmSmartcardUpload(CurrentUser.dbPeopleID);
			Response.Redirect("~/Pages/VendorFileManagement.aspx");
		}
		#endregion events
		
		#region helper methods
		protected void BindSmartcardNumberGrid()
		{
			List<vw_SmartcardNumberStaging> smartcardNumbers = Director.GetSmartcardNumbersStaging();

			bool emptyList = smartcardNumbers.Count==0;

			//If there is at least one fatal error - the smartcard or mifare has already been loaded
			//then hide the warnings and just show the fatal errors
			int existing = Director.GetExistingCount();
			int invalid = Director.GetInvalidCount();
			int duplicates = Director.GetDuplicatesCount();

			if (duplicates > 0 || invalid > 0)
				smartcardNumbers = smartcardNumbers.Where(a => a.UploadStatusID != (int)DBAccessEnums.SmartcardNumberUploadStatus.BadgeExists).ToList();
			
			ASPxGridViewSmartcardNumbers.DataSource = smartcardNumbers;
			ASPxGridViewSmartcardNumbers.DataBind();

			GetUploadMessage(emptyList, existing, invalid, duplicates);

			btnLoadSmartcardNumbers.Visible = invalid == 0 && duplicates == 0 && !emptyList;

			if (duplicates > 0 || invalid > 0 || emptyList)
				btnCancelUpload.Text = "Return";
		}

		public void GetUploadMessage(bool emptyList, int existing, int invalid, int duplicates)
		{
			string uploadMessage1 = string.Empty;
			string uploadMessage2 = string.Empty;
            
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

			if (emptyList)
			{
                uploadMessage1 = resourceManager.GetString("dbSmartcardError1");
			}
			else if (invalid > 0)
			{
                uploadMessage1 = resourceManager.GetString("dbSmartcardError2");
                uploadMessage2 = resourceManager.GetString("dbSmartcardError3"); 
			}
			else if (duplicates == 0 && existing == 0)
			{
				PanelUploadNoError.Visible = true;
				ASPxGridViewSmartcardNumbers.Visible = false;
				return;
			}
			else if (duplicates > 0)
			{
                uploadMessage1 = resourceManager.GetString("dbSmartcardError4"); 
				uploadMessage2 = string.Empty;
			}
			else if (existing > 0)
			{
                uploadMessage1 = resourceManager.GetString("dbSmartcardError5"); 
                uploadMessage2 = resourceManager.GetString("dbSmartcardError6"); 
			}

			LabelUploadWarning.Text = uploadMessage1;
			LabelUploadWarning2.Text = uploadMessage2;

			PanelUploadComplete.Visible = true;
		}		

		public bool SetErrorUploadStatusImageVisible(object status)
        {
			return ((string)status).StartsWith("Issued to") ;
        }

		public bool SetFatalErrorUploadStatusImageVisible(object status)
        {
			switch ((string)status)
			{
				case "Invalid characters":
				case "Mifare number not unique":
				case "dbSmartcard number not unique":
					return true;
				default:
					return false;
			}
        }

		public bool SetSuccessUploadStatusImageVisible(object status)
		{
			return ((string)status) == "Success";	
		}
		#endregion 
	}
}