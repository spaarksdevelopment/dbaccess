﻿using System;
using System.Configuration;
using System.Web;
using Microsoft.Security.Application;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class Error : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LabelEventID.Text = Global.LastErrorEventID.ToString();            
                TextBoxUserName.Text = CurrentUser.FullName;
                TextBoxUserType.Text = CurrentUser.RolesNamesList;
                TextBoxEmail.Text = CurrentUser.Email;
            }
            catch
            {
                TextBoxUserName.ReadOnly = false;
                TextBoxUserType.ReadOnly = false;
                TextBoxEmail.ReadOnly = false;
            }
        }
        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            var comments = Encoder.HtmlEncode(TextBoxComments.Text);
            var uname =  Encoder.HtmlEncode(TextBoxUserName.Text);
            var utype =  Encoder.HtmlEncode(TextBoxUserType.Text);
            var uemail =  Encoder.HtmlEncode(TextBoxEmail.Text);

            if (LogHelper.LogExceptionComment(Global.LastErrorEventID, comments, uemail, uname, utype))
                LabelThanks.Visible = true;
            else
                LabelError.Visible = true;

            PanelComments.Visible = false;

            Global.LastErrorEventID = 0;
        }
    }
}
