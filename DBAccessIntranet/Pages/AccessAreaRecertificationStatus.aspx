﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="AccessAreaRecertificationStatus.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.AccessAreaRecertificationStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
	<h1>Access Area Recertification Status</h1>
	<div>Access Area recertification</div>
	<br />
	<br />
	<dx:ASPxGridView ID="ASPxGridViewAccessAreas" ClientInstanceName="gvAccessAreas"
    runat="server" AutoGenerateColumns="False" KeyFieldName="AccessAreaID" Enabled="true" Visible="true" 
    ClientIDMode="AutoID" >
    <Columns>
        <dx:GridViewDataImageColumn Caption="Date Due" FieldName="NextCertificationDate" ShowInCustomizationForm="True" VisibleIndex="1">
                    <DataItemTemplate>
                        <asp:Image ID="RedImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightRed.gif" runat="server" Visible='<%# setImageRedVisible(Eval("LastCertifiedDate"), Eval("NextCertificationDate"))%>'/>
						<asp:Image ID="AmberImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightAmber.gif" runat="server" Visible='<%# setImageAmberVisible(Eval("LastCertifiedDate"), Eval("NextCertificationDate")) %>' />
						<asp:Image ID="GreenImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightGreen.gif" runat="server" Visible='<%# setImageGreenVisible(Eval("LastCertifiedDate"), Eval("NextCertificationDate")) %>' />
						 <asp:Label ID="Data" Text='<%# String.Format("{0:dd-MMM-yyyy}",Eval("NextCertificationDate")) %>' runat="server" />
                    </DataItemTemplate>
        </dx:GridViewDataImageColumn>
        <dx:GridViewDataTextColumn Caption="Division Name"  FieldName="DivisionName" ShowInCustomizationForm="True" VisibleIndex="2" CellStyle-HorizontalAlign="Right" />
        <dx:GridViewDataTextColumn Caption="Country" FieldName="CountryName" ShowInCustomizationForm="True" VisibleIndex="3" CellStyle-HorizontalAlign="Right" />
      
        
    </Columns>
  </dx:ASPxGridView>

</asp:Content>

