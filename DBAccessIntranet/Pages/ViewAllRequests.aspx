﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="ViewAllRequests.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.ViewAllRequests" UICulture="auto" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v13.2, Version=13.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v13.2, Version=13.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#popupRequestDetails").dialog({
                autoOpen: false,
                title: 'Request Details',
                width: '400',
                height: '500',
                modal: false
            });
        });

        function GetRequestDetails(requestId, type) {
            PageMethods.GetRequestDetails(requestId, type, OnGetRequestDetails);
        }

        function OnGetRequestDetails(result) {
            $("#TaskDescription").html(("" != result) ? result : "Sorry, the details for this task could not be retrieved.");
            $('#popupRequestDetails').dialog('open');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
    <h1>
        <asp:Literal ID="ViewAllRequestsTitle" runat="server" meta:resourceKey="ViewAllRequestsTitle"></asp:Literal>
    </h1>
    <div style="text-align: right; margin-right: 120px;">
        <table>
            <tr align="left">
                <td>
                    <dx:ASPxLabel ID="AspxlblStartDate" runat="server" Text=""
                        meta:resourcekey="AspxlblStartDate">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="AspxStartDateFilter" runat="server">
                    </dx:ASPxDateEdit>
                </td>
                <td></td>
                <td>
                    <dx:ASPxLabel ID="AspxlblEndDate" runat="server" Text=""
                        meta:resourcekey="AspxlblEndDate">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="ASPxEndDateFilter" runat="server">
                    </dx:ASPxDateEdit>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </div>
    <div style="text-align: right">
        <table>
            <tr>
                <td>
                    <dx:ASPxComboBox runat="server" SelectedIndex="0" ValueType="System.String"
                        ID="cbSearchType">
                        <items>
							<dx:ListEditItem Selected="True" Text="" Value="Applicant" 
								meta:resourcekey="ListEditItemApplicant" />
							<dx:ListEditItem Text="" Value="Requestor" 
								meta:resourcekey="ListEditItemRequestor" />
							<dx:ListEditItem Text="" Value="RequestID" 
								meta:resourcekey="ListEditItemRequestID" />
							<dx:ListEditItem Text="" Value="Building" 
								meta:resourcekey="ListEditItemBuilding" />
							<dx:ListEditItem Text="" Value="Area" 
								meta:resourcekey="ListEditItemArea" />
							<dx:ListEditItem Text="" Value="Task" 
								meta:resourcekey="ListEditItemTask" />
						</items>
                    </dx:ASPxComboBox>
                </td>
                <td>
                    <dx:ASPxTextBox ID="SearchBox" runat="server" ToolTip=""
                        Width="250px" meta:resourcekey="SearchBox">
                    </dx:ASPxTextBox>
                </td>
                <td>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text=""
                        meta:resourcekey="ASPxButton1">
                    </dx:ASPxButton>
                </td>
                <td>
                    <dx:ASPxButton ID="ASPxButton2" OnClick="Clear_Clicked" runat="server"
                        Text="" meta:resourcekey="ASPxButton2">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
    <div id="allrequests">
        <dx:ASPxPageControl ID="requestsTabPage" runat="server"
            ActiveTabIndex="0" EnableHierarchyRecreation="True"
            Height="250px" Width="100%"
            OnActiveTabChanging="requestsTabPage_ActiveTabChanging" AutoPostBack="True">
            <TabPages>
                <dx:TabPage Text="" meta:resourcekey="TabPageRequests">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <p>
                                <asp:Literal ID="ExportToExcel" runat="server" meta:resourceKey="ExportToExcel"></asp:Literal><asp:ImageButton ID="ImageButton2"
                                    OnClick="btnAllRequests_Click"
                                    AlternateText="" SkinID="FileXLS"
                                    runat="server" meta:resourcekey="ExportToExcelAlt" />
                            </p>
                            <dx:ASPxGridView ID="ASPxGridViewAllRequests" KeyFieldName="RequestID"
                                ClientInstanceName="gvViewAllRequests" runat="server" width="100%"
                                OnHtmlRowCreated="ASPxGridViewAllRequests_HtmlRowCreated">
                                <clientsideevents rowdblclick="function(s, e) { GetRequestDetails(s.GetRowKey(e.visibleIndex), &#39;All&#39;) }"></clientsideevents>
                                <columns>
                 <dx:GridViewDataTextColumn Caption="" FieldName="RequestID" 
					 ShowInCustomizationForm="True" VisibleIndex="1" 
					 meta:resourcekey="GridViewDataTextColumnRequestID"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Task" 
					 ShowInCustomizationForm="True" VisibleIndex="2" 
					 meta:resourcekey="GridViewDataTextColumnTask"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Applicant" 
					 ShowInCustomizationForm="True" VisibleIndex="3" 
					 meta:resourcekey="GridViewDataTextColumnApplicantName"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Building" FieldName="Building" 
					 ShowInCustomizationForm="True" VisibleIndex="4" 
					 meta:resourcekey="GridViewDataTextColumnBuilding"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Floor" 
					 ShowInCustomizationForm="True" VisibleIndex="5" 
					 meta:resourcekey="GridViewDataTextColumnFloor"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Access_Area" 
					 ShowInCustomizationForm="True" VisibleIndex="6" 
					 meta:resourcekey="GridViewDataTextColumnAccessArea"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="StartDate" 
					 ShowInCustomizationForm="True" VisibleIndex="7" 
					 meta:resourcekey="GridViewDataDateColumnStartDate" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="EndDate" 
					 ShowInCustomizationForm="True" VisibleIndex="8" 
					 meta:resourcekey="GridViewDataDateColumnEndDate" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="RequestorName" 
					 ShowInCustomizationForm="True" VisibleIndex="9" 
					 meta:resourcekey="GridViewDataTextColumnRequesterName"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="DateCreated" 
					 ShowInCustomizationForm="True" VisibleIndex="10" 
					 meta:resourcekey="GridViewDataDateColumnDateCreated" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="Status" 
					 ShowInCustomizationForm="True" VisibleIndex="11" 
					 meta:resourcekey="GridViewDataTextColumnStatus"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Visible="False" Caption="" 

					 FieldName="ApprovedBy" ShowInCustomizationForm="True" VisibleIndex="12" 
					 meta:resourcekey="GridViewDataTextColumnApprovedBy"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Visible="False" Caption="" 
					 FieldName="AccessApprovedDate" ShowInCustomizationForm="True" VisibleIndex="13" 
					 meta:resourcekey="GridViewDataDateColumnAccessApprovedDate" ></dx:GridViewDataDateColumn>
            </columns>
                                <settingstext emptydatarow="You don't have any tasks." />
                                <settingsbehavior allowselectbyrowclick="true" />
                                <settings showgrouppanel="True" />
                                <clientsideevents rowdblclick="function(s, e) { GetRequestDetails(s.GetRowKey(e.visibleIndex), 'All') }" />

                                <settings showfilterrow="False" showfilterrowmenu="False" showfilterbar="Hidden" showheaderfilterbutton="True" />

                                <settingsbehavior allowselectbyrowclick="True"></settingsbehavior>

                                <settings showheaderfilterbutton="True" showgrouppanel="True"></settings>

                                <settingstext emptydatarow="You don&#39;t have any tasks."></settingstext>
                            </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" ExportEmptyDetailGrid="False" PreserveGroupRowStates="True" Landscape="True" GridViewID="ASPxGridViewAllRequests">
                            </dx:ASPxGridViewExporter>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="" meta:resourcekey="TabPageAdminRequests">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl2" runat="server">
                            <p>
                                <asp:Literal ID="ExportToExcel1" runat="server" meta:resourceKey="ExportToExcel"></asp:Literal>
                                <asp:ImageButton ID="ImageButton1"
                                    OnClick="btnAllAdminRequests_Click"
                                    AlternateText="" SkinID="FileXLS"
                                    runat="server" meta:resourcekey="ExportToExcelAlt" />
                            </p>
                            <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="RequestID"
                                ClientInstanceName="gvViewAllAdminREquests" runat="server" Width="100%"
                                OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated">
                                <clientsideevents rowdblclick="function(s, e) { GetRequestDetails(s.GetRowKey(e.visibleIndex), &#39;Admin&#39;) }"></clientsideevents>
                                <columns>
                 <dx:GridViewDataTextColumn Caption="" FieldName="RequestID" 
					 ShowInCustomizationForm="True" VisibleIndex="1" 
					 meta:resourcekey="GridViewDataTextColumnRequestID"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Task" 
					 ShowInCustomizationForm="True" VisibleIndex="2" 
					 meta:resourcekey="GridViewDataTextColumnTask"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Applicant" 
					 ShowInCustomizationForm="True" VisibleIndex="3" 
					 meta:resourcekey="GridViewDataTextColumnApplicantName"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Building" 
					 ShowInCustomizationForm="True" VisibleIndex="4" 
					 meta:resourcekey="GridViewDataTextColumnBuilding"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Floor" 
					 ShowInCustomizationForm="True" VisibleIndex="5" 
					 meta:resourcekey="GridViewDataTextColumnFloor"></dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="" FieldName="Access_Area" 
					 ShowInCustomizationForm="True" VisibleIndex="6" 
					 meta:resourcekey="GridViewDataTextColumnAccessArea"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="StartDate" 
					 ShowInCustomizationForm="True" VisibleIndex="7" 
					 meta:resourcekey="GridViewDataDateColumnStartDate" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="EndDate" 
					 ShowInCustomizationForm="True" VisibleIndex="8" 
					 meta:resourcekey="GridViewDataDateColumnEndDate" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="RequestorName" 
					 ShowInCustomizationForm="True" VisibleIndex="9" 
					 meta:resourcekey="GridViewDataTextColumnRequesterName"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="" FieldName="DateCreated" 
					 ShowInCustomizationForm="True" VisibleIndex="10" 
					 meta:resourcekey="GridViewDataDateColumnDateCreated" ></dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="" FieldName="Status" 
					 ShowInCustomizationForm="True" VisibleIndex="11" 
					 meta:resourcekey="GridViewDataTextColumnStatus"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Visible="False" Caption="" 
					 FieldName="ApprovedBy" ShowInCustomizationForm="True" VisibleIndex="12" 
					 meta:resourcekey="GridViewDataTextColumnApprovedBy"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Visible="False" Caption="" 
					 FieldName="AccessApprovedDate" ShowInCustomizationForm="True" VisibleIndex="13" 
					 meta:resourcekey="GridViewDataDateColumnAccessApprovedDate" ></dx:GridViewDataDateColumn>
            </columns>
                                <settingstext emptydatarow="You don't have any tasks." />
                                <settingsbehavior allowselectbyrowclick="true" />
                                <settings showgrouppanel="True" />
                                <clientsideevents rowdblclick="function(s, e) { GetRequestDetails(s.GetRowKey(e.visibleIndex), 'Admin') }" />
                                <settings showfilterrow="False" showfilterrowmenu="False" showfilterbar="Hidden" showheaderfilterbutton="True" />

                                <settingsbehavior allowselectbyrowclick="True"></settingsbehavior>

                                <settings showheaderfilterbutton="True" showgrouppanel="True"></settings>

                                <settingstext emptydatarow="You don&#39;t have any tasks."></settingstext>
                            </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
                            </dx:ASPxGridViewExporter>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

            </TabPages>
        </dx:ASPxPageControl>
    </div>

    <div id="popupRequestDetails">
        <div id="TaskDescription"></div>
    </div>



    <ppc:Popups ID="PopupTemplates" runat="server" />
</asp:Content>
