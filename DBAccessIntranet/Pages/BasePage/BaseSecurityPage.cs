using System;
using System.Security;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Configuration;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Collections.Generic;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public abstract class BaseSecurityPage : BasePage
    {
        #region Declarations

        private DBAppUser _CurrentUser;
        private bool? m_isRestrictedUser = new Nullable<bool>();
        private int m_restrictedCountryId = 0;        

        #endregion

        #region Properties

        public DBAppUser CurrentUser
        {
            get { return _CurrentUser; }
        }

        public int RestrictedCountryId
        {
            get { return m_restrictedCountryId; }
        }

        public bool IsRestrictedUser
        {
            get
            {
                if (!m_isRestrictedUser.HasValue)
                {
                    m_isRestrictedUser = false;
                    int dbPeopleId = 0;

                    if (CurrentUser == null)
                    {
                        DBAppUser theCurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                        dbPeopleId = theCurrentUser.dbPeopleID;
                    }
                    else
                    {
                        dbPeopleId = CurrentUser.dbPeopleID;
                    }

                    mp_User theUser = Director.GetUserBydbPeopleId(dbPeopleId);

                    if (theUser != null && theUser.RestrictedCountryId.HasValue)
                    {
                        m_isRestrictedUser = true;
                        m_restrictedCountryId = theUser.RestrictedCountryId.Value;
                    }
                   
                }

                return m_isRestrictedUser.Value;
            }
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!this.DesignMode)
            {
                if (Context.User.Identity.IsAuthenticated && this.CurrentUser == null)
                {
                    //_CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                    _CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                    
                }
            }
        }
    }
}
