using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aduvo.Common.WebUtilityManager;
using DevExpress;
using DevExpress.Web.ASPxEditors;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public abstract class BasePage : Page
    {
        public string EntryPoint
        {
            get 
            {
                if (Session["EntryPoint"] == null)
                {
                    Session["EntryPoint"] = string.Empty;
                }

                return (string)Session["EntryPoint"];
            }
            set { Session["EntryPoint"] = value; }
        }

        public string PageThemeImagesFolder
        {
            get { return "~/App_Themes/" + Page.Theme + "/images/"; }
        }

        #region Page Events

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            AddHeaderScriptInclude("~/scripts/utils.js");
            AddHeaderScriptInclude("~/scripts/xml.js");
            
            //set up path variables
            string PathVariables = "var PageThemeImagesFolder = '" + Page.ResolveUrl(PageThemeImagesFolder) + "';";
            PathVariables += "var ServicesFolder = '" + Page.ResolveUrl("~/Services/") + "';";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "PathVariables", PathVariables, true);

        }

		protected void SetCulture(string lang)
		{			
			if (lang != null)
			{
				Session["Language"] = lang;
				System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo(lang);
				System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(lang);
			}
		}

        protected override void InitializeCulture()
        {
            CultureInfo c = new CultureInfo(Thread.CurrentThread.CurrentCulture.LCID);
            c.DateTimeFormat.ShortDatePattern = "dd/MMM/yyyy";
            Thread.CurrentThread.CurrentCulture = c;

			DBAppUser _CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

            if (Director.IsMultiLanguageOn())
            {
                string lang = GetCurrentUserCultureString(_CurrentUser);

                SetCulture(lang);
                base.InitializeCulture();
			}
        }

        private string GetCurrentUserCultureString(DBAppUser _CurrentUser)
        {
            string cultureString = null;
            int? languageId = null;
			
            if (_CurrentUser != null)
                languageId = Helper.SafeInt(_CurrentUser.LanguageId);

				string language = Request.Form["__EventTarget"];
            if (languageId == null)
				{
                if (IsLanguageStringValid(language))
					{
                    var currentLanguage = Director.GetLanguageEntityFromString(language);
                    cultureString = currentLanguage.LanguageCode;
					}

					if (Session["Language"] != null)
					{
                    cultureString = (string)Session["Language"];
					}
				}
				else
				{
                if (IsLanguageStringValid(language))
					{
                    var currentLanguage = Director.GetLanguageEntityFromString(language);
                    if (currentLanguage.LanguageId == languageId)
						{
                        cultureString = currentLanguage.LanguageCode;
						}
						else
						{
							string currentLanguageCode = Director.GetLanguageCode(currentLanguage.LanguageId);
                        cultureString = currentLanguageCode;
						}
					}
                else if (languageId > 0)
						{
                    string currentLanguageCode = Director.GetLanguageCode(languageId);
                    cultureString = currentLanguageCode;
					}
				}
			
            return cultureString;
        } 

        private bool IsLanguageStringValid(string language)
        {
            return language == Convert.ToString(DBAccessEnums.Language.English) || language == Convert.ToString(DBAccessEnums.Language.German) || language == Convert.ToString(DBAccessEnums.Language.Italian);
        } 

        #endregion

        #region Protected Methods

        #region EntryPoint

        protected void SetEntryPoint()
        {
            SetEntryPoint(Request.Url.PathAndQuery);
        }

        protected void SetEntryPoint(string pathAndQuery)
        {
            EntryPoint = pathAndQuery;
        }
                
        public string GetEntryPointURL()
        {
            string EntryUrl;

            if (string.IsNullOrEmpty(EntryPoint))
                EntryUrl = PageHelper.BuildUrl("Home.aspx", "Pages");
            else
                EntryUrl = EntryPoint;
            return EntryUrl;
        }

        protected void ReturnToEntryPoint()
        {
            string EntryUrl = GetEntryPointURL();
            Redirect(EntryUrl);           
        }

        #endregion

        #region Redirect

        protected void Redirect(string url)
        {
            try
            {
                Response.Redirect(url, true);
            }
            catch (Exception ex)
            {
                //need to catch pointless 'thread was aborted' error
                string error = ex.Message;
                Server.ClearError();
            }
        }

        protected void GoToErrorPage(string KnownError)
        {
            string ErrorUrl = PageHelper.BuildUrl("Error.aspx", "Pages");
            ErrorUrl += "?ErrorCode=" + KnownError;
            Redirect(ErrorUrl);
        }

        protected void RefreshPage()
        {
            string Url = Request.Url.ToString();
            Redirect(Url);
        }

        #endregion

        #region displaymessage

        protected void DisplayMessage(string caption, string message)
        {
            _DisplayMessage(message);
            //_DisplayMessage(caption, message, new Nullable<Unit>());
        }

        public void DisplayMessage(string caption, string message, int width)
        {
            _DisplayMessage(message);
            //_DisplayMessage(caption, message, new Unit(width));
        }

        protected void DisplayMessage(string caption, string message, Unit width)
        {
            _DisplayMessage(message);
            //_DisplayMessage(caption, message, width);
        }

        protected void DisplayMessageAndReturn(string caption, string message, bool prompt)
        {
            string url = GetEntryPointURL();
            DisplayMessageAndReturn(caption, message, prompt, url);
        }


        protected void DisplayMessageAndReturn(string caption, string message, bool prompt, string url)
        {
            if (prompt) _DisplayPromptToNavigate(message, url);
            else _DisplayMessageAndNavigate(message, url);
        }

        #endregion

        protected virtual void SetControls()
        {
        }

        #endregion

        #region client script related methods

        /// <summary>
        /// Use this to add scripts to the header.
        /// Page.RegisterClientScript methods place the scripts within the form.
        /// Some script includes have to go into the header to avoid problems.
        /// </summary>
        /// <param name="path"></param>
        protected void AddHeaderScriptInclude(string path)
        {
            HtmlGenericControl include = new HtmlGenericControl("script");
            include.Attributes.Add("type", "text/javascript");
            Random r = new Random();
            include.Attributes.Add("src", VirtualPathUtility.ToAbsolute(path) + "?rnd=" + r.Next().ToString());
            this.Page.Header.Controls.Add(include);
        }

        protected void AddHeaderScriptIncludeNoRnd(string path, string key)
        {     
           ClientScriptManager csm = Page.ClientScript;
           csm.RegisterClientScriptInclude(key, VirtualPathUtility.ToAbsolute(path));              
        }

        /// <summary>
        /// Register a script resource
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        protected void RegisterScriptResource(Type type, string scriptName)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);

            // Add script resource for item depending whether a scriptmanager is in use
            if (sm != null)
            {
                ScriptManager.RegisterClientScriptResource(this.Page, type, scriptName);
            }
            else
            {
                // If no Ajax ScriptManager then use standard ClientScriptManager
                Page.ClientScript.RegisterClientScriptResource(type, scriptName);
            }
        }

        /// <summary>
        /// Register a script include
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptURL">relateve script URL will be converted to Absolute</param>
        protected void RegisterScriptInclude(string scriptName, string scriptURL)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            //scriptURL = VirtualPathUtility.ToAbsolute(scriptURL);
            scriptURL = Page.ResolveClientUrl(scriptURL);

            // Add script resource for item depending whether a scriptmanager is in use
            if (sm != null)
            {
                ScriptManager.RegisterClientScriptInclude(this.Page, this.GetType(), scriptName, scriptURL);
            }
            else
            {
                // If no Ajax ScriptManager then use standard ClientScriptManager
                Page.ClientScript.RegisterClientScriptInclude(this.GetType(), scriptName, scriptURL);
            }
        }

        /// <summary>
        /// Register a startup script
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptText"></param>
        protected new void RegisterStartupScript(string scriptName, string scriptText)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);

            //startup scripts should be deferred, so adding tags manually
            //scriptText = string.Format("<script type=\"text/javascript\" language=\"javascript\" defer=\"defer\"><!--\n{0}\n//--></script>",scriptText);

            // Add script resource for item depending whether a scriptmanager is in use
            if (sm != null)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), scriptName, scriptText, true);
            }
            else
            {
                // If no Ajax ScriptManager then use standard ClientScriptManager
                Page.ClientScript.RegisterStartupScript(this.GetType(), scriptName, scriptText, true);
            }
        }


        /// <summary>
        /// Register a startup script
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptText"></param>
        protected new void RegisterClientScriptBlock(string scriptName, string scriptText)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);

            // Add script resource for item depending whether a scriptmanager is in use
            if (sm != null)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), scriptName, scriptText, true);
            }
            else
            {
                // If no Ajax ScriptManager then use standard ClientScriptManager
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptText, true);
            }
        }

        #endregion

        #region Private Methods

        /*
        private void _DisplayMessage(string caption, string message, Nullable<Unit> width)
        {
            EnsureChildControls();

            Web.MasterPages.DBIntranetMasterPage DBMaster = (Web.MasterPages.DBIntranetMasterPage)this.Master;

            Web.Controls.MessageBoxPopupControl MessageBox = DBMaster.MessagePopupControl;

            MessageBox.Caption = caption;
            MessageBox.Text = message;

            if (width.HasValue)
            {
                MessageBox.Width = width.Value;
            }
        }
         * */

        private void _DisplayMessage(string message)
        {
            string scriptText = "alert(\"" + message.Replace("\n","\\n") + "\");";
            RegisterStartupScript("DisplayMessage", scriptText);
        }

        private void _DisplayMessageAndNavigate(string message, string url)
        {
            string scriptText = "alert(\"" + message.Replace("\n", "\\n") + "\"); window.location='" + url + "';";
            RegisterStartupScript("DisplayMessage", scriptText);
        }

        private void _DisplayPromptToNavigate(string message, string url)
        {
            string scriptText = "if(confirm(\"" + message.Replace("\n","\\n") + "\")) window.location='" + url + "';";            
            RegisterStartupScript("DisplayPrompt", scriptText);
        }
        
        #endregion
    }
}