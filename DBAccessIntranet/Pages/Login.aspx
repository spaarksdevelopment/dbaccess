﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/DBPortal.master" CodeBehind="Login.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.LoginPage" uiculture="auto" Culture="auto" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>
<%@ Register TagPrefix="uc1" Src="~/Controls/NewsFeed.ascx" TagName="News" %>
<%@ Register TagPrefix="uc2" Src="~/Controls/NeedAssistance.ascx" TagName="NeedAssistance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="Server">

	<script language="javascript" type="text/javascript">

		function ImageShow (nodeImage, bShow, sImage)
		{
			if (null == bShow) bShow = false;
			if (null == sImage) sImage = "";
			if (null == nodeImage) bShow = false;

			var ctl_popupTooltip = ASPxClientPopupControl.Cast ("popupTooltip");
			var ctl_popupTooltip_Image = ASPxClientImage.Cast ("popupTooltip_Image");

			if (bShow)
			{
				ctl_popupTooltip_Image.SetImageUrl(sImage);
				ctl_popupTooltip.ShowAtElementByID(nodeImage.id);
			}
			else
			{
				ctl_popupTooltip.Hide();
			}
		}
		
		function ShowAddPersonAR()
		{
			popupEditDivision.Show();
        }
	</script>


	<table width="100%" cellspacing="4">
		<tr style="vertical-align: top;">

			<td  align="left" width="500px">
			</td>
			<td style="width: 40.0em; ">
				<div style="width: 40.0em;">
					<asp:Panel runat="server" id="PanelLogin" SkinID="GreyBorder320">
						<asp:Panel runat="server" id="Panel4" SkinID="Shaded">
							<asp:Label ID="Label3" runat="server" Text="Log in" SkinID="Title" 
								meta:resourcekey="Label3Resource1"></asp:Label>
						</asp:Panel>
						<div class="padding10" style="text-align:left;">

							<asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Pages/Home.aspx" OnLoggingIn="Login1_OnLoggingIn" OnLoggedIn="Login1_OnLoggedIn" 
								meta:resourcekey="Login1Resource1">
							<LayoutTemplate>
								<table border="0" cellpadding="0">
									<tr>
										<td align="right" class="form_label">
											<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" meta:resourcekey="UserNameLabelText">User Email:</asp:Label>
										</td>
										<td class="form_item">
											<asp:TextBox ID="UserName" runat="server"></asp:TextBox>
												<asp:RequiredFieldValidator ID="UserNameRequired" runat="server"
													ControlToValidate="UserName" ErrorMessage="User Email is required."
													ToolTip="Please enter you Group Directory Email address." ValidationGroup="Login1" 
												meta:resourcekey="UserNameRequiredResource1">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="right" class="form_label">
											<asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" 
												meta:resourcekey="PasswordLabelText">Password:</asp:Label>
										</td>
										<td class="form_item">
											<asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
											<asp:RequiredFieldValidator ID="PasswordRequired" runat="server"
												ControlToValidate="Password" ErrorMessage="Group Directory Password is required."
												ToolTip="Please enter your Group Directory Password." ValidationGroup="Login1" 
												meta:resourcekey="PasswordRequiredResource1">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="center" colspan="2" style="color:Red;">
											<asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
										</td>
									</tr>
									<tr>
										<td class="form_label" valign="top">
										</td>
										<td align="right">
											<asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
												ValidationGroup="Login1" meta:resourcekey="LoginButtonResource1" />
										</td>
									</tr>
								</table>

							</LayoutTemplate>
							</asp:Login>

							<asp:Label ID="LabelTestModeWarning" runat="server" 
								Text="Test Mode - any password will be accepted for registered email addresses." 
								ForeColor="Red" Visible="False" Font-Size="0.7em" 
								meta:resourcekey="LabelTestModeWarningResource1"></asp:Label>
						</div>
					</asp:Panel>
					<%--<asp:Panel runat="server" id="PanelNotAuthorised" Visible="false">
						<h1>Sorry - it appears that you are not authorised to access this site.</h1>
					</asp:Panel>--%>
					<br />
				</div>

				<br />

				<div>
					<table>
						<tr>
							<td align="left"> <div><h3>Group Directory Login</h3></div></td>
						</tr>
						<tr>
							<td align="left">								
								<div style="width:25.0em">
									<asp:Literal ID="GDLoginText1" runat="server" meta:resourcekey="GDLoginText1">
									</asp:Literal>
								</div>
							</td>
						</tr>
						<tr>
							<td align="left">
								<div style="width:25.0em">
									<asp:Literal ID="GDLoginText2" runat="server" meta:resourcekey="GDLoginText2">
									</asp:Literal>
								</div>
							</td>
						</tr>
						<tr>
							<td align="left">
								Go to <a href="http://gd.db.com/perl/faq#faq1">http://gd.db.com/perl/faq#faq1</a>
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td>
				<div>
					<uc2:NeedAssistance ID="NeedAssistance1" runat="server" />
				</div>
			</td>
		</tr>
	</table>
</asp:Content>
