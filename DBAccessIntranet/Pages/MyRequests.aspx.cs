﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.Services;
using System.Web.Script.Services;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Data;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;
using System.Resources;
using System.Threading;
using Spaarks.Common.UtilityManager;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class MyRequests : BaseSecurityPage
    {
		private int languageID
		{
			get { return Helper.SafeInt(this.CurrentUser.LanguageId); }
			set { this.CurrentUser.LanguageId = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			SetGridDataSource();		
	    }

       #region ASPX methods

		void SetGridDataSource()
		{
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(languageID, currentUserLanguageCode);
			languageID = Convert.ToInt32(LanguageId);
			var requestMaster = Director.GetMyMasterRequestsAll(this.CurrentUser.dbPeopleID, LanguageId);

			ASPxGridViewMyRequest.DataSource = requestMaster;
			ASPxGridViewMyRequest.DataBind();
			if (!IsPostBack)
				ASPxGridViewMyRequest.FocusedRowIndex = -1;
		}

		

		/// <summary>
        /// method to set the visibility for the red traffic light image
        /// </summary>
        /// <param name="LandlordID"></param>
        /// <returns></returns>
		/// 
        public bool setImageRedVisible(object status)
        {
				string strStatus = (string)status;
				switch (languageID)
				{
					case (int)DBAccessEnums.Language.English:
						{
							if (strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Rejected) ||
								strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Cancelled) ||
								strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Failed))
							{
								return true;
							}
							break;
						}				
					default:
						{
							//get the english version and compare it with the above valus and return the result
							string strEnglishVersion = Director.GetEnglishVersion(strStatus, languageID);
							if (strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Rejected) ||
								strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Cancelled) ||
								strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Failed))
							{
								return true;
							}
							break;
						}
				}
               
            return false;
        }

        /// <summary>
        /// method to set the visibility for the amber traffic light image
        /// </summary>
        /// <param name="LandlordID"></param>
        /// <returns></returns>
        public bool setImageAmberVisible(object status)
        {
			string strStatus = (string)status;
			switch (languageID)
			{
				case (int)DBAccessEnums.Language.English:
					{
						if (strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Draft) ||
							strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Submitted) ||
                            strStatus.Replace(" ", "") == Convert.ToString(DBAccessEnums.AccessRequestStatus.InProgress))
						{
							return true;
						}
						break;
					}
				default:
					{
						//get the english version and compare it with the above valus and return the result
						string strEnglishVersion = Director.GetEnglishVersion(strStatus, languageID);
						if (strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Draft) ||
							strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Submitted) ||
							strEnglishVersion.Replace(" ","") == Convert.ToString(DBAccessEnums.AccessRequestStatus.InProgress))
						{
							return true;
						}
						break;
					}
			}
            return false;
        }

        /// <summary>
        /// method to set the visibility for the green traffic light image
        /// </summary>
        /// <param name="LandlordID"></param>
        /// <returns></returns>
        public bool setImageGreenVisible(object status)
        {
			string strStatus = (string)status;
			switch (languageID)
			{
				case (int)DBAccessEnums.Language.English:
					{
						if (strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Approved) ||
							strStatus == Convert.ToString(DBAccessEnums.AccessRequestStatus.Actioned))
						{
							return true;
						}
						break;
					}
				default:
					{
						//get the english version and compare it with the above valus and return the result
						string strEnglishVersion = Director.GetEnglishVersion(strStatus, languageID);
						if (strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Approved) ||
							strEnglishVersion == Convert.ToString(DBAccessEnums.AccessRequestStatus.Actioned))
						{
							return true;
						}
						break;
					}
			}
            return false;
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
			ExportMyRequests();
        }
        #endregion

		private void ExportMyRequests() 
		{
			List<vw_MyRequestsDetailReport> exportData = Director.GetMyRequestDetailsReport(CurrentUser.dbPeopleID);
			List<string> columnsToExport = GetColumnNamesForExport();

			List<List<string>> exportDataStrings = ExcelExport.GetRowData(exportData, columnsToExport);

			List<string> columnHeaders = GetColumnHeaders();

			ExcelExport.ExportToExcel("MyRequests", columnHeaders, exportDataStrings, false);
		}

		private List<string> GetColumnNamesForExport()
		{
			return new List<string>()
			{
				"StatusDesc",
			    "RequestType", 
			    "Created", 
			    "ApplicantName", 
			    "StartDate", 
			    "EndDate", 
			    "CityName",
			    "BuildingName", 
			    "FloorName", 
			    "AccessAreaName", 
			    "History"
			};
		}

		private List<string> GetColumnHeaders()
		{
			return new List<string>()
			{
				"Status",
			    "Request Type",
			    "Date Submitted",
			    "Applicant Name",
				"Start Date", 
			    "End Date", 
			    "City",
			    "Building",
			    "Floor",
			    "Access Area",
			    "History"
			};
		}
    }
}