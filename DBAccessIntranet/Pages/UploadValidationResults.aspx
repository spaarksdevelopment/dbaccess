﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="UploadValidationResults.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.UploadValidationResults" %>

<%@ Register assembly="Brettle.Web.NeatUpload" namespace="Brettle.Web.NeatUpload" tagprefix="Upload" %>
<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
	<script type="text/javascript">
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
	<h1><asp:Literal ID="UploadVendorFile2" Visible="true" runat="server" ClientIDMode="Static"> </asp:Literal></h1>
	
	<asp:Panel ID="PanelUploadComplete" runat="server" Visible="false">
		<asp:Label class="body" style="color: Red" ID="LabelUploadWarning" runat="server">
		</asp:Label>
		<br />
		<asp:Label class="body" style="color: Red" ID="LabelUploadWarning2" runat="server">
		</asp:Label>
	</asp:Panel> 
	
	<asp:Panel ID="PanelUploadNoError" runat="server" Visible="false">
		<asp:Label class="body" ID="LabelNoError" runat="server" Text="<%$ Resources:CommonResource, ValidationSuccessfulResults %>">
		</asp:Label>
		<asp:Label class="body" ID="LabelNoError2" runat="server" Text="<%$ Resources:CommonResource, ClickUploadToContinue %>">
		</asp:Label>
	</asp:Panel>

	<br />
	<div>
		<dx:ASPxGridView ID="ASPxGridViewSmartcardNumbers" ClientInstanceName="gvSmartcardNumbers"
             runat="server"  AutoGenerateColumns="False"
             KeyFieldName="ID">
			<Columns>
				<dx:GridViewDataImageColumn Caption=" " FieldName="UploadStatus" ShowInCustomizationForm="True" VisibleIndex="1" CellStyle-HorizontalAlign="Left">
                    <DataItemTemplate>
                        <asp:Image ID="RedImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightRed.gif" runat="server" Visible="<%# SetFatalErrorUploadStatusImageVisible(Container.Text) %>" />
						<asp:Image ID="AmberImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightAmber.gif" runat="server" Visible="<%# SetErrorUploadStatusImageVisible(Container.Text) %>" />
                        <asp:Image ID="GreenImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightGreen.gif" runat="server" Visible="<%# SetSuccessUploadStatusImageVisible(Container.Text) %>" />
                    </DataItemTemplate>
                </dx:GridViewDataImageColumn>
				<dx:GridViewDataTextColumn FieldName="UploadStatus" Caption="Details" ShowInCustomizationForm="True" VisibleIndex="2">
			    </dx:GridViewDataTextColumn>
			    <dx:GridViewDataTextColumn FieldName="Mifare" ShowInCustomizationForm="True" VisibleIndex="3">
			    </dx:GridViewDataTextColumn>
			    <dx:GridViewDataTextColumn FieldName="SerialNumber" Caption="Serial Number" ShowInCustomizationForm="True" VisibleIndex="4">
			    </dx:GridViewDataTextColumn>
			</Columns>
			<Settings ShowStatusBar="Visible"/> 
			<SettingsPager PageSize="25" />                
		</dx:ASPxGridView>
		<br />
		<dx:ASPxButton style="float: left" ID="btnCancelUpload" AutoPostBack="false" ClientIDMode="Static" runat="server" Text="<%$ Resources:CommonResource, Cancel %>" OnClick="btnCancelUpload_Click">
		</dx:ASPxButton>
		<dx:ASPxButton style="float: left; margin-left: 10px" ID="btnLoadSmartcardNumbers" AutoPostBack="false" ClientIDMode="Static" runat="server" Text="<%$ Resources:CommonResource, Upload %>" OnClick="btnLoadSmartcardNumbers_Click">
		</dx:ASPxButton>
		<div style="clear"></div>
	</div>
</asp:Content>
