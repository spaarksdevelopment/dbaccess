﻿using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;

using DevExpress.Web.ASPxGridView;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.Common.UtilityManager;
using System.Resources;
using System.Threading;
using System.Configuration;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;
using DBAccess.Model.Enums;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class MyDivisions : BaseSecurityPage
    {
        public const string RequestApproverCacheKey = @"BusinessAreas/RequestApproverList";
        public const string DivisionRoleUsersCacheKey = @"BusinessAreas/DivisionRoleUserList";


        private int LanguageID
        {
            get { return Helper.SafeInt(CurrentUser.LanguageId); }
            set { CurrentUser.LanguageId = value; }
        }

        private static List<string> BAUpdateMessage = new List<string>();
        private static List<string> DAUpdateMessage = new List<string>();
        private static List<string> DAAddNewMessage = new List<string>();

        private int? DivisionId
        {
            get
            {
                return (ASPxHiddenFieldDivision.Contains("DivisionID")) ? Helper.SafeIntNullable(ASPxHiddenFieldDivision.Get("DivisionID")) : null;
            }
            set
            {
                ASPxHiddenFieldDivision.Set("DivisionID", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            base.AddHeaderScriptInclude("~/scripts/validationservice.js");
            string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
            int? languageIDNullable = Director.GetLanguageID(LanguageID, currentUserLanguageCode);
            LanguageID = Convert.ToInt32(languageIDNullable);

            if (!Page.IsPostBack)
            {
                Session[RequestApproverCacheKey] = null;
                Session[DivisionRoleUsersCacheKey] = null;
            }
            GetMyDivisions();

            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

            BAUpdateMessage.Clear();
            BAUpdateMessage.Add(resourceManager.GetString("CannotUpdateBA"));
            BAUpdateMessage.Add(resourceManager.GetString("BusinessArea"));
            BAUpdateMessage.Add(resourceManager.GetString("MinimumApproversConditionBA"));
            BAUpdateMessage.Add(resourceManager.GetString("BusinessArea"));
            BAUpdateMessage.Add(resourceManager.GetString("DivisionUpdate"));
            BAUpdateMessage.Add(resourceManager.GetString("Division"));

            DAUpdateMessage.Clear();

            DAUpdateMessage.Add(resourceManager.GetString("DivisionAdminUpdate"));
            DAUpdateMessage.Add(resourceManager.GetString("Confirmation"));
            DAUpdateMessage.Add(resourceManager.GetString("ProblemDAChanges"));

            DAAddNewMessage.Clear();

            DAAddNewMessage.Add(resourceManager.GetString("CannotAddDA"));
            DAAddNewMessage.Add(resourceManager.GetString("AlreadyDA"));
            DAAddNewMessage.Add(resourceManager.GetString("AlreadyDO"));
            DAAddNewMessage.Add(resourceManager.GetString("NoMoreThanFive").Replace("5", MaximumDAs.ToString())); // Somewhat hacky way to show correct maximum from config file
            DAAddNewMessage.Add(resourceManager.GetString("CannotAddInactivePerson"));
            DAAddNewMessage.Add(resourceManager.GetString("AddAdministrator"));

        }

        public string SetLanguageText(object text)
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            return resourceManager.GetString(Convert.ToString(text));
        }

        private void SetDivision()
        {
            if (DivisionId.HasValue)
            {
                var currentDivision = Director.GetDivision(DivisionId.Value);
                var divisionCountry = Director.GetCountry(currentDivision.CountryID);

                ASPxHiddenFieldDivision.Set("DivisionName", currentDivision.Name + " - " + divisionCountry.Name + " (" + currentDivision.UBR + ")");

                ASPxHiddenFieldDivision.Set("UBR", currentDivision.UBR);
                GetData();

                DivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "block");
            }
            else
            {
                DivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none");
            }
        }

        private void GetData()
        {
            List<vw_BusinessAreasWithApprovers> businessAreas = Director.GetMyDivisions(base.CurrentUser.dbPeopleID, DivisionId);

            if(businessAreas != null && businessAreas.Count()==1)
            ASPxHiddenFieldDivision.Set("BusinessAreaID", businessAreas.SingleOrDefault().BusinessAreaID.ToString());
        }

        protected void BusinessAreas_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GetData();
        }

        private void GetMyDivisions()
        {
            var divisions = Director.GetAllMyDivisions(base.CurrentUser.dbPeopleID, LanguageID);

            if (IsRestrictedUser)
            {
                divisions = divisions.Where(e => e.CountryID == base.RestrictedCountryId).ToList();
            }

            if (divisions.Count == 0)
            {
                ChooseDivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none");
                DivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none");
            }
            if (divisions.Count == 1)//
            {
                //ChooseDivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none");
                DivisionId = divisions[0].DivisionID;
                SetDivision();

                MyDdlDivisions.DataSource = divisions;
                MyDdlDivisions.DataBind();
            }
            else
            {
                MyDdlDivisions.DataSource = divisions;
                MyDdlDivisions.DataBind();

                DivisionPanel.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none");
            }

        }

        protected void ASPxCallbackPanelEditBusinessArea_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            // business area has changed so clear this cache
            Session[RequestApproverCacheKey] = null;

            int businessAreaID = Helper.SafeInt(e.Parameter);

            EditBusinessAreaControl1.BusinessAreaID = businessAreaID;
            EditBusinessAreaControl1.BindData();


            var businessArea = Director.GetVwBusinessArea(businessAreaID, LanguageID);

            string headerText = string.Empty;

            if (businessArea != null)
                headerText = " - " + businessArea.UBR + " - " + businessArea.BusinessAreaName + " - " + businessArea.CountryName;

            ASPxHiddenBADetailHeader.Set("HeaderText", headerText);
        }

        protected void ASPxCallbackPanelBusinessAreas_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            DivisionId = Helper.SafeInt(e.Parameter);
            SetDivision();
        }

        #region WebMethods

        [WebMethod(EnableSession = true), ScriptMethod]
        public static void RemoveDraftRequestsForThisDivision(string divisionIDString)
        {
            int? divisionID = Convert.ToInt32(divisionIDString);
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();
            GenericRequestService genericService = new GenericRequestService();
            genericService.RemoveDraftDivisionRequestsForCurrentUser(divisionID, currentUserID);
        }

        #region MyBusinessAreas

        [WebMethod, ScriptMethod]
        public static int SubmitCode(int divisionId, string ubrCode, string areaName)
        {
            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            return director.CreateBusinessArea(divisionId, areaName, ubrCode);
        }

        #endregion

        //Unfortunately web methods can only be defined in page, not in control.
        #region EditBusinessAreaControl

        [WebMethod, ScriptMethod]
        public static string UpdateBusinessArea(bool enabled, string baName, int appType, int appNum, int businessAreaId)
        {
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            int nrval = director.UpdateBusinessArea(businessAreaId, enabled, baName, appType, appNum);

            string popupDetails = SetUpdateBAPopUpContent(nrval);
            return Convert.ToString(nrval) + "-" + popupDetails;
        }

        [WebMethod, ScriptMethod]
        public static bool UpdateBusinessAreaEnabledStatus(int status, int divisionId, int businessAreaId)
        {
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            Director director = new Director(currentUser.dbPeopleID);
            return director.UpdateBusinessAreaStatus(divisionId, businessAreaId, Convert.ToBoolean(status));
        }


        [WebMethod(EnableSession = true), ScriptMethod]
        public static string AddDivisionAdmin(int personID, int requestMasterID, int divisionId, int businessAreaId)
        {
            string popupDetails = string.Empty;
            int nReturnValue = 0;
            int requestID = 0;

            List<DivisionRoleUser> listDivisionRoleUsers = GetListDivisionRoleUsersFromSession();

            HRPerson person = Director.GetPersonById(personID);

            if (person == null)
            {
                nReturnValue = 1;		// we cannot find the person
                popupDetails = AddDAPopUpContent(nReturnValue);
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

            if (listDivisionRoleUsers.Select(a => a.DBPeopleID).Contains(personID) &&
                (listDivisionRoleUsers.Where(a => a.DBPeopleID == personID).FirstOrDefault() != null) &&
                (listDivisionRoleUsers.Where(a => a.DBPeopleID == personID).FirstOrDefault().SecurityGroupID == (int)DBAccessController.DBAccessEnums.RoleType.DivisionAdministrator))
            {
                nReturnValue = 2;
                popupDetails = AddDAPopUpContent(nReturnValue);
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

            if (listDivisionRoleUsers.Select(a => a.DBPeopleID).Contains(personID))
            {
                nReturnValue = 3;
                popupDetails = AddDAPopUpContent(nReturnValue);
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

            if (listDivisionRoleUsers.Count(a => a.SecurityGroupID == (int)DBAccessEnums.RoleType.DivisionAdministrator) >= MaximumDAs)
            {
                nReturnValue = 4;
                popupDetails = AddDAPopUpContent(nReturnValue);
                //Convert.ToString(nReturnValue) + "-" +
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

            // update the list with this person
            // add in details so that we can see what this user is in the grid
            DivisionRoleUser newPerson = new DivisionRoleUser();

            newPerson.DBPeopleID = personID;
            newPerson.EmailAddress = person.EmailAddress;
            newPerson.Forename = person.Forename;
            newPerson.Surname = person.Surname;
            newPerson.DivisionID = divisionId;
            newPerson.SecurityGroup = "Divisional Administrator";
            newPerson.SecurityGroupID = (int)DBAccessController.DBAccessEnums.RoleType.DivisionAdministrator;

            // make sure it is not a duplicate
            if (0 != listDivisionRoleUsers.Count(a => a.DBPeopleID == personID && a.DivisionID == divisionId))
            {
                nReturnValue = 0;
                popupDetails = AddDAPopUpContent(nReturnValue);
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

            // okay, add it to the list
            listDivisionRoleUsers.Add(newPerson);
            listDivisionRoleUsers = listDivisionRoleUsers.OrderBy(a => a.Surname).ThenBy(a => a.Forename).ToList();

            HttpContext.Current.Session[DivisionRoleUsersCacheKey] = listDivisionRoleUsers;

            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();

            //Now create the badge request
            Director director = new Director(currentUser.dbPeopleID);

            int bSuccess = director.CreateNewRequestForDivisionAdministrator(personID, ref requestID, ref requestMasterID, divisionId, businessAreaId);

            if (bSuccess == 0 || bSuccess == -3)
            {
                // okay, remove the person from list
                listDivisionRoleUsers.Remove(newPerson);
            }


            if (bSuccess != 0 && bSuccess != -3)
            {
                return requestMasterID + "|" + requestID + "|" + person.dbPeopleID + "-" + popupDetails;
            }
            
                popupDetails = AddDAPopUpContent(bSuccess);
                return Convert.ToString(nReturnValue) + "-" + popupDetails;
            }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static int DeleteDivisionAdmin(int? divisionID, int personID)
        {
            int currentUserID = GenericGUIHelpers.GetCurrentUserID();

            List<DivisionRoleUser> listDivisionRoleUsers = GetListDivisionRoleUsersFromSession();

            if (!listDivisionRoleUsers.Any()) return 1;

            if (listDivisionRoleUsers.Count(a => a.SecurityGroupID == (int)DBAccessEnums.RoleType.DivisionAdministrator) <= 2) 
                return 2;

            IDivisionService divisionService = new DivisionService(currentUserID);

            if (divisionID.HasValue)
            {
                if (!divisionService.DeleteDivisionRoleUser((int)divisionID, (int)RoleType.DivisionAdministrator, personID))
                {
                    return (int)DeleteDivisionRoleResult.DivisionOwnerCouldNotBeDeleted;
                }
                divisionService.RemoveDraftAndPendingDivisionRoleRequestForDeletedPerson(divisionID.Value, RoleType.DivisionAdministrator, personID);
            }
            listDivisionRoleUsers.Remove(listDivisionRoleUsers.Where(a => a.DBPeopleID == personID).Single());

            //rebuild the sort order in list per classificationID basing on the list entries
            //director.UpdateSortOrderMyBusinessArea(null, listDivisionRoleUsers, null, personID, DBAccessEnums.RequestType.AccessDA);
            return 0;
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static string SubmitDivisionAdministratorChanges(int? divisionIDNullable, int requestMasterID, int peopleID)
        {
            bool success = true;

            if (!divisionIDNullable.HasValue)
                return GetMessageForConfirmDivisionAdministratorChanges(false);

            int divisionID = divisionIDNullable.Value;

            List<DivisionRoleUser> listDivisionRoleUsers = GetListDivisionRoleUsersFromSession();

            //no base class available when using a static method, so need to get the user
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
            Director director = new Director(currentUser.dbPeopleID);

            success = director.UpdateDivisionRoleUsers(divisionID, listDivisionRoleUsers, currentUser.dbPeopleID);
            if (!success)
                return GetMessageForConfirmDivisionAdministratorChanges(false);

            if (!director.SubmitDivisionRoleRequests(divisionID))
                return GetMessageForConfirmDivisionAdministratorChanges(false);

            RemoveDivisionRoleUsersFromCache();

            return GetMessageForConfirmDivisionAdministratorChanges(true);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static void RemoveDivisionRoleUsersFromCache()
        {
            HttpContext.Current.Session[DivisionRoleUsersCacheKey] = null;
        }

        private static List<DivisionRoleUser> GetListDivisionRoleUsersFromSession()
        {
            List<DivisionRoleUser> listDivisionRoleUsers = new List<DivisionRoleUser>();

            // pick up the list from the cache if it exists
            if (HttpContext.Current.Session[DivisionRoleUsersCacheKey] != null)
            {
                listDivisionRoleUsers = (List<DivisionRoleUser>) HttpContext.Current.Session[DivisionRoleUsersCacheKey];
            }
            return listDivisionRoleUsers;
        }

        #endregion

        #endregion

        #region PrivateMethods
        public static string SetUpdateBAPopUpContent(int nrval)
        {
            string HeaderText = string.Empty;
            string ContentText = string.Empty;

            switch (nrval)
            {
                case 0:
                    {
                        ContentText = BAUpdateMessage[0];
                        HeaderText = BAUpdateMessage[1];
                        break;
                    }
                case -1:
                    {
                        ContentText = BAUpdateMessage[2];
                        HeaderText = BAUpdateMessage[3];
                        break;
                    }
                default:
                    {
                        ContentText = BAUpdateMessage[4];
                        HeaderText = BAUpdateMessage[5];
                        break;
                    }
            }
            return ContentText + "-" + HeaderText;
        }

        public static string GetMessageForConfirmDivisionAdministratorChanges(bool success)
        {
            string headerText = string.Empty;
            string contentText = string.Empty;

            switch (success)
            {
                case true:
                    {
                        contentText = DAUpdateMessage[0];
                        headerText = DAUpdateMessage[1];
                        break;
                    }
                case false:
                    {
                        contentText = DAUpdateMessage[2];
                        headerText = DAUpdateMessage[1];
                        break;
                    }
            }
            return success + "-" + contentText + "-" + headerText;
        }

        public static string AddDAPopUpContent(int nReturnValue)
        {
            string HeaderText = string.Empty;
            string ContentText = string.Empty;

            switch (nReturnValue)
            {
                case 1:
                    {
                        ContentText = DAAddNewMessage[0];
                        HeaderText = DAAddNewMessage[5];
                        break;
                    }
                case 2:
                    {
                        ContentText = DAAddNewMessage[1];
                        HeaderText = DAAddNewMessage[5];
                        break;
                    }
                case 3:
                    {
                        ContentText = DAAddNewMessage[2];
                        HeaderText = DAAddNewMessage[5];
                        break;
                    }
                case 4:
                    {
                        ContentText = DAAddNewMessage[3];
                        HeaderText = DAAddNewMessage[5];
                        break;
                    }
                case -3:
                    {
                        ContentText = DAAddNewMessage[3];
                        HeaderText = DAAddNewMessage[5];
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            return ContentText + "-" + HeaderText;
        }

        #endregion

        public static int MaximumDAs
        {
            get { return Int32.Parse(ConfigurationManager.AppSettings["MaximumDAs"]); }
        }
    }
}

