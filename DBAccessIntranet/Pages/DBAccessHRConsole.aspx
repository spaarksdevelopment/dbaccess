﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master" AutoEventWireup="true" CodeBehind="DBAccessHRConsole.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.DBAccessHRConsole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBHeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DBMainContent" runat="server">
     


     <div id="header">
<h1 style="margin-left: 5px">dbAccess HR Console</h1></div>
<br />
<br />
<br />

 <div style="float:left";>
  <asp:Label ID="LabelHRID" runat="server" Text="Enter HRID:" Font-Size="Medium" 
         Font-Bold="True"></asp:Label>
  </div>

 <div style="float:left";>
  <dx:ASPxTextBox ID="UserTextfield" runat="server" style="margin-left: 30px"  Height="25px"  Width="197px"> </dx:ASPxTextBox> 
 </div>

                  
<div style="float:left";><dx:ASPxButton ID="FindButton" runat="server" 
        style="margin-left: 60px" Text="Find" Height="25px" Width="143px" 
        onclick="FindButton_Click"></dx:ASPxButton></div>
                    
<div><dx:ASPxButton ID="ResetButton" runat="server" style="margin-left: 585px" Text="Reset" Height="25px" Width="143px"></dx:ASPxButton></div>

                    
        <br />
    

    <dx:ASPxLabel ID="ReminderLabel" runat="server"
                        Text="For external resources, please prefix with a 'C'" Font-Italic="True" 
                        Font-Size="X-Small">
                    </dx:ASPxLabel>
    <br />
    <br />
    <br />
    <br />
    <br />

  
    <table width="60%" cellpadding="0" cellspacing="0" border="0">
    <tr>

        <!-- this column has been set to not display so I can create the appearance of the alternative content of this area -->
        <td width="33%" valign="top" style="display: none;">
        <asp:Label ID="DetailsNoMatches" runat="server"
         Text="Personal Details" Font-Size="Medium"></asp:Label>   
        
        <p>&nbsp;</p>
            <dx:ASPxLabel ID="NoMatchesText" runat="server"
                        Text="No matches were found for this HRID in the dbAccess database. Please check the HRID or click 'Queue Refresh' to attempt to retrieve HR data."
                        Font-Size="Small">
                    </dx:ASPxLabel>
        </td>


         <!-- this column is for the same area as the above but for a different event reaction -->
        <td valign="top" style="width: 33%;">
          <asp:Label ID="DetailsMatchesLabel" runat="server" style="margin-left: 10px" 
          Text="Personal Details" Font-Size="Medium"></asp:Label>
          <p>&nbsp;</p>

            <dx:ASPxGridView ID="GridViewPerson" runat="server" style="margin-left: 10px" 
                AutoGenerateColumns="False" Visible="False">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Person Field" VisibleIndex="0">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Attribute/Value" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                </Columns>
            
            </dx:ASPxGridView>
            </td>


        <!-- this column has been set to not display so I can create the appearance of the alternative content of this area -->
        <td width="33%" valign="top" style="display: none;">
        <asp:Label ID="ResultsNoMatches" runat="server" 
         Text="HR Polling Results" Font-Size="Medium"></asp:Label>
         <p>&nbsp;</p>
            <dx:ASPxLabel ID="NoResultsText" runat="server"
                        Text="This HRID has never been polled. Please check the HRID or click 'Queue Refresh' to attempt to retrieve HR data."
                        Font-Size="Small">
                        </dx:ASPxLabel>

        </td>



        <!-- this column is for the same area as the above but for a different event reaction -->
        <td valign="top" style="width: 33%;">
          <asp:Label ID="ResultsMatchesLabel" runat="server" style="margin-left: 10px" 
          Text="HR Polling Results" Font-Size="Medium"></asp:Label>
          <p>&nbsp;</p>
         

            <dx:ASPxGridView ID="GridViewResults" runat="server" style="margin-left: 10px" 
                Width="201px" AutoGenerateColumns="False" Visible="False">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Date and Time of Poll Attempt" VisibleIndex="0">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Attempt" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Current Attempt Result" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                </Columns>
            
            </dx:ASPxGridView>

         

        </td>





        <td width="33%" valign="top">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        <asp:Label ID="RefreshLabel" runat="server" style="margin-left: 10px" 
         Text="Refresh" Font-Size="Medium" Font-Bold="True"></asp:Label>
  
            <br />
  
            <br />
            <br />
  
        <dx:ASPxButton ID="RefreshButton" runat="server" Text="Queue HR Refresh" 
                Height="26px" Width="143px" style="margin-left: 40px">
         </dx:ASPxButton> 
        </td>

    </tr>
</table>
<br />
    <br />
    <br />
    <br />
    <br />  
    <br />
    <br />
    <br />
    <br /> 
    <br />
    <br />
    <br />
    <br />  
    <br />
    <br />
    <br />
    <br /> 


</asp:Content>
