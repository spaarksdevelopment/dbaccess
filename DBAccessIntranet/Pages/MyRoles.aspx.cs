﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxGridView;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages
{
    public partial class MyRoles : BaseSecurityPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetGridDataSource();
        }

        void SetGridDataSource()
        {
			List<DBAccessController.DAL.MyRoles_Combined> list = Director.GetMyRolesCombined(this.CurrentUser.dbPeopleID);
                     
            if (list == null)
            {
            }
            else if (list.Count > 0)
            {
                ASPxGridViewAccessAreaRoles.DataSource = list.OrderBy(a=>a.RoleName);
                ASPxGridViewAccessAreaRoles.DataBind();
            }
            else
                AccesAreaPanel.Visible = false;
        }

		public string GetPopupDisplayText(object resourceName)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			string strResourceValue = string.Empty;
			switch ((string)resourceName)
			{
				case "HeaderText":
					{
						strResourceValue = resourceManager.GetString("HeaderText");
						break;
					}
				case "ContentHeadText":
					{
						strResourceValue = resourceManager.GetString("ContentHeadText");
						break;
					}
				case "ContentBodyText":
					{
						strResourceValue = resourceManager.GetString("ContentBodyText");
						break;
					}
				default:
					{
						break;
					}
			}
			return strResourceValue;
		}

        #region Event Handlers
        protected void ASPxGridViewAccessAreaRoles_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            SetGridDataSource();
        }

        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }
        #endregion

        #region Web Methods
        [WebMethod, ScriptMethod]
        public static bool DeleteDivisionRoleUser(int divisionRoleUserID)
        {
            return Director.DeleteDivisionRoleUser(divisionRoleUserID);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool DeletePassOfficeRoleUser(int passOfficeRoleUserID)
        {
            return Director.DeletePassOfficeRoleUser(passOfficeRoleUserID);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static int DeleteAARoleUser(int AccessAreaApproverId, int HRClassificationID, int corportateDivisionAccessAreaId, int roleID)
        {           
           
            var listAccessAreaApprovers = Director.GetMyAccessAreaApprovers(corportateDivisionAccessAreaId, roleID);

            // get and sort the list we are going to display locally (by classification then sort order)
            // show all people (-1 == iClassificationId) or the people for a specific classification (0 represents unclassified)
            listAccessAreaApprovers = listAccessAreaApprovers.Where(a => a.ClassificationID == HRClassificationID).ToList();

            if (listAccessAreaApprovers.Count >= 3)
            {
                //can go delete this person
                if (Director.DeleteAApprovers(AccessAreaApproverId))
                    return 1;
                else
                    return 0;
            }
            else
            {
                return 2;
            }       
        }
        #endregion
    }
}
