﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="true" CodeBehind="MyRequests.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages.MyRequests" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors"
	TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView"
	TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl"
	TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
	<script type="text/javascript">
		$(document).ready(function () {
			$('#AccessHelp').bind('click', function () {
				$("#AccessHelpInfo").slideToggle("slow");
			});
			$('#AccessHelpInfo').hide();
		});

		function ShowMyRequestDetails(masterRequestID) {

			var popupURLMyRequestDetails = '<%= Page.ResolveUrl("~/Pages/Popup/MyRequestDetailsPopup.aspx") %>' + "?masterRequestID=" + masterRequestID;
			popupMyRequestDetails.SetContentUrl(popupURLMyRequestDetails);
			popupMyRequestDetails.Show();
		}
	</script>
	<h1>
		<asp:Literal ID="MyRequestsTitle" runat="server" meta:resourceKey="MyRequestsTitle"></asp:Literal>		
	</h1>
	<div id="footerLower">
	</div>
	<asp:Literal ID="MyRequestsDetail" runat="server" meta:resourceKey="MyRequestsDetail"></asp:Literal>	
	<br />
	<br />
	<asp:Literal ID="ExportToExcel" runat="server" text="<%$ Resources:CommonResource, ClickToExportToExcel %>"></asp:Literal>
	&nbsp<asp:ImageButton 
		ID="btnUploadExcel" AlternateText="Click to export to Excel"
		SkinID="FileXLS" runat="server" OnClick="btnXlsxExport_Click"/>
	<div style="clear:both;"></div>
	<br />
	
	<dx:ASPxGridView ID="ASPxGridViewMyRequest" ClientInstanceName="gvMyRequest" runat="server"
		AutoGenerateColumns="False" KeyFieldName="RequestMasterID" ClientIDMode="AutoID">
		<Columns>
			<dx:GridViewDataImageColumn Caption="" FieldName="RequestStatus" ShowInCustomizationForm="True"
				VisibleIndex="1" meta:resourcekey="GridViewDataImageColumnStatus" >
				<DataItemTemplate>
                    <div style="width:100px">
                    <div style="float:left;margin:0px 2px 0px 3px;">
					<asp:Image ID="RedImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightRed.gif"
						runat="server"   Visible="<%# setImageRedVisible(Container.Text) %>"/>
					<asp:Image ID="AmberImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightAmber.gif"
						runat="server" Visible="<%# setImageAmberVisible(Container.Text) %>" />
					<asp:Image ID="GreenImage" ImageUrl="~/App_Themes/DBIntranet2010/images/TrafficLightGreen.gif"
						runat="server" Visible="<%# setImageGreenVisible(Container.Text) %>" />
                   </div>
                    <div style="float:left;">
					 <asp:Label ID="Data" Text="<%# (Container.Text) %>" runat="server"  />
                   </div>
                  </div>
				</DataItemTemplate>
			</dx:GridViewDataImageColumn>
			<dx:GridViewDataTextColumn Caption="" FieldName="RequestType" ShowInCustomizationForm="True"
				VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnRequestType">
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataDateColumn Caption="" FieldName="Created" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy"
				VisibleIndex="3"  meta:resourcekey="Created">
				<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
			</dx:GridViewDataDateColumn>
			<dx:GridViewDataTextColumn Caption="" FieldName="Persons" ShowInCustomizationForm="True"
				VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnPersons">
			</dx:GridViewDataTextColumn>
			<dx:GridViewDataTextColumn Caption="&nbsp;" ShowInCustomizationForm="True" CellStyle-HorizontalAlign="Right"
				VisibleIndex="5">
				<DataItemTemplate>
					<asp:HyperLink NavigateUrl="javascript:void(0);" ID="Action" Text="" 
						runat="server" OnClick='<%# "ShowMyRequestDetails("+ Eval("RequestMasterID") + ")" %>' meta:resourcekey="Details"></asp:HyperLink>
				</DataItemTemplate>
				<CellStyle HorizontalAlign="Right"></CellStyle>
			</dx:GridViewDataTextColumn>
		</Columns>
		<SettingsBehavior AllowFocusedRow="true"/>
	    <SettingsBehavior AllowFocusedRow="True"></SettingsBehavior>
		<SettingsPager PageSize="25"></SettingsPager>
        <SettingsText EmptyDataRow="<%$ Resources:CommonResource, EmptyDataRow %>"/>
	</dx:ASPxGridView>

	<dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridViewMyRequest">
	</dx:ASPxGridViewExporter>
	<br />
	<dx:ASPxPopupControl ID="PopupMyRequestDetails" ClientInstanceName="popupMyRequestDetails"
		runat="server" SkinID="PopupCustomSize" Width="1000px" Height="350px" 
		HeaderText="" meta:resourcekey="PopupMyRequestDetails">
	</dx:ASPxPopupControl>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<!--hidden fields-->
	<dx:ASPxHiddenField ID="ASPxHiddenFieldAccessDetails" ClientInstanceName="hfAccessDetails"
		runat="server">
	</dx:ASPxHiddenField>
</asp:Content>
