﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPages/DBIntranet.master"
	AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.DefaultPage" %>

<%@ Register TagPrefix="uc2" Src="~/Controls/NeedAssistance.ascx" TagName="NeedAssistance" %>
<%@ Register TagPrefix="uc3" Src="~/Controls/NewsTicker.ascx" TagName="Ticker" %>
<%@ Register TagPrefix="uc1" Src="~/Controls/NewsFeed.ascx" TagName="News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DBMainContent" runat="server">
<script language="javascript" type="text/javascript">
	$(document).ready(function () {
		$('#theMenu').css('visibility', 'hidden');
		$('#brdInner').css('visibility', 'hidden');

	});
</script>
	<h1 id="heading" runat="server" style="position:relative; top:-60px; z-index:2;"><asp:Literal ID="WelcomeTodbAccess" runat="server"></asp:Literal> </h1>

	<table width="100%" cellspacing="1" style="position: relative;">
		<tr style="vertical-align: top;">
			<td  align="left" width="500px">
				<div>
					<table style="border-style:none;">
						<tr class="noBottomMargin">
							<td style="padding-left: 25px;">
								<h3>
									<asp:Literal ID="dbAccessNews" runat="server"></asp:Literal>
								</h3>
							</td>
						</tr>
						<tr>
							<td align="left">
								<uc1:News ID="NewsFeed1" runat="server" />
							</td>
						</tr>
					</table>
				</div>
				<br />
			</td>
			<%--<td align="left" width="30%">
			</td>--%>
			<td style="text-align: left; width: 443px; padding-left: 5px; padding-top: 27px; position: relative;" width="70%">		
				<asp:HyperLink style="position: absolute; left: 40px; top: 327px; color: #0098DB; text-decoration: underline; font-size: 16px;" ID="LoginLink" NavigateUrl="<%#FormsAuthentication.LoginUrl%>" runat="server" Text="<%$ Resources:CommonResource, Login %>" />
				<asp:Image runat="server" ID="BadgeImage" ImageUrl="~/App_Themes/DBIntranet2010/images/Badge.png"></asp:Image>
			</td>
			<td align="right">
				<uc2:NeedAssistance ID="NeedAssistance1" runat="server" />
			</td>
		</tr>
	</table>
</asp:Content>
