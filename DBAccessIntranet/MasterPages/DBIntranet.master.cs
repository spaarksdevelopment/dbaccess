using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls;
//using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages;

namespace DBAccessIntranet.MasterPages
{
	public partial class DBIntranetMasterPage : System.Web.UI.MasterPage
	{
		public bool ShowFlags
		{
			set
			{
				_ShowFlags = value;
			}
		}

		private bool _ShowFlags = false;

		protected override void OnInit(EventArgs e)
		{
			Response.ContentType = "text/html; charset=utf-8";

			//this to update user language when the suer clicks on the language links
			string language = Request.Form["__EventTarget"];
			if (language == Convert.ToString(DBAccessEnums.Language.English) || language == Convert.ToString(DBAccessEnums.Language.German) || language == Convert.ToString(DBAccessEnums.Language.Italian))
				UpdateUser(language);

			base.OnInit(e);
		}

		protected override void OnLoad(EventArgs e)
		{
			Control HeaderControl = LoadControl("~/Controls/Header/" + Page.Theme + ".ascx");

			base.OnLoad(e);
			HeaderPlaceholder.Controls.Add(HeaderControl);

			if(HeaderControl is DBIntranet2010)
				((DBIntranet2010)HeaderControl).ShowFlags = _ShowFlags;

			Control FooterControl = LoadControl("~/Controls/Footer/" + Page.Theme + ".ascx");
			base.OnLoad(e);
			FooterPlaceHolder.Controls.Add(FooterControl);

			string AppTitle = ConfigurationManager.AppSettings["AppTitle"].ToString();
			string AppEnv = ConfigurationManager.AppSettings["AppEnv"].ToUpper();
			if (AppEnv != "LIVE")
			{
				AppTitle += " " + AppEnv;
			}
			Page.Title = AppTitle;
		}

		private void UpdateUser(string language)
		{
            Language currentLanguage = Director.GetLanguageEntityFromString(language);
			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			int currentLanguageID = Helper.SafeInt(currentLanguage.LanguageId);
			Director.UpdateUserLanguage(currentLanguageID, currentUser.dbPeopleID, currentUser.Email.ToString());
			currentUser.LanguageId = currentLanguageID;
		}
	}
}
