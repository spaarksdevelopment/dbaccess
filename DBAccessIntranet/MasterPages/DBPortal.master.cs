using System;
using System.Web;
using System.Web.UI;
using System.Configuration;

namespace DBAccessIntranet.MasterPages
{
	public partial class DBPortalMasterPage : System.Web.UI.MasterPage
	{
		protected override void OnInit (EventArgs e)
		{
			Response.ContentType = "text/html; charset=utf-8";

			base.OnInit (e);
		}

		protected override void OnLoad (EventArgs e)
		{
			Control HeaderControl = LoadControl ("~/Controls/Header/DBPortal.ascx");
			base.OnLoad (e);
			HeaderPlaceholder.Controls.Add (HeaderControl);

			Control FooterControl = LoadControl ("~/Controls/Footer/" + Page.Theme + ".ascx");
			base.OnLoad (e);
			FooterPlaceHolder.Controls.Add (FooterControl);

			string AppEnv = ConfigurationManager.AppSettings["AppEnv"].ToUpper ();
			string AppTitle = ConfigurationManager.AppSettings["AppTitle"].ToString ();

			if (AppEnv != "LIVE")
			{
				AppTitle += " " + AppEnv;
			}
			Page.Title = AppTitle;

		}
	}
}

