﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers
{
    public static class Utility
    {
        public static string EscapeJavaScriptSpecialCharacters(string input)
        {
            return input.Replace(@"\", @"\\").Replace(@"'", @"\'").Replace("\"", "\\\"");
        }
    }
}