﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers
{
	public static class SwapListItems
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="direction">-1 => Sort down the way ie 2=> 1
		///		1 => Sort up the way ie 1=>2
		/// </param>	
		/// <param name="indexOfItemToSwap">0 based index of item to swap
		/// </param>	
		public static void SwapSortOrders<T>(int direction, int indexOfItemToSwap, ref List<T> list) where T : ISortable 
		{
			if (list.Count <= 1 || indexOfItemToSwap<0)
				return;

			//Sort order can't get any higher
			if (direction > 0 && indexOfItemToSwap >= list.Count - 1)
				return;

			//Sort order can't go any lower
			if (direction < 0 && indexOfItemToSwap == 0)
				return;

			if (direction > 0)
			{
				T thisItem = list[indexOfItemToSwap];
				T nextItem = list[indexOfItemToSwap + 1];

				thisItem.SortOrder += 1;
				nextItem.SortOrder -= 1;
			}
			else
			{
				T thisItem = list[indexOfItemToSwap];
				T previousItem = list[indexOfItemToSwap - 1];

				thisItem.SortOrder -= 1;
				previousItem.SortOrder += 1;
			}

			list = list.OrderBy(a => a.SortOrder).ToList();
		}
	}
}