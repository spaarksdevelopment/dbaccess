﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers
{
	public static class ExcelExport
	{
		public static List<List<string>> GetRowData<T>(List<T> rows, List<string> columns)
		{
			List<List<string>> data = new List<List<string>>();
			
			foreach (var row in rows)
			{
				List<string> rowData = new List<string>();
				foreach (string colName in columns)
				{
					object value = row.GetType().GetProperty(colName).GetValue(row, null) ?? string.Empty;
					string valueString = value.ToString().Replace("\r\n", " ");

					rowData.Add(valueString.Trim());
				}
				data.Add(rowData);
			}

			return data;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="columnHeaders"></param>
		/// <param name="exportData"></param>
		/// <param name="strictCommas">If true then don't split up columns which contain a comma</param>
		public static void ExportToExcel(string filename, List<string> columnHeaders, List<List<string>> exportData, bool strictCommas = true)
		{
			HttpContext context = HttpContext.Current;

			context.Response.Clear();
			context.Response.Buffer = true;
			context.Response.ContentType = "text/plain";
			context.Response.AddHeader("Cache-Control", "no-store, no-cache");
			context.Response.AddHeader("Content-Disposition: attachment", string.Format("attachment; filename={0}.csv", filename));

			if (columnHeaders.Count > 0)
			{
				bool firstColumn = true;

				foreach (string column in columnHeaders)
				{
					if (!firstColumn)
						context.Response.Write(",");

					firstColumn = false;

					context.Response.Write(column);
				}

				context.Response.Write(Environment.NewLine);

				foreach (List<string> row in exportData)
				{
					firstColumn = true;

					foreach (string column in row)
					{
						if (!firstColumn)
							context.Response.Write(",");

						firstColumn = false;

						string columnValue = column;

						if (strictCommas)
						{
							//If value contains a comma, then don't add the equals sign because this parses the value into two columns
							columnValue = "\"" + column + "\"";

							if (!columnValue.Contains(","))
								columnValue = "=" + columnValue;
						}

						context.Response.Write(columnValue);
					}
					context.Response.Write(Environment.NewLine);
				}
			}

			context.Response.Flush();
			context.Response.End();
		}	
	}
}