﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spaarks.CustomMembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers
{
    internal static class GenericGUIHelpers
    {
        internal static int? GetIntFromSession(string key)
        {
            if (HttpContext.Current.Session[key] == null)
                return null;

            return (int?)HttpContext.Current.Session[key];
        }

        internal static string GetStringFromSession(string key)
        {
            if (HttpContext.Current.Session[key] == null)
                return null;

            return (string)HttpContext.Current.Session[key];
        }

        internal static void ClearValueFromSession(string key)
        {
            HttpContext.Current.Session[key] = null;
        }

        internal static int GetCurrentUserID()
        {
            DBAppUser currentUser = new DBSqlMembershipProvider().GetUser();
            return currentUser.dbPeopleID;
        }
    }
}