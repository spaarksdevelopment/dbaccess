//General functions -------------------------------------------------------------------/
// Used to serialise 2 or more paramters when passing parms back via a grid custom callback.
function CallbackParamsToString(params) {
    var result = "";
    var $ = '|';
    for (var paramName in params) {
        var strValue = params[paramName].toString();
        result += paramName;
        result += $;
        result += strValue.length;
        result += $;
        result += strValue;
    }
    return result;
}

//function to open window
function openWindow(sURL, bSmall, bIndex) {

    if (bSmall) {
        sSize = "width=400, height=270";
    } else {
        sSize = "top=0, left=0, width=790, height=570";
    }
    var oWin = window.open(sURL, "popup_window" + bSmall + bIndex, sSize + ", toolbars=no, menubar=no, location=no, resizable=yes, scrollbars=yes");
    oWin.focus();
    return oWin;
}

//function that returns today's date
function today() {
    return "".toDate();
}

//functions to get date differences
function getNoWeeks(d1, d2) {

    var n = 0;
    while (d1 <= d2) {
        if (d1.getDay() != 0 && d1.getDay() != 6) n = n + 0.2;
        d1.setDate(d1.getDate() + 1);
    }

    return (n);
}

function getNoDays(d1, d2) {
    return (((d2 - d1) / 86400000) + 1);
}

//mod function

function mod(n1, n2) {
    var n = (n1 / n2);
    return (n1 - (Math.floor(n.roundTo(4)) * n2));
}

function checkEmail(src) {
    return src.isValidEmail();
}

function getCheckedIndex(objRadio) {
    var retVal = -1;
    for (i = objRadio.length - 1; i > -1; i--) {
        if (objRadio[i].checked) {
            retVal = i;
        }
    }
    return retVal;
}



/* Methods for handling strings/numbers/dates ------------------------------------------/

For any function which accepts parameter b - true gives a warning

Method							Parameter						Returns
----------------------------------------------------------------------------------------
string.trim()													trimmed string
string.isNumber()												true/false
string.isNumeric()												true/false
string.toInt(b)													integer
string.toFloat(b)												float
string.toRoundedFloat(x)		number of decimal places		float, x decimal places
string.toCurrency()				(ignores leading � in string)	float, 2 decimal places 
string.isValidEmail(b)											true/false
string.isValidNINumber()										true/false
string.isValidDate()										    true/false
string.toDate(b)												date object
string.getMonthNum()											0-11 for jan-dec
date.getMonthName(x)			true for long name				Jan-Dec or January-December
number.toInt()													integer
number.toRoundedFloat(x)		number of decimal places		float, x decimal places
number.toCurrency()												float, 2 decimal places 
number.displayCurrency()										�x.xx
number.roundTo(x)				number of decimal places		float, x decimal places
date.displayDate()												dd-mmm-yy
date.displayNumDate()											dd/mm/yy
date.addDays(x)					number of days to add			date object
date.addWeeks(x)				number of Weeks to add			date object
date.addMonths(x)				number of Months to add			date object
date.addYears(x)				number of Years to add			date object
-------------------------------------------------------------------------------------*/
var bWarningGiven = false;

String.prototype.trim = function() {
    var re = /^\s*|\s*$/g;
    return this.replace(re, "");
}

String.prototype.stripSpaces = function() {
    var re = /\s*/g;
    return this.replace(re, "");
}

//Checks that string contains a valid number
//Only allows figures, negative sign and one decimal place
String.prototype.isNumber = function() {
    var re = /^[\-]?[0-9]+[.]?[0-9]*$/;
    return re.test(this);
}

//Checks that string contains numeric content only
//allows ., -, and / any number of times
String.prototype.isNumeric = function() {
    var re = /^[0-9\.\-\/]+$/;
    return re.test(this);
}

//Safely converts to an integer
String.prototype.toBool = function (bWarn) {
    bWarningGiven = false; //reset the flag
    var bool = false;
    if (this.isNumber()) {
        if ((this > 0) || (this < 0))
            bool = true;
    } else {
        var lc = this.toLowerCase();
        if ((lc == 'true') || (lc == 'yes'))
            bool = true;
        else if ((lc == 'false') || (lc == 'no'))
            bool = false;
        else if (bWarn) {
            alert("Not a valid Boolean");
            bWarningGiven = true;
        }
    }
    return bool;
}


//Safely converts to an integer
String.prototype.toInt = function(bWarn) {
    bWarningGiven = false; //reset the flag
    if (!this.isNumber()) {
        if (bWarn) {
            alert("Not a valid Integer");
            bWarningGiven = true;
        }
        return 0;
    }
    return Math.round(parseFloat(this));
}

//Safely converts to a floating point number
String.prototype.toFloat = function(bWarn) {
    var s = this;
    bWarningGiven = false; //reset the flag
    if (s.charAt(0) == ".") s = "0" + s;
    if (!s.isNumber()) {
        if (bWarn) {
            alert("Not a valid Number");
            bWarningGiven = true;
        }
        return 0;
    }
    return parseFloat(s);
}

//Safely converts to a floating point number, rounded to the specified number of places
String.prototype.toRoundedFloat = function(x) {
    return this.toFloat().roundTo(x);
}

//Safely converts to a floating point number, rounded to 2 decimal places, ignores leading � symbol
String.prototype.toCurrency = function() {
    var s = (this.charAt(0) == "�") ? this.substr(1) : this;
    return s.toRoundedFloat(2);
}

String.prototype.isValidEmail = function(bWarn) {
    bWarningGiven = false; //reset the flag
    var re = /^[A-Za-z0-9._%-]+@[\w]+\.[a-z]{2,4}[\.]?[a-z]{0,3}$/i;
    var bValid = re.test(this);
    if ((!bValid) && (bWarn)) {
        alert("Please enter a valid E-mail address");
    }
    return bValid;
}

String.prototype.isValidNINumber = function() {
    var re = /^[a-z]{2}\d{6}[a-d]$/i;
    return re.test(this);
}

String.prototype.isValidDate = function() {
    var aSplitters = new Array("/", ".", "-", " ");
    var a = null;
    var sDateString = this.trim();
    for (var i in aSplitters) {
        if (sDateString.indexOf(aSplitters[i]) != -1) a = sDateString.split(aSplitters[i])
    }
    if (a == null) return false;
    switch (a.length) {
        case 2:
            a[2] = (new Date).getFullYear().toString();
            break;
        case 3:
            if (a[2] == "") a[2] = (new Date).getFullYear().toString();
            if (!a[2].isNumber()) return exitFunction();
            break;
        default:
            return false;
    }

    if (!a[0].isNumber()) {
        var aDateEnds = new Array("ST", "ND", "RD", "TH");
        for (var i in aDateEnds) {
            var iPos = a[0].toUpperCase().indexOf(aDateEnds[i]);
            if (iPos != -1) a[0] = a[0].substr(0, iPos);
        }
        if (!a[0].isNumber()) return false;
    }
    a[0] = a[0].toInt();
    if ((a[0] < 1) || (a[0] > 31)) return false;

    a[1] = (a[1].isNumber()) ? a[1].toInt() - 1 : a[1].getMonthNum();
    if (a[1] == null) return false;

    a[2] = a[2].toInt();
    if (a[2] < 100) a[2] += ((a[2] < 20) ? 2000 : 1900);

    try{
        var NewDate = new Date(a[2], a[1], a[0]);
    }
    catch(err)
    {
        return false;
    }

    return true;

}

//Safely converts to a date
//order must be d/m/y
//can accept /, ., - or a space between date parts
//can accept 1 or 1st for day
//can accept number, short month name or long month name
//for short years, assumes 20th century for 50-99
//assumes current year if year is omitted
String.prototype.toDate = function(bWarn) {
    bWarningGiven = false; //reset the flag

    var aSplitters = new Array("/", ".", "-", " ");
    var a = null;
    var sDateString = this.trim();
    for (var i in aSplitters) {
        if (sDateString.indexOf(aSplitters[i]) != -1) a = sDateString.split(aSplitters[i])
    }
    if (a == null) return exitFunction();
    switch (a.length) {
        case 2:
            a[2] = (new Date).getFullYear().toString();
            break;
        case 3:
            if (a[2] == "") a[2] = (new Date).getFullYear().toString();
            if (!a[2].isNumber()) return exitFunction();
            break;
        default:
            return exitFunction();
    }

    if (!a[0].isNumber()) {
        var aDateEnds = new Array("ST", "ND", "RD", "TH");
        for (var i in aDateEnds) {
            var iPos = a[0].toUpperCase().indexOf(aDateEnds[i]);
            if (iPos != -1) a[0] = a[0].substr(0, iPos);
        }
        if (!a[0].isNumber()) return exitFunction();
    }
    a[0] = a[0].toInt();
    if ((a[0] < 1) || (a[0] > 31)) return exitFunction();

    a[1] = (a[1].isNumber()) ? a[1].toInt() - 1 : a[1].getMonthNum();
    if (a[1] == null) return exitFunction();

    a[2] = a[2].toInt();
    if (a[2] < 100) a[2] += ((a[2] < 20) ? 2000 : 1900);

    return new Date(a[2], a[1], a[0]);

    function exitFunction() {
        if (bWarn) {
            alert("The value '" + sDateString + "' is not in a valid dd/Mmm/yyyy date format.\n\r\r");
            bWarningGiven = true;
            return new Date(1);
        } else {
            return new Date(1);
        }
    }
}

//returns month number from month name
String.prototype.getMonthNum = function() {
    switch (this.substr(0, 3).toUpperCase()) {
        case "JAN":
            return 0;
            break;
        case "FEB":
            return 1;
            break;
        case "MAR":
            return 2;
            break;
        case "APR":
            return 3;
            break;
        case "MAY":
            return 4;
            break;
        case "JUN":
            return 5;
            break;
        case "JUL":
            return 6;
            break;
        case "AUG":
            return 7;
            break;
        case "SEP":
            return 8;
            break;
        case "OCT":
            return 9;
            break;
        case "NOV":
            return 10;
            break;
        case "DEC":
            return 11;
            break;
        default:
            return null;
    }
}

//returns month name from date, long format if parameter specified
Date.prototype.getMonthName = function(bLong) {
    switch (this.getMonth()) {
        case 0:
            var m = "January";
            break;
        case 1:
            var m = "February";
            break;
        case 2:
            var m = "March";
            break;
        case 3:
            var m = "April";
            break;
        case 4:
            var m = "May";
            break;
        case 5:
            var m = "June";
            break;
        case 6:
            var m = "July";
            break;
        case 7:
            var m = "August";
            break;
        case 8:
            var m = "September";
            break;
        case 9:
            var m = "October";
            break;
        case 10:
            var m = "November";
            break;
        case 11:
            var m = "December";
            break;
    }
    return (bLong) ? m : m.substr(0, 3);
}

//returns an integer for any number
Number.prototype.toInt = function() {
    return Math.round(this);
}

//returns a floating point number, rounded to the specified number of places
Number.prototype.toRoundedFloat = function(x) {
    return this.roundTo(x);
}

//returns a floating point number, rounded to 2 decimal places
Number.prototype.toCurrency = function() {
    return this.roundTo(2);
}

//displays currency with � and 2 decimal places showing
Number.prototype.displayCurrency = function() {
    var s = this.toString();
    var i = s.indexOf(".");
    switch (i) {
        case -1:
            s += ".00";
            break;
        case 0:
            s = "0" + s;
            break;
        case (s.length - 2):
            s += "0";
            break;
        case (s.length - 3):
            break;
        default:
            s = s.substring(0, i + 2);
    }

    //IE6 cannot display � for some reason!
    //return "�"+s;
    return s;
}

//rounds a number to a specified number of decimal places
Number.prototype.roundTo = function(x) {
    var f = Math.pow(10, x);
    var i = this * f;
    return Math.round(i) / f;
}

//displays a date in short date format, dd-mmm-yyyy
Date.prototype.displayDate = function() {
    return (this.getUTCMilliseconds() == 1) ? "" : pad(this.getDate()) + "-" + this.getMonthName() + "-" + this.getFullYear();
}

//displays a date in numeric short date format, dd/mm/yyyy
Date.prototype.displayNumDate = function() {
    return (this.getUTCMilliseconds() == 1) ? "" : pad(this.getDate()) + "-" + pad(this.getMonth() + 1) + "-" + this.getFullYear();
}

function pad(i) {
    var s = i.toString();
    return (s.length == 1) ? "0" + s : s;
}
//adds a number of days onto a date
Date.prototype.addDays = function(x) {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + x);
}

//adds a number of weeks onto a date
Date.prototype.addWeeks = function(x) {
    var numDays = (parseInt(x) * 7) - 1;
    if (x - parseInt(x) > 0) {
        var partWeeks = (x - parseInt(x)) * 5;
        numDays = numDays + partWeeks;
    }
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + (numDays));
}

Date.prototype.adjustWeekend = function() {
    var d = this;
    switch (d.getDay()) {
        case 0:
            d = d.addDays(-2);
            break;
        case 6:
            d = d.addDays(-1);
    }
    return d;
}

//adds a number of months onto a date
Date.prototype.addMonths = function(x) {
    return new Date(this.getFullYear(), this.getMonth() + x, this.getDate());
}

//adds a number of years onto a date
Date.prototype.addYears = function(x) {
    return new Date(this.getFullYear() + x, this.getMonth(), this.getDate());
}