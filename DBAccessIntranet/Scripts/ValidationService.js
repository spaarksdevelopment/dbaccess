﻿//---------------------------------------------------------------------------------------------//
// This js file is for methods which access the validation.asmx service
//---------------------------------------------------------------------------------------------//
//document.write('<scr' + 'ipt type="text/javascript" src="json2.js" ></scr' + 'ipt>');

//HACK - ServiceURL does not resolve correctly for pages at the second level down so need to change it
function SetServicesFolderPath(path) {
    ServicesFolder = path;
}

function GetPersonIdFromIdentifier(idstring) {
     var personId;

     var ServiceURL = ".." + ServicesFolder + 'Validation.asmx/GetPersonIdFromIdentifier';
   // var params = JSON.stringify({'Identifier':'Chris'});
    //var params = '"'Identifier'":"'Chris'"';
    //"{'Identifier':'Chris'}");
    var InputString = '{"Identifier":"' + idstring + '"}'
    //alert(idstring);
    var result = GetSynchronousResponse(ServiceURL, InputString);

//    var t = "Hi";
//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: ServiceURL,
//        data: JSON.stringify(InputString),
//        dataType: "json",
//        success: function (result) {
//            alert(result.d);
//            personId = result.d;
//        },
//        error: function (result) {
//            alert("error " + result.responseText); 
//            alert(result.statusText); } 
//    });
  
    
    result = eval('(' + result + ')');
    personId = result.d;

    return personId;
}

function GetPersonBadgeCount(personId) {
   
    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetPersonBadgeCount';
    var InputString = '{"dbPeopleId":"' + personId + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetReplacementBadgeCount(requestId) {
   
    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetReplacementBadgeCount';
    var InputString ='{"RequestMasterId":"' + requestId + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetRequestPersonCount(requestId) {
   
    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetRequestPersonCount';
    var InputString ='{"RequestMasterId":"' + requestId + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetAccessRequestPersonsWithoutBadgeCount(requestId) {
   
    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetAccessRequestPersonsWithoutBadgeCount';
    var InputString ='{"RequestMasterId":"' + requestId + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetRequestAccessCount(requestId) {

    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetRequestAccessCount';
    var InputString = '{"RequestMasterId":"' + requestId + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetRequestPersonPassOfficeId(requestId, personId) {
    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetRequestPersonPassOfficeId';
    var InputString = '{"RequestMasterId":' + requestId + ', "dbPeopleId":' + personId + '}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}

function GetUBRTitle(ubr) {

    //debugger;
    var ServiceURL = ServicesFolder + 'Validation.asmx/GetUBRTitle';
    var InputString = '{"UBRCode":"' + ubr + '"}';
    var result = GetSynchronousResponse(ServiceURL, InputString);
    result = eval('(' + result + ')');
    return result.d;
}