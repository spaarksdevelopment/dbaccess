﻿/* XML Related functions -----------------------------------------------------*/

    function parseXML(xml) {
        if (window.ActiveXObject && window.GetObject) {
            var dom = new ActiveXObject('Microsoft.XMLDOM');
            dom.loadXML(xml);
            return dom;
        }
        if (window.DOMParser)
            return new DOMParser().parseFromString(xml, 'text/xml');
        throw new Error('No XML parser available');
    }

/* Javascript Web Service handling ------------------------------------------*/

    function CreateXMLHttpObject() {
        var xmlHttp = null;
        if (window.XMLHttpRequest)
            xmlHttp = new XMLHttpRequest();
        else if (window.ActiveXObject) {
            if (new ActiveXObject("Microsoft.XMLHTTP"))
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            else
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        return xmlHttp;
    }

    function GetSynchronousResponse(url, postData) {

        var xmlHttp = CreateXMLHttpObject();
        // to ensure a non-cached version of response
        url = url + "?rnd=" + Math.random();

        xmlHttp.open("POST", url, false); //false means synchronous
        xmlHttp.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xmlHttp.send(postData);
        var responseText = xmlHttp.responseText;
        return responseText;
    }

   