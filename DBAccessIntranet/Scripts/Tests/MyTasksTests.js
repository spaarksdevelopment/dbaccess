﻿module("MyTasksTests", function() {
    // Add element to dom
    //$("#qunit_fixture").append("<div id='#thing'>hello</div>");
});

/// <reference path="../MyTasks.js" />
test("isMifareValidTooLong", function () {
    var txt = "1234567890123";
    var res = IsMifareValid(txt);

    equal(res, false, "should be false");
});

/// <reference path="../MyTasks.js" />
test("isMifareValidTooShort", function () {
    var txt = "12345678901";
    var res = IsMifareValid(txt);

    equal(res, false, "should be false");
});

/// <reference path="../MyTasks.js" />
/// <reference path="../../utils.js" />
test("isMifareValidNotNumeric", function () {
    var txt = "12345678901a";
    var res = IsMifareValid(txt);

    equal(res, false, "should be false");
});

/// <reference path="../MyTasks.js" />
/// <reference path="../../utils.js" />
test("isMifareValidValidNumber", function () {
    var txt = "123456789012";
    var res = IsMifareValid(txt);

    equal(res, true, "should be true");
});