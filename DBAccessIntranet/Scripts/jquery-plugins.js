﻿/* --------------------------------------------------------------------------*/
//                  JQUERY Plugins
//
// Any small Jquery plugins can be added here rather than put in their own file
/* --------------------------------------------------------------------------*/ 

    
    jQuery.parseXML = function(xml) {
        return jQuery(parseXML(xml));
    };

    jQuery.preloadImages = function() {
        for (var i = 0; i < arguments.length; i++) {
            jQuery("<img>").attr("src", arguments[i]);
        }
    }