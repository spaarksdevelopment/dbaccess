﻿$(function () {
    $('#Details').hide();

    $("#popupTaskDetails").dialog({
        autoOpen: false,
        title: 'Task Details',
        width: '400',
        height: '500',
        modal: false
    });
});

function OnSubmitTasks() {
    if (!hfTasks.Get("Success")) {
        ShowAlertTemplatesPopup(myTasksErrorTask, myTasksProcessTask);
        return;
    }
    hfTasks.Clear();
    Tasks.PerformCallback();
    var pending = 1;
    UpdateStatusTasks.PerformCallback(pending);
}

function SaveReason() {
    var txt = memoReason.GetText();

    if (txt == '') {
        ShowAlertTemplatesPopup(myTasksRejectionReason, myTasksReason);
        return;
    }

    var taskId = hfInputs.Get("CurrentTaskId");
    hfInputs.Set("Input_" + taskId, txt);
    memoReason.SetText("");
    popupRejectReason.Hide();
}

function CancelPopup() {
    memoReason.SetText("");
    popupRejectReason.Hide();

    txtInput.SetText("");
    popupInput.Hide();

    DeselectTheButton();

    RemoveTaskFromHiddenField();
    RemoveTaskRadioFromHiddenField();
}

function RemoveTaskFromHiddenField() {
    var taskId = GetCurrentTaskID();
    hfTasks.Remove("task_" + taskId);
}

function RemoveTaskRadioFromHiddenField() {
    hfInputs.Remove("CurrentTaskId");
    hfInputsTaskRadioButton.Remove("CurrentTaskRadioButton");
}

function ConfirmTasks(s, e) {
    popupLoading.Show();

    RemoveTaskRadioFromHiddenField();
    hfTasks.PerformCallback();
}

function PerformTaskStatusCallback() {
    var Taskstatusid = MyTaskStatus.GetValue();
    Tasks.PerformCallback();
    UpdateStatusTasks.PerformCallback(Taskstatusid);
}

var taskStatus;
var selectedTaskId;

var position;

function ShowDetails(oLink, taskId) {
    position = $(oLink).position();

    GetTaskDetails(taskId);
}

function SaveInput() {
    var sInputDesc = hfInputs.Get("InputDesc");

    var txt = txtInput.GetText();

    if (txt == '') {
        var headerText = myTasksDesc + "  " + sInputDesc + ".";
        ShowAlertTemplatesPopup(headerText, myTasksInputDesc);
        return;
    }

    var isCorrect = true;

    if (sInputDesc == "MiFare Number") {

        isCorrect = IsMifareValid(txt);
        IsMiFareUnique(txt);

        if (!isCorrect) {
            ShowAlertTemplatesPopup(myTasksInvalidMiFare, myTasksInputDesc);
            return;
        }
    }

    if (isCorrect) {
        SetTaskIDHiddenValue(txt);
        HideInputPopup();
    }
}

function ShowAlertTemplatesPopup(content, header) {
    popupAlertTemplateContent.SetText(content); 
    popupAlertTemplate.SetHeaderText(header); 
    popupAlertTemplate.Show();
}

function SetTaskIDHiddenValue(txt) {
    var taskId = hfInputs.Get("CurrentTaskId");
    hfInputs.Set("Input_" + taskId, txt);
}

function HideInputPopup() {
    txtInput.SetText("");
    popupInput.Hide();
}

function IsMifareValid(txt){
    return (txt.length == 12) && txt.isNumeric();
}

var clickedDetailsLink;

function ShowDetails(oLink, taskId) {
    clickedDetailsLink = oLink;

    GetTaskDetails(taskId);
}

function GetTaskDetails(taskId) {
    PageMethods.GetTaskDetails(taskId, OnGetTaskDetails);
}

function OnGetTaskDetails(result) {
    if (result == "")
        $('#taskDetailsPopupContent').html("<%=MyTasksNoTaskDetails.Text %>");

    ShowTaskDetails(result);
}

function ShowTaskDetails(htmlDetails) {
    $('#popupTaskDetails').html(htmlDetails);
    $('#popupTaskDetails').dialog({
        position: { my: "left top", at: "left bottom", of: clickedDetailsLink }
    });

    $('#popupTaskDetails').dialog('open');
}

function IsMiFareUnique(mfNum) {
    var mfnValues = hfMFN.Get("MFNValues");

    if (mfnValues == null) {
        mfnValues = [mfNum];
        hfMFN.Set("MFNValues", mfnValues);
    }
    else {
        CheckIfMiFareHasBeenUsedInSession(mfnValues, mfNum);
    }

    PageMethods.GetMiFareNumber(mfNum, OnIsMiFareUnique);
}

function CheckIfMiFareHasBeenUsedInSession(currentSessionMifares, thisMifare) {
    var found = false;
    for (i = 0; i < currentSessionMifares.length; i++) {
        if (currentSessionMifares[i] == thisMifare) {
            DeselectCurrentTaskRadioButton(myTasksUsedMiFare)   
            found = true;
            break;
        }
    }

    if (!found) {
        currentSessionMifares.push(thisMifare);
        hfMFN.Set("MFNValues", currentSessionMifares);
    }
}

function OnIsMiFareUnique(res) {
    if (res != "")
        DeselectCurrentTaskRadioButton(res);  //MiFare Number already exists.
}

function DeselectCurrentTaskRadioButton(message) {
    ShowAlertTemplatesPopup(message, "Input");

    DeselectTheButton();

    RemoveTaskFromHiddenField();
    RemoveTaskRadioFromHiddenField();
}

function GetCurrentTaskRadioButton() {
    return hfInputsTaskRadioButton.Get("CurrentTaskRadioButton");
}

function GetCurrentTaskID(){
    return hfInputs.Get("CurrentTaskId");
}

function DeselectTheButton() {
    var taskRadioButton = GetCurrentTaskRadioButton();
    var ctl_rblTaskAction = ASPxClientRadioButtonList.Cast(taskRadioButton);
    if (null != ctl_rblTaskAction) ctl_rblTaskAction.SetSelectedIndex(-1);
}

function SelectTask(s, taskId, taskTypeId, value) {
    var sID = s.name;

    hfTasks.Set("task_" + taskId, value);
    hfInputs.Set("CurrentTaskId", taskId);
    hfInputsTaskRadioButton.Set("CurrentTaskRadioButton", null);
    if (value == '2') {

        popupRejectReason.ShowAtElementByID(sID);
        memoReason.maxLength = 500;
        memoReason.SetFocus();
        return;
    }

    if ((value == '3') && (taskTypeId == 1)) {
        hfInputsTaskRadioButton.Set("CurrentTaskRadioButton", s);
        hfInputs.Set("InputDesc", "MiFare Number");

        $('#spanInputDescription').html(strInputDescription);
        popupInput.ShowAtElementByID(sID);
        txtInput.SetFocus();
    }
}