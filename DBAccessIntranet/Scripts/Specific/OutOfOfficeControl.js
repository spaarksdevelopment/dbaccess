﻿$(function () {
    $("#tabs").tabs({
        select: function (event, ui) {
        }
    });
    displaySort();
    var LeaveDate = (ooEnableFrom.GetValue());
    var ReturnDate = (ooEnableUntil.GetValue());
    hfOutOfOfficeLeaveFrom.Set("LeaveDate", LeaveDate);
    hfOutOfOfficeLeaveUntil.Set("ReturnDate", ReturnDate);

});

function checkRequest() {
    popupRemoveOutOfOffice.Show();
}


function OnSetOutOfOffice(result) {
   
    if (!result) {
        hfOutOfOfficeEnabled.Set("Enabled", "false");
        popupAddOutOfOfficeError.Show();
    }
    else {
       
        //popupAddOutOfOfficeSuccess.Show();
        var LeaveDate = hfOutOfOfficeLeaveFrom.Get("LeaveDate");
        var ReturnDate = hfOutOfOfficeLeaveUntil.Get("ReturnDate");
        
        ooEnableFromDisable.SetDate(LeaveDate);
        ooEnableReturnDisable.SetDate(ReturnDate);
        hfOutOfOfficeEnabled.Set("Enabled", "true");

        $("#tabs").tabs("select", 1);
        $("#OOFO").show();
        $("#OOF").hide();  
    }
}

function displaySort() {
    var enabled = hfOutOfOfficeEnabled.Get("Enabled");
    if (enabled == "false") {
        $("#tabs").tabs("select", 0);
        $("#OOFO").hide();
    }
    else {
        $("#tabs").tabs("select", 1);
        $("#OOF").hide();
    }
}

function OnDeleteOutOfOffice(result) {
   
    if (!result) {
        hfOutOfOfficeEnabled.Set("Enabled", "true");
        popupRemoveOutOfOfficeError.Show();
    }
    else {
        //popupRemoveOutOfOfficeSuccess.Show();
        $("#tabs").tabs("select", 0);
        $("#OOFO").hide();
        $("#OOF").show();
    }
}

function resetOutOfOffice() {
    popupRemoveOutOfOffice.Hide();
    var LeaveDate = hfOutOfOfficeLeaveFrom.Get("LeaveDate");
    var ReturnDate = hfOutOfOfficeLeaveUntil.Get("ReturnDate");
    ooEnableFromDisable.SetValue(LeaveDate);
    ooEnableReturnDisable.SetValue(ReturnDate);
}

function ValidateDates() {

    var LeaveDate = (ooEnableFrom.GetValue());
    var ReturnDate = (ooEnableUntil.GetValue());

    if (LeaveDate == "" || ReturnDate == "")
    {
    	popupOutOfOfficeDateError.Show();
    	ooEnableFrom.SetValue(LeaveDate);
    	ooEnableUntil.SetValue(ReturnDate);
    	//Show popup with date error.
    }
    else if (LeaveDate == null || ReturnDate == null)
    {
    	popupOutOfOfficeDateError.Show();
    	ooEnableFrom.SetValue(LeaveDate);
    	ooEnableUntil.SetValue(ReturnDate);
    }
    else if (LeaveDate > ReturnDate)
    {
    	popupAlertTemplate.SetHeaderText("Invalid Dates");
    	popupAlertTemplateContent.SetText("Start date canot be greater than end date");
    	popupAlertTemplate.Show();
    }
    else
    {
    	//show the normal add officepopup
    	popupAddOutOfOffice.Show();
    	ooEnableFrom.SetValue(LeaveDate);
    	ooEnableUntil.SetValue(ReturnDate);
    	hfOutOfOfficeLeaveFrom.Set("LeaveDate", LeaveDate);
    	hfOutOfOfficeLeaveUntil.Set("ReturnDate", ReturnDate);
    }
}

function UpdateToDate() {
    try {
        var currentToDate = new Date(ooEnableUntil.GetDate());
        var currentFromDate = new Date(ooEnableFrom.GetDate());

        if (currentToDate < currentFromDate)
            ooEnableUntil.SetDate(currentFromDate);
    }
    catch (ex) { ; }
}

