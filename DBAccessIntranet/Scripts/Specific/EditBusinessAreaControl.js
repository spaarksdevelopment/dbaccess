﻿function HiddenExArrayData(result)
{
	var substr = result.split('|');
	var values = hfAccessRequestExArray.Get("AccessRequestExArray");
	if (values.length == 0)
	{
		values[0] = substr[1] + "-" + substr[2];
	}
	else
	{
		var index = values.length;
		values[index] = substr[1] + "-" + substr[2];
	}
	
	hfAccessRequestExArray.Set("AccessRequestExArray", values);
}


function updateBusinessArea()
{
	var baID = hdBusinessArea.Get("BusinessAreaID");
	var baname = txtBusinessAreaName.GetValue();
	var enabled = rbEnabled.GetSelectedItem().value;
	//var appType = rbApprovalType.GetSelectedItem().value;
	//set the app Type to 3 by deafult, as per the document
	var appType = 3;
	//var appNum = rbNumApprovals.GetSelectedItem().value;
	//as per discussion with martin, default to single
	var appNum = 1
	

	PageMethods.UpdateBusinessArea(enabled.toBool(false), baname, appType, appNum, baID, OnUpdateBusinessArea);
}

function OnUpdateBusinessArea(result)
{
	var arrayValues = result.split("-");

	popupAlertTemplateContent.SetText(arrayValues[1].toString());
	popupAlertTemplate.SetHeaderText(arrayValues[2].toString());
	popupAlertTemplate.Show();

	if (arrayValues[0].toString() != "0" && arrayValues[0].toString() == "-1")
	{
		//HACK: update business area list on calling page
		if (BusinessAreas != null) BusinessAreas.PerformCallback(result);
	}
	else 
	if (arrayValues[0].toString() == "-1")
	{
		rbEnabled.SetSelectedIndex(1);
	}
}

function HideBusinessAreaDetail() 
{
	$('#BusinessAreaContent').css('display', 'none');
	$('#BusinessAreasContent').css('display', '');

	$('#BusinessAreasHeader').css('display', '');
	$('#BusinessAreaDetailHeader').css('display', 'none');

	//Need to call this or event handlers are lost on the grid
	//Particularly if you click on a row no event is fired.
	PerformBusinessAreasCallback("Cancel");
}




function SortExApprover(direction, dbPeopleID) {
    var classificationID = classifications.GetValue();
    PageMethods.SortBusinessAreaApprover(direction, dbPeopleID, classificationID, OnSortExApprover);
}

function OnSortExApprover(result) {
    if (!result) {
        popupAlertTemplateContent.SetText("Approver sort order could not be changed.");
        popupAlertTemplate.SetHeaderText("Approver");
        popupAlertTemplate.Show();
    }
    else {
        var baID = hdBusinessArea.Get("BusinessAreaID");   
        var classificationID = classifications.GetSelectedItem().value;
        var type = "Sort";
        classificationApprovers.PerformCallback(baID + "|" + classificationID +'|' + type + '|none');
    }
}


function AddDivisionAdmin()
{
	var idstring = $get("PersonSelectorNameTextBox3").value;
	var personID = 0;

	if (idstring.length > 0)
	{
	    personID = GetPersonIdFromIdentifier(idstring);
	    hfPeoleDetail.Set("hfPeoleDetail", personID);
	}

	if (personID == 0)
	{
		var arrayValues = hfSetValuesForValidation.Get("ValuesForValidation").split("|");
		popupAlertTemplateContent.SetText(arrayValues[1].toString());
		popupAlertTemplate.SetHeaderText(arrayValues[0].toString());
		popupAlertTemplate.Show();
		return;
	}
	
    var iRequestMasterId = hfRequestDetail.Get("RequestMasterID_DA");
    if (iRequestMasterId == null) iRequestMasterId = 0;

    var divisionId = hdDivision.Get("DivisionID");
    var businessAreaId = hdBusinessArea.Get("BusinessAreaID");
    PageMethods.AddDivisionAdmin(personID, iRequestMasterId, divisionId, businessAreaId, OnAddDivisionAdmin);
}

function OnAddDivisionAdmin(result)
{
	var arrayValues = result.split("-");
	var substr = arrayValues[0].split('|');

	if ("" != arrayValues[1].toString())
	{
		popupAlertTemplateContent.SetText(arrayValues[1].toString());
		popupAlertTemplate.SetHeaderText(arrayValues[2].toString());
		popupAlertTemplate.Show();
		return;
	}

	hfRequestDetail.Set("RequestMasterID", substr[0].toString());
	HiddenArrayDataDA(result.replace('-', ''));

	$get("PersonSelectorNameTextBox3").value = '';

	setDirty(true);

	// we need to reload the grid
	gvDivisionAdmins.PerformCallback();
}

function HiddenArrayDataDA(result)
{
	var substr = result.split('|');
	var values = hfAccessRequestArrayDA.Get("AccessRequestArrayDA");
	if (values.length == 0)
	{
		values[0] = substr[1] + "-" + substr[2];
	}
	else
	{
		var index = values.length;
		values[index] = substr[1] + "-" + substr[2];
	}

	hfAccessRequestArrayDA.Set("AccessRequestArrayDA", values);
}

function SubmitDivisionAdministratorChanges() {
	popupLoading.Show();

	var iRequestMasterId = hfRequestDetail.Get("RequestMasterID");
	if (iRequestMasterId == null) iRequestMasterId = 0;

	var divisionID = hdDivision.Get("DivisionID");
	var dbpeopleId = hfPeoleDetail.Get("hfPeoleDetail");

	if (dbpeopleId == null) dbpeopleId = 0;

	PageMethods.SubmitDivisionAdministratorChanges(divisionID, iRequestMasterId, dbpeopleId, OnSubmitDivisionAdministratorChanges);
}

function OnSubmitDivisionAdministratorChanges(result) {
	popupLoading.Hide();

	var arrayValues = result.split("-");
	var success = arrayValues[0].toBool();

	if (!success)
	{
		popupAlertTemplateContent.SetText(arrayValues[1].toString());
		popupAlertTemplate.SetHeaderText(arrayValues[2].toString());
		popupAlertTemplate.Show();
		return;
	}

	hfRequestDetail.Set("RequestMasterID", 0);

	popupAlertTemplateContent.SetText(arrayValues[1].toString());
	popupAlertTemplate.SetHeaderText(arrayValues[2].toString());
	popupAlertTemplate.Show();

	setDirty(false);

	// we need to reload the grid
	gvDivisionAdmins.PerformCallback();

	//HideBusinessAreaDetail();
}

