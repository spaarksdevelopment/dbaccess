﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Services;
using System.Web.Script.Services;
using AjaxControlToolkit;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Services
{
    /// <summary>
    /// Summary description for ListBuilder
    /// </summary>
    [WebService(Namespace = "http://db.csbc.dbAccess/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class ListBuilder : WebService
    {
         //base security methods
        private DBAppUser _CurrentUser;

        protected DBAppUser CurrentUser
        {
            get
            {
                if (_CurrentUser == null) _CurrentUser = (DBAppUser)(new DBSqlMembershipProvider()).GetUser();
                return _CurrentUser;
            }
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetRegions(string knownCategoryValues, string category)
        {
            return GetRegions(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetRegions(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {          

                var Regions = Director.GetRegionsAllEnabled();

                if (Regions.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No regions found", ""));
                }
                else
                {
                    foreach (var li in Regions)
                    {
                        bool isSelected = (contextKey == li.RegionID.ToString());
                        if (Regions.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.RegionID.ToString(), isSelected));

                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetCountries(string knownCategoryValues, string category)
        {
            return GetCountries(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetCountries(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {
                StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

                int RegionID = 0;

                if (kv.ContainsKey("RegionID"))
                {
                    RegionID = Helper.SafeInt(kv["RegionID"]);
                }
                else
                {
                    //HACK: If Parent drop-down is not a 'Cascading Drop Down' then the key comes through as 'undefined'
                    RegionID = Helper.SafeInt(kv["undefined"]);
                }

                var Countries = Director.GetCountriesEnabledForRegion(RegionID);

                if (Countries.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No countries found", ""));
                }
                else
                {
                    foreach (var li in Countries)
                    {
                        bool isSelected = (contextKey == li.CountryID.ToString());
                       // if (Countries.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.CountryID.ToString(), isSelected));

                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetCities(string knownCategoryValues, string category)
        {
            return GetCities(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetCities(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            //Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {
                StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

                int? CountryID = null;

                if (kv.ContainsKey("CountryID"))
                {
                    CountryID = Helper.SafeInt(kv["CountryID"]);
                }

                var Cities = Director.GetCitiesForCountry(CountryID, true);

                if (Cities.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No Cities found", ""));
                }
                else
                {
                    foreach (var li in Cities)
                    {
                        bool isSelected = (contextKey == li.CityID.ToString());
                        ///HACK: disabling the automatic selection of city, because in the location selector control city needs to be selected manually in order to trigger the jQuery 'change' event
                        //if (Cities.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.CityID.ToString(), isSelected));
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetBuildings(string knownCategoryValues, string category)
        {
            return GetBuildings(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetBuildings(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {
                StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

                int? CityID = null;

                if (kv.ContainsKey("CityID"))
                {
                    CityID = Helper.SafeInt(kv["CityID"]);
                }

                var Buildings = Director.GetBuildingsForCity(CityID, true);

                if (Buildings.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No Buildings found", ""));
                }
                else
                {
                    foreach (var li in Buildings)
                    {
                        bool isSelected = (contextKey == li.BuildingID.ToString());
                        if (Buildings.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.BuildingID.ToString(), isSelected));
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetFloors(string knownCategoryValues, string category)
        {
            return GetFloors(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetFloors(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {
                StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

                int? BuildingID = null;

                if (kv.ContainsKey("BuildingID"))
                {
                    BuildingID = Helper.SafeInt(kv["BuildingID"]);
                }

                var Floors = Director.GetFloorsForBuilding(BuildingID, true, false);

                if (Floors.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No Floors found", ""));
                }
                else
                {
                    foreach (var li in Floors)
                    {
                        bool isSelected = (contextKey == li.FloorID.ToString());
                        if (Floors.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.FloorID.ToString(), isSelected));
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetAccessAreas(string knownCategoryValues, string category)
        {
            return GetAccessAreas(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetAccessAreas(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            //ac: commented out while passsing by, as not sure why it's here.
            //Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {
                StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

                int? FloorID = null;

                if (kv.ContainsKey("FloorID"))
                {
                    FloorID = Helper.SafeInt(kv["FloorID"]);
                }

                var AccessAreas = Director.GetAccessAreasForFloor(FloorID, true, false);

                if (AccessAreas.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No Access Areas found", ""));
                }
                else
                {
                    foreach (var li in AccessAreas)
                    {
                        bool isSelected = (contextKey == li.AccessAreaID.ToString());
                        if (AccessAreas.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.AccessAreaID.ToString(), isSelected));
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        [WebMethod, ScriptMethod]
        public string[] GetPersonCompletionList(string prefixText, int count, string contextKey)
          {
			Boolean bShowTerminated = false;

			try
			{
				if (!String.IsNullOrEmpty (contextKey)) bShowTerminated = Boolean.Parse (contextKey);
			}
			catch (FormatException)
			{
				// assume false
			}
			return DBAccessController.Director.GetPersonArray (prefixText, bShowTerminated);
		}

        [WebMethod, ScriptMethod]
        public string[] GetPersonCompletionListWithValidIds(string prefixText, int count, string contextKey)
        {
            return DBAccessController.Director.GetPersonArrayWithValidIds(prefixText);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetPassOffices(string knownCategoryValues, string category)
        {
            return GetPassOffices(knownCategoryValues, category, string.Empty);
        }

        [WebMethod, ScriptMethod]
        public CascadingDropDownNameValue[] GetPassOffices(string knownCategoryValues, string category, string contextKey)
        {
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

            Director dbAccessDirector = new Director(this.CurrentUser.dbPeopleID);

            try
            {         

                var PassOffice = Director.GetPassOfficesAllEnabled();

                if (PassOffice.Count == 0)
                {
                    values.Add(new CascadingDropDownNameValue("No Pass Offices found", ""));
                }
                else
                {
                    foreach (var li in PassOffice)
                    {
                        bool isSelected = (contextKey == li.PassOfficeID.ToString());
                        if (PassOffice.Count == 1) isSelected = true;
                        values.Add(new CascadingDropDownNameValue(li.Name, li.PassOfficeID.ToString(), isSelected));

                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return values.ToArray();
        }

        //[WebMethod]
        //public ArrayList GetCitiesByCountryId(int countryId)
        //{
        //   if (countryId < 1) return null;

        //   var list = new ArrayList {"0| - Select -"};

        //    var items = Director.GetCitiesEnabledForCountry(countryId);

        //    foreach(DBAccessController.DAL.LocationCity city in items)
        //    {
        //        list.Add(city.CityID + "|" + city.Name);
        //    }

        //    return list;
        //}

        [WebMethod]
        public ArrayList GetCitiesByCountryId(int countryId, int? languageID)
        {
            if (countryId < 1) return null;

            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            string culture = Director.GetLanguageCode(languageID);

            string select = resourceManager.GetString("Select",System.Globalization.CultureInfo.CreateSpecificCulture(culture));

            var list = new ArrayList { "0| -"+select+" -" };

            var items = Director.GetCitiesEnabledForCountry(countryId);

            foreach (DBAccessController.DAL.LocationCity city in items)
            {
               
                switch(languageID)
                {
                    case 1:
                        list.Add(city.CityID + "|" + city.Name);
                        break;
                    case 2:
                        if(!string.IsNullOrEmpty(city.GermanName))
                            list.Add(city.CityID + "|" + city.GermanName);
                        else
                            list.Add(city.CityID + "|" + city.Name);
                        break;
                    case 3:
                        if(!string.IsNullOrEmpty(city.ItalyName))
                            list.Add(city.CityID + "|" + city.ItalyName);
                        else
                            list.Add(city.CityID + "|" + city.Name);
                        break;
                    default:
                        list.Add(city.CityID + "|" + city.Name);
                        break;
                
                }
            }

            return list;
        }

        //[WebMethod]
        //public ArrayList GetBuildingsByCityId(int cityId)
        //{
        //    if (cityId < 1) return null;

        //    var list = new ArrayList {"0| - Select -"};

        //    var items = Director.GetBuildingsEnabledForCity(cityId, false);

            

        //    foreach (DBAccessController.DAL.LocationBuilding b in items)
        //    {
        //        list.Add(b.BuildingID + "|" + b.Name);
        //    }
                      
            

        //    return list;
        //}


        [WebMethod]
        public ArrayList GetBuildingsByCityId(int cityId, int? languageID)
        {
            if (cityId < 1) return null;

            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            string culture = Director.GetLanguageCode(languageID);

            string select = resourceManager.GetString("Select", System.Globalization.CultureInfo.CreateSpecificCulture(culture));

              var list = new ArrayList { "0| -" + select + " -" };

            var items = Director.GetBuildingsEnabledForCity(cityId, false);



            foreach (DBAccessController.DAL.LocationBuilding b in items)
            {                
                switch (languageID)
                {
                    case 1:
                        list.Add(b.BuildingID + "|" + b.Name);
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(b.GermanName))
                            list.Add(b.BuildingID + "|" + b.GermanName);
                        else
                            list.Add(b.BuildingID + "|" + b.Name);
                        break;
                    case 3:
                        if (!string.IsNullOrEmpty(b.ItalyName))
                            list.Add(b.BuildingID + "|" + b.ItalyName);
                        else
                            list.Add(b.BuildingID + "|" + b.Name);
                        break;
                    default:
                        list.Add(b.BuildingID + "|" + b.Name);
                        break;

                }
            }



            return list;
        }

        [WebMethod]
        public ArrayList GetFloorsByBuildingId(int buildingId, int? languageID)
        {
            if (buildingId < 1) return null;

            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            string culture = Director.GetLanguageCode(languageID);

            string select = resourceManager.GetString("Select", System.Globalization.CultureInfo.CreateSpecificCulture(culture));

            var list = new ArrayList { "0| -" + select + " -" };

            var items = Director.GetFloorsForBuilding(buildingId, true, false);

            foreach (DBAccessController.DAL.LocationFloor b in items)
            {
                list.Add(b.FloorID + "|" + b.Name);
            }

            return list;
        }

    

        [WebMethod]
        public ArrayList GetAccessAreaTypes(int? languageID) 
        {
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            string culture = Director.GetLanguageCode(languageID);

            string select = resourceManager.GetString("Select", System.Globalization.CultureInfo.CreateSpecificCulture(culture));

            var list = new ArrayList { "0| -" + select + " -" };

            var items = Director.GetAccessAreaTypes();

            foreach (DBAccessController.DAL.AccessAreaType b in items)
            {
                list.Add(b.AccessAreaTypeID + "|" + b.AccessAreaTypeName);
            }

            return list;
        }


        [WebMethod]
        public ArrayList GetDivisionsByCountry(int countryId, int? languageID)
        {
            if (countryId < 1) return null;

            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
            string culture = Director.GetLanguageCode(languageID);

            string select = resourceManager.GetString("Select", System.Globalization.CultureInfo.CreateSpecificCulture(culture));

            var list = new ArrayList { "0| -" + select + " -" };

            var items = Director.GetDivisionsByCountry(countryId);

            foreach (string[,] b in items)
            {
                list.Add(b[0,1] + "|" + b[1,0]);
            }

            return list;
        }

        [WebMethod]
        public ArrayList GetControlSystemCategoryName(int areaId, int controlsystemid)
        {
            if (areaId <= 0 || controlsystemid <=0) return null;
            //Control System check is not required here - 30/10/2014 - A.M
            //bool bval = Director.CheckIfControlSystemExists(areaId, controlsystemid); 
            var list = new ArrayList { };

                var items = Director.GetControlSystemCategoryName(areaId);
                var itemsavailable = Director.GetAvailableCategoryName();

                foreach (DBAccessController.DAL.Category b in items)
                {
                    list.Add(b.id + "|" + b.description.Trim());
                }

                foreach (DBAccessController.DAL.vw_ControlSystemCategoryName b in itemsavailable)
                {
                    list.Add(b.id + "|" + b.description.Trim());
                }

            return list;
        }

        //[WebMethod]
        //public ArrayList GetReloadCategoryName(int areaId, int controlsystemtypeId)
        //{
        //    if (areaId <= 0 || controlsystemtypeId <= 0) return null;

        //    var list = new ArrayList { };
        //    var items = Director.ReloadControlSystemCategoryName(areaId, controlsystemtypeId);

        //    foreach (DBAccessController.DAL.vw_ControlSystemCategory b in items)
        //    {
        //        list.Add(b.ControlSystemID + "-" + b.ControlSystemTypeID + "|" + b.ControlSystemCategoryName);
        //    }
        //    return list;
        //}

         [WebMethod]
        public ArrayList GetAvailableCategoryName(int? languageID)
         {
             ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
             string culture = Director.GetLanguageCode(languageID);

             string select = resourceManager.GetString("Select", System.Globalization.CultureInfo.CreateSpecificCulture(culture));

             var list = new ArrayList { "0| -" + select + " -" };

            // var list = new ArrayList { "0|- Select -" };
             var items = Director.GetAllCategoryName();

             foreach (DBAccessController.DAL.Category b in items)
             {
                 list.Add( b.id+ "|" + b.description.Trim());
             }
             return list;
         }

         public string GetResourceManager(string resourceName, string resourceIdentifier)
         {
             return Director.GetResourceManager(resourceName, resourceIdentifier);
         }
                       

    }
}
