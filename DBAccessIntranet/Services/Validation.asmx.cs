﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text.RegularExpressions;
//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Services
{
    /// <summary>
    /// Summary description for Validation
    /// </summary>
    [WebService(Namespace = "http://db.csbc.dbAccess/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Validation : System.Web.Services.WebService
    {

        //base security methods
        private DBAppUser _CurrentUser;

        protected DBAppUser CurrentUser
        {
            get
            {
                if (_CurrentUser == null) _CurrentUser = (DBAppUser)(new DBSqlMembershipProvider()).GetUser();
                return _CurrentUser;
            }
        }

        [WebMethod, ScriptMethod]
        public string MyTest(string Identifier)
        {
            return Identifier;
        }

        [WebMethod, ScriptMethod]
		//Returns 0 if noone found
		//Returns 1 if invalid email address
        public int GetPersonIdFromIdentifier(string Identifier)
        {
            //extract the dbPeopleid
            Regex re = new Regex("^C?[\\d]{7}\\s");

            List<DBAccessController.DAL.HRPerson> personList = new List<DBAccessController.DAL.HRPerson>();

            string dbPeopleidString = re.Match(Identifier).Value;
            if (dbPeopleidString != string.Empty)
            {
                dbPeopleidString = dbPeopleidString.Trim();
                if (dbPeopleidString.StartsWith("C")) dbPeopleidString = dbPeopleidString.TrimStart(new char[] { 'C' });

                int dbPeopleid = Helper.SafeInt(dbPeopleidString);
                //now validate in database
                personList = Director.GetPerson(dbPeopleid);
            }
            else
            {
                string emailString = string.Empty;
                string fullName = string.Empty;

                                        //extract the email address
                                        if (Identifier.Contains("("))
                                        {
                                            string[] prev = Identifier.Split(new char[] { '(' });
                                            
                                            // Change the def of aft to include all prev strings
                                            string[] aft = new string[prev.Length];

                                            for (int i = 0; i < (prev.Length); i++)
                                            {
                                                aft[i] = prev[i].ToString();
                                                
                                            }
                                           
                                            int result = 0;
                                            for (int j = 0; j < (prev.Length); j++)
                                            {
                                                if (!string.IsNullOrWhiteSpace(
                                                    aft[j].ToString()) && 
                                                    (aft[j].ToString().Contains("@")))
                                                {
                                                    
                                                    string[] mail =    aft[j].ToString().Split(new char[] { ')' });
                                                    emailString = mail[0].ToString();
                                                    result = 1;
                                                    break;
                                                }
                                            }

                                            if (result == 0)
                                            {
                                                emailString = prev[0].ToString();
                                                personList = Director.GetPerson(emailString);
                                            }
                                        }

                //check if it was actually just an email address
				if (DBSqlMembershipProvider.ValidateEmailAddress(emailString))
				{
					personList = Director.GetPerson(emailString);
				}
            }

            if (personList.Count == 0)
                return 0;
            else
                return personList[0].dbPeopleID;
        }

        [WebMethod, ScriptMethod]
        public int GetPersonBadgeCount(int dbPeopleId)
        {
            List<DBAccessController.DAL.vw_HRPersonBadge> personBadgeList = Director.GetPersonBadges(dbPeopleId, true);

            return personBadgeList.Count;
        }

        [WebMethod, ScriptMethod]
        public int GetReplacementBadgeCount(int RequestMasterId)
        {
            List<DBAccessController.DAL.vw_BadgeRequest> BadgeRequestList = Director.GetBadgeRequests(RequestMasterId, DBAccessEnums.BadgeRequestIssueType.Replacement);

            return BadgeRequestList.Count;
        }

        [WebMethod, ScriptMethod]
        public int GetRequestPersonCount(int RequestMasterId)
        {
            var RequestPersonCount = Director.GetAccessRequestPersonCount(RequestMasterId);

            return RequestPersonCount.HasValue ? RequestPersonCount.Value : 0;
        }

        [WebMethod, ScriptMethod]
        public int GetAccessRequestPersonsWithoutBadgeCount(int RequestMasterId)
        {
            var RequestPersonCount = Director.GetAccessRequestPersonsWithoutBadgeCount(RequestMasterId);

            return RequestPersonCount.HasValue ? RequestPersonCount.Value : 0;
        }

        [WebMethod, ScriptMethod]
        public int GetRequestAccessCount(int RequestMasterId)
        {
            List<DBAccessController.DAL.vw_AccessAreaRequest> AccessAreasList = Director.GetAccessAreasForRequest(RequestMasterId);

            return AccessAreasList.Count;
        }

        [WebMethod, ScriptMethod]
        public int GetRequestPersonPassOfficeId(int RequestMasterId, int dbPeopleId)
        {            
            DBAccessController.DAL.vw_AccessRequestPerson RequestPerson = Director.GetRequestPerson(RequestMasterId, dbPeopleId);

            return RequestPerson.PassOfficeID.HasValue ? RequestPerson.PassOfficeID.Value : 0;             
        }

        [WebMethod, ScriptMethod]
        public bool CheckRequestPersonsCountriesValid(int RequestMasterId)
        {
            List<DBAccessController.DAL.vw_AccessRequestPerson> RequestPersons = Director.GetAccessRequestPersons(RequestMasterId);

            bool valid = true;
            foreach (var person in RequestPersons)
            {
                if (!person.CountryEnabled.HasValue) valid = false;
                else if (!person.CountryEnabled.Value) valid = false;
            }

            return valid;
        }

        [WebMethod, ScriptMethod]
        public string GetUBRTitle(string UBRCode)
        {
            return Director.GetUBRTitle(UBRCode);
        }

    }
}
