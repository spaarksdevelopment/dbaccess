﻿
using System.Collections.Generic;
using System.Web.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Services
{
    /// <summary>
    /// Summary description for WorldMap
    /// </summary>
    [WebService(Namespace = "http://db.csbc.dbAccess/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WorldMap : WebService
    {

        [WebMethod]
        public List<DBAccessController.DAL.WorldMapData> GetWorldMapData()
        {
            List<DBAccessController.DAL.WorldMapData> items = new List<DBAccessController.DAL.WorldMapData>();

           // items = Director.GetMapData();

            //return items;

            return null;
        }

    }
}
