﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
    public partial class LoggedInUserDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoggedInAsUserLable.Text = "Logged in As: " + Session["FullNameOfCurrentUser"];
            LastLoggedInOnLable.Text = "Last Logged in On: " + Session["LastLoggedInDateTime"];
        }
    }
}