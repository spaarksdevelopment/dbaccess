using System;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
    public abstract class BaseSecurityControl : BaseControl
    {
        #region Properties

        protected Pages.BaseSecurityPage PageSecurity
        {
            get { return (Pages.BaseSecurityPage)Page; }
        }

        #endregion
    }
}