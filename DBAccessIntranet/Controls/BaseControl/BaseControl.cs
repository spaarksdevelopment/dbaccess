using System;
using System.Collections.Generic;
using System.Web.UI;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages;

using Aduvo.Common.WebUtilityManager;
using Spaarks.CustomMembershipManager;
using Spaarks.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
    public abstract class BaseControl : System.Web.UI.UserControl
    {
        public bool ReadOnly
        {
            get
            {
                ViewState["readonly"] = ViewState["readonly"] ?? true;

                return (bool)ViewState["readonly"];
            }
            set { ViewState["readonly"] = value; }
        }

        public virtual object DataSource { get; set; }
        private bool? m_isRestrictedUser = new Nullable<bool>();
        private int m_restrictedCountryId = 0;
        private string m_restrictedCountryCode = "";
        private string m_restrictedCountryName = "";

		private DBAppUser _CurrentUser;
		public DBAppUser CurrentUser
		{
			get { return _CurrentUser; }
		}

        public int RestrictedCountryId
        {
            get { return m_restrictedCountryId; }
        }

        public string RestrictedCountryCode
        {
            get { return m_restrictedCountryCode; }
        }

        public string RestrictedCountryName
        {
            get { return m_restrictedCountryName; }
        }        

        public bool IsRestrictedUser
        {
            get
            {
                if (!m_isRestrictedUser.HasValue)
                {
                    m_isRestrictedUser = false;

                    DBAppUser theUserSession = (DBAppUser)new DBSqlMembershipProvider().GetUser();
                    mp_User theUser = Director.GetUserBydbPeopleId(theUserSession.dbPeopleID);

                    if (theUser != null && theUser.RestrictedCountryId.HasValue)
                    {
                        m_isRestrictedUser = true;
                        m_restrictedCountryId = theUser.RestrictedCountryId.Value;

                        LocationCountry theCountry =  Director.GetCountry(m_restrictedCountryId);

                        if (theCountry != null && !String.IsNullOrEmpty(theCountry.Code))
                        {
                            m_restrictedCountryCode = theCountry.Code;
                            m_restrictedCountryName = theCountry.Name;
                        }
                    }
                }

                return m_isRestrictedUser.Value;
            }
        }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			if (!this.DesignMode)
			{
				if (Context.User.Identity.IsAuthenticated && this.CurrentUser == null)
				{
					_CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
				}
			}
		}
        
        /// <summary>
        /// Overriden base method
        /// Call SetControls() method to set controls prior to renering
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            List<Control> controlList = new List<Control>();
            string prefix = string.Empty;
            string OnloadFunctions = string.Empty;

            OnEmitClientScript(ref controlList, ref prefix, ref OnloadFunctions);

            string clientScript = ScriptHelper.CreateClientSideVariablesForControls(controlList, prefix, OnloadFunctions);

            RegisterStartupScript(this.ID + "FormFieldVariables", clientScript);
        }     

        #region client script related methods

        /// <summary>
        /// Register a script resource
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        protected void RegisterScriptResource(Type type, string scriptName)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);

            // Add script resource for item depending whether a scriptmanager is in use
            if (sm != null)
            {
                ScriptManager.RegisterClientScriptResource(this.Page, type, scriptName);
            }
            else
            {
                // If no Ajax ScriptManager then use standard ClientScriptManager
                Page.ClientScript.RegisterClientScriptResource(type, scriptName);
            }
        }

        /// <summary>
        /// Register a script include
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptURL">relateve script URL will be converted to Absolute</param>
        protected void RegisterScriptInclude(string scriptKey, string scriptURL)
        {
            var sm = ScriptManager.GetCurrent(this.Page);
            scriptURL = Page.ResolveClientUrl(scriptURL);
            Random r = new Random();
            scriptURL += "?rnd=" + r.Next().ToString();
            
            if (!Page.ClientScript.IsClientScriptIncludeRegistered(Page.GetType(), scriptKey))
            {
                // Add script resource for item depending whether a scriptmanager is in use
                if (sm != null)
                {
                    ScriptManager.RegisterClientScriptInclude(this.Page, this.GetType(), scriptKey, scriptURL);
                }
                else
                {
                    // If no Ajax ScriptManager then use standard ClientScriptManager
                    Page.ClientScript.RegisterClientScriptInclude(this.GetType(), scriptKey, scriptURL);
                }
            }
        }

        /// <summary>
        /// Register a startup script
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptText"></param>
        protected void RegisterStartupScript(string scriptKey, string scriptText)
        {
            var sm = ScriptManager.GetCurrent(this.Page);

            if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), scriptKey))
            {
                // Add script resource for item depending whether a scriptmanager is in use
                if (sm != null && sm.IsInAsyncPostBack)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), scriptKey, scriptText, true);
                }
                else
                {
                    // If no Ajax ScriptManager then use standard ClientScriptManager
                    Page.ClientScript.RegisterStartupScript(this.GetType(), scriptKey, scriptText, true);
                }
            }
        }

        /// <summary>
        /// Register a startup script
        /// Script manager is used if present
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="scriptText"></param>
        protected void RegisterClientScriptBlock(string scriptKey, string scriptText)
        {
            var sm = ScriptManager.GetCurrent(this.Page);

            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), scriptKey))
            {
                // Add script resource for item depending whether a scriptmanager is in use
                if (sm != null)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), scriptKey, scriptText, true);
                }
                else
                {
                    // If no Ajax ScriptManager then use standard ClientScriptManager
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptKey, scriptText, true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Override this method in order to set up client side variables and startup scripts for this control
        /// </summary>
        /// <param name="controlList">A list of controls to add the controls to</param>
        /// <param name="prefix">use a prefix if your control will appear more than once, the prefix should be unique for the instance of the user control</param>
        /// <param name="OnloadFunctions">any additional onload functions to call after setting up the variables</param>
        protected virtual void OnEmitClientScript(ref List<Control> controlList, ref string prefix, ref string OnloadFunctions)
        {

        }

        protected void DisplayMessage(string caption, string message, int width)
        {
            BasePage page = this.Page as BasePage;
            page.DisplayMessage(caption, message, width);
        }

        public override void DataBind()
        {
            base.DataBind();

            ClearForm();

            if (DataSource != null)
            {
                PopulateFormControls();
                CopyToForm();
            }
        }

        public virtual void ClearForm()
        {
        }

        public virtual void CopyFromForm()
        {
        }

        public virtual void CopyToForm()
        {
        }

        protected virtual void PopulateFormControls()
        {
        }

        protected virtual void SetControls()
        {
        }
    }
}