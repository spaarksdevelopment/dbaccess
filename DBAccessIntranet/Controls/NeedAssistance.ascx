﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NeedAssistance.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.NeedAssistance" %>
<script type="text/javascript">
    function openOpeningHoursPopUp(office) {
        if (office == 'UKMEA') {
            popupOpeningHours.SetContentHtml(
                '<div style="text-align: left;">'
                + '<div style="color: #0018a8; font-size: 15px; float: left; width: 200px;">Pass Office: ' + office + ' </div></br></br>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="MondayUK" runat="server" meta:resourceKey="Monday"></asp:Literal></div><div>07:00 to 19:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="TuesdayUK" runat="server" meta:resourceKey="Tuesday"></asp:Literal>   </div><div>07:00 to 07:00 (24 hr)</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="WednesdayUK" runat="server" meta:resourceKey="Wednesday"></asp:Literal> </div><div>07:00 to 07:00 (24 hr)</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="ThursdayUK" runat="server" meta:resourceKey="Thursday"></asp:Literal> </div><div>07:00 to 07:00 (24 hr)</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="FridayUK" runat="server" meta:resourceKey="Friday"></asp:Literal>    </div><div>07:00 to 07:00 (24 hr)</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SaturdayUK" runat="server" meta:resourceKey="Saturday"></asp:Literal> </div><div>07:00 to 13:00</div>'
                + '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SundayUK" runat="server" meta:resourceKey="Sunday"></asp:Literal>    </div><div><asp:Literal ID="SundayClosedUK" runat="server" meta:resourceKey="Closed"></asp:Literal></div>'
			    + '</div>');
        } else if (office == 'Americas') {
            popupOpeningHours.SetContentHtml(
                '<div style="text-align: left;">'
                + '<div style="color: #0018a8; font-size: 15px; float: left; width: 200px;">Pass Office: ' + office + ' </div></br></br>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="MondayUS" runat="server" meta:resourceKey="Monday"></asp:Literal>  </div><div>08:00 to 17:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="TuesdayUS" runat="server" meta:resourceKey="Tuesday"></asp:Literal>   </div><div>08:00 to 17:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="WednesdayUS" runat="server" meta:resourceKey="Wednesday"></asp:Literal> </div><div>08:00 to 17:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="ThursdayUS" runat="server" meta:resourceKey="Thursday"></asp:Literal> </div><div>08:00 to 17:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="FridayUS" runat="server" meta:resourceKey="Friday"></asp:Literal>      </div><div>08:00 to 17:00</div>'
				+ '<div style="clear:both;"/>'
                + '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SaturdayUS" runat="server" meta:resourceKey="Saturday"></asp:Literal>  </div><div><asp:Literal ID="SaturdayClosedUK" runat="server" meta:resourceKey="Closed"></asp:Literal></div>'
				+ '<div style="clear:both;"/>'
                + '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SundayUS" runat="server" meta:resourceKey="Sunday"></asp:Literal>    </div><div><asp:Literal ID="SundayClosedUS" runat="server" meta:resourceKey="Closed"></asp:Literal></div>'
			    + '</div>'
                );
        } else if (office == 'Deutschland') {
            popupOpeningHours.SetContentHtml(
                '<div style="text-align: left;">'
                + '<div style="color: #0018a8; font-size: 15px; float: left; width: 200px;">Pass Office: ' + office + ' </div></br></br>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="MondayDL" runat="server" meta:resourceKey="Monday"></asp:Literal>  </div><div>08:30 to 15:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="TuesdayDL" runat="server" meta:resourceKey="Tuesday"></asp:Literal>   </div><div>08:30 to 15:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="WednesdayDL" runat="server" meta:resourceKey="Wednesday"></asp:Literal> </div><div>08:30 to 15:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="ThursdayDL" runat="server" meta:resourceKey="Thursday"></asp:Literal> </div><div>08:30 to 15:00</div>'
				+ '<div style="clear:both;"/>'
				+ '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="FridayDL" runat="server" meta:resourceKey="Friday"></asp:Literal>      </div><div>08:30 to 15:00</div>'
				+ '<div style="clear:both;"/>'
                + '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SaturdayDL" runat="server" meta:resourceKey="Saturday"></asp:Literal>  </div><div><asp:Literal ID="SaturdayClosedDL" runat="server" meta:resourceKey="Closed"></asp:Literal></div>'
				+ '<div style="clear:both;"/>'
                + '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="SundayDL" runat="server" meta:resourceKey="Sunday"></asp:Literal>    </div><div><asp:Literal ID="SundayClosedDL" runat="server" meta:resourceKey="Closed"></asp:Literal></div>'
			    + '<div style="clear:both;"/></br>'
                + '<div style="color: #000; font-size: 12px; float: left; width: 200px;"><asp:Literal ID="ExtendedTimeDL" runat="server" meta:resourceKey="ExtendedTime"></asp:Literal>    </div><div>08:00 to 17:00 (Weekdays)</div>'
			    + '</div>'
                );
        } else {
            popupOpeningHours.SetContentHtml("Pass Office not found.");
        }
    }
</script>

<div style="padding-left: 50px;">
	<table style="height: 450px; width: 300px;">
<%--		<tr class="BlueTableHeader">
			<td class="table_cell_info" style="padding-left:55px;">
					<h2 style="position: relative; top: 5px;">Need Assistance?
					</h2>
			</td>
		</tr>--%>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 55px; padding-top: 20px; vertical-align: middle;">
				<h3 class="noBottomMargin"><asp:Literal ID="dbAccessSupportContact" runat="server" meta:resourceKey="dbAccessSupportContact"></asp:Literal></h3>
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 65px">
				<a href="mailto:dbaccess@db.com">dbaccess@db.com</a>
			</td>
		</tr>
		<tr>
			<td  class="table_cell_info">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 55px">
				<h3 class="noBottomMargin"><asp:literal ID="PassOfficeContacts" runat="server" meta:resourcekey="PassOfficeContacts"></asp:literal>
				</h3>
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 60px;">
				<h3 class="noBottomMargin" style="font-size: 12px !important">
					UKMEA
				</h3>
			</td>
		</tr>
		<tr>
			<td  height="5px" align="left" class="table_cell_info"  style="padding-left: 65px">
				London - Winchester House
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
				<a href="mailto:security.pass-office@db.com">security.pass-office@db.com</a>
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
				Tel: +44(20) 754-51515
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
				<asp:HyperLink style="color: #0098DB; text-decoration: underline;" 
					ID="OpeningHoursUK" NavigateUrl="javascript:openOpeningHoursPopUp('UKMEA');" runat="server" 
					onclick="popupOpeningHours.Show();" Text="Opening Hours" 
					meta:resourcekey="OpeningHours" />
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 60px;">
				<h3 class="noBottomMargin" style="font-size: 12px !important">
					Americas
				</h3>
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 65px">
				New York - 60 Wall Street
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 65px">
				<a href="mailto:americas.id-room@db.com">americas.id-room@db.com</a>
			</td>
		</tr>
		<tr>
			<td class="table_cell_info" align="left" style="padding-left: 65px">
				Tel: +1 (212) 250-1599
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
				<asp:HyperLink style="color: #0098DB; text-decoration: underline;" 
					ID="HyperLink1" NavigateUrl="javascript:openOpeningHoursPopUp('Americas');" runat="server" 
					onclick="popupOpeningHours.Show();" Text="Opening Hours" 
					meta:resourcekey="OpeningHours" />
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
			</td>
		</tr>

		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 60px;">
				<h3 class="noBottomMargin" style="font-size: 12px !important">
					Deutschland
				</h3>
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 65px">
				Frankfurt – Taunusanlage 12
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 65px">
				<a href="mailto:ausweisstelle.frankfurt@db.com">ausweisstelle.frankfurt@db.com</a>
			</td>
		</tr>
		<tr>
			<td class="table_cell_info" align="left" style="padding-left: 65px">
				Tel: +49 (69) 910-21100
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info"  style="padding-left: 65px">
				<asp:HyperLink style="color: #0098DB; text-decoration: underline;" 
					ID="HyperLink2" NavigateUrl="javascript:openOpeningHoursPopUp('Deutschland');" runat="server" 
					onclick="popupOpeningHours.Show();" Text="Opening Hours" 
					meta:resourcekey="OpeningHours" />
			</td>
		</tr>
		<tr style="height: 20px;">
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td align="left" class="table_cell_info" style="padding-left: 45px; padding-right: 15px; padding-bottom: 30px;">
				<asp:literal ID="ClickToSee" runat="server"></asp:literal><a href="Pages/PDFs/DBAccessUserGuide.pdf"><asp:Literal ID="dbAccessUserGuide" runat="server" meta:resourcekey="dbAccessUserGuide"></asp:Literal></a>
			</td>
		</tr>
	</table>
</div>

<dx:ASPxPopupControl ID="PopupControlTooltip" ClientInstanceName="popupTooltip" runat="server"
	ShowHeader="False" PopupVerticalAlign="Above" 
	PopupHorizontalAlign="OutsideRight" 
	meta:resourcekey="PopupControlTooltipResource1">
	<ContentCollection>
		<dx:PopupControlContentControl ID="PopupControlContentControlTooltip" runat="server">
			<dx:ASPxImage ID="ImageTooltip" ClientInstanceName="popupTooltip_Image" 
				runat="server">
			</dx:ASPxImage>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="ASPxPopupControlOpeningHours" ClientInstanceName="popupOpeningHours"
	runat="server" SkinID="PopupCustomSize" EnableViewState="False" HeaderText="Opening Hours"
	Width="390px" Height="170px"
	CloseAction="CloseButton" 
	meta:resourcekey="HeaderOpeningHours">
	<ContentCollection>
		<dx:PopupControlContentControl ID="PopupControlContentOpeningHours" 
			runat="server" SupportsDisabledAttribute="True">
			<div style="text-align: left;">
                Loading...
			</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
