﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPager;
using DevExpress.Web.ASPxHeadline;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxNewsControl;
using DevExpress.Web.ASPxHtmlEditor;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
	public partial class NewsFeed : System.Web.UI.UserControl
	{
		public spaarks.DB.CSBC.DBAccess.DBAccessController.DAL.LocationBuilding Building
		{
			set { }
		}

		public bool AllowEdit
		{
			set
			{
				NewsHtmlEditor.Settings.AllowDesignView = value;
				NewsHtmlEditor.Settings.AllowHtmlView = value;
				
				if(!value)	//ie save button hidden
					NewsHtmlEditor.Height = 550;

				ButtonSaveNewsHtml.Visible = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
            //This is a test, we will need to use the database to store this
            if (!Page.IsPostBack)
            {
                NewsHtmlEditor.Html = GetSavedtxt();
            }
		}

		protected void ButtonSaveNewsHtml_Click(object sender, EventArgs e)
		{
            Director.SaveNewsHtml("" +NewsHtmlEditor.Html);          
            
		}

        public string GetSavedtxt()
        {
            string newsFeed = "";
            var news = Director.GetNews().FirstOrDefault();
            
            if(news!=null)
                newsFeed = news.Text;
            
            return newsFeed;
        }

	}
}
