﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="OutOfOfficeControl.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.OutOfOfficeControl" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<h1><asp:label ID="LabelTitle" runat="server" meta:resourcekey="LabelTitle"></asp:label></h1>
<div id="footerLower"></div>
<p>
    <asp:label ID="LabelHelp" runat="server" meta:resourcekey="LabelHelp"></asp:label>
</p>
<br />
   

<div id="OOFHelpInfo">
    <p><asp:literal ID="OutOfficeLabelHelp" runat="server" meta:resourcekey="OutOfficeLabelHelp"></asp:literal></p>        
</div>
	<div id="tabs" style="width:550px">
    <ul>
		<li><a href="#tabs-1" id="OOF" class=".grey_11"><asp:Literal ID="TurnOnOutOfOffice" runat="server" meta:resourcekey="TurnOnOutOfOffice"></asp:Literal></a></li>
		<li><a href="#tabs-2" id="OOFO" class=".grey_11"><asp:Literal ID="TurnOffOutOfOffice" runat="server" meta:resourcekey="TurnOffOutOfOffice"></asp:Literal></a></li>
	</ul>
    <div id="tabs-1">
        <h3><asp:Literal ID="Enable" runat="server" meta:resourcekey="Enable"></asp:Literal></h3>
        <p><asp:Literal ID="OutOfOfficeEnabled" runat="server" meta:resourcekey="OutOfOfficeEnabled"></asp:Literal></p>
		<asp:Table runat="server" ID="Table1" SkinID="FormTable">
            <asp:TableRow ID="TableRow2" runat="server">                                
                <asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableItem">
                    <p><asp:Literal ID="EnableFrom" runat="server" meta:resourcekey="EnableFrom"></asp:Literal> </p>
                    <dx:ASPxDateEdit ID="ASPxDateEditEnableFromDate" 
					ClientInstanceName="ooEnableFrom" runat="server" CssClass="DateEditTextBox" EditFormat="Date" 
						EditFormatString="dd-MMM-yyyy"
						DisplayFormatString="dd-MMM-yyyy" ClientSideEvents-DateChanged="UpdateToDate" 
					Font-Size="Larger" >
                        <CalendarProperties TodayButtonText="<%$ Resources:CommonResource, Today %>"  ClearButtonText="<%$ Resources:CommonResource, Clear%>"></CalendarProperties>
					
					<ClientSideEvents DateChanged="UpdateToDate"></ClientSideEvents>
					</dx:ASPxDateEdit>
                
				</asp:TableCell>
            </asp:TableRow>                           
            <asp:TableRow ID="TableRow1" runat="server">                                
                <asp:TableCell ID="TableCell5" runat="server" SkinID="FormTableItem">
                    <p><asp:Literal ID="EnableUntil" runat="server" meta:resourcekey="EnableUntil"></asp:Literal></p>
					<dx:ASPxDateEdit ID="ASPxDateEditEnableUntilDate" 
					ClientInstanceName="ooEnableUntil" runat="server" EditFormat="Date" EditFormatString="dd-MMM-yyyy"
                         CssClass="DateEditTextBox" DisplayFormatString="dd-MMM-yyyy" 
					Font-Size="Larger" ClientSideEvents-DateChanged="ValidateDates">
                        <CalendarProperties TodayButtonText="<%$ Resources:CommonResource, Today %>"  ClearButtonText="<%$ Resources:CommonResource, Clear%>"></CalendarProperties>					
						<ClientSideEvents DateChanged="ValidateDates"></ClientSideEvents>
					</dx:ASPxDateEdit>
                
			</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow5" runat="server">
                <asp:TableCell>
					<br />
                    <dx:ASPxButton ID="Confirm" Text="" runat="server" 
						AutoPostBack="false" meta:resourcekey="Confirm" >
                        <ClientSideEvents Click="function(s,e){ ValidateDates(); }" />
					</dx:ASPxButton>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
	</div>
	<div id="tabs-2" >
        <h3><asp:Literal ID="Disable" runat="server" meta:resourcekey="Disable"></asp:Literal></h3>
        <p>
		<asp:Literal ID="OutOffOfficeDatesText" runat="server" meta:resourcekey="OutOffOfficeDatesText"></asp:Literal>
		</p>
		<asp:Table runat="server" ID="Table2" SkinID="FormTable">
            <asp:TableRow ID="TableRow3" runat="server">                                
                <asp:TableCell ID="TableCell1" runat="server" SkinID="FormTableItem">
                    <p style="width:150px"><asp:Literal ID="EnableFrom1" runat="server" meta:resourcekey="EnableFrom"></asp:Literal></p>
                    <dx:ASPxDateEdit ID="FromDateDisable" runat="server" Enabled="false" 
						ClientInstanceName="ooEnableFromDisable" CssClass="OutOfficeBoxStyle" Width="150px" 
                        EditFormat="Date" EditFormatString="dd-MMM-yyyy" 
						DisplayFormatString="dd-MMM-yyyy">
                        <CalendarProperties TodayButtonText="<%$ Resources:CommonResource, Today %>"  ClearButtonText="<%$ Resources:CommonResource, Clear%>"></CalendarProperties>
                    </dx:ASPxDateEdit>
				</asp:TableCell>
            </asp:TableRow>                           
            <asp:TableRow ID="TableRow4" runat="server">  
                 <asp:TableCell ID="TableCell2" runat="server" SkinID="FormTableItem">
                    <p><asp:Literal ID="EnableUntil1" runat="server" meta:resourcekey="EnableUntil"></asp:Literal></p>
                    <dx:ASPxDateEdit ID="UntilDateDisable" runat="server" Enabled="false" 
					ClientInstanceName="ooEnableReturnDisable" CssClass="OutOfficeBoxStyle" Width="150px" 
                        EditFormat="Date" EditFormatString="dd-MMM-yyyy" 
					DisplayFormatString="dd-MMM-yyyy">
                        <CalendarProperties TodayButtonText="<%$ Resources:CommonResource, Today %>"  ClearButtonText="<%$ Resources:CommonResource, Clear%>"></CalendarProperties>
                    </dx:ASPxDateEdit>               
				</asp:TableCell>
            </asp:TableRow>
             <asp:TableRow ID="TableRow6" runat="server">
                <asp:TableCell>
                    <br />
                    <dx:ASPxButton ID="ConfirmDisable" Text="" 
					 runat="server" ClientInstanceName="Disable" Enabled="True"  
					 AutoPostBack="false" meta:resourcekey="ConfirmDisable" >
						<ClientSideEvents Click="function(s,e){ checkRequest(); }" />
                   </dx:ASPxButton>                
				</asp:TableCell>
            </asp:TableRow>
        </asp:Table>
	</div>
</div>
      

    
<dx:ASPxHiddenField ID="ASPxHiddenFieldOutOfOfficeEnabled" ClientInstanceName="hfOutOfOfficeEnabled"
        runat="server">
    </dx:ASPxHiddenField>
<dx:ASPxHiddenField ID="ASPxHiddenFieldLeaveFromDate" ClientInstanceName="hfOutOfOfficeLeaveFrom"
        runat="server">
    </dx:ASPxHiddenField>
<dx:ASPxHiddenField ID="ASPxHiddenFieldLeaveUntilDate" ClientInstanceName="hfOutOfOfficeLeaveUntil"
    runat="server">
</dx:ASPxHiddenField>

<dx:ASPxPopupControl ID="PopupAddOutOfOffice" 
	ClientInstanceName="popupAddOutOfOffice" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="" 
	meta:resourcekey="PopupAddOutOfOffice">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">              
		<p><asp:Literal ID="ConfirmEnableOutOfOffice" runat="server" meta:resourcekey="ConfirmEnableOutOfOffice"></asp:Literal></p>
			<span style="display:inline; width:99%; float:left;">
				<span style="margin:2px;float:left;">   
				<dx:ASPxButton ID="btnYes" runat="server" AutoPostBack="false" OnClick="addOutOfOffice"
					Text="Yes" meta:resourcekey="btnAddOutOfOfficeYes">		
				</dx:ASPxButton>
				</span>
				<span style="margin:2px;float:left;">   
				<dx:ASPxButton ID="btnNo" runat="server" AutoPostBack="false"   
					Text="No" meta:resourcekey="btnAddOutOfOfficeNo" >
					<ClientSideEvents Click="function(s,e){ popupAddOutOfOffice.Hide(); }" />
				</dx:ASPxButton>
				</span>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="PopupRemoveOutOfOffice" 
	ClientInstanceName="popupRemoveOutOfOffice" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="" 
	meta:resourcekey="PopupRemoveOutOfOffice">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">
			<p><asp:Literal ID="ConfirmDisableOutOfOffice" runat="server" meta:resourcekey="ConfirmDisableOutOfOffice"></asp:Literal></p>
			<span style="display:inline; width:99%; float:left;">
				<span style="margin:2px;float:left;">   
			<dx:ASPxButton ID="ASPxButton4" runat="server" AutoPostBack="false"
				Text="" OnClick="DeleteOutOfOffice" meta:resourcekey="btnAddOutOfOfficeYes">					
			</dx:ASPxButton>
			</span>
				<span style="margin:2px;float:left;">   
			<dx:ASPxButton ID="ASPxButton5" runat="server" AutoPostBack="false"   
				Text="No" meta:resourcekey="btnAddOutOfOfficeNo" >
				<ClientSideEvents Click="function(s,e){ resetOutOfOffice(); }" />
				</dx:ASPxButton>
				</span>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="PopupAddOutOfOfficeError" 
	ClientInstanceName="popupAddOutOfOfficeError" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="Enable Out Of Office" 
	meta:resourcekey="PopupAddOutOfOfficeErrorResource1">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">
			<p> <asp:Literal ID="OutOfOfficeEnableError"  meta:resourcekey="OutOfOfficeEnableError" runat="server" ClientIDMode="Static"> </asp:Literal></p>
			<span>
              
			<dx:ASPxButton ID="btnAddErrorOk" runat="server" AutoPostBack="false"   
				Text="Ok">
				<ClientSideEvents Click="function(s,e){ popupAddOutOfOfficeError.Hide(); }" />
				</dx:ASPxButton>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="PopupAddOutOfOfficeSuccess" 
	ClientInstanceName="popupAddOutOfOfficeSuccess" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="" 
	meta:resourcekey="PopupAddOutOfOffice">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">              
			<p><asp:Literal ID="PopupAddOutOfOfficeSuccessMsg" runat="server" meta:resourcekey="PopupAddOutOfOfficeSuccessMsg"></asp:Literal></p>
			<span>
			    <dx:ASPxButton ID="ASPxButton1" runat="server"  AutoPostBack="false" 
				Text="Ok">
				    <ClientSideEvents Click="function(s,e){ popupAddOutOfOfficeSuccess.Hide(); }" />
				</dx:ASPxButton>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="PopupRemoveOutOfOfficeError" 
	ClientInstanceName="popupRemoveOutOfOfficeError" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="Disable Out Of Office" 
	meta:resourcekey="PopupRemoveOutOfOfficeErrorResource1">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">
			<p><asp:Literal ID="OutOfOfficeRemoveError"  meta:resourcekey="OutOfOfficeRemoveError" runat="server" ClientIDMode="Static"> </asp:Literal></p>
			<span>              
			<dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="false"   
				Text="Ok">
				<ClientSideEvents Click="function(s,e){ popupRemoveOutOfOfficeError.Hide(); }" />
				</dx:ASPxButton>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="PopupRemoveOutOfOfficeSuccess" 
	ClientInstanceName="popupRemoveOutOfOfficeSuccess" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="" 
	meta:resourcekey="PopupRemoveOutOfOffice">
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">
			<p><asp:Literal ID="PopupRemoveOutOfOfficeSuccessMsg" runat="server" meta:resourcekey="PopupRemoveOutOfOfficeSuccessMsg"></asp:Literal></p>
			<span>              
			<dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="false"   
				Text="Ok">
				<ClientSideEvents Click="function(s,e){ popupRemoveOutOfOfficeSuccess.Hide(); }" />
				</dx:ASPxButton>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="PopupOutOfOfficeDateError" 
	ClientInstanceName="popupOutOfOfficeDateError" runat="server"
	SkinID="PopupBasic"
	CloseAction="None"
	HeaderText="Enable Out Of Office" 
	meta:resourcekey="PopupOutOfOfficeDateErrorResource1">
	<ClientSideEvents PopUp="function(s, e){ return ASPxClientEdit.ClearGroup(); }" />
	<ContentCollection>            
		<dx:PopupControlContentControl ID="PopupControlContentControl7" runat="server" 
			SupportsDisabledAttribute="True">
		<div style="width:220px;margin-left:auto;margin-right:auto;clear:both">
			<p><asp:Literal ID="OutOfOfficeDateNotSpecified"  meta:resourcekey="OutOfOfficeDateNotSpecified" runat="server" ClientIDMode="Static"> </asp:Literal></p>
			<span>              
			<dx:ASPxButton ID="ASPxButton6" runat="server" AutoPostBack="false"   
				Text="Ok">
				<ClientSideEvents Click="function(s,e){ popupOutOfOfficeDateError.Hide(); }" />
				</dx:ASPxButton>
			</span>
		</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>
<ppc:Popups ID="PopupTemplates" runat="server" />


    <asp:literal ID="LabelHelpForPerson" runat="server" meta:resourcekey="LabelHelpForPerson" Visible="false"></asp:literal>
    <asp:literal ID="LabelTitleForPerson" runat="server" meta:resourcekey="LabelTitleForPerson" Visible="false"></asp:literal>

    <asp:literal ID="ErrorPopupHeader" runat="server" meta:resourcekey="ErrorPopupHeader" Visible="false"></asp:literal>
    <asp:literal ID="ErrorPopupMsg" runat="server" meta:resourcekey="ErrorPopupMsg" Visible="false"></asp:literal>