﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class LandlordSelector : System.Web.UI.UserControl
    {  
   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Bind();
            }
        }

        public int? LandlordID { get; set; }
        
        public void Bind()
        {
            var Landlords = Director.GetLandlords();
            LandlordDropDownList.DataSource = Landlords;
            LandlordDropDownList.DataBind();
        
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (LandlordID.HasValue)
            {
                var item = LandlordDropDownList.Items.FindByValue(LandlordID.Value.ToString());
                if (item != null) item.Selected = true;
            }
        }

        /// <summary>
        /// method to set the passoffice ID property on index changed
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetLandLordID(object sender, EventArgs e)
        {
            LandlordID = Convert.ToInt32(LandlordDropDownList.SelectedValue);
        }


    }
}