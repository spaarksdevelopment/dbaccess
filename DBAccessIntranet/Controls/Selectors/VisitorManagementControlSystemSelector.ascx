﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisitorManagementControlSystemSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.VisitorManagementControlSystemSelector" %>
<asp:DropDownList ID="vManagementDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList300" OnSelectedIndexChanged="SetVisitorAccessSysID"
    DataTextField="Name" DataValueField="ControlSystemID" AppendDataBoundItems="true">
        <asp:ListItem Text="<%$ Resources:CommonResource, VisitorMgtSelect %>" Value="0" />
</asp:DropDownList>


