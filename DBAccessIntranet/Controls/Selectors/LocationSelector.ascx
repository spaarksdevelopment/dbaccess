﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.LocationSelector" %>
<script type="text/javascript">
$(document).ready(function () {
       // help region
        $('#HelpRegion').bind('click', function () {
            $("#HelpRegionInfo").slideToggle("slow");
        });
        $("#HelpRegionInfo").hide();
        //help country
        $('#HelpCountry').bind('click', function () {
            $("#HelpCountryInfo").slideToggle("slow");
        });
        $("#HelpCountryInfo").hide();
        //help city
        $('#HelpCity').bind('click', function () {
            $("#HelpCityInfo").slideToggle("slow");
        });
        $("#HelpCityInfo").hide();
    });
</script>

<div id="locationDiv">
                
    <asp:Table runat="server" ID="LocationTable" SkinID="FormTable">
        <asp:TableRow ID="TableRow0" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" SkinID="FormTableLabel"><asp:Literal ID="LocationSelectorRegion" meta:resourcekey="LocationSelectorRegion" runat="server"></asp:Literal> &nbsp;<img id="HelpRegion" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminImgTitle%>" alt="<%$ Resources:CommonResource, divAdminImgTitle%>"  runat="server"/></asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server" SkinID="FormTableItem">
             <asp:DropDownList ID="regionDropDownList" ClientIDMode="Static" runat="server" SkinID="DropDownList160" >
                <asp:ListItem Value="" meta:resourcekey="LocationSelectSelectRegion" />
            </asp:DropDownList>
            </asp:TableCell>                
            <asp:TableCell ID="TableCell4" SkinID="FormTableHelp" runat="server"> 
             <div id="HelpRegionInfo" class="HelpPopUp">
            <p><asp:Literal ID="LocationSelectorPlsSelectRegion" meta:resourcekey="LocationSelectorPlsSelectRegion" runat="server"></asp:Literal></p>
            </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" SkinID="FormTableLabel"><asp:Literal ID="LocationSelectorCountry" meta:resourcekey="LocationSelectorCountry" runat="server"></asp:Literal> &nbsp;<img id="HelpCountry" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminImgTitle%>" alt="<%$ Resources:CommonResource, divAdminImgTitle%>"  runat="server"/></asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableItem">
              <asp:DropDownList ID="countryDropDownList" ClientIDMode="Static" runat="server" SkinID="DropDownList160" >
                <asp:ListItem Value="" />
              </asp:DropDownList>
            </asp:TableCell>                
            <asp:TableCell ID="TableCell5" SkinID="FormTableHelp" runat="server"> 
             <div id="HelpCountryInfo" class="HelpPopUp">
            <p><asp:Literal ID="LocationSelectorPlsSelectCountry" meta:resourcekey="LocationSelectorPlsSelectCountry" runat="server"></asp:Literal></p>
            </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell6" runat="server" SkinID="FormTableLabel"><asp:Literal ID="LocationSelectorCity" meta:resourcekey="LocationSelectorCity" runat="server"></asp:Literal> &nbsp;<img id="HelpCity" src="../../App_Themes/DBIntranet2010/images/question.gif" title="<%$ Resources:CommonResource, divAdminImgTitle%>"  alt="<%$ Resources:CommonResource, divAdminImgTitle%>" runat="server"/></asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server" SkinID="FormTableItem">
             <asp:DropDownList ID="cityDropDownList" ClientIDMode="Static" runat="server" SkinID="DropDownList160" >
                <asp:ListItem Value="" Text="Select a City" />
              </asp:DropDownList>
            </asp:TableCell>                
            <asp:TableCell ID="TableCell8" SkinID="FormTableHelp" runat="server"> 
             <div id="HelpCityInfo" class="HelpPopUp">
            <p><asp:Literal ID="LocationSelectorPlsSelectCity" meta:resourcekey="LocationSelectorPlsSelectCity" runat="server"></asp:Literal></p>
            </div>
            </asp:TableCell>
        </asp:TableRow>                      
    </asp:Table>

     <act:CascadingDropDown ID="CascadingDropDownRegion" runat="server" TargetControlID="regionDropDownList"
        Category="RegionId"  ServicePath="~/Services/ListBuilder.asmx" ServiceMethod="GetRegions" BehaviorID="ddRegionId" 
         UseContextKey="true" meta:resourcekey="LocationSelectorPromptRegion"/>
    <act:CascadingDropDown ID="CascadingDropDownCountry" runat="server" TargetControlID="countryDropDownList"
        Category="CountryId" ServicePath="~/Services/ListBuilder.asmx"
        ServiceMethod="GetCountries" BehaviorID="ddCountryId" ParentControlID="regionDropDownList" UseContextKey="true"  meta:resourcekey="LocationSelectorPromptCountry"/>
    <act:CascadingDropDown ID="CascadingDropDownCity" runat="server" TargetControlID="cityDropDownList"
        Category="CityId"  ServicePath="~/Services/ListBuilder.asmx" ServiceMethod="GetCities" BehaviorID="ddCityId" ParentControlID="countryDropDownList"
        UseContextKey="true" meta:resourcekey="LocationSelectorPromptCity"/> 
  
  

</div>