﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxClasses;


using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class ProfileAccessSelector : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ASPxListBoxProfile_Callback(object source, CallbackEventArgsBase e)
        {
            ASPxListBox lb = source as ASPxListBox;
            lb.Items.Clear();

            int? CityId = Helper.SafeIntNullable(e.Parameter);
            if (CityId.HasValue)
            {
                var DivisionList = Director.GetDivisionsForCity(CityId.Value);

                foreach (var Division in DivisionList) 
                {
                    lb.Items.Add(Division.GetValue(1, 0).ToString(), Division.GetValue(0, 1));
                }
                    
            }
        }
    }
}