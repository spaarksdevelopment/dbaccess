﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class DBMovesFloorSelector : System.Web.UI.UserControl
    {
        public int? DBMovesFloorID { get; set; }

        public int? DBMovesLocationID { get; set; }

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);

            if (null != ViewState["DBMovesFloorID"])
			{
                DBMovesFloorID = (Int32)ViewState["DBMovesFloorID"];
			}
		}

		protected override object SaveViewState()
		{
            ViewState.Add("DBMovesFloorID", DBMovesFloorID);
			return base.SaveViewState();
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Bind();
            }
        }

		public void Bind()
        {
            var floors = Director.GetDBMovesFloors(DBMovesLocationID);
            dbMovesFloorDropDownList.DataSource = floors;
            dbMovesFloorDropDownList.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var item = dbMovesFloorDropDownList.Items.FindByValue(DBMovesFloorID.ToString());
			
            if (item != null) item.Selected = true;
        }

        protected void SetDBMovesFloorID(object sender, EventArgs e)
        {
            DBMovesFloorID = Convert.ToInt32(dbMovesFloorDropDownList.SelectedValue);
        }
    }
}