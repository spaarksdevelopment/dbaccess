﻿using System;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class PersonSelector : BaseControl
    {
		public Boolean ShowTerminated { get; set; }
		public ServiceMethodEnum ServiceMethod { get; set; }

        //This allows you to have multiple instances of the uc on a page without getting conflicts
        public int ControlIndex 
        { 
            set 
            {
                PersonSelectorNameTextBox.ID = "PersonSelectorNameTextBox" + value;
                
                AutoCompleteExtender1.ID = "AutoCompleteExtender" + value;
                AutoCompleteExtender1.TargetControlID = PersonSelectorNameTextBox.ID;

                PersonSelectorNameTextBox_TextBoxWatermarkExtender.ID = "PersonSelectorNameTextBox_TextBoxWatermarkExtender" + value;
                PersonSelectorNameTextBox_TextBoxWatermarkExtender.TargetControlID = PersonSelectorNameTextBox.ID;

                HiddenFieldControlIndex.Value = value.ToString();
            } 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			AutoCompleteExtender1.ContextKey = ShowTerminated.ToString ();
			AutoCompleteExtender1.ServiceMethod = ServiceMethod.ToString ();            
        }

        public enum ServiceMethodEnum
        {
            GetPersonCompletionList,
            GetPersonCompletionListWithValidIds
        }
    }
}