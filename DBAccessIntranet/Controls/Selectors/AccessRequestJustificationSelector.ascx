﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessRequestJustificationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.AccessJustificationSelector" %>
<asp:DropDownList ID="AccessRequestJustificationSelectorDropDownList" 
	runat="server" ClientIDMode="Static"
    SkinID="DropDownList340" OnSelectedIndexChanged="SetAccessRequestJustificationID"
    DataTextField="Name" DataValueField="ID" AppendDataBoundItems="True">
        <asp:ListItem Text="" Value="0" meta:resourceKey="SelectJustificationForAccess" />
</asp:DropDownList>
