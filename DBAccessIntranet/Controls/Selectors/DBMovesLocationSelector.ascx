﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DBMovesLocationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.DBMovesLocationSelector" %>
<asp:DropDownList ID="dbMovesLocationDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList300" OnSelectedIndexChanged="SetDBMovesLocationID"
    DataTextField="LocationName" DataValueField="LocationId" AppendDataBoundItems="true">
        <asp:ListItem  Value="0"  meta:resourcekey="ListItemResource"/>
</asp:DropDownList>


