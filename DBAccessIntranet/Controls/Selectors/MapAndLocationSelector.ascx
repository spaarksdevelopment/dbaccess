﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MapAndLocationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.MapAndLocationSelector" %>


<script type="text/javascript">
    $(document).ready(function () {
        $("#AmericasCountry").hide();
        $("#AsiaCountry").hide();
        $("#EmeaCountry").hide();
        $("#ImageAmericas").show();
        $("#ImageEmea").show();
        $("#ImageAsia").show();
        //$('#SelectionContentAccessSelection').hide();
    });

   

       function HideThis(countryNames, ImageName) {
           //hide all country 

           $("#AmericasCountry").hide();
           $("#AsiaCountry").hide();
           $("#EmeaCountry").hide();
           jQuery(countryNames).hide();         
           jQuery(ImageName).show();
           jQuery("#BreadCrumbsMap").text('');

           
       }

       function ShowCountries(countryName, ImageName, crumbName) 
       {

           //show all images first
           $("#ImageAmericas").show();
           $("#ImageEmea").show();
           $("#ImageAsia").show();
           $("#AmericasCountry").hide();
           $("#AsiaCountry").hide();
           $("#EmeaCountry").hide();
           //hide the one we are viewing countries for
           jQuery(ImageName).hide();
           jQuery(countryName).show();
           //jQuery("#BreadCrumbsMap").html('<a href=# onclick="GoBackRegion(&quot;' + countryName + '&quot;);">' + crumbName + '</a>');
           if (crumbName == 'Middle-East and North Africa' || crumbName == 'United Kingdom' || crumbName == 'Germany' || crumbName == 'Western Europe exluding Germany' || crumbName == 'Central and East Europe' || crumbName == 'Africa')
		   {
               crumbName = 'Europe, Middle East and Africa';
           }
           else if (crumbName == 'Japan' || crumbName == 'Asia pacific') 
		   {
               crumbName = 'Asia';
           }

          //  var Asia = '<%= GetMapDisplayText("Asia") %>';
          //  var EuropeAndMiddleEast = '<%= GetMapDisplayText("EuropeMiddleEastandAfrica") %>';
          //  var Americas = '<%= GetMapDisplayText("Americas") %>';

            switch (crumbName)
            {
            	case "Asia":
            		crumbName = '<%= GetMapDisplayText("Asia") %>';
            		break;
            	case "Europe, Middle East and Africa":
            		crumbName = '<%= GetMapDisplayText("EuropeMiddleEastandAfrica") %>';
            		break;
            	case "Americas":
            		crumbName = '<%= GetMapDisplayText("Americas") %>';
            		break;
            }
              
           jQuery("#BreadCrumbsMap").html(crumbName + '&nbsp;>>');

          
       }


       function GoBack(countryName, regionName) {
         
           //hide the access selection
           $('#SelectionContentAccessSelection').hide();
           //go back
           //need to show the cities for that country

         
           if (countryName.length > 0) {
               // var substr = i.split('||');

               var countryClick = countryName; // substr[0].toString();
               var tempRegionList = countryName.replace(/\s+/g, '');

               jQuery('#data-container div').removeClass('selected').css('display', 'none');
               jQuery('#MapImages').removeClass('selected').css('display', 'none');

               //now show the city list
               var regionList = '.' + tempRegionList + '-list';

               jQuery(regionList).addClass('selected').css('display', 'inline');

               var countryDisplay = regionName.replace('Country','');
               var tempBreadCrumbs;
               tempBreadCrumbs = '<a onclick="GoBackRegion(&quot;' + regionName + '&quot;);">' + countryDisplay + '</a>';
               tempBreadCrumbs = tempBreadCrumbs + '&nbsp;&nbsp;>>&nbsp;' + countryName ;

               jQuery("#BreadCrumbsMap").html(tempBreadCrumbs);
               scrollTo($('div ' + countryName), 800);
           }          

           //now show the city list
           var regionList = '.' + countryName + '-list';
           jQuery(regionList).addClass('selected').css('display', 'inline');

           //clear the hidden field value
           clearSelection();
           
       }

       function PopulateLocation(i, countryName, regionName, ImageName) {
       
           //split i using the pipe || symbol
           if (i.length > 0) {
               var substr = i.split('||');
               var locationId = substr[1].toString();
               var location = substr[0].toString();
               //set hidden field
               hfLocationDetail.Set("CityID", locationId);
               hfLocationDetail.Set("CityString", location);
               //set heading
               lblLocationHeading.SetText(location + " is selected");
               //set building list
               SetAccessSelector(locationId);
               jQuery('#data-container div').removeClass('selected').css('display', 'none');

               var tempAccessType = hfAccessDetail.Get("RequestType")

            
                   $('#SelectionContentAccessSelection').show();
               

               var ImageName;
               if (regionName == "Americas") {
                   ImageName = "AmericasCountry";
               }
               else if (regionName == "Asia") {
                   ImageName = "AsiaCountry";
               }
               else if (regionName == "Europe, Middle East and Africa") {
                   ImageName = "EmeaCountry";
               }

               var crumbName = regionName;
               if (crumbName == 'Middle-East and North Africa' || crumbName == 'United Kingdom' || crumbName == 'Germany' || crumbName == 'Western Europe exluding Germany' || crumbName == 'Central and East Europe' || crumbName == 'Africa') {
                   crumbName = '<%= GetMapDisplayText("EuropeMiddleEastandAfrica") %>'; //'Europe, Middle East and Africa';
               }
               else if (crumbName == 'Japan' || crumbName == 'Asia pacific') {
                   crumbName = '<%= GetMapDisplayText("Asia") %>'; //'Asia';
               }

//               var Asia = '<%= GetMapDisplayText("Asia") %>';
//               var EuropeAndMiddleEast = '<%= GetMapDisplayText("EuropeMiddleEastandAfrica") %>';
//               var Americas = '<%= GetMapDisplayText("Americas") %>';

               var temphtml = '<a id="' + crumbName + '" onclick="GoBackRegion(&quot;' + crumbName + '&quot;);">' + crumbName + '</a>';
               jQuery("#BreadCrumbsMap").html(temphtml + '&nbsp;>>&nbsp;<a onclick="GoBack(&quot;' + countryName + '&quot;,&quot;' + crumbName + '&quot;);">' + countryName + '</a>&nbsp;&nbsp;>>&nbsp;' + location);
               showSelector(locationId);
           
           }
           else {
               //set hidden field
               hfLocationDetail.Set("CityID", 0);
               hfLocationDetail.Set("CityString", '');
               //clear heading
               lblLocationHeading.SetText('');
               //clear building list
               SetAccessSelector('');
           }
         
       }

       function showSelector(cityID) {
           
           var requestType = hfAccessDetail.Get("RequestType");
               

               locationId = cityID;

               var requestTypeText = '';

               switch (requestType) {
                   case 0:
                       requestTypeText = 'Business Access';
                       $("#divAccess0").show();
                       $("#divAccess1").hide();
                       $("#divAccess2").hide();
                       $("#divAddAccess").hide();
                       break;
                   case 1:
                       requestTypeText = 'Service Access';
                       $("#divAccess0").hide();
                       $("#divAccess1").show();
                       $("#divAccess2").hide();
                       $("#divAddAccess").hide();
                       break;
                   case 2:
                       requestTypeText = 'Division Profile ';
                       $("#divAccess0").hide();
                       $("#divAccess1").hide();
                       $("#divAccess2").show();
                       $("#divAddAccess").show();
                       break;
               }

               if (!parseInt(locationId) > 0) {
                   $("#divAccess0").hide();
                   $("#divAccess1").hide();
                   $("#divAccess2").hide();
                   $("#divAddAccess").hide();
               }
               lblTypeHeading.SetText(requestTypeText);

               //check if a city was selected and set the access selector if so

              SetAccessSelector(locationId);
          

       }

       function GoBackRegion(regionName) {
   
           $('#SelectionContentAccessSelection').hide();
           if (regionName == "Americas") {
               $("#ImageAmericas").hide();
               $("#ImageEmea").show();
               $("#ImageAsia").show();
               $("#AmericasCountry").show();
               $("#AsiaCountry").hide();
               $("#EmeaCountry").hide();
           }
           else if (regionName == "Asia") {
               $("#ImageAmericas").show();
               $("#ImageEmea").show();
               $("#ImageAsia").hide();
               $("#AmericasCountry").hide();
               $("#AsiaCountry").show();
               $("#EmeaCountry").hide();
           }
           else if (regionName == "Europe, Middle East and Africa") {
               $("#ImageAmericas").show();
               $("#ImageEmea").hide();
               $("#ImageAsia").show();
               $("#AmericasCountry").hide();
               $("#AsiaCountry").hide();
               $("#EmeaCountry").show();
           }
           
           jQuery('#data-container div').removeClass('selected').css('display', 'none');
           jQuery('#MapImages').addClass('selected').css('display', 'inline');
           //now show the city list
           var regionList = '.' + regionName + '-list';
           jQuery(regionList).addClass('selected').css('display', 'inline');
           clearSelection();

           var countryDisplay = regionName.replace('Country', '');
           jQuery("#BreadCrumbsMap").html(countryDisplay + '&nbsp;>>');

         
       }

       function clearSelection() {
           //set hidden field
    
           hfLocationDetail.Set("CityID", 0);
           hfLocationDetail.Set("CityString", '');
           //clear heading
           lblLocationHeading.SetText('');
           //clear building list
           SetAccessSelector('');
       }

       function PopulationCities(i)
       {
     
       	//split i using the pipe || symbol
       	if (i.length > 0)
       	{
       		var substr = i.split('||');
       		var countryClick = substr[0].toString();

       		var tempRegionList = countryClick.replace(/\s+/g, '');
       		tempRegionList = tempRegionList.replace(',', '');

       		//alert(tempRegionList);

       		jQuery('#data-container div').removeClass('selected').css('display', 'none');
       		jQuery('#MapImages').removeClass('selected').css('display', 'none');

       		//now show the city list
       		var regionList = '.' + tempRegionList + '-list';
       		jQuery(regionList).addClass('selected').css('display', 'inline').css('cursor', 'pointer');

       		var crumbName = substr[1].toString();

       		if (crumbName == 'Middle-East and North Africa' || crumbName == 'United Kingdom' || crumbName == 'Germany' || crumbName == 'Western Europe exluding Germany' || crumbName == 'Central and East Europe' || crumbName == 'Africa')
       		{
       		    //crumbName = 'Europe, Middle East and Africa';
       		    crumbName = '<%= GetMapDisplayText("EuropeMiddleEastandAfrica") %>';
       		}
       		else if (crumbName == 'Japan' || crumbName == 'Asia pacific')
       		{
       		   // crumbName = 'Asia';
       		    crumbName = '<%= GetMapDisplayText("Asia") %>';
       		}

         
       		var temphtml = '<a id="' + regionList + '" onclick="GoBackRegion(&quot;' + crumbName + '&quot;);">' + crumbName + '</a>' + '&nbsp;>>&nbsp;' + countryClick + '';
       		jQuery("#BreadCrumbsMap").html(temphtml);
       	}
       }
</script>


<div class="ClearBoth"></div>
<div> <p><asp:Literal ID="UseNavigationalMenu" runat="server" meta:resourceKey="UseNavigationalMenu"></asp:Literal><p> </div>
<div id="BreadCrumbsMap" class="CursorHandPointer" ></div>
<br />
<div id="MapImages">

<div id="MapAmericas">
    <div id="ImageAmericas">
   
        <img alt="Americas" src="../App_Themes/DBIntranet2010/images/americas-image.gif" class="Americas" onclick="ShowCountries('#AmericasCountry','#ImageAmericas','Americas');" onmouseover="ShowCountries('#AmericasCountry','#ImageAmericas','Americas');" width="250px" />
   
   
    </div>
    <div id="AmericasCountry">
        
        <asp:Literal ID="litAmericasContainer" runat="server"></asp:Literal>
    </div>
</div>
<div class="mapSpacer"></div>
<div id="MapEmea">
     <div id="ImageEmea">
        <img alt="Europe, Middle East and Africa" src="../App_Themes/DBIntranet2010/images/emea-image.gif"  class="Emea"  onclick="ShowCountries('#EmeaCountry','#ImageEmea','Europe, Middle East and Africa');" onmouseover="ShowCountries('#EmeaCountry','#ImageEmea','Europe, Middle East and Africa');" width="250px" />
    </div>
    <div id="EmeaCountry">
                <asp:Literal ID="litEmeaContainer" runat="server"></asp:Literal>
    </div>
</div>
<div class="mapSpacer"></div>
<div id="MapAsia">
    <div id="ImageAsia">
        <img alt="Africa" src="../App_Themes/DBIntranet2010/images/asia-image.gif"  class="Asia"  onclick="ShowCountries('#AsiaCountry','#ImageAsia','Asia');" onmouseover="ShowCountries('#AsiaCountry','#ImageAsia', 'Asia');" width="250px" />
    </div>
    <div id="AsiaCountry">
       
        <asp:Literal ID="litAsiaContainer" runat="server"></asp:Literal>
    </div>
</div>

   
 
   
</div>
<asp:Literal ID="litLargeString" runat="server"></asp:Literal>

