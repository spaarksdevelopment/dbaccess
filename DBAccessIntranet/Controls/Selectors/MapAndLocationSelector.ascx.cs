﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Web.Caching;
using System.Text;
using System.Resources;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class MapAndLocationSelector : BaseControl
    {
        StringBuilder americasSb = new StringBuilder();
        StringBuilder emeaSb = new StringBuilder();
        StringBuilder asiaSb = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        StringBuilder tempCity = new StringBuilder();

        private int languageID
        {
            get { return Helper.SafeInt(this.CurrentUser.LanguageId); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ParseMapData();
        }


        private void ParseMapData()
        {
            List<WorldMapData> items = GetMapData(this.languageID);

            foreach (WorldMapData region in items)
            {                
                if (region.Type=="Re")
                {
                    string regionName = region.Name;
                    int regionID = region.ParentId;

                   
                    foreach (WorldMapData co in items)
                    {
                      
                        if (co.Type == "Co" && co.ParentId == regionID )
                        {
                            string country = co.Name.Replace(" ", "").Replace(",", "");                          

                                temp.Append("<div class='" + country + "-list' id='countryDiv'>");
                                temp.Append("<h4>" + co.Name + "</h4>");
                                temp.Append("<div id='cityDiv' class='" + country + "-list'>");
                                int cityCount = 0;

                                temp.Append("<div class='" + country + "-list' id='div1' style='float:left;background-color:#FFF;width:150px;'>");

                                foreach (WorldMapData city in items)
                                {
                                    if (city.Type == "Ci" && city.CountryID == co.CountryID)
                                    {
                                        if ((bool)city.Enabled)
                                        {
                                            temp.Append("<a id='" + city.Name + "||" + city.ParentId + "' onclick='PopulateLocation(this.id,&quot;" + co.Name + "&quot;,&quot;" + region.Name + "&quot;)' class='cityName' >" + city.Name + "</a><br />");
                                        }
                                        else
                                        {
                                            temp.Append("<p>" + city.Name + "</p>");
                                        }
                                        cityCount++;

                                        if (cityCount % 10 == 0)
                                        {
                                            temp.Append("</div><div class='" + country + "-list' id='div" + cityCount + "' style='float:left;background-color:#FFF;width:150px;'>");
                                        }
                                    }
                                }

                                temp.Append("</div>");
                                temp.Append("</div></div>");

                                if ((bool)co.Enabled)
                                {
                                    // add the country name
                                    string tempID = co.Name + "||" + region.Name;
                                    tempCity.Append("<a style='cursor:pointer;' onclick='PopulationCities(&quot;" + tempID.Replace(",", "") + "&quot;)' id='" + co.Name + "' >" + co.Name + "</a><br />");
                                }
                                else
                                {
                                    tempCity.Append("<div>" + co.Name + "</div>");
                                }                            
                        }
                    }

                    appendData(regionName, tempCity);
                    tempCity.Clear();
                }

            }
            litAmericasContainer.Text = "<h4>" + GetMapDisplayText("Americas") + "</h4><div id='regioncities'>" + americasSb.ToString() + "</div>";
            litAsiaContainer.Text = "<h4>APAC</h4><div id='regioncities'>" + asiaSb.ToString() + "</div>";
            //litEmeaContainer.Text = "<h4>EMEA</h4><div id='regioncities'>" + emeaSb.ToString() + "</div>";
            litEmeaContainer.Text = "<h4>EMEA</h4><div id='regioncities' style='max-height:350px; overflow:scroll;'>" + emeaSb.ToString() + "</div>";

            litLargeString.Text = "<div id='data-container'>" +temp.ToString() + "</div>";
        }

        void appendData(string regionName, StringBuilder dataToAppend)
        {
            if (regionName.ToLower().Contains("americas"))
            {
                americasSb.Append(dataToAppend);             
            }
            else if (regionName.ToLower().Contains("asia"))
            { 
                asiaSb.Append(dataToAppend);
            }
            else if (regionName.ToLower().Contains("germany"))
            { 
                emeaSb.Append(dataToAppend);
            }
            else if (regionName.ToLower().Contains("middle-east"))
            { 
                emeaSb.Append(dataToAppend);
            }
            else if (regionName.ToLower().Contains("united kingdom"))
            {
                emeaSb.Append(dataToAppend);
            }
            else if (regionName.ToLower().Contains("emea"))
            {
                emeaSb.Append(dataToAppend);
            }
            else if (regionName.ToLower().Contains("apac"))
            {
                asiaSb.Append(dataToAppend);
            }
        }

        private List<WorldMapData> GetMapData(int LanguageID)
        {
            //  If no language has been specified, the default to english
            if (LanguageID == 0)
            {
                LanguageID = 1;
            }

            List<WorldMapData> items = new List<WorldMapData>();
            items = DBAccessController.Director.GetMapData(LanguageID);
            return items;
        }


		public string GetMapDisplayText(object resourceName)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			string strResourceValue = string.Empty;
			switch ((string)resourceName)
			{
				case "Americas":
					{
						strResourceValue = resourceManager.GetString("Americas");
						break;
					}
				case "EuropeMiddleEastandAfrica":
					{
						strResourceValue = resourceManager.GetString("EuropeMiddleEastandAfrica");
						break;
					}
				case "Asia":
					{
						strResourceValue = resourceManager.GetString("Asia");
						break;
					}
				default:
					{
						break;
					}
			}
			return strResourceValue;
		}
    }
}