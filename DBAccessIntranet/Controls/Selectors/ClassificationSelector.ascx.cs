﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Threading;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class ClassificationSelector : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);
			base.CurrentUser.LanguageId = Convert.ToInt32(LanguageId);

            if (!Page.IsPostBack)
            {
                Bind();
            }
        }

        public int? HRClassificationID { get; set; }

        public void Bind()
        {
			var Classes = Director.GetClassificationsEnabled(base.CurrentUser.LanguageId);
            classificationsDropDownList.DataSource = Classes;
            classificationsDropDownList.DataBind();

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HRClassificationID.HasValue)
            {
                var item = classificationsDropDownList.Items.FindByValue(HRClassificationID.Value.ToString());
                if (item != null) item.Selected = true;
            }
        }

        /// <summary>
        /// method to set the SetClassificationID property on index changed
        /// E.Parker
        /// 24.05.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetHRClassificationID(object sender, EventArgs e)
        {
            HRClassificationID = Convert.ToInt32(classificationsDropDownList.SelectedValue);
        }


    }
}