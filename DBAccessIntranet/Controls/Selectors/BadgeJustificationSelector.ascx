﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BadgeJustificationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.BadgeJustificationSelector" %>
<asp:DropDownList ID="BadgeJustificationSelectorDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList220" OnSelectedIndexChanged="SetBadgeJustificationID"
    DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true">
        <asp:ListItem  Value="0" meta:resourceKey="SelectJustificationForReplacement" />
</asp:DropDownList>


