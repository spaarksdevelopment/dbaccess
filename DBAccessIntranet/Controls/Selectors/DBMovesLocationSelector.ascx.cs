﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class DBMovesLocationSelector : System.Web.UI.UserControl
    {
		public int? DBMovesLocationID { get; set; }

        public int? DBMovesRegionID { get; set; }

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);

            if (null != ViewState["DBMovesLocationID"])
			{
                DBMovesLocationID = (Int32)ViewState["DBMovesLocationID"];
			}
		}

		protected override object SaveViewState()
		{
            ViewState.Add("DBMovesLocationID", DBMovesLocationID);
			return base.SaveViewState();
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Bind();
            }
        }

		public void Bind()
        {
            var locations = Director.GetDBMovesLocations(DBMovesRegionID);
            dbMovesLocationDropDownList.DataSource = locations;
            dbMovesLocationDropDownList.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var item = dbMovesLocationDropDownList.Items.FindByValue(DBMovesLocationID.ToString());
			
            if (item != null) item.Selected = true;
        }

        protected void SetDBMovesLocationID(object sender, EventArgs e)
        {
            DBMovesLocationID = Convert.ToInt32(dbMovesLocationDropDownList.SelectedValue);
        }
    }
}