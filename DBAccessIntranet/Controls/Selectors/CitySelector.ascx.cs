﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;

using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class CitySelector : System.Web.UI.UserControl
    {
        public int? CountryID { get; set; }

        public int? CityID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateCityDropdown();
            }
        }

        #region Manage ViewState Variables
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            if (null != ViewState["CityID"])
            {
                CityID = (Int32)ViewState["CityID"];
            }
        }

        protected override object SaveViewState()
        {
            ViewState.Add("CityID", CityID);
            return base.SaveViewState();
        }

        #endregion

        #region Data Bind
        private void PopulateCityDropdown()
        {
            bool isCountryEnabled = false;
            if (CountryID.HasValue && CountryID.Value > 0)
            {
                isCountryEnabled = Director.IsCountryEnabled(CountryID.Value);
                if (isCountryEnabled)
                {
                    BindCityDropDown();
                }
                else
                {
                    CityDropDownList.Items.Clear();
                }
            }
            else
            {
                CityDropDownList.Items.Clear();
            }
            AddDefaultSelectionToCityDropdown();
        }

        private void AddDefaultSelectionToCityDropdown()
        {
            string defaultSelect = GetLocalResourceObject("ListItemResource.Text").ToString();
            CityDropDownList.Items.Insert(0, new ListItem(defaultSelect, "0"));
        }

        private void BindCityDropDown()
        {
            var cities = Director.GetCitiesForCountry(CountryID.Value, true);
            CityDropDownList.DataSource = cities;
            CityDropDownList.DataBind();
        }

        #endregion

        #region Public Methods

        public void ReloadCities()
        {
            CityID = 0;
            PopulateCityDropdown();
        }

        #endregion
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (CityID.HasValue && CityID.Value > 0)
            {
                var item = CityDropDownList.Items.FindByValue(CityID.Value.ToString());
                if (item != null)
                { 
                    item.Selected = true;
                }
                else
                {
                    CityID = 0;
                }
            }
        }

        protected void SetCityID(object sender, EventArgs e)
        {
            CityID = Convert.ToInt32(CityDropDownList.SelectedValue);
        }
    }
}