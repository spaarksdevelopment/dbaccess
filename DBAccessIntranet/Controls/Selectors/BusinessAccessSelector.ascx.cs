﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxClasses;
using System.Collections;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Linq;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class BusinessAccessSelector : BaseControl
    {
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
		protected void ASPxListBoxBuilding_Callback(object source, CallbackEventArgsBase e)
		{
			ASPxListBox lb = source as ASPxListBox;
			lb.Items.Clear();

			int? CityId = Helper.SafeIntNullable(e.Parameter);
			if (CityId.HasValue)
			{
				var buildingList = Director.GetBuildingsEnabledForCity(CityId.Value, true, true);

                buildingList.Sort();

				foreach (var Building in buildingList)
					lb.Items.Add(Building.Name, Building.BuildingID);

                //new sort method E.Parker Feb 2011
                //string[] newArray = sortedArray(lb.Items);
                //lb.Items.Clear();
                //lb.Items.AddRange(newArray);
                reGetBuildingID(lb.Items, buildingList);
                
			}
		}

		protected void ASPxListFloor_Callback(object source, CallbackEventArgsBase e)
		{
			ASPxListBox lb = source as ASPxListBox;
			lb.Items.Clear();

			int? BuildingId = Helper.SafeIntNullable(e.Parameter);
			if (BuildingId.HasValue)
			{
				var FloorList = Director.GetFloorsForBuilding(BuildingId, true, true, true);

				foreach (var Floor in FloorList)
					lb.Items.Add(Floor.Name, Floor.FloorID);

				//new sort method E.Parker Feb 2011
				string[] newArray = sortedArray(lb.Items);
				lb.Items.Clear();
				lb.Items.AddRange(newArray);
				reGetFloorID(lb.Items, FloorList);
			}
		}

		protected void ASPxListBoxAccessArea_Callback(object source, CallbackEventArgsBase e)
        {
            ASPxListBox lb = source as ASPxListBox;
            lb.Items.Clear();

            int? FloorId = Helper.SafeIntNullable(e.Parameter);
            if (FloorId.HasValue)
            {
                var AccessAreaList = Director.GetAccessAreasForFloor(FloorId, true, true);

                foreach (var AccessArea in AccessAreaList)
                    lb.Items.Add(AccessArea.Name, AccessArea.AccessAreaID);

                //new sort method E.Parker Feb 2011
                string[] newArray = sortedArray(lb.Items);
                lb.Items.Clear();
                lb.Items.AddRange(newArray);
                reGetAccessID(lb.Items, AccessAreaList);
               
            }
        }

        /// <summary>
        /// Method to get the access ID 
        /// </summary>
        /// <param name="itemCollection"></param>
        /// <param name="FloorList"></param>
        protected void reGetAccessID(ListEditItemCollection itemCollection, List<DBAccessController.DAL.AccessArea> AccessAreaList)
        {
            foreach (DevExpress.Web.ASPxEditors.ListEditItem item in itemCollection)
            {
                var q = from i in AccessAreaList
                        where i.Name == item.Text
                        orderby i.Name
                        select i;
                List<DBAccessController.DAL.AccessArea> items = q.ToList();
                item.Value = items[0].AccessAreaID;
            }
        }

        /// <summary>
        /// Method to get the floor ID 
        /// </summary>
        /// <param name="itemCollection"></param>
        /// <param name="FloorList"></param>
        protected void reGetFloorID(ListEditItemCollection itemCollection, List<DBAccessController.DAL.LocationFloor> FloorList)
        {
            foreach (DevExpress.Web.ASPxEditors.ListEditItem item in itemCollection)
            {
                var q = from i in FloorList
                        where i.Name == item.Text
                        orderby i.Name
                        select i;
                List<DBAccessController.DAL.LocationFloor> items = q.ToList();
                item.Value = items[0].FloorID;
            }
        }


        /// <summary>
        /// Method to get the buildingID
        /// </summary>
        /// <param name="itemCollection"></param>
        /// <param name="FloorList"></param>
        protected void reGetBuildingID(ListEditItemCollection itemCollection, List<DBAccessController.DAL.LocationBuilding> BuildingList)
        {
            foreach (DevExpress.Web.ASPxEditors.ListEditItem item in itemCollection)
            {
                var q = from i in BuildingList
                        where i.Name == item.Text
                        orderby i.Name
                        select i;
                List<DBAccessController.DAL.LocationBuilding> items = q.ToList();
                item.Value = items[0].BuildingID;
            }
        }

        /// <summary>
        /// Method to split the item collection sort and put back together with strings at top of list then numerical values
        /// </summary>
        /// <param name="itemCollection"></param>
        /// <returns></returns>
        protected string[] sortedArray(ListEditItemCollection itemCollection)
        {
            List<object> stringList = new List<object>();
            List<object> numList = new List<object>();

            foreach (DevExpress.Web.ASPxEditors.ListEditItem item in itemCollection)
            {
                bool c1 = Char.IsDigit(item.Text, 0);
                if (!c1)
                {
                    stringList.Add(item.Text);

                }
                else
                {
                    numList.Add(item.Text);


                }
            }
            //create temp Arrays
            string[] stringArray = new string[stringList.Count];
            string[] numArray = new string[numList.Count];


            NumericComparer ns = new NumericComparer();
            ns = new NumericComparer();
            //copy all string values
            stringList.CopyTo(stringArray, 0);
            Array.Sort(stringArray, ns);
            //copy all numeric values
            numList.CopyTo(numArray, 0);
            Array.Sort(numArray, ns);

            //create final array
            int newLength = stringList.Count + numList.Count;
            string[] newArray = new string[newLength];
            //copy accross final values
            stringArray.CopyTo(newArray, 0);
            numArray.CopyTo(newArray, stringList.Count);

            return newArray;
        }

       
    }
}