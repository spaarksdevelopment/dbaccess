﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileAccessSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.ProfileAccessSelector" %>

<script type="text/javascript">
<!--
    function setProfileAccessSelector(cityId) {
        //get the list of divisions
        lbProfile.PerformCallback(cityId);

        //hide access area
        $("#divlbServiceAccess").hide();

        //clear selection as well
        lbServiceAccess.SetSelectedIndex(-1);
    }  
    
//-->
 </script>

       <div class="clearBoth">   
       
         <div id="float:left;width:200px;" style="float:left;width:400px;" class="marginB10 marginL10">
            <div class="form_label" style="width:400px"><asp:Literal ID="Division" Text="<%$ Resources:CommonResource, Division%>" runat="server"></asp:Literal></div><br />
            <dx:ASPxListBox ID="ASPxListBoxProfile" ClientInstanceName="lbProfile" runat="server" Width="400px" Height="200px" OnCallback="ASPxListBoxProfile_Callback">     
                <ClientSideEvents ValueChanged="function(s,e) { $('#divAddAccess').show(); }" />           
            </dx:ASPxListBox>
         </div>
     </div>
     