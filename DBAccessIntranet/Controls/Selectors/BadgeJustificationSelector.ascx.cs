﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DBAccess.Model.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
	public partial class BadgeJustificationSelector : BaseControl
    {
		public int BadgeJustificationID { get; set; }

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);

			if (null != ViewState["BadgeJustificationID"])
			{
				BadgeJustificationID = (Int32) ViewState["BadgeJustificationID"];
			}
		}

		protected override object SaveViewState()
		{
			ViewState.Add("BadgeJustificationID", BadgeJustificationID);
			return base.SaveViewState();
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			Bind();
        }

		public void Bind()
        {
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? languageID = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);
			base.CurrentUser.LanguageId = Convert.ToInt32(languageID);

            IBadgeRequestService badgeRequestService = new BadgeRequestService(base.CurrentUser.dbPeopleID);
            List<GetBadgeJustificationReasons_Result> reasons = badgeRequestService.GetBadgeJustificationReasons(languageID);

			BadgeJustificationSelectorDropDownList.DataSource = reasons;
			BadgeJustificationSelectorDropDownList.DataBind();
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

			var item = BadgeJustificationSelectorDropDownList.Items.FindByValue(BadgeJustificationID.ToString());
				if (item != null) item.Selected = true;
            
        }

        protected void SetBadgeJustificationID(object sender, EventArgs e)
        {
			BadgeJustificationID = Convert.ToInt32(BadgeJustificationSelectorDropDownList.SelectedValue);
        }
    }
}