﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxClasses;


using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class ServiceAccessSelector : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ASPxListBoxBuilding_Callback(object source, CallbackEventArgsBase e)
        {
            ASPxListBox lb = source as ASPxListBox;
            lb.Items.Clear();

            int? CityId = Helper.SafeIntNullable(e.Parameter);
            if (CityId.HasValue)
            {
                var BuildingList = Director.GetServiceBuildingsEnabledForCity(CityId.Value);

                foreach (var Building in BuildingList)
                    lb.Items.Add(Building.Name, Building.BuildingID);
            }
        }

        protected void ASPxListBoxServiceAccess_Callback(object source, CallbackEventArgsBase e)
        {
            ASPxListBox lb = source as ASPxListBox;
            lb.Items.Clear();

            int? BuildingId = Helper.SafeIntNullable(e.Parameter);
            if (BuildingId.HasValue)
            {
                var AccessAreaList = Director.GetServiceAccessAreasForBuilding(BuildingId, true);

                foreach (var AccessArea in AccessAreaList)
                    lb.Items.Add(AccessArea.Name, AccessArea.AccessAreaID);
            }
        }
    }
}