﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UBRSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.UBRSelector" %>

<script type="text/javascript">
//<!--

	function GetUBRKeyValue(sKey)
	{
		if (sKey == '')
		{
			sKey = $('#TextBoxUBRKey').val();
		}
		else
		{
			$('#TextBoxUBRKey').val(sKey);
		}

		if (sKey == '')
		{
			$('#TextBoxUBRKeyValue').val('');
		}
		else
		{
			var sValue = GetUBRTitle(sKey);

			if (sValue == '')
			{
			    popupAlertTemplateContent.SetText("<%=UBRSelectorError.Text %>"); //UBR Key not found.
			    popupAlertTemplate.SetHeaderText("<%=UBRSelectorKey.Text %>");
				popupAlertTemplate.Show();

				$('#TextBoxUBRKey').val('');
				$('#TextBoxUBRKeyValue').val('');
			}
			else
			{
				$('#TextBoxUBRKeyValue').val(sValue);
			}
		}
	}

	function ClearUBRSelector()
	{
		$('#TextBoxUBRKey').val('');
		$('#TextBoxUBRKeyValue').val('');
	}

	//-->
</script>

        <asp:textbox runat="server" ID="TextBoxUBRKey" ClientIDMode="Static" SkinID="TextBox60" onblur="GetUBRKeyValue('')"  ValidationGroup="CriteriaValidators"></asp:textbox>
        <asp:Image ID="ImageGetUBRKey" runat="server" SkinID="Search" onclick="GetUBRKeyValue('')" />
        <asp:textbox runat="server" ID="TextBoxUBRKeyValue"  ClientIDMode="Static" SkinID="TextBox180" ReadOnly="true"></asp:textbox>
                                      
             <asp:TreeView runat="server" ID="TreeViewUBR" BackColor="White" BorderColor="Silver"
                    ExpandDepth="1" 
                    PopulateNodesFromClient="true" 
                    EnableClientScript="true"                
                    OnTreeNodePopulate="TreeViewUBR_TreeNodePopulate">          
             </asp:TreeView>

             
     <asp:Literal ID="UBRSelectorError"  meta:resourcekey="UBRSelectorError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="UBRSelectorKey"  meta:resourcekey="UBRSelectorKey" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>