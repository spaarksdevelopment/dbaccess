﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassificationSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.ClassificationSelector" %>
<asp:DropDownList ID="classificationsDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList220" OnSelectedIndexChanged="SetHRClassificationID"
    DataTextField="Classification" DataValueField="HRClassificationID" AppendDataBoundItems="true">
        <asp:ListItem Text="Select Classification..." Value="0" meta:resourcekey="ListItemResource"/>
</asp:DropDownList>
