﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DBMovesFloorSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.DBMovesFloorSelector" %>
<asp:DropDownList ID="dbMovesFloorDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList220" OnSelectedIndexChanged="SetDBMovesFloorID"
    DataTextField="FloorNo" DataValueField="LocationFloorId" AppendDataBoundItems="true">
        <asp:ListItem  Value="0" meta:resourcekey="ListItemResource"/>
</asp:DropDownList>


