﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessAccessSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.BusinessAccessSelector" %>

 <script type="text/javascript">
    <!--

     function setBusinessAccessSelector(cityId) {
     	if (cityId != "") {
     		//get the list of buildings
     		lbBuilding.PerformCallback(cityId);
     	}

         //hide floor and access area
         $("#divlbFloor").hide();
         $("#divlbAccessArea").hide();

         //clear selection as well
         lbFloor.SetSelectedIndex(-1);
         lbAccessArea.SetSelectedIndex(-1);

     }

     function setBusinessAccessSelectorBuilding() {

         $("#divAddAccess").hide();

         var buildingId = lbBuilding.GetValue().toString();

         if (buildingId == '') {
            //no building selected, hide and deselect floor and access area
             $("#divlbFloor").hide();
             $("#divlbAccessArea").hide();
             lbFloor.SetSelectedIndex(-1);
             lbAccessArea.SetSelectedIndex(-1);
         } else {
             //hide access area and clear selection
             $("#divlbAccessArea").hide();
             lbAccessArea.SetSelectedIndex(-1);
            
             //show the floor selector
             $("#divlbFloor").show();             
             
             //get the floors
             lbFloor.PerformCallback(buildingId);
         }

        $("#divlbAccessArea").hide();         
     }

     function setBusinessAccessSelectorFloor() {
         var visitor = hfPersonDetail.Get("Visitor");
         
         $("#divAddAccess").hide();

         var floorId = lbFloor.GetValue().toString();

         if (floorId == '') {
             $("#divlbAccessArea").hide();
             lbAccessArea.SetSelectedIndex(-1);
         } else if (visitor == 'false') {
             $("#divlbAccessArea").show();
             lbAccessArea.PerformCallback(floorId);
         }        
     }

     function getAccessBusinessDetails() {
         //get the accessAreaID
         var accessAreaID = lbAccessArea.GetValue().toString();
         PageMethods.GetAccessEndDate(accessAreaID, onGetAccessEndDate);
     }
    //-->
 </script>

<div class="clearBoth">
	<div id="divlbBuilding" style="float:left;width:300px;" class="marginB10 marginL10">
		<div class="form_label" style="width:300px;height:20px;"><asp:Literal ID="BuildingAreaSelectorBld" meta:resourcekey="BuildingAreaSelectorBld" runat="server"></asp:Literal> </div>
		<br />
		<dx:ASPxListBox ID="ASPxListBoxBuilding" ClientInstanceName="lbBuilding" runat="server"
			Width="300px"
			Height="200px"
			OnCallback="ASPxListBoxBuilding_Callback"
			ItemStyle-Cursor="pointer">
			<ClientSideEvents ValueChanged="function(s,e) { setBusinessAccessSelectorBuilding(); }" />
		</dx:ASPxListBox>
	</div>
	<div id="divlbFloor" style="float:left;width:100px;" class="marginB10 marginL10">
		<div class="form_label" style="width:100px;height:20px;"><asp:Literal ID="BuildingAreaSelectorFloor" meta:resourcekey="BuildingAreaSelectorFloor" runat="server"></asp:Literal></div>
		<br />
		<dx:ASPxListBox ID="ASPxListBoxFloor" ClientInstanceName="lbFloor" runat="server"
			Width="100px"
			Height="200px"
			OnCallback="ASPxListFloor_Callback"
			ItemStyle-Cursor="pointer">
			<ClientSideEvents ValueChanged="function(s,e) { setBusinessAccessSelectorFloor(); }" />    
		</dx:ASPxListBox>
	</div>
	<div id="divlbAccessArea" style="float:left;width:200px;" class="marginB10 marginL10">
		<div class="form_label"style="width:200px;height:20px;"><asp:Literal ID="BuildingAreaSelectorAccess" meta:resourcekey="BuildingAreaSelectorAccess" runat="server"></asp:Literal></div>
		<br />
		<dx:ASPxListBox ID="ASPxListBoxAccessArea" ClientInstanceName="lbAccessArea" runat="server"
			Width="200px"
			Height="200px"
			OnCallback="ASPxListBoxAccessArea_Callback"
			SelectionMode="Single"
			ItemStyle-Cursor="pointer">
			<ClientSideEvents ValueChanged="function(s,e) {getAccessBusinessDetails(); }" />
		</dx:ASPxListBox>
	</div>
</div>
