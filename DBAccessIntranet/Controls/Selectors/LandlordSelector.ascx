﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandlordSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.LandlordSelector" %>
<asp:DropDownList ID="LandlordDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList300" OnSelectedIndexChanged="SetLandLordID"
    DataTextField="Name" DataValueField="LandlordID" AppendDataBoundItems="true">
        <asp:ListItem  Value="0" meta:resourcekey="ListItemResource"/>
</asp:DropDownList>


