﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountrySelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.CountrySelector" %>
<asp:DropDownList ID="CountryDropDownList" runat="server" ClientIDMode="Static" AutoPostBack="True"
    SkinID="DropDownList240" OnSelectedIndexChanged="SetCountryID"
    DataTextField="Name" DataValueField="CountryID" AppendDataBoundItems="True">
        <asp:ListItem Text="Select a Country..." Value="0" meta:resourcekey="ListItemResource"/>
</asp:DropDownList>
