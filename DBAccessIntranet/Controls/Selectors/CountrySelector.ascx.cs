﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class CountrySelector : System.Web.UI.UserControl
    {
        public delegate void OnCountryChanged(object o, CountryChangeEventArg e);

        public event OnCountryChanged CountryChanged;

        public int? CountryID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCountryDropDown();
            }
        }

        #region Manage ViewState Variables
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            if (null != ViewState["CountryID"])
            {
                CountryID = (Int32)ViewState["CountryID"];
            }
        }

        protected override object SaveViewState()
        {
            ViewState.Add("CountryID", CountryID);
            return base.SaveViewState();
        }

        #endregion
        
        private void BindCountryDropDown()
        {
            var countries = Director.GetCountriesAllEnabled();
            CountryDropDownList.DataSource = countries;
            CountryDropDownList.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (CountryID.HasValue)
            {
                var item = CountryDropDownList.Items.FindByValue(CountryID.Value.ToString());
                if (item != null) item.Selected = true;
            }
        }

        protected void SetCountryID(object sender, EventArgs e)
        {
            CountryID = Convert.ToInt32(CountryDropDownList.SelectedValue);

            if (CountryChanged != null)
            {
                CountryChangeEventArg evntArg = new CountryChangeEventArg();
                evntArg.CountryID = CountryID.Value;
                CountryChanged(this, evntArg);
            }
        }
    }

    public class CountryChangeEventArg : EventArgs
    {
        public int CountryID { get; set; }
    }
}