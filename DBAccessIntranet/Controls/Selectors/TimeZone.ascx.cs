﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class TimeZone : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind();
        }

        public int? TimeZoneID { get; set; }

        public void Bind()
        {
            var TZs = Director.GetAllTimeZones();
            timezonesDropDownList.DataSource = TZs;
            timezonesDropDownList.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (TimeZoneID.HasValue)
            {
                var item = timezonesDropDownList.Items.FindByValue(TimeZoneID.Value.ToString());
                if (item != null) item.Selected = true;
            }
        }

        protected void setValue(object sender, EventArgs e)
        {
            TimeZoneID = Convert.ToInt32(timezonesDropDownList.SelectedValue);
        }
    }
}