﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeZone.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.TimeZone" %>

<asp:DropDownList ID="timezonesDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList220" OnSelectedIndexChanged="setValue"
    DataTextField="Name" DataValueField="TimeZoneID" AppendDataBoundItems="true">
        <asp:ListItem Text="Select Time Zone..." Value="0" />
</asp:DropDownList>