﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSA = Microsoft.Security.Application;

using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
    public partial class UBRSelector : BaseControl
    {
        #region Properties
        public string UBRKey
        {
			get { return MSA.Encoder.HtmlEncode(TextBoxUBRKey.Text); }
            set { TextBoxUBRKey.Text = value; }
        }
        
        protected string _StartNode = string.Empty;
        public string StartNode
        {
            get { return _StartNode; }
            set { _StartNode = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {   
            if (!IsPostBack)
            {
                PopulateRootLevel();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);           
        }

        #region control events
        
        protected void TreeViewUBR_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            PopulateSubLevel(e.Node.Value, e.Node);
        }

        #endregion

        #region private methods

        public void PopulateRootLevel()
        {
            if(_StartNode.Length>0)
                PopulateRootLevel(_StartNode);
        }

        private void PopulateRootLevel(string parentid)
        {
            List<DBAccessController.DAL.UBRNodes> dt = GetData(parentid, true);
            PopulateNodes(dt, TreeViewUBR.Nodes);
           
        }

        private void PopulateSubLevel(string parentid, TreeNode parentNode)
        {
            List<DBAccessController.DAL.UBRNodes> dt = GetData(parentid, false);
            PopulateNodes(dt, parentNode.ChildNodes);
        }


        private void PopulateNodes(List<DBAccessController.DAL.UBRNodes> dt, TreeNodeCollection nodes)
        {
            foreach (var dr in dt)
            {
                TreeNode tn = new TreeNode();
                tn.Text = dr.title;
                tn.Value = dr.id.ToString();
                nodes.Add(tn);

                //If node has child nodes, then enable on-demand populating
                tn.PopulateOnDemand = ((int)(dr.childnodecount) > 0);
                tn.NavigateUrl = string.Format("javascript:GetUBRKeyValue('{0}');", tn.Value);
            }
        }

        private List<DBAccessController.DAL.UBRNodes> GetData(string ParentNode, bool IsRoot)
        {
            List<DBAccessController.DAL.UBRNodes> dt;

            string CacheId = IsRoot ? "UBRRoot_" : "UBRNode_";
            CacheId += ParentNode;

            if (Cache[CacheId] == null)
            {
                dt = Director.GetUBRNodes(ParentNode, IsRoot);

                Cache.Insert(CacheId, dt);
            }
            else
            {
                dt = (List<DBAccessController.DAL.UBRNodes>)Cache[CacheId];
            }

            return dt;
        }

        #endregion
    }
}