﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServiceAccessSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.ServiceAccessSelector" %>

<script type="text/javascript">
    $(document).ready(function () {
        //main help section
        $('#HelpServiceBuilding').bind('click', function () {
            $("#HelpServiceAccess").slideToggle("slow");
        });
        $("#HelpServiceAccess").hide();
    });
</script>
 <script type="text/javascript">
    <!--

     function setServiceAccessSelector(cityId) {

         //get the list of buildings
         lbServiceBuilding.PerformCallback(cityId);

         //hide access area
         $("#divlbServiceAccess").hide();

         //clear selection as well
         lbServiceAccess.SetSelectedIndex(-1);
     }

     function setServiceAccessSelectorBuilding() {

         $("#divAddAccess").hide();

         var buildingId = lbServiceBuilding.GetValue().toString();

         if (buildingId == '') {
             //no building selected, hide and deselect access area
             $("#divlbServiceAccess").hide();
             lbServiceAccess.SetSelectedIndex(-1);
         } else {
             //show access areas
             $("#divlbServiceAccess").show();
             
             //get the service access areas
             lbServiceAccess.PerformCallback(buildingId);
         }         
     }
     function getServiceAccessDetails() {
         //get the accessAreaID
         var accessAreaID = lbServiceAccess.GetValue().toString();

         PageMethods.GetAccessEndDate(accessAreaID, onGetAccessEndDate);
     }
    //-->
 </script>

      <div class="clearBoth">   
         <div id="float:left;width:200px;" style="float:left;width:400px;" class="marginB10 marginL10">
            <div class="form_label" style="width:400px;height:20px;"><asp:Literal ID="Building" Text="<%$ Resources:CommonResource, Building%>" runat="server"></asp:Literal>&nbsp;<img id="HelpServiceBuilding" src="../../App_Themes/DBIntranet2010/images/question.gif"  title="<%$ Resources:CommonResource, divAdminImgTitle%>" alt="<%$ Resources:CommonResource, divAdminImgTitle%>"  runat="server" style="Padding:0,0,0,0;"/></div><br />
            <dx:ASPxListBox ID="ASPxListBoxBuilding" ClientInstanceName="lbServiceBuilding" runat="server" Width="400px" Height="200px" OnCallback="ASPxListBoxBuilding_Callback">     
                <ClientSideEvents ValueChanged="function(s,e) { setServiceAccessSelectorBuilding(); }" />           
            </dx:ASPxListBox>
         </div>
         <div id="divlbServiceAccess" style="float:left;width:200px;" class="marginB10 marginL10">
            <div class="form_label"style="width:200px;height:20px;"><asp:Literal ID="ServiceAccess" Text="<%$ Resources:CommonResource, ServiceAccess%>" runat="server"></asp:Literal></div><br />
            <dx:ASPxListBox ID="ASPxListBoxServiceAccess" ClientInstanceName="lbServiceAccess" runat="server" Width="200px" Height="200px" OnCallback="ASPxListBoxServiceAccess_Callback" SelectionMode="Single">
                <ClientSideEvents ValueChanged="function(s,e) { getServiceAccessDetails(); }" />  
            </dx:ASPxListBox>
         </div>
         <div id="HelpLabel" style="float:left;width:200px;" class="marginB10 marginL10">
            <div id="HelpServiceAccess" class="HelpPopUp">
            <p><asp:Literal ID="ServiceAreaSelectorMsg" meta:resourcekey="ServiceAreaSelectorMsg" runat="server"></asp:Literal></p>
        </div>
        </div>
     </div>