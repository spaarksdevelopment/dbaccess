﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CitySelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.CitySelector" %>
<asp:DropDownList ID="CityDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList240" OnSelectedIndexChanged="SetCityID"
    DataTextField="Name" DataValueField="CityID">      
</asp:DropDownList>