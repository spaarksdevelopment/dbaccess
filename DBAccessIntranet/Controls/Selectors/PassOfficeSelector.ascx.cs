﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;
using Spaarks.CustomMembershipManager;
using System.Threading;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
	public partial class PassOfficeSelector : BaseControl
    {
		public int PassOfficeID { get; set; }

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);

			if (null != ViewState["PassOfficeID"])
			{
				PassOfficeID = (Int32) ViewState["PassOfficeID"];
			}
		}

		protected override object SaveViewState()
		{
			ViewState.Add("PassOfficeID", PassOfficeID);
			return base.SaveViewState();
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			Bind();
        }

		public void Bind()
        {
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);

			var POs = Director.GetAllPassOfficesEnabled(LanguageId);

            passofficesDropDownList.DataSource = POs;
            passofficesDropDownList.DataBind();
        
        }

	

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
           
				var item = passofficesDropDownList.Items.FindByValue(PassOfficeID.ToString());
				if (item != null) item.Selected = true;
            
        }

        /// <summary>
        /// method to set the passoffice ID property on index changed
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetPassOfficeID(object sender, EventArgs e)
        {
            PassOfficeID = Convert.ToInt32(passofficesDropDownList.SelectedValue);
        }


    }
}