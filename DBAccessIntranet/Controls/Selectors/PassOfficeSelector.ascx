﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassOfficeSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.PassOfficeSelector" %>
<asp:DropDownList ID="passofficesDropDownList" runat="server" ClientIDMode="Static"
    SkinID="DropDownList220" OnSelectedIndexChanged="SetPassOfficeID"
    DataTextField="DisplayName" DataValueField="PassOfficeID" 
	AppendDataBoundItems="True">
        <asp:ListItem Text="select pass office..." Value="0" meta:resourcekey="ListItemResourcePassOfficeDefault"/>
</asp:DropDownList>


