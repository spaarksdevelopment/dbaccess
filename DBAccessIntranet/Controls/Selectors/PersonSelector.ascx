﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PersonSelector.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors.PersonSelector" %>

<!--[if IE 7]>
<style type="text/css">
	.TrashPosition{
		position:relative; top: -6px;
	}
</style>
<![endif]-->

      <asp:Image ID="ClearPersonSelectorNameTextBoxImage" runat="server"  
	SkinID="Trash" ClientIDMode="Static" 
	onclick="ClearPersonSelectorNameTextBoxImage_click()" CssClass="TrashPosition" 
	meta:resourcekey="ClearPersonSelectorBin"/>
       
      <!-- Mahmoud.Ali, Changes to Employees' Information Display -->
      <asp:TextBox ID="PersonSelectorNameTextBox" runat="server" Width="400px" 
	ClientIDMode="Static" 
	ToolTip="" 
	meta:resourcekey="PersonSelectorNameTextBox"></asp:TextBox> 
       <!-- /Mahmoud.Ali, Changes to Employees' Information Display -->
        <act:TextBoxWatermarkExtender ID="PersonSelectorNameTextBox_TextBoxWatermarkExtender" 
            runat="server" Enabled="True" TargetControlID="PersonSelectorNameTextBox" WatermarkText="" meta:resourcekey="PersonSelectorNameTextBoxText"> 
        </act:TextBoxWatermarkExtender>
        <act:AutoCompleteExtender
            ID="AutoCompleteExtender1" runat="server" TargetControlID="PersonSelectorNameTextBox"
            MinimumPrefixLength="4" CompletionInterval="10" CompletionSetCount="20"
            UseContextKey="True" ServicePath="~/Services/ListBuilder.asmx"
            CompletionListItemCssClass="autoCompleteItem" 
	CompletionListCssClass="autoComplete" 
	CompletionListHighlightedItemCssClass="autoCompleteItem_Highlight" 
	DelimiterCharacters="" Enabled="True">
        </act:AutoCompleteExtender>

        <asp:HiddenField ID="HiddenFieldControlIndex" runat="server" />

        <script type="text/javascript">
            function ClearPersonSelectorNameTextBoxImage_click() {
               var controlIndex = document.getElementById("<%= HiddenFieldControlIndex.ClientID %>").value;

                if (controlIndex == "")
                    $get("PersonSelectorNameTextBox").value = '';
                else
                    $get("PersonSelectorNameTextBox" + controlIndex).value = '';
            }


        </script>