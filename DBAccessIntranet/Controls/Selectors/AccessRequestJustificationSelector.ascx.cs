﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.CustomMembershipManager;
using System.Threading;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Selectors
{
	public partial class AccessJustificationSelector : BaseControl
    {
        public int AccessRequestJustificationID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {            
                Bind();            
        }

        public void Bind()
        {
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);
			base.CurrentUser.LanguageId = Convert.ToInt32(LanguageId);

			var reasons = Director.GetAccessRequestJustificationReasons(LanguageId);
            AccessRequestJustificationSelectorDropDownList.DataSource = reasons;
            AccessRequestJustificationSelectorDropDownList.DataBind();        
        }

        protected override object SaveViewState()
        {
            ViewState.Add("AccessRequestJustificationID", AccessRequestJustificationID);
            return base.SaveViewState();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            var item = AccessRequestJustificationSelectorDropDownList.Items.FindByValue(AccessRequestJustificationID.ToString());
            if (item != null) item.Selected = true;
        }

        protected void SetAccessRequestJustificationID(object sender, EventArgs e)
        {
            AccessRequestJustificationID = Convert.ToInt32(AccessRequestJustificationSelectorDropDownList.SelectedValue);
        }

    }
}