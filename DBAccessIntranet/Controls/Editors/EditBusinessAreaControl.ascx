﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditBusinessAreaControl.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Editors.EditBusinessAreaControl" %>

<%@ Register src="~/Controls/Selectors/ClassificationSelector.ascx" tagname="Classification" tagprefix="uc1" %>

<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<br />
<br />
<div runat="server" id="divEditBusinessArea" clientidmode="Static">

    <script type="text/javascript" language="javascript">
        var popupOutOfOfficeURL;

        $(document).ready(function () {
            setDirty(false);
            popupOutOfOfficeURL = '<%= Page.ResolveUrl("~/Pages/Popup/OutOfOfficePopup.aspx") %>';
        });

        var g_popup_personId = 0;
        var g_popup_exPersonId = 0;
        var g_Popup_sortId = 0;
        var g_Popup_exsortId = 0;

        var isDirty;

        function setDirty(dirty) {
            isDirty = dirty;
        };

        function RaiseBasicPopupAlert(contentText, headerText) {
            popupAlertTemplateContent.SetText(contentText);
            popupAlertTemplate.SetHeaderText(headerText);
            popupAlertTemplate.Show();
        }

        function DeleteApprover(personId, sortId)
        {
            g_popup_personId = personId;
            g_Popup_sortId = sortId;
            popupConfirmTemplate.SetHeaderText("<%=EditBusinessAreaRemoveHeader.Text %>"); //Remove Approver
            popupConfirmTemplateContentHead.SetText("<%=Approver.Text%>");
            popupConfirmTemplateContentBody.SetText("<%=EditBusinessAreaRemoveConfirm.Text %>");
            popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemoveApprover();");
            popupConfirmTemplate.Show();
        }

        function DeleteExApprover(personId, sortId)
        {
            g_popup_exPersonId = personId;
            g_Popup_exsortId = sortId;
            popupConfirmRemoveExApprover.Show();
        }

        function OnRemoveApprover()
        {
            var personId = g_popup_personId;
            var baID = hdBusinessArea.Get("BusinessAreaID");
            var sortId = g_Popup_sortId;

            var classificationID = classifications.GetValue();
            CheckAndSetAccessRequestId(personId);
            var accessRequestID = hfAccessRequestId.Get("AccessRequestId");
            if (accessRequestID == undefined) accessRequestID = null;
			PageMethods.DeleteBusinessAreaApprover(accessRequestID, personId, baID, sortId, classificationID, OnDeleteApprover);
		}

		function CheckAndSetAccessRequestId(personId)
		{
			var bval = false;
			var values = hfAccessRequestArray.Get("AccessRequestArray");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == personId.toString())
				{
					bval = true;
					break;
				}
			}

			if (bval)
			{
				hfAccessRequestId.Set("AccessRequestId", rval[0].toString());
			}
			else
			{
				hfAccessRequestId.Set("AccessRequestId", undefined);
			}
		}

        function OnRemoveExApprover()
        {
            var personId = g_popup_exPersonId;
            var baID = hdBusinessArea.Get("BusinessAreaID");
            var sortId = g_Popup_exsortId;
            popupConfirmRemoveExApprover.Hide();

            var classificationID = classifications.GetValue();

            CheckAndSetExAccessRequestId(personId);
            var accessRequestID = hfAccessRequestExId.Get("AccessRequestExId");
            if (accessRequestID == undefined) accessRequestID = null;
            PageMethods.DeleteBusinessAreaApprover(accessRequestID, personId, baID, sortId, classificationID, OnDeleteExApprover);
           }


        function CheckAndSetExAccessRequestId(personId)
        {
        	var bval = false; 
			var values = hfAccessRequestExArray.Get("AccessRequestExArray");
			for(var i= 0; i<= values.length-1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == personId.toString())
				{
					bval = true;					
					break;
				}
			}  
			
			if(bval)
			{
				hfAccessRequestExId.Set("AccessRequestExId", rval[0].toString());
			} 
			else
			{
				hfAccessRequestExId.Set("AccessRequestExId", undefined);
			}  	
        }

        function HiddenArrayDataUpdate(g_popup_personId)
        {
        	var values = hfAccessRequestArray.Get("AccessRequestArray");
        	for (var i = 0; i <= values.length - 1; i++)
        	{
        		var rval = values[i].split('-');
        		if (rval[1].toString() == g_popup_personId.toString())
        		{
        			values.splice(i, 1);
        			hfAccessRequestArray.Set("AccessRequestArray", values);
        			break;
        		}
        	}
        }

        function OnDeleteApprover(result) 
		{
        	if (result == -1) {
        	    popupAlertTemplateContent.SetText("<%=EditBusinessAreaDeleteProblem.Text %>");
        	    popupAlertTemplate.SetHeaderText("<%=EditBusinessAreaDeleteApprover.Text %>"); //Delete Approver
                popupAlertTemplate.Show();
                return;
            }
            var baID = hdBusinessArea.Get("BusinessAreaID");
            var classificationID = classifications.GetValue();
            var classText = classifications.GetSelectedItem().text;
            var type = "";

            hdMaxOrder.Set("MaxOrder", result);

            BusinessAreaApprovers.PerformCallback(baID + '|' + classificationID + '|' + classText + '|' + type + '|none');
            HiddenArrayDataUpdate(g_popup_personId)
            
        }

        function ExHiddenArrayDataUpdate(g_popup_exPersonId)
		{
			var values = hfAccessRequestExArray.Get("AccessRequestExArray");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == g_popup_exPersonId.toString())
				{
					values.splice(i, 1);
					hfAccessRequestExArray.Set("AccessRequestExArray", values);
					break;
				}
			}   
		}

        function OnDeleteExApprover(result) {
        	if (result == -1) {
        	    popupAlertTemplateContent.SetText("<%=EditBusinessAreaDeleteProblem.Text %>"); //Sorry, there was a problem deleting the person.
        	    popupAlertTemplate.SetHeaderText("<%=EditBusinessAreaDeleteApprover.Text %>");
                popupAlertTemplate.Show();
                return;
            }
            var baID = hdBusinessArea.Get("BusinessAreaID");
            var classificationID = classifications.GetSelectedItem().value;
            var type = "";

            hdMaxOrder.Set("MaxOrder", result);

            classificationApprovers.PerformCallback(baID + "|" + classificationID + "|" + type + '|none');
            ExHiddenArrayDataUpdate(g_popup_exPersonId);
        }

        // ----------------------------------------------------------------------

        function DeleteDivisionAdmin(personId) {
            if (isDirty) {
                RaiseBasicPopupAlert('<%= SetLanguageText("SaveChangesWarning") %>', '<%= SetLanguageText("Error") %>');
		        return;
		    }
            g_popup_personId = personId;

            popupConfirmTemplate.SetHeaderText('<%= SetLanguageText("RemoveDivisionAdministrator") %>');
            popupConfirmTemplateContentHead.SetText('<%= SetLanguageText("DivisionAdministrator") %>');
            popupConfirmTemplateContentBody.SetText('<%= SetLanguageText("ConfirmDARemoval") %>');
            popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemoveDivisionAdmin();");
            popupConfirmTemplate.Show();
        }

        function OnRemoveDivisionAdmin()
        {
        	var personId = g_popup_personId;

        	CheckAndSetAccessRequestIdDA(personId);
        	var divisionId = hdDivision.Get("DivisionID");

        	PageMethods.DeleteDivisionAdmin(divisionId, personId, OnDeleteDivisionAdmin);
        }

        function OnDeleteDivisionAdmin(result)
        {
            var sError = "";

            switch (result) 
			{
                case 1:
                    {
                        sError = "<%=EditBusinessAreaDeleteDivApproverError.Text %>";
                        break;
                    }
                case 2:
                    {
                        sError = "<%=EditBusinessAreaMinDivApproverError.Text %>";
                        break;
                    }
                default:
                    {
                    	gvDivisionAdmins.PerformCallback();
                    	HiddenArrayDataUpdateDA(g_popup_personId);
                    }
            }

            if (sError != "") 
			{
                popupAlertTemplateContent.SetText(sError);
                popupAlertTemplate.SetHeaderText("<%=EditBusinessAreaDeleteAdmin.Text %>");
                popupAlertTemplate.Show();
            }
        }

        function CheckAndSetAccessRequestIdDA(personId)
        {
			var bval = false;
			var values = hfAccessRequestArrayDA.Get("AccessRequestArrayDA");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == personId.toString())
				{
              		bval = true;
              		break;
				}
			}

			if (bval)
			{
				hfAccessRequestIdDA.Set("AccessRequestIdDA", rval[0].toString());
			}
			else
			{
				hfAccessRequestIdDA.Set("AccessRequestIdDA", undefined);
			}
        }

        function HiddenArrayDataUpdateDA(g_popup_personId)
        {
			var values = hfAccessRequestArrayDA.Get("AccessRequestArrayDA");
			for (var i = 0; i <= values.length - 1; i++)
			{
				var rval = values[i].split('-');
				if (rval[1].toString() == g_popup_personId.toString())
				{
              		values.splice(i, 1);
              		hfAccessRequestArrayDA.Set("AccessRequestArrayDA", values);
              		break;
				}
			}
        }

        function showApproversAll(classificationId) {

            if (classificationId == "0") {
                $('#externals').hide();
                $('#requestApprovers').show();
            }
            else {
                $('#externals').show();
                $('#requestApprovers').hide();
            }
            var baID = hdBusinessArea.Get("BusinessAreaID");
            var classText = classifications.GetSelectedItem().text;
            cbClassificationApprove.PerformCallback(classificationId + '|' + classText + '|no');
        }

        function ShowOutOfOfficePopup(personID) {
            popupUrlOutOfOffice = popupOutOfOfficeURL + "?dbPeopleID=" + personID;
            popupOutOfOffice.SetContentUrl(popupUrlOutOfOffice);
            popupOutOfOffice.Show();
        }

        function CloseOutOfOfficePopup() {
            popupOutOfOffice.Hide();

            var classificationID = classifications.GetValue();
            var type = "";
            var baID = hdBusinessArea.Get("BusinessAreaID");

            if (classificationID == "0") {
                var baID = hdBusinessArea.Get("BusinessAreaID");
                var classificationID = classifications.GetValue();
                var classText = classifications.GetSelectedItem().text;

                BusinessAreaApprovers.PerformCallback(baID + '|' + classificationID + '|' + classText + '|' + type + '|reset');
            }
            else {
                classificationApprovers.PerformCallback(baID + "|" + classificationID + "|" + type + '|reset');
            }
            gvDivisionAdmins.PerformCallback();

        }
    </script>
    <input type="hidden" clientidmode="Static" id="hdField" runat="server" />
    <div class="shaded">
        <h3><asp:Literal ID="EditDivisions" runat="server" meta:resourceKey="EditDivisions"></asp:Literal><i><asp:Label runat="server" ID="LabelBusinessAreaName" 
				ClientIDMode="Static"/></i></h3>
    </div>
    <ul>
        <li id="tab1"><a href="#fragment1"><span> <asp:Literal ID="General" runat="server" meta:resourceKey="General"></asp:Literal></span></a></li>
        <li id="tab3"  clientidmode="Static" runat="server"><a href="#fragment3"><span><asp:Literal ID="DAs" runat="server" meta:resourceKey="DAs"></asp:Literal></span></a></li>
    </ul>
    <div id="fragment1">
        <asp:Table runat="server" ID="LocationTable" SkinID="FormTable">
            <asp:TableRow ID="TableRow0" runat="server">
                <asp:TableCell ID="TableCell0" runat="server" SkinID="FormTableLabel" 
					style="width: 200px;">
					<asp:Literal ID="Name" runat="server" meta:resourceKey="Name"></asp:Literal>&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell1" runat="server" SkinID="FormTableItem" 
					style="width: 450px;">
                    <dx:ASPxTextBox ID="ASPxTextBoxBusinessAreaName" 
					ClientInstanceName="txtBusinessAreaName" runat="server" Width="300px">       
                        
<ValidationSettings RequiredField-IsRequired="true" ValidateOnLeave="true">
<RequiredField IsRequired="True"></RequiredField>
</ValidationSettings>       
                    
</dx:ASPxTextBox>  
                
</asp:TableCell>  			
				<asp:TableCell ID="TableCell4" SkinID="FormTableHelp" runat="server"> 
                <p align="left">
				<asp:Literal ID="SetDivisionName" runat="server" meta:resourceKey="SetDivisionName"></asp:Literal></p>
				</asp:TableCell>            
            </asp:TableRow>
            
            <asp:TableRow ID="TableRow1" runat="server">
                <asp:TableCell ID="TableCell2" runat="server" SkinID="FormTableLabel" 
					style="vertical-align: middle;">
					<asp:Literal ID="Enabled" runat="server" meta:resourceKey="Enabled"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="TableCell3" runat="server" SkinID="FormTableItem" 
					style="width: 450px;">
             
                  <dx:ASPxRadiobuttonList Paddings-PaddingLeft="0px" ID="rbEnabled" 
					ClientEnabled="true" ClientInstanceName="rbEnabled" runat="server" 
					AutoPostBack="false" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        
<Paddings PaddingLeft="0px"></Paddings>
<Items>
                            
<dx:ListEditItem  Text="" Value="1" meta:resourcekey="Yes"/>
                            
<dx:ListEditItem Text="" Value="0" meta:resourcekey="No"/>
                        
</Items>
                    
</dx:ASPxRadiobuttonList >

                
</asp:TableCell>         
				<asp:TableCell ID="TableCell5" SkinID="FormTableHelp" runat="server"> 
                </asp:TableCell>       
            </asp:TableRow>
            <asp:TableRow ID="TableRow2" runat="server" Visible="false">
                <asp:TableCell ID="TableCell6" runat="server" SkinID="FormTableLabel" 
					style="vertical-align: middle;"><asp:Literal ID="EditBusinessApprovalType" meta:resourcekey="EditBusinessApprovalType" runat="server"></asp:Literal>
				</asp:TableCell>
                <asp:TableCell ID="TableCell7" runat="server" SkinID="FormTableItem" 
					style="width: 450px;">
					 <dx:ASPxRadiobuttonList Paddings-PaddingLeft="0px" ID="rbApprovalType" 
					ClientEnabled="true" ClientInstanceName="rbApprovalType" runat="server" 
					AutoPostBack="false" RepeatDirection="Horizontal" RepeatLayout="Flow" 
					RepeatColumns="3">
						
<Paddings PaddingLeft="0px"></Paddings>
<Items>
							
<dx:ListEditItem Text="Concurrent Notification to Group" Value="1"/>
							
<dx:ListEditItem  Text="Sequential Escalation" Value="2"/>
							
<dx:ListEditItem  Text="None" Value="3"/>
						
</Items>
					
</dx:ASPxRadiobuttonList >
                
</asp:TableCell>         
				<asp:TableCell ID="TableCell8" SkinID="FormTableHelp" runat="server"> 
                </asp:TableCell>       
            </asp:TableRow>
            <asp:TableRow ID="TableRow3" runat="server" Visible="false">
                <asp:TableCell ID="TableCell9" runat="server" SkinID="FormTableLabel" 
					style="width: 200px; vertical-align: middle;"><asp:Literal ID="EditBusinessApprovalRequired" meta:resourcekey="EditBusinessApprovalRequired" runat="server"></asp:Literal></asp:TableCell>
                <asp:TableCell ID="TableCell10" runat="server" SkinID="FormTableItem" 
					style="width: 450px;">
					<dx:ASPxRadiobuttonList Paddings-PaddingLeft="0px" ID="rbNumApprovals" 
					ClientEnabled="true" ClientInstanceName="rbNumApprovals" runat="server" 
					AutoPostBack="false" RepeatDirection="Horizontal" RepeatLayout="Flow">
						
<Paddings PaddingLeft="0px"></Paddings>
<Items>
							
<dx:ListEditItem  Text="1 - Single" Value="1"/>
							
<dx:ListEditItem Text="2 - Dual" Value="2"/>
						
</Items>
					
</dx:ASPxRadiobuttonList >
                
</asp:TableCell>   
				
				<asp:TableCell ID="TableCell11" SkinID="FormTableHelp" runat="server"> 
                </asp:TableCell>             
            </asp:TableRow>
        </asp:Table>

		<div id="divUpdateBusinessAreabutton" style="margin-top: 10px; margin-bottom: 10px;">
			<div id="divSaveUpdateGeneral" style="float:left;">
				<dx:ASPxButton ID="buttonUpdateBusinessArea" 
					ClientInstanceName="buttonUpdateBusinessArea" AutoPostBack="False" 
					runat="server" Text="" meta:resourcekey="btnConfirmSave">
					<ClientSideEvents Click="function(s,e){ updateBusinessArea(); }" />
				</dx:ASPxButton>
			</div>
			<div id="divCancelUpdateGeneral" style="margin-left: 5px; float:left;">
				<dx:ASPxButton  ForeColor="#0098DB" ID="ButtonCancelUpdateBusinessArea" 
					ClientInstanceName="buttonCancelUpdateBusinessArea" AutoPostBack="False" 
					runat="server" Text="" 
					meta:resourcekey="btnConfirmCancel">
					<ClientSideEvents Click="function(s,e){ HideBusinessAreaDetail(); }" /> 
				</dx:ASPxButton>
			</div>
			<div id="divClearGeneral" style="clear: both;"></div>
        </div>
    </div>
        
	<div id="fragment3" clientidmode="Static" runat="server">
        <br />
        <div style="margin: 10px" id="divDivisionAdmins">
            <!-- Mahmoud.Ali, Changes to Employees' Information Display -->
            <dx:ASPxGridView ID="ASPxGridViewDivisionRoleUsers" ClientInstanceName="gvDivisionAdmins"
                runat="server" AutoGenerateColumns="False" SettingsPager-Mode="ShowAllRecords"
                KeyFieldName="DBPeopleID" 
                ClientIDMode="AutoID" 
				oncustomcallback="ASPxGridViewDivisionRoleUsers_CustomCallback" 
				OnHtmlRowCreated="ASPxGridViewDivisionRoleUsers_HtmlRowCreated">
                <Columns>
                    <dx:GridViewDataImageColumn VisibleIndex="0">
                        <DataItemTemplate>
                            <img src="../../App_Themes/DBIntranet2010/images/trash.gif" alt="Delete" onclick='<%# "DeleteDivisionAdmin(\"" + Container.KeyValue + "\");" %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataImageColumn>
                    


                    <dx:GridViewDataTextColumn FieldName="Forename" ShowInCustomizationForm="True" 
						VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnForename">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Surname" ShowInCustomizationForm="True" 
						VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnSurname">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EmailAddress" 
						ShowInCustomizationForm="True" VisibleIndex="4" 
						meta:resourcekey="GridViewDataTextColumnEmailAddress">
                    </dx:GridViewDataTextColumn>                    
                    <dx:GridViewDataTextColumn Caption="" FieldName="SecurityGroup" 
						ShowInCustomizationForm="True" VisibleIndex="5" 
						meta:resourcekey="GridViewDataTextColumnRole">
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataDateColumn Caption="" FieldName="DateAccepted" 
						PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" 
						ShowInCustomizationForm="True" VisibleIndex="6" 
						meta:resourcekey="GridViewDataDateColumnDateAccepted" >
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
					</dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn Caption="" 
						FieldName="DateNextCertification" 
						PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" VisibleIndex="7" 
						meta:resourcekey="GridViewDataDateColumnRecertificationDue">                
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>                    
                    <dx:GridViewDataDateColumn Caption="" FieldName="LeaveDate"  
						PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" 
						ShowInCustomizationForm="True" VisibleIndex="8" 
						meta:resourcekey="GridViewDataDateColumnOutOfOfficeStart">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>                
                    <dx:GridViewDataDateColumn Caption="" 
						FieldName="ReturnDate" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" 
						ShowInCustomizationForm="True" VisibleIndex="9" 
						meta:resourcekey="GridViewDataDateColumnOutOfOfficeReturn">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                   
                    <dx:GridViewDataTextColumn FieldName="SecurityGroupID" 
						ShowInCustomizationForm="True" Visible="false" 
						meta:resourcekey="GridViewDataTextColumnSecurityGroupID">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="11">
                        <DataItemTemplate>
                            <asp:HyperLink ID="HyperLinkSetOutOfOffice" NavigateUrl="javascript:void(0);" 
								runat="server" Text=""  onclick='<%# "ShowOutOfOfficePopup(\"" + Eval("dbPeopleID") + "\");"%>'
								meta:resourcekey="HyperLinkSetOutOfOffice" />               
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                                       

                </Columns>
                <Settings ShowStatusBar="Visible" />
				<SettingsText EmptyDataRow="No Division Administrators have been setup." />
                <SettingsBehavior AllowFocusedRow="true"  AllowSort="false" />
                <SettingsPager Mode="ShowAllRecords" />
                <SettingsLoadingPanel Mode="Disabled" />

<SettingsBehavior AllowSort="False" AllowFocusedRow="True"></SettingsBehavior>

<SettingsPager Mode="ShowAllRecords"></SettingsPager>

<Settings ShowStatusBar="Visible"></Settings>

<SettingsText EmptyDataRow="No Division Administrators have been setup."></SettingsText>

<SettingsLoadingPanel Mode="Disabled"></SettingsLoadingPanel>

                <Templates>
                    <StatusBar>
                        <dx:ASPxButton ID="btnAddDivisionAdmin" AutoPostBack="False" runat="server" 
							Text="" 
							meta:resourcekey="AddDivisionAdminstrator">
                            <ClientSideEvents Click="function(s,e){ popupAddDivisionAdmin.Show(); }" />
                        </dx:ASPxButton>
                    </StatusBar>
                </Templates>
            </dx:ASPxGridView>
            <!-- Mahmoud.Ali, Changes to Employees' Information Display -->
			<div style="float: left;">
			</div>
			<br />
			<div class="displaytext" style="margin: 0px; width: 100%;">
			    <asp:Literal ID="AdditionsConfirmed" runat="server" meta:resourceKey="AdditionsConfirmed"></asp:Literal>
				
			</div>
			<br />
			<br />
			<div id="divDAButton" style="margin-top: 10px; margin-bottom: 10px;">
				<div id="divDASaveButton" style="float:left;">
					<dx:ASPxButton ID="btnConfirmDA" AutoPostBack="False" runat="server" 
						Text="" style="float: left;" meta:resourcekey="btnConfirmSave">
						<ClientSideEvents Click="SubmitDivisionAdministratorChanges" />
					</dx:ASPxButton>
				</div>
				<div id="divDACancelButton" style="margin-left: 5px; float:left;">
					<dx:ASPxButton  ForeColor="#0098DB" ID="btnCancelDA"  AutoPostBack="False" 
						runat="server" Text="" meta:resourcekey="btnConfirmCancel">
						<ClientSideEvents Click="function(s,e){ HideBusinessAreaDetail(); }" />
					</dx:ASPxButton>
				</div>
				<div id="divClearDA" style="clear: both;"></div>
			</div>
        </div>
   </div>
 </div>   
 


<dx:ASPxHiddenField ID="ASPxHiddenFieldBusinessArea" ClientInstanceName="hdBusinessArea" runat="server">
</dx:ASPxHiddenField>

	<dx:ASPxPopupControl ID="ConfirmRemoveApprover" 
	ClientInstanceName="popupConfirmRemoveApprover" runat="server"
		SkinID="PopupBasic"
		HeaderText="Remove Approver" 
	meta:resourcekey="ConfirmRemoveApproverResource1">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControlRemoveApprover" 
				runat="server" SupportsDisabledAttribute="True">
				<h2><asp:Literal ID="ApproverLiteral2" meta:resourcekey="Approver" runat="server"></asp:Literal></h2>
				<p>
					<asp:Literal ID="EditBusinessAreaRemoveConfirm2" meta:resourcekey="EditBusinessAreaRemoveConfirm" runat="server"></asp:Literal>
				</p>
				<br />
				<div class="PopupBasic">
					<div>
						<dx:ASPxButton ID="ASPxButtonCancel1" runat="server" AutoPostBack="false" 
							Text="Yes" Width="100px">
							<ClientSideEvents Click="function(s,e){ OnRemoveApprover(); }" />
<ClientSideEvents Click="function(s,e){ OnRemoveApprover(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div>
						<dx:ASPxButton ID="ASPxButtonAccept1" runat="server" AutoPostBack="false" 
							Text="No" Width="100px">
							<ClientSideEvents Click="function(s,e){ popupConfirmRemoveApprover.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmRemoveApprover.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
    
    <dx:ASPxPopupControl ID="popupConfirmRemoveExApprover" 
	ClientInstanceName="popupConfirmRemoveExApprover" runat="server"
		SkinID="PopupBasic"
		HeaderText="Remove Approver" 
	meta:resourcekey="popupConfirmRemoveExApproverResource1">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" 
				SupportsDisabledAttribute="True">
				<h2><asp:Literal ID="ApproverLiteral3" meta:resourcekey="Approver" runat="server"></asp:Literal></h2>
				<p>
					<asp:Literal ID="EditBusinessAreaRemoveConfirm3" meta:resourcekey="EditBusinessAreaRemoveConfirm" runat="server"></asp:Literal>
				</p>
				<br />
				<div class="PopupBasic">
					<div>
						<dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" Text="Yes" 
							Width="100px">
							<ClientSideEvents Click="function(s,e){ OnRemoveExApprover(); }" />
<ClientSideEvents Click="function(s,e){ OnRemoveExApprover(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div>
						<dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="false" Text="No" 
							Width="100px">
							<ClientSideEvents Click="function(s,e){ popupConfirmRemoveExApprover.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmRemoveExApprover.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>

	<dx:ASPxPopupControl ID="ConfirmRemoveDivisionAdmin" 
		ClientInstanceName="popupConfirmRemoveDivisionAdmin" runat="server"
		SkinID="PopupBasic"
		HeaderText="" 
		meta:resourcekey="ConfirmRemoveDivisionAdministrator">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControlRemoveDivisionAdmin" 
				runat="server" SupportsDisabledAttribute="True">
				<h2>
					<asp:Literal ID="DivisionAdministrator" runat="server" meta:resourceKey="DivisionAdmionistrator"></asp:Literal>
				</h2>
				<p>
				<asp:Literal ID="RemoveDivisionAdmionistrator" runat="server" meta:resourceKey="RemoveDivisionAdmionistrator"></asp:Literal>
				</p>
				<br />
				<div class="PopupBasic">
					<div>
						<dx:ASPxButton ID="ASPxButtonCancel2" runat="server" AutoPostBack="false" 
							Text="Yes" Width="100px" meta:resourcekey="Yes">
							<ClientSideEvents Click="function(s,e){ OnRemoveDivisionAdmin(); }" />
<ClientSideEvents Click="function(s,e){ OnRemoveDivisionAdmin(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
					<div>
						<dx:ASPxButton ID="ASPxButtonAccept2" runat="server" AutoPostBack="false" 
							Text="No" Width="100px" meta:resourcekey="No">
							<ClientSideEvents Click="function(s,e){ popupConfirmRemoveDivisionAdmin.Hide(); }" />
<ClientSideEvents Click="function(s,e){ popupConfirmRemoveDivisionAdmin.Hide(); }"></ClientSideEvents>
						</dx:ASPxButton>
					</div>
				</div>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>

 <dx:ASPxPopupControl ID="PopupOutOfOffice" 
                         ClientInstanceName="popupOutOfOffice" 
                         runat="server"
                         SkinID="PopupCustomSize"
                         HeaderText="<%$ Resources:CommonResource, OutofOffice %>"
                         Width="600px"
                         Height="500px"
                         PopupVerticalAlign="TopSides"
                         ContentURL = "~/Pages/Popup/OutOfOfficePopup.aspx"
                         ContentUrlIFrameTitle="OutOfOfficePopup" 
	meta:resourcekey="PopupOutOfOfficeResource1">
	 <contentcollection>
		 <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
		 </dx:PopupControlContentControl>
	 </contentcollection>

</dx:ASPxPopupControl>

<!--literals-->

     <asp:Literal ID="EditBusinessAreaRemoveHeader"  meta:resourcekey="EditBusinessAreaRemoveHeader" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="Approver"  meta:resourcekey="Approver" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="EditBusinessAreaRemoveConfirm"  meta:resourcekey="EditBusinessAreaRemoveConfirm" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

     <asp:Literal ID="EditBusinessAreaDeleteProblem"  meta:resourcekey="EditBusinessAreaDeleteProblem" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="EditBusinessAreaDeleteApprover"  meta:resourcekey="EditBusinessAreaDeleteApprover" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="EditBusinessAreaDeleteDivApproverError"  meta:resourcekey="EditBusinessAreaDeleteDivApproverError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

     <asp:Literal ID="EditBusinessAreaMinDivApproverError"  meta:resourcekey="EditBusinessAreaMinDivApproverError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="EditBusinessAreaDeleteAdmin"  meta:resourcekey="EditBusinessAreaDeleteAdmin" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     

    
