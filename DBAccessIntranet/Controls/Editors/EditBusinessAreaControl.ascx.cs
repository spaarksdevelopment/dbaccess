﻿using System;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web;
using System.Linq;
using System.Collections.Generic;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;


using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Resources;
using Spaarks.Common.UtilityManager;
using System.Threading;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Editors
{
	public partial class EditBusinessAreaControl : BaseControl
	{
		public int BusinessAreaID { get; set; }

		private int rowCount
		{
			get
			{
				if (ViewState["rowCount"] == null)
					return 0;

				return (int)ViewState["rowCount"];
			}
			set
			{
				ViewState["rowCount"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			string currentUserLanguageCode = Thread.CurrentThread.CurrentUICulture.Name;
			int? LanguageId = Director.GetLanguageID(Helper.SafeInt(base.CurrentUser.LanguageId), currentUserLanguageCode);
			base.CurrentUser.LanguageId = Convert.ToInt32(LanguageId);

			if (!Page.IsPostBack)
			{
				bindClassification();
			}
		}

		/// <summary>
		/// Method to return the classification list for the drop down list
		/// E.Parker
		/// 24.05.11
		/// </summary>
		void bindClassification()
		{
			var Classes = Director.GetClassificationsEnabled(base.CurrentUser.LanguageId);

			foreach (var clas in Classes.AsEnumerable().ToList())
			{
				ListEditItem lstItem = new ListEditItem();
				lstItem.Text = clas.Classification + " - " + clas.Description;
				lstItem.Value = clas.HRClassificationID;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			base.RegisterScriptInclude("EditBusinessAreaControl", "~/scripts/Specific/EditBusinessAreaControl.js");

			BindData();
		}

		public void BindData()
		{

			if (BusinessAreaID > 0)
			{
				hdField.Value = BusinessAreaID.ToString();
				divEditBusinessArea.Style.Add(HtmlTextWriterStyle.Display, "");
				var businessArea = Director.GetVwBusinessArea(BusinessAreaID, base.CurrentUser.LanguageId);

				//HACK: business area gets selected when you click delete
				//need to bomb out if BA not found
				if (businessArea == null)
				{
					divEditBusinessArea.Style.Add(HtmlTextWriterStyle.Display, "none");
				}
				else
				{
					ASPxHiddenFieldBusinessArea.Set("BusinessAreaID", BusinessAreaID);
					LabelBusinessAreaName.Text = businessArea.UBR + " - " + businessArea.BusinessAreaName + " - " + businessArea.CountryName;
					ASPxTextBoxBusinessAreaName.Text = businessArea.BusinessAreaName;
					rbEnabled.SelectedIndex = businessArea.Enabled ? 0 : 1;
					rbApprovalType.SelectedItem = rbApprovalType.Items.FindByValue(businessArea.ApprovalTypeID.ToString());
					rbNumApprovals.SelectedIndex = (businessArea.NumberOfApprovals == 1) ? 0 : 1;

					if (businessArea.HasDivisionNode)
					{
						tab3.Visible = true;
						fragment3.Visible = true;
						ASPxHiddenFieldBusinessArea.Set("DivisionID", businessArea.DivisionID);
						BindDAGrid();
					}
					else
					{
						tab3.Visible = false;
						fragment3.Visible = false;
					}

				}
			}
			else
			{
				divEditBusinessArea.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
		}

		private void BindDAGrid()
		{
			Int32 iDivisionID = Convert.ToInt32(ASPxHiddenFieldBusinessArea.Get("DivisionID"));

			var listDivisionRoleUsers = new List<DivisionRoleUser>();

			if (null == Session[Pages.MyDivisions.DivisionRoleUsersCacheKey])
			{
				listDivisionRoleUsers = Director.GetDivisionRoleUsers(iDivisionID);
				Session[Pages.MyDivisions.DivisionRoleUsersCacheKey] = listDivisionRoleUsers;
			}
			listDivisionRoleUsers = (List<DivisionRoleUser>)Session[Pages.MyDivisions.DivisionRoleUsersCacheKey];

			ASPxGridViewDivisionRoleUsers.DataSource = listDivisionRoleUsers.Where(a => a.SecurityGroupID == (int)DBAccessController.DBAccessEnums.RoleType.DivisionAdministrator).ToList();
			ASPxGridViewDivisionRoleUsers.DataBind();
		}


		protected void ASPxGridViewDivisionRoleUsers_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			BindDAGrid();
		}

	
		protected void BusinessAreaAppprovers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			this.HighlightGridRowsOnMouseOver(e);
		}

		protected void ClassificationApprovers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			this.HighlightGridRowsOnMouseOver(e);
		}


		protected void ASPxGridViewDivisionRoleUsers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			this.HighlightGridRowsOnMouseOver(e);
		}

		private void HighlightGridRowsOnMouseOver(ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}

		public string SetLanguageText(object text)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			return resourceManager.GetString(Convert.ToString(text));
		}
	}
}
