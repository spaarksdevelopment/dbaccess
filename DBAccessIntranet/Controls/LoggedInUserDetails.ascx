﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoggedInUserDetails.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.LoggedInUserDetails" %>
<div id="loggedinuserdetails">
    <table id="loggedinuserdetailsTable">
        <tr id="loggedInUserName">
            <td>
                <asp:Label ID="LoggedInAsUserLable" runat="server" Text="" ></asp:Label> 
            </td>       
        </tr>
        <tr id="lastLoggedInTime">
            <td>
                    <asp:Label ID="LastLoggedInOnLable" runat="server" Text="" ></asp:Label>
            </td>               
        </tr>
        <tr>
            <td>
                <span id="loginDateSecurityWarning" class="loginDate-SecurityWarning">Please contact security administrator if last login is incorrect</span>
            </td>
        </tr>
    </table>
</div>