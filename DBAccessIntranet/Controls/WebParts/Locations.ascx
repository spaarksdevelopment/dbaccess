﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Locations.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.WebParts.Locations" %>

<ul id="Locations">
	<li>
		<h2 class="lightblue_10">Visitor Reception</h2>
		Americas<br />
		Asia Pacific<br />
		Europe<br />
		Middle East / Africa
	</li>
	<li>
		<h2 class="lightblue_10">Pass Office Centers</h2>
		Americas<br />
		Asia Pacific<br />
		Europe<br />
		Middle East / Africa
	</li>
	<li>
		<h2 class="lightblue_10">SOC Centers</h2>
		Frankfurt<br />
		London<br />
		New York<br />
		Singapore
	</li>
	<li>
		<h2 class="lightblue_10">CSBC</h2>
		Americas<br />
		Asia Pacific<br />
		Continental Europe<br />
		Japan<br />
		MENA<br />
		UK
	</li>
	<li>
		<h2 class="lightblue_10">Help</h2>
		Training Video<br />
		User Guide<br />
		FAQ
	</li>
</ul>
