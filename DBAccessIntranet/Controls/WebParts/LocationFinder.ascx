﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationFinder.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.WebParts.LocationFinder" %>

<h2 class="lightblue_10">Location Finder</h2>

<div id="cc_17_FlashPlayer1838" class="cc_17_FlashPlayer">
	<div id="flashcontent1838" class=" "></div>
</div>

<script type="text/javascript">
<!--
	if ("undefined" == typeof (SWFObject))
	{
		var flashbuilderJS = document.createElement("script");
		flashbuilderJS.src = "/Scripts/flashbuilder.js";
		flashbuilderJS.type = "text/javascript";
		var cc = document.getElementById("cc_17_FlashPlayer1838");
		if (null != cc) cc.appendChild(flashbuilderJS);
	}

	///////// INTERVAL: execute following code only if flashbuilder is loaded
	var interval1838 = setInterval(function ()
	{
		if ("undefined" != typeof (SWFObject))
		{
			clearInterval(interval1838);

			var so1838 = new SWFObject("/locationfinder_nlvz(1).swf", "flashobject1838", "752", "204", "9", "");

			so1838.addParam("wmode", "opaque");
			so1838.addParam("allowScriptAccess", "always");
			so1838.addVariable("language", "ENG");
			so1838.addVariable("locationXML", ("/countries.xml"));
			so1838.write("flashcontent1838");
		}
	}, 50);
	///////// END INTERVAL

//-->
</script>
 <noscript>
	<p>The Location Finder requires JavaScript.</p>
</noscript>
