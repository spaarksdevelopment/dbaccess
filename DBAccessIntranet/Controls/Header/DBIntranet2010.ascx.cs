﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using System.Reflection;
using System.Resources;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
//using Aduvo.Common.MembershipManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
    public partial class DBIntranet2010 : BaseSecurityControl
    {
		public bool ShowFlags
		{
			set
			{
				HeaderStatus.ShowFlags = value;
			}
		}

		protected void theMenu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            string navigateUrl = e.Item.NavigateUrl.ToLower();

			if (e.Item.Text == "Build")
			{
 				string buildName = string.Empty;

				buildName = GetBuildName();

				e.Item.Text = string.Format("Build: {0}", buildName);
			}
            
			if (navigateUrl.Contains("pages/error.aspx") || navigateUrl.ToLower().Contains("pages/login.aspx"))
            {
                Menu menu = (Menu)sender;

                MenuItem menuItem = ((MenuItem)e.Item);

                if (menuItem.Parent == null)
                    menu.Items.Remove(menuItem);
                else
                    menuItem.Parent.ChildItems.Remove(menuItem);

            }

			if (HttpContext.Current.Request.IsAuthenticated)
            {
		        DBAppUser _CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
				//LabelLastLoggedIn.Text = String.Format ("{0:dd-MMM-yyyy} at {0:HH:mm}", _CurrentUser.LastActivityDate);
            }
		}

		private string GetBuildName()
		{
			string description = string.Empty;

			var descriptionAttribute = Assembly.GetExecutingAssembly()
				.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
				.OfType<AssemblyDescriptionAttribute>()
				.FirstOrDefault();

			if (descriptionAttribute != null)
				description = descriptionAttribute.Description;

			return description;
		}

        protected void logoutLinkButton_click(object sender, EventArgs e)
        {
            //ViewState.Remove("Filter");
            Session.Abandon();
            FormsAuthentication.SignOut();
			Response.Redirect (FormsAuthentication.LoginUrl, true);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);


            if (Request.IsAuthenticated && !Global.AdminLoginOverride)
            {
                LinkButton logoutLinkButton = (LinkButton)theLoginView.FindControl("logoutLinkButton");
                if (logoutLinkButton != null)
                    logoutLinkButton.Visible = (ConfigurationManager.AppSettings["UseSSO"].ToUpper() != "TRUE");

				Literal literal = (Literal)theLoginView.FindControl("LogOut");
				literal.Text = SetLanguageText("LogOut");
            }

            //for sites other than live, add environment label
            if (!IsPostBack)
            {
                string AppTitle = ConfigurationManager.AppSettings["AppTitle"].ToString();
                string AppEnv = ConfigurationManager.AppSettings["AppEnv"].ToUpper();
                if (AppEnv != "LIVE")
                {
                     AppTitle += " " + AppEnv;
                }
                aTitle.InnerText = AppTitle;

				//if (base.PageSecurity.CurrentUser != null)
				//{
				//    string ImplementationDirectoryLink = string.Empty;
				//    if(ConfigurationManager.AppSettings["ImplementationDirectoryLink"] != null)
				//        ImplementationDirectoryLink = ConfigurationManager.AppSettings["ImplementationDirectoryLink"].ToString() + base.PageSecurity.CurrentUser.dbPeopleID;

				//    LinkUserName.Text = base.PageSecurity.CurrentUser.Email;
				//    LinkUserName.NavigateUrl = ImplementationDirectoryLink;

				//   // string[] strRoles = base.PageSecurity.CurrentUser.RolesNamesList.Split(',');

				//   // //set the display string in short form
				//   // //if more than 1 role is present
				//   // if (strRoles.Length > 1)
				//   // {
				//   //     int i = base.PageSecurity.CurrentUser.RolesShortNamesList.Count;
				//   //     int j = 0;
				//   //     string strBuildList = String.Empty;
				//   //     while (i != 0 && i >= 1)
				//   //     {
				//   //         strBuildList += Convert.ToString(base.PageSecurity.CurrentUser.RolesShortNamesList[j]);
				//   //         i--;
				//   //         j++;
				//   //         if (i != 0)
				//   //             strBuildList += ",";                            
				//   //     }
				//   //     RoleType.Text = "[" + strBuildList + "]";
				//   // }
				//   // else
				//   //{
				//   //    RoleType.Text = "[" + base.PageSecurity.CurrentUser.RolesNamesList + "]";
				//   //}                    
				//}
				//else
                LinkUserName.Visible = false;
            }
        }


        public string SetLanguageText(string text)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			return resourceManager.GetString(Convert.ToString(text));
		}
    }
}