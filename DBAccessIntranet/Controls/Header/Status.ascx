﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Status.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Header.Status" %>


<script type = "text/javascript">
	//setting the post back Id to hidden default
	function SetSource(SourceID)
	{
		theForm.__EVENTTARGET.value = SourceID;
		theForm.__EVENTARGUMENT.value = SourceID;
	}
</script>

<dx:ASPxCallbackPanel ID="UpdateStatusTasks" ClientInstanceName="UpdateStatusTasks" ClientIDMode="Static" runat="server" OnCallback="UpdateStatusTasks_Callback">
	<PanelCollection>
	               <dx:PanelContent>
							<asp:Label ID="LabelStatusTasks" ClientIDMode="Static" runat="server"></asp:Label>
							&nbsp;&nbsp;
							<asp:Label ID="LabelStatusRequests" runat="server"></asp:Label><br />
                        <div style="">
							    <asp:ImageButton ID="English" runat="server" AlternateText="English" 
								    ClientIDMode="Static" OnClientClick="SetSource(this.id)" ImageUrl="~/App_Themes/DBIntranet2010/images/united_kingdom_flag.ico" Height="30px"/>
							    <asp:ImageButton ID="German" runat="server" AlternateText="Germany" 
								    ClientIDMode="Static" OnClientClick="SetSource(this.id)" ImageUrl="~/App_Themes/DBIntranet2010/images/germany_flag.ico" Height="30px"/>
							    <asp:ImageButton ID="Italian" runat="server" AlternateText="Italy" 
								    ClientIDMode="Static" OnClientClick="SetSource(this.id)" ImageUrl="~/App_Themes/DBIntranet2010/images/italy_flag.ico" Height="30px"/>
                            </div>
					</dx:PanelContent>
	</PanelCollection>
</dx:ASPxCallbackPanel>
<div style="position:relative; margin-left:250px; margin-top:-10px;">
</div>
