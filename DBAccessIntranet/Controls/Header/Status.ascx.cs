﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Resources;
using System.Reflection;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Configuration;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Header
{
	public partial class Status : System.Web.UI.UserControl
	{
		private readonly String m_sTemplateTasks = "&nbsp;&nbsp;Awaiting Tasks:&nbsp;{0}";
		private readonly String m_sTemplateRequests = "&nbsp;&nbsp;Pending Requests:&nbsp;{0}";

		public bool ShowFlags
		{
			get
			{
				return _ShowFlags;
			}
			set
			{
				_ShowFlags = value;
			}
		}

		private bool _ShowFlags = false;

		protected void Page_Load(object sender, EventArgs e)
		{
			this.SetFlagsVisible();

			String sStatusTasks = "";
			String sStatusRequests = "";
			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			if (null != currentUser)
			{
				Director director = new Director(currentUser.dbPeopleID);

                var roles = Director.GetAccessAreaRoles(currentUser.dbPeopleID);
				Boolean bHasRoles = (null != roles && 0 != roles.Count);

                Int32 iTasks = Director.GetMyTasksCount(currentUser.dbPeopleID, 1);
			    sStatusTasks = String.Format(m_sTemplateTasks, iTasks);

                Int32 iRequests = Director.GetMyMasterRequestsCount(currentUser.dbPeopleID);
				sStatusRequests = String.Format(m_sTemplateRequests, iRequests);

				//int nReturnValue = Director.UpdateUserProfile();

				ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

				if (resourceManager != null)
				{
					LabelStatusTasks.Text = "|" + sStatusTasks.Replace("Awaiting Tasks", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.AwaitingTasks)));
					LabelStatusRequests.Text = "|" + sStatusRequests.Replace("Pending Requests", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.PendingRequests)));
				}
			}
		}

		protected void UpdateStatusTasks_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
		{
			String sStatusTasks = "";
			String sStatusRequests = "";
			DBAppUser currentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

			Int32 iStatusId = Convert.ToInt32(Convert.ToString(e.Parameter[0]));

			if (null != currentUser)
			{
				Director director = new Director(currentUser.dbPeopleID);

				var roles = Director.GetAccessAreaRoles(currentUser.dbPeopleID);
				Boolean bHasRoles = (null != roles && 0 != roles.Count);

                Int32 iTasks = Director.GetMyTasksCount(currentUser.dbPeopleID, iStatusId);
				sStatusTasks = String.Format(m_sTemplateTasks, iTasks);


                Int32 iRequests = Director.GetMyMasterRequestsCount(currentUser.dbPeopleID);
				sStatusRequests = String.Format(m_sTemplateRequests, iRequests);
			}

			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			if (resourceManager != null)
			{
				LabelStatusTasks.Text = sStatusTasks.Replace("Awaiting Tasks", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.AwaitingTasks)));
				LabelStatusRequests.Text = sStatusRequests.Replace("Pending Requests", resourceManager.GetString(Convert.ToString(DBAccessEnums.GlobalResourceEnums.PendingRequests)));
			}
		}

		private void SetFlagsVisible()
		{
			if (ShouldShowFlags())
			{
			    English.Style.Remove("display");
			    German.Style.Remove("display");
			    Italian.Style.Remove("display");
			}
			else
			{
			    English.Style.Add("display", "none");
                German.Style.Add("display", "none");
                Italian.Style.Add("display", "none");
			}
		}

		private bool ShouldShowFlags()
		{
			bool showFlagsOnEveryPage = false;
			if (ConfigurationManager.AppSettings["AlwaysShowLanguageFlags"] != null)
				showFlagsOnEveryPage = ConfigurationManager.AppSettings["AlwaysShowLanguageFlags"].Equals("true", StringComparison.InvariantCultureIgnoreCase);

			bool multiLanguageOn = false;
			if (ConfigurationManager.AppSettings["MultiLanguageOn"] != null)
				multiLanguageOn = ConfigurationManager.AppSettings["MultiLanguageOn"].Equals("true", StringComparison.InvariantCultureIgnoreCase);


			return (_ShowFlags || showFlagsOnEveryPage)
				&& multiLanguageOn;
		}
	}
}