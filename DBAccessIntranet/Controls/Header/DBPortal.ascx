﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Header.DBPortal" EnableTheming="true" Codebehind="DBPortal.ascx.cs" %>

<!-- CONTENT: HEADER AREA -->
<div id="headerArea" class="trackMe">

	<!-- make sure intelli-sense works at design time -->
	<div runat="server">
		<% if (DesignMode) { %>
			<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
			<script type="text/javascript" src="/Scripts/ASPxScriptIntelliSense.js"></script>
		<% } %>
	</div>

	<script type="text/javascript" language="javascript">
	// <![CDATA[

		function GetCurrentFilePath()
		{
			return "<%= HttpContext.Current.Request.FilePath %>";
		}

		function ClosePopup_Feedback(bClear)
		{
			if (null == bClear) bClear = false;

			var ctrl_popupFeedback = ASPxClientPopupControl.Cast("popupFeedback");

			if (null != ctrl_popupFeedback)
			{
				if (bClear) ctrl_popupFeedback.RefreshContentUrl(); 	// reset page
				ctrl_popupFeedback.Hide();
			}
		}

		function ClosePopup_Recommendation(bClear)
		{
			if (null == bClear) bClear = false;

			var ctrl_popupRecommendation = ASPxClientPopupControl.Cast("popupRecommendation");

			if (null != ctrl_popupRecommendation)
			{
				if (bClear) ctrl_popupRecommendation.RefreshContentUrl(); 	// reset page
				ctrl_popupRecommendation.Hide();
			}
		}

		$(document).ready(function ()
		{
			$("#toolbarPrint").bind("click", function () { window.print(); });
			$("#toolbarBookmark").bind("click", function () { window.external.AddFavorite(location.href, document.title); });
			$("#toolbarFeedback").bind("click", function () { popupFeedback.Show(); });
			$("#toolbarRecommendation").bind("click", function () { popupRecommendation.Show(); });
		});

	// ]]>
	</script>

	<div id="topStage">
		<a id="clickLogo" href="#" title="" rel="logo"></a>
		<a href="/Default.aspx" target="_self" class="headerProjectName" title="">
			<asp:Image ID="headerImg" runat="server" 
			ImageUrl="~/App_Themes/DBIntranet2010/img/name_db.gif"/>
		</a>
		<a href="/Pages/Home.aspx" target="_self" class="headerApplicationTitle" title="" runat="server" id="aTitle"></a>              
	</div>

	<div class="trackMe" id="metaNavi">
		<ul id="list_meta1" class="horNav ">
			<li class=" first ">
				<a id="pi_meta_507" href="http://www.db.com" title="" class="" target="_blank" meta:resourcekey="DBPotalDBNameTitle">
					<asp:Literal ID="DBPotalDBNameText" runat="server" meta:resourcekey="DBPotalDBNameText" ></asp:Literal>
				</a>
			</li>
			<li><a accesskey="C" href="#" title="Contact" class="" runat="server" id="aContact">Contact</a></li>
			<li><a accesskey="M" href="#" title="Sitemap" class="" runat="server" id="aSitemap">Sitemap</a></li>
			<li><a accesskey="H" href="#" title="Help" class="" runat="server" id="aHelp">Help</a></li>     
			<li><asp:HyperLink runat="server" ID="LinkUserName" Target="_blank">[LinkUserName]</asp:HyperLink></li>                 
		</ul>

<%--		<asp:HyperLink runat="server" ID="LinkUserName" Target="_blank"></asp:HyperLink>
		<asp:Label runat="server" ID="RoleType"></asp:Label>--%>
	</div>

	<div id="level1NaviHolder">
		<div id="level1Navi" class="trackMe">

			<asp:LoginView ID="theLoginView" runat="server">
				<LoggedInTemplate>
					<ul id="LogOutLink" class="AspNet-Menu" style="float:right">
						<li class="AspNet-Menu first">
								<asp:LinkButton ID="logoutLinkButton" runat="server" 
								OnClick="logoutLinkButton_click" CausesValidation="False">
								<asp:Literal ID="LogOut" runat="server" meta:reosurceKey="LogOut"></asp:Literal>
								</asp:LinkButton>
						</li>
					</ul>
				</LoggedInTemplate>
			</asp:LoginView>
             
			<%--<asp:Menu ID="theMenu" SkinID="Menu" runat="server" DataSourceID="SiteMapDataSource1" OnMenuItemDataBound="theMenu_MenuItemDataBound">               
				<DataBindings>
					<asp:MenuItemBinding DataMember="Menu" NavigateUrlField="url" TextField="text" ValueField="text" />
				</DataBindings>
			</asp:Menu>

			<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" ShowStartingNode="false" SiteMapProvider="XmlSiteMapProviderPortal" />--%>
		</div>
	</div>
    
	<div id="breadcrumbNaviHolder"> 
		<div id="breadcrumbNavi">
			<div id="ToolBar_top" class="toolBarOuter">
				<span id="ToolBarNote_top" class="ToolBarNote"></span>
				<div class="toolBarInner">
<%--					<a id="toolbarPrint" title="Print version" class="toolBarPrint" href="javascript:{}"></a>
					<a id="toolbarRecommendation" title="Recommend this page" class="toolBarSendafriend" href="javascript:{}"></a>
					<a id="toolbarBookmark" title="Add bookmark" class="toolBarBookmark" href="javascript:{}"></a>
					<a id="toolbarFeedback" title="Vote for this page" class="toolBarFeedback" href="javascript:{}"></a>--%>
				</div>
			</div>
			<div id="brdInner" style="text-align:center;" class="CursorHandPointer">
				<%--You are here: 
				<asp:Label ID="LabelHome" runat="server" ForeColor="#0098DB" Text="Home"></asp:Label>--%>

			</div>
		</div>
      
	</div>
	<div id="contentAreaShadow">&nbsp;</div>

	<!-- feedback form -->
	<dx:ASPxPopupControl ID="ASPxPopupControlFeedback" 
		ClientInstanceName="popupFeedback" runat="server"
		SkinID="PopupNoHeader"
		HeaderText="Feedback"
		Width="530px"
		Height="740px"
		ContentUrl="~/Pages/Popup/Feedback.aspx"
		ContentUrlIFrameTitle="Feedback" 
		meta:resourcekey="ASPxPopupControlFeedbackResource1">
		<ContentCollection>
			<dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>

	<!-- recommendation form -->
	<dx:ASPxPopupControl ID="ASPxPopupControlRecommendation" 
		ClientInstanceName="popupRecommendation" runat="server"
		SkinID="PopupNoHeader"
		HeaderText="Recommendation"
		Width="530px"
		Height="530px"
		ContentUrl="~/Pages/Popup/Recommendation.aspx"
		ContentUrlIFrameTitle="Recommendation" 
		meta:resourcekey="ASPxPopupControlRecommendationResource1">
		<ContentCollection>
			<dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>

</div>
<!-- /CONTENT: HEADER AREA -->

