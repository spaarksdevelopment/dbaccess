﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.DBIntranet2010" EnableTheming="true"  Codebehind="DBIntranet2010.ascx.cs" %>

<%@ Register Src="~/Controls/Header/Status.ascx" TagName="Status" TagPrefix="hdr" %>

<!-- CONTENT: HEADER AREA -->
<div id="headerArea" class="trackMe">

	<!-- make sure intelli-sense works at design time -->
	<div runat="server">
		<% if (DesignMode) { %>
			<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
			<script type="text/javascript" src="/Scripts/ASPxScriptIntelliSense.js"></script>
		<% } %>
	</div>

   
	<script type="text/javascript" language="javascript">
	// <![CDATA[

		function GetCurrentFilePath()
		{
			return "<%= HttpContext.Current.Request.FilePath %>";
		}

		function ClosePopup_Feedback(bClear)
		{
			if (null == bClear) bClear = false;

			var ctrl_popupFeedback = ASPxClientPopupControl.Cast("popupFeedback");

			if (null != ctrl_popupFeedback)
			{
				if (bClear) ctrl_popupFeedback.RefreshContentUrl();		// reset page
				ctrl_popupFeedback.Hide();
			}
		}

		function ClosePopup_Recommendation(bClear)
		{
			if (null == bClear) bClear = false;

			var ctrl_popupRecommendation = ASPxClientPopupControl.Cast("popupRecommendation");

			if (null != ctrl_popupRecommendation)
			{
				if (bClear) ctrl_popupRecommendation.RefreshContentUrl(); 	// reset page
				ctrl_popupRecommendation.Hide();
			}
		}

		$(document).ready(function ()
		{
			$("#toolbarPrint").bind("click", function () { window.print(); });
			$("#toolbarBookmark").bind("click", function () { window.external.AddFavorite(location.href, document.title); });
			$("#toolbarFeedback").bind("click", function () { popupFeedback.Show(); });
			$("#toolbarRecommendation").bind("click", function () { popupRecommendation.Show(); });



			$("#toolbarPrint").attr("title", '<%=printTitle.Text %>');
		    $("#toolbarBookmark").attr("title", '<%=bookmarkLiteral.Text %>');
		    $("#toolbarFeedback").attr("title", '<%=FeedbackLiteral.Text %>');
		    $("#toolbarRecommendation").attr("title", '<%=RecLiteral.Text %>');
		});

	    // ]]>
	
	</script>
    
    <asp:Literal ID="printTitle" meta:resourcekey="PrintVersion" runat="server" ClientIDMode="Static" Visible="false"></asp:Literal>
    <asp:Literal ID="bookmarkLiteral" meta:resourcekey="AddBookmark" runat="server" ClientIDMode="Static" Visible="false"></asp:Literal>
    <asp:Literal ID="FeedbackLiteral" meta:resourcekey="VotePage" runat="server" ClientIDMode="Static" Visible="false"></asp:Literal>
    <asp:Literal ID="RecLiteral" meta:resourcekey="RecPage" runat="server" ClientIDMode="Static" Visible="false"></asp:Literal>


	<div id="topStage">
		<a id="clickLogo" href="#" title="" rel="logo"></a>
		<a href="/Default.aspx" target="_self" class="headerProjectName" title="">
			<asp:Image ID="headerImg" runat="server" ImageUrl="~/App_Themes/DBIntranet2010/img/name_db.gif" AlternateText="" />
		</a>
		<a href="/Default.aspx" target="_self" class="headerApplicationTitle" title="" runat="server" id="aTitle"></a>
	</div>

	<div class="trackMe" id="metaNavi">
		<ul id="list_meta1" class="horNav ">
			<li class=" first ">		
				<div style="float: left;">	
					<a id="pi_meta_507" href="http://www.db.com" title="Deutsche Bank Group" class="" target="_blank" meta:resourcekey="DBPotalDBNameTitle">
					<asp:Literal ID="DBPotalDBNameText" runat="server" meta:resourcekey="DBPotalDBNameText" ></asp:Literal>
					</a>&nbsp;		
				</div>
				<%--<div style="margin-left: 8px; position: absolute; width: 850px; margin-top:2px;">	--%>
				<div>
					<hdr:Status ID="HeaderStatus" runat="server"/>
				</div>
				<div style="float: left;">
					&nbsp;&nbsp;<asp:HyperLink runat="server" ID="LinkUserName" Target="_blank"></asp:HyperLink>
				</div>
				<%--</div>--%>
				<div style="clear:both"></div>
			</li>
		</ul>
		<br />
		<br />
		<span>
			<%--Last Login:&nbsp;<asp:Label ID="LabelLastLoggedIn" runat="server"></asp:Label>&nbsp;--%>
		</span>
	</div>

	<div id="level1NaviHolder">
		<div id="level1Navi" class="trackMe">

			<asp:LoginView ID="theLoginView" runat="server">
				<LoggedInTemplate>
					<ul id="LogOutLink" class="AspNet-Menu" style="float:right">
						<li class="AspNet-Menu first">
						<asp:LinkButton ID="logoutLinkButton" runat="server" OnClick="logoutLinkButton_click" CausesValidation="false">
						<asp:Literal ID="LogOut" ClientIDMode="Static" runat="server" meta:reosurceKey="LogOut"></asp:Literal>
						</asp:LinkButton></li>
					</ul>

					<asp:Menu ID="theMenu" ClientIDMode="Static" CssClass="hideOnDefault" SkinID="Menu" runat="server" EnableViewState="false" DataSourceID="SiteMapDataSource1" OnMenuItemDataBound="theMenu_MenuItemDataBound">
						<DataBindings>
							<asp:MenuItemBinding DataMember="Menu" NavigateUrlField="url" TextField="text" ValueField="text" />
						</DataBindings>
					</asp:Menu>

					<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" ShowStartingNode="false"/>

				</LoggedInTemplate>
			</asp:LoginView>

		</div>
	</div>

	<div id="breadcrumbNaviHolder">
		<div id="breadcrumbNavi">
			<div id="ToolBar_top" class="toolBarOuter">
				<span id="ToolBarNote_top" class="ToolBarNote"></span>
				<div class="toolBarInner">
					<a id="toolbarPrint"  class="toolBarPrint" href="javascript:{}"></a>
					<a id="toolbarRecommendation" class="toolBarSendafriend" href="javascript:{}" meta:resourcekey="RecPage"></a>
					<a id="toolbarBookmark"  class="toolBarBookmark" href="javascript:{}" meta:resourcekey="AddBookmark"></a>
					<a id="toolbarFeedback"  class="toolBarFeedback" href="javascript:{}" meta:resourcekey="VotePage"></a>
				</div>
			</div>
			<div id="brdInner"  class="CursorHandPointer">
				<asp:Literal ID="LiteralYouAreHere" runat="server" Text="<%$ Resources:SiteMapLocalizations, YouAreHereText %>"/> 
				<asp:SiteMapPath ID="SiteMapPath1" runat="server" CurrentNodeStyle-ForeColor="#0098DB" PathSeparator="&nbsp;&nbsp;|&nbsp;&nbsp;" ShowToolTips="true"></asp:SiteMapPath>
			</div>
		</div>
	</div>
	<div id="contentAreaShadow">&nbsp;</div>

	<!-- feedback form -->
	<dx:ASPxPopupControl ID="ASPxPopupControlFeedback" ClientInstanceName="popupFeedback" runat="server"
		SkinID="PopupNoHeader"
		HeaderText="Feedback"
		Width="530px"
		Height="740px"
		ContentUrl="~/Pages/Popup/Feedback.aspx"
		ContentUrlIFrameTitle="Feedback">
	</dx:ASPxPopupControl>

	<!-- recommendation form -->
	<dx:ASPxPopupControl ID="ASPxPopupControlRecommendation" ClientInstanceName="popupRecommendation" runat="server"
		SkinID="PopupNoHeader"
		HeaderText="Recommendation"
		Width="530px"
		Height="530px"
		ContentUrl="~/Pages/Popup/Recommendation.aspx"
		ContentUrlIFrameTitle="Recommendation">
	</dx:ASPxPopupControl>

</div>
<!-- /CONTENT: HEADER AREA -->
