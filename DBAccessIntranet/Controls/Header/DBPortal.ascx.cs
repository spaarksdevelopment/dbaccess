﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Resources;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Header
{
	// use BaseSecurityControl instead of System.Web.UI.UserControl
	// so that we can access the PageSecurity object in the OnPreRender method
	public partial class DBPortal : BaseSecurityControl
	{
		protected void theMenu_MenuItemDataBound (object sender, MenuEventArgs e)
		{
			string navigateUrl = e.Item.NavigateUrl.ToLower ();

			if (navigateUrl.Contains ("pages/error.aspx") ||
				navigateUrl.ToLower ().Contains ("pages/login.aspx"))
			{
				Menu menu = (Menu) sender;

				MenuItem menuItem = ((MenuItem) e.Item);

				if (menuItem.Parent == null)
				{
					menu.Items.Remove (menuItem);
				}
				else
				{
					menuItem.Parent.ChildItems.Remove (menuItem);
				}
			}
		}

		protected void logoutLinkButton_click (object sender, EventArgs e)
		{
			Session.Abandon ();
			FormsAuthentication.SignOut ();
			Response.Redirect (FormsAuthentication.LoginUrl, true);
		}

		protected override void OnPreRender (EventArgs e)
		{
			base.OnPreRender (e);

			if (Request.IsAuthenticated &&
				!Global.AdminLoginOverride)
			{
				LinkButton logoutLinkButton = (LinkButton) theLoginView.FindControl ("logoutLinkButton");

				if (logoutLinkButton != null)
				{
					logoutLinkButton.Visible = (ConfigurationManager.AppSettings["UseSSO"].ToUpper () != "TRUE");
				}

				Literal literal = (Literal)theLoginView.FindControl("LogOut");
				literal.Text = setLanguageText("LogOut");
			}

			//for sites other than live, add environment label
			if (!IsPostBack)
			{
				string AppEnv = ConfigurationManager.AppSettings["AppEnv"].ToUpper ();
				string AppTitle = ConfigurationManager.AppSettings["AppTitle"].ToString ();

				if (AppEnv != "LIVE")
				{
					AppTitle += " " + AppEnv;
				}
				aTitle.InnerText = AppTitle;

				string AppHelpPage = ConfigurationManager.AppSettings["AppHelpPage"].ToString ();
				string AppContactPage = ConfigurationManager.AppSettings["AppContactPage"].ToString ();
				string AppSitemapPage = ConfigurationManager.AppSettings["AppSitemapPage"].ToString ();

				aHelp.HRef = AppHelpPage;
				aContact.HRef = AppContactPage;
				aSitemap.HRef = AppSitemapPage;

				//if (base.PageSecurity.CurrentUser != null)
				//{
				//    string ImplementationDirectoryLink = ConfigurationManager.AppSettings["ImplementationDirectoryLink"].ToString () + base.PageSecurity.CurrentUser.dbPeopleID;
				//    LinkUserName.Text = base.PageSecurity.CurrentUser.FullName;
				//    LinkUserName.NavigateUrl = ImplementationDirectoryLink;
				//    //RoleType.Text = "[" + base.PageSecurity.CurrentUser.RolesNamesList + "]";

				//}
				//else
				//{
					LinkUserName.Visible = false;
				//}
			}
		}

		public string setLanguageText(string text)
		{
			ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
			return resourceManager.GetString(Convert.ToString(text));
		}
	}
}
