﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.Script.Services;
using System.Web.Services;
using System.Text;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
    public partial class OutOfOfficeControl : BaseControl
    {
        private string _PageTitle = "My Out Of Office";

        public string PageTitle 
        {
            get { return _PageTitle; }
            set { _PageTitle = value; } 
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            ASPxDateEditEnableFromDate.CssClass = "DateEditTextBox";
            ASPxDateEditEnableUntilDate.CssClass = "DateEditTextBox";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["dbPeopleID"] != null)
            {
                DBAccessController.DAL.HRPerson oooPerson = Director.GetUserBydbPeopleID(Convert.ToInt32(Request.QueryString["dbPeopleID"]));

                LabelHelp.Text = LabelHelpForPerson.Text;

                if (oooPerson != null)
                {
                    PageTitle = string.Format("{0} {1} {2}", LabelTitleForPerson.Text, oooPerson.Forename, oooPerson.Surname);
                    LabelTitle.Text = PageTitle;
                }
                else
                {
                    PopupTemplates.HeaderText = ErrorPopupHeader.Text;
                    PopupTemplates.MessageText = ErrorPopupMsg.Text;
                    PopupTemplates.PopupControl.ShowOnPageLoad = true;
                    return;
                }
            }

            if (!Page.IsPostBack)
            {
                Session["dbPeopleID"] = null;

                if (Request.QueryString["dbPeopleID"] == null)
                    Session["dbPeopleID"] = ((DBAppUser)new DBSqlMembershipProvider().GetUser()).dbPeopleID;
                else
                    Session["dbPeopleID"] = Request.QueryString["dbPeopleID"];
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            base.RegisterScriptInclude("OutOfOfficeControl", "~/scripts/Specific/OutOfOfficeControl.js");
           
            ASPxDateEditEnableFromDate.MinDate = DateTime.Today;
            ASPxDateEditEnableFromDate.Date = DateTime.Today;
            ASPxDateEditEnableUntilDate.MinDate = DateTime.Today;
            ASPxDateEditEnableUntilDate.Date = DateTime.Today;
            GetOutOfOffice();
            base.OnPreRender(e);
        }
        
        public void GetOutOfOffice()
        {
            var OutOfOffice = Director.GetOutOfOffice(Convert.ToInt32(Session["dbPeopleID"]));
            if (OutOfOffice.Count > 0)
            {
                DBAccessController.DAL.OutOfOffice mostRecentOutOfOffice = OutOfOffice.OrderByDescending(a => a.LeaveDate).First();

                ASPxDateEditEnableFromDate.Date = mostRecentOutOfOffice.LeaveDate;
                ASPxDateEditEnableUntilDate.Date = mostRecentOutOfOffice.ReturnDate;
                                
                FromDateDisable.Text = mostRecentOutOfOffice.LeaveDate.ToString("dd-MMM-yyyy");
                UntilDateDisable.Text = mostRecentOutOfOffice.ReturnDate.ToString("dd-MMM-yyyy");

                ASPxHiddenFieldOutOfOfficeEnabled.Set("Enabled", "true");
            }
            else
            {
                ASPxHiddenFieldOutOfOfficeEnabled.Set("Enabled", "false");
            }
        }

        /// <summary>
        /// Method to delete current out of office
        /// E.Parker 11.03.2011
        /// </summary>
        public void DeleteOutOfOffice(object sender, EventArgs e)
        {
            PopupRemoveOutOfOffice.ShowOnPageLoad = false;

            bool enabled = Convert.ToBoolean(ASPxHiddenFieldOutOfOfficeEnabled.Get("Enabled"));
            if (enabled)
            {
                ASPxHiddenFieldOutOfOfficeEnabled.Set("Enabled", "false");

                DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();

                Director director = new Director(CurrentUser.dbPeopleID);
                bool success = director.DeleteOutOffice(Convert.ToInt32(HttpContext.Current.Session["dbPeopleID"]));

                if (success)
                {
                    ASPxDateEditEnableFromDate.Value = DateTime.Today.ToString("dd-MMM-yyyy");
                    ASPxDateEditEnableUntilDate.Value = DateTime.Today.ToString("dd-MMM-yyyy");
                    
                    FromDateDisable.Text = null;
                    UntilDateDisable.Text = null;
                }
                else
                    ASPxHiddenFieldOutOfOfficeEnabled["Enabled"] = true;
                
                string script = "$(document).ready(function() {OnDeleteOutOfOffice(" + success.ToString().ToLower() + ");});";
                base.RegisterClientScriptBlock(Guid.NewGuid().ToString(), script);
            }
        }

        /// <summary>
        /// Method to add out of office
        /// E.Parker
        /// 14.06.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void addOutOfOffice(object sender, EventArgs e)
        {
            PopupAddOutOfOffice.ShowOnPageLoad = false;
            
            DateTime LeaveDate = ASPxDateEditEnableFromDate.Date;
            DateTime ReturnDate = ASPxDateEditEnableUntilDate.Date;
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            Director director = new Director(CurrentUser.dbPeopleID);
            bool success = director.CreateNewOutOfOffice(LeaveDate, ReturnDate, Convert.ToInt32(HttpContext.Current.Session["dbPeopleID"]), 0);
            string script = "$(document).ready(function() {OnSetOutOfOffice(" + success.ToString().ToLower() + ");});";
            base.RegisterClientScriptBlock(Guid.NewGuid().ToString(), script);
        }

        #region Web Methods
       
        #endregion
    }
}