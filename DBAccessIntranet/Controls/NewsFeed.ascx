﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsFeed.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.NewsFeed" %>
    <%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
    <style type="text/css">
		td.dxheContentArea { background-color: White; }
		.dxhePreviewArea { border-style: none; }
	</style>
	
	<div style="float: left; width: 100%; margin-right: 0%";>
		<dx:ASPxHtmlEditor ID="NewsHtmlEditor" runat="server" ClientInstanceName="HtmlEditor" Height="500px" Width="350px" style="padding-right: 15px; padding-left: 15px; padding-top: 15px; padding-bottom: 15px">
            <Toolbars>
                <dx:HtmlEditorToolbar>
                    <Items>
                        <dx:ToolbarCutButton />
                        <dx:ToolbarCopyButton />
                        <dx:ToolbarPasteButton />
                        <dx:ToolbarUndoButton BeginGroup="True" />
                        <dx:ToolbarRedoButton />
                        <dx:ToolbarRemoveFormatButton BeginGroup="True" />
                        <dx:ToolbarSuperscriptButton BeginGroup="True" />
                        <dx:ToolbarSubscriptButton />
                        <dx:ToolbarInsertOrderedListButton BeginGroup="True" />
                        <dx:ToolbarInsertUnorderedListButton />
                        <dx:ToolbarIndentButton BeginGroup="True" />
                        <dx:ToolbarOutdentButton />
                        <dx:ToolbarInsertLinkDialogButton BeginGroup="True" />
                        <dx:ToolbarUnlinkButton />
                        <dx:ToolbarInsertImageDialogButton />
                        <dx:ToolbarTableOperationsDropDownButton BeginGroup="True">
                            <Items>
                                <dx:ToolbarInsertTableDialogButton BeginGroup="True" ViewStyle="ImageAndText">
                                </dx:ToolbarInsertTableDialogButton>
                                <dx:ToolbarTablePropertiesDialogButton BeginGroup="True">
                                </dx:ToolbarTablePropertiesDialogButton>
                                <dx:ToolbarTableRowPropertiesDialogButton>
                                </dx:ToolbarTableRowPropertiesDialogButton>
                                <dx:ToolbarTableColumnPropertiesDialogButton>
                                </dx:ToolbarTableColumnPropertiesDialogButton>
                                <dx:ToolbarTableCellPropertiesDialogButton>
                                </dx:ToolbarTableCellPropertiesDialogButton>
                                <dx:ToolbarInsertTableRowAboveButton BeginGroup="True">
                                </dx:ToolbarInsertTableRowAboveButton>
                                <dx:ToolbarInsertTableRowBelowButton>
                                </dx:ToolbarInsertTableRowBelowButton>
                                <dx:ToolbarInsertTableColumnToLeftButton>
                                </dx:ToolbarInsertTableColumnToLeftButton>
                                <dx:ToolbarInsertTableColumnToRightButton>
                                </dx:ToolbarInsertTableColumnToRightButton>
                                <dx:ToolbarSplitTableCellHorizontallyButton BeginGroup="True">
                                </dx:ToolbarSplitTableCellHorizontallyButton>
                                <dx:ToolbarSplitTableCellVerticallyButton>
                                </dx:ToolbarSplitTableCellVerticallyButton>
                                <dx:ToolbarMergeTableCellRightButton>
                                </dx:ToolbarMergeTableCellRightButton>
                                <dx:ToolbarMergeTableCellDownButton>
                                </dx:ToolbarMergeTableCellDownButton>
                                <dx:ToolbarDeleteTableButton BeginGroup="True">
                                </dx:ToolbarDeleteTableButton>
                                <dx:ToolbarDeleteTableRowButton>
                                </dx:ToolbarDeleteTableRowButton>
                                <dx:ToolbarDeleteTableColumnButton>
                                </dx:ToolbarDeleteTableColumnButton>
                            </Items>
                        </dx:ToolbarTableOperationsDropDownButton>
                    </Items>
                </dx:HtmlEditorToolbar>
                <dx:HtmlEditorToolbar>
                    <Items>
                        <dx:ToolbarFontNameEdit>
                            <Items>
                                <dx:ToolbarListEditItem Value="Times New Roman" Text="Times New Roman"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="Tahoma" Text="Tahoma"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="Verdana" Text="Verdana"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="Arial" Text="Arial"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="MS Sans Serif" Text="MS Sans Serif"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="Courier" Text="Courier"></dx:ToolbarListEditItem>
                            </Items>
                        </dx:ToolbarFontNameEdit>
                        <dx:ToolbarFontSizeEdit>
                            <Items>
                                <dx:ToolbarListEditItem Value="1" Text="1 (8pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="2" Text="2 (10pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="3" Text="3 (12pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="4" Text="4 (14pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="5" Text="5 (18pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="6" Text="6 (24pt)"></dx:ToolbarListEditItem>
                                <dx:ToolbarListEditItem Value="7" Text="7 (36pt)"></dx:ToolbarListEditItem>
                            </Items>
                        </dx:ToolbarFontSizeEdit>
                        <dx:ToolbarBoldButton BeginGroup="True" />
                        <dx:ToolbarItalicButton />
                        <dx:ToolbarUnderlineButton />
                        <dx:ToolbarStrikethroughButton />
                        <dx:ToolbarJustifyLeftButton BeginGroup="True" />
                        <dx:ToolbarJustifyCenterButton />
                        <dx:ToolbarJustifyRightButton />
                        <dx:ToolbarJustifyFullButton />
                        <dx:ToolbarBackColorButton BeginGroup="True" />
                        <dx:ToolbarFontColorButton />
                    </Items>
                </dx:HtmlEditorToolbar>
            </Toolbars>
            <Styles>
                <ViewArea Font-Names="Tahoma">
                </ViewArea>
            </Styles>

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsImageSelector>
<CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
</SettingsImageSelector>

<SettingsDocumentSelector>
<CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp"></CommonSettings>
</SettingsDocumentSelector>
			<Border BorderStyle="None" />
        </dx:ASPxHtmlEditor>

		<dx:ASPxButton ID="ButtonSaveNewsHtml" AutoPostBack="false" runat="server" Text="Save" style="float: right; padding-right: 15px; padding-left: 5px; padding-top: 10px; padding-bottom: 15px" OnClick="ButtonSaveNewsHtml_Click"/>
    </div>
        
