﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsTicker.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.NewsTicker" %>
<link href="../App_Themes/DBIntranet2010/css/ticker.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
	$(document).ready(function () {
		setInterval(function () { tick() }, 3000);
	});

	function tick() {
		$('#ticker li:first').slideUp(
			function () {
				$(this).appendTo($('#ticker')).slideDown(); 
			}
		);
	}
</script>

<ul id="ticker">
    <li>
		This is a news item.
    </li>
    <li>
		New version of DBAccess to be released.
    </li>
    <li>
        DBaccess news stuff
    </li>
    <li>
        More News
    </li>
</ul>