﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using System.Web.Services;
using System.Web.Script.Services;


namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls
{
    public partial class LocationSelectorAdmin : BaseControl
    {
        #region pageLifeCycle events
        protected void Page_Load(object sender, EventArgs e)
        {
          GetData();
        }

        protected override void OnInit(EventArgs e)
        {
            
        }

        #endregion

        #region ASPxGridView methods
        /// <summary>
        /// method to determine if the add new link should show // is hidden for cities.
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public bool setShowNew(object itemID)
        {
            if (Convert.ToString(itemID).Contains("Ci"))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// method to determine what text should show on the add new link
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public string setTextNew(object itemID)
        {
            if (Convert.ToString(itemID).Contains("R"))
            {
                //return "Add Country";
                return GetResourceManager("Resources.CommonResource","AddCountry");
            }
            else if (Convert.ToString(itemID).Contains("C"))
            {
               // return "Add City";
                return GetResourceManager("Resources.CommonResource", "AddCity");
            }
            else
            {
                return "";
            }
        }

        #endregion

        #region DataBind

        public void GetData()
        {
            DBAppUser CurrentUser = (DBAppUser)new DBSqlMembershipProvider().GetUser();
            List<DBAccessController.DAL.GetAdminLocationsData> locations = Director.GetAdminLocationsforList(CurrentUser.LanguageId);

            if (base.IsRestrictedUser)
            {
                locations = locations.Where(e => (e.itemId == RestrictedCountryId + "C") || (e.parentId == RestrictedCountryId + "C")).ToList();
            }

            BindGrid(locations);
        }

      
        /// <summary>
        /// method to bind the data to the treelist
        /// E.Parker 
        /// 12.04.11
        /// </summary>
        /// <param name="locations"></param>
        public void BindGrid(List<DBAccessController.DAL.GetAdminLocationsData> locations)
        {
            locationAdmin.DataSource = locations;
            locationAdmin.DataBind();
        }

        #endregion


        #region WebMethods
        [ScriptMethod, WebMethod]
        public static void RefreshLocationGrid()
        {
            LocationSelectorAdmin refresh = new LocationSelectorAdmin();
            refresh.GetData();
        }
        #endregion


        /// <summary>
        /// Method to refresh the tree list data
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tree_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            GetData();
        }

		#region Highlight tree row on mouse over
		protected void tree_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}
		#endregion

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
    }
}