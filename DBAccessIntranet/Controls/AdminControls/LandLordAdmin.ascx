﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandLordAdmin.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls.LandLordAdmin" %>

<script type="text/javascript">
    $(document).ready(function () {
        hdLandlordAdmin.Set("poURL", '<%= Page.ResolveUrl("~/Pages/Popup/LandlordAdminPopupPage.aspx") %>');
//        $('#landlordImg').click(function (e) {
//            showEditLandlord(0, 'add');
//        });
        $("#landlordHelp").hide();
        $('#HelpLandlord').click(function (e) {
            $("#landlordHelp").show();
        });
    });

    function showEditLandlord(landlordID, type) {
        var popupLandlordURL = hdLandlordAdmin.Get("poURL");

        if (type == 'add') {
            popupLandlordURL = popupLandlordURL + "?type=add";
        }
        else {
            popupLandlordURL = popupLandlordURL + "?type=edit&landlordID=" + landlordID;
        }
        popupAddLandlord.SetContentUrl(popupLandlordURL);
        popupAddLandlord.Show();
    }

    function ErrorShowPopup()
    {
    	popupAddLandlord.Hide();
    	popupAlertTemplateContent.SetText('<%= GetResourceManager("Resources.CommonResource", "ErrorTryAgain") %>');
    	popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Error") %>');
    	popupAlertTemplate.Show();
    }

    function RefreshLandlordGrid() {
        popupAddLandlord.Hide();
        gvLandlords.PerformCallback();
    }

    function CancelEditLandlord() {
        popupAddLandlord.Hide();
    }

</script>

<style type="text/css">
	#ctl00_DBMainContent_LandLord_landlordGrid_col7 { cursor:default !important; }
</style>

<dx:ASPxGridView ID="landlordGrid" ClientInstanceName="gvLandlords"
        runat="server" AutoGenerateColumns="False" OnCustomCallback="gvLandlord_CustomCallback"     
        KeyFieldName="LandlordID"          
        ClientIDMode="AutoID"  OnHtmlRowCreated="gvLandlord_HtmlRowCreated" Width="70%">
		<Settings ShowStatusBar="Visible" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, NameTitle%>" FieldName="Name"  ShowInCustomizationForm="True" Visible="true"  >                     
            </dx:GridViewDataTextColumn>     
            <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, AddressTitle%>" FieldName="Address"  ShowInCustomizationForm="True" Visible="true"  >                     
            </dx:GridViewDataTextColumn>          
            <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, TelephoneLiteral%>" FieldName="Telephone"  ShowInCustomizationForm="True" Visible="true"  >                     
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, EmailTitle%>" FieldName="Email"  ShowInCustomizationForm="True" Visible="true"  >                     
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="<%$Resources:SiteMapLocalizations, RequiresPhotoTitle%>" FieldName="RequiresPhoto" Visible="true" />
            <dx:GridViewDataCheckColumn Caption="<%$Resources:SiteMapLocalizations, RequiresMiFareTitle%>" FieldName="RequiresMiFare" Visible="true" />
            <dx:GridViewDataCheckColumn Caption="<%$Resources:SiteMapLocalizations, RequiresEmailTitle%>" FieldName="RequiresEmail" Visible="true" />
            <dx:GridViewDataTextColumn Caption="&nbsp;">
                <DataItemTemplate>                                  
                    <asp:HyperLink ID="Edit" NavigateUrl="javascript:void(0);"  onclick='<%# "showEditLandlord(\"" + Container.KeyValue + "\",\"edit\");" %>' runat="server" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" ></asp:HyperLink>                
                </DataItemTemplate>
            	<HeaderStyle>
				<BackgroundImage ImageUrl="~/App_Themes/DBIntranet2010/images/editWithSpace.gif" 
					Repeat="NoRepeat" VerticalPosition="center" />
				</HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
		<Templates>
			<StatusBar>
				<div style="text-align: right;">
					<dx:ASPxButton ID="btnAddLandlord" AutoPostBack="false" runat="server" Text="<%$ Resources:CommonResource, AddNew%>">
						<ClientSideEvents Click='function(s,e){ showEditLandlord(0, "add"); }' />
					</dx:ASPxButton>
				</div>
			</StatusBar>
		</Templates>
</dx:ASPxGridView>

<!--popups-->
<dx:ASPxPopupControl ID="ASPxPopupAddLandlord" ClientInstanceName="popupAddLandlord" runat="server"
	SkinID="PopupCustomSize"
	HeaderText= "<%$ Resources:CommonResource, LandlordAdmin%>"
	ShowHeader="true"
	Width="500px"
	Height="350px"
	ContentUrl="~/Pages/Popup/LandlordAdminPopupPage.aspx"
	ContentUrlIFrameTitle="LandlordAdmin">
</dx:ASPxPopupControl>

<!--hiddenfield-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldLandlordAdmin" ClientInstanceName="hdLandlordAdmin" runat="server">
</dx:ASPxHiddenField>
