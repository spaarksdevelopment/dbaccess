﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using DevExpress.Web.ASPxGridView;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls
{
    public partial class PassOfficeAdmin : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind();
        }
   
        /// <summary>
        /// Method to bind the data to the data grid
        /// E.Parker
        /// </summary>
        public void Bind()
        {
            List<DBAccessController.DAL.LocationPassOffice> data = Director.GetAdminPassOffices();

            if (base.IsRestrictedUser)
            {
                //  get a list of cities for the country
               List<DBAccessController.DAL.LocationCity> cities = Director.GetCitiesForCountry(RestrictedCountryId, true);
               List<int?> citiesList = new List<int?>();

               foreach (DBAccessController.DAL.LocationCity item in cities)
               {
                   citiesList.Add(item.CityID);
               }

               data = data.Where(e => citiesList.Contains(e.CityID)).ToList();
            }

            GetTranslatedPassOffice(ref data);
            ASPxGridcurrentPassOffice.DataSource = data;
            ASPxGridcurrentPassOffice.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);           
        }

        /// <summary>
        /// Method to show whether the pass office is enabled or not
        /// E.Parker
        /// 18.04.11
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public string setEnabled(object status)
        {
            
            string returnValue;
            if (status == DBNull.Value || Convert.ToString(status) == "True")
            {
                switch (CurrentUser.LanguageId)
                {
                    case 1:
                        returnValue = "Yes";
                    break;
                    case 2:
                        returnValue = "Ja";
                    break;
                    case 3:
                        returnValue = "Sì";
                     break;
                    default:
                       returnValue= "Yes";
                    break;
                }
            }
            else
            {
                switch (CurrentUser.LanguageId)
                {
                    case 1:
                        returnValue = "No";
                        break;
                    case 2:
                        returnValue = "Nein";
                        break;
                    case 3:
                        returnValue = "No";
                        break;
                    default:
                        returnValue = "No";
                        break;
                }
            }
            return returnValue;    
        }

        private void GetTranslatedPassOffice(ref  List<DBAccessController.DAL.LocationPassOffice> data)
        {
            switch (CurrentUser.LanguageId)
            {
                case 1:
                    //do nothing
                    break;
                case 2:
                    foreach (DBAccessController.DAL.LocationPassOffice lpass in data)
                    {
                        lpass.Name = lpass.GermanName;
                    }
                    break;
                case 3:
                    foreach (DBAccessController.DAL.LocationPassOffice lpass in data)
                    {
                        lpass.Name = lpass.ItalyName;
                    }
                    break;
                default:

                    break;

            }
        }
        #region CallBack
        protected void gvPassOffice_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            Bind();

        }
        #endregion
		
		#region Highlight grid row on mouse over
		protected void gvPassOffice_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}
		#endregion

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
	}
}