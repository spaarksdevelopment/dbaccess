﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationSelectorAdmin.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls.LocationSelectorAdmin" %>

<%@ Register Src="~/Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Selectors/TimeZone.ascx" TagName="TimeZone" TagPrefix="uc1" %>

<script type="text/javascript">
    $(document).ready(function () {     
        hdLocationAdmin.Set("regionURL", '<%= Page.ResolveUrl("~/Pages/Popup/RegionAdmin.aspx") %>');
        hdLocationAdmin.Set("countryURL", '<%= Page.ResolveUrl("~/Pages/Popup/CountryAdmin.aspx") %>');
        hdLocationAdmin.Set("cityURL", '<%= Page.ResolveUrl("~/Pages/Popup/CityAdmin.aspx") %>');
    });
    function showAddPopup(RequestID) {        
        if (RequestID.indexOf("R") >= 0) {
            var popupUrlEditCountry = hdLocationAdmin.Get("countryURL");
            popupUrlEditCountry = popupUrlEditCountry + "?type=add&regionID=" + RequestID;
            popupEditCountry.SetContentUrl(popupUrlEditCountry);            
            popupEditCountry.Show();
        }
        else if(RequestID.indexOf("C") >=0){
            var popupUrlCity = hdLocationAdmin.Get("cityURL");
            popupUrlCity = popupUrlCity + "?type=add&countryID=" + RequestID;
            popupEditCity.SetContentUrl(popupUrlCity);
            popupEditCity.Show();
        }
    } 
    function showEditPopup(RequestID) {
        if (RequestID.indexOf("R") >= 0) {
            var popupUrlRegion = hdLocationAdmin.Get("regionURL");
            popupUrlRegion = popupUrlRegion + "?type=edit&regionID=" + RequestID;
            popupEditRegion.SetContentUrl(popupUrlRegion);            
            popupEditRegion.Show();
        }
        else if (RequestID.indexOf("Ci") >= 0) {
            var popupUrl = hdLocationAdmin.Get("cityURL");
            popupUrl = popupUrl + "?type=edit&cityid=" + RequestID;
            popupEditCity.SetContentUrl(popupUrl);            
            popupEditCity.Show();
        }
        else {
            var popupUrlCountry = hdLocationAdmin.Get("countryURL"); ;
            popupUrlCountry = popupUrlCountry + "?type=edit&countryid=" + RequestID ;
            popupEditCountry.SetContentUrl(popupUrlCountry);            
            popupEditCountry.Show();
        }
    }
    
    function CancelEditCity() {
        popupEditCity.Hide();
    }    
    function CancelEditCountry() {
        popupEditCountry.Hide();
    }
    function CancelEditRegion() {
        popupEditRegion.Hide();
    }
    function RefreshLocationGrid(popupClose) {
        if (popupClose == "region") {
            popupEditRegion.Hide();
        }
        else if (popupClose == "country") {
            popupEditCountry.Hide();
        }
        else if (popupClose == "city") {
            popupEditCity.Hide();
        }
        tvLocations.PerformCustomCallback();
    }

    function validation() {
        popupEditCountry.Hide();
        popupAlertTemplateContent.SetText("<%=LocationSelectorSelectPassOffice.Text %>"); //You must select a Pass Office.
        popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Error") %>');//Error
        popupAlertTemplate.Show();
    }
    function ErrorShowPopup(type)
    {
    	if (type == "country")
    	{
    		popupEditCountry.Hide();

    	}
    	else if (type == "city")
    	{
    		popupEditCity.Hide();
    	}
  popupAlertTemplateContent.SetText("<%=LocationSelectorTryAgain.Text %>"); //An error occurred, please check and try again.
  popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Error") %>');
    	popupAlertTemplate.Show();
    }

</script>
<style type="text/css">
	#ctl00_DBMainContent_Location_locationAdmin_DX-DnD-H-6 { cursor:default !important; }
	#ctl00_DBMainContent_Location_locationAdmin_DX-DnD-H-7 { cursor:default !important; }
</style>


 <dx:ASPxTreeList ID="locationAdmin" runat="server" Width="70%" AutoGenerateColumns="False"
       KeyFieldName="itemId" ParentFieldName="parentId" 
	ClientInstanceName="tvLocations" OnCustomCallback="tree_CustomCallback" 
	OnHtmlRowPrepared="tree_HtmlRowPrepared">
    <Settings GridLines="Both" />
    <SettingsBehavior ExpandCollapseAction="NodeDblClick" />       
    <Columns>            
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, code %>" FieldName="code" >
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, Name%>" FieldName="name" >
        </dx:TreeListTextColumn>
        <dx:TreeListCheckColumn Caption="<%$ Resources:CommonResource, Enabled%>" FieldName="enabled">
        </dx:TreeListCheckColumn>
        <dx:TreeListDataColumn Caption="<%$ Resources:CommonResource, CostCentre%>" FieldName="CostCentreCode" >
        </dx:TreeListDataColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, PassOffice %>" FieldName="PassOffice" >
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, TimeZoneColumnHeader %>" FieldName="TimeZone" >
        </dx:TreeListTextColumn>
        <dx:TreeListDataColumn Caption="&nbsp;" AllowSort="False">
            <DataCellTemplate>                                     
                <asp:HyperLink ID="Edit" NavigateUrl="javascript:void(0);" runat="server"  onclick='<%# "showEditPopup(\"" + Container.NodeKey + "\");" %>' Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" />               
            </DataCellTemplate>
        	<HeaderStyle HorizontalAlign="Left">
			<Paddings PaddingLeft="20px" />
			<BackgroundImage HorizontalPosition="left" 
				ImageUrl="~/App_Themes/DBIntranet2010/images/editWithSpace.gif" Repeat="NoRepeat" 
				VerticalPosition="center" />
			</HeaderStyle>
        </dx:TreeListDataColumn>
		<dx:TreeListDataColumn Caption="&nbsp;" AllowSort="False">
            <DataCellTemplate>                                     
                <asp:HyperLink ID="AddNew" NavigateUrl="javascript:void(0);"  onclick='<%# "showAddPopup(\"" + Container.NodeKey + "\");" %>' runat="server" Visible="<%#setShowNew(Container.NodeKey) %>" Text='<%#setTextNew(Container.NodeKey)%>'></asp:HyperLink>                
            </DataCellTemplate>
        	<HeaderStyle>
			<BackgroundImage ImageUrl="~/App_Themes/DBIntranet2010/images/Add_hlWithSpace.gif" 
				Repeat="NoRepeat" />
			</HeaderStyle>
        </dx:TreeListDataColumn>
    </Columns>
    <Settings ShowFooter="true" ShowGroupFooter="false" GridLines="Both" />
        <SettingsBehavior AutoExpandAllNodes="false" ProcessSelectionChangedOnServer="true"
            ExpandCollapseAction="NodeDblClick" />        
</dx:ASPxTreeList>

<!-- edit city-->
<dx:ASPxPopupControl ID="ASPxPopupControlEditCity" ClientInstanceName="popupEditCity" runat="server"
	SkinID="PopupCustomSize"
	meta:resourcekey="LocationSelectorCityAdmin"
	ShowHeader="true"
	Width="400px"
	Height="280px"
	ContentUrl="~/Pages/Popup/CityAdmin.aspx"
	ContentUrlIFrameTitle="CityPopup">
</dx:ASPxPopupControl>
<!--edit country-->
<dx:ASPxPopupControl ID="ASPxPopupControlEditCountry" ClientInstanceName="popupEditCountry" runat="server"
	SkinID="PopupCustomSize"
	meta:resourcekey="LocationSelectorCountryAdmin"
	ShowHeader="true"
	Width="500px"
	Height="320px"
	ContentUrl="~/Pages/Popup/CountryAdmin.aspx"
	ContentUrlIFrameTitle="CountryPopup">
</dx:ASPxPopupControl>
<!--edit region-->
<dx:ASPxPopupControl ID="ASPxPopControlEditRegion" ClientInstanceName="popupEditRegion" runat="server"
	SkinID="PopupCustomSize"
	meta:resourcekey="LocationSelectorRegionAdmin"
	ShowHeader="true"
	Width="400px"
	Height="275px"
	ContentUrl="~/Pages/Popup/RegionAdmin.aspx"
	ContentUrlIFrameTitle="RegionPopup">
</dx:ASPxPopupControl>

<!--hidden field-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldLocationAdmin" ClientInstanceName="hdLocationAdmin" runat="server">
</dx:ASPxHiddenField>
<dx:ASPxHiddenField ID="ASPxHiddenFieldTree" ClientInstanceName="cacheTree" runat="server">
</dx:ASPxHiddenField>

<!--literals-->
    <%-- <asp:Literal ID="DeletePersonLiteral"  text="<%$ Resources:CommonResource, DeletePerson%>" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>--%>
     <asp:Literal ID="LocationSelectorSelectPassOffice"  meta:resourcekey="LocationSelectorSelectPassOffice" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
     <asp:Literal ID="LocationSelectorTryAgain"  meta:resourcekey="LocationSelectorTryAgain" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
    
