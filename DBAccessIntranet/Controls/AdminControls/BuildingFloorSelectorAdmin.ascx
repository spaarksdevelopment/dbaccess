﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BuildingFloorSelectorAdmin.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls.BuildingFloorSelectorAdmin" %>

<script type="text/javascript">
    $(document).ready(function () {   
        hdSelectorAdmin.Set('buildingURL', '<%= Page.ResolveUrl("~/Pages/Popup/BuildingAdmin.aspx") %>');
        hdSelectorAdmin.Set('floorURL', '<%= Page.ResolveUrl("~/Pages/Popup/FloorAdmin.aspx") %>');      
    });
    function showEditSelector(RequestID) {
        if (RequestID.indexOf("B") >= 0) {
            var popupURLEditBuilding = hdSelectorAdmin.Get("buildingURL");
            popupURLEditBuilding = popupURLEditBuilding + "?type=edit&BuildingID=" + RequestID;
            popupEditBuilding.SetContentUrl(popupURLEditBuilding);
            popupEditBuilding.Show();
        }
        else {
            var popupURLEditFloor = hdSelectorAdmin.Get("floorURL");
            popupURLEditFloor = popupURLEditFloor + "?type=edit&FloorID=" + RequestID;
            popupEditFloor.SetContentUrl(popupURLEditFloor);
            popupEditFloor.Show();
        }
    }

    function showAddSelector(RequestID) {
        if (RequestID.indexOf("Ci") >= 0) {
            //add Building
            var popupURLEditBuilding = hdSelectorAdmin.Get("buildingURL");
            popupURLEditBuilding = popupURLEditBuilding + "?type=add&CityID=" + RequestID;
            popupEditBuilding.SetContentUrl(popupURLEditBuilding);            
            popupEditBuilding.Show();
        }
        else {
            //add floor
            var popupURLEditFloor = hdSelectorAdmin.Get("floorURL");
            popupURLEditFloor = popupURLEditFloor + "?type=add&BuildingID=" + RequestID;
            popupEditFloor.SetContentUrl(popupURLEditFloor);
            popupEditFloor.Show();
        }
    }   
    function CancelEditFloor() {
        popupEditFloor.Hide();
    }
    function CancelEditBuilding() {
        popupEditBuilding.Hide();
    }

    function ErrorShowPopup(type)
    {
    	if (type == "building")
    	{
    		popupEditBuilding.Hide();
    	}
    	else if (type == "floor")
    	{
    		popupEditFloor.Hide();
    	}
  popupAlertTemplateContent.SetText('<%= GetResourceManager("Resources.CommonResource", "ErrorTryAgain") %>'); //An error occurred, please check and try again.
  popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Error") %>');
    	popupAlertTemplate.Show();
    }

    function refreshTree(popupClose) {
        if (popupClose == "building") {
            popupEditBuilding.Hide();
        }
        else if (popupClose == "floor") {
            popupEditFloor.Hide();
        }       
        tvLocationFloors.PerformCustomCallback();
    }
</script>
    <style type="text/css">
		#ctl00_DBMainContent_buildings_locationFloors_DX-DnD-H-9 { cursor:default !important; }
		#ctl00_DBMainContent_buildings_locationFloors_DX-DnD-H-10 { cursor:default !important; }
	</style>
 <dx:ASPxTreeList ID="locationFloors" ClientInstanceName="tvLocationFloors" 
	runat="server" Width="70%" AutoGenerateColumns="False"
       KeyFieldName="itemId" ParentFieldName="parentId"
        OnCustomCallback="tree_CustomCallback" 
	OnHtmlRowPrepared="tree_HtmlRowPrepared">
        <Settings GridLines="Both" />
        <SettingsBehavior ExpandCollapseAction="NodeDblClick" />       
        <Columns>            
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, RegionNameColumnHeader %>" FieldName="RegionName" CellStyle-Wrap="True" >
            </dx:TreeListTextColumn>
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, CountryNameColumnHeader %>" FieldName="CountryName" >
            </dx:TreeListTextColumn>
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, CityNameColumnHeader %>" FieldName="CityName" >
            </dx:TreeListTextColumn>
            <dx:TreeListDataColumn Caption="<%$ Resources:SiteMapLocalizations, CodeColumnHeader %>" FieldName="Code" >
            </dx:TreeListDataColumn>
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, BuildingNameColumnHeader %>" FieldName="BuildingName" >
            </dx:TreeListTextColumn>
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, StreetAddressColumnHeader %>" FieldName="StreetAddress" >
            </dx:TreeListTextColumn>     
            <dx:TreeListTextColumn Caption="<%$ Resources:SiteMapLocalizations, FloorNameColumnHeader %>" FieldName="FLoorName" >
            </dx:TreeListTextColumn>
            <dx:TreeListCheckColumn Caption="<%$ Resources:SiteMapLocalizations, BuildingEnabledColumnHeader %>" FieldName="BuildingEnabled" >
            </dx:TreeListCheckColumn>
            <dx:TreeListCheckColumn Caption="<%$ Resources:SiteMapLocalizations, FloorEnabledColumnHeader %>" FieldName="FloorEnabled">
            </dx:TreeListCheckColumn>
            <dx:TreeListDataColumn Caption="&nbsp;" Width="50" AllowSort="False">
                <DataCellTemplate>    
                    <asp:HyperLink ID="Edit" NavigateUrl="javascript:void(0);" runat="server"  onclick='<%# "showEditSelector(\"" + Container.NodeKey + "\");" %>' Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>"  Visible="<%#setTextEdit(Container.NodeKey) %>" />               
                 </DataCellTemplate>
            	<HeaderStyle>
				<BackgroundImage ImageUrl="~/App_Themes/DBIntranet2010/images/editWithSpace.gif" 
					Repeat="NoRepeat" VerticalPosition="center" />
				</HeaderStyle>
            </dx:TreeListDataColumn>
			<dx:TreeListDataColumn Caption="&nbsp;" Width="100" AllowSort="False" HeaderStyle-Cursor="default">
                <DataCellTemplate>    
					<asp:HyperLink ID="AddNew" NavigateUrl="javascript:void(0);"  onclick='<%# "showAddSelector(\"" + Container.NodeKey + "\");" %>' runat="server" Visible="<%#setShowNew(Container.NodeKey) %>" Text='<%#setTextNew(Container.NodeKey)%>'></asp:HyperLink>                                                 
                </DataCellTemplate>
            	<HeaderStyle>
				<BackgroundImage ImageUrl="~/App_Themes/DBIntranet2010/images/Add_hlWithSpace.gif" 
					Repeat="NoRepeat" VerticalPosition="center" />
				</HeaderStyle>
            </dx:TreeListDataColumn>
        </Columns>
    <Settings ShowFooter="true" ShowGroupFooter="false" GridLines="Both" />
    <SettingsBehavior AutoExpandAllNodes="false" ProcessSelectionChangedOnServer="true"
            ExpandCollapseAction="NodeDblClick" />        
</dx:ASPxTreeList>

<!--popups-->
 <dx:ASPxPopupControl ID="ASPxPopControlEditBuilding" ClientInstanceName="popupEditBuilding" runat="server"
	SkinID="PopupCustomSize"
	meta:resourcekey="BuildingAdmin"
	ShowHeader="true"
	Width="550px"
	Height="450px"
	ContentUrl="~/Pages/Popup/BuildingAdmin.aspx"
	ContentUrlIFrameTitle="BuildingPopup">
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="ASPxPopControlEditFloor" ClientInstanceName="popupEditFloor" runat="server"
	SkinID="PopupCustomSize"
	meta:resourcekey="FloorAdmin"
	ShowHeader="true"
	Width="450px"
	Height="240px"
	ContentUrl="~/Pages/Popup/FloorAdmin.aspx"
	ContentUrlIFrameTitle="FloorPopup">
</dx:ASPxPopupControl>

<!--hidden fields-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldSelectorAdmin" ClientInstanceName="hdSelectorAdmin" runat="server">
</dx:ASPxHiddenField>
