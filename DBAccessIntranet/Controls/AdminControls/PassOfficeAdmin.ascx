﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PassOfficeAdmin.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls.PassOfficeAdmin" %>

<%@ Register Src="~/Controls/Selectors/PassOfficeSelector.ascx" TagName="PassOfficeSelector" TagPrefix="uc1" %>

<script type="text/javascript">
    $(document).ready(function () {
        //               $('#passImg').click(function (e) {
        //                   showAddPass();
        //               });
        $("#passHelp").hide();
        $('#HelpPass').click(function (e) {
            $("#passHelp").show();
        });
        hdPOAdmin.Set("poURL", '<%= Page.ResolveUrl("~/Pages/Popup/PassOfficeAdmin.aspx") %>');

    });
    function showAddPass() {
        var popupUrlRegion = hdPOAdmin.Get("poURL");
        popupUrlRegion = popupUrlRegion + "?type=add";
        popupAddPO.SetContentUrl(popupUrlRegion);
        popupAddPO.Show();
    }
    function showEditPass(RequestID) {
        var popupUrlRegion = hdPOAdmin.Get("poURL");
        popupUrlRegion = popupUrlRegion + "?type=edit&pass=" + RequestID;
        popupAddPO.SetContentUrl(popupUrlRegion);
        popupAddPO.Show();
    }
    function CancelPassOffice() {
        popupAddPO.Hide();
    }
    function RefreshPOGrid() {
        popupAddPO.Hide();
        gvPassOffice.PerformCallback();
    }

    function ErrorPOShowPopup() {
        popupAddPO.Hide();
        popupAlertTemplateContent.SetText('<%= GetResourceManager("Resources.CommonResource", "ErrorTryAgain") %>');
        popupAlertTemplate.SetHeaderText('<%= GetResourceManager("Resources.CommonResource", "Error") %>');
        popupAlertTemplate.Show();
    }

    function showPopupValidationError(validationFailedControls) {
        var errorMessageText = "";

        if ($.inArray("Name", validationFailedControls) != -1) {
            errorMessageText += "<%=PassOfficePopupEnterName.Text%>" + "</br>";
        }
        if ($.inArray("Country", validationFailedControls) != -1) {
            errorMessageText += "<%=PassOfficePopupSelectOffice.Text%> " + "</br>";
        }

        popupAlertTemplateContent.SetText(errorMessageText);
        popupAlertTemplate.SetHeaderText("<%=PassOfficeEnterArea.Text %>");
        popupAlertTemplate.Show();
    }
</script>

<dx:ASPxGridView ID="ASPxGridcurrentPassOffice" ClientInstanceName="gvPassOffice"
    runat="server" AutoGenerateColumns="False" OnCustomCallback="gvPassOffice_CustomCallback" OnHtmlRowCreated="gvPassOffice_HtmlRowCreated"
    KeyFieldName="PassOfficeID"
    ClientIDMode="AutoID" Width="70%">
    <Settings ShowStatusBar="Visible" />
    <Columns>
        <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, NameTitle%>" FieldName="Name" ShowInCustomizationForm="True" Visible="true">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, AddressTitle%>" FieldName="Address" ShowInCustomizationForm="True" Visible="true">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, EmailTitle%>" FieldName="Email" ShowInCustomizationForm="True" Visible="true" PropertiesTextEdit-EncodeHtml="false">
            <PropertiesTextEdit EncodeHtml="False"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, TelephoneLiteral%>" FieldName="Telephone" ShowInCustomizationForm="True" Visible="true">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, CountryNameTitle%>" FieldName="LocationCountry.Name" ShowInCustomizationForm="True" Visible="true">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:SiteMapLocalizations, CityNameTitle%>" FieldName="LocationCity.Name" ShowInCustomizationForm="True" Visible="true">
        </dx:GridViewDataTextColumn>       
        <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, EmailEnabledLabel%>" FieldName="EmailEnabled" ShowInCustomizationForm="True" Visible="true">
            <DataItemTemplate>
                <asp:Label ID="Label1" runat="server" Text="<%# setEnabled(Container.Text) %>"></asp:Label>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="<%$Resources:CommonResource, Enabled%>" FieldName="Enabled" ShowInCustomizationForm="True" Visible="true">
            <DataItemTemplate>
                <asp:Label ID="Label1" runat="server" Text="<%# setEnabled(Container.Text) %>"></asp:Label>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="&nbsp;">
            <DataItemTemplate>
                <asp:HyperLink ID="Edit" NavigateUrl="javascript:void(0);" onclick='<%# "showEditPass(\"" + Container.KeyValue + "\");" %>' runat="server" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>"></asp:HyperLink>
            </DataItemTemplate>
            <HeaderStyle>
                <BackgroundImage ImageUrl="~/App_Themes/DBIntranet2010/images/editWithSpace.gif"
                    Repeat="NoRepeat" VerticalPosition="center" />
            </HeaderStyle>
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <StatusBar>
            <div style="text-align: right;">
                <dx:ASPxButton ID="btnAddPassOffice" AutoPostBack="false" runat="server" Text="<%$ Resources:CommonResource, AddNew%>">
                    <ClientSideEvents Click='function(s,e){ showAddPass(); }' />
                </dx:ASPxButton>
            </div>
        </StatusBar>
    </Templates>
</dx:ASPxGridView>

<!--popups-->
<dx:ASPxPopupControl ID="ASPxPopupAddPassOffice" ClientInstanceName="popupAddPO" runat="server"
    SkinID="PopupCustomSize"
    HeaderText="<%$ Resources:CommonResource, PassOfficeAdmin%>"
    ShowHeader="true"
    Width="500px"
    Height="450px"
    ContentUrl="~/Pages/Popup/PassOfficeAdmin.aspx"
    ContentUrlIFrameTitle="PassOfficeAdmin">
</dx:ASPxPopupControl>

<asp:Literal ID="PassOfficePopupEnterName" meta:resourcekey="PassOfficePopupEnterName" Visible="False" runat="server" ClientIDMode="Static"></asp:Literal>
<asp:Literal ID="PassOfficePopupSelectOffice" meta:resourcekey="PassOfficePopupSelectOffice" Visible="False" runat="server" ClientIDMode="Static"></asp:Literal>
<asp:Literal ID="PassOfficeEnterArea" meta:resourcekey="PassOfficeEnterArea" Visible="False" runat="server" ClientIDMode="Static"></asp:Literal>

<!--hiddenfield-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldPOAdmin" ClientInstanceName="hdPOAdmin" runat="server">
</dx:ASPxHiddenField>
