﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DivisionSelectorAdmin.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls.DivisionSelectorAdmin" %>

<%@ Register Src="~/Controls/Popups.ascx" TagName="Popups" TagPrefix="ppc" %>

<script type="text/javascript">
    $(document).ready(function () {
        hdDivisionAdmin.Set("divisionURL", '<%= Page.ResolveUrl("~/Pages/Popup/DivisionAdminPopup.aspx") %>');
    });

    var popupURLCountryID = "";

    function CloseButtonClick() {
        var requestMasterIDDA = hfRequestDetail.Get("RequestMasterID_DA");
        var requestMasterIDDO = hfRequestDetail.Get("RequestMasterID_DO");

        alert("requestMasterIDDA=" + requestMasterIDDA + "; requestMasterIDDO=" + requestMasterIDDO);

        PageMethods.RemoveDraftRequests(null, null);
    }

    function showAddPopup(DivisionID, UBRCode, UBRName, isSubdivision) {
        var popupUrlEditDivision = hdDivisionAdmin.Get("divisionURL");
       
        PageMethods.GetCountriesForUBR(UBRCode, null, OnshowAddPopup);

        //Replace any & symbols in the division name
        UBRName = UBRName.replace("&", "%26");

        popupUrlEditDivision = popupUrlEditDivision + "?type=add&divisionID=" + DivisionID + "&ubrCode=" + UBRCode + "&ubrName=" + UBRName
                                                    + "&isSubdivision=" + isSubdivision;
        popupURLCountryID = popupUrlEditDivision;

        var popupHeaderText = getDivisionAdminPopupHeaderText(isSubdivision);
        setDivisionAdminPopupHeader(popupHeaderText);
    }

    function getDivisionAdminPopupHeaderText(isSubdivision) {
        if (isSubdivision)
            return "Subdivision Admin";

        return "Division Admin";
    };

    function setDivisionAdminPopupHeader(popupHeaderText) {
        popupEditDivision.SetHeaderText(popupHeaderText);
    }

    function OnshowAddPopup(result) {
        popupURLCountryID += "&CountryIDS=" + result + "&Edit=" + "";

        popupEditDivision.Title
        popupEditDivision.SetContentUrl(popupURLCountryID);
        popupEditDivision.Show();
    }

    function showEditPopup(divisionID, countryName, UBRCode, subdivisionParentDivisionID, isSubdivision) {
        var popupUrlEditDivision = hdDivisionAdmin.Get("divisionURL");

        PageMethods.GetCountriesForUBR(UBRCode, countryName, OnshowEditPopup);
        PageMethods.RemoveDraftRequestsForThisDivision(divisionID);

        popupUrlEditDivision = popupUrlEditDivision + "?type=edit&divisionID=" + divisionID + "&ubrCode=" + UBRCode
                                                    + "&subdivisionParentDivisionID=" + subdivisionParentDivisionID + "&isSubdivision=" + isSubdivision;
        popupURLCountryID = popupUrlEditDivision;

        var popupHeaderText = getDivisionAdminPopupHeaderText(isSubdivision);
        setDivisionAdminPopupHeader(popupHeaderText);
    }

    function OnshowEditPopup(result){
        popupURLCountryID += "&CountryIDS=" + result + "&Edit=" + "Edit";
        popupEditDivision.SetContentUrl(popupURLCountryID);
        popupEditDivision.Show();
    }

    function CancelEditDivision() {
        popupEditDivision.Hide();
    }

    function RefreshDivisionGrid(divisionIdentifier) {
        popupEditDivision.Hide();

        var treenodeToRefresh = divisionIdentifier;

        tlDivisions.PerformCustomCallback(treenodeToRefresh);
    }

    function ErrorShowPopup(type) {
        popupEditDivision.Hide();
        popupAlertTemplateContent.SetText('<%=LiteralEditDivisionErrorText.Text %>');
        popupAlertTemplate.SetHeaderText('<%=LiteralEditDivisionErrorTextHead.Text %>');
        popupAlertTemplate.Show();
    }

    function GetUBRKeyValue(sKey) {
        if (sKey == '') {
            sKey = $('#textBoxUBRKey').val();
        }
        else {
            $('#textBoxUBRKey').val(sKey);
        }

        if (sKey == '') {
            $('#textBoxUBRKeyValue').val('');
        }
        else {
            var sValue = GetUBRTitle(sKey);

            if (sValue == '') {
                popupAlertTemplateContent.SetText('<%=DiviSelectorAdminUBRText.Text%>');
                popupAlertTemplate.SetHeaderText('<%=DiviSelectorAdminUBRTextHeader.Text %>');
                popupAlertTemplate.Show();

                $('#textBoxUBRKey').val('');
                $('#textBoxUBRKeyValue').val('');
            }
            else {
                $('#textBoxUBRKeyValue').val(sValue);

                tlDivisions.PerformCustomCallback();
            }
        }
    }

    function ClearUBRSelector() {
        $('#textBoxUBRKey').val('');
        $('#textBoxUBRKeyValue').val('');
    }

    function CountryFilterChanged() {
        var selectedCountryID = ASPxComboCountryFilter.GetValue()
        if (selectedCountryID != undefined)
            tlDivisions.PerformCustomCallback("CountryID:" + selectedCountryID);
    }

    var divisionIDToDelete = "";
    function DeleteDivision(nodeID) {
        divisionIDToDelete = nodeID;
        var isSubdivision = IsSubdivision(nodeID);

        if (isSubdivision) {
            popupConfirmTemplate.SetHeaderText('<%=DeleteSubdivisionPopupHeader.Text %>');
            popupConfirmTemplateContentHead.SetText('<%=DeleteSubdivisionPopupHeader.Text %>');
            popupConfirmTemplateContentBody.SetText('<%=DeleteSubdivisionConfirm.Text %>');
        } else {
            popupConfirmTemplate.SetHeaderText('<%=DiviSelectorRemDivision.Text %>');
            popupConfirmTemplateContentHead.SetText('<%=DiviSelectorDivision.Text %>');
            popupConfirmTemplateContentBody.SetText('<%=DiviSelectorDivisionConfirm.Text %>');
        }

        popupConfirmTemplateHiddenField.Set("function_Yes", "OnRemoveDivision();");
        popupConfirmTemplate.Show();
    }

    function IsSubdivision(nodeID) {
        return nodeID.indexOf("S") > -1;
    }

    function OnRemoveDivision() {
        PageMethods.DeleteDivision(divisionIDToDelete, OnDeleteDivision);
    }

    function OnDeleteDivision(result) {
        if (!result) {
            popupAlertTemplate.SetHeaderText('<%=DiviSelectorAdminDelete.Text %>');
            popupAlertTemplateContent.SetText('<%=DiviSelectorAdminDeleteError.Text%>');
            popupAlertTemplate.Show();
            tlDivisions.PerformCustomCallback(divisionIDToDelete);
            return;
        }
        tlDivisions.PerformCustomCallback(divisionIDToDelete);
    }

</script>

<br />

<asp:textbox runat="server" ID="textBoxUBRKey" ClientIDMode="Static"  SkinID="TextBox60" onblur="GetUBRKeyValue('')" onclick = "this.select();"/>
<asp:Image ID="ImageGetUBRKey" runat="server" SkinID="Search" 
    onclick="GetUBRKeyValue('')" ImageAlign="Bottom"/>
<asp:textbox runat="server" ID="textBoxUBRKeyValue"  ClientIDMode="Static" 
    SkinID="TextBox300" ReadOnly="True"></asp:textbox>
<br />
<br />
<br />
<asp:PlaceHolder ID="m_countryFilterHolder" runat="server" Visible="true">
    <table>
        <tr>
            <td>
                  <asp:Literal runat="server" ID="litCountryFilterPrompt" meta:resourcekey="litCountryFilterPromptResource1" ></asp:Literal>
            </td>
            <td>
                  <dx:ASPxComboBox ID="ASPxComboCountryFilter" ClientInstanceName ="ASPxComboCountryFilter" runat="server" TextField="Name" ValueField="CountryID" ClientSideEvents-SelectedIndexChanged="CountryFilterChanged" AutoPostBack="false"/>
            </td>
        </tr>
    </table>
    <br />
</asp:PlaceHolder>
<p>
    <b><asp:Literal ID="KeyLit"  meta:resourcekey="Key" runat="server"></asp:Literal></b> <asp:Literal ID="DivisionSelectorRows"  meta:resourcekey="DivisionSelectorRows" runat="server"></asp:Literal>
</p>
 <dx:ASPxTreeList ID="treeListDivisionAdmin" runat="server" Width="70%" AutoGenerateColumns="false"
       KeyFieldName="id" ParentFieldName="id" ClientInstanceName="tlDivisions" OnCustomCallback="tree_CustomCallback"
       OnVirtualModeCreateChildren="treeListDivisionAdmin_VirtualModeCreateChildren" OnVirtualModeNodeCreating="treeListDivisionAdmin_VirtualModeNodeCreating"
       OnHtmlRowPrepared="treeList_HtmlRowPrepared">
    <Settings GridLines="Both" />
    <Columns>
		<dx:TreeListTextColumn Caption="">
			<DataCellTemplate>
				<asp:Image runat="server" ID="removeImage" SkinID="Trash" AlternateText="Delete" Visible="<%#SetShowDelete(Container.NodeKey)%>" onclick='<%# "DeleteDivision(\"" + Container.NodeKey + "\" )" %>' />
            </DataCellTemplate>			
		</dx:TreeListTextColumn>            
        <dx:TreeListTextColumn Caption="UBR" FieldName="ubr" >
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, Name%>" FieldName="name" PropertiesTextEdit-EncodeHtml="false" >
        </dx:TreeListTextColumn>
        <dx:TreeListDataColumn Caption="<%$ Resources:CommonResource, Country%>" FieldName="country" >
        </dx:TreeListDataColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, Enabled%>" FieldName="enabled" >
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn meta:resourcekey="DivisionAdminRecertPeriod" FieldName="recertPeriod" > 
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn FieldName="containsDivision" Visible="false" >
        </dx:TreeListTextColumn>
        <dx:TreeListTextColumn Caption="<%$ Resources:CommonResource, BusinessArea%>" FieldName="isBusinessArea" Visible="false" >
        </dx:TreeListTextColumn>
        <dx:TreeListDataColumn Caption="&nbsp;">
            <DataCellTemplate>                                     
                <asp:HyperLink ID="Edit" NavigateUrl="javascript:void(0);" runat="server"  onclick='<%# "showEditPopup(\"" + Container.NodeKey + "\",\"" + Container.GetValue("country") + "\",\"" + Container.GetValue("ubr")  + "\",null,false);" %>' Visible="<%#SetShowEdit(Container.NodeKey) %>" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" />   
                <asp:HyperLink ID="EditSubdivision" NavigateUrl="javascript:void(0);" runat="server"  onclick='<%# "showEditPopup(\"" + Container.NodeKey + "\",\"" + Container.GetValue("country")  + "\",\"" + Container.GetValue("ubr") + "\",\"" + Container.GetValue("subdivisionParentDivisionID")  + "\",true);" %>' Visible="<%#SetShowEditSubdivision(Container.NodeKey) %>" Text="<%$ Resources:CommonResource, GridViewCommandColumnEdit%>" />            
            </DataCellTemplate>
        </dx:TreeListDataColumn>
        <dx:TreeListDataColumn Caption="&nbsp;">
            <DataCellTemplate> 
                <asp:HyperLink ID="AddNew" NavigateUrl="javascript:void(0);" onclick='<%# "showAddPopup(\"" + Container.NodeKey + "\",\"" + Container.GetValue("ubr") + "\",\"" + Container.GetValue("name") + "\",false);" %>' runat="server" Visible="<%#SetShowAddDivision(Container.NodeKey) %>"  meta:resourcekey="DivisionAdminAddDivision">
                </asp:HyperLink>
                <asp:HyperLink ID="AddNewSubdivision" NavigateUrl="javascript:void(0);" onclick='<%# "showAddPopup(\"" + Container.NodeKey + "\",\"" + Container.GetValue("ubr") + "\",\"" + Container.GetValue("name")+ "\",true);" %>' runat="server" Visible="<%#SetShowAddSubdivision(Container.NodeKey) %>"  meta:resourcekey="DivisionAdminAddSubDivision">
                </asp:HyperLink>                     
            </DataCellTemplate>
        </dx:TreeListDataColumn>
    </Columns>
    <Settings ShowFooter="true" ShowGroupFooter="false" GridLines="Both" />
    <SettingsBehavior AutoExpandAllNodes="false" ProcessSelectionChangedOnServer="true"
        ExpandCollapseAction="NodeDblClick" AllowFocusedNode="true" />        
    <Styles>
        <AlternatingNode Enabled="True" />                       
    </Styles>
</dx:ASPxTreeList>

<br />
<br />

<!-- edit division-->
<dx:ASPxPopupControl ID="ASPxPopupControlEditDivision" ClientInstanceName="popupEditDivision" runat="server"
	SkinID="PopupCustomSize"
	ShowHeader="true"
	meta:resourcekey="DivisionAdmin"
	Width="850px"
	Height="500px"
	ContentUrl="~/Pages/Popup/DivisionAdminPopup.aspx"
	ContentUrlIFrameTitle="DivisionPopup" 
    OnWindowCallback="ASPxPopupControlEditDivision_WindowCallback">
    <ClientSideEvents CloseButtonClick="function(s,e) {s.PerformCallback();}" />
</dx:ASPxPopupControl>
<!--hidden field-->
<dx:ASPxHiddenField ID="ASPxHiddenFieldDivisionAdmin" ClientInstanceName="hdDivisionAdmin" runat="server">
</dx:ASPxHiddenField>

<!--literal-->
<asp:Literal ID="DiviSelectorAdminUBRTextHeader"  meta:resourcekey="DiviSelectorAdminUBRTextHeader" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="DiviSelectorAdminUBRText"  meta:resourcekey="DiviSelectorAdminUBRText" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

<asp:Literal ID="LiteralEditDivisionErrorText"  meta:resourcekey="EditDivisionErrorText" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="LiteralEditDivisionErrorTextHead"  meta:resourcekey="EditDivisionErrorTextHead" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

<asp:Literal ID="DiviSelectorDivision"  meta:resourcekey="DiviSelectorDivision" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="DiviSelectorRemDivision"  meta:resourcekey="DiviSelectorRemDivision" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="DiviSelectorDivisionConfirm"  meta:resourcekey="DiviSelectorDivisionConfirm" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>

<asp:Literal ID="DeleteSubdivisionPopupHeader"  meta:resourcekey="DeleteSubdivisionPopupHeader" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="DeleteSubdivisionConfirm"  meta:resourcekey="DeleteSubdivisionConfirm" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>



<asp:Literal ID="DiviSelectorAdminDelete"  meta:resourcekey="DiviSelectorAdminDelete" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>
<asp:Literal ID="DiviSelectorAdminDeleteError"  meta:resourcekey="DiviSelectorAdminDeleteError" Visible="false" runat="server" ClientIDMode="Static"> </asp:Literal>