﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTreeList;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBAccess.BLL.Concrete;
using DevExpress.Web.ASPxPopupControl;
using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Helpers;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls
{
    public partial class DivisionSelectorAdmin : BaseControl
    {
        private bool? _IsSwitzerlandUser = new bool?();
        private int _SwitzerlandID = 0;

        public int SwitzerlandID
        {
            get { return _SwitzerlandID; }
        }

        public bool IsSwitzerlandUser
        {
            get
            {
                if (!_IsSwitzerlandUser.HasValue)
                {
                    mp_User theUser = Director.GetUserBydbPeopleId(CurrentUser.dbPeopleID);

                    if (theUser != null && theUser.RestrictedCountryId.HasValue)
                    {
                        _IsSwitzerlandUser = true;
                        _SwitzerlandID = theUser.RestrictedCountryId.Value;
                    }
                    else
                        _IsSwitzerlandUser = false;
                }
                return _IsSwitzerlandUser.Value;
            }
        }
    
        #region pageLifeCycle events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                treeListDivisionAdmin.ExpandToLevel(1);

                if (IsSwitzerlandUser)
                    FilterTreeByCountryID(SwitzerlandID);
               
                LoadASPxComboCountry();
            }
        }

        #endregion

        #region Page helper methods
        public bool SetShowEdit(object nodeID)
        {
            return IsThisDivisionNode(nodeID);
        }

        public bool SetShowEditSubdivision(object nodeID)
        {
            return IsThisSubdivisionNode(nodeID);
        }

		public bool SetShowDelete(object nodeID)
		{
            return IsThisDivisionNode(nodeID) || IsThisSubdivisionNode(nodeID);
		}

        public bool SetShowAddDivision(object nodeID)
        {
            //Need to be able to add a division even if their is a business area at that node
            return IsThisUBRNode(nodeID) || IsThisBusinessAreaNode(nodeID);
        }

        public bool SetShowAddSubdivision(object nodeID)
        {
            return IsThisDivisionNode(nodeID);
        }

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
        #endregion

        #region treeList events
        protected void tree_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            if (IsCountrySearch(e))
                GetCountryIDAndFilterTree(e);
            else if (IsUBRSearch(e))
                SearchTreeByUBR();
            else
                RefreshTreeAfterUpdate(e);
        }

        protected void treeListDivisionAdmin_VirtualModeCreateChildren(object sender, TreeListVirtualModeCreateChildrenEventArgs e)
        {
            DBAccessController.DAL.UBRDivision nodeObject = (DBAccessController.DAL.UBRDivision)e.NodeObject;

            if (nodeObject == null)
                e.Children = GetChildDivisions(null, true);
            else
                e.Children = GetChildDivisions(nodeObject.Id, false);
        }

        protected void treeListDivisionAdmin_VirtualModeNodeCreating(object sender, TreeListVirtualModeNodeCreatingEventArgs e)
        {
            DBAccessController.DAL.UBRDivision nodeObject = (DBAccessController.DAL.UBRDivision)e.NodeObject;
            e.NodeKeyValue = nodeObject.Id;
            e.IsLeaf = IsLeafNode(nodeObject);
            e.SetNodeValue("id", nodeObject.Id);
            e.SetNodeValue("ubr", nodeObject.UBRCode);
            e.SetNodeValue("name", nodeObject.Name);
            e.SetNodeValue("country", nodeObject.CountryName);
            e.SetNodeValue("countryID", nodeObject.CountryID);

            string enabled = String.Empty;
            if (nodeObject.Enabled.HasValue)
                enabled = IsDivisionEnabled(nodeObject);

            e.SetNodeValue("enabled", enabled);
            e.SetNodeValue("recertPeriod", Director.GetRecertPeriodText(nodeObject.RecertPeriod, CurrentUser.LanguageId));

            bool containsDivision = ContainsDivision(nodeObject);
            e.SetNodeValue("containsDivision", containsDivision);

            bool isBusinessArea = IsThisBusinessAreaNode(nodeObject.Id);

            if (isBusinessArea)
                e.SetNodeValue("isBusinessArea", "Yes");

            e.SetNodeValue("subdivisionParentDivisionID", nodeObject.SubdivisionParentDivisionID);
        }

        protected void treeList_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (IsDivisionRow(e))
                e.Row.Font.Bold = true;
            else if (IsSubdivisionRow(e))
            {
                e.Row.Font.Bold = true;
                e.Row.Font.Italic = true;
            }
        }
        #endregion

        #region Popup Events
        protected void ASPxPopupControlEditDivision_WindowCallback(object source, PopupWindowCallbackArgs e)
        {
            GenericRequestService genericService = new GenericRequestService();

            int? requestMasterIDDA = GenericGUIHelpers.GetIntFromSession("requestMasterIDDA");
            int? requestMasterIDDO = GenericGUIHelpers.GetIntFromSession("requestMasterIDDO");
            genericService.RemoveDraftDivisionRequests(requestMasterIDDA, requestMasterIDDO);
        }
        #endregion

        #region helper methods

        private void RefreshTreeAfterUpdate(TreeListCustomCallbackEventArgs e)
        {
            string divisionNodeID = GetParentDivisionIDForRefresh(e.Argument);
            treeListDivisionAdmin.RefreshVirtualTree();
            ExpandNode(divisionNodeID);
        }

        private string GetParentDivisionIDForRefresh(string nodeID)
        {
            if (IsThisSubdivisionNode(nodeID))
                return GetSubdivisionParentID(nodeID);

            return GetDivisionParentID(nodeID);
        }

        private string GetDivisionParentID(string nodeID)
        {
            string divisionUBR = GenericGUIHelpers.GetStringFromSession("ParentDivisionID");
            GenericGUIHelpers.ClearValueFromSession("ParentDivisionID");

            if (divisionUBR == null)
                return nodeID;

            return divisionUBR + "|U";
        }

        private string GetSubdivisionParentID(string nodeID)
        {
            int? subdivisionParentID = GenericGUIHelpers.GetIntFromSession("ParentDivisionID");
            GenericGUIHelpers.ClearValueFromSession("ParentDivisionID");

            if (subdivisionParentID == null)
                return null;

            return subdivisionParentID + "|D";
        }

        private int GetCountryIDAndFilterTree(TreeListCustomCallbackEventArgs e)
        {
            int countryID = GetCountryIDFromFilterID(e.Argument);

            if (DoWeNeedToFilterTree(countryID))
                FilterTreeByCountryID(countryID);

            return countryID;
        }

        private void SearchTreeByUBR()
        {
            treeListDivisionAdmin.CollapseAll();
            ExpandNodeByUBR(textBoxUBRKey.Text);
        }

        private void ExpandNode(string nodeKey)
        {
            TreeListNode node = FindNode(nodeKey, string.Empty);
            if(node != null)
                node.Expanded = true;
        }

        private bool IsThisDivisionNode(object nodeID)
        {
            return IsThisNodeType(nodeID, "D");
        }

        private bool IsThisBusinessAreaNode(object nodeID)
        {
            return IsThisNodeType(nodeID, "B");
        }
        
        public bool IsThisSubdivisionNode(object nodeID)
        {
            return IsThisNodeType(nodeID, "S");
        }

        private bool IsThisUBRNode(object nodeID)
        {
            return IsThisNodeType(nodeID, "U");
        }

        private bool IsThisNodeType(object nodeID, string nodeType)
        {
            string stringItemID = Convert.ToString(nodeID);
            return stringItemID.EndsWith(nodeType);
        }

        private static bool IsCountrySearch(TreeListCustomCallbackEventArgs e)
        {
            return e.Argument.Contains("CountryID");
        }

        private bool IsUBRSearch(TreeListCustomCallbackEventArgs e)
        {
            return String.IsNullOrEmpty(e.Argument) && !String.IsNullOrEmpty(textBoxUBRKey.Text);
        }

        private void FilterTreeByCountryID(int countryID)
        {
            Session["CountryID"] = countryID;
            treeListDivisionAdmin.RefreshVirtualTree();
        }

        private bool DoWeNeedToFilterTree(int countryFilterID)
        {
            int? existingCountryFilterID = GenericGUIHelpers.GetIntFromSession("CountryID");
            return !existingCountryFilterID.HasValue || countryFilterID != existingCountryFilterID.Value;
        }

        private int GetCountryIDFromFilterID(string countryFilterText)
        {
            string countryFilter = countryFilterText.Replace("CountryID:", String.Empty);
            return Convert.ToInt32(countryFilter);
        }

        private bool IsLeafNode(DBAccessController.DAL.UBRDivision nodeObject)
        {
            return nodeObject.ChildNodeCount.HasValue ? nodeObject.ChildNodeCount == 0 : false;
        }

        private string IsDivisionEnabled(DBAccessController.DAL.UBRDivision nodeObject)
        {
            return nodeObject.Enabled.Value ? GetResourceManager("Resources.CommonResource", "Yes") : GetResourceManager("Resources.CommonResource", "No");
        }

        private bool IsDivisionRow(TreeListHtmlRowEventArgs e)
        {
            return e.RowKind == TreeListRowKind.Data &&
                            (IsThisDivisionNode(e.NodeKey) || (bool)(e.GetValue("containsDivision") ?? false));
        }

        private bool IsSubdivisionRow(TreeListHtmlRowEventArgs e)
        {
            return e.RowKind == TreeListRowKind.Data &&
                            (IsThisSubdivisionNode(e.NodeKey));
        }

        private void LoadASPxComboCountry()
        {
            int? languageId = base.CurrentUser.LanguageId;
            List<DBAccessController.DAL.LocationCountry> countries = GetCountries();
                      
            for (int i = 0; i <= countries.Count - 1; i++)
            {
                ListEditItem lstCountry = GetCountryListItem(languageId, countries[i]);
                ASPxComboCountryFilter.Items.Add(lstCountry);
            }

            ASPxComboCountryFilter.SelectedIndex = 0;
            ASPxComboCountryFilter.Items[0].Text = GetResourceManager("Resources.CommonResource", "SelectItem");
        }

        private List<LocationCountry> GetCountries()
        {
            List<DBAccessController.DAL.LocationCountry> countries = new List<DBAccessController.DAL.LocationCountry>();
            countries = Director.GetCountriesAllEnabled(true);

            if (IsSwitzerlandUser)
                countries = countries.Where(e => e.CountryID == SwitzerlandID).ToList();
            
            return countries;
        }

        private ListEditItem GetCountryListItem(int? LanguageId, LocationCountry country)
        {
            ListEditItem lstCountry = new ListEditItem();

            lstCountry.Value = Convert.ToString(country.CountryID);
            lstCountry.Text = GetCountryNameFromLanguageID(LanguageId, country);
            return lstCountry;
        }

        private string GetCountryNameFromLanguageID(int? languageId, LocationCountry country)
        {
            switch ((DBAccessEnums.Language)languageId)
            {
                case DBAccessEnums.Language.English:
                    return Convert.ToString(country.Name);
                case DBAccessEnums.Language.German:
                    if (!string.IsNullOrEmpty(country.GermanName))
                        return Convert.ToString(country.GermanName);
                    break;
                case DBAccessEnums.Language.Italian:
                    if (!string.IsNullOrEmpty(country.ItalyName))
                        return Convert.ToString(country.ItalyName);
                    break;
            }

            return Convert.ToString(country.Name);
        }

        private List<DBAccessController.DAL.UBRDivision> GetChildDivisions(string parentNodeID, bool isRoot)
        {
            List<DBAccessController.DAL.UBRDivision> childDivisions = GetChildDivisionsFromDatabase(isRoot, parentNodeID);
           
            return childDivisions;
        }

        private List<DBAccessController.DAL.UBRDivision> GetChildDivisionsFromDatabase(bool isRoot, string parentNodeID)
        {
            int? existingCountryFilterID = GenericGUIHelpers.GetIntFromSession("CountryID");
            bool isDivision = IsThisDivisionNode(parentNodeID);
            string parentUBRCode = GetUBRFromNodeID(parentNodeID);

            if (isDivision)
                return GetSubdivisions(existingCountryFilterID, parentNodeID);
            else
                return Director.GetUBRDivisions(parentUBRCode, isRoot, existingCountryFilterID);
        }

        private List<DBAccessController.DAL.UBRDivision> GetSubdivisions(int? existingCountryFilterID, string parentNodeID)
        {
            int divisionID = GetDivisionIDFromNodeID(parentNodeID);
            return Director.GetSubdivisions(existingCountryFilterID, divisionID);
        }

        private int GetDivisionIDFromNodeID(string nodeID)
        {
            string divisionIDString = nodeID.Split('|')[0];
            return Convert.ToInt32(divisionIDString);
        }

        private string GetUBRFromNodeID(string nodeID)
        {
            if (nodeID == null)
                return null;

            return nodeID.Split('|')[0];
        }

        private void ExpandNodeByUBR(string ubrKey)
        {
            ArrayList ubrKeys = Director.GetUBRPath(ubrKey);

            if (ubrKeys == null || ubrKeys.Count == 0)
                return;

            TreeListNode currentNode = null;
            for (int i = ubrKeys.Count - 1; i >= 0; i--)
            {
                currentNode = FindNode(ubrKeys[i].ToString()); 
                if (currentNode != null)
                    currentNode.Expanded = true;
            }
            if (currentNode != null)
                currentNode.Focus();
        }

        /// <summary>
        /// Try to find the node in the tree for the given ubr code and checks
        /// for U node, D node and B node
        /// ie UBR node, Division node or Business Area node
        /// </summary>
        /// <param name="ubrKey">A ubr in the path from the root to the ubr the user is looking up</param>
        /// <returns></returns>
        private TreeListNode FindNode(string ubrKey)
        {
            TreeListNode node = FindUBRNode(ubrKey);
            if (node != null)
                return node;

            node = FindDivisionNode(ubrKey);
            if (node != null)
                return node;

            node = FindBusinessAreaNode(ubrKey);
            return node;
        }

        private TreeListNode FindUBRNode(string ubrKey)
        {
            return FindNode(ubrKey, "|U");
        }

        private TreeListNode FindDivisionNode(string ubrKey)
        {
            return FindNode(ubrKey, "|D");
        }

        private TreeListNode FindBusinessAreaNode(string ubrKey)
        {
            return FindNode(ubrKey, "|B");
        }

        private TreeListNode FindNode(string ubrKey, string suffix)
        {
            string keyValue = GetUBRKeyValue(ubrKey, suffix);
            return treeListDivisionAdmin.FindNodeByKeyValue(keyValue);
        }

        private string GetUBRKeyValue(string ubrKey, string suffix)
        {
            return ubrKey + suffix;
        }

        //This method populates ContainsDivision field which allows us to highlight rows which have children that contain a division
        private bool ContainsDivision(DBAccessController.DAL.UBRDivision node)
        {
            if (IsThisDivisionNode(node.Id))
                return true;

            string nodeID = node == null ? null : node.Id;

            List<DBAccessController.DAL.UBRDivision> children = GetChildDivisions(nodeID, false);

            foreach(DBAccessController.DAL.UBRDivision childNode in children)
            {
                if (IsThisDivisionNode(childNode.Id))
                    return true;
            }

            return false;
        }
        #endregion
    }
}
