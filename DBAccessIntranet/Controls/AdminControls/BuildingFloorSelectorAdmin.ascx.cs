﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using spaarks.DB.CSBC.DBAccess.DBAccessController;

//using Aduvo.DB.DBMembershipManager;
using Spaarks.CustomMembershipManager;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using System.Web.Services;
using System.Web.Script.Services;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls
{
    public partial class BuildingFloorSelectorAdmin : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set the data source of the treelist viewn
            BindData();           
        }

      
        #region BindData
        private void BindData()
        {
            List<DBAccessController.DAL.AdminFloors> data = Director.GetAdminFloors();

            //if (base.IsRestrictedUser)
            //{
            //    data = data.Where(e => (e.ItemID == "C" + RestrictedCountryId) || (e.ParentID == "C" + RestrictedCountryId)).ToList();
            //}

            locationFloors.DataSource = data;
            locationFloors.DataBind();
        }
        #endregion

        #region ASPxGridView methods
        /// <summary>
        /// method to determine if the add new link should show // is hidden for cities.
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public bool setShowNew(object itemID)
        {
            if (Convert.ToString(itemID).Contains("Ci") || Convert.ToString(itemID).Contains("B"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// method to determine what text should show on the add new link
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public string setTextNew(object itemID)
        {
            if (Convert.ToString(itemID).Contains("B"))
            {
               // return "Add Floor";
                return GetResourceManager("Resources.CommonResource", "AddFloor");
            }
            else if (Convert.ToString(itemID).Contains("Ci"))
            {
               // return "Add Building";
                return GetResourceManager("Resources.CommonResource", "AddBuilding");
            }
            else
            {
                return "";
            }
        }

        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
        /// <summary>
        /// Method to set the edit link text which only shows for both floors and buildings
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public bool setTextEdit(object itemID)
        {
            if (Convert.ToString(itemID).Contains("F") || Convert.ToString(itemID).Contains("B"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Method to refresh the tree list data
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tree_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            BindData();
        }
        #endregion

		#region Highlight tree row on mouse over
		protected void tree_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
		{
            if (base.IsRestrictedUser && e.Level == 1)
            {
                bool showStatus = (e.NodeKey) == ("C" + RestrictedCountryId);

                if (!showStatus)
                {
                    e.Row.Attributes.Add("style", "display: none;");
                }
            }

            
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}
		#endregion
    }
}