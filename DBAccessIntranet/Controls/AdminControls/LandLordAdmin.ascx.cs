﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using DevExpress.Web.ASPxGridView;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.AdminControls
{
    public partial class LandLordAdmin : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
		}

        protected override void OnInit(EventArgs e)
        {
            BindData();
        }

        /// <summary>
        /// method to bind the data from the landlord table to the data grid
        /// E.Parker
        /// 12.05.11
        /// </summary>
        void BindData()
        {
            List<DBAccessController.DAL.ControlLandlord> landlords = Director.GetLandlords();

            if (base.IsRestrictedUser)
            {
                //  need to go from countryid -> cityId -> buildingid -> controlLandlord
                List<DBAccessController.DAL.LocationCity> cities = Director.GetCitiesForCountry(RestrictedCountryId, false);

                List<int?> citiesList = new List<int?>();

                foreach (DBAccessController.DAL.LocationCity item in cities)
                {
                    citiesList.Add(item.CityID);
                }

                //  now buildings to get controllandlord id's
                List<DBAccessController.DAL.LocationBuilding> buildings = Director.GetBuildingsAllEnabled();

                List<int> landlordsList = new List<int>();

                foreach (DBAccessController.DAL.LocationBuilding buildingItem in buildings)
                {
                    landlordsList.Add(buildingItem.LandlordID.GetValueOrDefault(0));
                }

                landlords = landlords.Where(e => landlordsList.Contains(e.LandlordID)).ToList();
            }

            landlordGrid.DataSource = landlords;
            landlordGrid.DataBind();       
        }

        /// <summary>
        /// Method used to refresh the grid after an add or edit to the landlords
        /// E.Parker
        /// 12.05.11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvLandlord_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            BindData();

        }

		#region Highlight grid row on mouse over
		protected void gvLandlord_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#BCD2E6';");
			e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
		}
		#endregion
        public string GetResourceManager(string resourceName, string resourceIdentifier)
        {
            return Director.GetResourceManager(resourceName, resourceIdentifier);
        }
    }
}