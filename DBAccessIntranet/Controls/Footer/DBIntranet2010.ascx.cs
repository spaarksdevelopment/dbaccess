﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;




namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Footer
{
	public partial class DBIntranet2010 : System.Web.UI.UserControl
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty (ConfigurationManager.AppSettings["AdminContactLink"]))
			{
				AdminContactLink.NavigateUrl = ConfigurationManager.AppSettings["AdminContactLink"].ToString ();

				Boolean bDefault = (Request.Path == "/Default.aspx");
				Boolean bAuthenticated = HttpContext.Current.Request.IsAuthenticated;

				subFooterNavigation.Visible = false;// bAuthenticated && !bDefault;
			}
		}


		protected void theMenu_MenuItemDataBound(object sender, MenuEventArgs e)
		{
			string navigateUrl = e.Item.NavigateUrl.ToLower();
			if (navigateUrl.Contains("pages/error.aspx") || navigateUrl.ToLower().Contains("pages/login.aspx"))
			{
				Menu menu = (Menu)sender;

				MenuItem menuItem = ((MenuItem)e.Item);

				if (menuItem.Parent == null)
					menu.Items.Remove(menuItem);
				else
					menuItem.Parent.ChildItems.Remove(menuItem);

			}

			
		}
	}
}
