﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DBIntranet2010.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Footer.DBIntranet2010" %>

<!-- LAYOUT: FOOTER -->
<div id="layoutFooter">
	<!-- CONTENT: FOOTER -->
	<div id="footerArea">
		<div id="footerUpper">
			<br />
			<br />
			<div id="footerLower">
				<asp:Literal ID="FooterDetails" runat="server" meta:resourceKey="FooterDetails"></asp:Literal> 
				<asp:HyperLink runat="server" ID="AdminContactLink" meta:resourcekey="AdminContactLink">System Administrator</asp:HyperLink>.
			</div>

			<div id="footerLeftLg">
				<div id="footerNavigation">
					<ul class="horNav" id="list_footer1">
						<li class=" first "><a class="" title="Imprint" href="http://www.db.com/en/content/imprint.htm" target="_blank"><asp:Literal ID="Imprint" runat="server" meta:resourcekey="Imprint"></asp:Literal> </a></li>
						<li><a class="" title="Legal Resources" href="http://www.db.com/en/content/legal_resources.htm" target="_blank"><asp:Literal ID="LegalResources" runat="server" meta:resourcekey="LegalResources"></asp:Literal></a></li>
						<li class=" last "><a class="" title="Accessibility" href="http://www.db.com/en/content/company/accessibility.htm" target="_blank"><asp:Literal ID="Accessibility" runat="server" meta:resourcekey="Accessibility"></asp:Literal></a></li>
					</ul>
					<div>
						Copyright &copy; <%=DateTime.Now.Year%> Deutsche Bank AG, Frankfurt am Main
					</div>
				</div>
			</div>

			<div id="footerRight">
			</div>
		</div>
	</div>

	<asp:Panel ID="subFooterNavigation" runat="server" Visible="False" 
		style="margin-top: 126px;">

	
		<asp:Repeater runat="server" ID="menu" DataSourceID="SiteMapDataSource1">
		
            <ItemTemplate>
			<ul style="width:200px; float:left; margin-left:0px;">
                <li>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Title") %></asp:HyperLink>
					<li>
					<asp:Repeater ID="Repeater1" runat="server" DataSource='<%# ((SiteMapNode) Container.DataItem).ChildNodes %>'>
                
						<FooterTemplate>
							
						</FooterTemplate>
						<HeaderTemplate>
						</HeaderTemplate>
						<ItemTemplate>
							<li>
								<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Title") %></asp:HyperLink>
							</li>
						</ItemTemplate>
					</asp:Repeater>
					</li>

                </li>
			</ul>
            </ItemTemplate>
			
        </asp:Repeater>

		<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" 
			ShowStartingNode="False"/>
	</asp:Panel>
</div>
