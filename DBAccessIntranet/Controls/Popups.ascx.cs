﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPopupControl;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls
{
	public partial class Popups : System.Web.UI.UserControl
	{
        private string _HeaderText = string.Empty;

        public string HeaderText 
        { 
            set
            {
                this._HeaderText = value;
            }
        }

        private string _MessageText = string.Empty;

        public string MessageText 
        {
            set
            {
                this._MessageText = value;
            }
        }

        public ASPxPopupControl PopupControl
        {
            get { return this.PopupControlAlertTemplate; }
        }

		protected void Page_Load (object sender, EventArgs e)
		{
            if (!String.IsNullOrEmpty(_HeaderText))
            {
                PopupControlAlertTemplate.HeaderText = _HeaderText;
            }

            if (!String.IsNullOrEmpty(_MessageText))
            {
                LabelAlertTemplate.Text = _MessageText;
            }
		}
	}
}