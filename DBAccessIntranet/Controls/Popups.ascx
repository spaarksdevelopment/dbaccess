﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Popups.ascx.cs" Inherits="spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Controls.Popups" %>

<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<!--

	These Controls are here to replace the 'alert' and the 'confirm' JS functions.
	It is important to note that these are asynchronous methods and any JS code will continue whilst these are being shown.

 -->

<!--

	After including the custom control in your page, you can use the control as follows to replace the JS 'alert' call.

    popupAlertTemplate.SetHeaderText("Header Text");
    popupAlertTemplateContent.SetText("This is where we put the alert message.");
    popupAlertTemplate.Show();

 -->

<dx:ASPxPopupControl ID="PopupControlAlertTemplate" 
	ClientInstanceName="popupAlertTemplate" runat="server"
	Modal="True"
	SkinID="PopupCustomSize"
	Width="330px"
	Height="150px"
	HeaderText="Alert" meta:resourcekey="PopupControlAlertTemplateResource1">
	<ContentCollection>
		<dx:PopupControlContentControl ID="PopupControlContentControlAlertTemplate" 
			runat="server" SupportsDisabledAttribute="True">
			<p class="PopupAlert">
				<dx:ASPxLabel ID="LabelAlertTemplate" 
					ClientInstanceName="popupAlertTemplateContent" runat="server"></dx:ASPxLabel>
			</p>
			<p class="PopupAlert">
                &nbsp;</p>
			<div class="PopupAlert">
				<dx:ASPxButton ID="ButtonAlertTemplate_Okay" runat="server" 
					AutoPostBack="false" Text="" 
					meta:resourcekey="ButtonAlertTemplateOkay">
					<ClientSideEvents Click='function(s,e){ popupAlertTemplate.Hide(); PopupAlertTemplate(); }' />
<ClientSideEvents Click="function(s,e){ popupAlertTemplate.Hide(); PopupAlertTemplate(); }"></ClientSideEvents>
				</dx:ASPxButton>
			</div>
			<dx:ASPxHiddenField ID="ASPxHiddenFieldAlertClickedFunction" ClientInstanceName="popupAlertTemplateHiddenField" runat="server"></dx:ASPxHiddenField>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>

<!--

	After including the custom control in your page, you can use the control as follows to replace the JS 'confirm' call.

    popupConfirmTemplate.SetHeaderText("Header Text");
	popupConfirmTemplateContentHead.SetText("Heading Text");
    popupConfirmTemplateContentBody.SetText("This is where we put the confirm question.");
	popupConfirmTemplateHiddenField.Set("function_Yes", "DoNextStep();");
    popupConfirmTemplate.Show();

	If "Yes" is clicked then, if specified, the code will attempt to call the "function" as defined in the hidden field set.
	The code will call the function as it appears, so it is acceptable to pass parameters, if that makes sense.

	Incidentally, the hidden field control can also be used to pass other information on to this function.

	function DoNextStep()
	{
		// ... do something
	}
 -->

<script type="text/javascript" language="javascript">

	//
	// takes the passed in function name and calls it if it exists
	//
	function PopupConfirmTemplate()
	{
		var sFunction = popupConfirmTemplateHiddenField.Get("function_Yes");

		if ("undefined" != typeof (sFunction))
		{
			var iIndex = sFunction.indexOf('(');

			// we only want to call the function if it exists
			// so check that the name of the function is of type "function" first

			if (-1 != iIndex)
			{
				var sFunctionOnly = sFunction.substring(0, iIndex);

				if (eval("typeof " + sFunctionOnly + " == \"function\"")) eval(sFunction);
			}
		}
	}

	//Similair to PopupConfirmTemplate but executes the function on click of "OK" button rather than yes
	function PopupAlertTemplate()
	{
		var sFunction = popupAlertTemplateHiddenField.Get("function_OK");

		if ("undefined" != typeof (sFunction))
		{
			var iIndex = sFunction.indexOf('(');

			// we only want to call the function if it exists
			// so check that the name of the function is of type "function" first

			if (-1 != iIndex)
			{
				var sFunctionOnly = sFunction.substring(0, iIndex);

				if (eval("typeof " + sFunctionOnly + " == \"function\"")) eval(sFunction);
			}
		}
		else
		{
			popupAlertTemplate.Hide();
			popupLoading.Hide();
		}
	}
</script>

<dx:ASPxPopupControl ID="PopupControlConfirmTemplate" 
	ClientInstanceName="popupConfirmTemplate" runat="server"
	SkinID="PopupCustomSize"
	Width="400px"
	Height="200px"
	HeaderText="Confirm" meta:resourcekey="PopupControlConfirmTemplateResource1">
	<ContentCollection>
		<dx:PopupControlContentControl ID="PopupControlContentControlConfirmTemplate" 
			runat="server" SupportsDisabledAttribute="True">
			<h2 class="PopupConfirm">
				<dx:ASPxLabel ID="LabelConfirmTemplate_ContentHead" 
					ClientInstanceName="popupConfirmTemplateContentHead" runat="server"></dx:ASPxLabel>
			</h2>
			<p class="PopupConfirm">
				<dx:ASPxLabel ID="LabelConfirmTemplate_ContentBody" 
					ClientInstanceName="popupConfirmTemplateContentBody" runat="server"></dx:ASPxLabel>
			</p>
			<div class="PopupConfirm">
				<div>
					<dx:ASPxButton ID="ButtonConfirmTemplate_Accept" runat="server" 
						AutoPostBack="false" Text="" 
						meta:resourcekey="ButtonConfirmTemplate_Accept" style="float: left;">
						<ClientSideEvents Click='function(s,e){ popupConfirmTemplate.Hide(); PopupConfirmTemplate(); }' />
                        <ClientSideEvents Click="function(s,e){ popupConfirmTemplate.Hide(); PopupConfirmTemplate(); }"></ClientSideEvents>
					</dx:ASPxButton>
					<dx:ASPxButton ID="ButtonConfirmTemplate_Cancel" runat="server" 
						AutoPostBack="false" Text="" 
						meta:resourcekey="ButtonConfirmTemplate_Cancel" style="float: left; margin-left: 20px;">
						<ClientSideEvents Click='function(s,e){ popupConfirmTemplate.Hide(); }' />
                        <ClientSideEvents Click="function(s,e){ popupConfirmTemplate.Hide(); }"></ClientSideEvents>
					</dx:ASPxButton>
                    <p style="clear:both"></p>
				</div>
			</div>
			<dx:ASPxHiddenField ID="ASPxHiddenFieldConfirmTemplate" ClientInstanceName="popupConfirmTemplateHiddenField" runat="server"></dx:ASPxHiddenField>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="PopupLoading" ClientInstanceName="popupLoading" runat="server"
	SkinID="PopupCustomSize" HeaderText="" Height="40px" Width="110px" 
	ShowHeader="False" meta:resourcekey="PopupLoadingResource1">
	<ContentCollection>
		<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" 
			SupportsDisabledAttribute="True">
			<div style="position: relative; margin-left: 0px; margin-top: 2px;">
				<img id="Img1" runat="server" src="../App_Themes/DBIntranet2010/images/devExSpinner.gif" alt="In progress" width="16" height="16"/>
				<div style=" position: absolute; top: 50%; height: 18px; margin-top: -9px; left: 25px; font-family: Verdana; color: #3c3c3c; font-size: 12px;">
					<%--Loading...--%>
                    <asp:Literal runat="server" Text="<%$ Resources:CommonResource, Loading %>" Visible="true"></asp:Literal>
				</div>
			</div>
		</dx:PopupControlContentControl>
	</ContentCollection>
</dx:ASPxPopupControl>