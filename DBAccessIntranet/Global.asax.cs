﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Spaarks.Common.UtilityManager.Mail;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet
{
	public class Global : System.Web.HttpApplication
	{
	    private static IUnityContainer _MainContainer;

		protected void Application_Start(object sender, EventArgs e) 		
		{
			SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(OnSiteMapResolve);

			_MainContainer = new UnityContainer();
			UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

			_MainContainer = section.Configure(_MainContainer);
			MailHelper.Mailer = _MainContainer.Resolve<IMailer>();
		}
		
		protected void Session_Start(object sender, EventArgs e) 
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e) 
		{

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) 
		{

		}

		protected void Application_Error(object sender, EventArgs e) 
		{
			//// Code that runs when an unhandled error occurs
			var lastException = Server.GetLastError().GetBaseException();

			if (lastException.Message == "Thread was being aborted. ")
			{
				//this error does not need to be logged
				Server.ClearError();
			}
			else
			{
				//log the error and re-throw it to get the error screen
				var application = (HttpApplication)sender;
				var userName = application.User.Identity.Name;
				var userRole = string.Empty;

				var logErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]);
				var emailErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["EmailErrors"]);

				if (logErrors | emailErrors)
				{
					LastErrorEventID = LogHelper.HandleException(lastException, ExceptionSeverity.Unhandled, userName, userRole);

					Server.ClearError();

					throw lastException;
				}
			}
		}
         
		protected void Session_End(object sender, EventArgs e) 
		{

		}

		protected void Application_End(object sender, EventArgs e) 
		{
			_MainContainer = null;
		}

		static SiteMapNode OnSiteMapResolve(object sender, SiteMapResolveEventArgs e)
		{
			//if we aren't working on a node then return
			if (SiteMap.CurrentNode == null)
			{
				return null;
			}

			//clone the current node and append the query string
			var resolvedNode = SiteMap.CurrentNode.Clone(false);
			resolvedNode.Url += e.Context.Request.Url.Query;

			if (resolvedNode.ParentNode != null)
			{
				//get the previous node and if it has the same key (basic url) as the current 
				//node's parent then assign it as the parent  
				SiteMapNode previousNode = null;

				if (e.Context.Session != null)
				{
					previousNode = e.Context.Session["SiteMapPreviousNode"] as SiteMapNode;
				}

				if (previousNode != null && string.Compare(resolvedNode.ParentNode.Key, previousNode.Key, true) == 0)
				{
					resolvedNode.ParentNode = previousNode;
				}
			}

			if (e.Context.Session != null)
			{
				//store the resolved node in the session state and return it
				e.Context.Session["SiteMapPreviousNode"] = resolvedNode;
			}

			return resolvedNode;
		}

		#region LastErrorEventID

		public static int LastErrorEventID
		{
			get
			{
				return (HttpContext.Current.Session["LastErrorEventId"] == null) ? 0 : (int)HttpContext.Current.Session["LastErrorEventId"];
			}
			set
			{
                if(HttpContext.Current.Session != null) HttpContext.Current.Session.Add("LastErrorEventId", value);
			}
		}

		#endregion

		#region AdminLoginOverride

		public static bool AdminLoginOverride
		{
			get
			{
				return (HttpContext.Current.Session["AdminLoginOverride"] == null) ? false : (bool)HttpContext.Current.Session["AdminLoginOverride"];
			}
			set
			{
				HttpContext.Current.Session.Add("AdminLoginOverride", value);
			}
		}

		#endregion
	}
}