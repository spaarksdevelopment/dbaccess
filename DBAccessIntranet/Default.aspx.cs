﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using spaarks.DB.CSBC.DBAccess.DBAccessIntranet.Pages;
using System.Web.Security;
using DBAccessIntranet.MasterPages;

namespace spaarks.DB.CSBC.DBAccess.DBAccessIntranet
{
	public partial class DefaultPage : BaseSecurityPage
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			DBIntranetMasterPage masterPage = (DBIntranetMasterPage)this.Master;

			if (masterPage != null)
			{
				masterPage.ShowFlags = true;
			}    
		}

		protected void Page_Load (object sender, EventArgs e)
		{
			//bool showHtmlEditor = false;

			if (CurrentUser == null)
			{
				heading.Style.Add(System.Web.UI.HtmlTextWriterStyle.Top, "-60px;");
			}
			else
			{
				heading.Style.Add(System.Web.UI.HtmlTextWriterStyle.Top, "-95px;");
			}

			bool showHtmlEditor = false;

			if (CurrentUser != null && this.CurrentUser.RolesShortNamesList.Contains("ADMIN"))
			{
				showHtmlEditor = true;
			}

			NewsFeed1.AllowEdit = showHtmlEditor;
			
			LoginLink.NavigateUrl = FormsAuthentication.LoginUrl;
		}
	}
}
