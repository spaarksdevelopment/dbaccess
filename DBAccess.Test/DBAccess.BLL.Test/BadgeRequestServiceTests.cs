﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using DBAccess.Test.DBAccess.BLL.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.BLL.Test
{
    [TestClass]
    public class BadgeRequestServiceTests
    {
        private TestableBadgeRequestService _BadgeRequestService;
        private Mock<IBadgeRequestManager> _MockBadgeRequestManager;
        private GenericRequestManager _GenericRequestManager;
        private PersonManager _PersonManager;
        private EmailService _EmailService;
        private EmailManager _EmailManager;
        private const int CurrentUserID = 12345;

        #region Data
        private readonly List<GetBadgeJustificationReasons_Result> _EnglishBadgeJustificationReasons =
                       new List<GetBadgeJustificationReasons_Result>
            {
                new GetBadgeJustificationReasons_Result()
                {
                    ID=1,
                    Name="Lost"
                },
                new GetBadgeJustificationReasons_Result()
                {
                    ID=2,
                    Name="Stolen"
                },
                new GetBadgeJustificationReasons_Result()
                {     
                    ID=3,
                    Name="Damaged"
                },
                new GetBadgeJustificationReasons_Result()
                {
                     ID=4,
                     Name="Forgotten"
                }
            };

        private readonly List<GetBadgeJustificationReasons_Result> _GermanBadgeJustificationReasons =
               new List<GetBadgeJustificationReasons_Result>
            {
                new GetBadgeJustificationReasons_Result()
                {
                    ID=1,
                    Name="verloren"
                },
                new GetBadgeJustificationReasons_Result()
                {
                    ID=2,
                    Name="gestohlen"
                },
                new GetBadgeJustificationReasons_Result()
                {     
                    ID=3,
                    Name="beschädigt"
                },
                new GetBadgeJustificationReasons_Result()
                {
                     ID=4,
                     Name="vergessen"
                }
            };

        private readonly List<GetBadgeJustificationReasons_Result> _ItalianBadgeJustificationReasons =
               new List<GetBadgeJustificationReasons_Result>
            {
                new GetBadgeJustificationReasons_Result()
                {
                    ID=1,
                    Name="perduto"
                },
                new GetBadgeJustificationReasons_Result()
                {
                    ID=2,
                    Name="rubato"
                },
                new GetBadgeJustificationReasons_Result()
                {     
                    ID=3,
                    Name="danneggiato"
                },
                new GetBadgeJustificationReasons_Result()
                {
                     ID=4,
                     Name="dimenticato"
                }
            };

        private readonly List<vw_BadgeRequest> _BadgeRequests =
            new List<vw_BadgeRequest>
            {
                new vw_BadgeRequest()
                {
                    RequestID = 1
                }
            };

        private readonly List<vw_BadgeRequest> _BadgeRequestsEmpty = new List<vw_BadgeRequest>();

        private readonly List<vw_BadgeRequest> _BadgeRequestsHavingSamePassOffice =
            new List<vw_BadgeRequest>
            {
                new vw_BadgeRequest()
                {
                    RequestID = 1,
                    CountryDefaultPassOffice = 1
                },
                new vw_BadgeRequest()
                {
                    RequestID = 2,
                    CountryDefaultPassOffice = 1
                },
                new vw_BadgeRequest()
                {
                    RequestID = 3,
                    CountryDefaultPassOffice = 1
                }
            };

        private readonly List<vw_BadgeRequest> _BadgeRequestsHavingDifferentPassOffices =
           new List<vw_BadgeRequest>
            {
                new vw_BadgeRequest()
                {
                    RequestID = 1,
                    CountryDefaultPassOffice = 1
                },
                new vw_BadgeRequest()
                {
                    RequestID = 2,
                    CountryDefaultPassOffice = 1
                },
                 new vw_BadgeRequest()
                {
                    RequestID = 3,
                    CountryDefaultPassOffice = 3
                }
            };

        #endregion

        [TestInitialize]
        public void Setup()
        {
            Mock<IDBAccessContainerFactory> mockIDBAccessContainerFactory = new Mock<IDBAccessContainerFactory>();

            _MockBadgeRequestManager = new Mock<IBadgeRequestManager>();

            SetupBadgeJustificationReasonsData();
            SetupUpdateBadgeJustification();
            SetupGetBadgeRequests();

            SetupManagers(mockIDBAccessContainerFactory);

            _EmailService = new EmailService(_EmailManager, _GenericRequestManager, _PersonManager);

            _BadgeRequestService = new TestableBadgeRequestService(CurrentUserID, _MockBadgeRequestManager.Object, _PersonManager, _GenericRequestManager,
                _EmailService);
        }

        private void SetupManagers(Mock<IDBAccessContainerFactory> mockIDBAccessContainerFactory)
        {
            _GenericRequestManager = new GenericRequestManager(mockIDBAccessContainerFactory.Object);
            _PersonManager = new PersonManager(mockIDBAccessContainerFactory.Object);
            _EmailManager = new EmailManager(mockIDBAccessContainerFactory.Object);
        }

        private void SetupUpdateBadgeJustification()
        {
            _MockBadgeRequestManager.Setup(a => a.UpdateBadgeJustification(0, null)).Throws(new ApplicationException("Failed to update the justificaiton"));
            _MockBadgeRequestManager.Setup(a => a.UpdateBadgeJustification(1, null)).Verifiable();
            _MockBadgeRequestManager.Setup(a => a.UpdateBadgeJustification(1, 1)).Verifiable();
        }

        private void SetupGetBadgeRequests()
        {
            _MockBadgeRequestManager.Setup(a => a.GetBadgeRequests(0)).Returns(new List<vw_BadgeRequest>());
            _MockBadgeRequestManager.Setup(a => a.GetBadgeRequests(1)).Returns(_BadgeRequests);
        }

        private void SetupBadgeJustificationReasonsData()
        {
            _MockBadgeRequestManager.Setup(a => a.GetBadgeJustificationReasons(null))
                .Returns(_EnglishBadgeJustificationReasons);
            _MockBadgeRequestManager.Setup(a => a.GetBadgeJustificationReasons(1))
                .Returns(_EnglishBadgeJustificationReasons);
            _MockBadgeRequestManager.Setup(a => a.GetBadgeJustificationReasons(2))
                .Returns(_GermanBadgeJustificationReasons);
            _MockBadgeRequestManager.Setup(a => a.GetBadgeJustificationReasons(3))
                .Returns(_ItalianBadgeJustificationReasons);
        }

        [TestMethod]
        public void When_LanguageIDIsNull_Expect_GetBadgeJustificationReasonsReturnsEnglishReasons()
        {
            var result = _BadgeRequestService.GetBadgeJustificationReasons(null);
            var expected = _EnglishBadgeJustificationReasons;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_LanguageIDIs1_Expect_GetBadgeJustificationReasonsReturnsEnglishReasons()
        {
            var result = _BadgeRequestService.GetBadgeJustificationReasons(1);
            var expected = _EnglishBadgeJustificationReasons;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_LanguageIDIs2_Expect_GetBadgeJustificationReasonsReturnsGermanReasons()
        {
            var result = _BadgeRequestService.GetBadgeJustificationReasons(2);
            var expected = _GermanBadgeJustificationReasons;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_LanguageIDIs3_Expect_GetBadgeJustificationReasonsReturnsGermanReasons()
        {
            var result = _BadgeRequestService.GetBadgeJustificationReasons(3);
            var expected = _ItalianBadgeJustificationReasons;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_RequestIDIs0_Expect_UpdateBadgeJustificationReturnsFalse()
        {
            var result = _BadgeRequestService.UpdateBadgeJustification(0, null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_RequestIDIsValidAndJustificationIDIsNull_Expect_UpdateBadgeJustificationReturnsTrue()
        {
            var result = _BadgeRequestService.UpdateBadgeJustification(1, null);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_RequestIDIsValidAndJustificationIDIsValid_Expect_UpdateBadgeJustificationReturnsTrue()
        {
            var result = _BadgeRequestService.UpdateBadgeJustification(1, 1);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_RequestMasterIDIs0_Expect_GetBadgeRequestsReturnsEmptyList()
        {
            List<vw_BadgeRequest> result = _BadgeRequestService.GetBadgeRequests(0);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void When_RequestMasterIDIsValid_Expect_GetBadgeRequestsReturnsListOfRequests()
        {
            List<vw_BadgeRequest> result = _BadgeRequestService.GetBadgeRequests(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public void When_CanBadgeRequestsBeCreatedReturnsFalse_Expect_CreateNewBadgeRequestForPersonReturnsFalse()
        {
            _BadgeRequestService.CanBadgeRequestsBeCreatedProperty = false;

            var requestMasterID = 0;
            bool result = _BadgeRequestService.CreateNewBadgeRequestForPerson(0, null, ref requestMasterID);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_BadgeRequestListIsNull_GetDefaultPassOffice_Returns_0()
        {
            int result = _BadgeRequestService.GetDefaultPassOffice(null);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_BadgeRequestListIsEmpty_GetDefaultPassOffice_Returns_0()
        {
            int result = _BadgeRequestService.GetDefaultPassOffice(_BadgeRequestsEmpty);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_DefaultPassOfficeNotSpecified_GetDefaultPassOffice_Returns_0()
        {
            int result = _BadgeRequestService.GetDefaultPassOffice(_BadgeRequests);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_BadgeRequestListHasSameDefaultPassOffice_GetDefaultPassOffice_Returns_1()
        {
            int result = _BadgeRequestService.GetDefaultPassOffice(_BadgeRequestsHavingSamePassOffice);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void When_BadgeRequestListHasDifferentDefaultPassOffices_GetDefaultPassOffice_Returns_0()
        {
            int result = _BadgeRequestService.GetDefaultPassOffice(_BadgeRequestsHavingDifferentPassOffices);
            Assert.AreEqual(0, result);
        }
    }
}
