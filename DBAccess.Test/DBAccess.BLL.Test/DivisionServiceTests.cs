﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.BLL.Test
{
    [TestClass]
    public class DivisionServiceTests
    {
        private Mock<IDivisionManager> _MockDivisionManager;
        private Mock<IPersonManager> _MockPersonManager;

        private PersonManager _PersonManager;
        private GenericRequestManager _GenericRequestManager;
        private DivisionService _DivisionService;
        private GenericRequestService _GenericRequestService;

        #region Data
        private const int CurrentUserID = 12345;

        private List<DivisionRoleUser> _DivisionRoleUsers = new List<DivisionRoleUser>
            {
                new DivisionRoleUser()
                {
                    DivisionRoleUserID = 1,
                    DivisionID = 1,
                    SecurityGroupID = (int) RoleType.DivisionAdministrator
                },
                new DivisionRoleUser()
                {
                    DivisionRoleUserID = 2,
                    DivisionID = 1,
                    SecurityGroupID = (int) RoleType.DivisionOwner
                }
            };

        private HRPerson _DisabledHRPerson = new HRPerson{ dbPeopleID = 1, Enabled = false, Revoked = false };
        private HRPerson _RevokedHRPerson = new HRPerson { dbPeopleID = 2, Enabled = true, Revoked = true };
        private HRPerson _PersonWithNoEmail = new HRPerson { dbPeopleID = 3, Enabled = true, EmailAddress = string.Empty };
        private HRPerson _ValidPerson = new HRPerson { dbPeopleID = 1, Enabled = true, EmailAddress = "test@db.com" };
        private HRPerson _ValidPerson2 = new HRPerson { dbPeopleID = 2, Enabled = true, EmailAddress = "test@db.com" };
        #endregion 

        [TestInitialize]
        public void Setup()
        {
            _MockDivisionManager = new Mock<IDivisionManager>();
            
            Mock<IDBAccessContainerFactory> mockIDBAccessContainerFactory = new Mock<IDBAccessContainerFactory>();

            _MockPersonManager = new Mock<IPersonManager>();
            _PersonManager = new PersonManager(mockIDBAccessContainerFactory.Object);
            _GenericRequestManager = new GenericRequestManager(mockIDBAccessContainerFactory.Object);

            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _PersonManager, _GenericRequestManager);
        }

        [TestMethod]
        public void When_PersonIsDisabled_Expect_CanAddDivisionRoleUserReturnsPersonIsDisabled()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_DisabledHRPerson);
            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);

            var result = _DivisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.PersonIsDisabled;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_PersonIsRevoked_Expect_CanAddDivisionRoleUserReturnsPersonIsDisabled()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_RevokedHRPerson);
            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);

            var result = _DivisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.PersonIsDisabled;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_PersonHasNoEmail_Expect_CanAddDivisionRoleUserReturnsPersonHasNoEmail()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_PersonWithNoEmail);
            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);

            var result = _DivisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.PersonHasNoEmail;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_HRPersonIsNullAndRoleIsDA_Expect_CanAddDivisionRoleUserReturnsInvalidDBPeopleIDForDivisionAdministrator()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns<HRPerson>(null);
            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);

            var result = _DivisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.InvalidDBPeopleIDForDivisionAdministrator;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_HRPersonIsNullAndRoleIsDivisionOwner_Expect_CanAddDivisionRoleUserReturnsInvalidDBPeopleIDForDivisionOwner()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns<HRPerson>(null);
            _DivisionService = new DivisionService(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);

            var result = _DivisionService.CanAddDivisionRoleUser(RoleType.DivisionOwner, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.InvalidDBPeopleIDForDivisionOwner;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void
            When_PersonAlreadyDivisionAdministratorForThisDivision_Expect_CanAddDivisionRoleUserReturnsPersonAlreadyDivisionAdministratorForThisDivision()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_ValidPerson);

            var mockDivisionService = new Mock<DivisionService>(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);
            mockDivisionService.Setup(a => a.IsPersonAlreadyADivisionAdministrator(_DivisionRoleUsers, 1)).Returns(true);
            mockDivisionService.CallBase = true;

            DivisionService divisionService = mockDivisionService.Object;

            var result = divisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.PersonAlreadyDivisionAdministratorForThisDivision;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void
            When_MaximumDivisionAdministratorsReached_Expect_CanAddDivisionRoleUserReturnsMaximumNumberOfDivisonAdministratorsReached()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_ValidPerson);

            var mockDivisionService = new Mock<DivisionService>(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);
            mockDivisionService.Setup(a => a.CanWeAddMoreDivisionAdministratorsToDivision(_DivisionRoleUsers)).Returns(false);
            mockDivisionService.CallBase = true;

            DivisionService divisionService = mockDivisionService.Object;

            var result = divisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 1, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.MaximumNumberOfDivisonAdministratorsReached;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void
            When_EverythingValidForDivisionAdministrator_Expect_CanAddDivisionRoleUserReturnsSuccess()
        {
            _MockPersonManager.Setup(a => a.GetPerson(2)).Returns(_ValidPerson2);

            var mockDivisionService = new Mock<DivisionService>(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);
            mockDivisionService.Setup(a => a.CanWeAddMoreDivisionAdministratorsToDivision(_DivisionRoleUsers)).Returns(true);
            mockDivisionService.CallBase = true;

            DivisionService divisionService = mockDivisionService.Object;

            var result = divisionService.CanAddDivisionRoleUser(RoleType.DivisionAdministrator, 2, _DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.Success;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void
            When_DivisionOwnerAlreadyExistsForDivision_Expect_CanAddDivisionOwnerReturnsDivisionAlreadyHasADivisionOwner()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_ValidPerson);

            var mockDivisionService = new Mock<DivisionService>(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);
            mockDivisionService.Setup(a => a.DoesDivisionAlreadyHaveADivisionOwner(_DivisionRoleUsers)).Returns(true);
            mockDivisionService.CallBase = true;
            DivisionService divisionService = mockDivisionService.Object;

            var result = divisionService.CanAddDivisionOwner(_DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.DivisionAlreadyHasADivisionOwner;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void
            When_EverythingValid_Expect_CanAddDivisionOwnerReturnsSuccess()
        {
            _MockPersonManager.Setup(a => a.GetPerson(1)).Returns(_ValidPerson);

            var mockDivisionService = new Mock<DivisionService>(CurrentUserID, _MockDivisionManager.Object, _MockPersonManager.Object, _GenericRequestManager);
            mockDivisionService.Setup(a => a.DoesDivisionAlreadyHaveADivisionOwner(_DivisionRoleUsers)).Returns(false);
            mockDivisionService.CallBase = true;
            DivisionService divisionService = mockDivisionService.Object;

            var result = divisionService.CanAddDivisionOwner(_DivisionRoleUsers);
            var expected = AddDivisionRoleUserResult.Success;
            Assert.AreEqual(expected, result);
        }
    }
}
