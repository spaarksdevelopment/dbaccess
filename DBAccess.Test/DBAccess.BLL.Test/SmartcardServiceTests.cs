﻿using System;
using System.Collections.Generic;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using DBAccess.Test.DBAccess.Domain.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.BLL.Test
{
    [TestClass]
    public class SmartcardServiceTests
    {
        private Mock<ISmartcardManager> _MockSmartcardManager;
        private Mock<IPersonManager> _MockPersonManager;

        private SmartcardServiceBusinessLogic _SmartcardService;

        #region Data

        private string[] CertificateDetails = new string[]
        {
            "CN=dbmadeup1234.de.db.com:scep, O=Deutsche Bank AG, DC=db, DC=com",
            "00D9FB123456789123456789",
            "CN=Deutsche Bank Server CA 100, OU=ABC, O=Deutsche Bank AG, C=DE"
        };
        #endregion 

        [TestInitialize]
        public void Setup()
        {
            _MockSmartcardManager = new Mock<ISmartcardManager>();
            _MockPersonManager = new Mock<IPersonManager>();

            _SmartcardService = new SmartcardServiceBusinessLogic(_MockSmartcardManager.Object, _MockPersonManager.Object);
        }

        [TestMethod]
        public void When_SmartcardNumberNotFound_Expect_GetSmartcardOwnerReturnsEmptyString()
        {
            string smartcardIdentifier = "gem2_ZXCVBN0123456789";
            _MockSmartcardManager.Setup(a => a.GetSmartcardNumber(smartcardIdentifier)).Returns<SmartcardNumber>(null);
            _SmartcardService = new SmartcardServiceBusinessLogic(_MockSmartcardManager.Object, _MockPersonManager.Object);

            var result = _SmartcardService.GetSmartcardOwner(smartcardIdentifier, CertificateDetails);
            var expected = string.Empty;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_SmartcardNumberFoundAndBadgeNotFound_Expect_GetSmartcardOwnerReturnsEmptyString()
        {
            string smartcardIdentifier = "gem2_ABCDEF0123456789";
            SmartcardNumber smartcardNumber = RepositoryTestData.GetSmartcardNumber();
            _MockSmartcardManager.Setup(a => a.GetSmartcardNumber(smartcardIdentifier)).Returns(smartcardNumber);
            _MockSmartcardManager.Setup(a => a.GetPersonBadgeFromMifare(smartcardNumber.Mifare)).Returns<HRPersonBadge>(null);
            _SmartcardService = new SmartcardServiceBusinessLogic(_MockSmartcardManager.Object, _MockPersonManager.Object);

            var result = _SmartcardService.GetSmartcardOwner(smartcardIdentifier, CertificateDetails);
            var expected = string.Empty;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_SmartcardNumberFoundAndBadgeFoundAndPersonNotFound_Expect_GetSmartcardOwnerReturnsEmptyString()
        {
            string smartcardIdentifier = "gem2_ABCDEF0123456789";
            SmartcardNumber smartcardNumber = RepositoryTestData.GetSmartcardNumber();
            HRPersonBadge hrPersonBadge = RepositoryTestData.GetHRPersonBadge1();

            _MockSmartcardManager.Setup(a => a.GetSmartcardNumber(smartcardIdentifier)).Returns(smartcardNumber);
            _MockSmartcardManager.Setup(a => a.GetPersonBadgeFromMifare(smartcardNumber.Mifare)).Returns(hrPersonBadge);
            _MockPersonManager.Setup(a => a.GetPerson(hrPersonBadge.dbPeopleID)).Returns<HRPerson>(null);
            
            _SmartcardService = new SmartcardServiceBusinessLogic(_MockSmartcardManager.Object, _MockPersonManager.Object);

            var result = _SmartcardService.GetSmartcardOwner(smartcardIdentifier, CertificateDetails);
            var expected = string.Empty;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_SmartcardNumberFoundAndBadgeFoundAndPersonFound_Expect_GetSmartcardOwnerReturnsDBPeopleID()
        {
            string smartcardIdentifier = "gem2_ABCDEF0123456789";
            SmartcardNumber smartcardNumber = RepositoryTestData.GetSmartcardNumber();
            HRPersonBadge hrPersonBadge = RepositoryTestData.GetHRPersonBadge1();
            HRPerson hrPerson = RepositoryTestData.GetHRPerson();

            _MockSmartcardManager.Setup(a => a.GetSmartcardNumber(smartcardIdentifier)).Returns(smartcardNumber);
            _MockSmartcardManager.Setup(a => a.GetPersonBadgeFromMifare(smartcardNumber.Mifare)).Returns(hrPersonBadge);
            _MockPersonManager.Setup(a => a.GetPerson(hrPersonBadge.dbPeopleID)).Returns(hrPerson);

            _SmartcardService = new SmartcardServiceBusinessLogic(_MockSmartcardManager.Object, _MockPersonManager.Object);

            var result = _SmartcardService.GetSmartcardOwner(smartcardIdentifier, CertificateDetails);
            var expected = hrPerson.dbPeopleID.ToString();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void When_InternalPersonHas7DigitID_Expect_IsValidDBPeopleID_ReturnsTrue()
        {
            HRPerson hrPerson = RepositoryTestData.GetHRPerson();

            bool result = _SmartcardService.IsValidDBPeopleID(hrPerson);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_InternalPersonHasNon7DigitID_Expect_IsValidDBPeopleID_ReturnsFalse()
        {
            HRPerson hrPerson = RepositoryTestData.GetHRPerson5();

            bool result = _SmartcardService.IsValidDBPeopleID(hrPerson);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_ExternalPersonHas7DigitDBPeopleID_Expect_IsValidDBPeopleID_ReturnsTrue()
        {
            HRPerson hrPerson = RepositoryTestData.GetHRPerson4();

            bool result = _SmartcardService.IsValidDBPeopleID(hrPerson);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_ExternalPersonHasNon7DigitID_Expect_IsValidDBPeopleID_ReturnsFalse()
        {
            HRPerson hrPerson = RepositoryTestData.GetHRPerson6();
            bool result = _SmartcardService.IsValidDBPeopleID(hrPerson);
            Assert.IsFalse(result);
        }
    }
}
