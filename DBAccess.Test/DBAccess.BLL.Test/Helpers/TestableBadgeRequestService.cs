﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;

namespace DBAccess.Test.DBAccess.BLL.Test.Helpers
{
    /// <summary>
    /// Extend the badge request service in order to mock up the static HandleException class
    /// </summary>
    public class TestableBadgeRequestService : BadgeRequestService
    {
        public bool CanBadgeRequestsBeCreatedProperty { get; set; }

        public TestableBadgeRequestService(int currentUserID, IBadgeRequestManager manager,
            IPersonManager personManager, IGenericRequestManager genericRequestManager, IEmailService emailService)
            : base(currentUserID, manager, personManager, genericRequestManager, emailService)
        {
        }

        protected override void HandleException(Exception ex)
        {
        }

        internal override bool CanBadgeRequestsBeCreated(int dbPeopleID, List<int> badgesToReplace, int requestMasterID)
        {
            return CanBadgeRequestsBeCreatedProperty;
        }
    }
}
