﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;

namespace DBAccess.Test.DBAccess.BLL.Test.Helpers
{
    /// <summary>
    /// Extend the access request service in order to mock up the static HandleException class
    /// </summary>
    public class TestableAccessRequestService : AccessRequestService
    {
        public TestableAccessRequestService(int currentUserID, IAccessRequestManager accessRequestManager, 
                                                        IGenericRequestManager genericRequestManager)
            : base(currentUserID, accessRequestManager, genericRequestManager)
        {
        }

        protected override void HandleException(Exception ex)
        {
        }
    }
}
