﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBAccess.BLL.Abstract.Helpers;
using DBAccess.BLL.Concrete.Helpers;
using MSTestCaseExtensions;

namespace DBAccess.Test.DBAccess.BLL.Test
{
    [TestClass]
    public class DivisionAdminPopupSettingsTests
    {
        /* compulsory code to allow test method arguments begin*/
        public TestContext TestContext {get; set;}
        [TestMethod]
        [ProvidesTestCases]
        [DataSource("MSTestCaseExtensions", "DivisionAdminPopupSettingsTests", "When_AddingOrEditingDivision_Expect_DateAcceptedColumnVisible", DataAccessMethod.Sequential)]
        public void When_AddingOrEditingDivision_Expect_DateAcceptedColumnVisible()
        {
        }
        /* compulsory code to allow test method arguments end*/

        private DivisionAdminPopupSettings objectUnderTest;

        [TestCase(false, "add", false)]
        [TestCase(true, "edit", false)]
        [TestCase(false, "add", true)]
        [TestCase(true, "edit", true)]
        public void When_AddingOrEditingDivision_Expect_DateAcceptedColumnVisible(bool columnVisibility, string pageType, bool isSubdivision)
        {
            objectUnderTest = DivisionAdminPopupSettingsFactory.GetDivisionAdminPopupSettings(pageType, isSubdivision, (x, y) => { return string.Empty; });
            Assert.AreEqual(columnVisibility, objectUnderTest.ShowDateAcceptedColumn);
        }

        [TestCase(false, "add", false)]
        [TestCase(true, "edit", false)]
        [TestCase(false, "add", true)]
        [TestCase(true, "edit", true)]
        public void When_AddingOrEditingDivision_Expect_CheckForDuplicateCountry(bool columnVisibility, string pageType, bool isSubdivision)
        {
            objectUnderTest = DivisionAdminPopupSettingsFactory.GetDivisionAdminPopupSettings(pageType, isSubdivision, (x, y) => { return string.Empty; });
            Assert.AreEqual(columnVisibility, objectUnderTest.ShowDateAcceptedColumn);
        }
    }
}
