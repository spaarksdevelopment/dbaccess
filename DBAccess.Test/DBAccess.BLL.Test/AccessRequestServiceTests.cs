﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBAccess.BLL.Concrete;
using DBAccess.Domain.Abstract;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Model.Enums;
using DBAccess.Test.DBAccess.BLL.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.BLL.Test
{
    [TestClass]
    public class AccessRequestServiceTests
    {
        private AccessRequestService _AccessRequestService;
        private AccessRequestManager _AccessRequestManager;
        private GenericRequestManager _GenericRequestManager;

        private const int CurrentUserID = 12345;
        private const int RequestPersonID = 1;
        private const int AccessAreaID = 1;

        private Mock<IAccessRequestManager> _MockAccessRequestManager;
        private Mock<IGenericRequestManager> _MockGenericRequestManager;

        #region Data
        private List<CorporateDivisionAccessAreaApprover> _CorporateDivisionAccessAreaApprovers =
            new List<CorporateDivisionAccessAreaApprover>
            {
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 1,
                    mp_SecurityGroupID = (int) RoleType.AccessApprover,
                    Classification = null,
                    Enabled = true
                },
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 2,
                    mp_SecurityGroupID = (int) RoleType.AccessApprover,
                    Classification = null,
                    Enabled = true
                }
            };

        private readonly List<CorporateDivisionAccessAreaApprover> _CorporateDivisionAccessAreaApproversAllDisabled =
            new List<CorporateDivisionAccessAreaApprover>
            {
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 1,
                    Classification = null,
                    Enabled = false
                },
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 2,
                    Classification = null,
                    Enabled = false
                }
            };

        private readonly List<CorporateDivisionAccessAreaApprover> _CorporateDivisionAccessAreaApproversAllWrongGroup =
            new List<CorporateDivisionAccessAreaApprover>
            {
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 1,
                    mp_SecurityGroupID = (int) RoleType.AccessRecertifier,
                    Classification = null,
                    Enabled = true
                },
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 2,
                    mp_SecurityGroupID = (int) RoleType.AccessRecertifier,
                    Classification = null,
                    Enabled = true
                }
            };

        private readonly List<CorporateDivisionAccessAreaApprover> _CorporateDivisionAccessAreaApproversSomeDisabled =
            new List<CorporateDivisionAccessAreaApprover>
            {
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 1,
                    mp_SecurityGroupID = (int) RoleType.AccessApprover,
                    Classification = null,
                    Enabled = true
                },
                new CorporateDivisionAccessAreaApprover()
                {
                    CorporateDivisionAccessAreaApproverId = 2,
                    mp_SecurityGroupID = (int) RoleType.AccessApprover,
                    Classification = null,
                    Enabled = false
                }
            };

        private readonly List<vw_AccessRequestPerson> _AccessRequestPersons = 
            new List<vw_AccessRequestPerson>
            {
                new vw_AccessRequestPerson()
                {
                    RequestPersonID = 1
                },
                new vw_AccessRequestPerson()
                {
                    RequestPersonID = 2
                }
            };
        #endregion 

        [TestInitialize]
        public void Setup()
        {
            _MockAccessRequestManager = new Mock<IAccessRequestManager>();
            _MockAccessRequestManager.Setup(a => a.GetDivisionIDFromAccessAreaID(AccessAreaID)).Returns(1);
            _MockAccessRequestManager.Setup(a => a.GetNumberOfApprovalsRequiredForAccessArea(AccessAreaID)).Returns(1);
            _MockAccessRequestManager.Setup(
                a =>
                    a.GetAccessAreaApproversWhoAreInTheOffice(AccessAreaID))
                        .Returns(_CorporateDivisionAccessAreaApprovers);

            _MockGenericRequestManager = new Mock<IGenericRequestManager>();

            Mock<IDBAccessContainerFactory> mockIDBAccessContainerFactory = new Mock<IDBAccessContainerFactory>();

            _GenericRequestManager = new GenericRequestManager(mockIDBAccessContainerFactory.Object);
            _AccessRequestManager = new AccessRequestManager(mockIDBAccessContainerFactory.Object);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);
        }

        [TestMethod]
        public void When_StartDateIsNull_Expect_DefaultStartDateIsNotNull()
        {
            var result = _AccessRequestService.SetDefaultStartDate(null);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void When_StartDateIsValid_Expect_DefaultStartDateIsSameAsInput()
        {
            DateTime? now = DateTime.Now;
            var result = _AccessRequestService.SetDefaultStartDate(now);
            Assert.AreEqual(now, result);
        }

        [TestMethod]
        public void When_NumberOfApproversLessThanRequired_Expect_DoesAccessAreaHaveEnoughValidApproversIsFalse()
        {
            _MockAccessRequestManager.Setup(a => a.GetNumberOfApprovalsRequiredForAccessArea(1)).Returns(100);

            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            bool result = _AccessRequestService.DoesAccessAreaHaveEnoughValidApprovers(AccessAreaID);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_AccessAreaHasNoDivision_Expect_DoesAccessAreaHaveEnoughValidApproversIsFalse()
        {
            _MockAccessRequestManager.Setup(a => a.GetDivisionIDFromAccessAreaID(1)).Returns((int?)null);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            bool result = _AccessRequestService.DoesAccessAreaHaveEnoughValidApprovers(AccessAreaID);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_NumberOfApproversRequiredIsNull_Expect_DoesAreaHaveEnoughValidApproversIsFalse()
        {
            _MockAccessRequestManager.Setup(a => a.GetNumberOfApprovalsRequiredForAccessArea(1)).Returns<int?>(null);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            bool result = _AccessRequestService.DoesAccessAreaHaveEnoughValidApprovers(AccessAreaID);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_NumberOfApproversRequiredIsMoreThanRequired_Expect_DoesAreaHaveEnoughValidApproversIsTrue()
        {
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            bool result = _AccessRequestService.DoesAccessAreaHaveEnoughValidApprovers(AccessAreaID);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_ApproversListIsEmpty_Expect_GetNumberOfValidApproversIs0()
        {
            List<CorporateDivisionAccessAreaApprover> emptyListCorporateDivisionAccessAreaApprovers =
                    new List<CorporateDivisionAccessAreaApprover>();

            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            int result = _AccessRequestService.GetNumberOfValidApprovers(emptyListCorporateDivisionAccessAreaApprovers);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_ApproversListIsNull_Expect_GetNumberOfValidApproversIs0()
        {
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _GenericRequestManager);

            int result = _AccessRequestService.GetNumberOfValidApprovers(null);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_ApproversAreAllDisabled_Expect_GetNumberOfValidApproversIs0()
        {
            int result =
                _AccessRequestService.GetNumberOfValidApprovers(_CorporateDivisionAccessAreaApproversAllDisabled);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_ApproversAreInWrongGroup_Expect_GetNumberOfValidApproversIs0()
        {
            int result =
                _AccessRequestService.GetNumberOfValidApprovers(_CorporateDivisionAccessAreaApproversAllWrongGroup);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void When_1ApproverInCorrectGroupAndEnabled_Expect_GetNumberOfValidApproversIs1()
        {
            int result =
                _AccessRequestService.GetNumberOfValidApprovers(_CorporateDivisionAccessAreaApproversSomeDisabled);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void When_HourIsNot23_Expect_CorrectDateDoesNotAlterTheDate()
        {
            DateTime validDate = new DateTime(2015, 5, 17, 13, 10, 0);
            DateTime? result =
                _AccessRequestService.CorrectDateForDaylightSavings(validDate);

            Assert.AreEqual(validDate, result);
        }

        [TestMethod]
        public void When_HourIs23_Expect_CorrectDateIncrementsDateBy1Hour()
        {
            DateTime originalDate = new DateTime(2015, 5, 17, 23, 10, 0);
            DateTime expectedDate = new DateTime(2015, 5, 18, 00, 10, 0);

            DateTime? result =
                _AccessRequestService.CorrectDateForDaylightSavings(originalDate);

            Assert.AreEqual(expectedDate, result);
        }

        [TestMethod]
        public void When_HourIs1_Expect_CorrectDateDecrementsDateBy1Hour()
        {
            DateTime originalDate = new DateTime(2015, 5, 17, 1, 10, 0);
            DateTime expectedDate = new DateTime(2015, 5, 17, 00, 10, 0);

            DateTime? result =
                _AccessRequestService.CorrectDateForDaylightSavings(originalDate);

            Assert.AreEqual(expectedDate, result);
        }

        [TestMethod]
        public void When_DateIsNull_Expect_CorrectDateReturnsNull()
        {
            DateTime? result =
                _AccessRequestService.CorrectDateForDaylightSavings(null);

            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void When_RequestPersonListIsEmpty_Expect_CreateAccessRequestForEachPersonResultIsFalse()
        {
            List<vw_AccessRequestPerson> accessRequestPersons = new List<vw_AccessRequestPerson>();
            bool result = _AccessRequestService.CreateAccessRequestForEachPerson(0, null, null, null, null,
                                                                                            accessRequestPersons);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_RequestPersonListIsNull_Expect_CreateAccessRequestForEachPersonResultIsFalse()
        {
            bool result = _AccessRequestService.CreateAccessRequestForEachPerson(0, null, null, null, null,
                                                                                            null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void When_RequestPersonForFirstPersonSuccessful_Expect_CreateAccessRequestForEachPersonResultIsTrue()
        {
            //Need to mock a helper method in the class
            //This code allows us to test a concrete method (CreateAccessRequestForEachPerson) whilst 
            //mocking a helper method (CreateAccessRequestForPerson)
            var mockAccessRequestService = new Mock<AccessRequestService>(CurrentUserID, _AccessRequestManager, 
                                                        _GenericRequestManager);
            List<vw_AccessRequestPerson> accessRequestPersons = _AccessRequestPersons.Take(1).ToList();

            mockAccessRequestService.Setup(a => a.CreateAccessRequestForPerson(0, null, null, null, null, accessRequestPersons.First()))
                                                                                                .Returns(true);
            
            mockAccessRequestService.CallBase = true;

            AccessRequestService accessRequestService = mockAccessRequestService.Object;
            bool result = accessRequestService.CreateAccessRequestForEachPerson(0, null, null, null, null, accessRequestPersons);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void When_RequestPersonForSecondPersonFails_Expect_CreateAccessRequestForEachPersonResultIsFalse()
        {
            var mockAccessRequestService = new Mock<AccessRequestService>(CurrentUserID, _AccessRequestManager,
                                                        _GenericRequestManager);

            mockAccessRequestService.Setup(a => a.CreateAccessRequestForPerson(0, null, null, null, null, _AccessRequestPersons.First()))
                                                                                                .Returns(true);
            vw_AccessRequestPerson secondPersonInList = _AccessRequestPersons[1];
            mockAccessRequestService.Setup(a => a.CreateAccessRequestForPerson(0, null, null, null, null, secondPersonInList))
                                                                                                .Returns(true);
            mockAccessRequestService.CallBase = true;

            AccessRequestService accessRequestService = mockAccessRequestService.Object;
            bool result = accessRequestService.CreateAccessRequestForEachPerson(0, null, null, null, null, _AccessRequestPersons);
            Assert.IsTrue(result);
        }

//        [TestMethod]
//        [ExpectedException(typeof(Exception))]
//        public void When_RequestIDIs0_Expect_CreateAccessRequestForPersonThrowsException()
//        {
//            //TODO - This test isn't working yet. Doesn't seem to call the real method, rather just returns false
//            Mock<IAccessRequestManager> mockAccessRequestManager = new Mock<IAccessRequestManager>();
//            mockAccessRequestManager.Setup(a => a.CreateAccessRequest(CurrentUserID, 0, 0, RequestType.Access, 0, null, null, null, null, null))
//                                            .Returns(0);

//            var mockAccessRequestService = new Mock<AccessRequestService>(CurrentUserID, mockAccessRequestManager.Object,
//                                                        _GenericRequestManager);

//            AccessRequestService accessRequestService = mockAccessRequestService.Object;
//            bool result = accessRequestService.CreateAccessRequestForPerson(0, null, null, null, null, null);
//            System.Diagnostics.Debug.Print(result.ToString());
//        }

        [TestMethod]
        public void
            When_RequestMasterIDDoesNotExist_Expect_CreateNewRequestForPersonReturnsFalseAndFailureReasonIsInvalidRequestMasterID
            ()
        {
            _MockGenericRequestManager.Setup(a => a.DoesAccessRequestMasterExist(1)).Returns(false);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _MockGenericRequestManager.Object);

            int requestMasterID = 1;
            bool actual = _AccessRequestService.CreateNewRequestForPerson(1, ref requestMasterID);

            Assert.IsFalse(actual);

            Assert.AreEqual(FailureReason.InvalidRequestMasterID, _AccessRequestService.AccessRequestFailureReason);
        }

        [TestMethod]
        public void
            When_CreateRequestMasterAndRequestPersonReturns0_Expect_CreateNewRequestForPersonReturnsFalseAndHandleExceptionIsCalled
            ()
        {
            int requestMasterID = 1;
            _MockGenericRequestManager.Setup(a => a.DoesAccessRequestMasterExist(1)).Returns(true);
            _MockGenericRequestManager.Setup(a => a.CreateRequestMasterAndRequestPerson(CurrentUserID, 1, ref requestMasterID)).Returns(0);
            _AccessRequestService = new TestableAccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _MockGenericRequestManager.Object);

            bool actual = _AccessRequestService.CreateNewRequestForPerson(1, ref requestMasterID);
            Assert.IsFalse(actual);

            Assert.AreEqual(FailureReason.Exception, _AccessRequestService.AccessRequestFailureReason);
        }

        [TestMethod]
        public void
            When_CreateRequestMasterAndRequestPersonReturnsMinus1_Expect_CreateNewRequestForPersonReturnsFalseAndFailureReasonIsPersonAlreadyOnRequest
            ()
        {
            int requestMasterID = 1;
            _MockGenericRequestManager.Setup(a => a.DoesAccessRequestMasterExist(1)).Returns(true);
            _MockGenericRequestManager.Setup(a => a.CreateRequestMasterAndRequestPerson(CurrentUserID, 1, ref requestMasterID)).Returns(-1);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _MockGenericRequestManager.Object);

            bool actual = _AccessRequestService.CreateNewRequestForPerson(1, ref requestMasterID);

            Assert.IsFalse(actual);

            Assert.AreEqual(FailureReason.PersonAlreadyOnRequest, _AccessRequestService.AccessRequestFailureReason);
        }

        [TestMethod]
        public void When_CreateRequestMasterAndRequestPersonReturnsRequestPersonID_Expect_CreateNewRequestForPersonReturnsTrue()
        {
            int requestMasterID = 1;
            _MockGenericRequestManager.Setup(a => a.DoesAccessRequestMasterExist(1)).Returns(true);
            _MockGenericRequestManager.Setup(a => a.CreateRequestMasterAndRequestPerson(CurrentUserID, 1, ref requestMasterID)).Returns(1);
            _AccessRequestService = new AccessRequestService(CurrentUserID, _MockAccessRequestManager.Object, _MockGenericRequestManager.Object);

            bool actual = _AccessRequestService.CreateNewRequestForPerson(1, ref requestMasterID);

            Assert.IsTrue(actual);
        }
    }
}
