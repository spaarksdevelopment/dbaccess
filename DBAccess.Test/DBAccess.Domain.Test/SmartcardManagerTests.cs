﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Test.DBAccess.Domain.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.Domain.Test
{
    [TestClass]
    public class SmartcardManagerTests
    {
        private SmartcardManager _SmartcardManager;
        private Mock<IDBAccessContainer> _MockDBAccessContainer;

        [TestInitialize]
        public void Setup()
        {
            _MockDBAccessContainer = new Mock<IDBAccessContainer>();

            Mock<IDbSet<HRPersonBadge>> mockDbSetHRPersonBadge = MockingHelpers.GetMockDbSetHRPersonBadge();
            _MockDBAccessContainer.Setup(a => a.HRPersonBadges).Returns(mockDbSetHRPersonBadge.Object);
            Mock<IDbSet<SmartcardNumber>> mockDbSetSmartcardNumber = MockingHelpers.GetMockDbSetSmartcardNumber();
            _MockDBAccessContainer.Setup(a => a.SmartcardNumbers).Returns(mockDbSetSmartcardNumber.Object);
            Mock<IDbSet<SmartcardServiceAudit>> mockDbSetSmartcardServiceAudit = MockingHelpers.GetMockDbSetSmartcardServiceAudit();
            _MockDBAccessContainer.Setup(a => a.SmartcardServiceAudits).Returns(mockDbSetSmartcardServiceAudit.Object);

            Mock<IDBAccessContainerFactory> mockDBAccessContainerFactory = MockingHelpers.GetMockDBAccessContainerFactory(_MockDBAccessContainer);

            _SmartcardManager = new SmartcardManager(mockDBAccessContainerFactory.Object);
        }

        [TestMethod]
        public void When_BadgeExistsWithMifareNoPaddingRequired_GetPersonBadgeFromMifare_ReturnsHRPersonBadge()
        {
            string mifare = "123456789012";
            HRPersonBadge result = _SmartcardManager.GetPersonBadgeFromMifare(mifare);

            Assert.AreEqual(1, result.PersonBadgeID);
            Assert.AreEqual(mifare, result.MiFareNumber);
        }

        [TestMethod]
        public void When_BadgeExistsWithMifareNotPadded_GetPersonBadgeFromMifare_ReturnsHRPersonBadge()
        {
            string mifare = "12345678901";
            string paddedMifare = "012345678901";

            HRPersonBadge result = _SmartcardManager.GetPersonBadgeFromMifare(mifare);

            Assert.AreEqual(2, result.PersonBadgeID);
            Assert.AreEqual(mifare, result.MiFareNumber);
        }

        [TestMethod]
        public void When_BadgeExistsWithMifarePadded_GetPersonBadgeFromMifare_ReturnsHRPersonBadge()
        {
            string mifare = "12345678902";
            string paddedMifare = "012345678902";

            HRPersonBadge result = _SmartcardManager.GetPersonBadgeFromMifare(mifare);

            Assert.AreEqual(3, result.PersonBadgeID);
            Assert.AreEqual(paddedMifare, result.MiFareNumber);
        }

        [TestMethod]
        public void When_BadgeExistsNotFound_GetPersonBadgeFromMifare_ReturnsNull()
        {
            HRPersonBadge result = _SmartcardManager.GetPersonBadgeFromMifare("123456789013");
            Assert.IsNull(result);
        }

        [TestMethod]
        public void When_SmartcardNumberFound_GetSmartcardNumber_ReturnsSmartcardNumber()
        {
            SmartcardNumber result = _SmartcardManager.GetSmartcardNumber("gem2_ABCDEF0123456789");
            Assert.AreEqual(1, result.ID);
            Assert.AreEqual(10, result.SmartcardProviderID);
            Assert.AreEqual(100, result.SmartcardAuditID);
            Assert.AreEqual(3, result.SmartcardTypeID);
            Assert.AreEqual("123456789012", result.Mifare);
        }

        [TestMethod]
        public void When_SmartcardNumberNotFound_GetSmartcardNumber_ReturnsNull()
        {
            SmartcardNumber result = _SmartcardManager.GetSmartcardNumber("gem2_ABCDEF0123456781");
            Assert.IsNull(result);
        }

        //[TestMethod]
        //public void When_AddingNewSmartcardAudit_AddSmartcardServiceAudit_AddsEntryToSmartcardServiceAudit()
        //{
        //    string methodCalled = "GetSmartcardStatus";
        //    string smartcardIdentifier = "gem2_ABCDEF0123456789";
        //    string returnedValue = "Active";

        //    string certSubjectName = "CN=dbmadeup1234.de.db.com:scep, O=Deutsche Bank AG, DC=db, DC=com";
        //    string certSerialNumber = "00D9FB123456789123456789";
        //    string certIssuerName = "CN=Deutsche Bank Server CA 100, OU=ABC, O=Deutsche Bank AG, C=DE";
        //    string[] certDetails = new string[]
        //    {
        //        certSubjectName,
        //        certSerialNumber,
        //        certIssuerName
        //    }; 

        //    _SmartcardManager.AddSmartcardServiceAudit(methodCalled, smartcardIdentifier, returnedValue, certDetails, null);
        //    SmartcardServiceAudit result = ;


        //    //Assert.AreEqual(methodCalled, result.MethodCalled);
        //    //Assert.IsNotNull(result.DateMethodCalled);
        //    //Assert.IsTrue(result.DateMethodCalled > DateTime.Today.Subtract(new TimeSpan(1,0,0)));
        //    //Assert.AreEqual(smartcardIdentifier, result.SmartcardIdentifier);
        //    //Assert.AreEqual(returnedValue, result.ReturnedValue);
        //    //Assert.AreEqual(certSubjectName, result.CertSubjectName);
        //    //Assert.AreEqual(certSerialNumber, result.CertSerialNumber);
        //    //Assert.AreEqual(certIssuerName, result.CertIssuerName);
        //    //Assert.IsNull(result.ErrorMessage);
        //}
    }
}
