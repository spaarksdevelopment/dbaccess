﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Test.DBAccess.Domain.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.Domain.Test
{
    [TestClass]
    public class PersonManagerTests
    {
        private PersonManager _PersonManager;
        private Mock<IDBAccessContainer> _MockDBAccessContainer;

        [TestInitialize]
        public void Setup()
        {
            _MockDBAccessContainer = new Mock<IDBAccessContainer>();

            Mock<IDbSet<HRPerson>> mockDbSetHRPerson = MockingHelpers.GetMockDbSetHRPerson();
            _MockDBAccessContainer.Setup(a => a.HRPersons).Returns(mockDbSetHRPerson.Object);

            Mock<IDBAccessContainerFactory> mockDBAccessContainerFactory = MockingHelpers.GetMockDBAccessContainerFactory(_MockDBAccessContainer);

            _PersonManager = new PersonManager(mockDBAccessContainerFactory.Object);
        }

        [TestMethod]
        public void When_HRPersonFound_GetSmartcardNumber_ReturnsHRPerson()
        {
            HRPerson result = _PersonManager.GetPerson(1234567);

            Assert.AreEqual(1234567, result.dbPeopleID);
            Assert.AreEqual("Testy", result.Forename);
            Assert.AreEqual("McTest", result.Surname);
            Assert.AreEqual("testy.mctest@db.com", result.EmailAddress);
        }

        [TestMethod]
        public void When_HRPersonNotFound_GetSmartcardNumber_ReturnsNull()
        {
            HRPerson result = _PersonManager.GetPerson(1234568);
            Assert.IsNull(result);
        }
    }
}
