﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using DBAccess.Domain.Concrete;
using DBAccess.Model.DAL;
using DBAccess.Test.DBAccess.Domain.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DBAccess.Test.DBAccess.Domain.Test
{
    [TestClass]
    public class GenericRequestManagerTests
    {
        private GenericRequestManager _GenericRequestManager;
        private Mock<IDBAccessContainer> _MockDBAccessContainer;

        [TestInitialize]
        public void Setup()
        {
            _MockDBAccessContainer = new Mock<IDBAccessContainer>();

            Mock<IDbSet<vw_AccessRequestPerson>> mockDbSetVWAccessRequestPerson = MockingHelpers.GetMockDbSetVwAccessRequestPerson();
            _MockDBAccessContainer.Setup(a => a.vw_AccessRequestPerson).Returns(mockDbSetVWAccessRequestPerson.Object);

            Mock<IDbSet<AccessRequestMaster>> mockDbSetAccessRequestMaster = MockingHelpers.GetMockDbSetAccessRequestMaster();
            _MockDBAccessContainer.Setup(a => a.AccessRequestMasters).Returns(mockDbSetAccessRequestMaster.Object);

            Mock<IDbSet<AccessRequestPerson>> mockDbSetAccessRequestPerson = MockingHelpers.GetMockDbSetAccessRequestPerson();
            _MockDBAccessContainer.Setup(a => a.AccessRequestPersons).Returns(mockDbSetAccessRequestPerson.Object);

            Mock<IDbSet<AccessRequest>> mockDbSetAccessRequest = MockingHelpers.GetMockDbSetAccessRequest();
            _MockDBAccessContainer.Setup(a => a.AccessRequests).Returns(mockDbSetAccessRequest.Object);

            Mock<IDBAccessContainerFactory> mockDBAccessContainerFactory = MockingHelpers.GetMockDBAccessContainerFactory(_MockDBAccessContainer);

            _GenericRequestManager = new GenericRequestManager(mockDBAccessContainerFactory.Object);
        }

        [TestMethod]
        public void When_AccessRequestPersonExists_GetAccessRequestPersons_ReturnsVWAccessRequestPersonsList()
        {
            List<vw_AccessRequestPerson> result = _GenericRequestManager.GetAccessRequestPersons(1710);

            var expected = new List<vw_AccessRequestPerson>{RepositoryTestData.GetVwAccessRequestPerson()};
            
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(expected.First().RequestPersonID, result.First().RequestPersonID);
            Assert.AreEqual(expected.First().dbPeopleID, result.First().dbPeopleID);
            Assert.AreEqual(expected.First().FulldbPeopleID, result.First().FulldbPeopleID);
            Assert.AreEqual(expected.First().Forename, result.First().Forename);
            Assert.AreEqual(expected.First().Surname, result.First().Surname);
            Assert.AreEqual(expected.First().Telephone, result.First().Telephone);
            Assert.AreEqual(expected.First().EmailAddress, result.First().EmailAddress);
            Assert.AreEqual(expected.First().CostCentre, result.First().CostCentre);
            Assert.IsNull(result.First().CostCentreName);
            Assert.IsNull(result.First().UBR_Code);
            Assert.IsNull(result.First().UBR_Name);
            Assert.AreEqual(expected.First().DivisionID, result.First().DivisionID);
            Assert.AreEqual(expected.First().DivisionName,result.First().DivisionName);
            Assert.AreEqual(expected.First().BusinessAreaID,result.First().BusinessAreaID);
            Assert.AreEqual(expected.First().BusinessAreaName,result.First().BusinessAreaName);
            Assert.IsTrue(result.First().IsExternal);
            Assert.AreEqual(expected.First().CreatedByID, result.First().CreatedByID);
            Assert.AreEqual(expected.First().Created, result.First().Created);
            Assert.AreEqual(expected.First().RequestMasterID,result.First().RequestMasterID);
            Assert.IsNull(result.First().VendorID);
            Assert.IsNull(result.First().VendorName);
            Assert.IsNull(result.First().CountryID);
            Assert.AreEqual(expected.First().CountryName,result.First().CountryName);
            Assert.IsNull(result.First().CountryEnabled);
            Assert.IsNull(result.First().RegionID);
            Assert.IsNull(result.First().PassOfficeID);
            Assert.AreEqual(expected.First().dbDirID,result.First().dbDirID);
            Assert.IsNull(result.First().Class);
            Assert.AreEqual(expected.First().HRClassificationID, result.First().HRClassificationID);
            Assert.IsNull(result.First().HRClassDescription);
            Assert.AreEqual(expected.First().PersonUBR, result.First().PersonUBR);
        }

        [TestMethod]
        public void When_AccessRequestPersonDoesntExist_GetAccessRequestPersons_ReturnsEmptyList()
        {
            List<vw_AccessRequestPerson> result = _GenericRequestManager.GetAccessRequestPersons(1711);

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void When_AccessRequestMasterDoesntExist_UpdateMasterRequestComment_ThrowsException()
        {
            _GenericRequestManager.UpdateMasterRequestComment(-1, "comment");
        }

        //[TestMethod]
        //public void When_AccessRequestMasterExists_UpdateMasterRequestComment_UpdatesComment()
        //{
        //    _GenericRequestManager.UpdateMasterRequestComment(123, "new comment");
        //    var accessRequestMaster = _MockDBAccessContainer.Object.AccessRequestMasters.Find(123);

        //    string expectedComment = "new comment";

        //    Assert.AreEqual(expectedComment, accessRequestMaster.Comment);
        //}

        [TestMethod]
        public void When_RequestExistForGivenRequestMasterID_GetRequestIDsReturnsRequestIDs()
        {
            List<int> requestIDs = _GenericRequestManager.GetRequestIDs(123);
            
            Assert.AreEqual(789, requestIDs.First());
            Assert.AreEqual(790, requestIDs.Skip(1).Take(1).First());
        }
    }
}
