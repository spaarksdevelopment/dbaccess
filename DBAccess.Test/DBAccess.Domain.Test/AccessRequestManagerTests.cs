﻿
using DBAccess.Model.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data.Entity;
using DBAccess.Domain.Concrete;
using DBAccess.Test.DBAccess.Domain.Test.Helpers;

namespace DBAccess.Test.DBAccess.Domain.Test
{
    [TestClass]
    public class AccessRequestManagerTests
    {
        private AccessRequestManager _AccessRequestManager;
        private Mock<IDBAccessContainer> _MockDBAccessContainer;

        [TestInitialize]
        public void Setup()
        {
            _MockDBAccessContainer = new Mock<IDBAccessContainer>();

            Mock<IDbSet<vw_AccessAreaRequest>> mockDbSetVwAccessAreaRequest = MockingHelpers.GetMockDbSetVwAccessAreaRequest();
            _MockDBAccessContainer.Setup(a => a.vw_AccessAreaRequest).Returns(mockDbSetVwAccessAreaRequest.Object);

            Mock<IDbSet<HRPerson>> mockDbSetHRPerson = MockingHelpers.GetMockDbSetHRPerson();
            mockDbSetHRPerson.Setup(a => a.Find(1234569)).Returns(RepositoryTestData.GetHRPerson2());
            mockDbSetHRPerson.Setup(a => a.Find(1234567)).Returns(RepositoryTestData.GetHRPerson());
            _MockDBAccessContainer.Setup(a => a.HRPersons).Returns(mockDbSetHRPerson.Object);

            Mock<IDbSet<AccessRequestPerson>> mockDbSetAccessRequestPerson = MockingHelpers.GetMockDbSetAccessRequestPerson();
            mockDbSetAccessRequestPerson.Setup(a => a.Find(458)).Returns(RepositoryTestData.GetAccessRequestPerson3());
            mockDbSetAccessRequestPerson.Setup(a => a.Find(457)).Returns(RepositoryTestData.GetAccessRequestPerson2());
            mockDbSetAccessRequestPerson.Setup(a => a.Find(456)).Returns(RepositoryTestData.GetAccessRequestPerson1());
            _MockDBAccessContainer.Setup(a => a.AccessRequestPersons).Returns(mockDbSetAccessRequestPerson.Object);

            //Mock<IDbSet<CorporateDivisionAccessArea>> mockDbSetCorporateDivisionAccessArea = MockingHelpers.GetMockDbSetCorporateDivisionAccessArea();

            Mock<IDbSet<HRClassification>> mockDbSetHRClassification = MockingHelpers.GetMockDbSetHRClassifications();
            _MockDBAccessContainer.Setup(a => a.HRClassifications).Returns(mockDbSetHRClassification.Object);


            Mock<IDBAccessContainerFactory> mockDBAccessContainerFactory = MockingHelpers.GetMockDBAccessContainerFactory(_MockDBAccessContainer);

            _AccessRequestManager = new AccessRequestManager(mockDBAccessContainerFactory.Object);
        }

        [TestMethod]
        public void When_AccessAreaAlreadyAdded_CheckAccessAreaExistsForRequest_ReturnsTrue()
        {
            var actual = _AccessRequestManager.CheckAccessAreaExistsForRequest(1, 1);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void When_AccessAreaAlreadyAdded_CheckAccessAreaExistsForRequest_ReturnsFalse()
        {
            var actual = _AccessRequestManager.CheckAccessAreaExistsForRequest(2, 1);
            Assert.IsFalse(actual);
        }

        //[TestMethod]
        //public void WhenCorporateDivisionAccessAreaDoesntExistForThisAccessAreaID_Expect_GetAccessAreaApproversByClassificationIDWhoAreInTheOfficeReturnsNull()
        //{
        //    _AccessRequestManager
        //}
    }
}
