﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBAccess.BLL.Concrete;
using DBAccess.Model.DAL;

namespace DBAccess.Test.DBAccess.Domain.Test.Helpers
{
    internal static class RepositoryTestData
    {
        internal static IQueryable<vw_AccessRequestPerson> GetViewAccessRequestPersonQuery()
        {
            return new List<vw_AccessRequestPerson>
            {
                GetVwAccessRequestPerson()
            }.AsQueryable();
        }


        internal static vw_AccessRequestPerson GetVwAccessRequestPerson()
        {
            return new vw_AccessRequestPerson
            {
                RequestPersonID = 229,
                dbPeopleID = 1396559,
                FulldbPeopleID = "1396559",
                Forename = "Cherie",
                Surname = "Pope",
                Telephone = "7192286720",
                EmailAddress = "Cherie.Pope@db.com",
                CostCentre = "21802150",
                CostCentreName = null,
                UBR_Code = null,
                UBR_Name = null,
                DivisionID = 235,
                DivisionName = "G_8223 - Compliance GTB",
                BusinessAreaID = 315,
                BusinessAreaName = "G_1200 - GTO Technology Supply",
                IsExternal = true,
                CreatedByID = 228,
                Created = DateTime.Parse("2011-07-18 14:33:16.730"),
                RequestMasterID = 1710,
                VendorID = null,
                VendorName = null,
                CountryID = null,
                CountryName = "Germany",
                CountryEnabled = null,
                RegionID = null,
                PassOfficeID = null,
                dbDirID = 8715941,
                Class = null,
                HRClassificationID = 0,
                HRClassDescription = null,
                PersonUBR = "G_2302 Registrar Services"
            };
        }

        internal static IQueryable<AccessRequestMaster> GetAccessRequestMastersQuery()
        {
            return new List<AccessRequestMaster>
            {
                GetAccessRequestMaster()
            }.AsQueryable();
        }

        internal static AccessRequestMaster GetAccessRequestMaster()
        {
            return new AccessRequestMaster
            {
                RequestMasterID = 123,
                Created = DateTime.Parse("2014-11-01 15:43:00.000"),
                Comment = "This is a comment",
                RequestTypeID = 2,
                RequestStatusID = 5,
                CreatedByID = 1245526,
                SponsorUserID = null
            };
        }

        internal static IQueryable<AccessRequestPerson> GetAccessRequestPersonQuery()
        {
            return new List<AccessRequestPerson>
            {
                GetAccessRequestPerson1(),
                GetAccessRequestPerson2(),
                GetAccessRequestPerson3()
            }.AsQueryable();
        }

        internal static AccessRequestPerson GetAccessRequestPerson1()
        {
            return new AccessRequestPerson
            {
                RequestPersonID = 456,
                RequestMasterID = 123,
                dbPeopleID = 1234569
            };
        }

        internal static AccessRequestPerson GetAccessRequestPerson2()
        {
            return new AccessRequestPerson
            {
                RequestPersonID = 457,
                RequestMasterID = 123,
                dbPeopleID = 1234568
            };
        }

        internal static AccessRequestPerson GetAccessRequestPerson3()
        {
            return new AccessRequestPerson
            {
                RequestPersonID = 458,
                RequestMasterID = 124,
                dbPeopleID = 1234567
            };
        }

        internal static IQueryable<AccessRequest> GetAccessRequestQuery()
        {
            return new List<AccessRequest>
            {
                GetAccessRequest1(),
                GetAccessRequest2()
            }.AsQueryable();
        }

        internal static AccessRequest GetAccessRequest1()
        {
            return new AccessRequest
            {
                RequestID = 789,
                RequestPersonID = 456,
            };
        }

        internal static AccessRequest GetAccessRequest2()
        {
            return new AccessRequest
            {
                RequestID = 790,
                RequestPersonID = 457,
            };
        }

        internal static IQueryable<HRPersonBadge> GetHRPersonBadgeQuery()
        {
            return new List<HRPersonBadge>
            {
                GetHRPersonBadge1(),
                GetHRPersonBadge2(),
                GetHRPersonBadge3()
            }.AsQueryable();
        }

        internal static HRPersonBadge GetHRPersonBadge1()
        {
            return new HRPersonBadge
            {
                PersonBadgeID = 1,
                MiFareNumber = "123456789012",
                dbPeopleID = 1234567
            };
        }

        internal static HRPersonBadge GetHRPersonBadge2()
        {
            return new HRPersonBadge
            {
                PersonBadgeID = 2,
                MiFareNumber = "12345678901" 
            };
        }

        internal static HRPersonBadge GetHRPersonBadge3()
        {
            return new HRPersonBadge
            {
                PersonBadgeID = 3,
                MiFareNumber = "012345678902" 
            };
        }

        internal static IQueryable<SmartcardNumber> GetSmartcardNumberQuery()
        {
            return new List<SmartcardNumber>
            {
                GetSmartcardNumber()
            }.AsQueryable();
        }

        internal static SmartcardNumber GetSmartcardNumber()
        {
            return new SmartcardNumber
            {
                ID = 1,
                SerialNumber = "gem2_ABCDEF0123456789",
                SmartcardProviderID = 10,
                SmartcardAuditID = 100,
                SmartcardTypeID = 3,
                Mifare = "123456789012"
            };
        }

        internal static IQueryable<HRPerson> GetHRPersonQuery()
        {
            return new List<HRPerson>
            {
                GetHRPerson()
            }.AsQueryable();
        }

        internal static HRPerson GetHRPerson()
        {
            return new HRPerson
            {
                dbPeopleID = 1234567,
                Forename = "Testy",
                Surname = "McTest",
                EmailAddress = "testy.mctest@db.com",
                Class = "Class_1",
                IsExternal = false
            };
        }

        internal static HRPerson GetHRPerson2()
        {
            return new HRPerson
            {
                dbPeopleID = 1234569,
                Forename = "Testy",
                Surname = "Otest",
                EmailAddress = "testy.otest@db.com",
                Class = "FakeClass"
            };
        }

        internal static HRPerson GetHRPerson3()
        {
            return new HRPerson
            {
                dbPeopleID = 8234567,
                Forename = "Janet",
                Surname = "Otest",
                EmailAddress = "janet.otest@db.com",
                Class = "FakeClass",
                IsExternal = true
            };
        }

        internal static HRPerson GetHRPerson4()
        {
            return new HRPerson
            {
                dbPeopleID = 9234567,
                Forename = "Robert",
                Surname = "Otest",
                EmailAddress = "robert.otest@db.com",
                Class = "FakeClass",
                IsExternal = true
            };
        }

        internal static HRPerson GetHRPerson5()
        {
            return new HRPerson
            {
                dbPeopleID = 12345678,
                Forename = "Simon",
                Surname = "McTest",
                EmailAddress = "simon.mctest@db.com",
                Class = "Class_1",
                IsExternal = false,
            };
        }

        internal static HRPerson GetHRPerson6()
        {
            return new HRPerson
            {
                dbPeopleID = 82345678,
                Forename = "Steven",
                Surname = "McTest",
                EmailAddress = "simon.mctest@db.com",
                Class = "Class_1",
                IsExternal = true,
            };
        }

        internal static IQueryable<SmartcardServiceAudit> GetSmartcardServiceAuditQuery()
        {
            return new List<SmartcardServiceAudit>
            {
                GetSmartcardServiceAudit()
            }.AsQueryable();
        }

        internal static SmartcardServiceAudit GetSmartcardServiceAudit()
        {
            return new SmartcardServiceAudit
            {
                ID = 1234,
                DateMethodCalled = DateTime.Parse("2014-10-03 21:42:56.743"),
                MethodCalled = "GetSmartcardStatus",
                SmartcardIdentifier = "gem2_ABCDEF0123456789",
                ReturnedValue = "Active",
                CertSubjectName = "CN=dbmadeup1234.de.db.com:scep, O=Deutsche Bank AG, DC=db, DC=com",
                CertSerialNumber = "00D9FB123456789123456789",
                CertIssuerName = "CN=Deutsche Bank Server CA 100, OU=ABC, O=Deutsche Bank AG, C=DE"
            };
        }

        internal static IQueryable<vw_AccessAreaRequest> GetVwAccessAreaRequestQuery()
        {
            return new List<vw_AccessAreaRequest>
            {
                GetVwAccessAreaRequest()
            }.AsQueryable();
        }

        internal static vw_AccessAreaRequest GetVwAccessAreaRequest()
        {
            return new vw_AccessAreaRequest
            {
                AccessAreaID=1, 
                RequestMasterID = 1
            };
        }

        internal static IQueryable<HRClassification> GetHRClassificationsQuery()
        {
            return GetHRClassifications().AsQueryable();
        }

        internal static List<HRClassification> GetHRClassifications()
        {
            return new List<HRClassification>
            {
                new HRClassification { HRClassificationID = 1, Classification = "Class_1" },
                new HRClassification { HRClassificationID = 2, Classification = "Class_2" },
                new HRClassification { HRClassificationID = 3, Classification = "Class_3" },
                new HRClassification { HRClassificationID = 4, Classification = "Class_4" },
                new HRClassification { HRClassificationID = 5, Classification = "Class_5" }
            };
        }
    }
}
