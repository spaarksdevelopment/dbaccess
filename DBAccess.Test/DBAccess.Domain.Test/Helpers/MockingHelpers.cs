﻿using System.Data.Entity;
using System.Linq;
using DBAccess.Model.DAL;
using Moq;

namespace DBAccess.Test.DBAccess.Domain.Test.Helpers
{
    internal static class MockingHelpers
    {
        internal static Mock<IDbSet<T>> GetMockDbSet<T>(IQueryable<T> data) where T : class
        {
            var mockDbSet = new Mock<IDbSet<T>>();

            mockDbSet.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(data.Provider);
            mockDbSet.As<IQueryable<T>>()
                .Setup(m => m.Expression)
                .Returns(data.Expression);
            mockDbSet.As<IQueryable<T>>()
                .Setup(m => m.ElementType)
                .Returns(data.ElementType);
            mockDbSet.As<IQueryable<T>>()
                .Setup(m => m.GetEnumerator())
                .Returns(data.GetEnumerator());
            return mockDbSet;
        }

        internal static Mock<IDBAccessContainerFactory> GetMockDBAccessContainerFactory(
            Mock<IDBAccessContainer> mockDBAccessContainer)
        {
            Mock<IDBAccessContainerFactory> mockDBAccessContainerFactory = new Mock<IDBAccessContainerFactory>();
            mockDBAccessContainerFactory.Setup(a => a.GetDBAccessContainer()).Returns(mockDBAccessContainer.Object);

            return mockDBAccessContainerFactory;
        }

        internal static Mock<IDbSet<vw_AccessRequestPerson>> GetMockDbSetVwAccessRequestPerson()
        {
            var data = RepositoryTestData.GetViewAccessRequestPersonQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<AccessRequestMaster>> GetMockDbSetAccessRequestMaster()
        {
            var data = RepositoryTestData.GetAccessRequestMastersQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<AccessRequestPerson>> GetMockDbSetAccessRequestPerson()
        {
            var data = RepositoryTestData.GetAccessRequestPersonQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<AccessRequest>> GetMockDbSetAccessRequest()
        {
            var data = RepositoryTestData.GetAccessRequestQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<HRPersonBadge>> GetMockDbSetHRPersonBadge()
        {
            var data = RepositoryTestData.GetHRPersonBadgeQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<SmartcardNumber>> GetMockDbSetSmartcardNumber()
        {
            var data = RepositoryTestData.GetSmartcardNumberQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<HRPerson>> GetMockDbSetHRPerson()
        {
            var data = RepositoryTestData.GetHRPersonQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<SmartcardServiceAudit>> GetMockDbSetSmartcardServiceAudit()
        {
            var data = RepositoryTestData.GetSmartcardServiceAuditQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<vw_AccessAreaRequest>> GetMockDbSetVwAccessAreaRequest()
        {
            var data = RepositoryTestData.GetVwAccessAreaRequestQuery();
            return MockingHelpers.GetMockDbSet(data);
        }

        internal static Mock<IDbSet<HRClassification>> GetMockDbSetHRClassifications()
        {
            var data = RepositoryTestData.GetHRClassificationsQuery();
            return MockingHelpers.GetMockDbSet(data);
        }
    }
}
