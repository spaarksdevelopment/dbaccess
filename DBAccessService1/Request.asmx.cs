﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Spaarks.CustomMembershipManager;
using Aduvo.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using System.Web.Security;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using Aduvo.Common.WebUtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using DBAccess.BLL.Abstract;
using DBAccess.BLL.Concrete;

namespace DBAccessService1
{
    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [Serializable()]
    public class DBAccessResponse
    {
        bool _Success = false;
        string _ErrorMessage = string.Empty;
        int _ErrorCode = 0;
        AccessAreaStatus[] _AccessAreaStatusArray;

        public bool Success
        {
            get { return _Success; }
            set { _Success = value; }
        }

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        public int ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        public AccessAreaStatus[] AccessAreaStatusArray
        {
            get { return _AccessAreaStatusArray; }
            set { _AccessAreaStatusArray = value; }
        }

        public class AccessAreaStatus 
        {
            int _ID;
            string _AccessAreaName;
            string _Status;

            public string Status
            {
                get { return _Status; }
                set { _Status = value; }
            }

            [XmlAttribute]
            public int ID
            {
                get { return _ID; }
                set { _ID = value; }
            }

            [XmlAttribute]
            public string AccessAreaName
            {
                get { return _AccessAreaName; }
                set { _AccessAreaName = value; }
            }
        } 

        public DBAccessResponse() { }
    }

    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [Serializable()]
    public class DBAccessBuilding
    {
        int _ID;
        string _BuildingName;
        int _CityID;
        string _CityName;

        DBAccessAccessArea[] _AccessAreas;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string BuildingName
        {
            get { return _BuildingName; }
            set { _BuildingName = value; }
        }

        public int CityID
        {
            get { return _CityID; }
            set { _CityID = value; }
        }

        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }

        public DBAccessAccessArea[] AccessAreas
        {
            get { return _AccessAreas; }
            set { _AccessAreas = value; }
        }        

        public DBAccessBuilding() { }
    }

    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [Serializable()]
    public class DBAccessAccessArea
    {
        int _ID;
        string _AccessAreaName;
        bool _IsGeneralArea;
        int _FloorID;
        string _FloorName;

        [XmlAttribute]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string AccessAreaName
        {
            get { return _AccessAreaName; }
            set { _AccessAreaName = value; }
        }

        public bool IsGeneralArea
        {
            get { return _IsGeneralArea; }
            set { _IsGeneralArea = value; }
        }

        public int FloorID
        {
            get { return _FloorID; }
            set { _FloorID = value; }
        }

        public string FloorName
        {
            get { return _FloorName; }
            set { _FloorName = value; }
        }

        public DBAccessAccessArea() { }
    }

    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [Serializable()]
    public class DBAccessDBMovesMatch
    {
        int _AccessAreaID;
        string _AccessAreaName;

        public int AccessAreaID
        {
            get { return _AccessAreaID; }
            set { _AccessAreaID = value; }
        }

        public string AccessAreaName
        {
            get { return _AccessAreaName; }
            set { _AccessAreaName = value; }
        }

        public DBAccessDBMovesMatch() { }
    }

    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [Serializable()]
    public class DBAccessPerson
    {
        int _DBPeopleID;
        string _Name;
        string _Email;
        string _DisplayName;

        public int DBPeopleID
        {
            get { return _DBPeopleID; }
            set { _DBPeopleID = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }        
        }
    }

    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://www.spaarks.com/DBAccess")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Request : System.Web.Services.WebService
    {
        private const string GenericErrorMessage = "Sorry, the service you require has encountered a problem, please contact your system administrator";

        #region HRID/ Person Search
        [WebMethod]
        public DBAccessPerson[] FindDBAccessPerson(string searchString, string costCentre)
        {
            List<vw_HRPerson> people = Director.GetPersonArray(searchString,false, costCentre);

            if(people.Count()==0)
                return new DBAccessPerson[0];

            return people.Select(a => 
                                    new DBAccessPerson 
                                    { 
                                        DBPeopleID = a.dbPeopleID, 
                                        Name = a.FullName, 
                                        Email = a.EmailAddress,
                                        DisplayName = a.FullName + " | " + a.EmailAddress + " | " + a.dbPeopleID
                                    }
                                ).ToArray();
        }

        [WebMethod]
        public DBAccessPerson GetDBAccessPerson(int dbPeopleID)
        {
            HRPerson person = Director.GetPersonById(dbPeopleID);

            if (person == null)
                return null;

            return new DBAccessPerson { 
                                            DBPeopleID = person.dbPeopleID, 
                                            Name = person.Forename + " " + person.Surname, 
                                            Email = person.EmailAddress,
                                            DisplayName = person.Forename + " " + person.Surname + " | " + person.EmailAddress + " | " + person.dbPeopleID
                                      };
        }
        #endregion

        #region Lookups
        [WebMethod]
        //Returns the building object associated with this access area
        public DBAccessBuilding GetBuildingByAccessArea(int accessAreaID)
        {
            try
            {
                AccessArea accessArea = Director.GetAccessArea(accessAreaID);
                if (accessArea == null)
                    throw new ApplicationException("Couldn't find an access area with this ID:" + accessAreaID);

                if (!accessArea.BuildingID.HasValue)
                    throw new ApplicationException("No building associated with this access area ID:" + accessAreaID);

                LocationBuilding building = Director.GetBuilding(accessArea.BuildingID.Value);

                LocationCity city = Director.GetCity(building.CityID);

                DBAccessBuilding returnBuilding = new DBAccessBuilding();
                returnBuilding.BuildingName = building.Name;
                returnBuilding.ID = building.BuildingID;
                returnBuilding.CityID = building.CityID;
                returnBuilding.CityName = city.Name;

                List<DBAccessAccessArea> accessAreaList = new List<DBAccessAccessArea>();

                List<AccessArea> accessAreas = Director.GetAccessAreasForBuilding(building.BuildingID, true);

                foreach (AccessArea aa in accessAreas)
                {
                    DBAccessAccessArea returnAccessArea = new DBAccessAccessArea();
                    returnAccessArea.ID = aa.AccessAreaID;
                    returnAccessArea.AccessAreaName = aa.Name;
                    returnAccessArea.IsGeneralArea = aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.General;

                    if (aa.FloorID == null || aa.FloorID < 1)
                    {
                        returnAccessArea.FloorID = 0;
                        returnAccessArea.FloorName = string.Empty;
                    }
                    else
                    {
                        LocationFloor accessAreaFloor = Director.GetFloor(aa.FloorID.Value);
                        returnAccessArea.FloorID = accessAreaFloor.FloorID;
                        returnAccessArea.FloorName = accessAreaFloor.Name;
                    }

                    accessAreaList.Add(returnAccessArea);
                }

                returnBuilding.AccessAreas = accessAreaList.ToArray();

                return returnBuilding;
            }
            catch (ApplicationException ae)
            {
                LogHelper.HandleException(ae);
                throw ae;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                throw ex;
            }
        }

        [WebMethod]
        public string GetRegionsAllEnabled(out string[] regions)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<LocationRegion> locationRegions = Director.GetRegionsAllEnabled();

                regions = locationRegions.Select(a => a.RegionID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                regions = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public string GetCountriesByRegion(int regionID, out string[] countries)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<LocationCountry> locationCountries = Director.GetCountriesEnabledForRegion(regionID);

                countries = locationCountries.Select(a => a.CountryID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                countries = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public string GetCitiesByCountry(int countryID, out string[] cities)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<LocationCity> locationCities = Director.GetCitiesEnabledForCountry(countryID);

                cities = locationCities.Select(a => a.CityID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch(Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                cities = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public string GetBuildingsByCity(int cityID, out string[] buildings)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<LocationBuilding> locationBuildings = Director.GetBuildingsForCity(cityID, true);

                buildings = locationBuildings.Select(a => a.BuildingID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                buildings = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public string GetFloorsByBuilding(int buildingID, out string[] floors)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<LocationFloor> locationFloors = Director.GetFloorsForBuilding(buildingID, true, false);

                floors = locationFloors.Select(a => a.FloorID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                floors = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public string GetAccessAreasByFloor(int floorID, out string[] accessAreas)
        {
            DBAccessResponse response = new DBAccessResponse();
            response.Success = true;

            try
            {
                List<AccessArea> accessAreaList = Director.GetAccessAreasForFloorAndBuildingGeneralAreas(floorID, true);

                accessAreas = accessAreaList.Select(a => a.AccessAreaID + "|" + a.Name).ToArray();

                return XmlHelper.ToXml(response);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                response.Success = false;
                accessAreas = new string[0];
                return XmlHelper.ToXml(response);
            }
        }

        [WebMethod]
        public DBAccessDBMovesMatch[] GetAccessAreasByDBMovesFloorID(int dbMovesFloorID)
        {
            List<AccessArea> matchingAccessAreas = Director.GetAccessAreasByDBMovesFloorID(dbMovesFloorID);

            if(matchingAccessAreas.Count==0)
                return null;

            return matchingAccessAreas.Select(a=>new DBAccessDBMovesMatch{AccessAreaName=a.Name, AccessAreaID=a.AccessAreaID}).ToArray();

            //AccessArea aa = matchingAccessAreas.First();

            //LocationCity city = Director.GetCityFromAccessArea(aa.AccessAreaID);
            //if(city==null)
            //    return null;
                
            //if(aa.BuildingID==null || aa.FloorID == null)
            //    return null;

            //LocationBuilding building = Director.GetBuilding(aa.BuildingID.Value);
                
            //LocationFloor floor = Director.GetFloor(aa.FloorID.Value);

            //int accessAreaID = matchingAccessAreas.Count == 1 ? aa.AccessAreaID : 0;

            //return new DBAccessDBMovesMatch { CityID = city.CityID, BuildingID = building.BuildingID, FloorID = floor.FloorID, AccessAreaID=accessAreaID };
        }

        [WebMethod]
        public string[] GetRegions()
        {
            List<LocationRegion> regions = Director.GetRegions(false);
            if(regions==null)
                return new string[0];

            return regions.Select(a => string.Format("RegionID={0};Code={1};Name={2};", a.RegionID, a.Code, a.Name)).ToArray(); 
        }
        #endregion

        #region Badge Request/ Access Area Request
            //ErrorCodes
            //0=Success
            //1=Exception raised by service
            //2=Invalid parameters
            //3=Could not find a valid business area for requestee
            //4=Not enough request approvers for business area
            //5=Not enough access approvers for access area
            //6=Access area is not assigned to a division
            [WebMethod]
            //DivisionID would be used to create access request for all areas in a division in that city but currently just supply a list of accessareaids
            public DBAccessResponse CreateBadgeRequest(Guid guid, int externalSystemRequestID, int externalSystemID,  int[] accessAreaIDs, int? divisionID, string lineManagerEmail, int newStarterDBPeopleID, DateTime startDate, DateTime? endDate)
            {
                DBAccessResponse response = new DBAccessResponse();

                try
                {
                    if (!AuthenticateGuid(guid, response))
                        return response;

                    if (!ValidateParams(accessAreaIDs, divisionID, lineManagerEmail, newStarterDBPeopleID, externalSystemRequestID, response))
                        return response;

                    DBAppUser currentUser = this.GetLineManagerUser(lineManagerEmail);

                    if (!CreateBadge(newStarterDBPeopleID, currentUser, response, externalSystemRequestID, externalSystemID))
                        return response;

                    if (!CreateAdditionalAccessRequest(accessAreaIDs, divisionID, newStarterDBPeopleID, startDate, endDate, currentUser, response, externalSystemRequestID, externalSystemID))
                        return response; 
                }
                catch (Exception ex)
                {
                    response.ErrorCode = 1;
                    response.ErrorMessage = GenericErrorMessage;
                    LogHelper.HandleException(ex);

                    return response; 
                }

                response.Success = true;

                return response; 
            }

        #region helper methods
        //Creates a draft pass request
        //If submitted pass request exists then do nothing
        //If draft pass request exists then submit draft request
        private bool CreateBadge(int newStarterDBPeopleID, DBAppUser currentUser, DBAccessResponse response, int externalSystemRequestID, int externalSystemID)
        {
            IBadgeRequestService badgeRequestService = new BadgeRequestService(currentUser.dbPeopleID);

            int requestMasterID = 0;

            bool success = false;
            bool badgeAlreadyRequested = false;

            if (badgeRequestService.HasActiveBadgeRequest(newStarterDBPeopleID))
                badgeAlreadyRequested = true;
            else
                success = badgeRequestService.CreateNewBadgeRequestForPerson(newStarterDBPeopleID, null, ref requestMasterID,
                    externalSystemRequestID, externalSystemID);

            if (!success)
            {
                if (badgeAlreadyRequested)
                {
                    requestMasterID = Director.GetPassRequestMasterId(newStarterDBPeopleID);

                    if (!badgeRequestService.HasDraftBadgeRequest(newStarterDBPeopleID))
                        return true;//Already has a submitted badge request
                }
                else
                {
                    response.ErrorCode = 1;
                    response.ErrorMessage = "Failed to create badge; ";
                    return false;
                }
            }

            //Possibly called by BMSC. The web API method can be modified to get the values for the below parameters when smartcard facilities will be provided vie BMSC
            bool isSmartCard = false;
            string smartCardJustification = "";

            success = badgeRequestService.SubmitBadgeRequest(requestMasterID, DBAccess.Model.Enums.DeliveryOption.Contact,
                    currentUser.Email + "," + currentUser.Telephone, "Created by BMSC", currentUser.Forename, currentUser.Email, isSmartCard, smartCardJustification);

            if (!success)
            {
                response.ErrorCode = 1;
                response.ErrorMessage = "Failed to submit badge request; ";
                return false;
            }

            return true;
        }

			private bool CreateAdditionalAccessRequest
				(
					int[] accessAreaIDs,
					int? divisionID,
					int newStarterDBPeopleID,
					DateTime? startDate,
					DateTime? endDate,
					DBAppUser currentUser,
					DBAccessResponse response,
					int externalSystemRequestID,
					int externalSystemID
				)
			{
				int? submitRequestErrorCode = 0;

				int requestMasterID = 0;
				bool success = false;

                Director director = new Director(currentUser.dbPeopleID);

				List<DBAccessResponse.AccessAreaStatus> accessAreas = new List<DBAccessResponse.AccessAreaStatus>();

				foreach (int accessAreaID in accessAreaIDs)
				{
					//Check if request exists already
					if (Director.DoesPendingAccessRequestExist(newStarterDBPeopleID, accessAreaID))
					{
                        director = new Director(currentUser.dbPeopleID);

						//If there is only a draft then submit it
						requestMasterID = Director.DoesOnlyDraftExist(newStarterDBPeopleID, accessAreaID);
						if (requestMasterID != 0)
						{
							string externalSystem = Director.GetExternalSystem(externalSystemID).Name;
							string comment = "Created by " + externalSystem;
							success = director.SubmitRequest(requestMasterID, comment, currentUser.Email, currentUser.Forename, ref submitRequestErrorCode);

							if (success)
								continue;
							if (submitRequestErrorCode.HasValue && (submitRequestErrorCode == 2 || submitRequestErrorCode == 3 || submitRequestErrorCode == 4))
							{
								if (submitRequestErrorCode == 2)
									response.ErrorCode = 3;
								else if (submitRequestErrorCode == 3)
									response.ErrorCode = 4;
								else if (submitRequestErrorCode == 4)
									response.ErrorCode = 5;

								response.ErrorMessage = "Failed to submit existing draft request; ";

								return false;
							}
							else
							{
								response.ErrorCode = 1;
								response.ErrorMessage = "Failed to submit existing draft request; ";
								return false;
							}
						}

						vw_AccessRequest existingRequest = Director.GetLatestAccessRequest(newStarterDBPeopleID, accessAreaID);
						accessAreas.Add(new DBAccessResponse.AccessAreaStatus { ID = existingRequest.AccessAreaID.Value, AccessAreaName = existingRequest.AccessArea, Status = existingRequest.RequestStatus });
						continue;
					}

					//Add person to request
                    IAccessRequestService accessRequestService = new AccessRequestService(currentUser.dbPeopleID);
                    success = accessRequestService.CreateNewRequestForPerson(newStarterDBPeopleID, ref requestMasterID);

					if (!success)
					{
						response.ErrorCode = 1;
						response.ErrorMessage = "Could not create person for access area request; ";
						return false;
					}

					var city = Director.GetCityFromAccessArea(accessAreaID);

					if (city == null)
					{
						response.ErrorCode = 1;
						response.ErrorMessage = "Could not find a city for this access area; ";
						return false;
					}

					if (!divisionID.HasValue)
						divisionID = -1;

					//Add access area to request
					int? createAccessAreaErrorCode = null;

                    success = accessRequestService.CreateAccessAreaRequest(accessAreaID, requestMasterID, startDate, endDate);

                    if (!success)
					{
						if (createAccessAreaErrorCode.HasValue && createAccessAreaErrorCode == 2)
						{
							if (createAccessAreaErrorCode == 2)
								response.ErrorCode = 6;

							response.ErrorMessage = "Failed to create access area request; ";

							return false;
						}
						else
						{
							response.ErrorCode = 1;
							response.ErrorMessage = "Could not create access area request; ";
							return false;
						}
					}

					//Submit Request
					success = director.SubmitRequest(requestMasterID, "Created by BMSC", currentUser.Email, currentUser.Forename, ref submitRequestErrorCode);

					if (!success)
					{
						if (submitRequestErrorCode.HasValue && (submitRequestErrorCode == 2 || submitRequestErrorCode == 3 || submitRequestErrorCode == 4))
						{
							if (submitRequestErrorCode == 2)
								response.ErrorCode = 3;
							else if (submitRequestErrorCode == 3)
								response.ErrorCode = 4;
							else if (submitRequestErrorCode == 4)
								response.ErrorCode = 5;

							response.ErrorMessage = "Failed to submit existing draft request; ";

							return false;
						}
						else
						{
							response.ErrorCode = 1;
							response.ErrorMessage = "Failed to submit existing draft request; ";
							return false;
						}
					}

					vw_AccessRequest newRequest = Director.GetLatestAccessRequest(newStarterDBPeopleID, accessAreaID);
					accessAreas.Add(new DBAccessResponse.AccessAreaStatus { ID = newRequest.AccessAreaID.Value, AccessAreaName = newRequest.AccessArea, Status = newRequest.RequestStatus });
				}

				response.AccessAreaStatusArray = accessAreas.ToArray();

				return true;
			}

            private bool AuthenticateGuid(Guid guid, DBAccessResponse response)
            {
                try
                {
                    Guid? serviceGuid = Director.GetServiceGuid();
                    return serviceGuid.HasValue && serviceGuid == guid;
                }
                catch
                {
                    response.ErrorCode = 2;
                    response.ErrorMessage = "Failed to authenticate; ";
                    return false;
                }
            }

            private bool ValidateParams(int[] accessAreaIDs, int? divisionID, string requesterEmail, int newStarterDBPeopleID, int bmscID, DBAccessResponse response)
            {
                try
                {
                    foreach (int accessAreaID in accessAreaIDs)
                    {
                        if (accessAreaID < 1)
                        {
                            response.ErrorCode = 2;
                            response.ErrorMessage = "accessAreaID param is invalid; ";
                            return false;
                        }
                        else if (Director.GetAccessArea(accessAreaID) == null)
                        {
                            response.ErrorCode = 2;
                            response.ErrorMessage = string.Format("No access areas exist in DBAccess that match param accessAreaID, value={0}; ", accessAreaID);
                            return false;
                        }
                    }

                    if (divisionID.HasValue && Director.GetDivision(divisionID.Value) == null)
                    {
                        response.ErrorCode = 2;
                        response.ErrorMessage = "No divisions exist in DBAccess that match param divisionID; ";
                        return false;
                    }

                    if (!DBSqlMembershipProvider.ValidateEmailAddress(requesterEmail))
                    {
                        response.ErrorCode = 2;
                        response.ErrorMessage = "requesterEmail param is invalid; ";
                        return false;
                    }

                    if (newStarterDBPeopleID < 1)
                    {
                        response.ErrorCode = 2;
                        response.ErrorMessage = "newStarterDBPeopleID param is invalid; ";
                        return false;
                    }
                    else if (Director.GetPersonById(newStarterDBPeopleID) == null)
                    {
                        response.ErrorCode = 2;
                        response.ErrorMessage = "No people exist in DBAccess that match param newStarterDBPeopleID; ";
                        return false;
                    }

                    if (bmscID < 1)
                    {
                        response.ErrorCode = 2;
                        response.ErrorMessage = "bmscID param is invalid; ";
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    response.ErrorCode = 2;
                    response.ErrorMessage = GenericErrorMessage;
                    LogHelper.HandleException(ex);

                    return false;
                }

                return true;
            }
            #endregion 
        #endregion

        #region Common
        private DBAppUser GetLineManagerUser(string email)
        {
            DBSqlMembershipProvider DBSqlMembershipProvider = new DBSqlMembershipProvider();

            DBAppUser user = DBSqlMembershipProvider.GetDBAppUser(email);
            
            if (user == null)
            {
                throw new ApplicationException("Failed to create user for access request");
            }
           

            return user;
        }
        #endregion
    }
}