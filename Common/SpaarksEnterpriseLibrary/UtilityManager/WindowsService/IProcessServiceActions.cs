﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Spaarks.Common.UtilityManager.WindowsService
{
	public interface IProcessServiceActions
	{
		void ProcessServiceActions(EventLog log, string appName);
		void WriteEventToWindowsLog(string app, string logEntry, EventLogEntryType type);
	}
}
