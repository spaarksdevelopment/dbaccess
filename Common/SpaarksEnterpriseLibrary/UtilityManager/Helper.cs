using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Helper
/// </summary>
namespace Spaarks.Common.UtilityManager
{
    public static class Helper
    {
        /// <summary>
        /// validates a number
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>true or false</returns>
        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        /// <summary>
        /// validates a date
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>true or false</returns>
        public static bool IsDate(object Expression)
        {
            string anyString;
            if (Expression == null)
            {
                return false;
            }
            else
            {
                anyString = Convert.ToString(Expression);
            }
            if (anyString.Length > 0)
            {                
                try
                {
                    DateTime d = DateTime.Parse(anyString);
                }
                catch
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// safely converts to an integer
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>an integer or 0</returns>
        public static int SafeInt(object Expression)
        {
            if (Expression == null)
            {
                return 0;
            }
            else if (IsNumeric(Expression))
            {
                return Convert.ToInt32(Expression);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// safely converts to a nullable integer
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>an integer or 0</returns>
        public static int? SafeIntNullable(object Expression)
        {
            if (Expression == null)
            {
                return null;
            }
            else if (IsNumeric(Expression))
            {
                return Convert.ToInt32(Expression);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// safely converts to a short
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>an integer or 0</returns>
        public static short SafeShort(object Expression)
        {
            if (Expression == null)
            {
                return 0;
            }
            else if (IsNumeric(Expression))
            {
                return Convert.ToInt16(Expression);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// safely converts to a nullable short
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>an integer or 0</returns>
        public static short? SafeShortNullable(object Expression)
        {
            if (Expression == null)
            {
                return null;
            }
            else if (IsNumeric(Expression))
            {
                return Convert.ToInt16(Expression);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// safely converts to a decimal
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>a decimal or 0</returns>
        public static Decimal SafeDecimal(object Expression)
        {
            if (Expression == null)
            {
                return 0m;
            }
            else if (Expression.ToString()=="0.0000")
            {
                return 0m;
            }
            else if (IsNumeric(Expression))
            {
                decimal d = Convert.ToDecimal(Expression);
                return Math.Round(d,2);
            }
            else
            {
                return 0m;
            }
        }

        /// <summary>
        /// safely converts to a decimal
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>a decimal or 0</returns>
        public static Decimal? SafeDecimalNullable(object Expression)
        {
            if (Expression == null)
            {
                return null;
            }
            else if (Expression.ToString() == "0.0000")
            {
                return 0m;
            }
            else if (IsNumeric(Expression))
            {
                decimal d = Convert.ToDecimal(Expression);
                return Math.Round(d, 2);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// safely converts to a date
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>a date object, set to minimum date if supplied expression is not valid</returns>
        public static DateTime SafeDateTime(object Expression)
        {
            if (Expression == null)
            {
                return DateTime.MinValue;
            }
            else if (IsDate(Expression))
            {
                return Convert.ToDateTime(Expression);
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// safely converts to a date (without time), and output as a string
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>a date string, set to minimum date if supplied expression is not valid</returns>
        public static string SafeDateString(object Expression)
        {
            return SafeDateString(Expression, false);
        }

        /// <summary>
        /// safely converts to a date, and output as a string
        /// </summary>
        /// <param name="Expression">input</param>
        /// <param name="WithTime">whether to include the time</param>
        /// <returns>a date string, set to minimum date if supplied expression is not valid</returns>
        public static string SafeDateString(object Expression, bool WithTime)
        {
            if (Expression == null)
            {
                return string.Empty;
            }
            else if (IsDate(Expression))
            {
                DateTime theDate = Convert.ToDateTime(Expression);
                if (WithTime)
                    return DateStringWithTime(theDate, true);
                else
                    return DateString(theDate, true);
            }
            else
            {
                return string.Empty;
            }          
        }


        /// <summary>
        /// safely converts null to string
        /// </summary>
        /// <param name="Expression">input</param>
        /// <returns>an empty string if expression is null</returns>
        public static string SafeString(object Expression)
        {
            return SafeString(Expression, string.Empty);
        }

        /// <summary>
        /// safely converts null to string
        /// </summary>
        /// <param name="Expression">input</param>
        /// <param name="returnValue">value to return if input is null</param>
        /// <returns>an empty string if expression is null</returns>
        public static string SafeString(object Expression, string returnValue)
        {
            if (Expression == null)
                return returnValue;
            else
                return Expression.ToString();
        }

        public static bool SafeBool(object Expression)
        {
            string sExpression = Expression.ToString().ToLower();
            bool bReturn = false;

            switch (sExpression)
            {
                case "true":
                    bReturn = true;
                    break;
                case "false":
                    bReturn = false;
                    break;
                case "-1":
                    bReturn = true;
                    break;
                case "1":
                    bReturn = true;
                    break;
                case "0":
                    bReturn = false;
                    break;
                case "yes":
                    bReturn = true;
                    break;
                case "no":
                    bReturn = false;
                    break;
            }

            return bReturn;
        }

        /// <summary>
        /// Converts a date object to a string in the default format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <returns>"dd/mmm/yy"</returns>
        public static string DateString(DateTime? theDateTime)
        {
            if (theDateTime == null) return string.Empty;
            else
            {
                return theDateTime.Value.ToString("dd-MMM-yyyy");
            }
        }

        /// <summary>
        /// Converts a date object to a string in the default format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <returns>"dd/mmm/yy"</returns>
        public static string DateStringWithTime(DateTime? theDateTime)
        {
            if (theDateTime == null) return string.Empty;
            else return theDateTime.Value.ToString("dd-MMM-yyyy HH:mm");
        }

        /// <summary>
        /// Converts a date object to a string in the default format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <returns>"dd/mmm/yy"</returns>
        public static string DateStringWithTime(DateTime? theDateTime, bool BlankMinValue)
        {
            if (theDateTime == null) return string.Empty;
            else if (BlankMinValue && theDateTime.Value == DateTime.MinValue)
                return string.Empty;
            else
                return theDateTime.Value.ToString("dd-MMM-yyyy HH:mm");
        }


        /// <summary>
        /// Converts a date object to a string in the supplied format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <param name="theFormat">the date format to use (dont append time format)</param>
        /// <returns>date in supplied format with time (HH:mm)</returns>
        public static string DateStringWithTime(DateTime? theDateTime, string theFormat)
        {
            if (theDateTime == null) return string.Empty;
            else return theDateTime.Value.ToString(theFormat + "HH:mm");
        }
        /// <summary>
        /// Converts a date object to a string in the default format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <param name="BlankMinValue">set to true if you want minimum date (1/1/1900) to return an empty string</param>
        /// <returns>"dd/mmm/yy" or ""</returns>
        public static string DateString(DateTime? theDateTime, bool BlankMinValue)
        {
            if (theDateTime == null) return string.Empty;
            else if (BlankMinValue && theDateTime.Value == DateTime.MinValue)
                return string.Empty;
            else
                return theDateTime.Value.ToString("dd-MMM-yyyy");
        }

        /// <summary>
        /// Converts a date object to a string in the supplied format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <param name="theFormat">the date format to use (dont append time format)</param>
        /// <returns>date in supplied format</returns>
        public static string DateString(DateTime? theDateTime, string theFormat)
        {
            if (theDateTime == null) return string.Empty;
            else
            {
                return theDateTime.Value.ToString(theFormat);
            }
        }


        /// <summary>
        /// Converts a date object to a string in the supplied format
        /// </summary>
        /// <param name="theDateTime">the date</param>
        /// <param name="theFormat">the date format to use (dont append time format)</param>
        /// <param name="BlankMinValue">set to true if you want minimum date (1/1/1900) to return an empty string</param>
        /// <returns>date in supplied format</returns>
        public static string DateString(DateTime? theDateTime, string theFormat, bool BlankMinValue)
        {
            if (theDateTime == null) return string.Empty;
            else if (BlankMinValue && theDateTime.Value == DateTime.MinValue)
                return string.Empty;
            else
                return theDateTime.Value.ToString("dd-MMM-yyyy");
        }


        /// <summary>
        /// Converts a decimal monetary value to a formatted string
        /// </summary>
        /// <param name="d">the decimal value</param>
        /// <param name="Prefix">a prefix, such as a currency symbol</param>
        /// <param name="Suffix">a suffix, such as a currency code</param>
        /// <returns></returns>
        public static string CurrencyString(decimal d, string Prefix, string Suffix)
        {
            string output = d.ToString("#,###.00");
            if (Prefix != string.Empty)
                output = Prefix + ' ' + output;
            if (Suffix != string.Empty)
                output = output + ' ' + Suffix;
            return output;
        }
            

        /// <summary>
        /// creates a mail to link 
        /// </summary>
        /// <param name="sName">the Name</param>
        /// <param name="sEmailAddress">the Email Address</param>
        /// <returns>a mailto link</returns>
        public static string EmailLink(string sName, string sEmailAddress)
        {
            return "<a href=\"mailto:" + sEmailAddress + "\">" + sName + "</a>";
        }

        /// <summary>
        /// outputs an array of the years between 2 dates
        /// </summary>
        /// <param name="d1">the smaller date</param>
        /// <param name="d2">the larger date</param>
        /// <returns>array of year numbers e.g. 2008,2009,2010</returns>
        public static int[] YearsArray(DateTime d1, DateTime d2)
        {
            List<int> years = new List<int>();
            int x = d1.Year;
            while (x <= d2.Year)
            {
                years.Add(x);
                x++;
            }
            return years.ToArray();
        }

        /// <summary>
        /// Replaces ' with '' for safe SQL statements
        /// </summary>
        /// <param name="sInput">the value</param>
        /// <returns>the value with quotes doubled up</returns>
        public static string SQLFixup(string sInput)
        {
           return sInput.Replace("'", "''");
        }

        /// <summary>
        /// Replaces ' with '' for safe SQL statements
        /// </summary>
        /// <param name="sInput">the value</param>
        /// <param name="Encapsulate">choose to encapsulate the value in quotes</param>
        /// <param name="EndWithComma">choose to place a comma after the value</param>
        /// <returns></returns>
        public static string SQLFixup(string sInput,bool Encapsulate,bool EndWithComma)
        {
            string s = sInput.Replace("'", "''");
            if(Encapsulate) s = "'"+s+"'";
            if(EndWithComma) s+=",";
            return s;
        }

        /// <summary>
        /// Formats date objects for use in SQL statements, and encapsulates in quotes
        /// </summary>
        /// <param name="dIP">the date object</param>
        /// <param name="UseTime">choose to include time potion of the date</param>
        /// <param name="EndWithComma">choose to place a comma after the value</param>
        /// <returns></returns>
        public static string SQLFixup(DateTime dIP,bool UseTime, bool EndWithComma)
        {
            string s;
            if(UseTime)
                s = "'" + dIP.ToString("yyyy-MM-dd HH:mm") + "'";
            else
                s = "'" + dIP.ToString("yyyy-MM-dd") + "'";

            if (EndWithComma) s += ",";

            return s;
        }

        /// <summary>
        /// Strip carriage retruns and Line Breaks from a string, wit the option to trim
        /// </summary>
        /// <param name="sInput">the input string</param>
        /// <param name="trim">trim thse output?</param>
        /// <returns>string without line breaks</returns>
        public static string StripLineBreaks(string sInput, bool trim)
        {
            string sOutput = sInput.Replace("\r\n", string.Empty);
            sOutput = sOutput.Replace("\n", string.Empty);
            if (trim) return sOutput.Trim();
            else return sOutput;
        }

        /// <summary>
        /// Takes an expression written in CamelCase
        /// and splits on uppper case characters
        /// </summary>
        /// <param name="source">'CamelCase' string</param>
        /// <returns>array {'Camel', 'Case'}</returns>
        public static string[] SplitOnUpperCase(string source)
        {
            if (source == null)
                return new string[] { }; //Return empty array.

            if (source.Length == 0)
                return new string[] { "" };

            StringCollection words = new StringCollection();
            int wordStartIndex = 0;

            char[] letters = source.ToCharArray();
            // Skip the first letter. we don't care what case it is.
            for (int i = 1; i < letters.Length; i++)
            {
                if (char.IsUpper(letters[i]))
                {
                    //Grab everything before the current index.
                    words.Add(new String(letters, wordStartIndex, i - wordStartIndex));
                    wordStartIndex = i;
                }
            }

            //We need to have the last word.
            words.Add(new String(letters, wordStartIndex, letters.Length - wordStartIndex));

            //Copy to a string array.
            string[] wordArray = new string[words.Count];
            words.CopyTo(wordArray, 0);
            return wordArray;
        }

        /// <summary>
        /// Takes an expression written in CamelCase
        /// and splits on uppper case characters
        /// </summary>
        /// <param name="source">'CamelCase' string</param>
        /// <returns>'Camel Case' string</returns>
        public static string SplitOnUpperCaseToString(string source)
        {
            return string.Join(" ", SplitOnUpperCase(source));
        }

        /// <summary>
        /// Converts a boolean value to "Yes" or "No"
        /// </summary>
        /// <param name="b">the boolean value</param>
        /// <returns>"Yes" or "No"</returns>
        public static string GetYesNo(bool b)
        { 
            string s;
            if (b) s = "Yes";
            else s = "No";
            return s;
        }

        /// <summary>
        /// Converts a boolean value to "Yes" or "No", or an alternative value for nulls
        /// </summary>
        /// <param name="b">the boolean value</param>
        /// <param name="IfNull">alternative value for nulls</param>
        /// <returns>"Yes" or "No" or an alternative value for nulls</returns>      
        public static string GetYesNo(bool? b, string IfNull)
        {
            string s;
            if (b==true) s = "Yes";
            else if (b==false) s = "No";
            else s = IfNull;
            return s;
        }
                      
        /// <summary>
        /// Replace line breaks with an HTML line break
        /// </summary>
        /// <param name="sInput">the input string with line breaks</param>
        /// <returns>string with line breaks replaced</returns>
        public static string MultilineHTML(string sInput)
        {
            string sOutput = sInput.Replace("\r\n", "<br/>");
            sOutput = sOutput.Replace("\n", "<br/>");
            return sOutput;
        }

        /// <summary>
        /// Replace line breaks with an HTML line break
        /// </summary>
        /// <param name="sInput">the input string with line breaks '<br>' for display in lotus notes</param>
        /// <returns>string with line breaks replaced</returns>
        public static string MultilineHTMLForEmail(string sInput)
        {
            string sOutput = sInput.Replace("\r\n", "<br>");
            sOutput = sOutput.Replace("\n", "<br>");
            return sOutput;
        }

        /// <summary>
        /// Pad a number with leading 0s
        /// </summary>
        /// <param name="sInput">the number as a string</param>
        /// <returns>string with leading 0s</returns>
        public static string PadNumber(string sInput, short length)
        {
            while (sInput.Length < length)
            {
                sInput = "0" + sInput;
            }
            
            return sInput;
        }


        /// <summary>
        ///  Validate expression using a regex - case is ignored
        /// </summary>
        /// <param name="expression">the expression to validate</param>
        /// <param name="regex">the regex</param>
        /// <returns></returns>
        public static bool ValidateRegex(string expression, string regex)
        {
            return ValidateRegex(expression, regex, true);
        }

        /// <summary>
        /// Validate expression using a regex
        /// </summary>
        /// <param name="expression">the expression to validate</param>
        /// <param name="regex">the regex</param>
        /// <param name="ignoreCase">whether or not case should be ignored</param>
        /// <returns></returns>
        public static bool ValidateRegex(string expression, string regex, bool ignoreCase)
        {
            Regex re;
            if(ignoreCase) 
                re = new Regex(regex, RegexOptions.IgnoreCase);
            else
                re = new Regex(regex);

            if (re.IsMatch(expression)) { return true; } else { return false; }
        }

        /// <summary>
        /// Validates the string passed in as a valid email address and returns a boolean; throws a handled exception if invalid
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        public static bool ValidateEmailAddress(string EmailAddress)
        {
            string strRegex = @"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(EmailAddress)) { return true; } else { return false; }
        }

        /// <summary>
        /// Splits and validates the string passed in as valid email addresses and returns a boolean; throws a handled exception if invalid
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        /// <param name="Delimiter">The value to split the string by</param>
        public static bool ValidateMultipleEmailAddress(string EmailAddress, Char[] delimiter)
        {
            string strRegex = @"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$";
            string[] strArr = EmailAddress.Split(delimiter);
            foreach (string s in strArr)
            {
                if (s.Trim() != "")
                {
                    Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
                    if (!re.IsMatch(s.Trim())) { return false; }
                }
            }
            return true;
        }

        /// <summary>
        /// Validates the string passed in as a valid telephone number and returns a boolean; can include the following special characters '+()'
        /// </summary>
        /// <param name="TelephoneNumber">The telephone number to validate</param>
        public static bool ValidatePhoneNumber(string TelephoneNumber)
        {
            string strRegex = @"^(\(|\d|\)|\+|\-)*$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(TelephoneNumber)) { return true; } else { return false; }
        }

        /// <summary>
        /// Dlimits a list of ints
        /// </summary>
        /// <param name="theList">the list of int</param>
        /// <param name="Delimiter">the delimter to use</param>
        /// <returns>a delimited string</returns>
        public static string DelimitList(List<int> theList, string Delimiter)
        {
            string output = string.Empty;

            foreach (int item in theList)
            {
                output += Delimiter + item;
            }

            output = output.Substring(Delimiter.Length);

            return output;
        }

        /// <summary>
        /// Dlimits a list of shorts
        /// </summary>
        /// <param name="theList">the list of short</param>
        /// <param name="Delimiter">the delimter to use</param>
        /// <returns>a delimited string</returns>
        public static string DelimitList(List<short> theList, string Delimiter)
        {
            string output = string.Empty;

            foreach (short item in theList)
            {
                output += Delimiter + item;
            }

            output = output.Substring(Delimiter.Length);

            return output;
        }

        /// <summary>
        /// Dlimits a list of strings
        /// </summary>
        /// <param name="theList">the list of strings</param>
        /// <param name="Delimiter">the delimter to use</param>
        /// <returns>a delimited string</returns>
        public static string DelimitList(List<string> theList, string Delimiter, string encapsulateCharacter)
        {
            string output = string.Empty;

            foreach (string item in theList)
            {
                if (encapsulateCharacter.Length>0)
                {
                    output += Delimiter + encapsulateCharacter + item + encapsulateCharacter;
                }
                else
                    output += Delimiter + item;
            }

            output = output.Substring(Delimiter.Length);

            return output;
        }


        /// <summary>
        /// Turns a string in any case to Title Case
        /// </summary>
        /// <param name="mText">The string</param>
        /// <returns>the string, in title case</returns>
        public static string ToTitleCase(string mText)
        {
            string rText = "";
            try
            {
                System.Globalization.CultureInfo cultureInfo =
                System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Globalization.TextInfo TextInfo = cultureInfo.TextInfo;
                rText = TextInfo.ToTitleCase(mText);
            }
            catch
            {
                rText = mText;
            }
            return rText;
        }

        
    }
}