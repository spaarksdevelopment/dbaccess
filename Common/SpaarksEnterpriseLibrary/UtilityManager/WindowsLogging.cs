﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Configuration;

namespace Spaarks.Common.UtilityManager
{
	public class WindowsLogging
	{
		public static void LogError(Exception exception)
		{
			try
			{
				StringBuilder sb = new StringBuilder();
				AppendExceptionText(exception, ref sb);
				string exceptionText = DateTime.Now.ToString("{dd/MM/yyyy HH:mm:ss}") + sb.ToString();

				using (FileStream fs = new FileStream(GetTodaysLogPath().FullName, FileMode.OpenOrCreate | FileMode.Append))
				{
					//FileInfo todaysLog = CreateTodaysLog();

					using (StreamWriter sw = new StreamWriter(fs))
					{
						sw.WriteLine(exceptionText);
					}
				}
			}
			catch
			{
				//Error Logger is bawsed - What are you gonna do?
			}

			depth = 0;
		}

		static int depth = 0;

		private static void AppendExceptionText(Exception ex, ref StringBuilder sb)
		{
			depth++;
			if (depth > 32)
				return;

			sb.Append(string.Format("Message={0};StackTrace={1}", ex.Message, ex.StackTrace));

			if (ex.InnerException != null)
				AppendExceptionText(ex.InnerException, ref sb);
		}

		private static FileInfo GetTodaysLogPath()
		{
			string Location = ConfigurationManager.AppSettings["Location"];				
			string errorLogPath = Path.Combine(Location, "WebsiteLogs");
			DirectoryInfo yearDirectory = CreateDirectory(errorLogPath, DateTime.Now.Year.ToString());
			DirectoryInfo monthDirectory = CreateDirectory(yearDirectory.FullName, DateTime.Now.Month.ToString());

			FileInfo fi = new FileInfo(Path.Combine(monthDirectory.FullName, DateTime.Now.Day.ToString()) + ".txt");

			//if (!fi.Exists)
			//    fi.Create();

			return fi;
		}

		private static DirectoryInfo CreateDirectory(string parent, string name)
		{
			DirectoryInfo di = new DirectoryInfo(Path.Combine(parent, name));
			if (!di.Exists)
				di.Create();

			return di;
		}
	}
}