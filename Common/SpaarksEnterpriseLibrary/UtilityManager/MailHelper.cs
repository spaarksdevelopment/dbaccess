using System;
using System.Configuration;
using System.Net.Mail;
using Spaarks.Common.UtilityManager.Mail;

namespace Spaarks.Common.UtilityManager
{
    /// <summary>
    /// MailManager all functions to do with emailing
    /// </summary>
	public static class MailHelper
	{
		//Default to using DBDevMailer
		private static IMailer _Mailer = new DBDevMailer();

		public static IMailer Mailer
		{
			get { return _Mailer; }
			set { _Mailer = value; }
		}

        /// <summary>
        /// Sends an email to one recipient
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        public static void SendEmailWithTestCheck(string Recipient, string Subject, string Body, bool IsHTML)
        {
            MailMessage mm = new MailMessage();

            if (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE")
            {
                //add original recipient to subject line, and replace with test email address
                Subject = Subject + " (recipient: " + Recipient + ")";
                string TestRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

                if (TestRecipient.Contains(","))
                {
                    string[] TestRecipients = TestRecipient.Split(Convert.ToChar(","));
                    foreach (string sTestRecipient in TestRecipients)
                    {
                        mm.To.Add(new MailAddress(sTestRecipient));
                    }
                }
                else
                {
                    mm.To.Add(new MailAddress(TestRecipient));
                }
            }
            else
            {
                mm.To.Add(new MailAddress(Recipient));
            }

            //check if a BCC recipient is specified for the site
            string BCCRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];
            if (BCCRecipient != null)
            {
                if (BCCRecipient != string.Empty)
                {
                    if (BCCRecipient.Contains(","))
                    {
                        string[] BCCRecipients = BCCRecipient.Split(Convert.ToChar(","));
                        foreach (string sBCCRecipient in BCCRecipients)
                        {
                            mm.Bcc.Add(new MailAddress(sBCCRecipient));
                        }
                    }
                    else
                    {
                        mm.Bcc.Add(new MailAddress(BCCRecipient));
                    }
                }
            }


             string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
             string SenderName = ConfigurationManager.AppSettings["SenderName"];
            

            MailAddress maSender = new MailAddress(SenderEmailAddress, SenderName);

            mm.From = maSender;
            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = IsHTML;

            try
            {
                SmtpClient smtp = new SmtpClient();

                //this is only here to enable relaying through gmail for test.
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPEnableSsl"]);
                //smtp.EnableSsl = false;

                smtp.Send(mm);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

        }

		/// <summary>
		/// Sends an email to one recipient
		/// </summary>
		/// <param name="recipient">the recipeint email address</param>
		/// <param name="subject">the subject</param>
		/// <param name="body">the mail body</param>
		/// <param name="isHTML">true if body contains html</param>
		/// <param name="senderEmailAddress">Sender Email Address</param>
		/// <param name="senderName">Sender Name</param>
		public static void SendEmail(string recipient, string subject, string body, bool isHTML, string senderEmailAddress, string senderName)
		{
			MailMessage mm = new MailMessage();          

            string[] recipients = recipient.Split(',');

			string bccRecipient = string.Empty;

			if (ConfigurationManager.AppSettings["BCCEmailAddress"] != null)
				bccRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];

			subject = MailHelperMethods.GetRecipients(ref recipients, string.Empty, bccRecipient, subject, mm);

			if (senderEmailAddress == string.Empty)
			{
				senderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
				senderName = ConfigurationManager.AppSettings["SenderName"];
			}

			MailAddress maSender = new MailAddress(senderEmailAddress, senderName);

			mm.From = maSender;
			mm.Subject = subject;
			mm.Body = body;
			mm.IsBodyHtml = isHTML;

			try
			{
				Mailer.SendMail(mm);
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		/// <summary>
		/// Sends an email to one recipient
		/// </summary>
		/// <param name="Recipient">the recipeint email address</param>
		/// <param name="Subject">the subject</param>
		/// <param name="Body">the mail body</param>
		/// <param name="IsHTML">true if body contains html</param>
		public static void SendEmail(string Recipient, string Subject, string Body, bool IsHTML)
		{
			SendEmail(Recipient, Subject, Body, IsHTML, string.Empty, string.Empty);
		}

		/// <summary>
		/// Sends an email to several recients
		/// </summary>
		/// <param name="recipients">the recipeints' email addresses</param>
		/// <param name="subject">the subject</param>
		/// <param name="body">the mail body</param>
		/// <param name="isHTML">true if body contains html</param>
		/// <param name="senderEmailAddress">Sender Email Address</param>
		/// <param name="senderName">Sender Name</param>
		public static void SendEmail(string[] recipients, string subject, string body, bool isHTML, string senderEmailAddress, string senderName)
		{
			if (recipients.Length == 0) return;

			MailAddress maSender = new MailAddress(senderEmailAddress, senderName);
			MailMessage mm = new MailMessage();

			string bccRecipient = string.Empty;

			if (ConfigurationManager.AppSettings["BCCEmailAddress"] != null)
				bccRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];

			subject = MailHelperMethods.GetRecipients(ref recipients, string.Empty, bccRecipient, subject, mm);

			if (senderEmailAddress == string.Empty)
			{
				senderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
				senderName = ConfigurationManager.AppSettings["SenderName"];
			}

			mm.From = maSender;
			mm.Subject = subject;
			mm.Body = body;
			mm.IsBodyHtml = isHTML;

			try
			{
				Mailer.SendMail(mm);
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		/// <summary>
		/// Sends an email to several recients
		/// </summary>
		/// <param name="Recipients">the recipeints' email addresses</param>
		/// <param name="Subject">the subject</param>
		/// <param name="Body">the mail body</param>
		/// <param name="IsHTML">true if body contains html</param>
		public static void SendEmail(string[] Recipients, string Subject, string Body, bool IsHTML)
		{
			SendEmail(Recipients, Subject, Body, IsHTML, string.Empty, string.Empty);
		}

		/// <summary>
		/// Sends an email to one recient without raising exceptions
		/// </summary>
		/// <param name="Recipient">the recipeint email address</param>
		/// <param name="Subject">the subject</param>
		/// <param name="Body">the mail body</param>
		/// <param name="IsHTML">true if body contains html</param>
		/// <param name="SenderEmailAddress">Sender Email Address</param>
		/// <param name="SenderName">Sender Name</param>
		/// <returns>true if email sent, false if not</returns>
		public static bool SafeSendEmail(string Recipient, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
		{
			return SafeSendEmail(Recipient, string.Empty, Subject, Body, IsHTML, SenderEmailAddress, SenderName);
		}

		/// <summary>
		/// Sends an email to one recipient without raising exceptions
		/// </summary>
		/// <param name="recipient">the recipeint email address</param>
		/// <param name="subject">the subject</param>
		/// <param name="body">the mail body</param>
		/// <param name="isHTML">true if body contains html</param>
		/// <param name="senderEmailAddress">Sender Email Address</param>
		/// <param name="senderName">Sender Name</param>
		/// <returns>true if email sent, false if not</returns>
		public static bool SafeSendEmail(string recipient, string ccRecipient, string subject, string body, bool isHTML, string senderEmailAddress, string senderName)
		{
			bool success = false;

			try
			{
				MailMessage mm = new MailMessage();

                string[] recipients = recipient.Split(',');

				string bccRecipient = string.Empty;

				if (ConfigurationManager.AppSettings["BCCEmailAddress"] != null)
					bccRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];

				if (senderEmailAddress == string.Empty)
				{
					senderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
					senderName = ConfigurationManager.AppSettings["SenderName"];
				}

				subject = MailHelperMethods.GetRecipients(ref recipients, ccRecipient, bccRecipient, subject, mm);

				if (senderEmailAddress == string.Empty)
				{
					senderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
					senderName = ConfigurationManager.AppSettings["SenderName"];
				}

				MailAddress maSender = new MailAddress(senderEmailAddress, senderName);

				mm.From = maSender;
				mm.Subject = subject;
				mm.Body = body;
				mm.IsBodyHtml = isHTML;

				success = Mailer.SendMail(mm);
			}
			catch (Exception Ex)
			{
				success = false;
				LogHelper.HandleException(Ex, ExceptionSeverity.High);
			}

			return success;
		}

		/// <summary>
		/// Sends an email to one recient without raising exceptions
		/// </summary>
		/// <param name="Recipient">the recipeint email address</param>
		/// <param name="Subject">the subject</param>
		/// <param name="Body">the mail body</param>
		/// <param name="IsHTML">true if body contains html</param>
		/// <returns>true if email sent, false if not</returns>
		public static bool SafeSendEmail(string Recipient, string Subject, string Body, bool IsHTML)
		{
			return SafeSendEmail(Recipient, Subject, Body, IsHTML, string.Empty, string.Empty);
		}

		/// <summary>
		/// Sends an email to several recients without raising exceptions
		/// </summary>
		/// <param name="Recipients">the recipeints' email addresses</param>
		/// <param name="subject">the subject</param>
		/// <param name="body">the mail body</param>
		/// <param name="isHTML">true if body contains html</param>
		/// <param name="senderEmailAddress">Sender Email Address</param>
		/// <param name="senderName">Sender Name</param>
		/// <returns>true if email sent, false if not</returns>
		public static bool SafeSendEmail(string[] recipients, string subject, string body, bool isHTML, string senderEmailAddress, string senderName)
		{
			if (recipients.Length == 0) return false;

			bool success;

			if (senderEmailAddress == string.Empty)
			{
				senderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
				senderName = ConfigurationManager.AppSettings["SenderName"];
			}

			MailAddress maSender = new MailAddress(senderEmailAddress, senderName);

			MailMessage mm = new MailMessage();

			string bccRecipient = string.Empty;

			if (ConfigurationManager.AppSettings["BCCEmailAddress"] != null)
				bccRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];

			subject = MailHelperMethods.GetRecipients(ref recipients, string.Empty, bccRecipient, subject, mm);

			mm.From = maSender;
			mm.Subject = subject;
			mm.Body = body;
			mm.IsBodyHtml = isHTML;

			try
			{
				success = Mailer.SendMail(mm);
			}
			catch (Exception Ex)
			{
				success = false;
				LogHelper.HandleException(Ex, ExceptionSeverity.High);
			}

			return success;
		}

		/// <summary>
		/// Sends an email to several recients without raising exceptions
		/// </summary>
		/// <param name="Recipients">the recipeints' email addresses</param>
		/// <param name="Subject">the subject</param>
		/// <param name="Body">the mail body</param>
		/// <param name="IsHTML">true if body contains html</param>
		/// <returns>true if email sent, false if not</returns>
		public static bool SafeSendEmail(string[] Recipients, string Subject, string Body, bool IsHTML)
		{
			return SafeSendEmail(Recipients, Subject, Body, IsHTML, string.Empty, string.Empty);
		}
	}
}
