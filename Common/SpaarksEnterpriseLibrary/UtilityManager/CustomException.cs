using System;

namespace Spaarks.Common.UtilityManager
{
    // / <summary>
    // / Standard exception class that provides support to add custom information.
    // / class can be inherited and specific information added by the derived class.
    // / Custom information can still be obtained using the base class <see cref="P:Spaarks.Common.UtilityManager.CustomException">CustomMessages</see> method without any understanding of the derived class
    // / </summary>
    // / <remarks>
    // / <para>No Debug or Trace information is provided this class simply acts a holder for messages</para>
    // / <para>Derived classes should following the naming convention <i>ClassName</i>Exception</para>
    // /	<para>This class should only be inherited if the inherited class will add custom information that is specific to it or a calling process needs to catch the derived type rather than a generic exception.
    // / if neither of these are the case then this base class should be thrown</para>
    // /	<para>Error messages should be descriptive, end user friendly and include any steps that a user can take to rectify the problem</para>
    // /	</remarks>
    public abstract class CustomException : ApplicationException
    {
        // ToDo: Consider developer, support and user properties, possibly walk all properties using reflection
        // ToDo: CurrentStack.GetFrame(1).GetMethod.Name to get calling void name
        // ToDo: Implememnt ISerializable to allow class to be used for remoting
        // ToDo: Add assembly, machine and user information using reflection

        #region Declarations

        private System.Collections.Generic.List<string> _CustomMessages;

        #endregion

        #region Properties

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / Gets the custom messages of this object
        // / </summary>
        // / <value>An array of custom message strings</value>
        // / <remarks>The order of the custom messages follows the order that the information was stored</remarks>
        // /-----------------------------------------------------------------------------
        public System.Collections.Generic.List<string> CustomMessages
        {
            get { return _CustomMessages; }
        }

        #endregion

        #region Constructors

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / Initialises a new instance of the <see cref="T:Spaarks.Common.UtilityManager.CustomException">CustomException</see> class
        // / </summary>
        // / <remarks></remarks>
        // /-----------------------------------------------------------------------------
        protected CustomException()
            : base()
        {
            // Only derived classes should have access
            _InitClass();
        }

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / Initialises a new instance of the <see cref="T:Spaarks.Common.UtilityManager.CustomException">CustomException</see> class with a specified error message
        // / </summary>
        // / <param name="message">A message that describes the error</param>
        // / <remarks></remarks>
        // /-----------------------------------------------------------------------------
        public CustomException(string message)
            : base(message)
        {
            _InitClass();
        }

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / Initializes a new instance of the <see cref="T:Spaarks.Common.UtilityManager.CustomException">CustomException</see> class with a specified error message and a reference to the inner exception that is the cause of this exception
        // / </summary>
        // / <param name="message">A message that describes the error</param>
        // / <param name="innerException">The exception that is the cause of the current exception</param>
        // / <remarks></remarks>
        // /-----------------------------------------------------------------------------
        public CustomException(string message, Exception innerException)
            : base(message, innerException)
        {
            _InitClass();
        }

        #endregion

        #region Methods

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / Allows a string message to be added to the collection of recorded custom messages
        // / </summary>
        // / <param name="message">Mesaage to add to the exception</param>
        // / <remarks>Message will only be added if it is a non-empty string</remarks>
        // /-----------------------------------------------------------------------------
        public void AddCustomMessage(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                if (message.Trim().Length > 0)
                {
                    _CustomMessages.Add(message);
                }
            }
        }

        // /-----------------------------------------------------------------------------
        // / <summary>
        // / <para>Gets exception message and if the exception is of type <see cref="T:Spaarks.Common.UtilityManager.CustomException">CustomException</see> gets the custom messages</para>
        // / <para>Recurses any inner exceptions to get their message and custom messages</para>
        // / <para>Finally adds the stack trace</para>
        // / </summary>
        // / <param name="exception">Exception to get messages for</param>
        // / <param name="seperator">Seperator text to use between messages</param>
        // / <returns>A string of exception messages seperated by the specified text</returns>
        // / <remarks></remarks>
        // /-----------------------------------------------------------------------------
        public static string GetExceptionTree(Exception exception, string seperator)
        {
            try
            {
                System.Text.StringBuilder SB = new System.Text.StringBuilder();

                SB.AppendFormat("{0}{1}", exception.GetType().Name, seperator);

                if (exception.Message.Length == 0)
                {
                    SB.Append("No error message available");
                }
                else
                {
                    SB.Append(exception.Message);
                }

                // Need to check if exception was derived from a custom exception rtaher than the type of the (possibly derived) class
                if (exception.GetType().BaseType == typeof(CustomException))
                {
                    CustomException CustomException = (CustomException)(exception);

                    foreach (string Message in CustomException.CustomMessages)
                    {
                        SB.AppendFormat("{0}{1}", seperator, Message);
                    }
                }

                // Get inner exception message
                if (exception.InnerException != null)
                {
                    SB.Append(GetExceptionTree(exception.InnerException, seperator));
                }

                if (exception.StackTrace.Length > 0)
                {
                    SB.AppendFormat("{0}{1}", seperator, exception.StackTrace);
                }

                return SB.ToString();
            }
            catch
            {
                return "Unable to determine error";
            }
        }

        #endregion

        #region Private Methods

        private void _InitClass()
        {
            _CustomMessages = new System.Collections.Generic.List<string>();
        }

        #endregion
    }
}