﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace Spaarks.Common.UtilityManager.Mail
{
	public class DBDevMailer : IMailer
	{
		public bool SendMail(MailMessage mm)
		{
			string ccRecipient = mm.CC.ToString();
			string bccRecipient = mm.Bcc.ToString();

			// clear out the email of recipients
			// but show their names in the ones we send
			mm.CC.Clear();
			mm.Bcc.Clear();
			//SetRecipients(mm);

			bool testMode = false;

			if (ConfigurationManager.AppSettings["TestMode"] != null)
				testMode = ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE";

			string testRecipient = string.Empty;

			if (ConfigurationManager.AppSettings["TestEmailAddress"] != null)
				testRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

            string[] recipients = new string[mm.To.Count];

            for (int i = 0; i < mm.To.Count; i++)
                recipients[i] = mm.To[i].ToString();

			mm.Subject = MailHelperMethods.GetTestRecipients(ref recipients, ccRecipient, bccRecipient, mm.Subject.ToString(), mm, testRecipient);


            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mm);
            }
            catch (Exception e)
            {
                LogHelper.HandleException(e);
            }

			return true;
		}
	}
}

