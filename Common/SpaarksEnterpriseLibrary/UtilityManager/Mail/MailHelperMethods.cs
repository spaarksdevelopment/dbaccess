﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace Spaarks.Common.UtilityManager.Mail
{
	public static class MailHelperMethods
	{
		public static string GetRecipients(ref string[] recipients, string ccRecipient, string bccRecipient, string subject, MailMessage mm)
		{
			foreach (string sRecipient in recipients)
			{
				mm.To.Add(new MailAddress(sRecipient));
			}

			GetCCRecipients(ccRecipient, mm);

			GetBCCRecipients(bccRecipient, mm);

			return subject;
		}

		public static string GetTestRecipients(ref string[] recipients, string ccRecipient, string bccRecipient, string subject, MailMessage mm, string testRecipient)
		{
			//add original recipients to subject line, and replace with test email address
			subject = GetTestToRecipients(ref recipients, subject, mm, testRecipient);

			subject += GetTestCCRecipients(ccRecipient, mm);

			subject += GetTestBCCRecipients(bccRecipient, mm);

			return subject;
		}

		public static string GetTestToRecipients(ref string[] recipients, string subject, MailMessage mm, string testRecipient)
		{
            if (testRecipient == null || testRecipient == string.Empty || string.IsNullOrEmpty(testRecipient.Trim()))
                return string.Empty;

			string recipientList = string.Empty;
			foreach (string sRecipient in recipients)
			{
				recipientList += ", " + sRecipient;
			}
			recipientList = recipientList.Substring(2);

			subject += " (recipients: " + recipientList + ")";

			mm.To.Clear();

			if (testRecipient.Contains(","))
			{
				string[] testRecipients = testRecipient.Split(Convert.ToChar(","));
				foreach (string sTestRecipient in testRecipients)
				{
					mm.To.Add(new MailAddress(sTestRecipient));
				}
			}
			else
				mm.To.Add(new MailAddress(testRecipient));

			return subject;
		}

		public static string GetTestCCRecipients(string testCCRecipient, MailMessage mm)
		{
            if (testCCRecipient == null || testCCRecipient == string.Empty || string.IsNullOrEmpty(testCCRecipient.Trim()))
                return string.Empty;

			if (string.IsNullOrEmpty(testCCRecipient))
				return string.Empty;

			string ccRecipientList = string.Empty;

			if (testCCRecipient.Contains(","))
			{
				string[] testBCCRecipients = testCCRecipient.Split(Convert.ToChar(","));
				foreach (string r in testBCCRecipients)
				{
					ccRecipientList += ", " + r;
				}

				ccRecipientList = ccRecipientList.Substring(2);
			}
			else
				ccRecipientList = testCCRecipient;

			return " (cc recipients: " + ccRecipientList + ")";
		}

		public static string GetTestBCCRecipients(string testBCCRecipient, MailMessage mm)
		{
            if (testBCCRecipient == null || testBCCRecipient == string.Empty || string.IsNullOrEmpty(testBCCRecipient.Trim()))
                return string.Empty;

			if (string.IsNullOrEmpty(testBCCRecipient))
				return string.Empty;

			string bccRecipientList = string.Empty;

			if (testBCCRecipient.Contains(","))
			{
				string[] testBCCRecipients = testBCCRecipient.Split(Convert.ToChar(","));
				foreach (string r in testBCCRecipients)
				{
					bccRecipientList += ", " + r;
				}

				bccRecipientList = bccRecipientList.Substring(2);
			}
			else
				bccRecipientList = testBCCRecipient;

			return " (bcc recipients: " + bccRecipientList + ")";
		}

		public static void GetCCRecipients(string ccRecipient, MailMessage mm)
		{
			if (!string.IsNullOrEmpty(ccRecipient))
			{
				if (ccRecipient.Contains(","))
				{
					string[] CCRecipients = ccRecipient.Split(Convert.ToChar(","));
					foreach (string sCCRecipient in CCRecipients)
					{
                        if(!string.IsNullOrEmpty(sCCRecipient.Trim()))
						    mm.CC.Add(new MailAddress(sCCRecipient));
					}
				}
				else
				{
                    if (!string.IsNullOrEmpty(ccRecipient.Trim()))
                        mm.CC.Add(new MailAddress(ccRecipient));
                    
				}
			}
		}

		public static void GetBCCRecipients(string bccRecipient, MailMessage mm)
		{
			if (bccRecipient != null)
			{
				if (bccRecipient != string.Empty)
				{
					if (bccRecipient.Contains(","))
					{
						string[] BCCRecipients = bccRecipient.Split(Convert.ToChar(","));
						foreach (string sBCCRecipient in BCCRecipients)
						{
							mm.Bcc.Add(new MailAddress(sBCCRecipient));
						}
					}
					else
					{
                        if (!string.IsNullOrEmpty(bccRecipient.Trim()))
						mm.Bcc.Add(new MailAddress(bccRecipient));
					}
				}
			}
		}
	}
}
