﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Spaarks.Common.UtilityManager.Mail
{
    public class Mailer : IMailer
    {
        public bool SendMail(MailMessage mm)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Send(mm);

            return true;
        }
    }
}
