﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Configuration;

namespace Spaarks.Common.UtilityManager.Mail
{
    public class TestMailer : IMailer
    {
		private bool _OverrideTestMode = false;
		private bool _TestMode = false;

		public TestMailer()
		{
			_OverrideTestMode = false;
			_TestMode = false;
		}

		public TestMailer(bool overrideTestMode, bool testMode)
		{
			_OverrideTestMode = overrideTestMode;
			_TestMode = testMode;
		}

		public string EmailText;

        public bool SendMail(MailMessage mm)
        {
			string ccRecipient = mm.CC.ToString();
			string bccRecipient = mm.Bcc.ToString();

			mm.CC.Clear();
			mm.Bcc.Clear();

			bool testMode = false;

			if (_OverrideTestMode)
				testMode = _TestMode;
			else if (ConfigurationManager.AppSettings["TestMode"] != null)
				testMode = ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE";

            string[] recipients = new string[mm.To.Count];

            for (int i = 0; i < mm.To.Count; i++)
                recipients[i] = mm.To[i].ToString();

			if (testMode)
			{
				string testRecipient = string.Empty;

				if (ConfigurationManager.AppSettings["TestEmailAddress"] != null)
					testRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

				mm.Subject = MailHelperMethods.GetTestRecipients(ref recipients, ccRecipient, bccRecipient, mm.Subject.ToString(), mm, testRecipient);
			}
			else
				mm.Subject = MailHelperMethods.GetRecipients(ref recipients, ccRecipient, bccRecipient, mm.Subject.ToString(), mm);

			EmailText = mm.RawMessage();

            return true;
        }
    }
}
