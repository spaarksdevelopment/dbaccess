﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace Spaarks.Common.UtilityManager.Mail
{
	//Remove the real recipients/ CCs/ BCCs and add them to the subject line
    public class DevMailer : IMailer
    {
        public bool SendMail(MailMessage mm)
        {
			string ccRecipient = mm.CC.ToString();
			string bccRecipient = mm.Bcc.ToString();
            
			mm.CC.Clear();
            mm.Bcc.Clear();
            
            SmtpClient smtp = new SmtpClient();
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            
			string host = "smtp.gmail.com";
 
			if(ConfigurationManager.AppSettings["DevMailerHost"] != null)
				host = ConfigurationManager.AppSettings["DevMailerHost"];

			string userName = "spaarksemailtest@gmail.com";
 
			if(ConfigurationManager.AppSettings["DevMailerUserName"] != null)
				userName = ConfigurationManager.AppSettings["DevMailerUserName"];
            
			string password = "testemail";

			if (ConfigurationManager.AppSettings["DevMailerPassword"] != null)
				password = ConfigurationManager.AppSettings["DevMailerPassword"];

			bool testMode = false;

			if (ConfigurationManager.AppSettings["TestMode"] != null)
				testMode = ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE";

			string testRecipient = string.Empty;

			if (ConfigurationManager.AppSettings["TestEmailAddress"] != null)
				testRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

			string[] recipients = mm.To.ToString().Split(',');

			mm.Subject = MailHelperMethods.GetTestRecipients(ref recipients, ccRecipient, bccRecipient, mm.Subject.ToString(), mm, testRecipient);

			smtp.Host = host;
            smtp.UseDefaultCredentials = false;

			smtp.Credentials = new NetworkCredential(userName, password);
            smtp.EnableSsl = true; //required to use gmail;

            smtp.Send(mm);

            return true;
        }
    }
}
