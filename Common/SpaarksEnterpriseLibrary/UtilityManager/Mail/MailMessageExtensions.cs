﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Reflection;

namespace Spaarks.Common.UtilityManager.Mail
{
	public static class MailMessageExtensions
	{
		private static readonly BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
		private static readonly Type mailWriter = typeof(SmtpClient).Assembly.GetType("System.Net.Mail.MailWriter");
		private static readonly ConstructorInfo mailWriterConstructor = mailWriter.GetConstructor(flags, null, new[] { typeof(Stream) }, null);
		private static readonly MethodInfo closeMethod = mailWriter.GetMethod("Close", flags);
		private static readonly MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", flags);

		public static string RawMessage(this MailMessage m)
		{
			MemoryStream stream = new MemoryStream();
			var mailWriter = mailWriterConstructor.Invoke(new object[] { stream });
			sendMethod.Invoke(m, flags, null, new[] { mailWriter, true }, null);

			string result = string.Empty;

			using (var emailStream = stream)
			{
				stream.Position = 0;
				StreamReader reader = new StreamReader(emailStream);
				result = reader.ReadToEnd();
			}
			return result;
		}
	}
}
