using System;

namespace Spaarks.Common.UtilityManager
{
  /// <summary>
  /// Holds information about a list item
  /// </summary>
  /// <remarks></remarks>
  public class AduvoListItem
  {
    #region Declaration

    private string _ItemValue = string.Empty;
    private string _ItemText = String.Empty;

    #endregion

    #region Properties

    /// <summary>
    /// Gets the ID for this object
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string ItemText
    {
      get { return _ItemText; }
    }

    /// <summary>
    /// Gets the item value for this object
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string ItemValue
    {
      get { return _ItemValue; }
    }

    #endregion

    #region Constructors

    private AduvoListItem()
    {
    }

    /// <summary>
    /// Instantiates a new ListItem object
    /// </summary>
    /// <param name="itemID">ID of the list item</param>
    /// <param name="itemValue">Value of the list item</param>
    /// <param name="valueHeading">Heading to associate with the items value</param>
    /// <remarks></remarks>
    public AduvoListItem(string itemText, string itemValue)
      : base()
    {
      _ItemText = itemText;

      _ItemValue = itemValue;
    }

    #endregion
  }
}