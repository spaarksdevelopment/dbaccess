using System;

namespace Spaarks.Common.UtilityManager
{
  /// <summary>
  /// Provides methods for common SQL s
  /// </summary>
  public static class StorageHelper
  {
    #region Methods

    /// <summary>
    /// Returns nullable Boolean value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static Nullable<bool> NullBoolean(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      if (reader[columnName] == System.DBNull.Value)
      {
        return new Nullable<bool>();
      }
      else
      {
        return new Nullable<bool>(reader.GetBoolean(reader.GetOrdinal(columnName)));
      }
    }

    /// <summary>
    /// Returns nullable DateTime value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static Nullable<System.DateTime> NullDate(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      // Have DateTime class so specify want System DateTime class
      if (reader[columnName] == System.DBNull.Value)
      {
        return new Nullable<System.DateTime>();
      }
      else
      {
        return new Nullable<System.DateTime>(reader.GetDateTime(reader.GetOrdinal(columnName)));
      }
    }

    /// <summary>
    /// Returns nullable Byte value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static Nullable<byte> NullInt8(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      if (reader[columnName] == System.DBNull.Value)
      {
        return new Nullable<byte>();
      }
      else
      {
        return new Nullable<byte>(reader.GetByte(reader.GetOrdinal(columnName)));
      }
    }

    /// <summary>
    /// Returns nullable Int16 value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static Nullable<Int16> NullInt16(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      if (reader[columnName] == System.DBNull.Value)
      {
        return new Nullable<Int16>();
      }
      else
      {
        return new Nullable<Int16>(reader.GetInt16(reader.GetOrdinal(columnName)));
      }
    }

    /// <summary>
    /// Returns nullable Int32 value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static Nullable<Int32> NullInt32(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      if (reader[columnName] == System.DBNull.Value)
      {
        return new Nullable<Int32>();
      }
      else
      {
        return new Nullable<Int32>(reader.GetInt32(reader.GetOrdinal(columnName)));
      }
    }

    /// <summary>
    /// Returns string value for tablereader column, checking for DBNull
    /// </summary>
    /// <param name="tableReader">Table reader object to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns>Object converted to string</returns>
    /// <remarks>String.Empty is returned if ColumnValue is null. No exception handling is performed. SQL GetString should not be used to pass ColumnValue GetString will generate a runtime exception if the column is DBNull, GetValue should be used instead</remarks>
    public static string NullString(System.Data.IDataReader reader, string columnName)
    {
      _CheckColumnInReader(reader, columnName);

      return _NullString(reader.GetValue(reader.GetOrdinal(columnName)));
    }

    /// <summary>
    /// Returns string value for row column, checking for DBNull
    /// </summary>
    /// <param name="row">Row to get column from</param>
    /// <param name="columnName">Name of column to get value for</param>
    /// <returns>Object converted to string</returns>
    /// <remarks>String.Empty is returned if ColumnValue is null. No exception handling is performed</remarks>
    public static string NullString(System.Data.DataRow row, string columnName)
    {
      return _NullString(row[columnName]);
    }

    public static byte[] NullTimeStamp(System.Data.IDataReader reader, string columnName)
    {
      byte[] ReturnValue = null;

      _CheckColumnInReader(reader, columnName);

      if (reader[columnName] != System.DBNull.Value)
      {
        ReturnValue = new byte[8];

        long BytesRead = reader.GetBytes(reader.GetOrdinal(columnName), 0, ReturnValue, 0,ReturnValue.Length);
      }

      return ReturnValue;
    }

    /// <summary>
    /// Checks for single quote in string and converts to two single quotes
    /// </summary>
    /// <param name="String">String to check</param>
    /// <returns>String with any single quotes replaced with two single quotes</returns>
    /// <remarks>String.Empty is returned if Text is null. Only needed for raw SQL coammnds, stored parameters do not need to call this . If text if null then String.Empty is returned</remarks>
    public static string SQLQuote(string text)
    {
      if (text == null)
      {
        return string.Empty;
      }
      else
      {
        return text.Replace("'", "''");
      }
    }

    /// <summary>
    /// Appends two SQL where clauses, enclosing each in brackets
    /// </summary>
    /// <param name="String">ExistingWhere</param>
    /// <param name="String"></param>
    /// <returns>SQL where clause with each clause enclosed in brackets</returns>
    /// <remarks>If either clause is empty then the other clause is returned, without brackets. If both clauses are empty then an empty string is returned</remarks>
    public static string WhereAnd(string existingWhere, string appendWhere)
    {
      return _Where(existingWhere, appendWhere, "AND");
    }

    /// <summary>
    /// Appends two SQL where clauses, enclosing each in brackets
    /// </summary>
    /// <param name="existingWhere"></param>
    /// <param name="appendWhere"></param>
    /// <returns>SQL where clause with each clause enclosed in brackets</returns>
    /// <remarks>If either clause is empty then the other clause is returned, without brackets. If both clauses are empty then an empty string is returned</remarks>
    public static String WhereOr(string existingWhere, string appendWhere)
    {
      return _Where(existingWhere, appendWhere, "OR");
    }

    #endregion

    #region Private Methods

    private static void _CheckColumnInReader(System.Data.IDataReader reader, string columnName)
    {
      if (string.IsNullOrEmpty(columnName))
      {
        throw new ArgumentException("Column name must be specified");
      }
      try
      {
        int ColumnOrdinal = reader.GetOrdinal(columnName);
      }
      catch
      {
        throw new ArgumentOutOfRangeException(string.Format("Column '{0}' is not present in reader", columnName));
      }
    }

    /// <summary>
    /// Returns string value for object passed, checking for DBNull
    /// </summary>
    /// <param name="value">Object to convert to string</param>
    /// <returns>Object converted to string</returns>
    /// <remarks>String.Empty is returned if ColumnValue is null. No exception handling is performed. SQL GetString should not be used to pass ColumnValue GetString will generate a runtime exception if the column is DBNull, GetValue should be used instead</remarks>
    private static string _NullString(object value)
    {
      // No Try ctach so exception passed back
      if (value == null)
      {
        return string.Empty;
      }
      else
      {
        if (value == DBNull.Value)
        {
          return string.Empty;
        }
        else
        {
          return value.ToString();
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="existingWhere"></param>
    /// <param name="appendWhere"></param>
    /// <param name="AppendText"></param>
    /// <returns></returns>
    private static string _Where(string existingWhere, string appendWhere, string appendText)
    {
      string ReturnValue = string.Empty;

      string existing = string.Empty;
      string append = string.Empty;

      if (existingWhere != null)
      {
        if (existingWhere.Trim().Length > 0)
        {
          existing = existingWhere;
        }
      }
      if (appendWhere != null)
      {
        if (appendWhere.Trim().Length > 0)
        {
          append = appendWhere;
        }
        if (existing.Length > 0)
        {
          if (append.Length > 0)
          {
            ReturnValue = string.Format("({0}) {1} ({2})", existing, appendText, append);
          }
          else
            ReturnValue = existing;
        }
        else
          // Null in exisitng so just use append 
          if (append.Length > 0)
          {
            ReturnValue = append;
          }
          else
          {
            // Both exisitng and append are empty/null
          }
      }
      return ReturnValue;
    }

    #endregion
  }
}