using System;
using System.Configuration;
using System.Net.Mail;

namespace Aduvo.Common.UtilityManager
{
    /// <summary>
    /// MailManager all functions to do with emailing
    /// </summary>
    public static class MailHelper
    {
        /// <summary>
        /// Sends an email to one recipient
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        public static void SendEmail(string Recipient, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
        {
            MailMessage mm = new MailMessage();

            if (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE")
            {
                //add original recipient to subject line, and replace with test email address
                Subject = Subject + " (recipient: " + Recipient + ")";
                string TestRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

                if (TestRecipient.Contains(","))
                {
                    string[] TestRecipients = TestRecipient.Split(Convert.ToChar(","));
                    foreach (string sTestRecipient in TestRecipients)
                    {
                        mm.To.Add(new MailAddress(sTestRecipient));
                    }
                }
                else
                {
                    mm.To.Add(new MailAddress(TestRecipient));
                }
            }
            else
            {
                mm.To.Add(new MailAddress(Recipient));
            }

            //check if a BCC recipient is specified for the site
            string BCCRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];
            if (BCCRecipient != null)
            {
                if (BCCRecipient != string.Empty)
                {
                    if (BCCRecipient.Contains(","))
                    {
                        string[] BCCRecipients = BCCRecipient.Split(Convert.ToChar(","));
                        foreach (string sBCCRecipient in BCCRecipients)
                        {
                            mm.Bcc.Add(new MailAddress(sBCCRecipient));
                        }
                    }
                    else
                    {
                        mm.Bcc.Add(new MailAddress(BCCRecipient));
                    }
                }
            }

            if (SenderEmailAddress == string.Empty)
            {
                SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
                SenderName = ConfigurationManager.AppSettings["SenderName"];
            }

            MailAddress maSender = new MailAddress(SenderEmailAddress, SenderName);
            
            mm.From = maSender;
            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = IsHTML;

            try
            {
                SmtpClient smtp = new SmtpClient();

                smtp.Send(mm);
            }            
            catch (Exception Ex)
            {
                throw Ex;
            }

        }

        /// <summary>
        /// Sends an email to one recipient
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        public static void SendEmail(string Recipient, string Subject, string Body, bool IsHTML)
        {
            SendEmail(Recipient, Subject, Body, IsHTML, string.Empty, string.Empty);
        }

        /// <summary>
        /// Sends an email to several recients
        /// </summary>
        /// <param name="Recipients">the recipeints' email addresses</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        public static void SendEmail(string[] Recipients, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
        {
            if (Recipients.Length == 0) return;

            if (SenderEmailAddress == string.Empty)
            {
                SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
                SenderName = ConfigurationManager.AppSettings["SenderName"];
            }

            MailAddress maSender = new MailAddress(SenderEmailAddress, SenderName);
            MailMessage mm = new MailMessage();

            if (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE")
            {
                //add original recipients to subject line, and replace with test email address
                string RecipientList = string.Empty;
                foreach (string sRecipient in Recipients)
                {
                    RecipientList += ", " + sRecipient;
                }
                RecipientList = RecipientList.Substring(2);

                Subject = Subject + " (recipients: " + RecipientList + ")";
                string TestRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

                if (TestRecipient.Contains(","))
                {
                    string[] TestRecipients = TestRecipient.Split(Convert.ToChar(","));
                    foreach (string sTestRecipient in TestRecipients)
                    {
                        mm.To.Add(new MailAddress(sTestRecipient));
                    }
                }
                else
                    mm.To.Add(new MailAddress(TestRecipient));
            }
            else
            {
                foreach (string sRecipient in Recipients)
                {
                    mm.To.Add(new MailAddress(sRecipient));
                }
            }

            //check if a BCC recipient is specified for the site
            string BCCRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];
            if (BCCRecipient != null)
            {
                if (BCCRecipient != string.Empty)
                {
                    if (BCCRecipient.Contains(","))
                    {
                        string[] BCCRecipients = BCCRecipient.Split(Convert.ToChar(","));
                        foreach (string sBCCRecipient in BCCRecipients)
                        {
                            mm.Bcc.Add(new MailAddress(sBCCRecipient));
                        }
                    }
                    else
                    {
                        mm.Bcc.Add(new MailAddress(BCCRecipient));
                    }
                }
            }
            
            mm.From = maSender;
            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = IsHTML;

            try
            {
                SmtpClient smtp = new SmtpClient();

                smtp.Send(mm);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

        }

        /// <summary>
        /// Sends an email to several recients
        /// </summary>
        /// <param name="Recipients">the recipeints' email addresses</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        public static void SendEmail(string[] Recipients, string Subject, string Body, bool IsHTML)
        {
            SendEmail(Recipients, Subject, Body, IsHTML, string.Empty, string.Empty);
        }

        /// <summary>
        /// Sends an email to one recient without raising exceptions
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        /// <returns>true if email sent, false if not</returns>
        public static bool SafeSendEmail(string Recipient, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
        {
            return SafeSendEmail(Recipient, string.Empty, Subject, Body, IsHTML, SenderEmailAddress, SenderName);
        }

        /// <summary>
        /// Sends an email to one recient without raising exceptions
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        /// <returns>true if email sent, false if not</returns>
        public static bool SafeSendEmail(string Recipient, string CCRecipient, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
        {
            bool success = false;

            try
            {
                MailMessage mm = new MailMessage();

                if ((ConfigurationManager.AppSettings["TestMode"] != null) && (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE"))
                {
                    //add original recipient to subject line, and replace with test email address
                    Subject = Subject + " (recipient: " + Recipient + ")";
                    string TestRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];

                    if (TestRecipient.Contains(","))
                    {
                        char[] recpSep = { ',' };
                        string[] TestRecipients = TestRecipient.Split(recpSep, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string sTestRecipient in TestRecipients)
                        {
                            if (!string.IsNullOrEmpty(sTestRecipient))
                            {
                                mm.To.Add(new MailAddress(sTestRecipient));
                            }
                        }
                    }
                    else
                    {
                        mm.To.Add(new MailAddress(TestRecipient));
                    }

                    if (!string.IsNullOrEmpty(CCRecipient))
                    {
                        if (CCRecipient.Contains(","))
                        {
                            char[] ccRecepSep = { ',' };
                            string[] CCRecipients = CCRecipient.Split(ccRecepSep, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string sCCRecipient in CCRecipients)
                            {
                                if (!string.IsNullOrEmpty(sCCRecipient))
                                {
                                    mm.CC.Add(new MailAddress(sCCRecipient));
                                }
                            }
                        }
                        else
                        {
                            mm.CC.Add(new MailAddress(CCRecipient));
                        }
                    }
                }
                else
                {
                    if (Recipient.Contains(","))
                    {
                        char[] sprtor = { ',' };
                        string[] Recipients = Recipient.Split(sprtor, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string sRecipient in Recipients)
                        {
                            if (!string.IsNullOrEmpty(sRecipient))
                            {
                                mm.To.Add(new MailAddress(sRecipient));
                            }
                        }
                    }
                    else
                    {
                        mm.To.Add(new MailAddress(Recipient));
                    }

                    if (!string.IsNullOrEmpty(CCRecipient))
                    {
                        if (CCRecipient.Contains(","))
                        {
                            char[] ccSprtor = { ',' };
                            string[] CCRecipients = CCRecipient.Split(ccSprtor, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string sCCRecipient in CCRecipients)
                            {
                                if (!string.IsNullOrEmpty(sCCRecipient))
                                {
                                    mm.CC.Add(new MailAddress(sCCRecipient));
                                }
                            }
                        }
                        else
                        {
                            mm.CC.Add(new MailAddress(CCRecipient));
                        }
                    }
                }

                //check if a BCC recipient is specified for the site
                string BCCRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];
                if (!string.IsNullOrEmpty(BCCRecipient))
                {

                    if (BCCRecipient.Contains(","))
                    {
                        char[] bccSprtor = { ',' };
                        string[] BCCRecipients = BCCRecipient.Split(bccSprtor, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string sBCCRecipient in BCCRecipients)
                        {
                            if (!string.IsNullOrEmpty(sBCCRecipient))
                            {
                                mm.Bcc.Add(new MailAddress(sBCCRecipient));
                            }
                        }
                    }
                    else
                    {
                        mm.Bcc.Add(new MailAddress(BCCRecipient));
                    }

                }

                if (SenderEmailAddress == string.Empty)
                {
                    SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
                    SenderName = ConfigurationManager.AppSettings["SenderName"];
                }

                MailAddress maSender = new MailAddress(SenderEmailAddress, SenderName);

                mm.From = maSender;
                mm.Subject = Subject;
                mm.Body = Body;
                mm.IsBodyHtml = IsHTML;
                
                SmtpClient smtp = new SmtpClient();

                //this is only here to enable relaying through gmail for test.
				smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPEnableSsl"]);
				//smtp.EnableSsl = false;

                smtp.Send(mm);
				
                success = true;
            }
            catch (SmtpException s)
            {
                LogHelper.HandleException(s, ExceptionSeverity.High);
            }
            catch (Exception Ex)
            {
                LogHelper.HandleException(Ex, ExceptionSeverity.High);
            }

            return success;
        }

        /// <summary>
        /// Sends an email to one recient without raising exceptions
        /// </summary>
        /// <param name="Recipient">the recipeint email address</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <returns>true if email sent, false if not</returns>
        public static bool SafeSendEmail(string Recipient, string Subject, string Body, bool IsHTML)
        {
            return SafeSendEmail(Recipient, Subject, Body, IsHTML, string.Empty, string.Empty);
        }

        /// <summary>
        /// Sends an email to several recients without raising exceptions
        /// </summary>
        /// <param name="Recipients">the recipeints' email addresses</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <param name="SenderEmailAddress">Sender Email Address</param>
        /// <param name="SenderName">Sender Name</param>
        /// <returns>true if email sent, false if not</returns>
        public static bool SafeSendEmail(string[] Recipients, string Subject, string Body, bool IsHTML, string SenderEmailAddress, string SenderName)
        {
            if (Recipients.Length == 0) return false;

            bool success;

            if (SenderEmailAddress == string.Empty)
            {
                SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
                SenderName = ConfigurationManager.AppSettings["SenderName"];
            }

            MailAddress maSender = new MailAddress(SenderEmailAddress, SenderName);
            
            MailMessage mm = new MailMessage();

            if ((ConfigurationManager.AppSettings["TestMode"] != null) && (ConfigurationManager.AppSettings["TestMode"].ToUpper() == "TRUE"))
            {
                //add original recipients to subject line, and replace with test email address
                string RecipientList = string.Empty;
                foreach (string sRecipient in Recipients)
                {
                    RecipientList += ", " + sRecipient;
                }
                RecipientList = RecipientList.Substring(2);

                Subject = Subject + " (recipients: " + RecipientList + ")";

                string TestRecipient = ConfigurationManager.AppSettings["TestEmailAddress"];
                if (!string.IsNullOrEmpty(TestRecipient))
                {
                    if (TestRecipient.Contains(","))
                    {
                        char[] tSprtor = { ',' };
                        string[] TestRecipients = TestRecipient.Split(tSprtor,StringSplitOptions.RemoveEmptyEntries);

                        foreach (string sTestRecipient in TestRecipients)
                        {

                            if (!string.IsNullOrEmpty(sTestRecipient))
                            {
                                mm.To.Add(new MailAddress(sTestRecipient));
                            }

                        }
                    }
                    else
                        mm.To.Add(new MailAddress(TestRecipient));
                }
            }
            else
            {
                foreach (string sRecipient in Recipients)
                {
                    mm.To.Add(new MailAddress(sRecipient));
                }
            }

            //check if a BCC recipient is specified for the site
            string BCCRecipient = ConfigurationManager.AppSettings["BCCEmailAddress"];
            if (!string.IsNullOrEmpty(BCCRecipient))
            {

                if (BCCRecipient.Contains(","))
                {
                    char[] bccSep = { ',' };
                    string[] BCCRecipients = BCCRecipient.Split(bccSep, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string sBCCRecipient in BCCRecipients)
                    {
                        if (!string.IsNullOrEmpty(sBCCRecipient))
                        {
                            mm.Bcc.Add(new MailAddress(sBCCRecipient));
                        }
                    }
                }
                else
                {
                    mm.Bcc.Add(new MailAddress(BCCRecipient));
                }

            }

            mm.From = maSender;
            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = IsHTML;

            try
            {
                SmtpClient smtp = new SmtpClient();

                smtp.Send(mm);
                
                success = true;
            }
            catch (Exception Ex)
            {
                success = false;
                LogHelper.HandleException(Ex, ExceptionSeverity.High);
            }

            return success;        
        }

        /// <summary>
        /// Sends an email to several recients without raising exceptions
        /// </summary>
        /// <param name="Recipients">the recipeints' email addresses</param>
        /// <param name="Subject">the subject</param>
        /// <param name="Body">the mail body</param>
        /// <param name="IsHTML">true if body contains html</param>
        /// <returns>true if email sent, false if not</returns>
        public static bool SafeSendEmail(string[] Recipients, string Subject, string Body, bool IsHTML)
        {
            return SafeSendEmail(Recipients, Subject, Body, IsHTML, string.Empty, string.Empty);
        }

    }
}
