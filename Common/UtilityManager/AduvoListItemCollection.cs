using System;

namespace Aduvo.Common.UtilityManager
{
    /// <summary>
    /// Provides methods for storing and retrieving ListItem collections
    /// </summary>
    /// <remarks></remarks>
    public class AduvoListItemCollection
    {
        #region Declaration

        private enum EnumChange
        {
            NoChange,
            ReplaceText,
            SplitCapitalisation
        }

        private System.Collections.Generic.List<AduvoListItem> _ListItems;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the collection of list items for this object
        /// </summary>
        /// <value>A generic list of <see cref="Aduvo.Common.UtilityManager.ListItem">ListItems</see></value>
        /// <remarks></remarks>
        public System.Collections.Generic.List<AduvoListItem> ListItems
        {
            get { return _ListItems; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates a new list item object
        /// </summary>
        /// <param name="valueHeading">Heading to associate with items values</param>
        /// <remarks></remarks>
        public AduvoListItemCollection()
        {
            _ListItems = new System.Collections.Generic.List<AduvoListItem>();
        }

        #endregion

        #region Methods

        public static AduvoListItemCollection AbbreviatedMonthList()
        {
            return _MonthList(true);
        }

        /// <summary>
        /// Adds the supplied <see cref="Aduvo.Common.UtilityManager.ListItem">ListItem</see> to this objects collection
        /// </summary>
        /// <param name="itemID">ID of the list item</param>
        /// <param name="itemValue">Value of the list item</param>
        /// <remarks></remarks>
        /// <exception cref="ArgumentNullException">ListItem == null</exception>
        public void AddListItem(string itemText, string itemValue)
        {
            _ListItems.Add(new AduvoListItem(itemText, itemValue));
        }

        /// <summary>
        /// Gets day of month list with text and value 1 to 31
        /// </summary>
        /// <returns></returns>
        public static AduvoListItemCollection DayOfMonthList()
        {
            return _DayOfMonthList(31);
        }

        /// <summary>
        /// Gets day of month list with text and value 1 to appropriate number of days in specified month of year
        /// </summary>
        /// <returns></returns>
        public static AduvoListItemCollection DayOfMonthList(int year, byte month)
        {
            return _DayOfMonthList(System.DateTime.DaysInMonth(year, month));
        }

        /// <summary>
        /// Gets a list of months using full month name and value 1 to 12
        /// </summary>
        /// <returns></returns>
        public static AduvoListItemCollection MonthList()
        {
            return _MonthList(false);
        }

        /// <summary>
        /// Populates list with supplied reader values
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="textColumn"></param>
        /// <param name="valueColumn"></param>
        public void PopulateItems(System.Data.IDataReader reader, string textColumn, string valueColumn)
        {
            while (reader.Read())
            {
                this.AddListItem(reader[textColumn].ToString(), reader[valueColumn].ToString());
            }

            reader.Close();
        }

        /// <summary>
        /// Populates list with all values in enum
        /// </summary>
        /// <param name="enumType"></param>
        public static AduvoListItemCollection CreateListFromEnum(Type enumType)
        {
            return _CreateListFromEnum(enumType, EnumChange.NoChange, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Populates list with all values in enum, changing underscores to specified text
        /// </summary>
        /// <param name="enumType"></param>
        public static AduvoListItemCollection CreateListFromEnumReplaceText(Type enumType, string findText, string replacementText)
        {
            return _CreateListFromEnum(enumType, EnumChange.ReplaceText, findText, replacementText, string.Empty);
        }

        /// <summary>
        /// Populates list with all values in enum, splitting text of entries that are capitalised
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="padding">Text to use for padding split capitalised text</param>
        public static AduvoListItemCollection CreateListFromEnumSplitCapitalised(Type enumType, string padding)
        {
            return _CreateListFromEnum(enumType, EnumChange.SplitCapitalisation, string.Empty, string.Empty, padding);
        }

        #endregion

        #region Private Methods

        private static AduvoListItemCollection _CreateListFromEnum(Type enumType, EnumChange enumChange, string findText, string replacementText, string padding)
        {
            AduvoListItemCollection ReturnValue = new AduvoListItemCollection();

            foreach (object EnumValue in System.Enum.GetValues(enumType))
            {
                string ItemText = System.Enum.GetName(enumType, EnumValue).ToString();

                switch (enumChange)
                {
                    case EnumChange.NoChange:
                        break;

                    case EnumChange.ReplaceText:
                        ItemText = ItemText.Replace(findText, replacementText);
                        break;

                    case EnumChange.SplitCapitalisation:
                        ItemText = Text.SplitCapitalisedString(ItemText, padding);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unknown enum change type '{0}'", enumChange));
                }

                ReturnValue.AddListItem(ItemText, ((int)EnumValue).ToString());
            }

            return ReturnValue;
        }

        private static AduvoListItemCollection _DayOfMonthList(int lastDay)
        {
            // lastDay is int as DateTime.DaysInMonth() returns int

            AduvoListItemCollection ReturnValue = new AduvoListItemCollection();

            for (byte DayNumber = 1; DayNumber <= lastDay; DayNumber++)
            {
                ReturnValue.AddListItem(DayNumber.ToString(), DayNumber.ToString());
            }

            return ReturnValue;
        }

        private static AduvoListItemCollection _MonthList(bool abbreviated)
        {
            AduvoListItemCollection ReturnValue = new AduvoListItemCollection();

            // Months are zero based !
            for (byte MonthNumber = 0; MonthNumber < 12; MonthNumber++)
            {
                string MonthName = System.Globalization.DateTimeFormatInfo.CurrentInfo.MonthNames.GetValue(MonthNumber).ToString();

                if (abbreviated)
                {
                    MonthName = MonthName.Substring(0, 3);
                }

                // Use 'standard' 1-12 for value
                ReturnValue.AddListItem(MonthName, (MonthNumber + 1).ToString());
            }

            return ReturnValue;
        }

        #endregion
    }
}