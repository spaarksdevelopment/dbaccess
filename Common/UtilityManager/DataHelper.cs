﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;



namespace Aduvo.Common.UtilityManager
{
    public static class DataHelper
    {
        /// <summary>
        /// Gets a reference to the default connection as specified in web.config
        /// </summary>
        /// <returns>the database connection</returns>
        public static SqlConnection getConn()
        {
            return getConn(System.Web.Configuration.WebConfigurationManager.AppSettings["MembershipConnectionString"]);
        }

        /// <summary>
        /// Gets a reference to the default connection as specified in web.config
        /// </summary>
        /// <param name="SettingName">The name name of the setting containing the active connection string</param>
        /// <returns>the database connection</returns>
        public static SqlConnection getConn(string sActiveConnection)
        {
            string ConnectionPoolMinSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMinSize"];
            string ConnectionPoolMaxSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMaxSize"];
            string ConnectionTimeOutPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutPooled"];

            string sConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings[sActiveConnection].ToString();

            SqlConnection _conn = new SqlConnection();

            string sConnQualified = sConn + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;

            try
            {
                //try a connection from the pool
                _conn.ConnectionString = sConnQualified;
                _conn.Open();
            }
            catch (SqlException ex1)
            {
                LogHelper.HandleException(ex1);

                //try clearing pools
                SqlConnection.ClearAllPools();

                try
                {
                    _conn.ConnectionString = sConnQualified;
                    _conn.Open();
                }
                catch (SqlException ex2)
                {
                    LogHelper.HandleException(ex2);

                    //open connection without using pooling
                    if (_conn.State != ConnectionState.Closed) _conn.Close();
                    string ConnectionTimeOutNonPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutNonPooled"];
                    sConnQualified = sConn + ";Pooling=false;Connect Timeout=" + ConnectionTimeOutNonPooled;
                    _conn.ConnectionString = sConnQualified;
                    _conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            
            return _conn;
        }

        /// <summary>
        /// Gets a the default connection string as specified in web.config
        /// </summary>
        /// <returns>the connection string</returns>
        public static string getConnectionString()
        {
            return getConnectionString("MembershipConnectionString");
        }

        /// <summary>
        /// Gets a the default connection string as specified in web.config
        /// </summary>
        /// <returns>the connection string</returns>
        public static string getConnectionString(string SettingName)
        {
            string sActiveConnection = System.Web.Configuration.WebConfigurationManager.AppSettings[SettingName];
            string ConnectionPoolMinSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMinSize"];
            string ConnectionPoolMaxSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMaxSize"];
            string ConnectionTimeOutPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutPooled"];

            string sConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings[sActiveConnection].ToString();

            string sConnQualified = sConn + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;

            return sConnQualified;
        }

        /// <summary>
        /// closes the connection
        /// </summary>
        /// <param name="_conn">the database connection</param>
        public static void closeConn(ref SqlConnection _conn)
        {
            _conn.Close();
            _conn.Dispose();
            _conn = null;
        }
    }
}
