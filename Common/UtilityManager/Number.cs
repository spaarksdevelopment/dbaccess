﻿using System;

/// <summary>
/// Provides helper functions for general number usage
/// </summary>
namespace Aduvo.Common.UtilityManager
{
  public static class Number
  {
    public static Nullable<Byte> NullableInt8(String value)
    {
      Nullable<Byte> ReturnValue = new Nullable<Byte>();

      Byte TestedValue;

      if (Byte.TryParse(value, out TestedValue))
      {
        ReturnValue = new Nullable<Byte>(TestedValue);
      }
      return ReturnValue;
    }

    public static Nullable<Byte> NullableInt8(String value, byte minValue, byte maxValue)
    {
      Nullable<Byte> ReturnValue = new Nullable<Byte>();

      Byte TestedValue;

      if (Byte.TryParse(value, out TestedValue))
      {
        if (TestedValue >= minValue && TestedValue <= maxValue)
        {
          ReturnValue = TestedValue;
        }
      }

      return ReturnValue;
    }

    public static Nullable<Int16> NullableInt16(String value)
    {
      Nullable<Int16> ReturnValue = new Nullable<Int16>();

      Int16 TestedValue;

      if (Int16.TryParse(value, out TestedValue))
      {
        ReturnValue = new Nullable<Int16>(TestedValue);
      }
      return ReturnValue;
    }

    public static Nullable<Int32> NullableInt32(String value)
    {
      Nullable<Int32> ReturnValue = new Nullable<Int32>();

      Int32 TestedValue;

      if (Int32.TryParse(value, out TestedValue))
      {
        ReturnValue = new Nullable<Int32>(TestedValue);
      }
      return ReturnValue;
    }

    public static Nullable<Int64> NullableInt64(String value)
    {
      Nullable<Int64> ReturnValue = new Nullable<Int64>();

      Int32 TestedValue;

      if (Int32.TryParse(value, out TestedValue))
      {
        ReturnValue = new Nullable<Int32>(TestedValue);
      }
      return ReturnValue;
    }
  }
}