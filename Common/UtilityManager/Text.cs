using System;

namespace Aduvo.Common.UtilityManager
{
  /// <summary>
  /// Provides helper functions for general text usage
  /// </summary>
  public static class Text
  {
    public static System.Collections.Generic.List<string> GetStringList(string text, char separator)
    {
      System.Collections.Generic.List<string> ReturnValue = new System.Collections.Generic.List<string>();

      if (text != null && text.Length > 0)
      {
        char[] Separators = { separator };

        string[] StringValues = text.Split(Separators, StringSplitOptions.RemoveEmptyEntries);

        foreach (string StringValue in StringValues)
        {
          ReturnValue.Add(StringValue.Trim());

        }
      }

      return ReturnValue;
    }

    /// <summary>
    /// Indicates if the supplied string is null, empty of just contains spaces
    /// </summary>
    /// <param name="value">String to test</param>
    /// <returns>True if string is either null, empty or just spaces; otherwise false</returns>
    static bool IsNullOrEmptyOrJustSpaces(string value)
    {
      if (string.IsNullOrEmpty(value))
      {
        return true;
      }
      else
      {
        if (value.Trim().Length == 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }

    /// <summary>
    /// Encloses specified text with supplied quotes if it contains a space
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string QuoteSpacedText(string text, string quote)
    {
      // Return same value if no space
      string ReturnValue = text;

      if (!string.IsNullOrEmpty(text))
      {
        if (text.Contains(" "))
        {
          ReturnValue = string.Format("{0}{1}{0}", quote, text);
        }
      }

      return ReturnValue;
    }

    /// <summary>
    /// Splits text where capitalised
    /// </summary>
    /// <param name="text">Text to split</param>
    /// <returns></returns>
    public static string SplitCapitalisedString(string text)
    {
      return SplitCapitalisedString(text, string.Empty);
    }

    /// <summary>
    /// Splits text where capitalised
    /// </summary>
    /// <param name="text">Text to split</param>
    /// <param name="padding">Text to use for padding split capitalised text</param>
    /// <returns></returns>
    public static string SplitCapitalisedString(string text, string padding)
    {
      System.Text.StringBuilder ReturnValue = new System.Text.StringBuilder();

      if (!string.IsNullOrEmpty(text))
      {
        for (int Index = 0; Index < text.Length; Index++)
        {
          string Character = text[Index].ToString();

          if (Character.ToLower() == Character)
          {
            // No need to process
            ReturnValue.Append(Character);
          }
          else
          {
            // No need to split if first character
            if (Index == 0)
            {
              ReturnValue.Append(Character);
            }
            else
            {
              ReturnValue.AppendFormat("{0}{1}", padding, Character);
            }
          }
        }
      }

      return ReturnValue.ToString();
    }

    public static Nullable<byte> ToInt8(string value)
    {
      Nullable<byte> ReturnValue = new Nullable<byte>();

      byte TestValue;

      if (byte.TryParse(value, out TestValue))
      {
        ReturnValue = TestValue;
      }

      return ReturnValue;
    }

    public static Nullable<Int16> ToInt16(string value)
    {
      Nullable<Int16> ReturnValue = new Nullable<Int16>();

      Int16 TestValue;

      if (Int16.TryParse(value, out TestValue))
      {
        ReturnValue = TestValue;
      }

      return ReturnValue;
    }

    public static Nullable<Int32> ToInt32(string value)
    {
      Nullable<Int32> ReturnValue = new Nullable<Int32>();

      Int32 TestValue;

      if (Int32.TryParse(value, out TestValue))
      {
        ReturnValue = TestValue;
      }

      return ReturnValue;
    }

    public static Nullable<Int64> ToInt64(string value)
    {
      Nullable<Int64> ReturnValue = new Nullable<Int64>();

      Int64 TestValue;

      if (Int64.TryParse(value, out TestValue))
      {
        ReturnValue = TestValue;
      }

      return ReturnValue;
    }
  }
}