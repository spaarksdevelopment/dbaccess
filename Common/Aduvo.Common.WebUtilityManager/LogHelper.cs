using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using Aduvo.Common.UtilityManager;

namespace Aduvo.Common.WebUtilityManager
{
    public static class WebLogHelper
    {
        private static string DBErrorMessage = string.Empty;

        #region Handle Exception

        /// <summary>
        /// This is a generic exception handler which takes care of logging and emailing exceptions
        /// </summary>
        /// <param name="ex">the exception to be handled</param>
        /// <param name="Severity">the severity of the exception</param>
        /// <param name="UserName">the logged in user name</param>
        /// <param name="UserRole">the logged in user's role</param>
		public static int HandleException(Exception ex, ExceptionSeverity Severity, string UserName, string UserRole)
		{
			HttpContext ctx = HttpContext.Current;	
			string strData = String.Empty;
			int evtId = 0;
			bool DoLogErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]);
            bool DoEmailErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["EmailErrors"]);
            short EmailErrorsMinSeverity = Convert.ToInt16(ConfigurationManager.AppSettings["EmailErrorsMinSeverity"]);
            string WebAppName = ConfigurationManager.AppSettings["AppName"].ToString();
            string WebAppEnv = ConfigurationManager.AppSettings["AppEnv"].ToString();
            //set to true if writing to db fails
            bool DBError = false;

            string referer = String.Empty;
            if (ctx.Request.ServerVariables["HTTP_REFERER"] != null)
            {
                referer = ctx.Request.ServerVariables["HTTP_REFERER"].ToString();
            }

            string sForm = (ctx.Request.Form != null) ? ctx.Request.Form.ToString() : String.Empty;

            if (sForm != String.Empty)
            {
                //remove the viewstate as its huge and not much use to us
                int start = sForm.IndexOf("__VIEWSTATE=");
                if (start > -1)
                {
                    int end = sForm.IndexOf("&",start);
                    if (end == -1)
                    {
                        sForm = sForm.Remove(start);
                    }
                    else
                    {
                        end = end + 1; //include the '&'
                        sForm = sForm.Remove(start, end - start);
                    }
                }
                
            }

            DateTime d = DateTime.Now;
            string logDateTime = d.ToString("yyyy-MM-dd HH:mm:ss");            

            string sQuery = (ctx.Request.QueryString != null) ? ctx.Request.QueryString.ToString() : String.Empty;

            strData =   "\nAPP: " + WebAppName +
                        "\nENV: " + WebAppEnv + 
                        "\nSEVERITY: " + Severity +
                        "\nUSER: " + UserName + " (" + UserRole + ")" +
                        "\nSOURCE: " + ex.Source +
                        "\nLogDateTime: " + logDateTime +
                        "\nMESSAGE: " + ex.Message +
                        //"\nFORM: " + sForm +
                        "\nQUERYSTRING: " + sQuery +
                        "\nTARGETSITE: " + ex.TargetSite +
                        "\nSTACKTRACE: " + ex.StackTrace +
                        "\nREFERER: " + referer;

			if(DoLogErrors)
			{                
                evtId = LogException(WebAppName, WebAppEnv, referer, sForm,sQuery, Severity, UserName, UserRole, ex);
                if (DBErrorMessage != string.Empty) DBError = true;

                if (evtId > 0)
                    strData += "\nEVENT ID: " + evtId.ToString();
                
                    /*
                    try
                    {					
	                    EventLog.WriteEntry (ex.Source, strData,EventLogEntryType.Error,evtId);
	                }
	                catch(Exception exl)
	                {
		                Debug.WriteLine("EventLog Write Error: " +exl.Message );
		                throw;
                    }
                     */
                      
            }

            if ((DoEmailErrors & Convert.ToInt16(Severity) >= EmailErrorsMinSeverity) || (DBError))
            {
                string strEmail = ConfigurationManager.AppSettings["ErrorEmailAddress"].ToString();
                if (strEmail.Length > 0)
                {                   
                    MailAddress recipient = new MailAddress(strEmail);
                    MailAddress sender = new MailAddress(ConfigurationManager.AppSettings["SenderEmailAddress"], ConfigurationManager.AppSettings["SenderName"]);

                    MailMessage mm = new MailMessage(sender,recipient);                  
                    mm.IsBodyHtml = false;

                    if (DBError)
                    {
                        mm.Subject = "LOGGING DATABASE ERROR";
                        mm.Body = "Database error: " + DBErrorMessage + "\n\n\nORIGINAL ERROR:\n" + strData; 
                    }
                    else
                    {
                        mm.Subject = "WEB APP ERROR: " + " " + WebAppName + " " + WebAppEnv + " severity: " + Severity.ToString();
                        mm.Body = strData;                        
                    }

                    try
                    {
                        SmtpClient smtp = new SmtpClient();

                        smtp.Send(mm);
                    }
                    catch (Exception exc)
                    {
                        if (DoLogErrors & !DBError)
                        {
                            LogException(WebAppName, WebAppEnv, string.Empty, string.Empty, string.Empty, ExceptionSeverity.High, UserName, UserRole, exc);
                        }
                    }
                }
			}

            return evtId;
		}
                
        public static int HandleException(Exception ex)
        {
            return HandleException(ex, ExceptionSeverity.Unhandled, String.Empty, String.Empty);
        }

        public static int HandleException(Exception ex, ExceptionSeverity Severity)
        {
            return HandleException(ex, Severity, String.Empty, String.Empty);
        }

        public static int HandleException(Exception ex, string UserName, string UserRole)
        {
            return HandleException(ex, ExceptionSeverity.Unhandled, UserName, UserRole);
        }

        #endregion

        #region Log Exception

        public static int LogException(ExceptionSeverity Severity, string UserName, string UserRole, string message)
        {
            string WebAppName = ConfigurationManager.AppSettings["AppName"].ToString();
            string WebAppEnv = ConfigurationManager.AppSettings["AppEnv"].ToString();
            return LogException(WebAppName, WebAppEnv, string.Empty, string.Empty, string.Empty, Severity, UserName, UserRole, new Exception(message));
        }

        public static int LogException(ExceptionSeverity Severity, string UserName, string UserRole, Exception ex)
        {
            string WebAppName = ConfigurationManager.AppSettings["AppName"].ToString();
            string WebAppEnv = ConfigurationManager.AppSettings["AppEnv"].ToString();
            return LogException(WebAppName, WebAppEnv, string.Empty, string.Empty, string.Empty, Severity, UserName, UserRole, ex);
        }


        public static int LogException(string WebAppName, string WebAppEnv, string referer, string sForm, string sQuery, ExceptionSeverity Severity, string UserName, string UserRole, Exception ex)
        {
            DBErrorMessage = string.Empty; //clear db error message
            int EventID = 0;

            string WebAppLogsConnString;
            string dbConnString;

            if (ConfigurationManager.AppSettings["WebAppLogs"] != null)
            {
                WebAppLogsConnString = ConfigurationManager.AppSettings["WebAppLogs"].ToString();
                dbConnString = ConfigurationManager.ConnectionStrings[WebAppLogsConnString].ToString();
            }
            else
                dbConnString = ConfigurationManager.ConnectionStrings["WebAppLogs"].ToString();


            if (dbConnString.Length > 0)
            {
                //make sure form is not more than 4000 characters
                if (sForm.Length > 4000) sForm = sForm.Substring(0, 4000);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_WebAppLogsInsert";

                SqlConnection cn = new SqlConnection(dbConnString);
                cmd.Connection = cn;
                cn.Open();

                try
                {
                    cmd.Parameters.Add(new SqlParameter("@WebApp", WebAppName));
                    cmd.Parameters.Add(new SqlParameter("@Environment", WebAppEnv));
                    cmd.Parameters.Add(new SqlParameter("@Source", ex.Source));                    
                    cmd.Parameters.Add(new SqlParameter("@Message", ex.Message));
                    cmd.Parameters.Add(new SqlParameter("@Form", sForm));
                    cmd.Parameters.Add(new SqlParameter("@QueryString", sQuery));
                    cmd.Parameters.Add(new SqlParameter("@TargetSite", ex.TargetSite.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@StackTrace", ex.StackTrace.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Referer", referer));
                    cmd.Parameters.Add(new SqlParameter("@Severity", Severity));
                    cmd.Parameters.Add(new SqlParameter("@UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("@UserRole", UserRole));

                    SqlParameter outParm = new SqlParameter("@EventId", SqlDbType.Int);
                    outParm.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outParm);

                    cmd.ExecuteNonQuery();

                    EventID = Convert.ToInt32(cmd.Parameters["@EventId"].Value);
                    
                }
                catch (Exception exc)
                {
                    //try writing to event log
                    //EventLog.WriteEntry(ex.Source, "Database Error From Exception Log!", EventLogEntryType.Error, 65535);
                    DBErrorMessage = exc.Message;
                }
                finally
                {
                    cmd.Dispose();
                    cn.Close();
                }
            }
            return EventID;
        }

        public static bool LogExceptionComment(int EventID, string Comment, string UserEmail, string UserName, string UserRole)
        {            
            string WebAppLogsConnString;
            string dbConnString;

            if (ConfigurationManager.AppSettings["WebAppLogs"] != null)
            {
                WebAppLogsConnString = ConfigurationManager.AppSettings["WebAppLogs"].ToString();
                dbConnString = ConfigurationManager.ConnectionStrings[WebAppLogsConnString].ToString();
            }
            else
                dbConnString = ConfigurationManager.ConnectionStrings["WebAppLogs"].ToString();
                                    
            bool DoLogErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]);
            bool DoEmailErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["EmailErrors"]);
            string WebAppName = ConfigurationManager.AppSettings["AppName"].ToString();
            string WebAppEnv = ConfigurationManager.AppSettings["AppEnv"].ToString();
            bool DBError = false;

            if (DoLogErrors & (dbConnString.Length > 0))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_ErrorCommentInsert";

                SqlConnection cn = new SqlConnection(dbConnString);
                cmd.Connection = cn;
                cn.Open();

                try
                {
                    cmd.Parameters.Add(new SqlParameter("@EventID", EventID));
                    cmd.Parameters.Add(new SqlParameter("@WebApp", WebAppName));
                    cmd.Parameters.Add(new SqlParameter("@Environment", WebAppEnv));
                    cmd.Parameters.Add(new SqlParameter("@Comment", Comment));
                    cmd.Parameters.Add(new SqlParameter("@UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("@UserEmail", UserEmail));
                    cmd.Parameters.Add(new SqlParameter("@UserRole", UserRole));
                    
                    cmd.ExecuteNonQuery();
                    
                    cmd.Dispose();
                    cn.Close();
                }
                catch (Exception exc)
                {
                    DBError = true;
                    DBErrorMessage = exc.Message;
                }
                finally
                {
                    cmd.Dispose();
                    cn.Close();
                }
            }
            
            if (DoEmailErrors || DBError)
            {
                string strData = "\nUSER NAME: " + UserName +
                                 "\nUSER ROLE: " + UserRole +
                                 "\nUSER EMAIL: " + UserEmail +
                                 "\nEVENT ID: " + EventID +
                                 "\nCOMMENTS:\n" + Comment;
                
                string strEmail = ConfigurationManager.AppSettings["ErrorEmailAddress"].ToString();
                if (strEmail.Length > 0)
                {
                    MailAddress recipient = new MailAddress(strEmail);
                    MailAddress sender = new MailAddress(ConfigurationManager.AppSettings["SenderEmailAddress"], ConfigurationManager.AppSettings["SenderName"]);

                    MailMessage mm = new MailMessage(sender, recipient);
                    mm.IsBodyHtml = false;

                    if (DBError)
                    {
                        mm.Subject = "LOGGING DATABASE ERROR";
                        mm.Body = "Database error: " + DBErrorMessage + "\n\n\nORIGINAL ERROR:\n" + strData;
                    }
                    else
                    {
                        mm.Subject = "WEB APP ERROR: " + " " + WebAppName + " " + WebAppEnv + " - COMMENT FROM " + UserName;
                        mm.Body = strData;
                    }

                    try
                    {
                        SmtpClient smtp = new SmtpClient();

                        smtp.Send(mm);
                    }
                    catch (Exception exc)
                    {
                        if (!DBError)
                        {
                            LogException(WebAppName, WebAppEnv, string.Empty, string.Empty, string.Empty, ExceptionSeverity.High, UserName, UserRole, exc);
                        }
                    }
                }
            }

            return !DBError;
        }

        #endregion
    }

   
}

   
