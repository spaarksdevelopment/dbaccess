using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

namespace Aduvo.Common.WebUtilityManager
{
	public class ScriptHelper
	{

		/// <summary>
		/// Create variables on the client side which point to controls
		/// e.g. for server control DropDownList1
		/// client variable DropDownList1 will be created
		/// </summary>
		/// <param name="controlList">the list of controls that are required in client side code</param>
		/// <param name="OnloadFunctions">any other functions that should be called on page load</param>
		/// <param name="prefix">a prefix to attach to variable name.  Use this if the user control might be used more than once on a page to avoid clashes</param>
		public static string CreateClientSideVariablesForControls(List<Control> controlList, string prefix, string OnloadFunctions)
		{
			//not allowed code blocks in the control, so have to do this to get the client ids accross to the client side function

			StringBuilder ClientScript = new StringBuilder();

			foreach (Control ctrl in controlList)
			{
				ClientScript.Append("var ");
				ClientScript.Append(prefix);
				ClientScript.Append(ctrl.ID);
				ClientScript.Append(" = $get('");
				ClientScript.Append(ctrl.ClientID);
				ClientScript.AppendLine("');");
			}

			ClientScript.AppendLine(OnloadFunctions);

			return ClientScript.ToString();

		}

	}
}