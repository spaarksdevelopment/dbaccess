using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Aduvo.Common.UtilityManager;

namespace Aduvo.Common.WebUtilityManager
{
    public static class UIHelper
    {
        #region declarations

        public static string NO_ID = String.Empty;

        #endregion

        /// <summary>
        /// For dynamically created tables - creates a cell containing a textbox
        /// </summary>
        /// <param name="xindex">row identifier</param>
        /// <param name="yindex">column identifier</param>
        /// <param name="CellClassName">css class applied to the cell</param>
        /// <param name="TextBoxClassName">css class applied to the textbox</param>        
        /// <param name="TextBoxValue">value of textbox</param>
        /// <param name="ReadOnly">is textbox readonly?</param>
        /// <returns></returns>
        public static TableCell CreateCellWithTextbox(string xindex, string yindex, string CellClassName, string TextBoxClassName, string TextBoxValue, bool ReadOnly, string OnChange, ref short PageTabIndex)
        {
            TableCell tc = new TableCell();
            tc.CssClass = CellClassName;

            TextBox tb = new TextBox();
            tb.ID = "TextBox" + xindex + "_" + yindex;
            tb.Text = TextBoxValue;

            if (ReadOnly)
            {
                tb.CssClass = TextBoxClassName + " readonly";
                tb.Attributes.Add("onfocus", "blur()");
            }
            else
            {
                tb.CssClass = TextBoxClassName;
                tb.Attributes.Add("onfocus", "select()");
                if (OnChange != string.Empty)
                {
                    tb.Attributes.Add("onchange", OnChange);
                }
                tb.TabIndex = PageTabIndex;
                PageTabIndex++;
            }
            tc.Controls.Add(tb);

            return tc;
        }

        /// <summary>
        /// Binds a dropdown to a datasource, and adds empty item
        /// </summary>
        /// <param name="EmptyItem">An empty item to insert at the beginning - leave as empty string if not desired</param>
        /// <param name="SelectedValue">A value to select - leave as empty string if not desired</param>
        /// <param name="theDropDownList">the DropDown List</param>
        /// <param name="theDataSource">the Data Source</param>
        public static void BindDropDown(string EmptyItem, string SelectedValue, ref DropDownList theDropDownList, ref Object theDataSource)
        {
            theDropDownList.DataSource = theDataSource;
            theDropDownList.DataBind();

            if (EmptyItem != string.Empty)
            {
                ListItem li = new ListItem(EmptyItem);
                theDropDownList.Items.Insert(0, li);
            }

            if (SelectedValue != string.Empty)
            {
                theDropDownList.ClearSelection();
                theDropDownList.SelectedValue = SelectedValue;
            }
        }

        /// <summary>
        /// Finds a control by recursively searching the control tree
        /// </summary>
        /// <param name="root">the starting control to search, usually 'Page'</param>
        /// <param name="id">the id of the control to find</param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control root, string id)
        {

            if (root.ID == id)
            {

                return root;

            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);

                if (t != null)
                {

                    return t;

                }

            }

            return null;

        }

        public static int FindIndexByValue(ListControl control, int? value)
        {
            if (value.HasValue)
                return FindIndexByValue(control, value.Value);
            else
                return -1;
        }

        public static int FindIndexByValue(ListControl control, int value)
        {
            return FindIndexByValue(control, Convert.ToString(value));
        }

        public static int FindIndexByValue(ListControl control, string value)
        {
            return control.Items.IndexOf(control.Items.FindByValue(value));
        }

        public static string GetSelectedValue(ListControl control)
        {
            if (control.SelectedIndex > -1)
                return control.SelectedItem.Value;
            else
                return null;
        }

        public static int GetSelectedIntValue(ListControl control)
        {
            string value = GetSelectedValue(control);

            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int? GetSelectedIntRefValue(ListControl control)
        {
            string value = GetSelectedValue(control);

            if (!string.IsNullOrEmpty(value) && Helper.IsNumeric(value))
            {
                return new int?(Convert.ToInt32(value));
            }
            else
            {
                return new int?();
            }
        }

        public static void SetIndexByText(ListControl control, string text)
        {
            ListItem Item = control.Items.FindByText(text);

            if (Item != null)
            {
                control.SelectedValue = Item.Value;
            }
        }

        public static void SetReadOnly(RadioButtonList rbl)
        {
            foreach (ListItem li in rbl.Items)
            {
                if (!li.Selected) li.Enabled = false;
            }
        }

        /// <summary>
        /// Gets the full URL of the current page
        /// </summary>
        /// <returns></returns>
        public static string GetPageUrl()
        {
            string URL = HttpContext.Current.Request.Url.ToString().ToLower();
            return URL;
        }

        public static void BindListItems(System.Web.UI.WebControls.ListControl listControl, AduvoListItemCollection listItemCollection)
        {
            BindListItems(listControl, listItemCollection, NO_ID);
        }

        public static void BindListItems(System.Web.UI.WebControls.ListControl listControl, AduvoListItemCollection listItemCollection, string selectedValue)
        {
            foreach (AduvoListItem ListItem in listItemCollection.ListItems)
            {
                System.Web.UI.WebControls.ListItem Item = new System.Web.UI.WebControls.ListItem(ListItem.ItemText, ListItem.ItemValue);

                Item.Selected = (ListItem.ItemValue == selectedValue);

                listControl.Items.Add(Item);
            }
        }

        public static void BindListItemsWithPrompt(System.Web.UI.WebControls.ListControl listControl, AduvoListItemCollection listItemCollection, string prompt)
        {
            foreach (AduvoListItem ListItem in listItemCollection.ListItems)
            {
                listControl.Items.Add(new System.Web.UI.WebControls.ListItem(ListItem.ItemText, ListItem.ItemValue));
            }

            if(prompt!=null)
                listControl.Items.Insert(0, new System.Web.UI.WebControls.ListItem(prompt, NO_ID));
        }

        public static void BindListItems(System.Web.UI.WebControls.Table table, string navigateURLForm, AduvoListItemCollection listItemCollection)
        {
            foreach (AduvoListItem ListItem in listItemCollection.ListItems)
            {
                System.Web.UI.WebControls.TableRow Row = new System.Web.UI.WebControls.TableRow();

                System.Web.UI.WebControls.TableCell Cell = new System.Web.UI.WebControls.TableCell();

                System.Web.UI.WebControls.HyperLink Link = new System.Web.UI.WebControls.HyperLink();

                Link.Text = ListItem.ItemText;

                Link.NavigateUrl = String.Format(navigateURLForm, ListItem.ItemValue);

                Cell.Controls.Add(Link);

                Row.Cells.Add(Cell);

                table.Rows.Add(Row);
            }
        }

        /// <summary>
        /// Gets the day number from 1 to 31 if the specified list control has a selected item with a value in this range
        /// </summary>
        /// <param name="listControl"></param>
        /// <returns></returns>
        public static Nullable<byte> GetSelectedDay(System.Web.UI.WebControls.ListControl listControl)
        {
            Nullable<byte> ReturnValue = new Nullable<byte>();

            if (listControl.SelectedItem != null)
            {
                ReturnValue = Number.NullableInt8(listControl.SelectedValue, 1, 31);
            }

            return ReturnValue;
        }

        /// <summary>
        /// Gets the month number from 1 to 12 if the specified list control has a selected item with a value in this range
        /// </summary>
        /// <param name="listControl"></param>
        /// <returns></returns>
        public static Nullable<byte> GetSelectedMonth(System.Web.UI.WebControls.ListControl listControl)
        {
            Nullable<byte> ReturnValue = new Nullable<byte>();

            if (listControl.SelectedItem != null)
            {
                ReturnValue = Number.NullableInt8(listControl.SelectedValue, 1, 12);
            }

            return ReturnValue;
        }

        /// <summary>
        /// Gets the year number if the specified list control has a selected item
        /// </summary>
        /// <param name="listControl"></param>
        /// <returns></returns>
        public static Nullable<Int16> GetSelectedYear(System.Web.UI.WebControls.ListControl listControl)
        {
            Nullable<Int16> ReturnValue = new Nullable<Int16>();

            if (listControl.SelectedItem != null)
            {
                ReturnValue = Number.NullableInt16(listControl.SelectedValue);
            }

            return ReturnValue;
        }
    }
}