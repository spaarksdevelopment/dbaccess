﻿using System;
using System.Collections.Generic;
using System.Web.Caching;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Aduvo.Common.WebUtilityManager
{
    public class XmlHelper
    {
        public static XmlDocument LoadDocument(string filePath)
        {
            return LoadDocument(filePath, true);
        }

        public static XmlDocument LoadDocument(string filePath, bool FromCache)
        {
            XmlDocument doc;

            if (FromCache)
            {
                if (HttpRuntime.Cache[filePath] == null)
                {
                    doc = new XmlDocument();
                    doc.Load(filePath);

                    CacheDependency cd = new CacheDependency(filePath);
                    HttpRuntime.Cache.Add(filePath, doc, cd, Cache.NoAbsoluteExpiration, new TimeSpan(0, 60, 0), CacheItemPriority.AboveNormal, null);
                }
                else
                {
                    doc = (XmlDocument)HttpRuntime.Cache[filePath];
                }
            }
            else
            {
                doc = new XmlDocument();
                doc.Load(filePath);
            }
            return doc;
        }

        /// <summary>
        /// Find all nodes with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static XmlNodeList SelectNodes(string filePath, string xpath)
        {
            return SelectNodes(filePath, xpath, true);
        }

        /// <summary>
        /// Find all nodes with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static XmlNodeList SelectNodes(string filePath, string xpath, bool FromCache)
        {
            XmlDocument document = LoadDocument(filePath, FromCache);
            return document.SelectNodes(xpath);
        }

        /// <summary>
        /// Find node with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static XmlNode SelectNode(string filePath, string xpath)
        {
            return SelectNode(filePath, xpath, true);
        }

        /// <summary>
        /// Find node with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static XmlNode SelectNode(string filePath, string xpath, bool FromCache)
        {
            XmlDocument document = LoadDocument(filePath, FromCache);
            return document.SelectSingleNode(xpath);
        }

        public static string ToXml<T>(T o)
        {
            XmlDocument xDoc = new XmlDocument();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                serializer.Serialize(writer, o);
                xDoc.LoadXml(sb.ToString());
            }
            catch
            {
                return null;
            }
            finally
            {
                if (writer != null) writer.Close();
            }
            return xDoc.OuterXml;
        }
    }
}
