using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Aduvo.Common.WebUtilityManager
{
    public class PageHelper
    {
        public static string GetAbsolutePath(string virtualPath)
        {
            return HttpContext.Current.Server.MapPath(virtualPath);
        }

        public static string BuildUrl(string pageUrl, string pageFolder)
        {
            return BuildUrl(pageUrl, pageFolder, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, bool GetFullURL)
        {
            string builtURL = string.Format("{0}", pageUrl);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId)
        {
            return BuildUrl(pageUrl, pageFolder, recordId, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, bool GetFullURL)
        {
            string builtURL = string.Format("{0}?id={1:d}", pageUrl, recordId);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, int parentId)
        {
            return BuildUrl(pageUrl, pageFolder, recordId, parentId, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, int parentId, bool GetFullURL)
        {
            string builtURL = string.Format("{0}?id={1:d}&pid={2:d}", pageUrl, recordId, parentId);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, EditMode mode)
        {
            return BuildUrl(pageUrl, pageFolder, recordId, mode, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, EditMode mode, bool GetFullURL)
        {
            string builtURL = string.Format("{0}?id={1:d}&m={2:d}", pageUrl, recordId, EditMode.Edit);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, EditMode mode, int customModeId)
        {
            return BuildUrl(pageUrl, pageFolder, recordId, mode, customModeId, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, EditMode mode, int customModeId, bool GetFullURL)
        {
            string builtURL = string.Format("{0}?id={1:d}&m={2:d}&cm={3:d}", pageUrl, recordId, EditMode.Edit, customModeId);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, int parentId, EditMode mode)
        {
            return BuildUrl(pageUrl, pageFolder, recordId, parentId, mode, false);
        }

        public static string BuildUrl(string pageUrl, string pageFolder, int recordId, int parentId, EditMode mode, bool GetFullURL)
        {
            string builtURL = string.Format("{0}?id={1:d}&pid={2:d}&m={3:d}", pageUrl, recordId, parentId, EditMode.Edit);
            if (GetFullURL) builtURL = GetFullPageUrl(builtURL, pageFolder);
            return builtURL;
        }
                
        public static string ReferenceScript(string scriptFile)
        {
            var filePath = VirtualPathUtility.ToAbsolute(scriptFile);
            return "<script type=\"text/javascript\" src=\"" + filePath + "\"></script>";
        }

        public static string GetFullPageUrl(string PagePath, string folderName)
        {
            string URL = HttpContext.Current.Request.Url.ToString().ToLower();

            int pos = 0;

            if (folderName != string.Empty)
            {
                folderName = folderName.ToLower();
                if(!folderName.StartsWith("/"))
                    folderName = "/" + folderName;
                if (!folderName.EndsWith("/"))
                    folderName += "/";

                pos = URL.IndexOf(folderName);
            }

            if(pos>0)
                URL = URL.Substring(0, pos + folderName.Length);
            else
            {
                pos = URL.LastIndexOf("/");
                URL = URL.Substring(0, pos);
            }
            
            URL += PagePath;
            return URL;
        }
    }

    public enum EditMode
    {
        ReadOnly, Edit
    }
}