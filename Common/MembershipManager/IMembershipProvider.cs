﻿using System.Web.Security;

namespace Aduvo.Common.MembershipManager
{
    public interface IMembershipProvider
    {
        /// <summary>
        /// Get a users Id
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <returns></returns>
        int? GetUserID(string email);
       

        /// <summary>
        /// Find ApplicationUsers for specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        MembershipUserCollection FindUsersByName(string name);

        /// <summary>
        /// Disable a user
        /// </summary>
        /// <param name="userName">the users email address</param>
        /// <returns></returns>
        bool LockUser(string userName);

        /// <summary>
        /// Get an application user from the database by username
        /// </summary>
        /// <param name="searchUserName">username</param>
        /// <param name="GetRoleInfo">to get role info?</param>
        /// <returns>ApplicationUser</returns>
        ApplicationUser GetApplicationUser(string searchUserName);

        /// <summary>
        /// Get an application user from the database by user id
        /// </summary>
        /// <param name="searchUserId">the user Id</param>
        /// <param name="GetRoleInfo">to get role info?</param>
        /// <returns>ApplicationUser</returns>
        ApplicationUser GetApplicationUser(int searchUserId);
                
        /// <summary>
        /// Checks if an Email address is available
        /// </summary>
        /// <param name="Email">the email address</param>
        /// <returns>true if it is available</returns>
        bool CheckEmailAvailable(string Email);

        /// <summary>
        /// Update the user activity
        /// </summary>
        /// <param name="userName">username (only required if id missing)</param>
        /// <param name="userId">user id (only required if username missing)</param>
        /// <param name="isLogin">is this the initial login?</param>
        void UpdateUserActivity(string userName, int userId, bool isLogin);
    }
}
