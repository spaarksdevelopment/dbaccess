﻿using System;
using System.Configuration.Provider;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;

using Aduvo.Common.UtilityManager;

namespace Aduvo.Common.MembershipManager
{
    [Serializable]
    public class ApplicationUser : MembershipUser
    {   
        public ApplicationUser(string providername,
                                 string username,
                                 object providerUserKey,
                                 string email,
                                 string passwordQuestion,
                                 string comment,
                                 bool isApproved,
                                 bool isLockedOut,
                                 DateTime creationDate,
                                 DateTime lastLoginDate,
                                 DateTime lastActivityDate,
                                 DateTime lastPasswordChangedDate,
                                 DateTime lastLockedOutDate)
            :
                                  base(providername,
                                       username,
                                       providerUserKey,
                                       email,
                                       passwordQuestion,
                                       comment,
                                       isApproved,
                                       isLockedOut,
                                       creationDate,
                                       lastLoginDate,
                                       lastActivityDate,
                                       lastPasswordChangedDate,
                                       lastLockedOutDate)
        {
            base.Email = username;
            this.Enabled = isApproved & !isLockedOut;
        }

        public ApplicationUser(string providername, ApplicationUser au)
            : base(providername, au.Email, au.Id, au.Email, string.Empty, string.Empty, au.IsApproved, au.IsLockedOut, au.CreationDate,au.LastLoginDate,au.LastActivityDate,au.LastPasswordChangedDate,au.LastLockoutDate)
        {
            this.Fullname = au.Fullname;
            this.Forename = au.Forename;
            this.Surname = au.Surname;
            this.Enabled = au.Enabled;
            this.Telephone = au.Telephone;
            this.CreatedByUserId = au.CreatedByUserId;
            this.UpdatedByUserId = au.UpdatedByUserId;
            this.VisitCount = au.VisitCount;
            this.Roles = au.Roles;
            this.RoleList = au.RoleList;
            this.dbPeopleID = au.dbPeopleID;
        }

        public string Fullname
        {
            get
            {
                if ((_surname == string.Empty) & (_forename == string.Empty)) GetNameFromEmail();
                return _forename + " " + _surname;
            }
            set
            {

            }
        }

        //this is the mp_UserID / DBDirID not the DBPeopleID
        public int Id
        {
            get { return Convert.ToInt32(base.ProviderUserKey); }
        }

        public int dbPeopleID
        {
            get { return _dbPeopleID; }
            set { _dbPeopleID = value; }
        }

        private int _dbPeopleID = 0;
        private string _forename = string.Empty;

        public string Forename
        {
            get { return _forename; }
            set { _forename = value; }
        }
        private string _surname = string.Empty; 

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        private string _telephone = string.Empty;

        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }

        private bool _Enabled;

        public bool Enabled
        {
            get { return _Enabled; }
            set { _Enabled = value; }
        }

        private int _CreatedByUserId;

        public int CreatedByUserId
        {
            get { return _CreatedByUserId; }
            set { _CreatedByUserId = value; }
        }

        private int _UpdatedByUserId;

        public int UpdatedByUserId
        {
            get { return _UpdatedByUserId; }
            set { _UpdatedByUserId = value; }
        }

        private int _VisitCount;

        public int VisitCount
        {
            get { return _VisitCount; }
            set { _VisitCount = value; }
        }
        
        public string DisplayName
        {
            get
            {
                if ((_surname == string.Empty) & (_forename == string.Empty)) GetNameFromEmail();
                return _forename + " " + _surname + " (" + base.Email + ")";
            }
        }

        /// <summary>
        /// List containing roles
        /// </summary>
        private List<string> _roles = new List<string>();
        public List<string> Roles
        {
            get { return _roles; }
            set { _roles = value; }
        }

        /// <summary>
        /// string containing roles
        /// </summary>
        private string _rolelist = string.Empty;
        public string RoleList
        {
            get { return _rolelist; }
            set { _rolelist = value; }
        }

        public bool IsInRole(string RoleName)
        {
            return _roles.Contains(RoleName);
        }
               
        private void GetNameFromEmail()
        {
            //get name by splitting email address
            string _EmailAddress = base.Email;
            if (_EmailAddress != string.Empty)
            {
                int pos = _EmailAddress.IndexOf("@");
                string fullname = _EmailAddress.Substring(0, pos).Replace("."," ");
                fullname = Helper.ToTitleCase(fullname);
                pos = fullname.LastIndexOf(" ");
                if (pos > 0)
                {
                    _surname = fullname.Substring(pos + 1);
                    _forename = fullname.Substring(0, pos);
                }
                else
                {
                    _surname = fullname;
                }
            }
       }

                    
    }

}
