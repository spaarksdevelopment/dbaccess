﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using System.Web.Configuration;
using System.Xml;
using Aduvo.Common.UtilityManager;
using Aduvo.Common.WebUtilityManager;

namespace Aduvo.Common.MembershipManager
{
    /// <summary>
    /// DB Membership Provider controls all aspects of user management, and blends user table data with DB Group Directory data
    /// </summary>
    public class XmlMembershipProvider : ApplicationMembershipProvider
    {
        private string XmlDataVirtualFilePath = @"~\APP_DATA\users.xml";
        private string XmlDataAbsoluteFilePath = string.Empty;

        #region ctor(s)

        public XmlMembershipProvider()
        {            
        }

        #endregion

        #region Initialize

        public override void Initialize(string name, NameValueCollection config)
        {
            if (name == null || name.Length == 0)
            {
                name = "XmlMembershipProvider";
            }

            string ConfigFilePath = ConfigurationManager.AppSettings["UserDataAbsoluteFilePath"];
            if (ConfigFilePath == string.Empty)
            {
                ConfigFilePath = ConfigurationManager.AppSettings["UserDataVirtualFilePath"];
                if (ConfigFilePath != string.Empty)
                {
                    XmlDataVirtualFilePath = ConfigFilePath;            
                }
                XmlDataAbsoluteFilePath = PageHelper.GetAbsolutePath(XmlDataVirtualFilePath);
            }
            else
            {
                XmlDataAbsoluteFilePath = ConfigFilePath;
            }
            
            base.Initialize(name, config);
        }

        #endregion

        #region CreateUser

        /// <summary>
        /// Add a new user (default provider method with some unnecessary arguments)
        /// </summary>
        /// <param name="username">users email address</param>
        /// <param name="password">N/A</param>
        /// <param name="email">users email address</param>
        /// <param name="passwordQuestion">N/A</param>
        /// <param name="passwordAnswer">N/A</param>
        /// <param name="isApproved">Enabled</param>
        /// <param name="providerUserKey">N/A</param>
        /// <param name="status">Output parameter for status</param>
        /// <returns></returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            //get user in group directory
            int UserId = 0;
            int CreatedById = 0;
            DateTime LastLoggedIn = DateTime.Now;
            
            //set up a blank user to return
            MembershipUser mu = new ApplicationUser("XMLMembershipProvider", string.Empty, 0, string.Empty, string.Empty, string.Empty, false, false, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);

            if(!Helper.ValidateEmailAddress(username))
            {
                status = MembershipCreateStatus.InvalidEmail;
                return mu;
            }

            XmlDocument document = XmlHelper.LoadDocument(XmlDataAbsoluteFilePath, true);
            XmlNode node = document.SelectSingleNode(string.Format("//user[@EmailAddress='{0}']", email));

            if (node!=null)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return (MembershipUser)CreateApplicationUserFromNode(node);
            }

            if (providerUserKey == null)
            {
                //assign a random number as the user Id
                Random random = new Random();
                do
                {
                    UserId = random.Next();
                }
                while (FindById(UserId).Count > 0);
            }
            else
                UserId = Convert.ToInt32(providerUserKey);

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                //another user is registering this one, get their ID

                CreatedById = GetUserID(HttpContext.Current.User.Identity.Name).Value;
                LastLoggedIn = DateTime.MinValue;
            }
            else
            {
                //user is registering self
                CreatedById = UserId;
            }

            //select the first node, and clone it
            node = document.SelectSingleNode(string.Format("//user", email)).Clone();
            
            //derive the name
            string fullname = base.DeriveNameFromEmail(email);
            string surname = string.Empty;
            string forename = string.Empty;

            int pos = fullname.LastIndexOf(" ");
            if (pos > 0)
            {
                surname = fullname.Substring(pos + 1);
                forename = fullname.Substring(0, pos);
            }
            else
            {
                surname = fullname;
            }
            
            //set the attributes
            node.Attributes["ID"].Value = UserId.ToString();
            node.Attributes["Name"].Value = fullname;
            node.Attributes["EmailAddress"].Value = email;
            node.Attributes["Forename"].Value = forename;
            node.Attributes["Surname"].Value = surname;
            node.Attributes["Telephone"].Value = string.Empty;
            node.Attributes["Enabled"].Value = isApproved.ToString();

            //insert the new node
            document.SelectSingleNode("//users").AppendChild(node);

            status = MembershipCreateStatus.Success;
            //get user that was created
            return CreateApplicationUserFromNode(node);
        }

        #endregion

        #region GetUserID

        public override int? GetUserID(string username)
        {
            XmlNodeList xnl = FindByEmail(username);
            if (xnl.Count == 0)
                return null;
            else
            {
                return Helper.SafeInt(xnl[0].Attributes["ID"].Value);
            }
        }

        #endregion

        #region FindUsersByName

        /// <summary>
        /// Find users by name (from Group Directory)
        /// </summary>
        /// <param name="usernameToMatch">Name to search for</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">output parameter for total records</param>
        /// <returns>a collection of user objects</returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection muc = GetApplicationUsersByEmail(usernameToMatch);
            
            totalRecords = muc.Count;
            return muc;
        }

        #endregion

        #region GetAllUsers

        /// <summary>
        /// Get all users
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">output parameter for total records</param>
        /// <returns>a collection of user objects</returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection muc = GetApplicationUsers();
            
            totalRecords = muc.Count;
            return muc;
        }

        #endregion

        #region ValidateUser

        /// <summary>
        /// Validate password, and check if they are a registered user
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>true if password correct and user is registered</returns>
        public override bool ValidateUser(string username, string password)
        {
            // check strName is in Group Directory, and password correct

            bool isValid = true;

            //change to lower case for consistency
            username = username.ToLower();
            isValid = (GetApplicationUser(username) != null);
            
            return isValid;
        }

        #endregion

        #region FindUsersByName

        public override MembershipUserCollection FindUsersByName(string name)
        {
            return GetApplicationUsersByName(name);
        }

        #endregion

        #region GetApplicationUser

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override ApplicationUser GetApplicationUser(int id)
        {
            XmlNodeList xnl = FindById(id);
            if (xnl.Count > 0) return CreateApplicationUserFromNode(xnl[0]);
            else return null;
        }

        /// <summary>
        /// GetUserByEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns>ApplicationUser</returns>
        public override ApplicationUser GetApplicationUser(string email)
        {
            XmlNodeList xnl = FindByEmail(email);
            if (xnl.Count > 0) return CreateApplicationUserFromNode(xnl[0]);
            else return null;
        }

        #endregion
        
        #region Private Methods

        #region GetApplicationUserByName

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private ApplicationUser GetApplicationUserByName(string name)
        {
            XmlNodeList xnl = FindByName(name);
            if (xnl.Count > 0) return CreateApplicationUserFromNode(xnl[0]);
            else return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private MembershipUserCollection GetApplicationUsersByName(string name)
        {
           return CreateApplicationUserList(FindByName(name));
        }

        #endregion

        #region GetApplicationUsersByID
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private MembershipUserCollection GetApplicationUsersByID(int id)
        {
            return CreateApplicationUserList(FindById(id));
        }

        #endregion

        #region GetApplicationUsersByEmail
        
        /// <summary>
        /// GetUsersByEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns>ApplicationUserList</returns>
        private MembershipUserCollection GetApplicationUsersByEmail(string email)
        {
            return CreateApplicationUserList(FindByEmail(email));
        }

        #endregion

        #region GetApplicationUsers

        /// <summary>
        /// GetUsersByEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns>ApplicationUserList</returns>
        private MembershipUserCollection GetApplicationUsers()
        {
            return CreateApplicationUserList(XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, "//user"));
        }

        private MembershipUserCollection CreateApplicationUserList(XmlNodeList nodeList)
        {
            MembershipUserCollection ApplicationUserList = new MembershipUserCollection();

            foreach (XmlNode node in nodeList)
            {
                ApplicationUserList.Add(CreateApplicationUserFromNode(node));
            }

            return ApplicationUserList;
        }

        #endregion
 
        #region CreateApplicationUserFromNode

        /// <summary>
        /// Create ApplicationUser from specified XmlNode.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private ApplicationUser CreateApplicationUserFromNode(XmlNode node)
        {
            ApplicationUser ApplicationUser = null;

            if (node != null)
            {
                string id = node.Attributes["ID"].Value;
                string emailAddress = node.Attributes["EmailAddress"].Value;
                string name = node.Attributes["Name"].Value;
                string forename = node.Attributes["Forename"].Value;
                string surname = node.Attributes["Surname"].Value;
                              

                ApplicationUser = new ApplicationUser("XMLMembershipProvider", emailAddress, id, emailAddress, string.Empty, string.Empty, true, false, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime())
                {
                    Forename = forename,
                    Surname = surname
                };
            }

            return ApplicationUser;
        }

        #endregion

        #region FindByName

        /// <summary>
        /// Find all nodes with specified email address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private XmlNodeList FindByName(string name)
        {
            name = Helper.ToTitleCase(name);
            //TODO: split name into components
            return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, string.Format("//user[contains(@Name, '{0}')]", name));
        }

        #endregion

        #region FindByEmail

        /// <summary>
        /// Find all nodes with specified email address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private XmlNodeList FindByEmail(string email)
        {
            return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, string.Format("//user[@EmailAddress='{0}']", email));
        }

        #endregion

        #region FindById

        /// <summary>
        /// Find all nodes with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private XmlNodeList FindById(int id)
        {
            return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, string.Format("//user[@ID='{0}']", id));
        }

        #endregion

        #endregion

        #region CheckEmailAvailable

        public override bool CheckEmailAvailable(string Email)
        {
            XmlNodeList xnl = FindByEmail(Email);

            return (xnl.Count==0);
        }

        #endregion
    }
}  