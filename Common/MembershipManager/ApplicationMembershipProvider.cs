﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using Aduvo.Common.UtilityManager;

namespace Aduvo.Common.MembershipManager
{
    /// <summary>
    ///Application Membership Provider is a standardised provider which controls all aspects of user management
    /// </summary>
    public class ApplicationMembershipProvider : MembershipProvider, IMembershipProvider
    {

        #region constructor
        public ApplicationMembershipProvider()
        {
            this.ApplicationName = ConfigurationManager.AppSettings["AppName"];
            if (ConfigurationManager.AppSettings["MembershipConnectionString"] != null)
                this.MembershipConnectionString = ConfigurationManager.AppSettings["MembershipConnectionString"];
            else if (ConfigurationManager.AppSettings["ActiveConnectionString"] != null)
                this.MembershipConnectionString = ConfigurationManager.AppSettings["ActiveConnectionString"];
            else
                throw new Exception("Connection string must be defined in 'ActiveConnectionString' or 'MembershipConnectionString' AppSetting");

            int cacheMinutes = 60;
            if (ConfigurationManager.AppSettings["UserCacheMinutes"] != null)
            {
                cacheMinutes = Helper.SafeInt(ConfigurationManager.AppSettings["UserCacheMinutes"]);
            }
            cacheTimespan = new TimeSpan(0, cacheMinutes, 0);
        }
        #endregion


        #region properties

        protected TimeSpan cacheTimespan;
        
        public override string ApplicationName { get; set; }
        public string MembershipConnectionString { get; set; }
        
        #endregion

        #region Initialize

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);
        }

        #endregion

        #region static methods

        public static bool GetCurrentIsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static string GetCurrentUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        #endregion

        #region CreateUser

        /// <summary>
        /// Add a new user to the database
        /// </summary>
        /// <param name="username">The user's name</param>
        /// <param name="email">Email address</param>
        /// <param name="isApproved">Enabled or not</param>
        /// <param name="providerUserKey">User Id</param>
        /// <param name="status">output param for status</param>
        /// <returns></returns>
        public virtual MembershipUser CreateUser(string fullname, string email, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            return CreateUser(fullname, string.Empty, email, string.Empty, string.Empty, isApproved, providerUserKey, out status);
        }
       
        /// <summary>
        /// Add a new user to the database
        /// </summary>
        /// <param name="username">The Users Name</param>
        /// <param name="password">N/A</param>
        /// <param name="email">Email address</param>
        /// <param name="passwordQuestion">N/A</param>
        /// <param name="passwordAnswer">N/A</param>
        /// <param name="isApproved">Enabled or not</param>
        /// <param name="providerUserKey">User Id</param>
        /// <param name="status">output param for status</param>
        /// <returns></returns>        
        public override MembershipUser CreateUser(string fullname, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            //get current user ID
            MembershipUser mu = Membership.GetUser();
            int CreatedByUserId = (int)mu.ProviderUserKey;

            string Forename = string.Empty;
            string Surname = string.Empty;

            if (fullname.Contains(" "))
            {
                int pos = fullname.LastIndexOf(" ");
                Forename = fullname.Substring(0, pos);
                Surname = fullname.Substring(pos + 1);
            }
            else
            {
                Forename = fullname;
            }

            if (InsertUser(email, isApproved, providerUserKey, CreatedByUserId, Forename, Surname, string.Empty))
            {
                status = MembershipCreateStatus.Success;
                return GetUser((int)providerUserKey,false);
            }
            else
            {
                status = MembershipCreateStatus.ProviderError;
                return null;
            }                        
        }

        protected virtual bool InsertUser(string email, bool isApproved, object providerUserKey, int CreatedByUserId, string Forename, string Surname, string Telephone)
        {
            bool success;
            /*
             mp_AddMembershipUser 
                @UserId int
                ,@EmailAddress varchar(255)
                ,@Forename varchar(255)
                ,@Surname varchar(255)
                ,@Telephone varchar(255)
                ,@CreatedByUserId int
                ,@Enabled bit = 1
             */

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
              SqlCommand cmd = new SqlCommand("mp_AddMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", providerUserKey));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", email));
                cmd.Parameters.Add(new SqlParameter("Forename", Forename));
                cmd.Parameters.Add(new SqlParameter("Surname", Surname));
                cmd.Parameters.Add(new SqlParameter("Telephone", Telephone));
                cmd.Parameters.Add(new SqlParameter("CreatedByUserId", CreatedByUserId));
                cmd.Parameters.Add(new SqlParameter("Enabled", isApproved));

                try
                {
                    cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                    success = false;
                }

                conn.Close();
            }
            return success;
        }

        #endregion

        #region GetUser

        public virtual ApplicationUser GetUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return (ApplicationUser)GetUser(HttpContext.Current.User.Identity.Name, true);
            }
            else
                return null;
        }

        /// <summary>
        /// Geta a user object by their email address - checks cache first
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="userIsOnline">update user activity flag</param>
        /// <returns>Membership user object with full directory data</returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            ApplicationUser applicationUser = null;

            if (HttpContext.Current.Cache["UserByEmail_" + username] != null)
            {
                applicationUser = (ApplicationUser)HttpContext.Current.Cache["UserByEmail_" + username];
            }
            else
            {
                applicationUser = GetApplicationUser(username);

                if (applicationUser!=null)
                {
                    AddUserToCache(applicationUser);
                }
                else
                {
                    throw new ProviderException("User not found");
                }
             }

            UpdateActivity(username, 0, userIsOnline);

            return applicationUser;
        }
              

        /// <summary>
        /// Geta a user object by their Id - checks cache first
        /// </summary>
        /// <param name="providerUserKey">the user Id</param>
        /// <param name="userIsOnline">update user activity flag</param>
        /// <returns>Membership user object with full directory data</returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            int userId = Convert.ToInt32(providerUserKey);
            
            ApplicationUser applicationUser;

            //check cache for this user

            if (HttpContext.Current.Cache["UserById_" + userId.ToString()] != null)
            {
                applicationUser = (ApplicationUser)HttpContext.Current.Cache["UserById_" + userId.ToString()];
                return applicationUser;
            }

            applicationUser = GetApplicationUser(userId);

            if (applicationUser!=null)
            {   
                AddUserToCache(applicationUser);
            }
            
            UpdateActivity(string.Empty, userId, userIsOnline);

            return applicationUser;
        }
        
        #endregion

        #region GetUserId

        public virtual int? GetUserID(string username)
        {
            ApplicationUser au = (ApplicationUser)GetUser(username, false);

            if (au != null)
                return au.Id;
            else
                return null;
        }

        #endregion

        #region GetUserNameByEmail

        /// <summary>
        /// Get a users name
        /// </summary>
        /// <param name="email">the email address of the user</param>
        /// <returns></returns>
        public override string GetUserNameByEmail(string email)
        {
            string _name = string.Empty;

            //check cache for this user's name first
            if (HttpContext.Current.Cache["UserNameByEmail_" + email] != null)
            {
                _name = (string)HttpContext.Current.Cache["UserNameByEmail_" + email];
            }
            else
            {
                ApplicationUser applicationUser = (ApplicationUser)GetUser(email, false);
                _name = applicationUser.UserName;

                //add name to cache for easy retrieval later
                HttpContext.Current.Cache.Add("UserNameByEmail_" + email, _name, null, System.Web.Caching.Cache.NoAbsoluteExpiration, this.cacheTimespan, CacheItemPriority.Normal, null);
            }

            return _name;
        }

        #endregion

        #region FindUsersByName

        public virtual MembershipUserCollection FindUsersByName(string usernameToMatch)
        {
            int totalRecords;
            return FindUsersByName(usernameToMatch, 0, 0, out totalRecords);
        }
        
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("User");

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_GetMembershipUsersByName", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("Name", usernameToMatch));

                sda.SelectCommand = cmd;
                sda.Fill(dt);

                conn.Close();
            }
            
            totalRecords = dt.Rows.Count;

            MembershipUserCollection muc = new MembershipUserCollection();

            if (totalRecords > 0)
            {
                ApplicationUser au = GetApplicationUser(dt.Rows[0]);
                if (au != null)
                    muc.Add(au);
            }
                        
            return muc;
        }

        #endregion

        #region GetAllUsers

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("User");

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_GetMembershipUsersByName", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                sda.SelectCommand = cmd;
                sda.Fill(dt);

                conn.Close();
            }

            totalRecords = dt.Rows.Count;

            MembershipUserCollection muc = new MembershipUserCollection();

            if (totalRecords > 0)
            {
                //ApplicationUser au = GetApplicationUser(dt.Rows[0]);
                muc = GetApplicationUsers(dt);
                //if (au != null)
                  //  muc.Add(au);
            }

            return muc;
        }

        #endregion

        #region FindUsersByEmail

        /// <summary>
        /// Find users by email - there can be only one
        /// </summary>
        /// <param name="emailToMatch">the email address</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns>a collection of user objects</returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            //these can be only one user which matches the email
            MembershipUserCollection muc = new MembershipUserCollection();

            try
            {
                MembershipUser mu = GetUser(emailToMatch, false);
                totalRecords = 1;
                muc.Add(mu);
            }
            catch
            {
                totalRecords = 0;
            }

            return muc;
        }

        #endregion

        #region ChangePassword

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ChangePasswordQuestionAndAnswer

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region DeleteUser

        /// <summary>
        /// Delete a user from the database
        /// </summary>
        /// <param name="username">the user's email address</param>
        /// <param name="deleteAllRelatedData">N/A</param>
        /// <returns></returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return false;
        }

        #endregion

        #region EnablePasswordReset

        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        #endregion

        #region EnablePasswordRetrieval

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        #endregion

        #region Requires

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }
        
        #endregion
        
        #region NotImplemented members
        
        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }
           
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
     
        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region UnlockUser

        /// <summary>
        /// Enable a user
        /// </summary>
        /// <param name="userName">the users email address</param>
        /// <returns></returns>
        public override bool UnlockUser(string userName)
        {
            ApplicationUser applicationUser = GetApplicationUser(userName);

            if (applicationUser!=null)
            {
                applicationUser.Enabled = true;
                UpdateUser(applicationUser);
                                
                return true;
            }
            else 
                return false;
        }

        /// <summary>
        /// Enable a user
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <returns></returns>
        public bool UnlockUser(int userId)
        {
            ApplicationUser applicationUser = GetApplicationUser(userId);

            if (applicationUser != null)
            {
                applicationUser.Enabled = true;
                UpdateUser(applicationUser);

                return true;
            }
            else
                return false;
        }

        #endregion

        #region LockUser

        /// <summary>
        /// Disable a user
        /// </summary>
        /// <param name="userName">the users email address</param>
        /// <returns></returns>
        public bool LockUser(string userName)
        {
            ApplicationUser applicationUser = GetApplicationUser(userName);

            if (applicationUser != null)
            {
                applicationUser.Enabled = false;
                UpdateUser(applicationUser);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Disable a user
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <returns></returns>
        public bool LockUser(int userId)
        {
            ApplicationUser applicationUser = GetApplicationUser(userId);

            if (applicationUser != null)
            {
                applicationUser.Enabled = false;
                UpdateUser(applicationUser);

                return true;
            }
            else
                return false;
        }

        #endregion

        #region UpdateUser

        /// <summary>
        /// Update a user
        /// </summary>
        /// <param name="user">The user object</param>
        public override void UpdateUser(MembershipUser user)
        {
            //cast as ApplicationUser 
            ApplicationUser applicationUser = (ApplicationUser)user;
            
            if(SaveApplicationUser(applicationUser)){
                AddUserToCache(applicationUser);
            }
            
        }

        #endregion

        #region GetApplicationUser

        /// <summary>
        /// Get an application user from the database
        /// </summary>
        /// <param name="searchUserName">username</param>
        /// <param name="GetRoleInfo">to get role info?</param>
        /// <returns>ApplicationUser</returns>
        public virtual ApplicationUser GetApplicationUser(string searchUserName)
        {
            bool found = false;
            ApplicationUser au = null;

            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dtUser = new DataTable("User");

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_GetMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("EmailAddress", searchUserName));

                sda.SelectCommand = cmd;
                sda.Fill(dtUser);

                if (dtUser.Rows.Count > 0)
                {
                    found = true;
                    au = GetApplicationUser(dtUser.Rows[0]);
                }
                
                if (found)
                {
                    //Now get the Roles
                    DataTable dtRoles = new DataTable("Roles");

                    cmd = new SqlCommand("mp_GetSecurityGroups", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("EmailAddress", searchUserName));

                    sda.SelectCommand = cmd;
                    sda.Fill(dtRoles);

                    List<string> Roles = new List<string>();
                    string Role = string.Empty;
                    string RoleList = string.Empty;

                    bool FirstRole = true;
                    foreach (DataRow dr in dtRoles.Rows)
                    {
                        Role = dr["RoleName"].ToString();
                        if (Role != string.Empty)
                        {
                            Roles.Add(dr["RoleShortName"].ToString());
                            if(!FirstRole) RoleList += ", ";
                            RoleList += Role;
                            FirstRole = false;
                        }
                    }
                    
                    au.Roles = Roles;
                    au.RoleList = RoleList;
                }
                conn.Close();
            }

            if (found)
            {
                return au;
            }
            else
                return null;
        }
        
        /// <summary>
        /// Get an application user from the database by user id
        /// </summary>
        /// <param name="searchUserId">the user Id</param>
        /// <param name="GetRoleInfo">to get role info?</param>
        /// <returns>ApplicationUser</returns>
        public virtual ApplicationUser GetApplicationUser(int searchUserId)
        {
            bool found = false;
            ApplicationUser au = null;

            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dtUser = new DataTable("User");

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_GetMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", searchUserId));

                sda.SelectCommand = cmd;
                sda.Fill(dtUser);

                if (dtUser.Rows.Count > 0)
                {
                    found = true;
                    au = GetApplicationUser(dtUser.Rows[0]);
                }

                if (found)
                {
                    //Now get the Roles
                    DataTable dtRoles = new DataTable("Roles");
                 
                    cmd = new SqlCommand("mp_GetSecurityGroups", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("EmailAddress", au.Email));

                    sda.SelectCommand = cmd;
                    sda.Fill(dtRoles);

                    List<string> Roles = new List<string>();
                    string Role = string.Empty;
                    string RoleList = string.Empty;

                    bool FirstRole = true;
                    foreach (DataRow dr in dtRoles.Rows)
                    {
                        Role = dr["RoleName"].ToString();
                        if (Role != string.Empty)
                        {
                            Roles.Add(dr["RoleShortName"].ToString());
                            if (!FirstRole) RoleList += ", ";
                            RoleList += Role;
                            FirstRole = false;
                        }
                    }

                    au.Roles = Roles;
                    au.RoleList = RoleList;
                }
                conn.Close();
            }

            if (found)
            {
                return au;
            }
            else
                return null;
        }

        protected MembershipUserCollection GetApplicationUsers(DataTable dt)
        {
            MembershipUserCollection aul = new MembershipUserCollection();

            foreach (DataRow dr in dt.Rows)
            {
                ApplicationUser au = GetApplicationUser(dr);

                if (au != null) aul.Add(au);
            }

            return aul;
        }

        protected ApplicationUser GetApplicationUser(DataRow dr)
        {            
            int userId = (int)dr["Id"];
            string email = dr["EmailAddress"].ToString();
            bool enabled = (bool)dr["Enabled"];
            DateTime created = Helper.SafeDateTime(dr["CreatedDateTime"]);
            DateTime lastLogin = Helper.SafeDateTime(dr["LastLoggedInDateTime"]);
            DateTime lastActivity = Helper.SafeDateTime(dr["LastActivityDateTime"]);

            //create object using the standard constructor
            ApplicationUser au = new ApplicationUser(Membership.Provider.Name, email, userId, email, "", "", enabled, !enabled, created, lastLogin, lastActivity, new DateTime(), new DateTime());

            //now set custom fields
            au.Forename = dr["Forename"].ToString();
            au.Surname = dr["Surname"].ToString();
            au.Telephone = dr["Telephone"].ToString();
            au.CreatedByUserId = Helper.SafeInt(dr["CreatedByUserId"]);
            //au.UpdatedByUserId = (int)dr["UpdatedByUserId"];
            au.VisitCount = Helper.SafeInt(dr["VisitCount"]);
            au.dbPeopleID = Helper.SafeInt(dr["dbPeopleID"]);
            return au;
        }

        #endregion

        #region SaveApplicationUser

        /// <summary>
        /// Save an application User - override if using a different data structure
        /// </summary>
        /// <param name="applicationUser">the ApplicationUser object</param>
        /// <returns>true is success</returns>
        protected virtual bool SaveApplicationUser(ApplicationUser applicationUser)
        {
            /*
             [dbo].[mp_UpdateMembershipUser] 
                @UserID int,
                @EmailAddress varchar(255),
                @Forename varchar(255),
                @Surname varchar(255),
                @Telephone varchar(255),
                @Enabled bit
             */

            int rows = 0;

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_UpdateMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", applicationUser.dbPeopleID));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", applicationUser.Email));
                cmd.Parameters.Add(new SqlParameter("Forename", applicationUser.Forename));
                cmd.Parameters.Add(new SqlParameter("Surname", applicationUser.Surname));
                cmd.Parameters.Add(new SqlParameter("Telephone", applicationUser.Telephone));
                //cmd.Parameters.Add(new SqlParameter("UpdatedByUserId", applicationUser.UpdatedByUserId));
                cmd.Parameters.Add(new SqlParameter("Enabled", applicationUser.Enabled));

                rows = cmd.ExecuteNonQuery();
                conn.Close();
            }
            
            return (rows>0);
        }

        #endregion
        
        #region UpdateActivity

        protected void UpdateActivity(string userName, int userId, bool userIsOnline)
        {
            //in order to avoid updating user activity many times in one request, 
            //ensure a minute has elapsed since the last time user activity was updated
            //by caching a variable and allowing it to expire

            string cachekey = "UserActivity_" + userName + "_" + userId;

            if (userIsOnline)
            {
                object ActivityCache = HttpContext.Current.Cache[cachekey];
                if (ActivityCache == null)
                {
                    UpdateUserActivity(userName, userId, false);
                    HttpContext.Current.Cache.Insert(cachekey, true, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero);
                }
            }
        }

        /// <summary>
        /// Update the user activity
        /// </summary>
        /// <param name="userName">username (only required if id missing)</param>
        /// <param name="userId">user id (only required if username missing)</param>
        /// <param name="isLogin">is this the initial login?</param>
        public virtual void UpdateUserActivity(string userName, int userId, bool isLogin)
        {
            if (isLogin) RemoveUserFromCache(userName, userId.ToString());

            /*
                mp_UpdateUserActivity]
	                @EmailAddress VARCHAR(200) = '',
	                @UserId INT = 0,
	                @Login BIT = 0
            */

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_UpdateUserActivity", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", userId));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", userName));
                cmd.Parameters.Add(new SqlParameter("Login", isLogin));

                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }
        
        #endregion

        #region CheckEmailAvailable

        public virtual bool CheckEmailAvailable(string Email)
        {
            return true;
        }

        #endregion

        #region DeriveNameFromEmail

        public virtual string DeriveNameFromEmail(string _EmailAddress)
        {
            //get name by splitting email address
            if (_EmailAddress != string.Empty)
            {
                int pos = _EmailAddress.IndexOf("@");
                string fullname = _EmailAddress.Substring(0, pos).Replace(".", " ");
                fullname = Helper.ToTitleCase(fullname);
                return fullname;
            }
            else return string.Empty;
        }

        #endregion

        #region ValidateUser
                
        /// <summary>
        /// Validate password, and check if they are a registered user
        /// </summary>
        /// <param name="username">email address</param>
        /// <param name="password">password</param>
        /// <returns>true if user is registered</returns>
        public override bool ValidateUser(string username, string password)
        {
            bool boolReturn;

            ApplicationUser u = GetApplicationUser(username);

            if (u == null)
                boolReturn = false;
            else
            {
                boolReturn = u.Enabled;
            }
                        
            if (boolReturn) UpdateUserActivity(u.Email, u.Id, true);

            return boolReturn;
        }

        #endregion

        #region Cache Methods
        
        protected void AddUserToCache(ApplicationUser applicationUser)
        {
            //cache result by email address and id
            //it could be requested by either
            HttpContext.Current.Cache.Add("UserByEmail_" + applicationUser.Email, applicationUser, null, System.Web.Caching.Cache.NoAbsoluteExpiration, this.cacheTimespan, CacheItemPriority.Normal, null);
            HttpContext.Current.Cache.Add("UserById_" + applicationUser.Id, applicationUser, null, System.Web.Caching.Cache.NoAbsoluteExpiration, this.cacheTimespan, CacheItemPriority.Normal, null);
        }

        protected void RemoveUserFromCache(string Email, string Id)
        {
            if (Email!=string.Empty) HttpContext.Current.Cache.Remove("UserByEmail_" + Email);
            if (Id != string.Empty) HttpContext.Current.Cache.Remove("UserById_" + Id);
        }

        #endregion
    }
}  
