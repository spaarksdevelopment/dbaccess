﻿using System;
using System.Configuration.Provider;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;

using Aduvo.Common.UtilityManager;
using Aduvo.Common.MembershipManager;


namespace Aduvo.DB.DBMembershipManager
{
    [Serializable]
    public class DBAppUser : ApplicationUser
    {
        //custom properties for a DB user
        public LDAPData GDInfo { get; set; }
        public string CostCentre { get; set; }
        public string CostCentreDescription { get; set; }
        public string UBR { get; set; }
        public bool BaseDataChanged { get; set; }

        //construtor
        public DBAppUser(string providername,
                                  string username,
                                  object providerUserKey,
                                  string email,
                                  string passwordQuestion,
                                  string comment,
                                  bool isApproved,
                                  bool isLockedOut,
                                  DateTime creationDate,
                                  DateTime lastLoginDate,
                                  DateTime lastActivityDate,
                                  DateTime lastPasswordChangedDate,
                                  DateTime lastLockedOutDate,
                                  LDAPData gdinfo)
            :
                                  base(providername,
                                       username,
                                       providerUserKey,
                                       email,
                                       passwordQuestion,
                                       comment,
                                       isApproved,
                                       isLockedOut,
                                       creationDate,
                                       lastLoginDate,
                                       lastActivityDate,
                                       lastPasswordChangedDate,
                                       lastLockedOutDate)
        {
            this.GDInfo = gdinfo;
            this.BaseDataChanged = false;
        }

        public DBAppUser(ApplicationUser applicationUser)
            : base("DBSqlMembershipProvider", applicationUser)
        {
            this.BaseDataChanged = false;
        }

        public DBAppUser(ApplicationUser applicationUser, LDAPData gdinfo)
            : base("DBSqlMembershipProvider", applicationUser)
        {
            this.GDInfo = gdinfo;
            this.BaseDataChanged = false;
        }

        public DBAppUser(LDAPData gdinfo)
            : base("DBSqlMembershipProvider", gdinfo.email, Convert.ToInt32(gdinfo.dbdirid), gdinfo.email, string.Empty, string.Empty, false, false, DateTime.Now, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue)
        {
            this.GDInfo = gdinfo;
            this.BaseDataChanged = false;
            RefreshGDInfo();
        }

        public DBAppUser(DBAppUser DBAppUser)
            : base("DBSqlMembershipProvider", DBAppUser)
        {
            this.BaseDataChanged = false;
            this.GDInfo = DBAppUser.GDInfo;
            this.CostCentre = DBAppUser.CostCentre;
            this.CostCentreDescription = DBAppUser.CostCentreDescription;
            this.UBR = DBAppUser.UBR;
        }

        public DBAppUser(string ProviderName, DBAppUser DBAppUser)
            : base(ProviderName, DBAppUser)
        {
            this.BaseDataChanged = false;
            this.GDInfo = DBAppUser.GDInfo;
            this.CostCentre = DBAppUser.CostCentre;
            this.CostCentreDescription = DBAppUser.CostCentreDescription;
            this.UBR = DBAppUser.UBR;
        }

        //refresh fields from the GDInfo object
        public void RefreshGDInfo()
        {
            if (GDInfo != null)
            {
                //Check if any base data has changed and needs to be updated in the database
                if ((Email != GDInfo.email) || (Forename != GDInfo.forename) || (Surname != GDInfo.surname) || (Telephone != GDInfo.telephone) || (UBR != GDInfo.ubrusercode))
                    BaseDataChanged = true;

                Email = GDInfo.email;
                Fullname = GDInfo.name;
                Forename = GDInfo.forename;
                Surname = GDInfo.surname;
                Telephone = GDInfo.telephone;
                CostCentre = GDInfo.costcenter;
                CostCentreDescription = GDInfo.costcenterdesc;
                UBR = GDInfo.ubrusercode;
            }
        }
               
    }
        
}
