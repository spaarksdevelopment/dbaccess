﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using System.Web.Configuration;
using System.Xml;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

using Aduvo.Common.UtilityManager;
using Aduvo.Common.MembershipManager;

namespace Aduvo.DB.DBMembershipManager
{
    public class DBSqlMembershipProvider : ApplicationMembershipProvider
    {
        protected bool UseGD { get; set; }
        protected bool TestMode { get; set; }

        #region Constructor

        public DBSqlMembershipProvider()
        {
            this.UseGD = Helper.SafeBool(ConfigurationManager.AppSettings["UseGD"]);
            this.TestMode = Helper.SafeBool(ConfigurationManager.AppSettings["TestMode"]);
        }

        #endregion

        #region Initialize

        public override void Initialize(string name, NameValueCollection config)
        {
            if (name == null || name.Length == 0)
            {
                name = "DBSqlMembershipProvider";
            }            

            base.Initialize(name, config);
        }

        #endregion

        #region GetUserId

        public override int? GetUserID(string username)
        {
            return GetDBDirId(username);
        }

        #endregion

        #region CreateUser

        public DBAppUser CreateUser(int DBDirID, bool isApproved, out MembershipCreateStatus status)
        {
            return CreateUser(DBDirID, isApproved, out status, false);
        }

        public DBAppUser CreateUser(int DBDirID, bool isApproved, out MembershipCreateStatus status, bool createdByExternalSystem)
        {
            string email = GetEmail(DBDirID.ToString());
            if (email != string.Empty)
                return (DBAppUser)CreateUser(email, string.Empty, email, string.Empty, string.Empty, isApproved, DBDirID, out status, createdByExternalSystem);
            else
            {
                status = MembershipCreateStatus.InvalidProviderUserKey;
                return null;
            }
        }

        public DBAppUser CreateUser(string email, bool isApproved, out MembershipCreateStatus status)
        {
            return (DBAppUser)CreateUser(email, string.Empty, email, string.Empty, string.Empty, isApproved, null, out status);
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            return CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status, false);
        }

        /// <summary>
        /// Add a new user (default provider method with some unnecessary arguments)
        /// </summary>
        /// <param name="username">users email address</param>
        /// <param name="password">N/A</param>
        /// <param name="email">users email address</param>
        /// <param name="passwordQuestion">N/A</param>
        /// <param name="passwordAnswer">N/A</param>
        /// <param name="isApproved">Enabled</param>
        /// <param name="providerUserKey">N/A</param>
        /// <param name="status">Output parameter for status</param>
        /// <returns></returns>
        /// 
        public MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status, bool createdByExternalSystem)
        {           
            //set up a blank user to return
            DBAppUser dbu = new DBAppUser("DBSqlMembershipProvider", string.Empty, 0, string.Empty, string.Empty, string.Empty, false, false, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, null);
            
            //check the email address is valid
            if (!ValidateEmailAddress(email))
            {
                status = MembershipCreateStatus.InvalidEmail;
                return dbu;
            }
            
            //Get DBDirID
            int? DBDirId = GetUserID(email);
            if (!DBDirId.HasValue)
            {
                status = MembershipCreateStatus.InvalidProviderUserKey;
                return dbu;
            }
            else
            {
                if (providerUserKey != null)
                {
                    if (Helper.SafeInt(providerUserKey) != DBDirId.Value)
                    {
                        status = MembershipCreateStatus.InvalidProviderUserKey;
                        return dbu;
                    }
                }
                else
                {
                    //set the user key
                    providerUserKey = DBDirId.Value;
                }
            }

            //check if user is already registered
            dbu = GetDBAppUser(username);
            if (dbu != null)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return dbu;
            }

            //get the user's data from the directory
            LDAPData ldapData = GetLDAPData(DBDirId.Value);

            if (ldapData != null)
            {
                dbu = new DBAppUser(ldapData);
                //explicitly set Enabled property here
                dbu.Enabled = isApproved;
            }
            else
                dbu = new DBAppUser("DBSqlMembershipProvider", username, providerUserKey, email, string.Empty, string.Empty, isApproved, false, DateTime.Now, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, null);
            
            //get the current users id            
            int CreatedById = 0;

            ApplicationUser CreatedByUser = null;

            if(!createdByExternalSystem)
                CreatedByUser = (new DBSqlMembershipProvider()).GetUser();
            
            if(CreatedByUser==null) 
                CreatedById = DBDirId.Value;
            else
                CreatedById = CreatedByUser.Id;

            if (InsertUser(dbu.Email, dbu.Enabled, dbu.Id, CreatedById, dbu.Forename, dbu.Surname, dbu.Telephone, dbu.UBR, dbu.CostCentre))
                status = MembershipCreateStatus.Success;
            else
                status = MembershipCreateStatus.ProviderError;
            
            //return user that was created
            return dbu;

        }

        protected virtual bool InsertUser(string email, bool isApproved, object providerUserKey, int CreatedByUserId, string Forename, string Surname, string Telephone, string UBR, string CostCentre)
        {
            bool success;
            /*
             mp_AddMembershipUser 
                @UserId int
                ,@EmailAddress varchar(255)
                ,@Forename varchar(255)
                ,@Surname varchar(255)
                ,@Telephone varchar(255)
                ,@UBR varchar(50) 
                ,@CreatedByUserId int
                ,@Enabled bit = 1
             */

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_AddMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("UserID", providerUserKey));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", email));
                cmd.Parameters.Add(new SqlParameter("Forename", Forename));
                cmd.Parameters.Add(new SqlParameter("Surname", Surname));
                cmd.Parameters.Add(new SqlParameter("Telephone", Telephone));
                cmd.Parameters.Add(new SqlParameter("CostCentre", CostCentre));
                cmd.Parameters.Add(new SqlParameter("UBR", UBR));
                cmd.Parameters.Add(new SqlParameter("CreatedByUserId", CreatedByUserId));
                cmd.Parameters.Add(new SqlParameter("Enabled", isApproved));
                

                try
                {
                    cmd.ExecuteNonQuery();
                    
                    success = true;
                    
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                    success = false;
                }

                conn.Close();
            }
            return success;
        }

        #endregion
               
        #region GetDBAppUser

        /// <summary>
        /// Get the GetDBAppUser from database and update details from directory. Does not use cached instance.
        /// </summary>
        /// <param name="email">the users email</param>
        /// <returns></returns>
        public DBAppUser GetDBAppUser(string email)
        {
            ApplicationUser au = base.GetApplicationUser(email);

            if (au == null) return null;
            
            DBAppUser dbu = new DBAppUser(au);
            
            //get ldapdata via id rather than email
            //the email can change if someone changes their name
            LDAPData LD = GetLDAPData(au.Id);
            dbu.GDInfo = LD;
            dbu.RefreshGDInfo();

            //check if GD data has changed, if so update database
            if (dbu.BaseDataChanged)
            {
                try
                {
                    dbu.UpdatedByUserId = dbu.Id;
                    SaveApplicationUser(dbu);
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                }                
            }

            return dbu;
        }

        /// <summary>
        ///  Get the GetDBAppUser from database and update details from directory. Does not use cached instance.
        /// </summary>
        /// <param name="dbdirid">the users dbdirid</param>
        /// <returns></returns>
        public DBAppUser GetDBAppUser(int dbPeopleID)
        {
            ApplicationUser au = base.GetApplicationUser(dbPeopleID);

            if (au == null) return null;

            DBAppUser dbu = new DBAppUser(au);
            LDAPData LD = GetLDAPData(au.Id);
            dbu.GDInfo = LD;
            dbu.RefreshGDInfo();

            //check if GD data has changed, if so update database
            //this can happen if someone gets married and changes their name for example
            if (dbu.BaseDataChanged)
            {
                try
                {
                    dbu.UpdatedByUserId = dbu.Id;
                    base.SaveApplicationUser(dbu);
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                }   
            }

            return dbu;
        }

        #endregion

        #region SaveDBAppUser

        /// <summary>
        /// Save a DB application User - overrides the Save Application User
        /// </summary>
        /// <param name="applicationUser">the ApplicationUser object</param>
        /// <returns>true is success</returns>
        protected virtual bool SaveApplicationUser(DBAppUser applicationUser)
        {
            /*
             [dbo].[mp_UpdateMembershipUser] 
                @UserID int,
                @EmailAddress varchar(255),
                @Forename varchar(255),
                @Surname varchar(255),
                @Telephone varchar(255),
                @UBR varchar(50),
                @Enabled bit
             */

            int rows = 0;

            SqlConnection conn;
            using (conn = DataHelper.getConn(MembershipConnectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_UpdateMembershipUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", applicationUser.dbPeopleID));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", applicationUser.Email));
                cmd.Parameters.Add(new SqlParameter("Forename", applicationUser.Forename));
                cmd.Parameters.Add(new SqlParameter("Surname", applicationUser.Surname));
                cmd.Parameters.Add(new SqlParameter("Telephone", applicationUser.Telephone));
                cmd.Parameters.Add(new SqlParameter("UBR", applicationUser.UBR));
                cmd.Parameters.Add(new SqlParameter("CostCentre", applicationUser.CostCentre));
                //cmd.Parameters.Add(new SqlParameter("UpdatedByUserId", applicationUser.UpdatedByUserId));
                cmd.Parameters.Add(new SqlParameter("Enabled", applicationUser.Enabled));

                rows = cmd.ExecuteNonQuery();
                conn.Close();
            }

            return (rows > 0);
        }

        #endregion

        #region GetUser

        /// <summary>
        /// Get the current logged in user
        /// </summary>
        /// <returns>an ApplicationUser object which can be cast as DBAppUser</returns>
        public override ApplicationUser GetUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return (ApplicationUser)GetUser(HttpContext.Current.User.Identity.Name, true);
            }
            else
                return null;
        }

        /// <summary>
        /// Geta a user object by their email address - checks cache first
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="userIsOnline">update user activity flag</param>
        /// <returns>Membership user object with full directory data which can be cast as DBAppUser</returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            DBAppUser applicationUser = null;

            if (HttpContext.Current.Cache["UserByEmail_" + username] != null)
            {
                applicationUser = (DBAppUser)HttpContext.Current.Cache["UserByEmail_" + username];
            }
            else
            {
                applicationUser = GetDBAppUser(username);

                if (applicationUser != null)
                {
                    base.AddUserToCache(applicationUser);
                }
                else
                {
                    applicationUser = null;
                    //throw new ProviderException("User not found");
                }
            }

            base.UpdateActivity(username, 0, userIsOnline);

            return applicationUser;
        }


        /// <summary>
        /// Geta a user object by their Id - checks cache first
        /// </summary>
        /// <param name="providerUserKey">the user Id</param>
        /// <param name="userIsOnline">update user activity flag</param>
        /// <returns>Membership user object with full directory data which can be cast as DBAppUser</returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            int userId = Convert.ToInt32(providerUserKey);

            DBAppUser applicationUser;

            //check cache for this user

            if (HttpContext.Current.Cache["UserById_" + userId.ToString()] != null)
            {
                applicationUser = (DBAppUser)HttpContext.Current.Cache["UserById_" + userId.ToString()];
                return applicationUser;
            }

            applicationUser = GetDBAppUser(userId);

            if (applicationUser != null)
            {
                base.AddUserToCache(applicationUser);
            }

            UpdateActivity(string.Empty, userId, userIsOnline);

            return applicationUser;
        }

                

        #endregion

        #region GetUserFromDirectory

        /// <summary>
        /// Get a User object direct from Group Directory - not a registered user if this application
        /// </summary>
        /// <param name="email">the email address</param>
        /// <returns></returns>
        public DBAppUser GetUserFromDirectory(string email)
        {
            LDAPData LD = GetLDAPData(email);
            if (LD == null) return null;
            if (!LD.Found) return null;
            
            DBAppUser dbu = new DBAppUser(LD);
            
            return dbu;
        }

        /// <summary>
        /// Get a User object direct from Group Directory - not a registered user if this application
        /// </summary>
        /// <param name="dbdirid">the DBDirId to get</param>
        /// <returns></returns>
        public DBAppUser GetUserFromDirectory(int dbdirid)
        {           
            LDAPData LD = GetLDAPData(dbdirid);
            if (LD == null) return null;
            if (!LD.Found) return null;

            DBAppUser dbu = new DBAppUser(LD);

            return dbu;
        }

        #endregion

        #region FindUsersByNameFromDirectory

        public List<DBAppUser> FindUsersByNameFromDirectory(string usernameToMatch)
        {
            int totalRecords;
            return FindUsersByNameFromDirectory(usernameToMatch, 0, 0, out totalRecords);
        }

        /// <summary>
        /// Find users by name (from Group Directory)
        /// </summary>
        /// <param name="usernameToMatch">Name to search for</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">output parameter for total records</param>
        /// <returns>a collection of user objects</returns>
        public List<DBAppUser> FindUsersByNameFromDirectory(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            usernameToMatch = usernameToMatch.Trim();
            List<DBAppUser> listDBAU = new List<DBAppUser>();

            //first check if it was an email which was supplied

            if (usernameToMatch.Contains("@"))
            {
                //now check if its a valid db email
                if (ValidateEmailAddress(usernameToMatch))
                {
                    //get the user who has this email address
                    DBAppUser DBAU = GetUserFromDirectory(usernameToMatch);
                    if (DBAU != null)
                        listDBAU.Add(DBAU);
                }
            }
            else
            {
                if (usernameToMatch.Contains(".")) usernameToMatch = usernameToMatch.Replace('.', ' ');
                //search group directory for matching names

                List<LDAPData> listLD = GetDataForName(usernameToMatch);

                foreach (LDAPData LD in listLD)
                {
                    DBAppUser dbu = new DBAppUser(LD);
                    listDBAU.Add(dbu);
                }
            }

            totalRecords = listDBAU.Count;
            return listDBAU;
        }

        #endregion


        #region ValidateUser

        /// <summary>
        /// Validate password, and check if they are a registered user
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>true if password correct and user is registered</returns>
        public override bool ValidateUser(string username, string password)
        {
            // check strName is in Group Directory, and password correct

            bool boolReturn = false;
                        
            //change to lower case for consistency
            username = username.ToLower();

            if (this.TestMode || !this.UseGD)
            {
                //don't check password in test mode
                boolReturn = true;
            }
            else
            {
                try
                {
                    GDLookup gdl = new GDLookup();
                    boolReturn = gdl.CheckPassword(username, password);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Substring(0, 30) == "The server is not operational.")
                    {
                        //log error
                        throw new Exception("LDAP Server is not operational");
                    }
                    //otherwise, its an invalid password                                
                }
            }

            if (boolReturn)
            {
                boolReturn = base.ValidateUser(username, password);
            }
            
            return boolReturn;
        }

        /// <summary>
        /// Validate password, and check if they are a registered user
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>true if password correct and user is registered</returns>
        public virtual bool ValidateUser(string username, string password, bool ValidatePasswordOnly)
        {
            // check strName is in Group Directory, and password correct

            bool boolReturn = false;

            //change to lower case for consistency
            username = username.ToLower();

            if (this.TestMode || !this.UseGD)
            {
                //don't check password in test mode
                boolReturn = true;
            }
            else
            {
                try
                {
                    GDLookup gdl = new GDLookup();
                    boolReturn = gdl.CheckPassword(username, password);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Substring(0, 30) == "The server is not operational.")
                    {
                        //log error
                        throw new Exception("LDAP Server is not operational");
                    }
                    //otherwise, its an invalid password                                
                }
            }

            if (boolReturn && !ValidatePasswordOnly)            
            {
                boolReturn = base.ValidateUser(username, password);
            }

            return boolReturn;
        }

        #endregion

        #region checkemailavailable

        public override bool CheckEmailAvailable(string Email)
        {
            if (!ValidateEmailAddress(Email))
            {
                return false;
            }

            LDAPData LD = GetLDAPData(Email);
            return (LD == null);                           
        }

        public bool CheckEmailExists(string Email)
        {
            if (!ValidateEmailAddress(Email))
            {
                return false;
            }

            LDAPData LD = GetLDAPData(Email);
            return (LD != null);
        }

        #endregion

        #region validate email address

        /// <summary>
        /// Validates the string passed in as a valid DB email address and returns a boolean;
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        public static bool ValidateEmailAddress(string EmailAddress)
        {
            if (string.IsNullOrEmpty(EmailAddress)) return false;

            string strRegex = @"^[\w-]+[\.\w-]*@db.com$";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(EmailAddress)) { return true; } else { return false; }
        }

        /// <summary>
        /// Splits and validates the string passed in as valid DB email addresses and returns a boolean;
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        /// <param name="Delimiter">The value to split the string by</param>
        public static bool ValidateMultipleEmailAddress(string EmailAddress, Char[] delimiter)
        {
            string strRegex = @"^[\w-]+[\.\w-]*@db.com$";
            string[] strArr = EmailAddress.Split(delimiter);
            foreach (string s in strArr)
            {
                if (s.Trim() != "")
                {
                    Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
                    if (!re.IsMatch(s.Trim())) { return false; }
                }
            }
            return true;
        }

        #endregion

        #region LDAP methods

        public LDAPData GetLDAPData(int DBDirId)
        {
            LDAPData ldapData;

            if (this.UseGD)
            {
                GDLookup gdLookup = new GDLookup();
                ldapData = gdLookup.GetDataForIDSafe(DBDirId.ToString());

                //save in XML file
                GDLookupXML gdLookupxml = new GDLookupXML();
                gdLookupxml.SaveData(ldapData);
            }
            else
            {
                GDLookupXML gdLookupxml = new GDLookupXML();
                ldapData = gdLookupxml.GetDataForID(DBDirId.ToString());
            }

            return ldapData;
        }

      

        public LDAPData GetLDAPData(string Email)
        {
            LDAPData ldapData;

            if (this.UseGD)
            {
                GDLookup gdLookup = new GDLookup();
                ldapData = gdLookup.GetDataForEmailSafe(Email);

                //save in XML file
                GDLookupXML gdLookupxml = new GDLookupXML();
                gdLookupxml.SaveData(ldapData);
            }
            else
            {
                GDLookupXML gdLookupxml = new GDLookupXML();
                ldapData = gdLookupxml.GetDataForEmail(Email);
            }

            return ldapData;        
        }

        public int? GetDBDirId(string Email)
        {
            string dbdirid = string.Empty;

            if (this.UseGD)
            {
                GDLookup gdl = new GDLookup();
                dbdirid = gdl.GetDBDirIDSafe(Email);               
            }
            else
            {
                GDLookupXML gdLookup = new GDLookupXML();
                dbdirid = gdLookup.GetDBDirID(Email);
            }

            if (dbdirid == string.Empty)
                return null;
            else
                return Convert.ToInt32(dbdirid); 
        }

        public string GetEmail(string DBDirId)
        {
            string dbdirid = string.Empty;

            if (this.UseGD)
            {
                GDLookup gdl = new GDLookup();
                LDAPData ld = gdl.GetDataForID(DBDirId);
                if (ld != null)
                    return ld.email;
                else
                    return string.Empty;
            }
            else
            {
                GDLookupXML gdLookup = new GDLookupXML();
                LDAPData ld = gdLookup.GetDataForID(DBDirId);
                if (ld != null)
                    return ld.email;
                else
                    return string.Empty;
            }                        
        }

        public List<LDAPData> GetDataForName(string nameToMatch)
        {
            List<LDAPData> listLD;

            if (this.UseGD)
            {
                GDLookup gdLookup = new GDLookup();
                listLD = gdLookup.GetDataForName(nameToMatch);
            }
            else
            {
                GDLookupXML gdLookup = new GDLookupXML();
                listLD = gdLookup.GetDataForName(nameToMatch);
            }

            return listLD;
        }

        #endregion
    }
}
