﻿using System;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

using Aduvo.Common.UtilityManager;
using Aduvo.Common.WebUtilityManager;

namespace Aduvo.DB.DBMembershipManager
{
    class GDLookupXML
    {
        private string XmlDataVirtualFilePath = @"~\APP_DATA\users.xml";
        private string XmlDataAbsoluteFilePath = string.Empty;

        public GDLookupXML()
        {
            //configure 
            string ConfigFilePath = Helper.SafeString(ConfigurationManager.AppSettings["GDDataAbsoluteFilePath"]);
            if ((ConfigFilePath == string.Empty))
            {

                ConfigFilePath = Helper.SafeString(ConfigurationManager.AppSettings["GDDataVirtualFilePath"]);
                if (ConfigFilePath != string.Empty)
                {
                    XmlDataVirtualFilePath = ConfigFilePath;
                }

                if (System.Web.HttpContext.Current==null)
                    XmlDataAbsoluteFilePath = System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, XmlDataVirtualFilePath.Replace("~\\","")); 
                else
                    XmlDataAbsoluteFilePath = PageHelper.GetAbsolutePath(XmlDataVirtualFilePath);
            }
            else
            {
                XmlDataAbsoluteFilePath = ConfigFilePath;
            }
        }

        #region GetDBDirID

        public string GetDBDirID(string email)
        {
            string RequiredField = "dbdirid";
            return GetDataForEmail(email, RequiredField);
        }

        #endregion

        #region GetData

        public string GetDataForEmail(string email, string RequiredField)
        {
            XmlNodeList nodeList = FindByEmail(email);
            if (nodeList.Count == 0) return string.Empty;
            XmlAttribute RequiredFieldValue = nodeList[0].Attributes[RequiredField];
            if(RequiredFieldValue==null) return string.Empty;
            return RequiredFieldValue.Value;
        }


        public LDAPData GetDataForEmail(string email)
        {
            XmlNodeList nodeList = FindByEmail(email);
            if (nodeList.Count == 0) return null;
            return CreateLDAPDataFromNode(nodeList[0]);
        }

        public LDAPData GetDataForID(string dbdirid)
        {
            XmlNodeList nodeList = FindById(dbdirid);
            if (nodeList.Count == 0) return null;
            return CreateLDAPDataFromNode(nodeList[0]);
        }


        #endregion

        #region GetDataForName

        public List<LDAPData> GetDataForName(string searchname)
        {
            List<LDAPData> ListLDAPData = new List<LDAPData>();

            XmlNodeList nodeList = FindByName(searchname);

            foreach (XmlNode node in nodeList)
            {
                ListLDAPData.Add(CreateLDAPDataFromNode(node));
            }

            return ListLDAPData;
        }

        #endregion

        #region SaveData

        /// <summary>
        /// Save Direcory Data to the XML file
        /// </summary>
        /// <param name="ldapData"></param>
        public void SaveData(LDAPData ldapData)
        {
            XmlDocument document = XmlHelper.LoadDocument(XmlDataAbsoluteFilePath, true);
            XmlNode node = document.SelectSingleNode(string.Format("//user[@dbdirid='{0}']", ldapData.dbdirid));

            bool Changed = false;
            
            if (node == null)
            {
                //pick the first node and clone it
                node = document.SelectSingleNode("//user");
                node = node.Clone();

                SetAttribute(node, ldapData.dbdirid, "dbdirid", ref Changed);

                node = document.DocumentElement.AppendChild(node);

                Changed = true;
            }
           
            //update node
            SetAttribute(node, ldapData.email, "email", ref Changed);                                
            SetAttribute(node, ldapData.name,"name", ref Changed);
            SetAttribute(node, ldapData.forename,"forename", ref Changed);
            SetAttribute(node, ldapData.surname,"surname", ref Changed);
            SetAttribute(node, ldapData.telephone,"telephone", ref Changed);
            SetAttribute(node, ldapData.costcenter,"costcenter", ref Changed);
            SetAttribute(node, ldapData.costcenterdesc,"costcenterdesc", ref Changed);
            SetAttribute(node, ldapData.ubrusercode, "ubrusercode", ref Changed);     
            

            //if anything has changed, need to save the file
            if (Changed)
            {
                try
                {
                    document.Save(XmlDataAbsoluteFilePath);
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                }
            }
        }


        private void SetAttribute(XmlNode node, string value, string Attr, ref bool Changed)
        {
            if (node.Attributes[Attr].Value != value)
            {
                node.Attributes[Attr].Value = value;
                Changed = true;
            }
        }

        #endregion

        #region protected methods

        #region CreateApplicationUserFromNode

        /// <summary>
        /// Create LDAPData from specified XmlNode.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected LDAPData CreateLDAPDataFromNode(XmlNode node)
        {
            LDAPData ldapData = null;

            if (node != null)
            {
                ldapData = new LDAPData();
                ldapData.dbdirid = node.Attributes["dbdirid"].Value;
                ldapData.email = node.Attributes["email"].Value;
                ldapData.name = node.Attributes["name"].Value;
                ldapData.forename = node.Attributes["forename"].Value;
                ldapData.surname = node.Attributes["surname"].Value;
                ldapData.telephone = node.Attributes["telephone"].Value;
                ldapData.costcenter = node.Attributes["costcenter"].Value;
                ldapData.costcenterdesc = node.Attributes["costcenterdesc"].Value;
                ldapData.ubrusercode = node.Attributes["ubrusercode"].Value;

                ldapData.Found = true;
            }

            return ldapData;
        }

       

        #endregion

        #region FindByName

        /// <summary>
        /// Find all nodes with specified email address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        protected XmlNodeList FindByName(string name)
        {
            name = Helper.ToTitleCase(name);
            string xPath;
            //split name into components
            int iPos = name.LastIndexOf(" ");
            if(iPos<0)
            {
                xPath = string.Format("//user[contains(@name, '{0}')]", name);
            }
            else
            {
                string surname = name.Substring(iPos).Trim();
                string forename = name.Substring(0, iPos).Trim();
                xPath = string.Format("//user[(contains(@name, '{0}')) or (contains(@forename, '{1}') and contains(@surname, '{2}')) ]", name, forename, surname);
            }

            return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, xPath);
        }

        #endregion

        #region FindByEmail

        /// <summary>
        /// Find all nodes with specified email address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        protected XmlNodeList FindByEmail(string email)
        {
            return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, string.Format("//user[@email='{0}']", email));
        }

        #endregion

        #region FindById

        /// <summary>
        /// Find all nodes with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected XmlNodeList FindById(string id)
        {
          return XmlHelper.SelectNodes(XmlDataAbsoluteFilePath, string.Format("//user[@dbdirid='{0}']", id));
        }

        #endregion

        #endregion
    }
}
