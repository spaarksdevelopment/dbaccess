﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Text;

namespace Aduvo.DB.DBMembershipManager
{
    /// <summary>
    /// All known fields in group directory
    /// </summary>
    public enum DBDirectoryData
    {
        cn,
        dbacademictitle,
        dbcostcenter,
        dbcostcenterdesc,
        dbdesk,
        dbdirid,
        dblegalentity,
        dblocationcity,
        dblocationcountry,
        dblocationpostalcode,
        dblocationstreet,
        dborglevel1,
        dborglevel2,
        dborglevel3,
        dborglevel4,
        dborglevel5,
        dborglevel6,
        dbsalutation,
        dbsecretary,
        function,
        givenName,
        initials,
        mail,
        mobile,
        roomnumber,
        sn,
        telephoneNumber,
        ubrusercode,
        ubruserfullpath,
        dbentrytype,
        dbtraderintercom,
        dbntloginid,
        

    }

    [Serializable]
    public class LDAPData
    {
        #region properties


        
        private string _dbtraderintercom;

        public string dbtraderintercom
        {
            get { return _dbtraderintercom; }
            set { _dbtraderintercom = value; }
        }

        private string _dblocationstreet;

        public string dblocationstreet
        {
            get { return _dblocationstreet; }
            set { _dblocationstreet = value; }
        }
        private string __dblocationcountry;

        public string dblocationcountry
        {
            get { return __dblocationcountry; }
            set { __dblocationcountry = value; }
        }

        private string _dbntloginid;

        public string dbntloginid
        {
            get { return _dbntloginid; }
            set { _dbntloginid = value; }
        }
        private string _dbdirid;
        public string dbdirid
        {
            get { return _dbdirid; }
            set { _dbdirid = value; }
        }

        private string _email; //mail
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _name; //cn
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        private string _forename; //givenname
        public string forename
        {
            get { return _forename; }
            set { _forename = value; }
        }
        
        private string _surname; //sn
        public string surname
        {
            get { return _surname; }
            set { _surname = value; }
        }
        
        private string _telephone; //telephoneNumber
        public string telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }
        
        private string _costcenter; //cost centre
        public string costcenter
        {
            get { return _costcenter; }
            set { _costcenter = value; }
        }

        private string _costcenterdesc; //cost centre description
        public string costcenterdesc
        {
            get { return _costcenterdesc; }
            set { _costcenterdesc = value; }
        }

        private string _ubrusercode; //UBR code
        public string ubrusercode
        {
            get { return _ubrusercode; }
            set { _ubrusercode = value; }
        }

        private Dictionary<DBDirectoryData, string> _DirectoryFields; //to contain all fields
        public Dictionary<DBDirectoryData, string> DirectoryFields
        {
            get { return _DirectoryFields; }
            set { _DirectoryFields = value; }
        }

        private bool _found = false;
        public bool Found
        {
            get { return _found; }
            set { _found = value; }
        }

        #endregion

        #region constructors

        public LDAPData()
        {
            _DirectoryFields = new Dictionary<DBDirectoryData, string>();
        }

        public LDAPData(SearchResult theResult)
            : this()
        {
            if (theResult != null)
            {
                _found = true;

                //fill hashtable with results
                foreach (DBDirectoryData dbDD in Enum.GetValues(typeof(DBDirectoryData)))
                {
                    string sVal = GetValueFromResult(theResult, dbDD);
                    _DirectoryFields.Add(dbDD, sVal);
                }

                //fill the properties
                _dbdirid = _DirectoryFields[DBDirectoryData.dbdirid];
                _email = _DirectoryFields[DBDirectoryData.mail];
                _name = _DirectoryFields[DBDirectoryData.cn];
                _forename = _DirectoryFields[DBDirectoryData.givenName];
                _surname = _DirectoryFields[DBDirectoryData.sn];
                _telephone = _DirectoryFields[DBDirectoryData.telephoneNumber];
                _costcenter = _DirectoryFields[DBDirectoryData.dbcostcenter];
                _costcenterdesc = _DirectoryFields[DBDirectoryData.dbcostcenterdesc];
                _ubrusercode = _DirectoryFields[DBDirectoryData.ubrusercode];
                _dbtraderintercom = _DirectoryFields[DBDirectoryData.dbtraderintercom];
                _dblocationstreet = _DirectoryFields[DBDirectoryData.dblocationstreet];
                _dbntloginid = _DirectoryFields[DBDirectoryData.dbntloginid];
                __dblocationcountry = _DirectoryFields[DBDirectoryData.dblocationcountry];
            }
        }

        #endregion

        #region methods

        private string GetValueFromResult(SearchResult theResult, DBDirectoryData dbdd)
        {
            string sItem = string.Empty;

            //test if data exists
            if (theResult.Properties[dbdd.ToString()] != null)
            {
                if (theResult.Properties[dbdd.ToString()].Count > 0)
                {
                    //get it
                    sItem = (string)theResult.Properties[dbdd.ToString()][0];
                }
            }

            return sItem;
        }

        //outputs all data
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (DBDirectoryData dbDD in Enum.GetValues(typeof(DBDirectoryData)))
            {
                sb.AppendFormat("{0}={1};", _DirectoryFields[dbDD]);
            }

            return sb.ToString();
        }

        #endregion
    }
}