using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.OleDb;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using Aduvo.Common.UtilityManager;

namespace Aduvo.DB.DBMembershipManager
{
    class GDLookup
    {
        protected string ADPath = string.Empty;
        protected DirectoryEntry oRoot;
        protected DirectorySearcher oSearcher;
        protected SearchResultCollection oResults;
        protected SearchResult oResult;
        protected DirectoryEntry oItem;
        protected string oItemPath;
        protected string LDAPUID;
        protected string LDAPPWD;
        protected StringCollection LDAPAttributes = new StringCollection();


        /// <summary>
        /// create a GDLookup object bound to default settings in app.config
        /// </summary>
        public GDLookup()
        {
            string LDAPhostname = ConfigurationManager.AppSettings["LDAPhostname"];
            string LDAPportno = ConfigurationManager.AppSettings["LDAPportno"];
            LDAPUID = ConfigurationManager.AppSettings["LDAPUID"];
            LDAPPWD = ConfigurationManager.AppSettings["LDAPPWD"];

            ADPath = "LDAP://" + LDAPhostname + ":" + LDAPportno + "/ou=people,ou=global,dc=dbgroup,dc=com";

        }

        /// <summary>
        /// create a GDLookup object bound to specified settings
        /// </summary>
        /// <param name="LDAPhostname"></param>
        /// <param name="LDAPportno"></param>
        public GDLookup(string LDAPhostname, string LDAPportno)
        {
            ADPath = "LDAP://" + LDAPhostname + ":" + LDAPportno + "/ou=people,ou=global,dc=dbgroup,dc=com";
        }

        protected void ConnectToLDAP(string sFilter)
        {
            oRoot = new DirectoryEntry(ADPath, LDAPUID, LDAPPWD);
            oSearcher = new DirectorySearcher(oRoot);
            oRoot.AuthenticationType = AuthenticationTypes.None;
            oSearcher.Filter = sFilter;

            foreach (DBDirectoryData dbDD in Enum.GetValues(typeof(DBDirectoryData)))
            {
                oSearcher.PropertiesToLoad.Add(dbDD.ToString());
            } 
        }

        protected bool GetResult()
        {
            try
            {
                oResult = oSearcher.FindOne();
                bool isFound = (oResult != null);
                if (isFound)
                {
                    oItem = oResult.GetDirectoryEntry();
                    oItemPath = oItem.Path;
                }
                return isFound;
            }
            catch (ExternalException e)
            {
                throw new Exception("External Excpetion occurred", e);
            }
        }

        protected bool GetResults()
        {
            try
            {
                oResults = oSearcher.FindAll();
                return (oResults.Count > 0);
            }
            catch (ExternalException e)
            {
                throw new Exception("External Excpetion occurred", e);
            }
        }

        /// <summary>
        /// Checks the GD password against the supplied email address
        /// </summary>
        /// <param name="email">the login email address</param>
        /// <param name="pwd">the password</param>
        /// <returns>True if correct password, false otherwise</returns>
        public bool CheckPassword(string email, string pwd)
        {
            if ((email == string.Empty) || (pwd == string.Empty))
                return false;

            string sFilter = "(mail=" + email + ")";
            ConnectToLDAP(sFilter);
            GetResult();
            try
            {
                string strdbdirid = (string)oResult.Properties["dbdirid"][0];
                string uid = "dbdirid=" + strdbdirid + ",ou=people,ou=global,dc=dbgroup,dc=com";

                OleDbConnectionStringBuilder connsb = new OleDbConnectionStringBuilder();
                connsb.Provider = "ADsDSOObject";
                connsb.Add("User ID", uid);
                connsb.Add("Password", pwd);

                OleDbConnection conn = new OleDbConnection(connsb.ConnectionString);
                conn.Open();

                string sQuery = "<" + ADPath + ">;(dbdirid=" + strdbdirid + ");cn,mail;onelevel";

                OleDbCommand cmd = new OleDbCommand(sQuery, conn);
                cmd.ExecuteNonQuery();

            }
            catch
            //catch (Exception ex)
            {
                //throw new Exception("Error authenticating user. " + ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets data from LDAP using supplied filter
        /// </summary>
        /// <param name="FilterText">Filter to apply</param>
        /// <param name="RequiredField">The field to retrieve</param>
        /// <returns></returns>
        public string GetData(string FilterText, string RequiredField)
        {
            ConnectToLDAP(FilterText);
            if (GetResult())
                return (string)oResult.Properties[RequiredField][0];
            else
                return string.Empty;          
        }

        /// <summary>
        /// Gets data from LDAP using supplied filter
        /// </summary>
        /// <param name="FilterText">Filter to apply</param>
        /// <returns>LDAPDATA object containing all data for supplied user</returns>
        public LDAPData GetData(string FilterText)
        {
            LDAPData ld;
            ConnectToLDAP(FilterText);
            if (GetResult())
                ld = new LDAPData(oResult);
            else
                ld = null;

            return ld;
        }

      

        /// <summary>
        /// Gets data from LDAP using supplied filter
        /// </summary>
        /// <param name="FilterText">Filter to apply</param>
        /// <returns>LDAPDATA object containing all data for supplied user</returns>
        public List<LDAPData> GetDataList(string FilterText)
        {
            List<LDAPData> LDAPDataList = new List<LDAPData>();

            ConnectToLDAP(FilterText);
            if (GetResults())
            {
                foreach (SearchResult oSearchResult in oResults)
                {
                    LDAPData ld = new LDAPData(oSearchResult);
                    LDAPDataList.Add(ld);
                }
            }
            return LDAPDataList;
        }

        /// <summary>
        /// Gets data from LDAP using supplied dbdirid
        /// </summary>
        /// <param name="email">the email address</param>
        /// <returns></returns>
        public LDAPData GetDataForID(string dbdirid)
        {
            string sFilter = "(dbdirid=" + dbdirid + ")";
            return GetData(sFilter);
        }

        /// <summary>
        /// Gets data from LDAP using supplied dbdirid without exceptions
        /// </summary>
        /// <param name="email">the email address</param>
        /// <returns></returns>
        public LDAPData GetDataForIDSafe(string dbdirid)
        {
            LDAPData ld;
            string sFilter = "(dbdirid=" + dbdirid + ")";
            try
            {
                ld = GetData(sFilter);
            }
            catch
            {
                ld = new LDAPData();

            }
            return ld;
        }

    

        /// <summary>
        /// Gets data from LDAP using supplied email
        /// </summary>
        /// <param name="email">the email address</param>
        /// <param name="RequiredField">The field to retrieve</param>
        /// <returns></returns>
        public string GetDataForID(string dbdirid, string RequiredField)
        {
            string sFilter = "(dbdirid=" + dbdirid + ")";
            return GetData(sFilter, RequiredField);
        }

        /// <summary>
        /// Gets data from LDAP using supplied email without exceptions
        /// </summary>
        /// <param name="email">the email address</param>
        /// <param name="RequiredField">The field to retrieve</param>
        /// <returns></returns>
        public string GetDataForIDSafe(string dbdirid, string RequiredField)
        {
            string sValue = string.Empty;
            string sFilter = "(dbdirid=" + dbdirid + ")";
            try
            {
                sValue = GetData(sFilter, RequiredField);
            }
            catch
            {
                sValue = string.Empty;

            }
            return sValue;

        }

        /// <summary>
        /// Gets data from LDAP using supplied email
        /// </summary>
        /// <param name="email">the email address</param>
        /// <returns></returns>
        public LDAPData GetDataForEmail(string email)
        {
            string sFilter = "(mail=" + email + ")";
            return GetData(sFilter);
        }

        /// <summary>
        /// Gets data from LDAP using supplied email without exceptions
        /// </summary>
        /// <param name="email">the email address</param>
        /// <returns></returns>
        public LDAPData GetDataForEmailSafe(string email)
        {
            LDAPData ld;
            string sFilter = "(mail=" + email + ")";
            try
            {
                ld = GetData(sFilter);
            }
            catch
            {
                ld = new LDAPData();

            }
            return ld;
        }
        /// <summary>
        /// Gets data from LDAP using supplied email
        /// </summary>
        /// <param name="email">the email address</param>
        /// <param name="RequiredField">The field to retrieve</param>
        /// <returns></returns>
        public string GetDataForEmail(string email, string RequiredField)
        {
            string sFilter = "(mail=" + email + ")";
            return GetData(sFilter, RequiredField);
        }

        /// <summary>
        /// Gets data from LDAP using supplied email without exceptions
        /// </summary>
        /// <param name="email">the email address</param>
        /// <param name="RequiredField">The field to retrieve</param>
        /// <returns></returns>
        public string GetDataForEmailSafe(string email, string RequiredField)
        {
            string sValue = string.Empty;
            string sFilter = "(mail=" + email + ")";
            try
            {
                sValue = GetData(sFilter, RequiredField);
            }
            catch
            {
                sValue = string.Empty;

            }
            return sValue;

        }

        /// <summary>
        /// Gets data from LDAP, searching by name
        /// </summary>
        /// <param name="searchname">the name</param>
        /// <returns></returns>
        public List<LDAPData> GetDataForName(string searchname)
        {
            string sFilter = string.Empty;

            //first check if an email address was entered
            if (searchname.Contains("@"))
            {
                List<LDAPData> listLD = new List<LDAPData>();

                LDAPData ld = GetDataForEmail(searchname);

                if (ld.Found)
                {
                    listLD.Add(ld);
                }

                return listLD;
            }

            //check if a dbdirid was passed in
            if (Helper.IsNumeric(searchname))
            {
                List<LDAPData> listLD = new List<LDAPData>();

                LDAPData ld = GetDataForID(searchname);

                if (ld.Found)
                {
                    listLD.Add(ld);
                }

                return listLD;
            }

            if (searchname.Contains(" "))
            {
                //first search on full name
                sFilter = "(cn=" + searchname + ")";

                List<LDAPData> listLD = GetDataList(sFilter);

                if (listLD.Count == 0)
                {
                    //nothing was found, so split the name up and seartch on the parts
                    string[] searchnameparts = searchname.Split(Convert.ToChar(" "));
                    sFilter = "(&(givenname=" + searchnameparts[0] + "*)(sn=" + searchnameparts[searchnameparts.GetUpperBound(0)] + "*))";
                    listLD = GetDataList(sFilter);
                }
                return listLD;
            }
            else
            {
                sFilter = "(|(givenname=*" + searchname + "*)(sn=*" + searchname + "*))";
                return GetDataList(sFilter);
            }       
        }

        /// <summary>
        /// gets the DBDirID for the specified email
        /// </summary>
        /// <param name="email">email address</param>
        /// <returns>string containing DBDirID</returns>
        public string GetDBDirID(string email)
        {
            string sFilter = "(mail=" + email + ")";
            string RequiredField = "dbdirid";
            return GetData(sFilter, RequiredField);
        }

        /// <summary>
        /// gets the DBDirID for the specified email
        /// </summary>
        /// <param name="email">email address</param>
        /// <returns>string containing DBDirID</returns>
        public string GetDBDirIDSafe(string email)
        {
            string sFilter = "(mail=" + email + ")";
            string RequiredField = "dbdirid";
            try
            {
                return GetData(sFilter, RequiredField);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}