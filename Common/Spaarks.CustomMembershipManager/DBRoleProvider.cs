﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration.Provider;
using Aduvo.Common.UtilityManager;

namespace Spaarks.CustomMembershipManager
{
    public class DBRoleProvider : RoleProvider
    {
        

        #region RoleProvider

        #region Constructor
        public DBRoleProvider()
        {
            //ApplicationName:
            this.ApplicationName = MembershipHelper.GetConfigValue(ConfigurationManager.AppSettings["AppName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

            //ConnectionString:
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["MembershipConnectionString"]];

            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            this.connectionString = ConnectionStringSettings.ConnectionString;

        }
        #endregion

        #region Global variables:

        //public string loggedInUserName { get; set; }
        //public string loggedInUserRole { get; set; }
        public string connectionString { get; set; }

        #endregion

        #region Initialize:
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
            {
                name = "DBRoleProvider";
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "DB Application Role Provider");
            }


            // Initialize the abstract base class.
            base.Initialize(name, config);

            //if (HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    this.loggedInUserName = HttpContext.Current.User.Identity.Name;
            //this.loggedInUserRole = GetRolesForUser(this.loggedInUserName)[0];
            //}

        }
        #endregion

        #region Provider Properties:

        public override string ApplicationName
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// Add a list of user email addresses to a list of roles
        /// </summary>
        /// <param name="usernames">array of email addresses</param>
        /// <param name="roleNames">array of role names</param>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            foreach (string username in usernames)
            {
                AddUserToRoles(username, roleNames);
            }
        }

        /// <summary>
        /// Add a user to a list of roles
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="roleNames">the array of role names</param>
        public void AddUserToRoles(string username, string[] roleNames)
        {
            foreach (string roleName in roleNames)
            {
                AddUserToRole(username, roleName);
            }
        }

        /// <summary>
        /// Add a user to a list of roles
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="roleNames">the array of role names</param>
        public void AddUserToRole(string username, string roleName)
        {
            /*
             mp_AddUserToSecurityGroup
	          @Email VARCHAR(255), @SecurityGroup VARCHAR(10)
             */

            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_AddUserToSecurityGroup", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("Email", username));
                cmd.Parameters.Add(new SqlParameter("SecurityGroup", roleName));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            conn.Close();
        }

        /// <summary>
        /// Create a role in the database
        /// </summary>
        /// <param name="roleName">The security group short name</param>
        public override void CreateRole(string roleName)
        {
            if (RoleExists(roleName))
            {
                throw new ProviderException("Role already exists.");
            }

            InsertSecurityGroup(roleName, string.Empty, string.Empty);

        }

        /// <summary>
        /// Create a role in the database
        /// </summary>
        /// <param name="roleName">The security group short name</param>
        /// <param name="FullName">The fully qualified name of the group</param>
        /// <param name="Description">A description</param>
        public void CreateRole(string roleName, string FullName, string Description)
        {
            if (RoleExists(roleName))
            {
                throw new ProviderException("Role already exists.");
            }

            InsertSecurityGroup(roleName, FullName, Description);
        }

        public int? InsertSecurityGroup(string groupName, string FullName, string Description)
        {
            return null;
        }

        /// <summary>
        /// Delete a role from the database
        /// </summary>
        /// <param name="roleName">The security group short name</param>
        /// <param name="throwOnPopulatedRole">wether to throw an error if the role has users</param>
        /// <returns>true if successful</returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (!RoleExists(roleName))
            {
                if (throwOnPopulatedRole)
                {
                    throw new ProviderException("Role does not exist.");
                }
                return false;
            }

            if (GetUsersInRole(roleName).Length > 0)
            {
                if (throwOnPopulatedRole)
                {
                    throw new ProviderException("Cannot delete a populated role.");
                }
                return false;
            }

            return DeleteSecurityGroup(roleName);
        }

        public virtual bool DeleteSecurityGroup(string groupName)
        {
            return false;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return null;
        }

        /// <summary>
        /// Get a list of all security groups
        /// </summary>
        /// <returns>array of all security group short names</returns>
        public override string[] GetAllRoles()
        {
            /*
            mp_GetSecurityGroups
	
            [Id]
            ,[RoleShortName]
            ,[RoleName]
            ,[Description]
	        */

            List<string> Roles = new List<string>();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("Role");
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_GetSecurityGroups", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                sda.SelectCommand = cmd;
                sda.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    Roles.Add(dr["RoleShortName"].ToString());
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            conn.Close();

           return Roles.ToArray();

        }

        /// <summary>
        /// Get the list of roles for a user
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <returns>array of all security group short names</returns>
        public override string[] GetRolesForUser(string username)
        {
            /*
            mp_GetSecurityGroups
	
            [Id]
            ,[RoleShortName]
            ,[RoleName]
            ,[Description]
	        */

            List<string> Roles = new List<string>();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("Role");

            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_GetSecurityGroups", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("EmailAddress", username));

                sda.SelectCommand = cmd;
                sda.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    Roles.Add(dr["RoleShortName"].ToString());
                }

            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            conn.Close();

           return Roles.ToArray();
        }

        /// <summary>
        /// Get the list of role descriptions for a user
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <returns>array of all security group short names</returns>
        public string[] GetRolesDescsForUser(string username)
        {
            /*
            mp_GetSecurityGroups
	
            [Id]
            ,[RoleShortName]
            ,[RoleName]
            ,[Description]
	        */
            List<string> Roles = new List<string>();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("Role");
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_GetSecurityGroups", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("EmailAddress", username));

                sda.SelectCommand = cmd;
                sda.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    Roles.Add(dr["RoleName"].ToString());
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            conn.Close();

            return Roles.ToArray();

        }


        /// <summary>
        /// Get a list of all users in a role
        /// </summary>
        /// <param name="roleName">the role short name</param>
        /// <returns>an array of email addresses</returns>
        public override string[] GetUsersInRole(string roleName)
        {
            return null;
        }

        /// <summary>
        /// Check if a user belongs to a group
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="roleName">the role short name</param>
        /// <returns>true if they are a member</returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            /*
              mp_IsUserInSecurityGroup
	            @Email VARCHAR(255), 
                @SecurityGroup VARCHAR(10)
              
             */

            SqlConnection conn = new SqlConnection();

            try 
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_IsUserInSecurityGroup", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("Email", username));
                cmd.Parameters.Add(new SqlParameter("SecurityGroup", roleName));
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            conn.Close();
            return false;
        }



        /// <summary>
        /// Remove a user from a role
        /// </summary>
        /// <param name="username">email address</param>
        /// <param name="roleName">role name</param>
        public void RemoveUserFromRole(string username, string roleName)
        {
            SqlConnection conn = new SqlConnection();

           try
            {
                conn.ConnectionString = this.connectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_RemoveUserFromSecurityGroup", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("Email", username));
                cmd.Parameters.Add(new SqlParameter("SecurityGroup", roleName));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LogHelper.HandleException(ex);
                }
            }
           catch (Exception ex)
           {
               LogHelper.HandleException(ex);
           }
           conn.Close();
        }



        /// <summary>
        /// Remove a list of users from a list of roles
        /// </summary>
        /// <param name="usernames">an array of email addresses</param>
        /// <param name="roleNames">an array of role short names</param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            foreach (string username in usernames)
            {
                RemoveUserFromRoles(username, roleNames);
            }
        }

        /// <summary>
        /// Remove a user from a list of roles
        /// </summary>
        /// <param name="username">the users email address</param>
        /// <param name="roleNames">an array of role short name</param>
        public virtual void RemoveUserFromRoles(string username, string[] roleNames)
        {
            foreach (string rolename in roleNames)
            {
                RemoveUserFromRole(username, rolename);
            }
        }

        /// <summary>
        /// Remove user from all roles
        /// </summary>
        /// <param name="username">email address of user</param>
        public virtual void RemoveUserFromRoles(string username)
        {
        }

        /// <summary>
        /// Check if role exists
        /// </summary>
        /// <param name="roleName">the role short name</param>
        /// <returns>True if it exisits</returns>
        public override bool RoleExists(string roleName)
        {
            return false;
        }

        #endregion
    }
}