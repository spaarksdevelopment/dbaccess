﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Aduvo.Common.UtilityManager;


namespace Spaarks.CustomMembershipManager
{
    public class DBSqlMembershipProvider : SQLMembershipProvider
    {
        #region Constructor:
        public DBSqlMembershipProvider()
        {
            //Set Membership Properties' values from web.config:

            //ApplicationName:
            this.ApplicationName = MembershipHelper.GetConfigValue(ConfigurationManager.AppSettings["AppName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

            //ConnectionString:
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["MembershipConnectionString"]];

            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            this.connectionString = ConnectionStringSettings.ConnectionString;

            //chacheTimeSpan:
            int cacheMinutes = 60;
            if (ConfigurationManager.AppSettings["UserCacheMinutes"] != null)
            {
                cacheMinutes = Helper.SafeInt(ConfigurationManager.AppSettings["UserCacheMinutes"]);
            }
            this.cacheTimeSpan = new TimeSpan(0, cacheMinutes, 0);
        }
        #endregion

        #region Global Variables:

        public string connectionString { get; set; }

        protected bool TestMode { get; set; }

        #endregion

        #region Initialize:

        public override void Initialize(string name, NameValueCollection config)
        {
            if (name == null || name.Length == 0)
            {
                name = "DBMembershipProvider";
            }

            base.Initialize(name, config);
        }
        #endregion

        #region Membership Properties:

        public override string ApplicationName
        {
            get;
            set;
        }


        protected TimeSpan cacheTimeSpan
        {
            get;
            set;
        }

        #region unused properties:
        /*
            private bool _EnablePasswordReset;
            public override bool EnablePasswordReset
            {
                get { return _EnablePasswordReset; }
            }

            private bool _EnablePasswordRetrieval;
            public override bool EnablePasswordRetrieval
            {
                get { return _EnablePasswordRetrieval; }
            }

            private bool _RequiresQuestionAndAnswer;
            public override bool RequiresQuestionAndAnswer
            {
                get { return _RequiresQuestionAndAnswer; }
            }

            private bool _RequiresUniqueEmail;
            public override bool RequiresUniqueEmail
            {
                get { return _RequiresUniqueEmail; }
            }

            private int _MaxInvalidPasswordAttempts;
            public override int MaxInvalidPasswordAttempts
            {
                get { return _MaxInvalidPasswordAttempts; }
            }
            private int _PasswordAttemptWindow;
            public override int PasswordAttemptWindow
            {
                get { return _PasswordAttemptWindow; }
            }

            private MembershipPasswordFormat _PasswordFormat;
            public override MembershipPasswordFormat PasswordFormat
            {
                get { return _PasswordFormat; }
            }

            private int _MinRequiredNonAlphanumericCharacters;

            public override int MinRequiredNonAlphanumericCharacters
            {
                get { return _MinRequiredNonAlphanumericCharacters; }
            }

            private int _MinRequiredPasswordLength;

            public override int MinRequiredPasswordLength
            {
                get { return _MinRequiredPasswordLength; }
            }

            private string _PasswordStrengthRegularExpression;

            public override string PasswordStrengthRegularExpression
            {
                get { return _PasswordStrengthRegularExpression; }
            }
             * */
        #endregion

        #endregion

        #region Get user Details from HttpContext:

        //Authentication:
        public static bool GetCurrentIsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        //Name
        public static string GetCurrentUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        //Full Record
        public DBAppUser GetUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return (DBAppUser)GetUser(HttpContext.Current.User.Identity.Name, true);
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Get Membership User from HttpContext:

        //By EmailAddress:
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            DBAppUser appUser = null;

            if (HttpContext.Current.Cache["UserByEmail_" + username] != null)
            {
                appUser = (DBAppUser)HttpContext.Current.Cache["UserByEmail_" + username];
            }
            else
            {
                appUser = GetDBAppUser(username);

                if (appUser != null)
                {
                    AddUserToCache(appUser);
                }
                else
                {
                    throw new ProviderException("User not found");
                }
            }

            UpdateActivity(username, 0, userIsOnline);

            return appUser;

        }

        //By Id:
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            int dbPeopleId = Convert.ToInt32(providerUserKey);
            DBAppUser appUser = null;

            if (HttpContext.Current.Cache["UserById_" + dbPeopleId.ToString()] != null)
            {
                appUser = (DBAppUser)HttpContext.Current.Cache["UserById_" + dbPeopleId.ToString()];
            }
            else
            {
                appUser = GetDBAppUser(dbPeopleId);

                if (appUser != null)
                {
                    AddUserToCache(appUser);
                }
                else
                {
                    throw new ProviderException("User not found");
                }
            }

            UpdateActivity(string.Empty, dbPeopleId, userIsOnline);

            return appUser;

        }
        #endregion

        #region Get DBAppUser:

        public DBAppUser GetDBAppUser(string userName)
        {
            //user to be returned:
            DBAppUser dbau = null;

            CommandType objectCommandType = CommandType.StoredProcedure;
            DataTable dtUser = base.GetAppUserDetails(userName, this.connectionString, "mp_GetDBUser", objectCommandType, "EmailAddress");
            
            if (dtUser.Rows.Count > 0)
            {
                dbau = GetDBAppUser(dtUser.Rows[0]);  
            }

            return dbau;
        }

        public DBAppUser GetDBAppUser(int userId)
        {
            //user to be returned:
            DBAppUser dbau = null;

            CommandType objectCommandType = CommandType.StoredProcedure;
            DataTable dtUser = base.GetAppUserDetails(userId, this.connectionString, "mp_GetDBUser", objectCommandType, "UserID");

            if (dtUser.Rows.Count > 0)
            {
                dbau = GetDBAppUser(dtUser.Rows[0]);
            }

            return dbau;
        }

        protected MembershipUserCollection GetDBAppUsers(DataTable dt)
        {
            MembershipUserCollection aul = new MembershipUserCollection();

            foreach (DataRow dr in dt.Rows)
            {
                DBAppUser dbau = GetDBAppUser(dr);

                if (dbau != null) aul.Add(dbau);
            }

            return aul;
        }

        protected DBAppUser GetDBAppUser(DataRow dr)
        {
            object dbPeopleID;
            if (dr["dbPeopleID"] != null)
            {
                dbPeopleID = dr["dbPeopleID"];
            }
            else
            {
                dbPeopleID = dr["Id"];
            }
            string email = dr["EmailAddress"].ToString();
            string forename = dr["Forename"].ToString();
            string surname = dr["Surname"].ToString();
            string telephone = dr["Telephone"].ToString();
            string costCenter = dr["CostCentre"].ToString();
            string ubr = dr["UBR"].ToString();
            bool enabled = (bool)dr["Enabled"];
            DateTime created = Helper.SafeDateTime(dr["CreatedDateTime"]);
            int createdByUserId = Helper.SafeInt(dr["CreatedByUserId"]);
            DateTime lastLogin = Helper.SafeDateTime(dr["LastLoggedInDateTime"]);
            DateTime lastActivity = Helper.SafeDateTime(dr["LastActivityDateTime"]);
            int visitCount = Helper.SafeInt(dr["VisitCount"]);
            int vendorId = Helper.SafeInt(dr["VendorID"]);
            
            string rolesShrtNamesList = dr["RolesShortNamesList"].ToString();
            char[] charSeparators = new char[] {','};
            List<string> rolesShortNamesList = new List<string>(rolesShrtNamesList.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries));
            string rolesNamesList = dr["RolesNamesList"].ToString();
			int languageId = Helper.SafeInt(dr["LanguageID"]);

            DBAppUser dbau = new DBAppUser(Membership.Provider.Name, email, dbPeopleID, email, "", "", enabled, !enabled, created, lastLogin, lastActivity, new DateTime(), new DateTime(), forename, surname, telephone, ubr, costCenter, vendorId, createdByUserId, createdByUserId, visitCount, rolesShortNamesList, rolesNamesList,languageId);
            return dbau;
        }
        #endregion

        #region FindUsersByName

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            CommandType objectCommandType = CommandType.StoredProcedure;
            DataTable dt = base.GetAppUserDetails(usernameToMatch, connectionString, "mp_GetMembershipUsersByName", objectCommandType, "Name");

            totalRecords = dt.Rows.Count;

            MembershipUserCollection muc = new MembershipUserCollection();

            if (totalRecords > 0)
            {
                DBAppUser dbau = GetDBAppUser(dt.Rows[0]);
                if (dbau != null)
                {
                    muc.Add(dbau);
                }
            }

            return muc;
        }

        #endregion

        #region GetAllUsers

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable("User");

            SqlConnection conn;
            using (conn = new SqlConnection (connectionString))
            {
                SqlCommand cmd = new SqlCommand("mp_GetMembershipUsersByName", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                sda.SelectCommand = cmd;
                sda.Fill(dt);

                conn.Close();
            }

            totalRecords = dt.Rows.Count;

            MembershipUserCollection muc = new MembershipUserCollection();

            if (totalRecords > 0)
            {
                muc = GetDBAppUsers(dt);
            }

            return muc;
        }

        #endregion

        #region GetUserNameByEmail

        public override string GetUserNameByEmail(string email)
        {
            string _name = string.Empty;

            //check cache for this user's name first
            if (HttpContext.Current.Cache["UserNameByEmail_" + email] != null)
            {
                _name = (string)HttpContext.Current.Cache["UserNameByEmail_" + email];
            }
            else
            {
                DBAppUser DBAppUser = (DBAppUser)GetUser(email, false);
                _name = DBAppUser.UserName;

                //add name to cache for easy retrieval later
                HttpContext.Current.Cache.Add("UserNameByEmail_" + email, _name, null, System.Web.Caching.Cache.NoAbsoluteExpiration, cacheTimeSpan, CacheItemPriority.Normal, null);
            }

            return _name;
        }

        #endregion

        #region Update User Activity:

        protected void UpdateActivity(string userName, int userId, bool userIsOnline)
        {
            //in order to avoid updating user activity many times in one request, 
            //ensure a minute has elapsed since the last time user activity was updated
            //by caching a variable and allowing it to expire

            string cachekey = "UserActivity_" + userName + "_" + userId;

            if (userIsOnline)
            {
                object ActivityCache = HttpContext.Current.Cache[cachekey];
                if (ActivityCache == null)
                {
                    UpdateUserActivity(userName, userId, false);
                    HttpContext.Current.Cache.Insert(cachekey, true, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero);
                }
            }
        }

        /// <summary>
        /// Update the user activity
        /// </summary>
        /// <param name="userName">username (only required if id missing)</param>
        /// <param name="userId">user id (only required if username missing)</param>
        /// <param name="isLogin">is this the initial login?</param>
        public void UpdateUserActivity(string userName, int userId, bool isLogin)
        {
            //[TO Be Updated]
            if (isLogin) RemoveUserFromCache(userName, userId.ToString());

            /*
                mp_UpdateUserActivity]
	                @EmailAddress VARCHAR(200) = '',
	                @UserId INT = 0,
	                @Login BIT = 0
            */

            string ConnectionPoolMinSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMinSize"];
            string ConnectionPoolMaxSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMaxSize"];
            string ConnectionTimeOutPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutPooled"];
            //string sConnQualified = "Data Source=tfs.spaarks.com,1436;Initial Catalog=DBAccess;User ID=dbAccessUser;Password=accessme;" + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            string sConnQualified = this.connectionString + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            SqlConnection conn = new SqlConnection();
            try
            {
                //try a connection from the pool
                conn.ConnectionString = sConnQualified;
                conn.Open();

                SqlCommand cmd = new SqlCommand("mp_UpdateUserActivity", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("UserID", userId));
                cmd.Parameters.Add(new SqlParameter("EmailAddress", userName));
                cmd.Parameters.Add(new SqlParameter("Login", isLogin));

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex1)
            {
                LogHelper.HandleException(ex1);

                //try clearing pools
                SqlConnection.ClearAllPools();

                try
                {
                    conn.ConnectionString = sConnQualified;
                    conn.Open();
                }
                catch (SqlException ex2)
                {
                    LogHelper.HandleException(ex2);

                    //open connection without using pooling
                    if (conn.State != ConnectionState.Closed) conn.Close();
                    string ConnectionTimeOutNonPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutNonPooled"];
                    sConnQualified = this.connectionString + ";Pooling=false;Connect Timeout=" + ConnectionTimeOutNonPooled;
                    conn.ConnectionString = sConnQualified;
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            //close connection string:
            conn.Close();

        }

        #endregion

        #region Cache Methods:

        protected void AddUserToCache(DBAppUser DBAppUser)
        {
            //cache result by email address and id
            //it could be requested by either
            HttpContext.Current.Cache.Add("UserByEmail_" + DBAppUser.Email, DBAppUser, null, System.Web.Caching.Cache.NoAbsoluteExpiration, cacheTimeSpan, CacheItemPriority.Normal, null);
            HttpContext.Current.Cache.Add("UserById_" + DBAppUser.dbPeopleID, DBAppUser, null, System.Web.Caching.Cache.NoAbsoluteExpiration, cacheTimeSpan, CacheItemPriority.Normal, null);
        }

        protected void RemoveUserFromCache(string Email, string Id)
        {
            if (Email != string.Empty) HttpContext.Current.Cache.Remove("UserByEmail_" + Email);
            if (Id != string.Empty) HttpContext.Current.Cache.Remove("UserById_" + Id);
        }

        #endregion

        #region ValidateUser:

        public override bool ValidateUser(string username, string password)
        {
            bool boolReturn;

           DBAppUser u = GetDBAppUser(username);

           if (u == null)
           {
               boolReturn = false;
           }
           else
           {
               boolReturn = u.Enabled;
           }

           if (boolReturn)
           {
               UpdateUserActivity(u.Email, u.dbPeopleID, true);
           }

            return boolReturn;
        }

        #endregion

        #region validate email address:

        /// <summary>
        /// Validates the string passed in as a valid DB email address and returns a boolean;
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        public static bool ValidateEmailAddress(string EmailAddress)
        {
            if (string.IsNullOrEmpty(EmailAddress)) return false;

            string strRegex = @"^[\w-]+[\.\w-]*@db.com$";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(EmailAddress)) { return true; } else { return false; }
        }

        /// <summary>
        /// Splits and validates the string passed in as valid DB email addresses and returns a boolean;
        /// </summary>
        /// <param name="EmailAddress">The email address to validate</param>
        /// <param name="Delimiter">The value to split the string by</param>
        public static bool ValidateMultipleEmailAddress(string EmailAddress, Char[] delimiter)
        {
            string strRegex = @"^[\w-]+[\.\w-]*@db.com$";
            string[] strArr = EmailAddress.Split(delimiter);
            foreach (string s in strArr)
            {
                if (s.Trim() != "")
                {
                    Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
                    if (!re.IsMatch(s.Trim())) { return false; }
                }
            }
            return true;
        }

        #endregion


    }
}