﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using Aduvo.Common.UtilityManager;

namespace Spaarks.CustomMembershipManager
{
    public class SQLMembershipProvider : MembershipProvider
    {
        #region Initialize:

        public override void Initialize(string name, NameValueCollection config)
        {
            /*Get values from web.config:
             ============================*/
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (name == null || name.Length == 0)
            {
                name = "SqlMembershipProvider";
            }

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Spaarks Custom Membership Provider");
            }

            // Initialize the abstract base class:
            base.Initialize(name, config);

            //ApplicationName:
            _ApplicationName = MembershipHelper.GetConfigValue(ConfigurationManager.AppSettings["AppName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        }
        #endregion

        #region Membership Properties:

        private string _ApplicationName;
        public override string ApplicationName
        {
            get { return _ApplicationName; }
            set { _ApplicationName = value; }
        }
        #endregion

        #region Get Application User Details:

        //By userName
        public DataTable GetAppUserDetails(string userName, string connectionString, string databaseObject, CommandType databseObjectType, string parameter)
        {
            //sql variables:
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dtUser = new DataTable("User");

            string ConnectionPoolMinSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMinSize"];
            string ConnectionPoolMaxSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMaxSize"];
            string ConnectionTimeOutPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutPooled"];
            //string sConnQualified = "Data Source=tfs.spaarks.com,1436;Initial Catalog=DBAccess;User ID=dbAccessUser;Password=accessme;" + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            string sConnQualified = connectionString + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            SqlConnection conn = new SqlConnection();
            try
            {
                //try a connection from the pool
                conn.ConnectionString = sConnQualified;
                conn.Open();

                SqlCommand cmd = new SqlCommand(databaseObject, conn);

                cmd.CommandType = databseObjectType;

                cmd.Parameters.Add(new SqlParameter(parameter, userName));

                sda.SelectCommand = cmd;
                sda.Fill(dtUser);
            }
            catch (SqlException ex1)
            {
                LogHelper.HandleException(ex1);

                //try clearing pools
                SqlConnection.ClearAllPools();

                try
                {
                    conn.ConnectionString = sConnQualified;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(databaseObject, conn);

                    cmd.CommandType = databseObjectType;

                    cmd.Parameters.Add(new SqlParameter(parameter, userName));

                    sda.SelectCommand = cmd;
                    sda.Fill(dtUser);
                }
                catch (SqlException ex2)
                {
                    LogHelper.HandleException(ex2);

                    //open connection without using pooling
                    if (conn.State != ConnectionState.Closed) conn.Close();
                    string ConnectionTimeOutNonPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutNonPooled"];
                    sConnQualified = connectionString + ";Pooling=false;Connect Timeout=" + ConnectionTimeOutNonPooled;
                    conn.ConnectionString = sConnQualified;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(databaseObject, conn);

                    cmd.CommandType = databseObjectType;

                    cmd.Parameters.Add(new SqlParameter(parameter, userName));

                    sda.SelectCommand = cmd;
                    sda.Fill(dtUser);
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            //close connection string:
            conn.Close();
            return dtUser;
            
        }

        //By userId
        public DataTable GetAppUserDetails(int userId, string connectionString, string databaseObject, CommandType databseObjectType, string parameter)
        {
            //sql variables:
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dtUser = new DataTable("User");

            string ConnectionPoolMinSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMinSize"];
            string ConnectionPoolMaxSize = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionPoolMaxSize"];
            string ConnectionTimeOutPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutPooled"];
            //string sConnQualified = "Data Source=tfs.spaarks.com,1436;Initial Catalog=DBAccess;User ID=dbAccessUser;Password=accessme;" + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            string sConnQualified = connectionString + ";Min Pool Size=" + ConnectionPoolMinSize + ";Max Pool Size=" + ConnectionPoolMaxSize + ";Connect Timeout=" + ConnectionTimeOutPooled;
            SqlConnection conn = new SqlConnection();
            try
            {
                //try a connection from the pool
                conn.ConnectionString = sConnQualified;
                conn.Open();
                SqlCommand cmd = new SqlCommand(databaseObject, conn);
                cmd.CommandType = databseObjectType;

                cmd.Parameters.Add(new SqlParameter(parameter, userId));

                sda.SelectCommand = cmd;
                sda.Fill(dtUser);

            }
            catch (SqlException ex1)
            {
                LogHelper.HandleException(ex1);

                //try clearing pools
                SqlConnection.ClearAllPools();

                try
                {
                    conn.ConnectionString = sConnQualified;
                    conn.Open();
                }
                catch (SqlException ex2)
                {
                    LogHelper.HandleException(ex2);

                    //open connection without using pooling
                    if (conn.State != ConnectionState.Closed) conn.Close();
                    string ConnectionTimeOutNonPooled = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionTimeOutNonPooled"];
                    sConnQualified = connectionString + ";Pooling=false;Connect Timeout=" + ConnectionTimeOutNonPooled;
                    conn.ConnectionString = sConnQualified;
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            //close connection string:
            conn.Close();
            return dtUser;

        }

        #endregion

        #region NotImplemented members

         public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
         {
             throw new NotImplementedException();
         }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public bool LockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        


        #endregion

        #region EnablePasswordRetrieval

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        #endregion

        #region EnablePasswordReset

        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        #endregion

        #region DeleteUser

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return false;
        }

        #endregion

       

    }
}