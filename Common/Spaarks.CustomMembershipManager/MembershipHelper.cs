﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spaarks.CustomMembershipManager
{
    public class MembershipHelper
    {
        #region Retrieve config values:

        /*Retrieve Config Values from web.config:
         ========================================*/

        public static string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }
            else
            {
                return configValue;
            }
        }
        #endregion
    }
}