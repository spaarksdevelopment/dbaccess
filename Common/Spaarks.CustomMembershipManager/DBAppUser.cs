﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spaarks.CustomMembershipManager
{
    [Serializable]
    public class DBAppUser : SpaarksAppUser
    {
        #region Constructors:

        public DBAppUser(string providername,
                            string username,
                            object providerUserKey,
                            string email,
                            string passwordQuestion,
                            string comment,
                            bool isApproved,
                            bool isLockedOut,
                            DateTime creationDate,
                            DateTime lastLoginDate,
                            DateTime lastActivityDate,
                            DateTime lastPasswordChangedDate,
                            DateTime lastLockedOutDate,
                            string forename,
                            string surname,
                            //int dbDirId,
                            string telephone,
                            //string exClass,
                            string ubr,
                            string costCentre,
                            //string costCentreName,
                            //string legalEntityID,
                            //string legalEntity,
                            //string divPath,
                            //string managerName,
                            //string cityName,
                            //string countryName,
                            //string status,
                            int vendorId,
                            //string vendorName,
                            //bool isExternal,
                            //string officeId,
                            int createdByUserId,
                            int updatedByUserId,
                            int visitCount,
                            List<string> rolesShortNamesList,
                            string rolesNamesList,
							int languageId)
            :
                        base(providername,
                                    username,
                                    providerUserKey,
                                    email,
                                    passwordQuestion,
                                    comment,
                                    isApproved,
                                    isLockedOut,
                                    creationDate,
                                    lastLoginDate,
                                    lastActivityDate,
                                    lastPasswordChangedDate,
                                    lastLockedOutDate,
                                    forename,
                                    surname,
                                    telephone,
                                    createdByUserId,
                                    updatedByUserId,
                                    visitCount,
                                    rolesShortNamesList,
                                    rolesNamesList,
									languageId)
        {
            //this.DBDirID = dbDirId;
            base.Email = username;
            //this.EXClass = exClass;
            this.UBR = ubr;
            this.CostCentre = costCentre;
            //this.CostCentreName = costCentreName;
            //this.LegalEntityID = legalEntityID;
            //this.LegalEntity = legalEntity;
            //this.DivPath = divPath;
            //this.ManagerName = managerName;
            //this.CityName = cityName;
            //this.CountryName = countryName;
            //this.Status = status;
            this.VendorID = vendorId;
            //this.VendorName = vendorName;
            //this.IsExtrnal = isExternal;
            this.Enabled = isApproved & !isLockedOut;
            //this.OfficerId = officeId;
			//this.LanguageId = languageId;
        }

         
        #endregion

        #region DBAppUser Properties:

        public int dbPeopleID
        {
            get { return Convert.ToInt32(base.ProviderUserKey); }
        }

        //private int _dbDirId;
        //public int DBDirID
        //{
        //    get { return _dbDirId; }
        //    set { _dbDirId = value; }
        //}

        //private string _exClass;
        //public string EXClass
        //{
        //    get { return _exClass; }
        //    set { _exClass = value; }
        //}

        private string _ubr;
        public string UBR
        {
            get { return _ubr; }
            set { _ubr = value; }
        }

        private string _costCentre;
        public string CostCentre
        {
            get { return _costCentre; }
            set { _costCentre = value; }
        }

        //private string _costCentreName;
        //public string CostCentreName
        //{
        //    get { return _costCentreName; }
        //    set { _costCentreName = value; }
        //}

        //private string _legalEntityID;
        //public string LegalEntityID
        //{
        //    get { return _legalEntityID; }
        //    set { _legalEntityID = value; }
        //}

        //private string _legalEntity;
        //public string LegalEntity
        //{
        //    get { return _legalEntity; }
        //    set { _legalEntity = value; }
        //}

        //private string _divPath;
        //public string DivPath
        //{
        //    get { return _divPath; }
        //    set { _divPath = value ; }
        //}

        //private string _managerName;
        //public string ManagerName
        //{
        //    get { return _managerName; }
        //    set { _managerName = value; }
        //}

        //private string _cityName;
        //public string CityName
        //{
        //    get { return _cityName; }
        //    set { _cityName = value; }
        //}

        //private string _countryName;
        //public string CountryName
        //{
        //    get { return _countryName; }
        //    set { _countryName = value; }
        //}

        //private string _status;
        //public string Status
        //{
        //    get { return _status; }
        //    set { _status = value; }
        //}

        private int _vendorId;
        public int VendorID
        {
            get { return _vendorId; }
            set { _vendorId = value; }
        }

        //private string _vendorName;
        //public string VendorName
        //{
        //    get { return _vendorName; }
        //    set { _vendorName = value; }
        //}

        //private bool _isExternal;
        //public bool IsExtrnal
        //{
        //    get { return _isExternal; }
        //    set { _isExternal = value; }
        //}

        private bool _enabled;
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

		//private int _languageId;
		//public int LanguageId
		//{
		//    get { return _languageId; }
		//    set { _languageId = value; }
		//}

        //private string _officerId;
        //public string OfficerId
        //{
        //    get { return _officerId; }
        //    set { _officerId = value; }
        //}

        #endregion
    }
}