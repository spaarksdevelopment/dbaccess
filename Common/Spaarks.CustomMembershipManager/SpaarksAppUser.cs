﻿using System;
using System.Configuration.Provider;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Caching;
using Aduvo.Common.UtilityManager;

namespace Spaarks.CustomMembershipManager
{
    [Serializable]
    public class SpaarksAppUser : MembershipUser
    {
       
        #region Constructors:

        public  SpaarksAppUser(string providername,
                                    string username,
                                    object providerUserKey,
                                    string email,
                                    string passwordQuestion,
                                    string comment,
                                    bool isApproved,
                                    bool isLockedOut,
                                    DateTime creationDate,
                                    DateTime lastLoginDate,
                                    DateTime lastActivityDate,
                                    DateTime lastPasswordChangedDate,
                                    DateTime lastLockedOutDate,
                                    string forename,
                                    string surname,
                                    string telephone,
                                    int createdByUserId,
                                    int updatedByUserId,
                                    int visitCount,
                                    List<string> rolesShortNamesList,
                                    string rolesNamesList,
									int languageId)
            :
                                    base(providername,
                                        username,
                                        providerUserKey,
                                        email,
                                        passwordQuestion,
                                        comment,
                                        isApproved,
                                        isLockedOut,
                                        creationDate,
                                        lastLoginDate,
                                        lastActivityDate,
                                        lastPasswordChangedDate,
                                        lastLockedOutDate)
        {
            this.Forename = forename;
            this.Surname = surname;
            this.Telephone = telephone;
            this.CreatedByUserId = createdByUserId;
            this.UpdatedByUserId = updatedByUserId;
            this.VisitCount = visitCount;
            this.RolesShortNamesList = rolesShortNamesList;
            this.RolesNamesList = rolesNamesList;
			this.LanguageId = languageId;
        
        }

      
        #endregion

        #region SpaarksAppUser Properties:
        
              
       //Forename
        private string _forename = string.Empty;
        public string Forename
        {
            get { return _forename; }
            set { _forename = value; }
        }
        
        //Surname
        private string _surname = string.Empty; 
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        //FullName
         public string FullName
        {
            get
            {
                if ((_surname == string.Empty) & (_forename == string.Empty)) GetNameFromEmail();
                return _forename + " " + _surname;
            }
            
        }

        private void GetNameFromEmail()
        {
            //get name by splitting email address
            string _EmailAddress = base.Email;
            if (_EmailAddress != string.Empty)
            {
                int pos = _EmailAddress.IndexOf("@");
                string fullname = _EmailAddress.Substring(0, pos).Replace("."," ");
                fullname = Helper.ToTitleCase(fullname);
                pos = fullname.LastIndexOf(" ");
                if (pos > 0)
                {
                    _surname = fullname.Substring(pos + 1);
                    _forename = fullname.Substring(0, pos);
                }
                else
                {
                    _surname = fullname;
                }
            }
       }
        
        private string _telephone = string.Empty;
        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }

       

        private int _createdByUserId;

        public int CreatedByUserId
        {
            get { return _createdByUserId; }
            set { _createdByUserId = value; }
        }

        private int _updatedByUserId;

        public int UpdatedByUserId
        {
            get { return _updatedByUserId; }
            set { _updatedByUserId = value; }
        }

        private int _visitCount;

        public int VisitCount
        {
            get { return _visitCount; }
            set { _visitCount = value; }
        }
        
        public string DisplayName
        {
            get
            {
                if ((_surname == string.Empty) & (_forename == string.Empty)) GetNameFromEmail();
                return _forename + " " + _surname + " (" + base.Email + ")";
            }
        }

        private List<string> _rolesShortNamesList = new List<string>();
        public List<string> RolesShortNamesList
        {
            get { return _rolesShortNamesList; }
            set { _rolesShortNamesList = value; }
        }

        private string _rolesNameslist = string.Empty;
        public string RolesNamesList
        {
            get { return _rolesNameslist; }
            set { _rolesNameslist = value; }
        }

        public bool IsInRole(string RoleName)
        {
            return _rolesShortNamesList.Contains(RoleName);
        }


		private int _languageId;
		public int LanguageId
		{
			get { return _languageId; }
			set { _languageId = value; }
		}
        #endregion
    }
}