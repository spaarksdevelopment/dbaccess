﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region static get methods

        public static List<vw_DistinctDivisionAccessAreasWithApprovers> GetAccessAreasByDA(int userId, int? languageID)
        {
            List<vw_DistinctDivisionAccessAreasWithApprovers> items = new List<vw_DistinctDivisionAccessAreasWithApprovers>();

            try
            {
                items = CorporationManager.GetAccessAreasByDA(userId, languageID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<vw_DivisionAccessAreasWithApprovers> GetAccessAreasAndApproversByDA(int userId)
        {
            List<vw_DivisionAccessAreasWithApprovers> items = new List<vw_DivisionAccessAreasWithApprovers>();

            try
            {
                items = CorporationManager.GetAccessAreasAndApproversByDA(userId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        /// <summary>
        /// Populates the approvers grid on the My Access Areas page
        /// </summary>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public static List<AccessAreaApprover> GetMyAccessAreaApprovers(int selectedCorporateDivisionAccessAreaId, int role)
        {
            List<AccessAreaApprover> items = new List<AccessAreaApprover>();

            try
            {
                items = CorporationManager.GetMyAccessAreaApprovers(selectedCorporateDivisionAccessAreaId, role);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        /// <summary>
        /// Method to go get the external class approvers 
        /// based on classification ID
        /// E.Parker
        /// 26.05.11
        /// </summary>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="role"></param>
        /// <param name="classificationID"></param>
        /// <returns></returns>
        public static List<DAL.GetMyAccessAreaApproversExternalClass> GetMyAccessAreaApproversEx(int selectedCorporateDivisionAccessAreaId, int role, int classificationID)
        {
            List<GetMyAccessAreaApproversExternalClass> items = new List<GetMyAccessAreaApproversExternalClass>();

            try
            {
                items = CorporationManager.GetMyAccessAreaApproversEx(selectedCorporateDivisionAccessAreaId, role, classificationID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID)
        {
            List<DivisionRoleUser> items = new List<DivisionRoleUser>();

            try
            {
                items = CorporationManager.GetDivisionRoleUsers(divisionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<RoleForRecertification> GetRolesForRecertification(int userId)
        {
            List<RoleForRecertification> items = new List<RoleForRecertification>();
            try
            {
                items = CorporationManager.GetRolesForRecertification(userId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<RoleForRecertification> GetDARolesForRecertification(int userId)
        {
            List<RoleForRecertification> items = new List<RoleForRecertification>();
            try
            {
                items = CorporationManager.GetDARolesForRecertification(userId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static int? GetCertifiedDACount(int userId, int actionType, string[] selectedUsers)
        {
            try
            {
                string delimitedSelectedUsers = String.Join(",", selectedUsers);

                return CorporationManager.GetCertifiedDACount(userId, actionType, delimitedSelectedUsers);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<BusinessAreaApproverForRecertification> GetApproversForRecertificationByDivisionId(int divisionId)
        {
            List<BusinessAreaApproverForRecertification> items = new List<BusinessAreaApproverForRecertification>();
            try
            {
                items = CorporationManager.GetApproversForRecertificationByDivisionId(divisionId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<GetMyDivisions> GetAllMyDivisions(int userId, int? languageID)
        {
            List<GetMyDivisions> items = new List<GetMyDivisions>();
            try
            {
                items = CorporationManager.GetAllMyDivisions(userId, languageID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
            return items;
        }

        public static List<KeyValuePair<int, string>> GetRecertPeriods()
        {
            List<KeyValuePair<int, string>> recertPeriods = new List<KeyValuePair<int, string>>();

            recertPeriods.Add(new KeyValuePair<int, string>(1, "Monthly"));
            recertPeriods.Add(new KeyValuePair<int, string>(3, "Quarterly"));
            recertPeriods.Add(new KeyValuePair<int, string>(6, "Biannually"));
            recertPeriods.Add(new KeyValuePair<int, string>(12, "Annually"));

            return recertPeriods;
        }

        public static List<KeyValuePair<int, string>> GetRecertPeriods(int? languageID)
        {
            List<KeyValuePair<int, string>> recertPeriods = new List<KeyValuePair<int, string>>();

            switch (languageID)
            {
                case 1:
                default:
                    recertPeriods.Add(new KeyValuePair<int, string>(1, "Monthly"));
                    recertPeriods.Add(new KeyValuePair<int, string>(3, "Quarterly"));
                    recertPeriods.Add(new KeyValuePair<int, string>(6, "Biannually"));
                    recertPeriods.Add(new KeyValuePair<int, string>(12, "Annually"));
                    break;
                case 2:
                    recertPeriods.Add(new KeyValuePair<int, string>(1, "Monatlich"));
                    recertPeriods.Add(new KeyValuePair<int, string>(3, "Vierteljährlich"));
                    recertPeriods.Add(new KeyValuePair<int, string>(6, "Halbjährlich"));
                    recertPeriods.Add(new KeyValuePair<int, string>(12, "Jährlich"));
                    break;

                case 3:
                    recertPeriods.Add(new KeyValuePair<int, string>(1, "Mensile"));
                    recertPeriods.Add(new KeyValuePair<int, string>(3, "Trimestrale"));
                    recertPeriods.Add(new KeyValuePair<int, string>(6, "Semestrale"));
                    recertPeriods.Add(new KeyValuePair<int, string>(12, "Annuale"));
                    break;

            }

            return recertPeriods;
        }

        public static bool IsSubdivision(CorporateDivision division)
        {
            if (division != null && division.IsSubdivision != null)
                return (bool)division.IsSubdivision;

            return false;
        }

        #region Business Areas

        /// <summary>
        /// Method to delete the RA approvers from the My Roles page
        /// E.Parker
        /// 13.09.11
        /// </summary>
        /// <returns></returns>
        public static bool DeleteRAApprovers(int CorporateBusinessAreaApproverId)
        {
            bool result = false;
            try
            {
                result = Managers.CorporationManager.DeleteRAApprovers(CorporateBusinessAreaApproverId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return result;
        }

        /// <summary>
        /// Method to delete the AA approvers from the My Roles page
        /// E.Parker
        /// 13.09.11
        /// </summary>
        /// <returns></returns>
        public static bool DeleteAApprovers(int AccessAreaApproverID)
        {
            bool result = false;
            try
            {
                result = Managers.CorporationManager.DeleteAApprovers(AccessAreaApproverID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return result;
        }


        public static List<DAL.vw_BusinessAreasWithApprovers> GetMyDivisions(int UserId, int? divisionId)
        {
            List<DAL.vw_BusinessAreasWithApprovers> items = new List<DAL.vw_BusinessAreasWithApprovers>();

            try
            {
                items = Managers.CorporationManager.GetMyDivisions(UserId, divisionId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return items;
        }
        #endregion


        public static CorporateDivision GetDivision(int divisionId)
        {
            try
            {
                return CorporationManager.GetDivision(divisionId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static CorporateBusinessArea GetBusinessArea(int BusinessAreaId)
        {
            try
            {
                return CorporationManager.GetBusinessArea(BusinessAreaId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static CorporateBusinessArea GetBusinessArea(int divisionID, string ubr)
        {
            try
            {
                return CorporationManager.GetBusinessArea(divisionID, ubr);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int DeleteDivision(int divisionID)
        {
            try
            {
                return CorporationManager.DeleteDivision(divisionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return 1;
            }
        }

        public static GetCorporateBusinessArea GetVwBusinessArea(int BusinessAreaId, int? languageID)
        {
            try
            {
                return CorporationManager.GetVwBusinessArea(BusinessAreaId, languageID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<string[,]> GetDivisionsByCountry(int countryId)
        {
            try
            {
                return CorporationManager.GetDivisionsByCountry(countryId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// AC: added 1/3/11 to support Division Profile option on Access Request page.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static List<string[,]> GetDivisionsForCity(int cityId)
        {
            try
            {
                return CorporationManager.GetDivisions(cityId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static string GetUBRTitle(string _Code)
        {
            try
            {
                DAL.UBRCode _UBRCode = CorporationManager.GetUBRCode(_Code);
                if (_UBRCode == null)
                    return string.Empty;
                else
                    return _UBRCode.UBR_Name;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return string.Empty;
            }
        }

        public static List<UBRNodes> GetUBRNodes(string startNode, bool IsRoot)
        {
            try
            {
                return CorporationManager.GetUBRNodes(startNode, IsRoot);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<UBRDivision> GetUBRDivisions(string startNode, bool IsRoot)
        {
            try
            {
                return CorporationManager.GetUBRDivisions(startNode, IsRoot, null);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<UBRDivision> GetUBRDivisions(string startNode, bool IsRoot, int? countryFilterID)
        {
            try
            {
                return CorporationManager.GetUBRDivisions(startNode, IsRoot, countryFilterID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<UBRDivision> GetSubdivisions(int? existingCountryFilterID, int divisionID)
        {
            try
            {
                return CorporationManager.GetSubdivisions(existingCountryFilterID, divisionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static System.Collections.ArrayList GetUBRPath(string targetUBR)
        {
            try
            {
                return CorporationManager.GetUBRPath(targetUBR);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Method to call the manager for the recerification access areas for a specific person
        /// E.Parker
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <returns></returns>
        public static List<vw_RecertifierAccessAreas> GetMyRecertifyAccessAreas(int dbPeopleID)
        {
            try
            {
                return CorporationManager.GetMyRecertifyAccessAreas(dbPeopleID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }



        /// <summary>
        /// Method to call the manager for the recerification access areas for a specific person
        /// E.Parker
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <returns></returns>
        public static List<vw_AdminRecertifierAccessAreas> GetAdminRecertifyAccessAreas(int dbPeopleID)
        {
            try
            {
                return CorporationManager.GetAdminRecertifyAccessAreas(dbPeopleID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Method to return a list of the people in an access area who havent been certified for a specific access area
        /// E.Parker
        /// </summary>
        /// <returns></returns>
        public static List<vw_AccessAreaRecertifications> GetRecertifierPeople(int accessAreaID, int divisionID)
        {
            try
            {
                return CorporationManager.GetRecertifierPeople(accessAreaID, divisionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }
        #endregion


        #region Create


        public int InsertAccessAreaApprover(int dbPeopleID, int roleType, int selectedCorporateDivisionAccessAreaId, int sortOrder)
        {
            int returnValue = 0;
            try
            {
                returnValue = CorporationManager.InsertAccessAreaApprover(selectedCorporateDivisionAccessAreaId, dbPeopleID, roleType, sortOrder);
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// Method to add the external classification approver
        /// E.Parker 27.05.11
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <param name="roleType"></param>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public int InsertExAccessAreaApprover(int dbPeopleID, int roleType, int selectedCorporateDivisionAccessAreaId, int sortOrder, int classificationID)
        {
            int returnValue = 0;
            try
            {
                returnValue = CorporationManager.InsertExAccessAreaApprover(selectedCorporateDivisionAccessAreaId, dbPeopleID, roleType, sortOrder, classificationID);
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }


        public int InsertBusinessAreaApprover(int dbPeopleID, int businessAreaId, int sortOrder)
        {
            int returnValue = 0;
            try
            {
                returnValue = CorporationManager.InsertBusinessAreaApprover(businessAreaId, dbPeopleID, sortOrder);
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// Method to insert an approver for an external classification
        /// E.Parker
        /// 25.05.11
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <param name="businessAreaId"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public int InsertBusinessAreaExApprover(int dbPeopleID, int businessAreaId, int sortOrder, int classificationID)
        {
            int returnValue = 0;
            try
            {
                returnValue = CorporationManager.InsertBusinessAreaExApprover(businessAreaId, dbPeopleID, sortOrder, classificationID);
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        public int CreateBusinessArea(int divisionId, string name, string ubrCode)
        {
            int returnValue = 0;
            try
            {
                returnValue = CorporationManager.InsertBusinessArea(_dbPeopleID, divisionId, name, ubrCode);
                HRPerson hrPersonSubmitter = GetPersonById(_dbPeopleID);
                //DAL.mp_User mpUser = GetUserByID(hrPersonSubmitter.DBDirID.Value); 

                if (returnValue > 0)
                {

                    EmailTemplate email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.BusinessAreaAddition);
                    //// Now email the person to confirm ooo being set.
                    List<DAL.GetAllDAEmails> peopleToEmail = CorporationManager.DAsToEmail(divisionId);

                    foreach (GetAllDAEmails person in peopleToEmail)
                    {
                        int languageID = PersonManager.GetPersonLanguage(null, person.emailAddress); // receipient's language, default to English
                        Guid guid = Guid.NewGuid();

                        EmailManager.GetEmailTemplateLanguageRefined(ref email, languageID);

                        email.Body = email.Body.Replace("[$$NAME$$]", person.forename);
                        email.Body = email.Body.Replace("[$DIVISION$]", person.CountryName + " " + person.DivisionName);
                        email.Body = email.Body + "<br/>" + ConfigurationManager.AppSettings["HTTP_ROOT"] + "/Pages/MyDivisions.aspx";
                        //EmailManager.SendEmail(person.emailAddress, string.Empty, email.Subject, email.Body, guid);
                        EmailManager.InsertBatchEmailLog(person.emailAddress, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
                    }
                }
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaTypeId"></param>
        /// <param name="floorId"></param>
        /// <param name="buildingId"></param>
        /// <param name="name"></param>
        /// <param name="divisionId"></param>
        /// <param name="controlSystemId"></param>
        /// <returns></returns>
        public int CreateAccessArea(int areaTypeId, int? floorId, int buildingId, string name, int? divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int? defaultAccessPeriod, string defaultAccessDuration)
        {
            if (floorId == 0) floorId = null;
            if (divisionId == 0) divisionId = null;


            int returnValue;
            try
            {
                returnValue = CorporationManager.InsertAccessArea(areaTypeId, floorId, buildingId, name, divisionId, controlSystemId, recertPeriod, categoryid, enabled, defaultAccessPeriod, defaultAccessDuration);
                HRPerson hrPersonSubmitter = GetPersonById(_dbPeopleID);



                //email the division admins
                if (returnValue > 0)
                {

                    if (divisionId.HasValue)
                    {
                        //// Now email the person to confirm ooo being set.
                        List<DAL.GetAllDAEmails> peopleToEmail = CorporationManager.DAsToEmail(divisionId.Value);
                        foreach (GetAllDAEmails person in peopleToEmail)
                        {
                            EmailTemplate email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.AccessAreaAddition);

                            int languageID = PersonManager.GetPersonLanguage(null, person.emailAddress); // receipient's language, default to English

                            EmailManager.GetEmailTemplateLanguageRefined(ref email, languageID);
                            Guid guid = new Guid();

                            email.Body = email.Body.Replace("[$$NAME$$]", person.forename);
                            email.Body = email.Body.Replace("[$AccessArea$]", name);
                            email.Body = email.Body.Replace("[$DIVISION$]", person.CountryName + " " + person.DivisionName);
                            email.Body = email.Body + "<br />" + ConfigurationManager.AppSettings["HTTP_ROOT"] + "/Pages/MyAccessAreas.aspx";
                            //EmailManager.SendEmail(person.emailAddress, string.Empty, email.Subject, email.Body, guid);
                            EmailManager.InsertBatchEmailLog(person.emailAddress, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnValue = 0;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        //public int CreateEditDivision(DAL.UBRDivision division, bool isNew, int iRequestMasterID_DA, int iRequestMasterID_DO, bool isSubdivision, int? subdivisionParentDivisionID)
        //{
        //    int returnValue;

        //    try
        //    {
        //        returnValue = Managers.CorporationManager.CreateEditDivision(division, isNew, _dbPeopleID, iRequestMasterID_DA, iRequestMasterID_DO, isSubdivision, subdivisionParentDivisionID);
        //    }
        //    catch (Exception ex)
        //    {
        //        returnValue = 0;
        //        LoggedExceptionId = LogHelper.HandleException(ex);
        //        LoggedException = ex;
        //        FailureReason = DBAccessEnums.FailureReason.Exception;
        //    }
        //    return returnValue;
        //}

        public bool InsertNewPassOfficeUser(int dbPeopleId, int PassOfficeId)
        {
            //check if the person already exists
            bool returnValue;
            returnValue = CorporationManager.InsertNewPassOfficeUser(dbPeopleId, PassOfficeId, _dbPeopleID);
            return returnValue;

        }
        #endregion

        #region Remove

        /// <summary>
        /// 
        /// </summary>
        /// <param name="approverId"></param>
        /// <returns></returns>
        public bool DeleteAccessAreaApprover(int approverId)
        {
            bool success;

            try
            {
                success = CorporationManager.DeleteAccessAreaApprover(approverId, _dbPeopleID);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                success = false;
            }

            return success;
        }



        /// <summary>
        /// AC: removes an business area approver. Called from editbusinessareas.aspx
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public bool DeleteBusinessAreaApprover(int areaId, int personId)
        {
            bool success = false;

            try
            {
                Managers.CorporationManager.DeleteBusinessAreaApprover(personId, areaId);
                success = true;
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }

            return success;
        }



        public bool UpdateApproverEsculation(int nRAprover, int nAApprover)
        {
            bool success = false;

            try
            {
                success = Managers.CorporationManager.UpdateApproverEsculation(nRAprover, nAApprover);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                success = false;
            }

            return success;
        }


        /// <summary>
        /// Removes an business area. Called from myDivisions.aspx
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public bool DeleteMyBusinessArea(int areaId)
        {
            bool success = false;

            try
            {
                int k = Managers.CorporationManager.DeleteMyBusinessArea(areaId);

                if (k > 0)
                    success = true;

            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                success = false;
            }

            return success;
        }
        /// <summary>
        /// This deletes a single user
        /// </summary>
        public bool DeletePassOfficeUserByUserId(int dbPeopleID, int PassOfficeId)
        {
            Boolean bSuccess = false;

            try
            {
                bSuccess = CorporationManager.DeletePassOfficeUserByUserId(_dbPeopleID, dbPeopleID, PassOfficeId);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;

        }
        #endregion

        #region Update

        public void UpdateSortOrder(List<AccessAreaApprover> listAccessAreaApprovers, int iClassificationID, DBAccessEnums.RequestType requestType)
        {
            try
            {
                CorporationManager.UpdateSortOrder(listAccessAreaApprovers, iClassificationID, requestType);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
        }

        public Boolean UpdateAccessAreaApprovers(Int32 iDivisionAccessAreaID, List<AccessAreaApprover> listAccessAreaApprovers)
        {
            Boolean bSuccess = false;
            try
            {
                bSuccess = CorporationManager.UpdateAccessAreaApprovers(_dbPeopleID, iDivisionAccessAreaID, listAccessAreaApprovers);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;
        }

        public Boolean UpdateAccessAreaRecertifiers(Int32 iDivisionAccessAreaID, List<AccessAreaApprover> listAccessRecertifiers)
        {
            Boolean bSuccess = false;
            try
            {
                bSuccess = CorporationManager.UpdateAccessAreaRecertifiers(_dbPeopleID, iDivisionAccessAreaID, listAccessRecertifiers);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;
        }

        /// <summary>
        /// Method to update the Msp's
        /// </summary>
        /// <param name="iBusinessAreaID"></param>
        /// <param name="listRequestApprovers"></param>
        /// <returns></returns>
        public Boolean UpdateMSPs(Int32 vendorID, List<GetMspsByVendorID_Result> listMSPs)
        {
            Boolean bSuccess = false;
            try
            {
                bSuccess = CorporationManager.UpdateMSPs(vendorID, listMSPs);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;
        }

        public bool UpdateDivisionRoleUsers(Int32 iDivisionID, List<DivisionRoleUser> listDivisionRoleUsers, int dbPeopleId)
        {
            bool success = false;
            try
            {
                success = CorporationManager.UpdateDivisionRoleUsers(iDivisionID, listDivisionRoleUsers, dbPeopleId);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return success;
        }

        public Boolean UpdatePassOfficeUsers(List<vw_PassOfficeDetails> listPassOfficeUsers)
        {
            Boolean bSuccess = false;

            try
            {
                bSuccess = CorporationManager.UpdatePassOfficeUsers(_dbPeopleID, listPassOfficeUsers);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;
        }

        /// <summary>
        /// updates the ordering for selected approvers on mayaccessareas.aspx page.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public bool UpdateMyAccessAreaApproverOrdering(string values)
        {
            try
            {
                return CorporationManager.UpdateMyAccessAreaApproverOrdering(values);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return false;
            }
        }

        public bool UpdateRoleCertification(int columnId, int roleId, int action)
        {
            bool success = false;
            try
            {
                success = CorporationManager.UpdateRoleCertification(columnId, roleId, _dbPeopleID, action);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return success;
        }

        public bool UpdateDARoleCertification(int columnId, int action)
        {
            bool success = false;
            try
            {
                success = CorporationManager.UpdateDARoleCertification(columnId, _dbPeopleID, action);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return success;
        }

        public bool UpdateBusinessAreaApproverCertification(int corporateBusinessAreaApproverId, int divisionId, int action)
        {
            bool success = false;
            try
            {
                success = CorporationManager.UpdateBusinessAreaApproverCertification(corporateBusinessAreaApproverId, divisionId, _dbPeopleID, action);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return success;
        }

        /// <summary>
        /// Called from MyAccessArea.aspx. 3rd tab from the sun ;)
        /// </summary>
        /// <param name="areaID"></param>
        /// <param name="enabled"></param>
        /// <param name="approvalType"></param>
        /// <param name="approvalNumber"></param>
        /// <returns></returns>
        public bool UpdateMyAccessArea(int areaID, bool enabled, int approvalType, int approvalNumber, int frequency, String sName)
        {
            try
            {
                bool success = CorporationManager.UpdateMyAccessArea(areaID, enabled, approvalType, approvalNumber, frequency, sName);

                //EmailManager.SendEmail(DBAccessEnums.EmailType.AccessAreaUpdated, areaID, _dbPeopleID, Guid.NewGuid(), null);

                return success;
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return false;
            }
        }

        public int UpdateBusinessArea(int businessAreaId, bool enabled, string baName, int approvalType, int approvalNumber)
        {

            try
            {
                return CorporationManager.UpdateBusinessArea(businessAreaId, enabled, baName, approvalType, approvalNumber);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return 0;
            }
        }

        public bool UpdateBusinessAreaApprovalType(int divisionId, int areaId, int approvalType)
        {
            bool success = false;

            try
            {
                return CorporationManager.UpdateBusinessAreaApprovalType(areaId, divisionId, approvalType);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }

            return success;
        }

        public bool UpdateBusinessAreaStatus(int divisionId, int areaId, bool status)
        {
            bool success = false;

            try
            {
                return CorporationManager.UpdateBusinessAreaStatus(divisionId, areaId, status);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }

            return success;
        }

        public bool RejectAccessAreaRecertify(string userIDs, int AccessAreaID, int DivisionID, int ApproverID)
        {
            try
            {
                return CorporationManager.RejectAccessAreaRecertify(userIDs, AccessAreaID, DivisionID, ApproverID);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return false;
            }
        }

        public bool ApprovedAccessAreaRecertify(string userIDs, int DivisionID, int AccessAreaID, int ApproverID)
        {
            try
            {
                return CorporationManager.ApproveAccessAreaRecertify(userIDs, DivisionID, AccessAreaID, ApproverID);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return false;
            }
        }

        public bool SortBusinessAreaApprover(DBAccessEnums.SortDirection direction, int ApproverID, int classificationID)
        {
            try
            {
                return CorporationManager.SortBusinessAreaApprover(direction, ApproverID, classificationID);
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return false;
            }
        }

        #endregion

        #region Helpers
        public static string GetRecertPeriodText(int? numberOfMonths, int? languageID)
        {
            if (!numberOfMonths.HasValue)
                return String.Empty;

            switch (languageID)
            {
                case (int)DBAccessEnums.Language.English:
                    return GetRecertPeriodEnglish(numberOfMonths);
                case (int)DBAccessEnums.Language.German:
                    return GetRecertPeriodGerman(numberOfMonths);
                case (int)DBAccessEnums.Language.Italian:
                    return GetRecertPeriodItalian(numberOfMonths);
                default:
                    return GetRecertPeriodEnglish(numberOfMonths);
            }
        }

        private static string GetRecertPeriodItalian(int? numberOfMonths)
        {
            switch (numberOfMonths)
            {
                case 3:
                    return "Trimestrale";
                case 1:
                    return "Mensile";
                case 6:
                    return "Semestrale";
                case 12:
                    return "Annuale";
                default:
                    return numberOfMonths.ToString();
            }
        }

        private static string GetRecertPeriodGerman(int? numberOfMonths)
        {
            switch (numberOfMonths)
            {
                case 3:
                    return "Vierteljährlich";
                case 1:
                    return "Monatlich";
                case 6:
                    return "Halbjährlich";
                case 12:
                    return "Jährlich";
                default:
                    return numberOfMonths.ToString();
            }
        }

        private static string GetRecertPeriodEnglish(int? numberOfMonths)
        {
            switch (numberOfMonths)
            {
                case 3:
                    return "Quarterly";
                case 1:
                    return "Monthly";
                case 6:
                    return "Biannually";
                case 12:
                    return "Annually";
                default:
                    return numberOfMonths.ToString();
            }
        }
        #endregion
        #region MSP
        /// <summary>
        /// Method to return a list of people associated to a specific vendor
        /// E.Parker 
        /// 20.06.11
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public static List<DAL.GetMspsByVendorID_Result> GetMspsByVendorID(int vendorId)
        {
            try
            {
                return Managers.PersonManager.GetMspsByVendorID(vendorId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }
        #endregion
    }
}
