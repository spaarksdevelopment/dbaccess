﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Transactions;
using System.Data.SqlClient;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
	public partial class Director
	{
		public static List<BatchEmailPeople> GetOutStandingEmailLog()
		{
			try
			{
				return Managers.EmailManager.GetOutStandingEmailLog();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				throw new ApplicationException("Failed to fetch outstanding emails", ex);
			}
		}

		public bool SendEmail(BatchEmailPeople batchEmail)
		{
			bool success = false;
			BatchEmailPerUser batchEmailDetailsPerUser = CreateBatchEmail(batchEmail);
			if (batchEmailDetailsPerUser == null)
			{
				return success;
			}

				try
				{
					success = Managers.EmailManager.SendEmail(batchEmail, batchEmailDetailsPerUser);
				}
				catch (Exception ex)
				{
					success = false;

					if (ex.InnerException != null && ex.InnerException is SqlException)
					{
						SqlException sqlEx = ex.InnerException as SqlException;

						LoggedExceptionId = LogHelper.HandleException(sqlEx);
						LoggedException = sqlEx;
					}
					else
					{
						LoggedExceptionId = LogHelper.HandleException(ex);
						LoggedException = ex;
					}
				}
			return success;
		}


		public static BatchEmailPerUser CreateBatchEmail(BatchEmailPeople batchEmail)
		{
			try
			{
				return Managers.EmailManager.CreateBatchEmail(batchEmail);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}			 
		}

	}
}
