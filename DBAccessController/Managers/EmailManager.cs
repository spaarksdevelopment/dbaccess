﻿using System;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using Spaarks.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Collections.Generic;
using System.Transactions;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    public static class EmailManager
    {
        /// <summary>
        /// This method needs to go get the content it needs for sending an email.
        /// </summary>
        /// <param name="templateType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static bool SendEmail(DBAccessEnums.EmailType templateType, int entityId, int userId, Guid guid, string Comment)
        {
            bool result;
            
            using (var context = new DBAccessEntities())
            {
                var comment = new ObjectParameter("Comment", typeof(string));
                var content = new ObjectParameter("content", typeof (string));
                var subject = new ObjectParameter("subject", typeof (string));
                var toAddress = new ObjectParameter("toAddress", typeof (string));

                var ccAddress = new ObjectParameter("ccAddress", typeof(string));

                context.GetEmailContent((int)templateType, entityId, userId, ConfigurationManager.AppSettings["HTTP_ROOT"], comment, content, subject, toAddress, ccAddress).ToList();

                result = MailHelper.SafeSendEmail(toAddress.Value.ToString(),
                                               ccAddress.Value.ToString(),
                                               subject.Value.ToString(),
                                               content.Value.ToString(),
                                               true,
                                               ConfigurationManager.AppSettings["SenderEmailAddress"],
                                               ConfigurationManager.AppSettings["SenderName"]);

                try
                {
                    // Now feed the log table with this email.
                    int emailId = InsertEmailLog(toAddress.Value.ToString(), ConfigurationManager.AppSettings["SenderEmailAddress"], ccAddress.Value.ToString(),
								   subject.Value.ToString(), content.Value.ToString(), result, guid, (int)templateType);

                    //as long as the email was logged, it does not matter if it was sent or not, as it will get picked up by the emailer service
                    //return false only if the email could not be logged
                    result = (emailId > 0);
                }
                catch(Exception ex)
                {
                    LogHelper.HandleException(ex);

                    result = false;
                }
            }

            return result;
        }


        /// <summary>
        /// This method is passed in the content it needs to send an email. Typically used in a batch emailing.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static bool SendEmail(string to, string cc, string subject, string content, Guid g)
        {
            // Copy in the http root value from web.config.
            content = content.Replace("[$$HTTP_ROOT$$]", ConfigurationManager.AppSettings["HTTP_ROOT"]);

            bool result = MailHelper.SafeSendEmail(to,
                                                cc,
                                                subject,
                                                content,
                                                true,
                                                ConfigurationManager.AppSettings["SenderEmailAddress"],
                                                ConfigurationManager.AppSettings["SenderName"]);

            try
            {
                // Now feed the log table with this email.
                int emailId = InsertEmailLog(to, ConfigurationManager.AppSettings["SenderEmailAddress"], cc, subject, content, result, g, null);

                //as long as the email was logged, it does not matter if it was sent or not, as it will get picked up by the emailer service
                //return false only if the email could not be logged
                result = (emailId > 0);
            }
            catch(Exception ex)
            {
                LogHelper.HandleException(ex);

                result = false;
            }

            return result;
        }

        private static int InsertEmailLog(string to, string from, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
        {
			return InsertEmailLog(to, from, null, subject, body, isSent, guid, emailTypeId);
        }

        /// <summary>
        /// Inserts a row into the EmailLog table.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isSent"></param>
        /// <returns></returns>
		private static int InsertEmailLog(string to, string from, string cc, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
        {
            using (var context = new DBAccessEntities())
            {
                var result = new ObjectParameter("Result", typeof (int));

				context.InsertEmailLog(to, from, cc, subject, body, isSent, guid, emailTypeId, result);

                return (int)result.Value;
            }
        }

		public static int InsertBatchEmailLog(string to, string from, string cc, string subject, string body, bool isSent, Guid guid, int? emailTypeId)
		{
			from = ConfigurationManager.AppSettings["SenderEmailAddress"];
			return InsertEmailLog(to, from, cc, subject, body, isSent, guid, emailTypeId);
		}

		public static int InsertTemplatedBatchEmailLog(DBAccessEnums.EmailType templateType, int entityId, int userId, Guid guid, string Comment)
		{
			int emailId = 0;
			 using (var context = new DBAccessEntities())
			 {
				 var comment = new ObjectParameter("Comment", typeof(string));
				 var content = new ObjectParameter("content", typeof(string));
				 var subject = new ObjectParameter("subject", typeof(string));
				 var toAddress = new ObjectParameter("toAddress", typeof(string));

				 var ccAddress = new ObjectParameter("ccAddress", typeof(string));

				 context.GetEmailContent((int)templateType, entityId, userId, ConfigurationManager.AppSettings["HTTP_ROOT"], comment, content, subject, toAddress, ccAddress).ToList();

				 try
				 {
					 // Now feed the log table with this email.
					 emailId = InsertBatchEmailLog(toAddress.Value.ToString(), string.Empty, ccAddress.Value.ToString(), subject.Value.ToString(), content.Value.ToString(), false, guid, (int)templateType);
				 }
				 catch (Exception ex)
				 {
					 LogHelper.HandleException(ex);
				 }
			 }
			 return emailId;
		}

        /// <summary>
        /// Gets the contents of an email template for processing up in the BLL.
        /// </summary>
        /// <param name="templateType"></param>
        /// <returns></returns>
        public static EmailTemplate  GetEmailTemplate(DBAccessEnums.EmailType templateType)
        {
            var emailTemplate = new EmailTemplate();

            using (var context = new DBAccessEntities())
            {
                emailTemplate = context.GetEmailTemplate((int)templateType).ToList().First();
            }

            return emailTemplate;
        }

        public static EmailTemplateShort GetEmailTemplate(DBAccessEnums.EmailType templateType, int? LanguageID)
        {
            var emailTemplate = new EmailTemplateShort();

            using (var context = new DBAccessEntities())
            {
                emailTemplate = context.GetEmailTemplateForLanguage((int)templateType, LanguageID).ToList().First();
            }

            return emailTemplate;
        }


		public static void BuildApplicantDetails(List<vw_AccessRequestPerson> emailRecipients, ref string strRequestID, ref string strApplicantBuild, ref string strReceipient)
		{
			foreach (vw_AccessRequestPerson reciep in emailRecipients)
			{
				//get the details of Applicant by passing the request person ID
				List<vw_AccessApproverDetails> applicantDet = Director.GetAccessApproverDetails(Convert.ToInt32(reciep.RequestPersonID));

                //  Need to check to see if the applicantDet contains any items before we reference it (Zendesk ticket 2958)

                if (applicantDet.Count > 0 && reciep.RequestPersonID == applicantDet[0].RequestPersonID)
				{
					string strApplicant = "Applicant: [$$ApplicantName$$] from [$$CountryName$$] of [$$UbrCode$$], [$$UbrName$$]<br />";

					strRequestID = Convert.ToString(reciep.RequestMasterID);
					strApplicant = strApplicant.Replace("[$$ApplicantName$$]", Convert.ToString(applicantDet[0].ApplicantName));
					strApplicant = strApplicant.Replace("[$$CountryName$$]", Convert.ToString(applicantDet[0].CountryName));
					strApplicant = strApplicant.Replace("[$$UbrCode$$]", Convert.ToString(applicantDet[0].UbrCode));
					strApplicant = strApplicant.Replace("[$$UbrName$$]", Convert.ToString(applicantDet[0].UbrName));
					strApplicantBuild += strApplicant;
					strReceipient += reciep.EmailAddress + ",";
				}
			}
		}

		/// <summary>
		/// Builds the AccessArea details for the current request
		/// </summary>
		public static void BuildAccessAreaDetails(List<vw_AccessRequestPerson> emailRecipients, ref string strAccessAreaBuild)
		{
			foreach (vw_AccessRequestPerson recipient in emailRecipients)
			{
				//get the details of Applicant by passing the request person ID
				List<vw_AccessApproverDetails> applicantDetails = Director.GetAccessApproverDetails(Convert.ToInt32(recipient.RequestPersonID));

				if (string.IsNullOrEmpty(recipient.EmailAddress)) continue;

				foreach (vw_AccessApproverDetails applicantDet in applicantDetails)
				{
					string strAccessArea = "Area: [$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])<br />";
					while (recipient.RequestPersonID == applicantDet.RequestPersonID)
					{
						strAccessArea = strAccessArea.Replace("[$$AccessArea$$]", Convert.ToString(applicantDet.Name));
						strAccessArea = strAccessArea.Replace("[$$StartDate$$]", applicantDet.StartDate.Value.ToShortDateString());
						if (applicantDet.EndDate != null)
						{
							strAccessArea = strAccessArea.Replace("[$$EndDate$$]", applicantDet.EndDate.Value.ToShortDateString());
						}
						else
						{
							strAccessArea = strAccessArea.Replace("[$$EndDate$$]", Convert.ToString(applicantDet.EndDate));
						}
						strAccessAreaBuild += strAccessArea;
						break;
					}
				}
				break;
			}
		}

		/// <summary>
		/// Builds the Tasks details for the current request
		/// </summary>
		public static void BuildTaskDetails(List<vw_AccessRequestPerson> emailRecipients, int? requestMasterId, ref string strTaskBuild)
		{
			foreach (vw_AccessRequestPerson reciep in emailRecipients)
			{
               int  languageID = PersonManager.GetPersonLanguage(reciep.dbPeopleID,reciep.EmailAddress); // receipient's language, default to English
				List<int> requestids = Director.GetRequestIds(reciep.RequestPersonID);

				foreach (int reqid in requestids)
				{
					List<vw_AccessRequestApprover> TasksDetails = Director.GetAccessRequestApprovers(Helper.SafeInt(requestMasterId)).Where(a => a.RequestID == reqid).ToList();

					string strTask = string.Empty;

                    ////Tasks Template

                    if (TasksDetails.Count == 0) return;
                  
                     switch (TasksDetails[0].TaskTypeId)
                     {
                         case (int)DBAccess.DBAccessController.DBAccessEnums.TaskType.ApproveAccess:
                             switch(languageID)
                             {
                                 case (int)DBAccessEnums.Language.German:
                                     {
                                         strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Access Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                         break;
                                     }
                                 case (int)DBAccessEnums.Language.Italian:
                                     {
                                         strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Access Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                         break;
                                     }
                                 default:
                                      {
                                         strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Access Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                         break;
                                     }
                                    
                             }
                             break;
                         default:
                             {
                                 switch (languageID)
                                 {
                                     case (int)DBAccessEnums.Language.German:
                                         {
                                             strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Request Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                             break;
                                         }
                                     case (int)DBAccessEnums.Language.Italian:
                                         {
                                             strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Request Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                             break;
                                         }
                                     default://english
                                         {
                                             strTask = "[$$TaskID$$] for ([$$ApplicantName$$]) for Request Approval to ([$$AccessArea$$] (From: [$$StartDate$$], To:[$$EndDate$$])) has been sent to [$$ApproverEmail$$]/[$$ApproverCountry$$]/[$$ApproverUBR$$], [$$ApproverUBRName$$]<br />";
                                             break;
                                         }

                                 }
                                 break;
                             }
                     }
                 
                 

					//the above list gets a unique tak id
					//even though the list returns more than one row taskid is same for all
					strTask = strTask.Replace("[$$TaskID$$]", Convert.ToString(TasksDetails[0].TaskId));

					//get the details of Applicant by passing the request person ID annnd request id
					List<vw_AccessApproverDetails> applicantDet = Director.GetAccessApproverDetails(Convert.ToInt32(reciep.RequestPersonID)).Where(a => a.RequestID == reqid).ToList();

					strTask = strTask.Replace("[$$ApplicantName$$]", Convert.ToString(applicantDet[0].ApplicantName));

					strTask = strTask.Replace("[$$AccessArea$$]", Convert.ToString(applicantDet[0].Name));
					strTask = strTask.Replace("[$$StartDate$$]", applicantDet[0].StartDate.Value.ToShortDateString());

					if (applicantDet[0].EndDate != null)
					{
						strTask = strTask.Replace("[$$EndDate$$]", applicantDet[0].EndDate.Value.ToShortDateString());
					}
					else
					{
						strTask = strTask.Replace("[$$EndDate$$]", Convert.ToString(applicantDet[0].EndDate));
					}

					string strEmail = string.Empty;
					if (TasksDetails.Count > 1)
					{
						for (int i = 0; i <= TasksDetails.Count - 1; i++)
						{
							strEmail += Convert.ToString(TasksDetails[i].EmailAddress) + ',';

						}
					}
					else
					{
						strEmail = Convert.ToString(TasksDetails[0].EmailAddress);
					}

					strTask = strTask.Replace("[$$ApproverEmail$$]", strEmail);
					strTask = strTask.Replace("[$$ApproverCountry$$]", Convert.ToString(TasksDetails[0].ApproverCountryName));
					strTask = strTask.Replace("[$$ApproverUBR$$]", Convert.ToString(TasksDetails[0].UBR_Code));
					strTask = strTask.Replace("[$$ApproverUBRName$$]", Convert.ToString(TasksDetails[0].UBR_Name));
					strTaskBuild += strTask;
				}
			}
		}

		/// <summary>
		/// Sends emails to requester and applicants on the request
		/// </summary>
		/// <param name="strReceipient"></param>
		/// <param name="strRequestID"></param>
		/// <param name="userForename"></param>
		/// <param name="strApplicantBuild"></param>
		/// <param name="strAccessAreaBuild"></param>
		/// <param name="strTaskBuild"></param>
		/// <param name="userEmail"></param>
		public static void SendEmailTo_Requester_Applicants(string strReceipient, string strRequestID, string userForename, string strApplicantBuild, string strAccessAreaBuild, string strTaskBuild, string userEmail)
		{
			//remove the last comma for cc
			strReceipient = strReceipient.TrimEnd(',');
			int nEmailManager;


			EmailTemplateShort email = new EmailTemplateShort();
            int languageID = PersonManager.GetPersonLanguage(null, userEmail); // receipient's language, default to English
			// Now email all relevant persons (ie people who are on the request list)_
            email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.AccessRequest, languageID);

			email.Subject = email.Subject.Replace("[$$RequestId$$]", strRequestID);
			email.Body = email.Body.Replace("[$$NAME$$]", userForename);
			email.Body = email.Body.Replace("[$$RequestId$$]", strRequestID);
			email.Body = email.Body.Replace("[$$Applicant$$]", strApplicantBuild);
			email.Body = email.Body.Replace("[$$Area$$]", strAccessAreaBuild);
			email.Body = email.Body.Replace("[$$Task$$]", strTaskBuild);
			Guid guid = Guid.NewGuid();
			//EmailManager.SendEmail(userEmail, strReceipient, email.Subject, email.Body, guid);

			nEmailManager = EmailManager.InsertBatchEmailLog(userEmail, string.Empty, strReceipient, email.Subject, email.Body, false, guid, email.Type);
		}

		/// <summary>
		/// Sends Emails to current Approvers
		/// </summary>
		/// <param name="emailRequestApprovers"></param>
		/// <param name="userForename"></param>
        public static void SendEmailToApprovers(List<vw_AccessRequestApprover> emailRequestApprovers, string userForename)
		{
			foreach (vw_AccessRequestApprover approver in emailRequestApprovers)
			{
				// Now email all relevant approvers (ie people who are set as approvers in task users)
                EmailTemplate email = new EmailTemplate();
				switch (approver.TaskTypeId)
				{
					case (int)DBAccess.DBAccessController.DBAccessEnums.TaskType.ApproveAccess:
                        email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.AccessApproval);
						break;
					default:
                        email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.RequestApproval);
						break;
				}


				if (string.IsNullOrEmpty(approver.EmailAddress)) continue;

				//get the details of Applicant by passing the request person ID
				List<vw_AccessApproverDetails> applicantDetails = Director.GetAccessApproverDetails(Convert.ToInt32(approver.RequestPersonID)).Where(a => a.RequestID == approver.RequestID).ToList();

				if (applicantDetails.Count == 1)
				{
                    string strRequesterEmail = TaskManager.GetRequesterEmailAddress(Convert.ToInt32(approver.RequestPersonID));

                    if (approver.dbPeopleID != null)
                    {
                        int languageID = PersonManager.GetPersonLanguage(approver.dbPeopleID, null);
                        GetEmailTemplateLanguageRefined(ref email, languageID);
                    }

					email.Subject = email.Subject.Replace("[$$RequestId$$]", Convert.ToString(approver.RequestMasterID));
					email.Body = email.Body.Replace("[$$RequestId$$]", Convert.ToString(approver.RequestMasterID));
					email.Body = email.Body.Replace("[$$NAME$$]", userForename);
					email.Body = email.Body.Replace("[$$TaskID$$]", Convert.ToString(approver.TaskId));

					email.Body = email.Body.Replace("[$$ApplicantName$$]", applicantDetails[0].ApplicantName);
					email.Body = email.Body.Replace("[$$CountryName$$]", applicantDetails[0].CountryName);
					email.Body = email.Body.Replace("[$$UbrCode$$]", applicantDetails[0].UbrCode);
					email.Body = email.Body.Replace("[$$UbrName$$]", applicantDetails[0].UbrName);

					email.Body = email.Body.Replace("[$$AccessArea$$]", applicantDetails[0].Name);
					email.Body = email.Body.Replace("[$$StartDate$$]", applicantDetails[0].StartDate.Value.ToShortDateString());

					if (applicantDetails[0].EndDate != null)
					{
						email.Body = email.Body.Replace("[$$EndDate$$]", applicantDetails[0].EndDate.Value.ToShortDateString());
					}
					else
					{
						email.Body = email.Body.Replace("[$$EndDate$$]", Convert.ToString(applicantDetails[0].EndDate));
					}

					//string stRequesterEmail = TaskManager.GetRequesterEmailAddress(Convert.ToInt32(approver.RequestPersonID));
					//check if theapprover has email address and not no.email@db.com address
					Guid guid = Guid.NewGuid();
					int nEmailManager;
					if (approver.EmailAddress == null || approver.EmailAddress == "no.email@db.com")
					{						
						//send the email to the requestor
						//EmailManager.SendEmail(strRequesterEmail, string.Empty, email.Subject + ' ' + "Invalid Email for" + ' ' + approver.Forename + ' ' + approver.Surname, email.Body, guid);

						nEmailManager = EmailManager.InsertBatchEmailLog(strRequesterEmail, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
					}
					else
					{
						//string ccList = GetApproversEmailList(emailRequestApprovers);
						//ccList.TrimEnd(',');
						string ccList = string.Empty;

						//EmailManager.SendEmail(approver.EmailAddress, ccList, email.Subject, email.Body, guid);
						nEmailManager = EmailManager.InsertBatchEmailLog(approver.EmailAddress, string.Empty, ccList, email.Subject, email.Body, false, guid, email.Type);
					}
				}
			}
		}

		/// <summary>
		/// Registers the data for email that are 
		/// sent when we raise new pass ofiice request
		/// </summary>
		/// <param name="RequestMasterId"></param>
		/// <param name="userForename"></param>
		/// <param name="userEmail"></param>
		/// <param name="req"></param>
		public static void SendEmail_PassOffice(int RequestMasterId, string userForename, string userEmail, HRPerson req)
		{
            int languageID;
           // DAL.mp_User mpUser = Director.GetUserByID(req.dbPeopleID);
            languageID = PersonManager.GetPersonLanguage(null, userEmail); // receipient's language, default to English
            EmailTemplateShort email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.CreatePassRequest, languageID);
		    Guid guid = Guid.NewGuid();

			email.Body = email.Body.Replace("[$$NAME$$]", userForename);
			//EmailManager.SendEmail(userEmail, string.Empty, email.Subject, email.Body, guid);
			EmailManager.InsertBatchEmailLog(userEmail, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);

			List<vw_AccessRequestPerson> emailRecipients = Director.GetAccessRequestPersons(Helper.SafeInt(RequestMasterId));
			foreach (vw_AccessRequestPerson recipient in emailRecipients)
			{
				guid = new Guid();

				// Now email all relevant persons (ie people who are on the request list)
                 languageID = PersonManager.GetPersonLanguage(recipient.dbPeopleID,recipient.EmailAddress); // receipient's language, default to English
				email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.PassToRequestee,languageID);

				if (string.IsNullOrEmpty(recipient.EmailAddress)) continue;

				email.Body = email.Body.Replace("[$$NAME$$]", recipient.Forename);
				//EmailManager.SendEmail(recipient.EmailAddress, string.Empty, email.Subject, email.Body, guid);
				EmailManager.InsertBatchEmailLog(recipient.EmailAddress, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailRequestApprovers"></param>
		/// <param name="requestMasterId"></param>
		/// <param name="requestID"></param>
		/// <param name="_dbPeopleID"></param>
		/// <param name="taskId"></param>
		public static void SendEmail_OnUpdateTaskStatus(List<vw_AccessRequestApprover> emailRequestApprovers, int requestMasterId, int requestID, int _dbPeopleID, int taskId)
		{
         
           // DAL.mp_User mpUser = Director.GetUserByID(_dbPeopleID); //in mp_user ID and dbPeopleID are now the same

			foreach (vw_AccessRequestApprover approver in emailRequestApprovers)
			{
			    Guid guid = Guid.NewGuid();

				if (approver.RequestStatusID == 2 || approver.RequestStatusID == 3 || approver.TaskTypeId == 2 || approver.TaskTypeId == 3)
				{
                    string strRequesterEmail = TaskManager.GetRequesterEmailAddress(Convert.ToInt32(approver.RequestPersonID));
					// Now email all relevant approvers (ie people who are set as approvers in task users)
                    EmailTemplate email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.AccessApproval);

                    if (approver.EmailAddress == null || approver.EmailAddress == "no.email@db.com")
                    {
                        int languageID = PersonManager.GetPersonLanguage(null, strRequesterEmail); // receipient's language, default to English
                        GetEmailTemplateLanguageRefined(ref email, languageID);
                    }
                    else
                    {
                        int languageID = PersonManager.GetPersonLanguage(approver.dbPeopleID,approver.EmailAddress); // receipient's language, default to English
                        GetEmailTemplateLanguageRefined(ref email, languageID);
                    }

					if (string.IsNullOrEmpty(approver.EmailAddress)) continue;

					//get the details of Applicant by passing the request person ID
					List<vw_AccessApproverDetails> applicantDetails = Director.GetAccessApproverDetails(Convert.ToInt32(approver.RequestPersonID)).Where(a => a.RequestID == approver.RequestID).ToList();

					if (applicantDetails == null || applicantDetails.Count == 0)
						throw new ApplicationException(string.Format("About to send an email after updating task id {0}/ request id {1}, but there are no details to populate the email.", taskId, requestID));

					if (applicantDetails.Count == 1)
					{
						DAL.HRPerson person = PersonManager.GetPersonById(_dbPeopleID);

						email.Subject = email.Subject.Replace("[$$RequestId$$]", Convert.ToString(approver.RequestMasterID));
						email.Body = email.Body.Replace("[$$RequestId$$]", Convert.ToString(approver.RequestMasterID));

                        SetSubmittedByToTheOriginalApplicant(email, applicantDetails);

						email.Body = email.Body.Replace("[$$TaskID$$]", Convert.ToString(approver.TaskId));

						email.Body = email.Body.Replace("[$$RequesterName$$]", approver.RequesterName);
						email.Body = email.Body.Replace("[$$ApplicantName$$]", applicantDetails[0].ApplicantName);
						email.Body = email.Body.Replace("[$$CountryName$$]", applicantDetails[0].CountryName);
						email.Body = email.Body.Replace("[$$UbrCode$$]", applicantDetails[0].UbrCode);
						email.Body = email.Body.Replace("[$$UbrName$$]", applicantDetails[0].UbrName);

						email.Body = email.Body.Replace("[$$AccessArea$$]", applicantDetails[0].Name);

						email.Body = email.Body.Replace("[$$StartDate$$]", applicantDetails[0].StartDate.Value.ToShortDateString());
						email.Body = email.Body.Replace("[$$StartDate$$]", Convert.ToString(applicantDetails[0].StartDate));

						if (applicantDetails[0].EndDate != null)
						{
							email.Body = email.Body.Replace("[$$EndDate$$]", applicantDetails[0].EndDate.Value.ToShortDateString());
						}
						else
						{
							email.Body = email.Body.Replace("[$$EndDate$$]", Convert.ToString(applicantDetails[0].EndDate));
						}
					}

					email.Body = email.Body.Replace("[$$NAME$$]", approver.Forename);

					//check if the approval type is mutliple approval
					//if so then check if any one of the tasks is processed if so then by pass the emails
					if (!Director.CheckMultiApproval(approver.RequestID, approver.TaskId))
					{
						//check if theapprover has email address and not no.email@db.com address
						if (approver.EmailAddress == null || approver.EmailAddress == "no.email@db.com")
						{
							//Get the requester EmailAddress  from request RequestPersonid
							//string strRequesterEmail = TaskManager.GetRequesterEmailAddress(Convert.ToInt32(approver.RequestPersonID));

							//send the email to the requestor
							//EmailManager.SendEmail(strRequesterEmail, string.Empty, email.Subject + ' ' + "Invalid Email for" + ' ' + approver.Forename + ' ' + approver.Surname, email.Body, guid);
							EmailManager.InsertBatchEmailLog(strRequesterEmail, string.Empty, string.Empty, email.Subject + ' ' + "Invalid Email for" + ' ' + approver.Forename + ' ' + approver.Surname, email.Body, false, guid, email.Type);
						}
						else
						{
							//EmailManager.SendEmail(approver.EmailAddress, string.Empty, email.Subject, email.Body, guid);
							EmailManager.InsertBatchEmailLog(approver.EmailAddress, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
						}
					}
				}
				else
					if (approver.RequestStatusID == 4 && approver.RequestTypeID == 2 && approver.TaskTypeId == 7)
					{
						int requestId = approver.RequestID;
						//send emails to requestor and the person in the list saying that their request has been approved
						List<vw_AccessApproverDetails> emailRequesterRequestee = Director.GetAccessApproverDetails(Convert.ToInt32(approver.RequestPersonID)).Where(a => a.RequestID == approver.RequestID).ToList();

						//Get the requester EmailAddress  from request RequestPersonid
						string strRequesterEmail = TaskManager.GetRequesterEmailAddress(Convert.ToInt32(approver.RequestPersonID));

						//send emails to requester approval
						if (emailRequesterRequestee.Count == 1)
						{
                            EmailTemplate email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.ApproveAccessResponse);
							break;
						}
					}
			}
		}

        public static void SetSubmittedByToTheOriginalApplicant(EmailTemplate email, List<vw_AccessApproverDetails> applicantDetails)
        {
            string requesterName = applicantDetails.First().RequesterName;
            if (applicantDetails.First().RequesterName != string.Empty)
            {
                email.Body = email.Body.Replace("[$$NAME$$]", requesterName);
            }
            else
            {
                email.Body = email.Body.Replace("[$$NAME$$]", string.Empty);
            }
        }

		public static List<BatchEmailPeople> GetOutStandingEmailLog()
		{
			using (var context = new DAL.DBAccessEntities())
			{
				
				List<BatchEmailPeople> emailLog = context.GetBatchEmailPeople().ToList();
				return emailLog;
			}
		}

		public static bool SendEmail(BatchEmailPeople batchEmail, BatchEmailPerUser batchEmailDetailsPerUser)
		{
			bool result = false;
			int numberOfEntities = 0;
			
			using (var context = new DAL.DBAccessEntities())
			{
				if (batchEmailDetailsPerUser != null)
				{
					int counter = 0;
					for (int i = 1; i <= Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfAttempts"]); i++)
					{
						result = MailHelper.SafeSendEmail(batchEmailDetailsPerUser.To.ToString(),
															(batchEmailDetailsPerUser.CC == null) ? string.Empty : batchEmailDetailsPerUser.CC.ToString(),
															batchEmailDetailsPerUser.Subject.ToString(),
															batchEmailDetailsPerUser.Body.ToString(),
															true,
															ConfigurationManager.AppSettings["SenderEmailAddress"],
															ConfigurationManager.AppSettings["SenderName"]);
						counter = i;
						if (result)
						{
							break;
						}
					}

					//update All the emaillog entries for the current person
					List<EmailLog> tblEmaillog = context.EmailLogs.Where(a => (a.To == batchEmail.To) && (a.DateSent == null) && (a.InProgress == true) && (a.EmailType != null)).ToList();
					foreach (EmailLog logentry in tblEmaillog)
					{
						if (result)
						{
							logentry.DateSent = DateTime.Now;
						}
						else
						{
							logentry.InProgress = false;
						}
						logentry.Resends = counter;
					}
					numberOfEntities = context.SaveChanges();
				}
			}
			return (result);
		}

		public static BatchEmailPerUser CreateBatchEmail(BatchEmailPeople batchEmail)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				context.CommandTimeout = 600;
				return context.CreateBatchEmailPerUser(batchEmail.To.ToString()).Single();
			}
		}


        public static void GetEmailTemplateLanguageRefined(ref EmailTemplate email, int languageID)
        {
            switch (languageID)
            {
                case (int)DBAccessEnums.Language.German:
                    {
                        email.Body = email.GermanBody;
                        email.Subject = email.GermanSubject;
                        break;
                    }
                case (int)DBAccessEnums.Language.Italian:
                    {
                        email.Body = email.ItalianBody;
                        email.Subject = email.ItalianSubject;
                        break;
                    }
            }
        }        

	}
}
