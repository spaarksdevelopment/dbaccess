﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    static class OutOfOfficeManager
    {

        /// <summary>
        /// Method to delete out of office
        /// E.Parker 11.03.11
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Result"></param>
        /// <returns></returns>
        public static bool DeleteOutOfOffice(int oooPersonID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                context.DeleteOutOfOffice(oooPersonID, Result);
                return Convert.ToBoolean(Result.Value);
            }

        }

        /// <summary>
        /// Method to get out of office for the current user
        /// E.Parker 11.03.11
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static List<DAL.OutOfOffice> GetOutOfOffice(int dbPeopleId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from pb in context.OutOfOffices
                        where pb.dbPeopleID == dbPeopleId
                        select pb;


                List<DAL.OutOfOffice> outOfOffice = q.ToList();
                return outOfOffice;
            }
        }


        /// <summary>
        /// Method to create a new out of office
        /// E.Parker 11.03.11
        /// </summary>
        /// <param name="LeaveDate"></param>
        /// <param name="ReturnDate"></param>
        /// <param name="SubstituteUserID"></param>
        /// <returns></returns>
        public static bool CreateOutOfOffice(DateTime LeaveDate, DateTime ReturnDate, int SubstituteUserID, int UserID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                if (SubstituteUserID == 0)
                {
                    context.AddOutOfOffice(UserID, LeaveDate, ReturnDate, null, Result);
                    return Convert.ToBoolean(Result.Value);
                }
                else
                {
                    context.AddOutOfOffice(UserID, LeaveDate, ReturnDate, SubstituteUserID, Result);
                    return Convert.ToBoolean(Result.Value);
                }
            }
        }
    }
}
