﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Objects;
using System.Configuration;
using System.Data.Objects.SqlClient;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;


namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    /// <summary>
    /// handles all activity relating to persons
    /// i.e.
    /// HR data
    /// Visitor Data
    /// employment types
    /// 
    /// </summary> 
    static class PersonManager
    {
        #region GET
        public static int GetPersonLanguage(int? dbpeopleID, string emailAddress)
        {
            ObjectParameter languageID = new ObjectParameter("languageID", typeof(int));

            using (var context = new DAL.DBAccessEntities())
            {
                context.GetPersonLanguage(dbpeopleID, emailAddress, languageID);

            }
            return Convert.ToInt32(languageID.Value);

        }
        public static DAL.mp_User GetUserByID(int userID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return (DAL.mp_User)context.mp_User.Where(a => a.Id == userID).SingleOrDefault();
            }
        }

        public static DAL.mp_User GetUserBydbPeopleId(int dbPeopleId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return (DAL.mp_User)context.mp_User.Where(a => a.dbPeopleID == dbPeopleId).FirstOrDefault();
            }
        }

        /// <summary>
        /// Method to return a person based on dbPeopleID
        /// E.Parker
        /// July 2011
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <returns></returns>
        public static DAL.HRPerson GetUserBydbPeopleID(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return (DAL.HRPerson)context.HRPersons.Where(a => a.dbPeopleID == dbPeopleID).SingleOrDefault();
            }
        }

        /// <summary>
        /// Method to return all detaisl for the extend MSP pass page
        /// E.Parker 22.04.11
        /// </summary>
        /// <param name="mspId"></param>
        /// <param name="dbPeopleID"></param>
        /// <returns></returns>
        public static DAL.vw_MSPPersonDetails GetMSPPersonDetails(int mspId, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.vw_MSPPersonDetails
                        //where i.VendorId == mspId
                        where i.dbPeopleID == dbPeopleID
                        select i;
                List<DAL.vw_MSPPersonDetails> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;
            }

        }

        /// <summary>
        /// Method to get the vendorID
        /// E.Parker
        /// 22.04.11
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static int GetVendorID(int userID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = context.mp_User.Where(a => a.dbPeopleID == userID).Select(b => b.VendorID);



                int vendorID = Convert.ToInt32(q.SingleOrDefault());
                return vendorID;
            }
        }

        /// <summary>
        /// Method to return a vendor based on Id
        /// E.Parker 23.06.11
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public static DAL.HRVendor GetVendorById(int vendorId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from b in context.HRVendors
                        join c in context.HRVendorCountries on b.VendorId equals c.VendorID
                        where c.VendorCountryID == vendorId
                        select b;

                return q.ToList().FirstOrDefault();
            }
        }

        /// <summary>
        /// Method to return list of users assinged to a specific msp
        /// E.Parker 20.04.11
        /// </summary>
        /// <param name="mspId"></param>
        /// <returns></returns>
        public static List<DAL.UserForMspManagement> GetUsersByMspId(int mspId)
        {
            using (var context = new DAL.DBAccessEntities())
            {

                var q = from i in context.GetUsersByMspId(mspId)
                        orderby i.Surname, i.Forename
                        select i;


                List<DAL.UserForMspManagement> users = q.ToList();
                return users;
            }
        }

        public static List<DAL.HRPerson> GetPerson()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        select hr;
                List<DAL.HRPerson> hrPerson = q.ToList();
                return hrPerson;

            }
        }

        public static List<DAL.HRPerson> GetPerson(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        where hr.dbPeopleID == dbPeopleID
                        select hr;
                List<DAL.HRPerson> hrPerson = q.ToList();
                return hrPerson;

            }
        }

        /// <summary>
        /// ac: returns a single person by dbdirID
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <returns></returns>
        public static DAL.HRPerson GetPersonById(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        where hr.dbPeopleID == dbPeopleID
                        select hr;

                List<DAL.HRPerson> hrPerson = q.ToList();
                return hrPerson.First();
            }
        }

        public static List<DAL.HRPerson> GetPersonByUserId(int dbDirID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        where hr.DBDirID == dbDirID
                        select hr;
                List<DAL.HRPerson> hrPersons = q.ToList();
                return hrPersons;

            }
        }

        public static int? GetdbPeopleIDByUserId(int dbDirID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        where hr.DBDirID == dbDirID
                        select hr;
                List<DAL.HRPerson> hrPersons = q.ToList();
                if (hrPersons.Count > 0)
                    return hrPersons[0].dbPeopleID;
                else
                    return null;
            }
        }


        public static List<DAL.HRPerson> GetPerson(string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString))
                return new List<DAL.HRPerson>();

            using (var context = new DAL.DBAccessEntities())
            {
                var q = from hr in context.HRPersons
                        where hr.EmailAddress.Equals(searchString)
                        select hr;

                if (searchString.Contains(' '))
                {
                    string firstname, lastname;

                    GetFirstAndLast(searchString, out firstname, out lastname);

                    q = from hr in context.HRPersons
                        where hr.Forename.Equals(firstname) && hr.Surname.Equals(lastname) && (hr.Status.Equals("A") || hr.Status.Equals("P") || hr.Status.Equals("L"))
                        select hr;
                }

                if (q.Count() == 0)
                {
                    //*****************************************************************************************************************************************************//
                    string firstname, lastname;

                    GetFirstMultipleLast(searchString, out firstname, out lastname, context);

                    q = from hr in context.HRPersons
                        where hr.Forename.Equals(firstname) && hr.Surname.Equals(lastname) && (hr.Status.Equals("A") || hr.Status.Equals("P") || hr.Status.Equals("L"))
                        select hr;
                    //*****************************************************************************************************************************************************//
                }

                if (q.Count() == 0)
                {
                    string firstname, lastname;

                    GetMultipleFirstSingleLast(searchString, out firstname, out lastname, context);

                    q = from hr in context.HRPersons
                        where hr.Forename.Equals(firstname) && hr.Surname.Equals(lastname) && (hr.Status.Equals("A") || hr.Status.Equals("P") || hr.Status.Equals("L"))
                        select hr;
                }

                //Order by email address. If there are multiple entries and we are searching by name,
                //we want the entry with no email to come first (hence why we are searching by name rather than email
                List<DAL.HRPerson> hrPerson = q.OrderBy(a => a.EmailAddress).ToList();
                return hrPerson;
            }
        }

        /// <summary>
        /// ac: populates the approver selection grid for DAs
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public static string[] GetPersonArrayWithValidIds(string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString))
                return new string[0];

            searchString = searchString.Trim();

            using (var context = new DAL.DBAccessEntities())
            {
                IQueryable<DAL.vw_HRPerson> q;
                FilterPersonArray(searchString, false, context, out q);

                //List<string> hrPerson = q.Take(10).ToList();
                List<string> hrPerson = q.Select(hr => hr.Identifier).Take(10).ToList();
                return hrPerson.ToArray();
            }
        }

        public static string[] GetPersonArray(string searchString, Boolean bShowTerminated)
        {
            if (string.IsNullOrWhiteSpace(searchString))
                return new string[0];

            searchString = searchString.Trim();

            using (var context = new DAL.DBAccessEntities())
            {
                IQueryable<DAL.vw_HRPerson> q;
                FilterPersonArray(searchString, bShowTerminated, context, out q);

                List<string> hrPerson = q.Select(hr => hr.Identifier).Take(10).ToList();

                return hrPerson.ToArray();
            }
        }

        private static void FilterPersonArray(string searchString, Boolean bShowTerminated, DAL.DBAccessEntities context, out IQueryable<DAL.vw_HRPerson> q)
        {
            q = context.vw_HRPerson.Where(hr => (bShowTerminated || hr.Status.Equals("A") || hr.Status.Equals("P") || hr.Status.Equals("L")) && (!hr.Revoked.HasValue || !(bool)hr.Revoked));

            //List<vw_HRPerson> vr = q.Where(a => a.EmailAddress == "tracy.ward@DB.com").ToList();
            //if the search expression contains a space, we can assume its forename and surname
            if (searchString.Contains(' '))
            {
                string firstname, lastname;

                GetFirstAndLast(searchString, out firstname, out lastname);

                if (string.IsNullOrEmpty(lastname))
                    q = q.Where(hr => hr.Forename.Equals(firstname) || hr.PreferredFirstName.Equals(firstname));
                else
                    q = q.Where(hr => ((hr.Forename.Contains(firstname) || (!string.IsNullOrEmpty(hr.PreferredFirstName) && hr.PreferredFirstName.Contains(firstname))) && (hr.Surname.Contains(lastname) || (!string.IsNullOrEmpty(hr.PreferredLastName) && hr.PreferredLastName.Contains(lastname)))));

                if (!q.Any())
                {
                    //*****************************************************************************************************************************************************//

                    GetFirstMultipleLast(searchString, out firstname, out lastname, context);

                    q = q.Where(hr => ((hr.Forename.Contains(firstname) || (!string.IsNullOrEmpty(hr.PreferredFirstName) && hr.PreferredFirstName.Contains(firstname))) && (hr.Surname.Contains(lastname) || (!string.IsNullOrEmpty(hr.PreferredLastName) && hr.PreferredLastName.Contains(lastname)))));

                    //*****************************************************************************************************************************************************//
                }


            }
            else
            {
                //default search string checks email, dbpeopleid and dbdirid, with a partial search
                q = q.Where(hr => hr.EmailAddress.Equals(searchString) || SqlFunctions.StringConvert((Double)hr.dbPeopleID).Trim().Equals(searchString) || hr.DBDirString.Equals(searchString));

               if (!q.Any())
                    q = context.vw_HRPerson.Where(hr => (bShowTerminated || hr.Status.Equals("A") || hr.Status.Equals("P") || hr.Status.Equals("L")) && (!hr.Revoked.HasValue || !(bool)hr.Revoked)).Where(a => (a.Forename.ToLower().Contains(searchString.ToLower()) || a.PreferredFirstName.ToLower().Contains(searchString.ToLower())));

            }
        }


        private static void GetFirstAndLast(string searchString, out string firstname, out string lastname)
        {
            searchString = searchString.TrimEnd();
            string[] wholename = searchString.Split(' ');

            firstname = wholename[0];
            lastname = string.Empty;

            if (wholename.Length == 2)
            {
                lastname = wholename[1];
            }

            if (wholename.Length == 3)
            {
                lastname = wholename[2];
            }

            if (wholename.Length == 4)
            {
                lastname = wholename[3];
            }
        }

        private static void GetMultipleFirstSingleLast(string searchString, out string firstname, out string lastname, DBAccessEntities context)
        {
            searchString = searchString.TrimEnd();
            string[] wholename = searchString.Split(' ');

            string copyoffirstname = string.Empty;
            string copyoflastname = wholename[0];

            firstname = copyoffirstname;
            lastname = copyoflastname;

            switch (wholename.Length)
            {
                case 3:
                    {
                        copyoflastname = wholename[2];
                        copyoffirstname = wholename[0] + " " + wholename[1];
                        break;
                    }
                case 4:
                    {
                        copyoflastname = wholename[3];
                        copyoffirstname = wholename[0] + " " + wholename[1] + " " + wholename[2];
                        break;
                    }
                case 5:
                    {
                        copyoflastname = wholename[4];
                        copyoffirstname = wholename[0] + " " + wholename[1] + " " + wholename[2] + " " + wholename[3];
                        break;
                    }
            }
            firstname = copyoffirstname;
            lastname = copyoflastname;
        }

        private static void GetFirstMultipleLast(string searchString, out string firstname, out string lastname, DBAccessEntities context)
        {
            searchString = searchString.TrimEnd();
            string[] wholename = searchString.Split(' ');

            string firstnamecopy = wholename[0];
            string lastnamecopy = string.Empty;

            firstname = firstnamecopy;
            lastname = lastnamecopy;

            switch (wholename.Length)
            {

                case 3:
                    {
                        lastnamecopy = wholename[1] + " " + wholename[2];
                        firstnamecopy = wholename[0];

                        break;
                    }
                case 4:
                    {
                        lastnamecopy = wholename[1] + " " + wholename[2] + " " + wholename[3];
                        firstnamecopy = wholename[0];

                        if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                        {
                            break;
                        }

                        else
                        {
                            lastnamecopy = wholename[2] + " " + wholename[3];
                            firstnamecopy = wholename[0] + " " + wholename[1];
                        }


                        break;
                    }
                case 5:
                    {
                        lastnamecopy = wholename[1] + " " + wholename[2] + " " + wholename[3] + " " + wholename[4];
                        firstnamecopy = wholename[0];

                        if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                        {
                            break;
                        }

                        else
                        {
                            lastnamecopy = wholename[2] + " " + wholename[3] + " " + wholename[4];
                            firstnamecopy = wholename[0] + " " + wholename[1];

                            if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                            {
                                break;
                            }
                            else
                            {
                                lastnamecopy = wholename[3] + " " + wholename[4];
                                firstnamecopy = wholename[0] + " " + wholename[1] + " " + wholename[2];
                            }

                        }

                        break;
                    }
                case 6:
                    {
                        lastnamecopy = wholename[1] + " " + wholename[2] + " " + wholename[3] + " " + wholename[4] + " " + wholename[5];
                        firstnamecopy = wholename[0];

                        if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                        {
                            break;
                        }

                        else
                        {
                            lastnamecopy = wholename[2] + " " + wholename[3] + " " + wholename[4] + " " + wholename[5];
                            firstnamecopy = wholename[0] + " " + wholename[1];

                            if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                            {
                                break;
                            }
                            else
                            {
                                lastnamecopy = wholename[3] + " " + wholename[4] + " " + wholename[5];
                                firstnamecopy = wholename[0] + " " + wholename[1] + " " + wholename[2];

                                if (CheckNameExists(firstnamecopy, lastnamecopy, context))
                                {
                                    break;
                                }
                                else
                                {
                                    lastnamecopy = wholename[4] + " " + wholename[5];
                                    firstnamecopy = wholename[0] + " " + wholename[1] + " " + wholename[2] + " " + wholename[3];

                                    break;
                                }


                            }


                        }

                    }
            } 
            
            firstname = firstnamecopy;
            lastname = lastnamecopy;
        }

        private static bool CheckNameExists(string firstname, string lastname, DBAccessEntities context)
        {
            return (context.HRPersons.Where(hr => ((hr.Forename.Contains(firstname) || (!string.IsNullOrEmpty(hr.PreferredFirstName) && hr.PreferredFirstName.Contains(firstname))) && (hr.Surname.Contains(lastname) || (!string.IsNullOrEmpty(hr.PreferredLastName) && hr.PreferredLastName.Contains(lastname))))).Count() > 0);
           
        }
        //*****************************************************************************************************************************************************//




        public static List<DAL.vw_HRPerson> GetPersonArray(string searchString, Boolean bShowTerminated, string costCentre = null)
        {
            if (string.IsNullOrWhiteSpace(searchString))
                searchString = string.Empty;

            searchString = searchString.Trim();

            using (var context = new DAL.DBAccessEntities())
            {
                IQueryable<DAL.vw_HRPerson> q;
                FilterPersonArray(searchString, bShowTerminated, context, out q);

                return q.Take(10).ToList();
            }
        }

        public static List<DAL.vw_HRPersonBadge> GetPersonBadges(int dbPplID, bool? valid)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from pb in context.vw_HRPersonBadge
                        where pb.dbPeopleID == dbPplID
                        select pb;

                if (valid.HasValue)
                {
                    q = from pb in context.vw_HRPersonBadge
                        where pb.dbPeopleID == dbPplID
                        where pb.Valid == valid.Value
                        select pb;
                }

                List<DAL.vw_HRPersonBadge> listBadges = q.ToList();
                return listBadges;
            }
        }

        public static List<DAL.vw_MyDBAccess> GetMyDBAccess(int dbPeopleID, int filter)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int recentRequestDays = 14;

                try
                {
                    recentRequestDays = Convert.ToInt32(ConfigurationManager.AppSettings["RecentRequestsDays"]);
                }
                catch
                { ;}

                DateTime oldestDate = DateTime.Today.Subtract(new TimeSpan(recentRequestDays, 0, 0, 0));

                var q = context.vw_MyDBAccess.Where(a => filter == 1 || a.Created == null ||
                  a.Created.Value >= oldestDate);


                q = q.Where(a => a.dbPeopleID == dbPeopleID && a.RequestStatusID != 10);

                List<DAL.vw_MyDBAccess> myDBRequests = q.ToList();
                return myDBRequests;

            }
        }

        public static List<DAL.LocationPassOffice> GetUserPassOffice(int dbPeopleId)
        {
            List<DAL.LocationPassOffice> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.LocationPassOffice>)context.GetUserPassOffices(dbPeopleId).ToList();
            }

            return data;
        }

        public static List<DAL.LocationPassOffice> GetPersonPassOffice(int dbPeopleId)
        {
            List<DAL.LocationPassOffice> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.LocationPassOffice>)context.GetPersonPassOffices(dbPeopleId).ToList();
            }

            return data;
        }

        public static List<DAL.vw_MyRequestsDetail> GetMyRequestDetails(int masterRequestID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.vw_MyRequestsDetail.Where(a => a.requestMasterID == masterRequestID).ToList();
            }
        }

        public static List<DAL.vw_MyRequestsDetailReport> GetMyRequestDetailsReport(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                context.CommandTimeout = 300;

                return context.vw_MyRequestsDetailReport.Where(a => a.CreatedByID == dbPeopleID).OrderByDescending(a => a.Created).ToList();
            }
        }

        public static List<DAL.vw_MyBadges> GetMyBadges(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from pb in context.vw_MyBadges
                        where pb.dbPeopleID == dbPeopleID
                        select pb;
                List<DAL.vw_MyBadges> myBadges = q.ToList();
                return myBadges;
            }
        }

        public static List<DAL.AccessRequestVisitorOptionsLookup> GetAccessRequestVisitorOptionsLookUp()
        {
            using (var context = new DAL.DBAccessEntities())
            {

                var p = from vo in context.AccessRequestVisitorOptionsLookups
                        select vo;

                List<DAL.AccessRequestVisitorOptionsLookup> visitorOptions = p.ToList();
                return visitorOptions;
            }
        }


        public static List<DAL.GetAccessRequestMasters> GetMyMasterRequestsAll(int PersonID, int? languageID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.GetAccessRequestMasters(languageID, PersonID).ToList();

            }
        }



        /// <summary>
        /// get the request whos satus is submitted(2) and inprogress(3)
        /// </summary>
        /// <param name="PersonID"></param>
        /// <returns></returns>
        public static List<DAL.vw_AccessRequestMaster> GetMyMasterRequests(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from br in context.vw_AccessRequestMaster
                        where br.CreatedByID == dbPeopleID
                        where br.RequestStatusID > 1
                        where br.RequestStatusID < 4
                        orderby br.Created descending
                        select br;
                List<DAL.vw_AccessRequestMaster> myRequests = q.ToList();
                return myRequests;
            }
        }

        /// <summary>
        /// get the number of requests whos satus is submitted(2) and inprogress(3)
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="taskStatus"></param>
        /// <returns></returns>
        public static int GetMyMasterRequestsCount(int dbPeoplId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));

                context.GetMyMasterRequestsCount(dbPeoplId, Result).FirstOrDefault();

                return Convert.ToInt32(Result.Value);
            }
        }

        /// <summary>
        /// call the complex type method GetMyRoles and pass the PersonID
        /// </summary>
        /// <param name="PersonID"></param>
        /// <returns></returns>
        public static List<DAL.MyRoles_AccessAreas> GetAccessAreaRoles(int PersonID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.MyRoles_AccessAreas> myRoles = new List<DAL.MyRoles_AccessAreas>(context.GetMyRoles_AccessAreas(PersonID));
                return myRoles;
            }
        }

        public static List<DAL.MyRoles_Combined> GetMyRolesCombined(int personID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var roles = context.GetMyRolesCombined(personID);
                return roles.ToList();
            }
        }

        public static List<DAL.MspRoles> GetMspRoles(int nroleid, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.MspRoles> myRoles = new List<DAL.MspRoles>(context.GetMspRoles(nroleid, dbPeopleID));
                return myRoles;
            }
        }

        public static List<DAL.PassOfficeRoles> GetPassOfficeRoles(int nroleid, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.PassOfficeRoles> myRoles = new List<DAL.PassOfficeRoles>(context.GetPassOfficeRoles(nroleid, dbPeopleID));
                return myRoles;
            }
        }

        public static List<DBAccessController.DAL.GetVisitorRequest_Result> GetVisitorRequest(string users)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DBAccessController.DAL.GetVisitorRequest_Result> visitors = new List<DBAccessController.DAL.GetVisitorRequest_Result>(context.GetVisitorRequest(users));
                return visitors;
            }
        }

        public static List<DAL.MyRoles_DivisionAreas> GetMyDivisionRoles(int PersonID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.MyRoles_DivisionAreas> myRoles = new List<DAL.MyRoles_DivisionAreas>(context.GetMyRoles_DivisionAreas(PersonID));
                return myRoles;
            }
        }

        /// <summary>
        /// method to return an access area's details
        /// E.Parker
        /// 27.04.11
        /// </summary>
        /// <param name="accessArea"></param>
        /// <returns></returns>
        public static DAL.vw_AccessArea GetMSPAccessArea(int accessArea)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from br in context.vw_AccessArea
                        where br.AccessAreaID == accessArea
                        select br;

                DAL.vw_AccessArea areas = q.ToList().FirstOrDefault();
                return areas;
            }
        }

        /// <summary>
        /// Method to return all msps (role) for a specific vendor
        /// E.Parker
        /// 22.06.11
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public static List<DAL.GetMspsByVendorID_Result> GetMspsByVendorID(int vendorId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from br in context.GetMspsByVendorID(vendorId)
                        select br;

                List<DAL.GetMspsByVendorID_Result> items = q.ToList();
                return items;

            }
        }

        /// <summary>
        /// Method to return all vendors
        /// E.Parker
        /// 20.06.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.GetVendors_Result> GetVendors()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from br in context.GetVendors()
                        select br
                            ;


                List<DAL.GetVendors_Result> items = q.ToList();

                return items;
            }
        }
        #endregion

        #region Create

        /// <summary>
        /// Method to extend the MSP badge or Access
        /// E.Parker 
        /// 21.04.11
        /// </summary>
        /// <param name="dbPeopleID"></param>
        /// <param name="userID"></param>
        /// <param name="oldexpiryDate"></param>
        /// <param name="renewalPeriod"></param>
        /// <param name="forename"></param>
        /// <param name="surname"></param>
        /// <param name="company"></param>
        /// <param name="BadgeID"></param>
        /// <param name="miFairNo"></param>
        /// <param name="accessAreaID"></param>
        /// <returns></returns>
        public static bool ExtendMSP(int dbPeopleID, int userID, DateTime oldexpiryDate, int renewalPeriod, string forename, string surname, string company, int BadgeID, string miFairNo, int accessAreaID)
        {
            ObjectParameter Result = new ObjectParameter("Result", typeof(int));
            using (var context = new DAL.DBAccessEntities())
            {
                if (accessAreaID == 0)
                {
                    //renew pass
                    context.ExtendMSPBadge(dbPeopleID, userID, miFairNo, oldexpiryDate, renewalPeriod, forename, surname, company, BadgeID, Result);
                }
                else
                {
                    //renew access
                    context.ExtendMSPAccess(dbPeopleID, userID, oldexpiryDate, renewalPeriod, forename, surname, company, accessAreaID, BadgeID, Result);
                }
            }
            return Convert.ToBoolean(Result.Value);

        }

        #endregion

        #region Remove
        public static void DeleteDivisionRoleUser(int divisionRoleUserID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var divisionRoleUser = context.CorporateDivisionRoleUsers.Where(a => a.DivisionRoleUserID == divisionRoleUserID).SingleOrDefault();
                if (divisionRoleUser != null)
                    context.CorporateDivisionRoleUsers.DeleteObject(divisionRoleUser);

                context.SaveChanges();
            }
        }


        public static void DeletePassOfficeRoleUser(int passOfficeRoleUserID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var PassOfficeRoleUser = context.LocationPassOfficeUsers.Where(a => a.PassOfficeUserID == passOfficeRoleUserID).SingleOrDefault();
                if (PassOfficeRoleUser != null)
                    context.LocationPassOfficeUsers.DeleteObject(PassOfficeRoleUser);

                context.SaveChanges();
            }
        }

        public static void DeleteMspRoleUser(int mspID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var mspRoleUser = context.MSPRoleUsers.Where(a => a.MSPId == mspID).SingleOrDefault();
                if (mspRoleUser != null)
                    context.MSPRoleUsers.DeleteObject(mspRoleUser);
                context.SaveChanges();
            }
        }


        #endregion

    }
}
