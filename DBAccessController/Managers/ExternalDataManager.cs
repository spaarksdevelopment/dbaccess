﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    static class ExternalDataManager
    {
        public static List<DAL.DBMovesLocation> GetDBMovesLocations(int? dbMovesRegionID)
        {
            using (var context = new DAL.ExternalDataEntities())
            {
                var q = (IQueryable<DAL.DBMovesLocation>)context.DBMovesLocations;
                if (dbMovesRegionID.HasValue)
                    q = q.Where(a => a.RegionID == dbMovesRegionID);

                return q.OrderBy(a => a.LocationName).ToList();
            }
        }

        public static List<DAL.DBMovesLocationFloor> GetDBMovesFloors(int? dbMovesLocationID)
        {
            using (var context = new DAL.ExternalDataEntities())
            {
                var q = (IQueryable<DAL.DBMovesLocationFloor>)context.DBMovesLocationFloors;
                if (dbMovesLocationID.HasValue)
                    q = q.Where(a => a.LocationId == dbMovesLocationID);

                return q.ToList();
            }
        }
    }
}
