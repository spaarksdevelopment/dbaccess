﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Configuration;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    static class SmartcardServiceManager
    {
        #region Get methods
        /// <summary>
        /// Returns the status of the badge associated with the given smartcard identifier
        /// </summary>
        /// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
        public static string GetSmartcardStatus(string smartcardIdentifier, string[] certDetails)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardNumber number = context.SmartcardNumbers.Where(a => a.SerialNumber == smartcardIdentifier).SingleOrDefault();

                DBAccessEnums.SmartcardStatus status = DBAccessEnums.SmartcardStatus.Unknown;

                if (number != null)
                {
                    status = DBAccessEnums.SmartcardStatus.Initial;
                    string mifareZeros = "00" + number.Mifare;
                    HRPersonBadge badge = context.HRPersonBadges.Where(a => a.MiFareNumber == number.Mifare || a.MiFareNumber == mifareZeros).SingleOrDefault();

					//Try removing leading zeroes
					if (badge == null)
					{
						string paddedMifare = number.Mifare.PadLeft(12, '0');
						badge = context.HRPersonBadges.Where(a => a.MiFareNumber == paddedMifare).SingleOrDefault();
					}	

                    if (badge != null)
                        status = badge.Active ? DBAccessEnums.SmartcardStatus.Active : DBAccessEnums.SmartcardStatus.Inactive;
                }

                AddSmartcardServiceAudit("GetSmartcardStatus", smartcardIdentifier, status.ToString(), certDetails, null);

				switch (status)
				{
					case DBAccessEnums.SmartcardStatus.Active:
						return "activated";
					case DBAccessEnums.SmartcardStatus.Inactive:
						return "deactivated";
					case DBAccessEnums.SmartcardStatus.Initial:
						return "initial";
					default:
						return status.ToString();
				}
            }
        }

        /// <summary>
        /// Returns the PUK (Personal Unblocking Key) for the given smartcard or an empty string if smartcard is not known
        /// The PUK is associated with a vendor and will change every few years.
        /// The PUK is stored against the smartcard in the db when the smartcard is issued
        /// </summary>
        /// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
        public static string GetKeyIdentifier(string smartcardIdentifier, string[] certDetails)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardNumber number = context.SmartcardNumbers.Where(a => a.SerialNumber == smartcardIdentifier).SingleOrDefault();

                string puk = string.Empty;

                if (number != null)
                {
                    if (number.PUK != null)
                        puk = number.PUK;
                }

                AddSmartcardServiceAudit("GetKeyIdentifier", smartcardIdentifier, puk, certDetails, null);

                return puk;
            }
        }

        //overloaded methods because of translations for true/false
        public static List<GetSmartcardProviders_Result> GetSmartCardProviders(int LanguageId, bool includeDisabled) 
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (!includeDisabled)
                   // return context.SmartcardProviders.Where(a => a.Enabled).ToList();
                    return context.GetSmartcardProviders(LanguageId).Where(a => a.EnabledValue).ToList();

                return context.GetSmartcardProviders(LanguageId).ToList();
            }
        }

        public static List<SmartcardProvider> GetSmartCardProviders(bool includeDisabled)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (!includeDisabled)
                    return context.SmartcardProviders.Where(a => a.Enabled).ToList();


                return context.SmartcardProviders.ToList();
            }
        }

		public static List<vw_SmartcardProviderDetails> GetSmartCardProvidersExtended(bool includeDisabled)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				if (!includeDisabled)
					return context.vw_SmartcardProviderDetails.Where(a => a.Enabled).ToList();

				return context.vw_SmartcardProviderDetails.ToList();
			}
		}

        public static List<vw_SmartcardTypeDetails> GetSmartCardTypes(bool includeDisabled)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (!includeDisabled)
                    return context.vw_SmartcardTypeDetails.Where(a => a.Enabled).ToList();

                return context.vw_SmartcardTypeDetails.ToList();
            }
        }

        public static List<GetSmartCardTypes_Result> GetSmartCardTypes(int languageid, bool includeDisabled)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (!includeDisabled)
                    return context.GetSmartCardTypes(languageid).Where(a => a.EnabledValue).ToList();

                return context.GetSmartCardTypes(languageid).ToList();  
            }
        }
        public static vw_SmartcardProviderDetails GetSmartCardProvider(int id)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.vw_SmartcardProviderDetails.Where(a => a.ID == id).SingleOrDefault();
            }
        }


        public static List<vw_SmartcardNumberStaging> GetSmartcardNumbersStaging()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<vw_SmartcardNumberStaging> smartcardNumbersStaging = new List<vw_SmartcardNumberStaging>();
                var dbSmartcardNumbersStaging = context.vw_SmartcardNumberStaging;
                return dbSmartcardNumbersStaging.ToList();
            }
        }

        public static int GetDuplicatesCount()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.vw_SmartcardNumberStaging.Where(a => a.UploadStatusID == (int)DBAccessEnums.SmartcardNumberUploadStatus.MifareExists || 
																a.UploadStatusID == (int)DBAccessEnums.SmartcardNumberUploadStatus.dbSmartcardNumberExists).Count();
            }
        }

        public static int GetExistingCount()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.vw_SmartcardNumberStaging.Where(a => a.UploadStatusID == (int)DBAccessEnums.SmartcardNumberUploadStatus.BadgeExists).Count();
            }
        }

        public static int GetInvalidCount()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.vw_SmartcardNumberStaging.Where(a => a.UploadStatusID == (int)DBAccessEnums.SmartcardNumberUploadStatus.InvalidCharacters).Count();
            }
        }

        public static bool GetCanDeleteVendor(int vendorID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var provider = context.SmartcardProviders.Where(a => a.ID == vendorID).SingleOrDefault();
                if (provider == null)
                    return false;

                return provider.SmartcardTypes.Count == 0;
            }
        }

        public static bool GetCanDeleteSmartcardType(int smartcardTypeId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int serialNumbers = context.SmartcardNumbers.Where(a => a.SmartcardTypeID == smartcardTypeId).Count();

                return serialNumbers == 0;
            }
        }

        public static bool IsVendorNameUnique(int vendorID, string vendorName)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if(vendorID==0)
                    return context.SmartcardProviders.Where(a => a.ProviderName == vendorName).Count()==0;

                return context.SmartcardProviders.Where(a => a.ProviderName == vendorName && a.ID!=vendorID).Count() == 0;
            }
        }

        public static bool IsTypeNameUnique(int typeId, string keyIdentifier)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (typeId == 0)
                    return context.SmartcardTypes.Where(a => a.TypeIdentifier == keyIdentifier).Count() == 0;

                return context.SmartcardTypes.Where(a => a.TypeIdentifier == keyIdentifier && a.ID != typeId).Count() == 0;
            }
        }

		public static List<vw_SmartcardUploadAudit> GetVendorFiles()
		{
			using (var context = new DAL.DBAccessEntities())
			{
				List<vw_SmartcardUploadAudit> audits = new List<vw_SmartcardUploadAudit>();
				return context.vw_SmartcardUploadAudit.OrderByDescending(a => a.DateUploaded).ToList();
			}
		}

		public static List<vw_SmartcardNumbers> GetSmartcardNumbers(int auditID)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				return context.vw_SmartcardNumbers.Where(a=>a.SmartcardAuditID==auditID).ToList();
			}
		}

		//Returns the number of numbers in a vendor upload which have had a badge created with the corresponding mifare
		public static int GetNumberOfMatchingBadges(int auditID)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				return context.GetNumberOfMatchingBadges(auditID);
			}
		}

		public static bool RecallFile(int auditID)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				try
				{
					context.PopulateCreateBadgeTasks(auditID);
				}
				catch(Exception ex)
				{
					LogHelper.HandleException(ex);
					return false;
				}
				
				try
				{
					context.CopySmartcardNumbersToArchive(auditID);
				}
				catch(Exception ex)
				{
					LogHelper.HandleException(ex);
					return false;
				}
				
				SmartcardUploadAudit audit = context.SmartcardUploadAudits.Where(a=>a.ID==auditID).SingleOrDefault();
				audit.RecallDate = DateTime.Now;
				context.SaveChanges();

				return true;
			}
		}

		public static List<vw_SmartcardServiceAction> GetPendingActions()
		{
			using (var context = new DAL.DBAccessEntities())
			{
				return
					context.vw_SmartcardServiceAction.Where(a => a.Status == (int)DBAccessEnums.SmartcardServiceActionStatus.New
										|| a.Status == (int)DBAccessEnums.SmartcardServiceActionStatus.CommunicationError).ToList();
			}
		}

		public static HRPersonBadge GetBadge(string mifare)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				return context.HRPersonBadges.Where(a => a.MiFareNumber == mifare).FirstOrDefault();
			}
		}
        #endregion

        #region Create
		public static void InsertSmartcardNumbersStaging(List<SmartcardNumberStaging> list)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                context.ClearSmartcardNumberStaging();

                foreach (SmartcardNumberStaging s in list)
                {
                    context.SmartcardNumberStagings.AddObject(s);
                }
                context.SaveChanges();

                context.ProcessSmartcardNumberStaging();
            }
        }

        public static int ConfirmSmartcardUpload(int userID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter NumberOfUploads = new ObjectParameter("NumberOfUploads", typeof(int));

                context.ConfirmSmartcardNumberUpload(userID, NumberOfUploads);

                if (NumberOfUploads == null)
                    return 0;

                return (int)NumberOfUploads.Value;
            }
        }

        public static void AddSmartcardServiceAudit(string method, string smartcardIdentifier, string returnValue, string[] certDetails, string errorMessage)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardServiceAudit audit = new SmartcardServiceAudit();
                audit.DateMethodCalled = DateTime.Now;
                audit.MethodCalled = method;
                audit.SmartcardIdentifier = smartcardIdentifier;
                audit.ReturnedValue = returnValue;
                audit.CertSubjectName = certDetails[0];
                audit.CertSerialNumber = certDetails[1];
                audit.CertIssuerName = certDetails[2];
                audit.ErrorMessage = errorMessage;
                context.SmartcardServiceAudits.AddObject(audit);
                context.SaveChanges();
            }
        }

        public static void AddSmartcardProviderAudit(SmartcardProvider oldProvider, SmartcardProvider newProvider, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardProviderAudit audit = new SmartcardProviderAudit();
                if (oldProvider != null)
                {
                    audit.OldProviderID = oldProvider.ID;
                    audit.OldProviderName = oldProvider.ProviderName;
                    audit.OldUseHexMifare = oldProvider.UseHexMifare;
                    audit.OldCurrentPUK = oldProvider.CurrentPUK;
                    audit.OldEnabled = oldProvider.Enabled;
                }

                if (newProvider != null)
                {
                    audit.NewProviderID = newProvider.ID;
                    audit.NewProviderName = newProvider.ProviderName;
                    audit.NewUseHexMifare = newProvider.UseHexMifare;
                    audit.NewCurrentPUK = newProvider.CurrentPUK;
                    audit.NewEnabled = newProvider.Enabled;
                }
                audit.ChangeDate = DateTime.Now;

                mp_User user = context.mp_User.Where(a => a.dbPeopleID == dbPeopleID).FirstOrDefault();
                if (user == null)
                    return;

                audit.mp_UserId = user.Id;

                context.SmartcardProviderAudits.AddObject(audit);
                context.SaveChanges();
            }
        }

        public static void AddSmartcardTypeAudit(SmartcardType oldType, SmartcardType newType, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardTypeAudit audit = new SmartcardTypeAudit();
                if (oldType != null)
                {
                    audit.OldTypeID = oldType.ID;
                    audit.OldProviderID = oldType.SmartCardProviderID;
                    audit.OldTypeName = oldType.Type;
                    audit.OldLabel = oldType.Label;
                    audit.OldTypeIdentifier = oldType.TypeIdentifier;
                    audit.OldEnabled = oldType.Enabled;
                }

                if (newType != null)
                {
                    audit.NewTypeID = newType.ID;
                    audit.NewProviderID = newType.SmartCardProviderID;
                    audit.NewTypeName = newType.Type;
                    audit.NewLabel = newType.Label;
                    audit.NewTypeIdentifier = newType.TypeIdentifier;
                    audit.NewEnabled = newType.Enabled;
                }

                audit.ChangeDate = DateTime.Now;

                mp_User user = context.mp_User.Where(a => a.dbPeopleID == dbPeopleID).FirstOrDefault();
                if (user == null)
                    return;

                audit.mp_UserId = user.Id;

                context.SmartcardTypeAudits.AddObject(audit);
                context.SaveChanges();
            }
        }
        #endregion

        #region Remove
        public static int DeleteSmartCardProvider(int id, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardProvider smartcardProvider = context.SmartcardProviders.Where(a => a.ID == id).SingleOrDefault();

                if (smartcardProvider == null)
                    return 0;

                SmartcardProvider oldSmartcardProvider = new SmartcardProvider();
                oldSmartcardProvider.Enabled = smartcardProvider.Enabled;
                oldSmartcardProvider.ID = smartcardProvider.ID;
                oldSmartcardProvider.CurrentPUK = smartcardProvider.CurrentPUK;
                oldSmartcardProvider.ProviderName = smartcardProvider.ProviderName;
                oldSmartcardProvider.UseHexMifare = smartcardProvider.UseHexMifare;

                context.SmartcardProviders.DeleteObject(smartcardProvider);

                AddSmartcardProviderAudit(oldSmartcardProvider, null, dbPeopleID);

                return context.SaveChanges();
            }
        }

        public static int DeleteSmartCardType(int id, int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                SmartcardType smartCardType = context.SmartcardTypes.Where(a => a.ID == id).SingleOrDefault();

                if (smartCardType == null)
                    return 0;

                SmartcardType oldSmartcardType = new SmartcardType();
                oldSmartcardType.Enabled = smartCardType.Enabled;
                oldSmartcardType.ID = smartCardType.ID;
                oldSmartcardType.Label = smartCardType.Label;
                oldSmartcardType.SmartCardProviderID = smartCardType.SmartCardProviderID;
                oldSmartcardType.Type = smartCardType.Type;
                oldSmartcardType.TypeIdentifier = smartCardType.TypeIdentifier;

                context.SmartcardTypes.DeleteObject(smartCardType);

                AddSmartcardTypeAudit(oldSmartcardType, null, dbPeopleID);

                return context.SaveChanges();
            }
        }

		public static void ClearSmartcardNumberStaging()
		{
			using (var context = new DAL.DBAccessEntities())
            {
                context.ClearSmartcardNumberStaging();
            }
		}
        #endregion

        #region Update
        /// <summary>
        /// Updating the existing values
        /// </summary>
        /// <param name="vendorID"></param>
        /// <param name="vendorName"></param>
        /// <param name="keyIdentifier"></param>
        /// <param name="enabled"></param>
        /// <returns></returns>
        public static void UpdateSmartCardProvider(int _dbPeopleID, string vendorName, string keyIdentifier, bool useHexMifare, bool enabled, int vendorID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (vendorID > 0)
                {
                    SmartcardProvider existingSmartcardProvider = context.SmartcardProviders.Where(a => a.ID == vendorID).FirstOrDefault();
                    if (existingSmartcardProvider != null)
                    {
                        SmartcardProvider oldSmartcardProvider = new SmartcardProvider();
                        oldSmartcardProvider.ProviderName = existingSmartcardProvider.ProviderName;
                        oldSmartcardProvider.CurrentPUK = existingSmartcardProvider.CurrentPUK;
                        oldSmartcardProvider.Enabled = existingSmartcardProvider.Enabled;
                        oldSmartcardProvider.UseHexMifare = existingSmartcardProvider.UseHexMifare;

                        existingSmartcardProvider.ProviderName = vendorName;
                        existingSmartcardProvider.CurrentPUK = keyIdentifier;
                        existingSmartcardProvider.Enabled = enabled;
                        existingSmartcardProvider.UseHexMifare = useHexMifare;

						context.SaveChanges();

                        SmartcardServiceManager.AddSmartcardProviderAudit(oldSmartcardProvider, existingSmartcardProvider, _dbPeopleID);
                    }
                }
                else
                {
                    SmartcardProvider newSmartcardProvider = new SmartcardProvider();
                    if (newSmartcardProvider != null)
                    {
                        newSmartcardProvider.ProviderName = vendorName;
                        newSmartcardProvider.CurrentPUK = keyIdentifier;
                        newSmartcardProvider.Enabled = enabled;
                        newSmartcardProvider.UseHexMifare = useHexMifare; 
                        context.SmartcardProviders.AddObject(newSmartcardProvider);

                        context.SaveChanges(); 

                        SmartcardServiceManager.AddSmartcardProviderAudit(null, newSmartcardProvider, _dbPeopleID);
                    }
                }
            }
        }


        public static void UpdateSmartCardType(int dbPeopleID, string smartCardType, string keyIdentifier, bool enabled, int providerID, string label, int typeID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                if (typeID > 0)
                {
                    SmartcardType existingSmartcardType = context.SmartcardTypes.Where(a => a.ID == typeID).FirstOrDefault();
                    if (existingSmartcardType != null)
                    {
                        SmartcardType oldSmartcardType = new SmartcardType();

                        oldSmartcardType.ID = existingSmartcardType.ID;
                        oldSmartcardType.SmartCardProviderID = existingSmartcardType.SmartCardProviderID;
                        oldSmartcardType.Type = existingSmartcardType.Type;
                        oldSmartcardType.Enabled = existingSmartcardType.Enabled;
                        oldSmartcardType.Label = existingSmartcardType.Label;
                        oldSmartcardType.TypeIdentifier = existingSmartcardType.TypeIdentifier;

                        existingSmartcardType.Type = smartCardType;
                        existingSmartcardType.TypeIdentifier = keyIdentifier;
                        existingSmartcardType.Enabled = enabled;
                        existingSmartcardType.Label = label;

						context.SaveChanges();

                        SmartcardServiceManager.AddSmartcardTypeAudit(oldSmartcardType, existingSmartcardType, dbPeopleID);
                    }
                }
                else
                {
                    SmartcardType newSmartcardType = new SmartcardType();
                    if (newSmartcardType != null)
                    {
                        newSmartcardType.SmartCardProviderID = providerID;
                        newSmartcardType.Type = smartCardType;
                        newSmartcardType.TypeIdentifier = keyIdentifier;
                        newSmartcardType.Enabled = enabled;
                        newSmartcardType.Label = label;
                        context.SmartcardTypes.AddObject(newSmartcardType);

                        context.SaveChanges(); //Save so ID is available to audit

                        SmartcardServiceManager.AddSmartcardTypeAudit(null, newSmartcardType, dbPeopleID);
                    }
                }
            }
        }

		public static void UpdateRevokeSmartcardAction(int id, int result)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				DBAccessEnums.SmartcardServiceActionStatus newStatus;

				switch (result)
				{
					case 0:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.Success;
						break;
					case 1:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.ErrorInvalidArgument;
						break;
					case 2:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.ErrorSmartcardNumberNotFound;
						break;
					case 256:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.CommunicationError;
						break;
					case -1:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.UnknownError;
						break;
					default:
						throw new ApplicationException("An unknown result was passed to method UpdateRevokeSmartcardAction");
				}

				SmartcardServiceAction action = context.SmartcardServiceActions.Where(a=>a.ID == id).SingleOrDefault();
				if(action==null)
					throw new ApplicationException(string.Format("Tried to update an action that wasn't found in the database with id={0}", id));

				if (newStatus == DBAccessEnums.SmartcardServiceActionStatus.CommunicationError)
				{
					int maxRetries = GetMaximumRetries();
					if (maxRetries == action.NumberOfAttempts + 1)
					{
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.ReachedMaximumRetries;
						SmartcardServiceManager.SendRevokeServiceDownEmail();	
					}

					action.NumberOfAttempts++;
				}

				action.Status = (int)newStatus;
				action.LastAttempt = DateTime.Now;
				context.SaveChanges();
			}
		}

		private static void SendRevokeServiceDownEmail()
		{
			try
			{
				string adminAddress = ConfigurationManager.AppSettings["AdminContactLink"];

				if (string.IsNullOrEmpty(adminAddress))
					throw new ConfigurationErrorsException("The configuration parameter \"AdminContactLink\" is missing, please review the configuration file of the service");


				Guid guid = Guid.NewGuid();
				string body = "The windows service which runs as part of the DBAccess project depends on the Smartcard Revoke service.\n" +
								"The service could not access the Revokation webservice and the maximum retries have been reached";
				EmailManager.SendEmail(adminAddress, string.Empty, "PKI Revokation Webservice is Down", body, guid);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}

		public static void UpdateReplacementBadgeAction(int id, int result)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				DBAccessEnums.SmartcardServiceActionStatus newStatus;

				switch (result)
				{
					case 0:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.Success;
						break;
					case 1:
						newStatus = DBAccessEnums.SmartcardServiceActionStatus.UnknownError;
						break;
					default:
						throw new ApplicationException("An unknown result was passed to method UpdateReplacementBadgeAction");
				}

				SmartcardServiceAction action = context.SmartcardServiceActions.Where(a => a.ID == id).SingleOrDefault();
				if (action == null)
					throw new ApplicationException(string.Format("Tried to update an action that wasn't found in the database with id={0}", id));

				action.Status = (int)newStatus;
				action.LastAttempt = DateTime.Now;
				context.SaveChanges();
			}
		}

		public static int GetMaximumRetries()
		{
			using (var context = new DAL.DBAccessEntities())
			{
				int retriesInt = 5;

				var adminValue = context.AdminValues.Where(a => a.Key == "RevokeSmartcardRetries").FirstOrDefault();
				if (adminValue == null)
					return retriesInt;

				string retries = adminValue.Value;

				int.TryParse(retries, out retriesInt);

				return retriesInt;
			}
		}
        #endregion
	}
}
