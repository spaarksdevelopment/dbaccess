﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Configuration;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using Spaarks.Common.UtilityManager;
//using Aduvo.Common.UtilityManager;


namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    public static class PagingExtensions
    {
        //used by LINQ to SQL
        public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }

    }
    /// <summary>
    /// handles all activity relating to corporate structure
    /// i.e.
    /// Divisions
    /// Business Areas
    /// UBR
    /// Approvers
    /// Vendors
    /// 
    /// </summary>
    static class CorporationManager
    {
        #region GET

        /// <summary>
        /// AC: Called by a DA on the MyAccessAreaApprovers page.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<vw_DistinctDivisionAccessAreasWithApprovers> GetAccessAreasByDA(int userId, int? languageID)
        {
            List<vw_DistinctDivisionAccessAreasWithApprovers> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetMyDistinctAccessAreas(userId, languageID).ToList();
            }

            return data;
        }

        public static List<vw_DivisionAccessAreasWithApprovers> GetAccessAreasAndApproversByDA(int userId)
        {
            List<vw_DivisionAccessAreasWithApprovers> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetMyAccessAreas(userId).ToList();
            }

            return data;
        }

        /// <summary>
        /// AC: Called by a DA on the MyAccessAreaApprovers page to populate the approvers grid once an area has been selected.
        /// </summary>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="role"></param>
        /// <returns>A list of acccess area approvers for the selected division</returns>
        public static List<AccessAreaApprover> GetMyAccessAreaApprovers(int selectedCorporateDivisionAccessAreaId, int role)
        {
            List<AccessAreaApprover> data;

            using (var context = new DBAccessEntities())
            {
                data = context.GetMyAccessAreaApprovers(selectedCorporateDivisionAccessAreaId, role).ToList();
            }

            return data;
        }

        /// <summary>
        /// Method to bring back the access area approvers based on division id and classificationid
        /// E.Parker
        /// 26.05.11
        /// </summary>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public static List<GetMyAccessAreaApproversExternalClass> GetMyAccessAreaApproversEx(int selectedCorporateDivisionAccessAreaId, int role, int classificationID)
        {
            List<GetMyAccessAreaApproversExternalClass> data;

            using (var context = new DBAccessEntities())
            {
                data = context.GetMyAccessAreaApproversExternalClass(selectedCorporateDivisionAccessAreaId, classificationID, role).ToList();
            }

            return data;
        }

        public static List<RoleForRecertification> GetRolesForRecertification(int userId)
        {
            List<RoleForRecertification> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetRolesForRecertification(userId).ToList();
            }

            return data;
        }

        public static List<RoleForRecertification> GetDARolesForRecertification(int userId)
        {
            List<RoleForRecertification> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetDARolesForRecertification(userId).ToList();
            }

            return data;
        }

        public static List<DivisionRoleUser> GetDivisionRoleUsers(int divisionID)
        {
            List<DivisionRoleUser> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetDivisionRoleUsers(divisionID).ToList();
            }

            return data;
        }

        public static List<PassOfficeUser> GetPassOfficeUsers(Int32 passOfficeId)
        {
            List<PassOfficeUser> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetPassOfficeUsers(passOfficeId).ToList();
            }
            return data;
        }

        public static System.Collections.ArrayList GetUBRPath(string targetUBR)
        {
            System.Collections.ArrayList data = new System.Collections.ArrayList();

            using (var context = new DAL.DBAccessEntities())
            {
                var currentUBR = context.UBRCodes.Where(a => a.UBR_Code == targetUBR).SingleOrDefault();

                while (currentUBR != null && currentUBR.Parent != null)
                {
                    data.Add(currentUBR.UBR_Code);

                    if (currentUBR.UBR_Code == "G_0183")
                        break;

                    currentUBR = context.UBRCodes.Where(a => a.UBR_Code == currentUBR.Parent).SingleOrDefault();
                }
            }

            return data;
        }

        public static int GetCertifiedDACount(int userId, int actionType, string selectedUsers)
        {
            int count = 0;

            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                context.GetCertifiedDACount(userId, actionType, selectedUsers, Result);

                count = (int)Result.Value;
            }

            return count;
        }

        public static List<mp_SecurityGroup> GetAllRoles()
        {
            List<mp_SecurityGroup> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<mp_SecurityGroup>)context.GetAllRoles().ToList();
            }

            return data;
        }

        public static DAL.CorporateBusinessArea GetBusinessArea(int BusinessAreaId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var qry = (from ba in context.CorporateBusinessAreas where ba.BusinessAreaID == BusinessAreaId select ba);

                return qry.First();
            }
        }

        public static DAL.CorporateBusinessArea GetBusinessArea(int divisionID, string ubr)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var qry = (from ba in context.CorporateBusinessAreas where ba.DivisionID == divisionID & ba.UBR == ubr select ba);

                return qry.First();
            }
        }

        public static int DeleteDivision(int divisionID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                context.DeleteDivision(divisionID, Result);

                int returnVal = Convert.ToInt32(Result.Value);
                return returnVal;
            }

        }

        public static DAL.GetCorporateBusinessArea GetVwBusinessArea(int BusinessAreaId, int? languageID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                //var qry = (from ba in context.vw_CorporateBusinessArea where ba.BusinessAreaID == BusinessAreaId select ba);
                var qry = context.GetCorporateBusinessAreas(languageID, BusinessAreaId);
                return qry.First();
            }
        }

        public static List<BusinessAreaApproverForRecertification> GetApproversForRecertificationByDivisionId(int divisionId)
        {
            List<BusinessAreaApproverForRecertification> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<BusinessAreaApproverForRecertification>)context.GetBusinessAreaApproversForRecertification(divisionId).ToList();
            }

            return data;
        }

        public static List<GetMyDivisions> GetAllMyDivisions(int userId, int? languageID)
        {
            List<GetMyDivisions> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<GetMyDivisions>)context.GetMyDivisions(userId, languageID).ToList();
            }

            return data;
        }

        public static List<DAL.vw_BusinessAreasWithApprovers> GetMyDivisions(int UserId, int? divisionId)
        {
            List<DAL.vw_BusinessAreasWithApprovers> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.vw_BusinessAreasWithApprovers>)context.GetMyBusinessAreas(divisionId, UserId).ToList().Where(a => a.HasDivisionNode == true).ToList();
            }

            return data;
        }


        public static List<AccessAreaApproverForConfirmation> GetAccessAreaApproversForConfirmation(int corporateDivisionAccessAreaID)
        {
            List<AccessAreaApproverForConfirmation> data;

            using (var context = new DBAccessEntities())
            {
                data = context.GetAccessAreaApproversForConfirmation(corporateDivisionAccessAreaID).ToList();
            }

            return data;
        }


        /// <summary>
        /// Gets approvers for confirmation, but also returns division id and business area id from within the same database context
        /// </summary>
        /// <param name="corporateDivisionAccessAreaID"></param>
        /// <param name="divisionId"></param>
        /// <param name="accessAreaId"></param>
        /// <returns></returns>
        public static List<AccessAreaApproverForConfirmation> GetAccessAreaApproversForConfirmation(int corporateDivisionAccessAreaID, ref int divisionId, ref int accessAreaId)
        {
            List<AccessAreaApproverForConfirmation> data;

            using (var context = new DBAccessEntities())
            {
                var CDAA = (from cdaa in context.CorporateDivisionAccessAreas
                            where cdaa.CorporateDivisionAccessAreaID == corporateDivisionAccessAreaID
                            select cdaa).ToList().First();

                divisionId = CDAA.DivisionID;
                accessAreaId = CDAA.AccessAreaID;

                data = context.GetAccessAreaApproversForConfirmation(corporateDivisionAccessAreaID).ToList();
            }

            return data;
        }

        public static List<BusinessAreaApprover> GetBusinessAreaApproversForConfirmation(int businessAreaID, int classificationID)
        {
            List<BusinessAreaApprover> data;

            using (var context = new DBAccessEntities())
            {
                data = context.GetBusinessAreaApproversForConfirmation(businessAreaID, classificationID).ToList();
            }

            return data;
        }

        public static List<BusinessAreaApprover> GetBusinessAreaApproversForConfirmation(int businessAreaID, ref int divisionId, int classificationID)
        {
            List<BusinessAreaApprover> data;

            using (var context = new DBAccessEntities())
            {
                var businessArea = (from ba in context.CorporateBusinessAreas where ba.BusinessAreaID == businessAreaID select ba).First();

                divisionId = businessArea.DivisionID;

                data = context.GetBusinessAreaApproversForConfirmation(businessAreaID, classificationID).ToList();
            }

            return data;
        }

        public static List<string[,]> GetDivisionsByCountry(int countryId)
        {
            using (var context = new DBAccessEntities())
            {
                List<string[,]> divisions = new List<string[,]>();


                var qry = (from cust in context.CorporateDivisions
                           where cust.CountryID == countryId && cust.Enabled
                           orderby cust.Name
                           select new { cust.DivisionID, cust.Name }).Distinct().OrderBy(a => a.Name);

                foreach (var a in qry)
                {
                    string[,] division = new string[2, 2];
                    division.SetValue(a.Name, 1, 0);
                    division.SetValue(a.DivisionID.ToString(), 0, 1);
                    divisions.Add(division);
                }

                return divisions;
            }
        }

        public static bool DeleteRAApprovers(int CorporateBusinessAreaApproverId)
        {
            Boolean bval = false;
            using (var context = new DBAccessEntities())
            {
                var qry = (from cust in context.CorporateBusinessAreaApprovers
                           where cust.CorporateBusinessAreaApproverId == CorporateBusinessAreaApproverId
                           select cust).FirstOrDefault();

                context.DeleteObject(qry);
                int ndelcount = context.SaveChanges();
                bval = (ndelcount == 1) ? true : false;

            }
            return bval;
        }

        public static bool DeleteAApprovers(int AccessAreaApproverID)
        {
            Boolean bval = false;
            using (var context = new DBAccessEntities())
            {
                var qry = (from cust in context.CorporateDivisionAccessAreaApprovers
                           where cust.CorporateDivisionAccessAreaApproverId == AccessAreaApproverID
                           select cust).FirstOrDefault();

                context.DeleteObject(qry);
                int ndelcount = context.SaveChanges();

                bval = (ndelcount == 1) ? true : false;

            }
            return bval;
        }

        /// <summary>
        /// AC: added 1/3/11 to support Division Profile option on Access Request page.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static List<string[,]> GetDivisions(int cityId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<string[,]> divisions = new List<string[,]>();


                var qry = (from cust in context.vw_DivisionAccessArea where cust.CityID == cityId select new { cust.DivisionID, cust.DivisionName }).Distinct();

                foreach (var a in qry)
                {
                    string[,] division = new string[2, 2];
                    division.SetValue(a.DivisionName.ToString(), 1, 0);
                    division.SetValue(a.DivisionID.ToString(), 0, 1);
                    divisions.Add(division);
                }

                return divisions;
            }
        }

        public static DAL.CorporateDivision GetDivision(int divisionId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.CorporateDivisions.Where(a => a.DivisionID == divisionId).SingleOrDefault();
            }
        }

        public static DAL.UBRCode GetUBRCode(string code)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from ubrcode in context.UBRCodes
                        where ubrcode.UBR_Code == code
                        select ubrcode;
                List<DAL.UBRCode> ubrDetails = q.ToList();

                return (ubrDetails.Count > 0) ? ubrDetails[0] : null;
            }
        }

        public static List<DAL.UBRNodes> GetUBRNodes(string startNode, bool IsRoot)
        {
            List<DAL.UBRNodes> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.UBRNodes>)context.GetUBRNodes(startNode, IsRoot).ToList();
            }

            return data;
        }

        public static List<DAL.UBRDivision> GetUBRDivisions(string startNode, bool IsRoot, int? countryFilterID)
        {
            List<DAL.UBRDivision> data;

            using (var context = new DAL.DBAccessEntities())
            {
                if (!countryFilterID.HasValue)
                    countryFilterID = 0;

                data = (List<DAL.UBRDivision>)context.GetUBRDivisions(startNode, IsRoot, countryFilterID).ToList();
            }

            return data;
        }


        public static List<UBRDivision> GetSubdivisions(int? existingCountryFilterID, int divisionID)
        {
            List<DAL.UBRDivision> data;

            using (var context = new DAL.DBAccessEntities())
            {

                data = (List<DAL.UBRDivision>)context.GetSubdivisions(existingCountryFilterID, divisionID).ToList();
            }

            return data;
        }


        /// <summary>
        /// Method to return a list of the access areas a recertifier is assigned to
        /// E.Parker
        /// March 2011
        /// </summary>
        /// <param name="DbPeopleID"></param>
        /// <returns></returns>
        public static List<DAL.vw_RecertifierAccessAreas> GetMyRecertifyAccessAreas(int dbPeopleID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from recertiferAreas in context.vw_RecertifierAccessAreas
                        where recertiferAreas.dbPeopleID == dbPeopleID
                        select recertiferAreas;

                List<DAL.vw_RecertifierAccessAreas> myAccessAreas = q.ToList();
                return myAccessAreas;
            }
        }

        /// <summary>
        /// Method to return a list of the access areas a recertifier is assigned to
        /// E.Parker
        /// March 2011
        /// </summary>
        /// <param name="DbPeopleID"></param>
        /// <returns></returns>
        public static List<DAL.vw_AdminRecertifierAccessAreas> GetAdminRecertifyAccessAreas(int dbPeopleID)
        {

            using (var context = new DAL.DBAccessEntities())
            {
                var t = from locationCountryUser in context.LocationCountryUsers
                        where locationCountryUser.dbPeopleID == dbPeopleID
                        select locationCountryUser.CountryID;

                List<int> countryList = t.ToList();

                var q = from recertiferAreas in context.vw_AdminRecertifierAccessAreas
                        where countryList.Contains(recertiferAreas.CountryID)
                        select recertiferAreas;

                List<DAL.vw_AdminRecertifierAccessAreas> myAccessAreas = q.ToList();
                return myAccessAreas;
            }
        }

        /// <summary>
        /// Method to return a list of the people in an access area who havent been certified for a specific access area
        /// E.Parker
        /// March 2011
        /// </summary>
        /// <param name="DbPeopleID"></param>
        /// <returns></returns>
        public static List<DAL.vw_AccessAreaRecertifications> GetRecertifierPeople(int accessAreaID, int corporateDivisionAccessAreaId)
        {

            List<DAL.vw_AccessAreaRecertifications> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.vw_AccessAreaRecertifications>)context.GetUsersForRecertificationByAccessArea(accessAreaID, corporateDivisionAccessAreaId).ToList();
            }

            return data;
        }
        #endregion

        #region Create
        //Returns 0 for an error or divisionID for success
        public static int CreateEditDivision(UBRDivision division, bool isNew, int currentUserID, int iRequestMasterID_DA, int iRequestMasterID_DO, bool isSubdivision, int? subdivisionParentDivisionID)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                context.CreateEditDivision(Convert.ToInt32(division.Id), division.CountryID, division.Name, division.UBRCode, division.Enabled, 
                    division.RecertPeriod, isNew, currentUserID, iRequestMasterID_DA, iRequestMasterID_DO, isSubdivision, subdivisionParentDivisionID, Result);

                return Convert.ToInt32(Result.Value);
            }
        }

        public static int InsertAccessArea(int areaTypeId, int? floorId, int buildingId, string name, int? divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int? defaultAccessPeriod, string defaultAccessDuration)
        {
            if (String.IsNullOrEmpty(name))
                return 0;

            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                context.InsertAccessArea(areaTypeId, floorId, buildingId, divisionId, controlSystemId, recertPeriod, name, categoryid, enabled, defaultAccessPeriod, defaultAccessDuration, Result);

                return (int)Result.Value;
            }
        }

        /// <summary>
        /// Method to return an object of emails for the DA's
        /// E.Parker 
        /// 16.06.11
        /// </summary>
        /// <param name="divisionID"></param>
        /// <returns></returns>
        public static List<DAL.GetAllDAEmails> DAsToEmail(int divisionID)
        {
            using (var context = new DBAccessEntities())
            {
                List<DAL.GetAllDAEmails> items = context.GetAllDAs(divisionID).ToList();
                return items;
            }
        }

        public static int InsertBusinessArea(int userID, int divisionId, string name, string ubrCode)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.InsertBusinessArea(userID, divisionId, name, ubrCode, Result);
                return (int)Result.Value;
            }
        }

        public static int InsertBusinessAreaApprover(int businessAreaId, int dbPeopleID, int sortOrder)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int r = context.InsertBusinessAreaApprover(businessAreaId, dbPeopleID, sortOrder, 0, Result);
                return (int)Result.Value;
            }
        }

        /// <summary>
        /// Method to add an approver for a classification
        /// E.Parker
        /// 25.05.11
        /// </summary>
        /// <param name="businessAreaId"></param>
        /// <param name="dbPeopleID"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public static int InsertBusinessAreaExApprover(int businessAreaId, int dbPeopleID, int? sortOrder, int? classificationID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int r = context.InsertBusinessAreaApprover(businessAreaId, dbPeopleID, sortOrder, classificationID, Result);
                return (int)Result.Value;
            }
        }

        public static int InsertAccessAreaApprover(Int32 iAccessAreaID, Int32 dbPeopleID, Int32 iSortOrder, Int32? iClassificationID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                Int32 iRowsAffected = context.InsertAccessAreaApprover(iAccessAreaID, dbPeopleID, null, 5, iSortOrder, iClassificationID, iClassificationID.HasValue, Result);
                return (int)Result.Value;
            }
        }

        public static Int32 InsertAccessAreaRecertifier(Int32 iAccessAreaID, Int32 dbPeopleID, Int32 iSortOrder)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                Int32 iRowsAffected = context.InsertAccessRecertifier(iAccessAreaID, dbPeopleID, null, 8, iSortOrder, null, false, Result);
                return (int)Result.Value;
            }
        }

        /// <summary>
        /// Method to insert a new approver for an access area for the external classifications
        /// E.Parker
        /// 27.05.11
        /// </summary>
        /// <param name="selectedCorporateDivisionAccessAreaId"></param>
        /// <param name="dbPeopleId"></param>
        /// <param name="roleType"></param>
        /// <param name="sortOrder"></param>
        /// <param name="classificationId"></param>
        /// <returns></returns>
        public static int InsertExAccessAreaApprover(int selectedCorporateDivisionAccessAreaId, int dbPeopleId, int roleType, int sortOrder, int classificationId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int r = context.InsertAccessAreaApprover(selectedCorporateDivisionAccessAreaId, dbPeopleId, null, roleType, sortOrder, classificationId, true, Result);
                return (int)Result.Value;
            }
        }

        public static int InsertDivisionRoleUser(int divisionID, int dbPeopleID, int securityGroupID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int r = context.InsertDivisionRoleUser(divisionID, dbPeopleID, securityGroupID, Result);
                return (int)Result.Value;
            }
        }

        public static Boolean InsertPassOfficeUser(Int32 iUserId, Int32 iPassOfficeId, Int32 iCreatedByUserId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter opResult = new ObjectParameter("bResult", typeof(Boolean));
                Int32 iRowsAffected = context.InsertPassOfficeUser(iUserId, iPassOfficeId, iCreatedByUserId, opResult);
                return (Boolean)opResult.Value;
            }
        }

        public static Boolean InsertNewPassOfficeUser(int dbPeopleId, int PassOfficeId, int iCreatedByUserId)
        {
            //check if the person already exists
            using (var context = new DAL.DBAccessEntities())
            {
                var person = context.LocationPassOfficeUsers.Where(a => a.dbPeopleID == dbPeopleId && a.PassOfficeID == PassOfficeId).FirstOrDefault();
                if (person != null) return false;//already exist

                if (!InsertPassOfficeUser(dbPeopleId, PassOfficeId, iCreatedByUserId))
                {
                    return false;
                }
                else
                {
                    LocationPassOffice thePassOffice = context.LocationPassOffices.SingleOrDefault(e => e.PassOfficeID == PassOfficeId);

                    string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                    bool isSwitzerland = false;
                    int switzerlandIdValue = 0;
                    string changeMakerName = string.Empty;
                    string passOfficeName = string.Empty;

                    if (thePassOffice != null && !String.IsNullOrEmpty(switzerlandStringId) && Int32.TryParse(switzerlandStringId, out switzerlandIdValue))
                    {

                        //  Now we need to get the list of Cities for this countryId
                        List<LocationCity> theCities = context.LocationCities.Where(e => e.CountryID == switzerlandIdValue).ToList();
                        List<int> citiesList = new List<int>();

                        if (theCities != null)
                        {
                            foreach (LocationCity cityItem in theCities)
                            {
                                citiesList.Add(cityItem.CityID);
                            }
                        }

                        if (citiesList.Contains(thePassOffice.CityID.GetValueOrDefault(0)))
                        {

                            mp_User changeUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == iCreatedByUserId);
                            changeMakerName = changeUser.Forename + " " + changeUser.Surname;
                            isSwitzerland = true;
                            passOfficeName = thePassOffice.Name;
                        }
                    }

                    //  Need to send email here if in switzerland
                    if (isSwitzerland)
                    {
                        mp_User deletionUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == dbPeopleId);
                        SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Added", "Pass Office Administrator", passOfficeName, changeMakerName);
                    }

                    return true;
                }
            }
        }
        #endregion

        #region Remove

        /// <summary>
        /// AC: deletes an access request person from accessrequest.aspx
        /// </summary>
        /// <param name="approverId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool DeleteAccessAreaApprover(int approverId, int userId)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));

                context.DeleteAccessAreaApprover(approverId, userId, result);

                return Convert.ToBoolean(result.Value);
            }

        }



        public static bool UpdateApproverEsculation(int nRApprover, int nAApprover)
        {
            Boolean success = false;
            using (var context = new DAL.DBAccessEntities())
            {
                if (nRApprover != 0)
                {
                    var nRequestApprover = context.ApprovalEsculationFrequencies.Where(x => x.TaskTypeID == 2).FirstOrDefault();
                    if (nRequestApprover != null)
                    {
                        nRequestApprover.EsculationFrequency = nRApprover;
                    }
                }

                if (nAApprover != 0)
                {
                    var nAccessApprover = context.ApprovalEsculationFrequencies.Where(x => x.TaskTypeID == 3).FirstOrDefault();

                    if (nAccessApprover != null)
                    {
                        nAccessApprover.EsculationFrequency = nAApprover;
                    }
                }

                if (context.SaveChanges() > 0)
                    success = true;
            }
            return success;
        }

        /// <summary>
        /// AC: deletes an access request person from accessrequest.aspx
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public static int DeleteMyBusinessArea(int areaID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));

                context.DeleteMyBusinessArea(areaID, result);

                return (int)result.Value;
            }

        }

        public static int DeleteBusinessAreaApprover(int personId, int areaId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                context.DeleteBusinessAreaApprover(areaId, personId, Result);
                return (int)Result.Value;
            }
        }

        public static Boolean DeletePassOfficeUserByUserId(int _dbPeopleID, int dbPeopleId, int PassOfficeId)
        {

            using (var context = new DBAccessEntities())
            {
                LocationPassOffice thePassOffice = context.LocationPassOffices.SingleOrDefault(e => e.PassOfficeID == PassOfficeId);

                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                bool isSwitzerland = false;
                int switzerlandIdValue = 0;
                string changeMakerName = string.Empty;
                string passOfficeName = string.Empty;

                if (thePassOffice != null && !String.IsNullOrEmpty(switzerlandStringId) && Int32.TryParse(switzerlandStringId, out switzerlandIdValue))
                {

                    //  Now we need to get the list of Cities for this countryId
                    List<LocationCity> theCities = context.LocationCities.Where(e => e.CountryID == switzerlandIdValue).ToList();
                    List<int> citiesList = new List<int>();

                    if (theCities != null)
                    {
                        foreach (LocationCity cityItem in theCities)
                        {
                            citiesList.Add(cityItem.CityID);
                        }
                    }

                    if (citiesList.Contains(thePassOffice.CityID.GetValueOrDefault(0)))
                    {

                        mp_User changeUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == _dbPeopleID);
                        changeMakerName = changeUser.Forename + " " + changeUser.Surname;
                        isSwitzerland = true;
                        passOfficeName = thePassOffice.Name;
                    }
                }

                var listToDelete = context.LocationPassOfficeUsers.Where(a => a.dbPeopleID == dbPeopleId && a.PassOfficeID == PassOfficeId).FirstOrDefault();// expecting only one person ideally

                context.DeleteObject(listToDelete);
                context.SaveChanges();

                //  Need to send email here if in switzerland
                if (isSwitzerland)
                {
                    mp_User deletionUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == dbPeopleId);
                    SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Deleted", "Pass Office Administrator", passOfficeName, changeMakerName);
                }

                return true;
            }
        }
        #endregion

        #region Update

        public static void UpdateSortOrder(List<AccessAreaApprover> listAccessAreaApprovers, int iClassificationID, DBAccessEnums.RequestType requestType)
        {
            //sort the order
            List<AccessAreaApprover> currentClassificationList = new List<AccessAreaApprover>();
            switch (requestType)
            {
                case DBAccessEnums.RequestType.AccessAA:
                    {
                        currentClassificationList = listAccessAreaApprovers.Where(a => a.ClassificationID == iClassificationID).OrderBy(a => a.SortOrder).ToList();
                        break;
                    }
                case DBAccessEnums.RequestType.AccessAREC:
                    {
                        currentClassificationList = listAccessAreaApprovers.Where(a => a.CorporateDivisionAccessAreaID == iClassificationID).OrderBy(a => a.SortOrder).ToList();
                        break;
                    }
            }


            byte i = 0;
            foreach (var list in currentClassificationList)
            {
                list.SortOrder = ++i;
                if (currentClassificationList.Count() == i) break;
            }
        }

        /// <summary>
        /// synchronise the list we have with the database
        /// </summary>
        /// <param name="iCreatedByUserId"></param>
        /// <param name="listPassOfficeDetails"></param>
        /// <returns></returns>
        public static Boolean UpdateAccessAreaApprovers(Int32 iCreatedByUserId, Int32 iDivisionAccessAreaID, List<AccessAreaApprover> listAccessApprovers)
        {
            using (var context = new DBAccessEntities())
            {
                vw_DivisionAccessArea rootArea = context.vw_DivisionAccessArea.SingleOrDefault(e => e.CorporateDivisionAccessAreaID == iDivisionAccessAreaID);
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                bool isSwitzerland = false;
                int switzerlandIdValue = 0;
                string changeMakerName = string.Empty;
                string accessAreaName = string.Empty;

                if (rootArea != null && !String.IsNullOrEmpty(switzerlandStringId) && Int32.TryParse(switzerlandStringId, out switzerlandIdValue) && rootArea.CountryID == switzerlandIdValue)
                {
                    mp_User changeUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == iCreatedByUserId);
                    changeMakerName = changeUser.Forename + " " + changeUser.Surname;
                    isSwitzerland = true;
                    accessAreaName = rootArea.CountryName + " - " + rootArea.CityName + " - " + rootArea.BuildingName + " - " + rootArea.AccessAreaName;
                }

                var listAccessApproverIDs = listAccessApprovers.Select(a => a.CorporateDivisionAccessAreaApproverId);
                var listDBAccessApproverIDs = context.CorporateDivisionAccessAreaApprovers.Where(a => a.CorporateDivisionAccessAreaID == iDivisionAccessAreaID && a.mp_SecurityGroupID == (Int32)DBAccessEnums.RoleType.AccessApprover).Select(a => a.CorporateDivisionAccessAreaApproverId);

                // anyone in the DB but not in our changed list then delete from the DB

                var listToDelete = context.CorporateDivisionAccessAreaApprovers.Where(a => listDBAccessApproverIDs.Contains(a.CorporateDivisionAccessAreaApproverId) && !listAccessApproverIDs.Contains(a.CorporateDivisionAccessAreaApproverId));

                foreach (var entity in listToDelete)
                {
                    //  Need to send email here if in switzerland
                    if (isSwitzerland)
                    {
                        mp_User deletionUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == entity.dbPeopleID);
                        SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Deleted", "Access Approver", accessAreaName, changeMakerName);
                    }

                    context.DeleteObject(entity);
                }

                context.SaveChanges();

                //Anyone in list but not in db then add to db
                foreach (var accessApprover in listAccessApprovers)
                {
                    if (!listDBAccessApproverIDs.Contains(accessApprover.CorporateDivisionAccessAreaApproverId))
                    {
                        int? iClassificationID = accessApprover.ClassificationID;
                        if (iClassificationID == 0)
                            iClassificationID = null;

                        if (0 == InsertAccessAreaApprover(accessApprover.CorporateDivisionAccessAreaID, accessApprover.dbPeopleID, (Int32)accessApprover.SortOrder, iClassificationID))
                            return false;

                        if (isSwitzerland)
                        {
                            mp_User addtionnUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == accessApprover.dbPeopleID);
                            SendSwitzerlandEmail(addtionnUser.Forename + " " + addtionnUser.Surname, "Added", "Access Approver", accessAreaName, changeMakerName);
                        }
                    }
                }

                // update sort orders for this area
                var listDBAccessApprovers = context.CorporateDivisionAccessAreaApprovers.Where(a => a.CorporateDivisionAccessAreaID == iDivisionAccessAreaID);

                foreach (var aaaDB in listDBAccessApprovers)
                {
                    foreach (var aaa in listAccessApprovers)
                    {
                        if (aaa.CorporateDivisionAccessAreaApproverId == aaaDB.CorporateDivisionAccessAreaApproverId)
                        {
                            aaaDB.SortOrder = aaa.SortOrder;
                        }
                    }
                }
                context.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// synchronise the list we have with the database
        /// </summary>
        /// <param name="iCreatedByUserId"></param>
        /// <param name="listPassOfficeDetails"></param>
        /// <returns></returns>
        public static Boolean UpdateAccessAreaRecertifiers(Int32 iCreatedByUserId, Int32 iDivisionAccessAreaID, List<AccessAreaApprover> listAccessRecertifiers)
        {
            using (var context = new DBAccessEntities())
            {
                //  Get the country
                vw_DivisionAccessArea rootArea = context.vw_DivisionAccessArea.SingleOrDefault(e => e.CorporateDivisionAccessAreaID == iDivisionAccessAreaID);
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                bool isSwitzerland = false;
                int switzerlandIdValue = 0;
                string changeMakerName = string.Empty;
                string accessAreaName = string.Empty;

                if (rootArea != null && !String.IsNullOrEmpty(switzerlandStringId) && Int32.TryParse(switzerlandStringId, out switzerlandIdValue) && rootArea.CountryID == switzerlandIdValue)
                {
                    mp_User changeUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == iCreatedByUserId);
                    changeMakerName = changeUser.Forename + " " + changeUser.Surname;
                    isSwitzerland = true;
                    accessAreaName = rootArea.CountryName + " - " + rootArea.CityName + " - " + rootArea.BuildingName + " - " + rootArea.AccessAreaName;
                }

                var listAccessRecertifierIDs = listAccessRecertifiers.Select(a => a.CorporateDivisionAccessAreaApproverId);
                var listDBAccessRecertifierIDs = context.CorporateDivisionAccessAreaApprovers.Where(a => a.CorporateDivisionAccessAreaID == iDivisionAccessAreaID && a.mp_SecurityGroupID == (Int32)DBAccessEnums.RoleType.AccessRecertifier).Select(a => a.CorporateDivisionAccessAreaApproverId);

                // anyone in the DB but not in our changed list then delete from the DB

                var listToDelete = context.CorporateDivisionAccessAreaApprovers.Where(a => listDBAccessRecertifierIDs.Contains(a.CorporateDivisionAccessAreaApproverId) && !listAccessRecertifierIDs.Contains(a.CorporateDivisionAccessAreaApproverId));

                foreach (var entity in listToDelete)
                {
                    //  Need to send email here if in switzerland
                    if (isSwitzerland)
                    {
                        mp_User deletionUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == entity.dbPeopleID);
                        SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Deleted", "Access Recertifier", accessAreaName, changeMakerName);
                    }

                    context.DeleteObject(entity);
                }
                context.SaveChanges();

                //Anyone in list but not in db then add to db
                foreach (var accessRecertifier in listAccessRecertifiers)
                {
                    if (!listDBAccessRecertifierIDs.Contains(accessRecertifier.CorporateDivisionAccessAreaApproverId))
                    {
                        //  Need to send email here if in switzerland
                        if (isSwitzerland)
                        {
                            mp_User addtionnUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == accessRecertifier.dbPeopleID);
                            SendSwitzerlandEmail(addtionnUser.Forename + " " + addtionnUser.Surname, "Added", "Access Recertifier", accessAreaName, changeMakerName);
                        }

                        if (0 == InsertAccessAreaRecertifier(accessRecertifier.CorporateDivisionAccessAreaID, accessRecertifier.dbPeopleID, (Int32)accessRecertifier.SortOrder)) return false;
                    }
                }

                // update sort orders for this area
                var listDBAccessApprovers = context.CorporateDivisionAccessAreaApprovers.Where(a => a.CorporateDivisionAccessAreaID == iDivisionAccessAreaID);

                foreach (var aaaDB in listDBAccessApprovers)
                {
                    foreach (var aaa in listAccessRecertifiers)
                    {
                        if (aaa.CorporateDivisionAccessAreaApproverId == aaaDB.CorporateDivisionAccessAreaApproverId)
                        {
                            aaaDB.SortOrder = aaa.SortOrder;
                        }
                    }
                }
                context.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Method to update MSPs
        /// E.Parker 
        /// 22.06.11
        /// </summary>
        /// <param name="iBusinessAreaID"></param>
        /// <param name="listRequestApprovers"></param>
        /// <returns></returns>
        public static Boolean UpdateMSPs(Int32 vendorID, List<GetMspsByVendorID_Result> listMSPs)
        {
            using (var context = new DBAccessEntities())
            {
                var listMSPsIDs = listMSPs.Select(a => a.dbPeopleID);
                // anyone in the DB but not in our changed list then delete from the DB
                var listToDelete = Director.GetMspsByVendorID(vendorID).Where(a => !listMSPsIDs.Contains(a.dbPeopleID));
                string deletelist = null;
                string addList = null;
                foreach (var entity in listToDelete)
                {
                    deletelist = deletelist + entity.dbPeopleID.ToString() + ",";
                }

                //Anyone in list but not in db then add to db
                foreach (var requestMSPs in listMSPs)
                {
                    addList = addList + requestMSPs.dbPeopleID.ToString() + ",";
                }

                //submit the changes to db
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));

                context.ModifyMSPs(addList, deletelist, vendorID, Result);
                return Convert.ToBoolean(Result.Value);

            }
        }

        public static bool UpdateDivisionRoleUsers(int divisionID, List<DivisionRoleUser> newList, int dbPeopleId)
        {
            using (var context = new DBAccessEntities())
            {
                CorporateDivision rootArea = context.CorporateDivisions.SingleOrDefault(e => e.DivisionID == divisionID);
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];
                bool isSwitzerland = false;
                int switzerlandIdValue = 0;
                string changeMakerName = string.Empty;
                string divisionName = string.Empty;

                if (rootArea != null && !String.IsNullOrEmpty(switzerlandStringId) && Int32.TryParse(switzerlandStringId, out switzerlandIdValue) && rootArea.CountryID == switzerlandIdValue)
                {
                    mp_User changeUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == dbPeopleId);
                    changeMakerName = changeUser.Forename + " " + changeUser.Surname;
                    isSwitzerland = true;
                    divisionName = rootArea.UBR + " - " + rootArea.Name;

                    //  Need to get the country id
                    LocationCountry theCountry = context.LocationCountries.SingleOrDefault(e => e.CountryID == rootArea.CountryID);

                    if (theCountry != null)
                    {
                        divisionName += " - " + theCountry.Name;
                    }
                }

                //Anyone in db but not in list then delete from db
                //Anyone in list but not in db then add to db
                IEnumerable<int> dbListIDs = GetDivisionRoleUsers(divisionID).Select(a => a.DBPeopleID);

                IEnumerable<int> listToDeleteIDs = newList.Select(a => a.DivisionRoleUserID);

                var drusToDelete = context.CorporateDivisionRoleUsers.Where(a => !listToDeleteIDs.Contains(a.DivisionRoleUserID) && a.DivisionID == divisionID);

                foreach (var entity in drusToDelete)
                {
                    context.DeleteObject(entity);

                    if (isSwitzerland)
                    {

                        mp_User deletionUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == entity.dbPeopleID);

                        if (entity.mp_SecurityGroupID == 7)
                        {
                            SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Deleted", "Division Owner", divisionName, changeMakerName);
                        }
                        else
                        {
                            SendSwitzerlandEmail(deletionUser.Forename + " " + deletionUser.Surname, "Deleted", "Division Administrator", divisionName, changeMakerName);
                        }
                    }
                }

                foreach (var divisionRoleUser in newList)
                {
                    if (!dbListIDs.Contains(divisionRoleUser.DBPeopleID))
                    {
                        InsertDivisionRoleUser(divisionID, divisionRoleUser.DBPeopleID, divisionRoleUser.SecurityGroupID);

                        if (isSwitzerland)
                        {
                            mp_User addtionnUser = context.mp_User.FirstOrDefault(e => e.dbPeopleID == divisionRoleUser.DBPeopleID);

                            if (divisionRoleUser.SecurityGroupID == 7)
                            {
                                SendSwitzerlandEmail(addtionnUser.Forename + " " + addtionnUser.Surname, "Added", "Division owner", divisionName, changeMakerName);
                            }
                            else
                            {
                                SendSwitzerlandEmail(addtionnUser.Forename + " " + addtionnUser.Surname, "Added", "Division Administrator", divisionName, changeMakerName);
                            }
                        }
                    }
                }

                context.SaveChanges();

                return true;
            }
        }

        /// <summary>
        /// synchronise the list we have with the database
        /// </summary>
        /// <param name="iCreatedByUserId"></param>
        /// <param name="listPassOfficeDetails"></param>
        /// <returns></returns>
        public static Boolean UpdatePassOfficeUsers(Int32 iCreatedByUserId, List<vw_PassOfficeDetails> listPassOfficeDetails)
        {
            using (var context = new DBAccessEntities())
            {
                var listPassOfficeUserIDs = listPassOfficeDetails.Select(a => a.PassOfficeUserID);
                var listDBPassOfficeUserIDs = context.LocationPassOfficeUsers.Select(a => a.PassOfficeUserID);

                // anyone in the DB but not in our changed list then delete from the DB

                var listToDelete = context.LocationPassOfficeUsers.Where(a => !listPassOfficeUserIDs.Contains(a.PassOfficeUserID));

                foreach (var entity in listToDelete) context.DeleteObject(entity);
                context.SaveChanges();

                //Anyone in list but not in db then add to db
                foreach (var passOfficeDetails in listPassOfficeDetails)
                {
                    if (!listDBPassOfficeUserIDs.Contains(passOfficeDetails.PassOfficeUserID))
                    {
                        if (!InsertPassOfficeUser(passOfficeDetails.dbPeopleID.Value, passOfficeDetails.PassOfficeID, iCreatedByUserId)) return false;
                    }
                }
                return true;
            }
        }

        public static bool UpdateMyAccessAreaApproverOrdering(string values)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateMyAccessAreaApproverOrdering(values, Result);

                return (bool)Result.Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnId"></param>
        /// <param name="roleId"></param>
        /// <param name="recertifier"></param>
        /// <param name="action">0=renew, 1=remove</param>
        /// <returns></returns>
        public static bool UpdateRoleCertification(int columnId, int roleId, int recertifier, int action)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateRoleCertification(columnId, recertifier, roleId, action, Result);
                return (bool)(Result.Value.ToString() == "1");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnId"></param>
        /// <param name="roleId"></param>
        /// <param name="recertifier"></param>
        /// <param name="action">0=renew, 1=remove</param>
        /// <returns></returns>
        public static bool UpdateDARoleCertification(int columnId, int recertifier, int action)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateDARoleCertification(columnId, recertifier, action, Result);
                return (bool)(Result.Value.ToString() == "1");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="corporateBusinessAreaApproverId"></param>
        /// <param name="divisionId"></param>
        /// <param name="recertifier"></param>
        /// <param name="action">0=renew, 1=remove</param>
        /// <returns></returns>
        public static bool UpdateBusinessAreaApproverCertification(int corporateBusinessAreaApproverId, int divisionId, int recertifier, int action)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateBusinessAreaApproverCertification(corporateBusinessAreaApproverId, recertifier, divisionId, action, Result);
                return (bool)(recordAffected == 1);

            }
        }

        public static bool UpdateMyAccessArea(int areaId, bool enabled, int approvalType, int approvalNumber, int frequency, String sName)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateMyAccessArea(areaId, enabled, approvalType, approvalNumber, frequency, sName, Result);

                return (bool)Result.Value;
            }
        }

        public static int UpdateBusinessArea(int businessAreaId, bool enabled, string baName, int approvalType, int approvalNumber)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateBusinessArea(businessAreaId, enabled, baName, null, approvalType, approvalNumber, Result);
                return int.Parse(Result.Value.ToString());
            }
        }

        public static bool UpdateBusinessAreaApprovalType(int divisionId, int areaId, int approvalType)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateBusinessAreaApprovalType(areaId, divisionId, approvalType, Result);
                return (bool)Result.Value;
            }
        }

        public static bool UpdateBusinessAreaStatus(int divisionId, int areaId, bool status)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int recordAffected = context.UpdateBusinessAreaStatus(areaId, divisionId, status, Result);
                return (bool)Result.Value;
            }
        }

        public static bool RejectAccessAreaRecertify(string userIds, int AccessAreaID, int DivisionID, int Approver)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Success = new ObjectParameter("Success", typeof(bool));
                int recordAffected = context.ApproveRejectRecertificationAccessAreas(Approver, userIds, false, AccessAreaID, DivisionID, Success);
                return Convert.ToBoolean(Success.Value);
            }

        }

        public static bool ApproveAccessAreaRecertify(string userIds, int DivisionID, int AccessAreaID, int Approver)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Success = new ObjectParameter("Success", typeof(bool));
                int recordAffected = context.ApproveRejectRecertificationAccessAreas(Approver, userIds, true, DivisionID, AccessAreaID, Success);
                return Convert.ToBoolean(Success.Value);
            }
        }

        public static bool SortBusinessAreaApprover(DBAccessEnums.SortDirection direction, int ApproverID, int classificationID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int recordAffected = context.SortBusinessAreaApprovers((short)direction, ApproverID, classificationID);
                return (recordAffected > 0);
            }
        }

        private static void SendSwitzerlandEmail(string additionUserName, string actionText, string changeType, string area, string actionerName)
        {
            EmailTemplate theTemplate = null;

            using (var context = new DAL.DBAccessEntities())
            {
                theTemplate = context.EmailTemplates.Single(e => e.Type == (int)DBAccessEnums.EmailType.SwitzerlandEmail);
            }

            if (theTemplate != null)
            {
                string emailBody = string.Empty;
                string subject = string.Empty;
                string switzerlandStringId = ConfigurationManager.AppSettings["SwitzerlandLocationCountryId"];

                int switzerlandId = 0;


                if (Int32.TryParse(switzerlandStringId, out switzerlandId))
                {
                    List<mp_User> theUsers = new List<mp_User>();

                    using (var context = new DAL.DBAccessEntities())
                    {
                        theUsers = context.mp_User.Where(e => e.RestrictedCountryId == switzerlandId).ToList();
                    }

                    //  Get the list of restricted users for switzerland

                    foreach (mp_User item in theUsers)
                    {
                        try
                        {
                            switch (item.LanguageID.GetValueOrDefault(0))
                            {
                                case 1:
                                    emailBody = theTemplate.Body;
                                    subject = theTemplate.Subject;
                                    break;
                                case 2:
                                    emailBody = theTemplate.GermanBody;
                                    subject = theTemplate.GermanSubject;
                                    break;
                                case 3:
                                    emailBody = theTemplate.ItalianBody;
                                    subject = theTemplate.ItalianSubject;
                                    break;
                                default:
                                    emailBody = theTemplate.Body;
                                    subject = theTemplate.Subject;
                                    break;
                            }

                            emailBody = emailBody.Replace("[$$NAME$$]", item.Forename + " " + item.Surname).Replace("[$$DATETIME$$]", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")).Replace("[$$USERNAME$$]", additionUserName)
                                .Replace("[$$ACTIONSTATUS$$]", actionText).Replace("[$$TYPE$$]", changeType).Replace("[$$AREA$$]", area).Replace("[$$ACTIONERNAME$$]", actionerName);


                            MailHelper.SendEmailWithTestCheck(item.EmailAddress, subject, emailBody, true);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }

            }
        }

        #endregion
    }
}
