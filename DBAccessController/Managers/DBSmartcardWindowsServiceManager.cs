﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using Aduvo.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    static class DBSmartcardWindowsServiceManager
    {
        #region Get methods
		public static List<vw_SmartcardServiceAction> GetPendingActions()
		{
			using (var context = new DAL.DBAccessEntities())
			{
				return
					context.vw_SmartcardServiceAction.Where(a => a.Status == (int)DBAccessEnums.SmartcardServiceActionStatus.New
										|| a.Status == (int)DBAccessEnums.SmartcardServiceActionStatus.CommunicationError).ToList();
			}
		}
        #endregion

        #region Create
        
        #endregion

        #region Remove
        
        #endregion

        #region Update
        
        #endregion
    }
}
