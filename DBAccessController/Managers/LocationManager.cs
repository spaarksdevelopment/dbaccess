﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    /// <summary>
    /// handles all activity relating to Location
    /// i.e.
    /// Regions
    /// Countries
    /// Cities
    /// Buildings
    /// Floors
    /// Access Areas
    /// 
    /// </summary>
    static class LocationManager
    {
        public static int? GetDBMovesRegionIDByCityID(int cityID)
        {
            using (var context = new DBAccessEntities())
            {
                DAL.LocationCity city = context.LocationCities.Where(a => a.CityID == cityID).SingleOrDefault();

                if (city == null)
                    return null;

                DAL.LocationCountry country = context.LocationCountries.Where(a => a.CountryID == city.CountryID).SingleOrDefault();

                if (country == null)
                    return null;

                return country.DBMovesRegionID;
            }
        }

        public static int? GetDBMovesRegionIDByBuildingID(int buildingID)
        {
            using (var context = new DBAccessEntities())
            {
                DAL.LocationBuilding building = context.LocationBuildings.Where(a => a.BuildingID == buildingID).SingleOrDefault();

                if (building == null)
                    return null;

                return GetDBMovesRegionIDByCityID(building.CityID);
            }
        }

        public static int? GetDBMovesLocationIDByBuildingID(int buildingID)
        {
            using (var context = new DBAccessEntities())
            {
                DAL.LocationBuilding building = context.LocationBuildings.Where(a => a.BuildingID == buildingID).SingleOrDefault();

                if (building == null)
                    return null;

                return building.DBMovesLocationID;
            }
        }

        public static int? GetDBMovesLocationIDByFloorID(int floorID)
        {
            using (var context = new DBAccessEntities())
            {
                DAL.LocationFloor floor = context.LocationFloors.Where(a => a.FloorID == floorID).SingleOrDefault();

                if (floor == null)
                    return null;
                
                return GetDBMovesLocationIDByBuildingID(floor.BuildingID);
            }
        }

        #region Map Data

        public static List<DAL.WorldMapData> GetMapData(int LanguageID)
        {
            List<DAL.WorldMapData> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<DAL.WorldMapData>)context.GetWorldMapData(LanguageID).ToList();  
            }

            return data;
        }
            

        #endregion

        #region Region

        public static List<DAL.LocationRegion> GetRegions()
        {
            return GetRegions(true);
        }

        public static List<DAL.LocationRegion> GetRegions(bool EnabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationRegions
                        where i.Enabled == EnabledOnly
                        orderby i.Name
                        select i;
                List<DAL.LocationRegion> items = q.ToList();
                return items;

            }

        }

        /// <summary>
        /// Method to return a single region based on ID
        /// E.Parker 
        /// 13.04.11
        /// </summary>
        /// <param name="RegionID"></param>
        /// <returns></returns>
        public static DAL.LocationRegion GetRegion(int RegionID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationRegions
                        where i.RegionID == RegionID
                        select i;

                List<DAL.LocationRegion> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;

            }
        }

        /// <summary>
        /// method to edit a region
        /// written with add functionality just not turnedon
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="region"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public static bool CreateRegion(DAL.LocationRegion region, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.CreateEditRegion(region.RegionID, region.Enabled, isNew, region.Name, region.Code, Result);

                return Convert.ToBoolean(Result.Value);
            }
        }

        #endregion

        #region Country

        public static List<DAL.LocationCountry> GetCountries()
        {
            return GetCountries(null, true);
        }

        public static List<DAL.LocationCountry> GetCountries(int RegionId)
        {
            return GetCountries(RegionId, true);
        }

        public static List<DAL.LocationCountry> GetCountries(int? RegionId, bool EnabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationCountries
                        where i.Enabled == EnabledOnly
                        orderby i.Name
                        select i;
                if (RegionId.HasValue)
                {
                    q = from i in context.LocationCountries
                            where i.Enabled == EnabledOnly
                            where i.RegionID == RegionId.Value
                            orderby i.Name
                            select i;
                }
                 
                List<DAL.LocationCountry> items = q.ToList();
                
                return items;

            }

        }

        public static DAL.LocationCountry GetCountry(int CountryId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationCountries
                        where i.CountryID == CountryId
                        select i;
               
                List<DAL.LocationCountry> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;

            }

        }

        public static List<DAL.ControlSystem> GetControlSystems()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.ControlSystems
                        where i.Enabled == true
                        select i;

                List<DAL.ControlSystem> items = q.ToList();

                return items;
            }

        }

        public static List<DAL.LocationCountry> GetCountriesByDbPeopleID(int dbPeopleID,int? languageID)
        {
            List<DAL.LocationCountry> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = context.GetCountriesByUserId(dbPeopleID, languageID).ToList();
            }

            return data;
        }

        /// <summary>
        /// method to create a country 
        /// E.Parker 
        /// 12.04.11
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public static bool CreateCountry(DAL.LocationCountry country, bool IsNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.CreateEditCountry(country.CountryID, country.Name, country.Enabled, IsNew, country.RegionID, country.Code, country.PassOfficeID, country.CostCentreCode, Result);

                return Convert.ToBoolean(Result.Value);
            }
        }

        #endregion

        #region City

        /// <summary>
        /// method to create a city 
        /// E.Parker
        /// 12.04.11
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public static bool CreateCity(DAL.LocationCity city, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.CreateEditCity(city.CountryID, city.Name, city.Code, city.Enabled, isNew, city.CityID, city.TimeZoneID, Result);

                return Convert.ToBoolean(Result.Value);
            }
        }


        public static List<DAL.LocationCity> GetCities()
        {
            return GetCities(null, true);
        }

        public static List<DAL.LocationCity> GetCities(int CountryId)
        {
            return GetCities(CountryId, true);
        }

        public static List<DAL.LocationCity> GetCities(int? CountryId, bool EnabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationCities
                        where i.Enabled == EnabledOnly
                        orderby i.Name
                        select i;
                if (CountryId.HasValue)
                {
                    q = from i in context.LocationCities
                        where i.Enabled == EnabledOnly
                        where i.CountryID == CountryId.Value
                        orderby i.Name
                        select i;
                }

                List<DAL.LocationCity> items = q.ToList();

                return items;

            }

        }

        public static DAL.LocationCity GetCity(int CityId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationCities
                        where i.CityID == CityId
                        select i;

                List<DAL.LocationCity> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;
            }
        }

        public static DAL.LocationCity GetCityFromAccessArea(int accessAreaID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var aa = context.AccessAreas.Where(a => a.AccessAreaID == accessAreaID).SingleOrDefault();

                if (aa == null)
                    return null;

                var building = context.LocationBuildings.Where(a => aa.BuildingID == a.BuildingID).SingleOrDefault();

                if (building == null)
                    return null;

                return context.LocationCities.Where(a => building.CityID == a.CityID).SingleOrDefault();
            }
        }

        /// <summary>
        /// Method to get all timezones
        /// </summary>
        /// <returns></returns>
        public static List<DAL.TimeZone> GetAllTimeZones()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.TimeZones
                        orderby i.Name
                        select i;

                List<DAL.TimeZone> items = q.ToList();
                return items;
                              
            }
        }

        /// <summary>
        /// method to bring back the admin locations in a union select 
        /// E.Parker 11.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.GetAdminLocationsData> GetAdminLocationsforList(int LanguageId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.GetAdminLocations(LanguageId)
                        orderby i.name
                        select i;   

                List<GetAdminLocationsData> locations = q.ToList();
                return locations;

            }
        }

        /// <summary>
        /// method to bring back the admin floors in a union select
        /// E.Parker 13.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.AdminFloors> GetAdminFloors()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.GetAdminFloors()
                        orderby i.RegionName, i.CountryName, i.CityName,i.BuildingName,i.FloorName, i.ParentID
                        select i;
                List<AdminFloors> floors = q.ToList();
                return floors;
            }
        }

        /// <summary>
        /// method to return an access area's details
        /// E.Parker
        /// 27.04.11
        /// </summary>
        /// <param name="accessArea"></param>
        /// <returns></returns>
        public static List<DAL.vw_AccessArea> GetAccessAreasForCountry(int countryId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from br in context.vw_AccessArea
                        where br.CountryID == countryId
                        select br;

                List<DAL.vw_AccessArea> areas = q.ToList();
                return areas;
            }
        }

        #endregion

        #region Building

        /// <summary>
        /// method to add a building to the locationbuildings collection
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="building"></param>
        /// <returns></returns>
        public static bool CreateBuilding(DAL.LocationBuilding building, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));

                if (building.DBMovesLocationID == 0)
                    building.DBMovesLocationID = null;

                int recordAffected = context.CreateEditBuildings(building.BuildingID, building.Name, building.Enabled, isNew, building.CityID, building.StreetAddress, building.LandlordID,building.VisitorControlSystem, building.DBMovesLocationID, building.PassOfficeID, Result);

                return Convert.ToBoolean(Result.Value);
            }
            
        }

        public static List<DAL.LocationBuilding> GetBuildings()
        {
			return GetBuildings(null, true, false);
        }

        public static List<DAL.LocationBuilding> GetBuildings(int CityId)
        {
			return GetBuildings(CityId, true, false);
        }

		public static List<LocationBuilding> GetBuildings(int? CityId, bool EnabledOnly)
		{
			return GetBuildings(CityId, EnabledOnly, false);
		}

		public static List<LocationBuilding> GetBuildings(int? cityID, bool enabledOnly, bool hasApprovers, bool hasAccessAreaOnly = false)
		{
			using (var context = new DBAccessEntities())
			{
				List<LocationBuilding> items = null;

				if (hasApprovers)
				{
					var q =
						(
							from lb in context.LocationBuildings
                            join aa in context.AccessAreas on lb.BuildingID equals aa.BuildingID
                            join daa in context.CorporateDivisionAccessAreas on aa.AccessAreaID equals daa.AccessAreaID
							where
								(!enabledOnly || lb.Enabled == enabledOnly)
								&&
                                daa.Enabled
								&&
								(!cityID.HasValue || lb.CityID == cityID.Value)
							orderby lb.Name
							select lb
						).Distinct();

					items = q.ToList();
				}
				else
				{
					var q = from lb in context.LocationBuildings
							where
								(!enabledOnly || lb.Enabled == enabledOnly)
								&&
								(!cityID.HasValue || lb.CityID == cityID.Value)
							orderby lb.Name
							select lb;

					items = q.ToList();
				}

				if (hasAccessAreaOnly)
				{
				    List<int> buildingIDs = items.Select(a => a.BuildingID).Distinct().ToList();

				    foreach (var building in items)
				    {
				        int numFloors = LocationManager.GetFloors(building.BuildingID, true, true, true).Count();

				        if (numFloors == 0)
				            buildingIDs.Remove(building.BuildingID);

				        items = items.Where(a => buildingIDs.Contains(a.BuildingID)).Distinct().ToList();;
				    }
				}

				return items;
			}
		}

        /// <summary>
        /// Method to return a specific building based on ID
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="buildingId"></param>
        /// <returns></returns>
        public static DAL.LocationBuilding GetBuilding(int buildingId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationBuildings
                        where i.BuildingID == buildingId
                        select i;

                List<DAL.LocationBuilding> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;

            }

        }

       
        #endregion

        #region Pass Office

		public static List<GetAllPassOffices> GetAllPassOfficesEnabled(int? languageID)
		{
			using (var context = new DAL.DBAccessEntities())
			{
                var q = from vm in context.GetAllPassOffices(languageID) where vm.Name != ""
                        select vm;
				List<DAL.GetAllPassOffices> passOffices = q.ToList();

				return passOffices;
			}
		}

		public static List<DAL.LocationPassOffice> GetPassOffices()
        {
			return GetPassOffices(true);
        }

		public static List<DAL.LocationPassOffice> GetPassOffices(bool EnabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {

				var q = from po in context.LocationPassOffices
						where po.Enabled == EnabledOnly
						select po;
							
				
				//var q = from vm in	context.GetAllPassOffices(languageID) select vm;
						
                List<DAL.LocationPassOffice> passOffices = q.ToList();
                
                return passOffices;

            }

        }

        public static List<DAL.LocationPassOffice> GetAdminPassOffices()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                IQueryable<LocationPassOffice> passOffices = from po in context.LocationPassOffices
                       select po;
                passOffices = ((ObjectQuery<LocationPassOffice>)passOffices).Include("LocationCity");
                passOffices = ((ObjectQuery<LocationPassOffice>)passOffices).Include("LocationCountries");

              
                return passOffices.ToList();             
            }
        }

        /// <summary>
        /// Method to return the visitor control systems
        /// E.Parker
        /// 28.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.ControlSystem> GetVistiorControlSystems()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from c in context.ControlSystems
                join o in context.ControlSystemTypeLookups 
                        on c.ControlSystemTypeID equals o.ControlSystemTypeId
                where o.IsVisitorManagement == true
                select c;

                List<DAL.ControlSystem> items = q.ToList();
                return items;
            }
        }

        /// <summary>
        /// Method to return external landlords to assign to a building
        /// E.Parker
        /// 28.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.ControlLandlord> GetLandlords()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from vm in context.ControlLandlords
                        select vm;
                List<DAL.ControlLandlord> items = q.ToList();
                return items;
            }
        }

        /// <summary>
        /// Method to return a landlord based on an ID
        /// E.Parker
        /// 13.05.11
        /// </summary>
        /// <param name="landlordID"></param>
        /// <returns></returns>
        public static DAL.ControlLandlord GetLandLord(int landlordID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from vw in context.ControlLandlords
                        where vw.LandlordID == landlordID
                        select vw;

                List<DAL.ControlLandlord> landlord = q.ToList();

                if (landlord.Count > 0)
                    return landlord[0];
                else
                    return null;               
            }
        }


        /// <summary>
        /// Method to return a pass office based on ID
        /// E.PArker 15.04.11
        /// </summary>
        /// <param name="passOfficeID"></param>
        /// <returns></returns>
        public static DAL.LocationPassOffice GetPassOffice(int passOfficeID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from po in context.LocationPassOffices
                        where po.PassOfficeID == passOfficeID
                        select po;

                List<DAL.LocationPassOffice> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;
            }
        }

        /// <summary>
        /// Method to return a pass office based on ID
        /// E.PArker 15.04.11
        /// </summary>
        /// <param name="passOfficeID"></param>
        /// <returns></returns>
        public static List<DAL.LocationPassOffice> GetPassOfficeFilteredByCountryId(int CountryId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                
                var q = from po in context.LocationPassOffices
                        where po.CountryID.Value == CountryId && po.Name != ""
                        orderby po.Name ascending
                        select po;

                List<DAL.LocationPassOffice> items = q.ToList();

                return items;
                
            }
        }
        
        /// <summary>
        /// Method to return a pass office details including display name based on Country
        /// Asanka 21.05.2015
        /// </summary>
        /// <param name="languageID"></param>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public static List<GetAllPassOffices> GetEnabledPassOfficeDetailsByCountryID(int? languageID, int CountryId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from vm in context.GetAllPassOffices(languageID)
                        where vm.Name != "" && vm.CountryID == CountryId
                        select vm;
                List<DAL.GetAllPassOffices> passOffices = q.ToList();

                return passOffices;
            }
        }

        #endregion

        #region Floor

        /// <summary>
        /// method to add a floor to the locationFloors collection
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="floor"></param>
        /// <returns></returns>
        public static bool CreateFloor(DAL.LocationFloor floor, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                
                if(floor.DBMovesLocationFloorID == 0)
                    floor.DBMovesLocationFloorID = null;

                int recordAffected = context.CreateEditFloors(floor.BuildingID, floor.Name, floor.Enabled, isNew, floor.FloorID, floor.DBMovesLocationFloorID, Result);

                return Convert.ToBoolean(Result.Value);
            }
        }

        public static List<DAL.LocationFloor> GetFloors()
        {
			return GetFloors(null, true, false);
        }

        public static List<DAL.LocationFloor> GetFloors(int BuildingId)
        {
			return GetFloors(BuildingId, true, false);
        }

		public static List<LocationFloor> GetFloors(int? BuildingId, bool EnabledOnly)
		{
			return GetFloors(BuildingId, EnabledOnly, false);
		}

		public static List<LocationFloor> GetFloors(int? buildingID, bool enabledOnly, bool hasApprovers, bool hasAccessAreasOnly = false)
		{
			using (var context = new DBAccessEntities())
			{
				List<LocationFloor> items = null;

				if (hasApprovers)
				{
					var q =
						(
							from lf in context.LocationFloors
							join aa in context.AccessAreas on lf.FloorID equals aa.FloorID
							join daa in context.CorporateDivisionAccessAreas on aa.AccessAreaID equals daa.AccessAreaID
							where
								(!enabledOnly || lf.Enabled == enabledOnly)
								&&
								daa.Enabled
								&&
								lf.BuildingID == buildingID
							orderby lf.Name
							select lf
							);

					if (hasAccessAreasOnly)
					{
						items = q.Distinct().ToList();

						List<int> floorIDs = items.Select(a=>a.FloorID).Distinct().ToList();

						foreach(var item in items)
						{
							if (
									(
										from aa in context.AccessAreas
										join daa in context.CorporateDivisionAccessAreas on aa.AccessAreaID equals daa.AccessAreaID
										where
											aa.FloorID == item.FloorID
											&&
											(!enabledOnly || aa.Enabled == enabledOnly)
											&&
											daa.Enabled
											&&
											(
												(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Regular)
												||
												(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Restricted)
											)
										orderby aa.Name
										select aa.AccessAreaID
									).Count() == 0
								)
								floorIDs.Remove(item.FloorID);
						}

						q = q.Where(a => floorIDs.Contains(a.FloorID));

						items = q.Distinct().ToList();
					}
					else
						items = q.Distinct().ToList();
				}
				else
				{
					var q = from lf in context.LocationFloors
							where
								(!enabledOnly || lf.Enabled == enabledOnly)
								&&
								lf.BuildingID == buildingID
							orderby lf.Name
							select lf;

					items = q.ToList();
				}
				return items;
			}
		}

        /// <summary>
        /// method to return a floor based on ID
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="floorID"></param>
        /// <returns></returns>
        public static DAL.LocationFloor GetFLoor(int floorID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.LocationFloors
                        where i.FloorID == floorID
                        select i;
                List<DAL.LocationFloor> items = q.ToList();

                if (items.Count > 0)
                    return items[0];
                else
                    return null;
            }
        }

        #endregion

        #region AccessArea

        public static int DeleteAccessArea(int areaId)
        {
            using (var context = new DBAccessEntities())
            {
                var result = new ObjectParameter("Result", typeof(int));

                context.DeleteAccessArea(areaId, result);

                return (int)result.Value;
            }
        }

		public static bool UpdateAccessArea(int areaId, int areaTypeId, int? floorId, int buildingId, string name, int? divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int corporateDivisionaccessAreaId,int? defaultAccessPeriod, string defaultAccessDuration)
        {
            using (var context = new DBAccessEntities())
            {
                var result = new ObjectParameter("Result", typeof(bool));

                context.UpdateAccessArea(areaId, areaTypeId, floorId, buildingId, divisionId, controlSystemId, recertPeriod, name, categoryid, enabled, corporateDivisionaccessAreaId, defaultAccessPeriod, defaultAccessDuration, result);

                return (bool)result.Value;
            }
        }

        /// <summary>
        /// ac: returns a single access area for editing
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
		public static List<string> GetAccessAreaForEditing(int areaId, string areaType, int areaTypeId)
        {
            using (var context = new DBAccessEntities())
            {                        
				//split the original for Floor and Building editing

				var s = new List<string>();
				if (areaType != "Floor" && areaTypeId != 4)
				{
						var	t = from c in context.AccessAreas
								where c.AccessAreaID == areaId
								join o in context.CorporateDivisionAccessAreas on c.AccessAreaID equals o.AccessAreaID

									into left
								from o in left.DefaultIfEmpty()
								select new
								{
									c.Name,
									c.AccessAreaTypeID,
									c.ControlSystemID,
									c.AccessAreaID,
									c.RecertPeriod,
									DivisionID = (int?)o.DivisionID,
									c.Enabled

								};


						var t2 = (from c in context.AccessAreas
								  where c.AccessAreaID == areaId
								  select c.AccessControlID).FirstOrDefault();



						//var s = new List<string>();
						var u = t.First();

						s.Add(u.Name);
						s.Add(u.AccessAreaTypeID.ToString());
						s.Add(u.ControlSystemID.ToString());
						s.Add(u.RecertPeriod.ToString());
						s.Add(u.DivisionID.ToString());



						if (t2 == null)
						{
							s.Add("-100");
						}
						else
						{

							s.Add(t2.Value.ToString());
						}
						s.Add(u.Enabled.ToString());

					//	return s;

				}
				else
				{
						var	t = from c in context.AccessAreas
									where c.AccessAreaID == areaId
									join o in context.CorporateDivisionAccessAreas on c.AccessAreaID equals o.AccessAreaID

										into left
									from o in left.DefaultIfEmpty()
									select new
									{
										c.Name,
										c.AccessAreaTypeID,
										c.ControlSystemID,
										c.AccessAreaID,
										c.RecertPeriod,
										DivisionID = (int?)o.DivisionID,
										c.Enabled,
										CorporateDivisionAccessAreaId = (int)o.CorporateDivisionAccessAreaID

									};

						var t2 = (from c in context.AccessAreas
								  where c.AccessAreaID == areaId
								  select c.AccessControlID).FirstOrDefault();



						
						var u = t.First();

						s.Add(u.Name);
						s.Add(u.AccessAreaTypeID.ToString());
						s.Add(u.ControlSystemID.ToString());
						s.Add(u.RecertPeriod.ToString());
						s.Add(u.DivisionID.ToString());



						if (t2 == null)
						{
							s.Add("-100");
						}
						else
						{

							s.Add(t2.Value.ToString());
						}
						s.Add(u.Enabled.ToString());
						s.Add(Convert.ToString(u.CorporateDivisionAccessAreaId));						
				}
                return s;
            }
        }

        public static string GetLinkedAccessAreas(int buildingId)
        {
            using (var context = new DBAccessEntities())
            {
                IQueryable<AccessArea> area = from p in context.AccessAreas
                                              where (p.BuildingID == buildingId && (p.AccessAreaType.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.General))
                                              select p;

                area = ((ObjectQuery<AccessArea>)area).Include("AccessArea1");

                StringBuilder sb = new StringBuilder();

                foreach (AccessArea e in area)
                {
                    sb.Append(e.Name + "," + e.AccessAreaID + "|");
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// AC: gets access areas for display. Allows the display of related table columns in a grid.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<AccessArea> GetAccessAreasForEditing(int id, string type)
        {
            using (var context = new DBAccessEntities())
            {
                IQueryable<AccessArea> areas;

                switch(type)
                {
                    case "Floor":
                        areas = from p in context.AccessAreas
                                where p.FloorID == id
                                                       select p;
                        break;
                    case "Building":
                        areas = from p in context.AccessAreas
                                where p.BuildingID== id && p.FloorID == null
                                                       select p;
                        break;
                    default:
                        areas = from p in context.AccessAreas
                                where p.FloorID == id
                                select p;
                        break;
                }
                
                areas = ((ObjectQuery<AccessArea>)areas).Include("ControlSystem");
                areas = ((ObjectQuery<AccessArea>)areas).Include("AccessAreaType");
               
                return areas.ToList();
            }
        }


        public static List<DBAccessController.DAL.ViewAllTasks> GetViewAllMyTasks(int nUserID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.GetViewAllTasks(nUserID)
                        select i;

                List<DBAccessController.DAL.ViewAllTasks> items = q.ToList();
                return items;
            }
        }

		public static List<DBAccessController.DAL.GetViewAllRequests> GetViewAllRequests(int nUserID, DateTime StartDate, DateTime EndDate)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				List<DBAccessController.DAL.GetViewAllRequests> q = new List<DBAccessController.DAL.GetViewAllRequests>();
				if (EndDate == DateTime.MinValue)
				{
					q = context.GetViewAllRequests(nUserID, StartDate, null).ToList();
				}
				else
				{
					TimeSpan time = new TimeSpan(0, 23, 59, 59, 999);
					EndDate = EndDate.Add(time);
					q = context.GetViewAllRequests(nUserID, StartDate, EndDate).ToList();
				}
				return q;
			}
		}

		public static List<DBAccessController.DAL.GetViewAllAdminRequests> GetViewAllAdminRequests(int nUserID, DateTime StartDate, DateTime EndDate)
        {
            using (var context = new DAL.DBAccessEntities())
            {
				List<DBAccessController.DAL.GetViewAllAdminRequests> q = new List<DBAccessController.DAL.GetViewAllAdminRequests>();
				if (EndDate == DateTime.MinValue)
				{
					 q = context.GetViewAllAdminRequests(nUserID, StartDate, null).ToList();
				}
				else
				{
					TimeSpan time = new TimeSpan(0, 23, 59, 59, 999);
					EndDate = EndDate.Add(time);
					q = context.GetViewAllAdminRequests(nUserID, StartDate, EndDate).ToList();
				}
				return q;
            }
        }

		public static string GetRequestDetails(int ncurrentUserId, int requestID, string type)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				List<string> data =  (List<string>)context.GetRequestDetails(ncurrentUserId, requestID, type).ToList(); 

				return data.FirstOrDefault();
			}
		}
       

        public static List<DBAccessController.DAL.ViewAllMyRequests> GetViewAllMyRequests(int nUserID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.GetViewAllMyRequests(nUserID)
                        select i;

                List<DBAccessController.DAL.ViewAllMyRequests> items = q.ToList();
                return items;
            }
        }

		/// <summary>
		/// returns details for specified pass office
		/// or, if iPassOfficeId is not 0, details for all pass offices
		/// </summary>
		/// <param name="psOfficeId"></param>
		/// <returns></returns>
		public static List<DBAccessController.DAL.vw_PassOfficeDetails> GetPassOfficeDetails(Int32 iPassOfficeId)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				var q =
					from
						i in context.vw_PassOfficeDetails
					where
						i.PassOfficeID == iPassOfficeId || 0 == iPassOfficeId
					select
						i;

				List<DBAccessController.DAL.vw_PassOfficeDetails> items = q.ToList();
				return items;
			}
		}

		

		 /// <summary>
        /// Get the location Pass office
        /// </summary>
        /// <returns></returns>
		public static int GetApproverEsculationFrequency(int nTaskType)
        {
            using (var context = new DAL.DBAccessEntities())
            {
				int nFrequency = (from i in context.ApprovalEsculationFrequencies
									 where i.TaskTypeID == nTaskType
									 select i.EsculationFrequency).FirstOrDefault();



				return nFrequency;
            }
        }


        /// <summary>
        /// Get the location Pass office
        /// </summary>
        /// <returns></returns>
        public static List<DBAccessController.DAL.GetPassOffice> GetPassOffice(int? LanguageID, string Language)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.GetPassOffice(LanguageID)
                        orderby i.Name ascending
                        select i;

                List<DBAccessController.DAL.GetPassOffice> items = q.ToList();

                return items;
            }
        }


        /// <summary>
        /// Gets a list of all area types eg for populating a dropdown
        /// </summary>
        /// <returns></returns>
        public static List<AccessAreaType> GetAccessAreaTypes()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.AccessAreaTypes
                        where i.Enabled == true
                        orderby i.AccessAreaTypeName
                        select i;

                List<AccessAreaType> items = q.ToList();

                return items;
            }
        }

        public static bool CheckIfControlSystemExists(int areaId, int controlsystemid)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                Boolean bval = false;

                var q = from c in context.AccessAreas
                        where c.AccessAreaID == areaId & c.ControlSystemID == controlsystemid
                        select c;
                if (q.Count() > 0)
                {
                    bval = true;
                }
                return bval;
            }
        }

        public static List<Category> GetControlSystemCategoryName(int areaid)
        {
            using (var context = new DAL.DBAccessEntities())
            {
             
                var u = (from c in context.AccessAreas
                         where c.AccessAreaID == areaid
                         select c.AccessControlID).FirstOrDefault();

                var q = from c in context.Categories
                        where c.id == u.Value
                        select c;

                               
                List<Category> items = q.ToList();

                return items;
            }
        }
            

        public static List<vw_ControlSystemCategoryName> GetAvailableCategoryName()
        {
            using (var context = new DAL.DBAccessEntities())
            {
               var q = from c in context.vw_ControlSystemCategoryName
                       orderby c.description ascending
                       select c;

                List<vw_ControlSystemCategoryName> items = q.ToList();
                return items;
            }
        }

        public static List<Category> GetAllCategoryName()
        {
            using (var context = new DAL.DBAccessEntities())
            {
               var q = from c in context.Categories
                    where !(from o in context.AccessAreas
                            where o.AccessControlID != null
                            select o.AccessControlID)
                            .Contains(c.id)
                    orderby c.description ascending               
                    select c;


                List<Category> items = q.ToList();
                return items;
            }
        } 
        
        

        public static List<AccessArea> GetAccessAreas()
        {
			return GetAccessAreas(null, true, false);
        }

        public static List<DAL.AccessArea> GetAccessAreas(int FloorId)
        {
			return GetAccessAreas(FloorId, true, false);
        }

        public static List<AccessArea> GetAccessAreas(int? FloorId, bool EnabledOnly)
        {
			return GetAccessAreas(FloorId, EnabledOnly, false);
		}

		public static List<AccessArea> GetAccessAreas(int? FloorId, bool EnabledOnly, bool HasApprovers)
        {
            using (var context = new DBAccessEntities())
            {
				List<AccessArea> items = null;

				if (HasApprovers)
				{
					var q =
						(
							from aa in context.AccessAreas
							join daa in context.CorporateDivisionAccessAreas on aa.AccessAreaID equals daa.AccessAreaID
							where
								aa.FloorID == FloorId
								&&
								(!EnabledOnly || aa.Enabled == EnabledOnly)
								&&
								daa.Enabled
								&&
								(
									(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Regular)
									||
									(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Restricted)
								)
							orderby aa.Name
							select aa
						).Distinct();

					items = q.ToList();
				}
				else
				{
					var q = from aa in context.AccessAreas
							where
								aa.FloorID == FloorId
								&&
								(!EnabledOnly || aa.Enabled == EnabledOnly)
								&&
								(
									(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Regular)
									||
									(aa.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.Restricted)
								)
							orderby aa.Name
							select aa;

					items = q.ToList();
				}
                return items;
            }
        }

		public static List<DAL.AccessArea> GetAccessAreasForBuilding(int buildingID, bool enabledOnly)
		{
			using (var context = new DAL.DBAccessEntities())
			{
				var q = context.AccessAreas.Where(a => a.BuildingID != null && a.BuildingID == buildingID);

				if (enabledOnly)
					q = q.Where(a => a.Enabled);

                //Only return access areas which are enabled in CorporateDivisionAccessAreas table
                //Don't want to return general areas here as they are not put through the request / authorisation system, only
                //included went going to access control system
                q = q.Where(a => context.CorporateDivisionAccessAreas.Where(b => b.Enabled).Select(c => c.AccessAreaID).Contains(a.AccessAreaID));

				return q.ToList();
			}
		}

        public static List<DAL.AccessArea> GetAccessAreasForFloorAndBuildingGeneralAreas(int? floorID, bool enabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int? buildingID = null;

                if (floorID.HasValue)
                {
                    var floor = context.LocationFloors.Where(a => a.FloorID == floorID).SingleOrDefault();
                    if (floor != null)
                        buildingID = floor.BuildingID;
                }

                var q = from i in context.AccessAreas
                        where i.Enabled == enabledOnly
                        where i.FloorID == floorID || (i.BuildingID != null && buildingID != null && i.BuildingID == buildingID && i.AccessAreaTypeID == (int)DBAccessEnums.AccessAreaType.General) 
                        select i;

                //Only return access areas which are enabled in CorporateDivisionAccessAreas table
                q = q.Where(a => context.CorporateDivisionAccessAreas.Where(b => b.Enabled).Select(c => c.AccessAreaID).Contains(a.AccessAreaID));

                List<DAL.AccessArea> items = q.OrderBy(a => a.Name).ToList();

                return items;
            }
        }

		public static List<DAL.AccessArea> GetAccessAreasForFloor(int? floorID, bool enabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int? buildingID = null;

                if (floorID.HasValue)
                {   
                    var floor = context.LocationFloors.Where(a => a.FloorID == floorID).SingleOrDefault();
                    if (floor != null)
                        buildingID = floor.BuildingID;
                }

                var q = from i in context.AccessAreas
                        where i.Enabled == enabledOnly
                        where i.FloorID == floorID 
                        select i;

                //Only return access areas which are enabled in CorporateDivisionAccessAreas table
                q = q.Where(a => context.CorporateDivisionAccessAreas.Where(b => b.Enabled).Select(c => c.AccessAreaID).Contains(a.AccessAreaID));

                List<DAL.AccessArea> items = q.OrderBy(a=>a.Name).ToList();

                return items;
            }
        }

        public static List<DAL.AccessArea> GetAccessAreasByDBMovesFloorID(int dbMovesFloorID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<AccessArea> accessAreas = new List<AccessArea>();

                var floorIDs = context.LocationFloors.Where(a => a.DBMovesLocationFloorID == dbMovesFloorID).Select(b=>b.FloorID);

                if (floorIDs != null)
                    accessAreas = context.AccessAreas.Where(b => b.FloorID.HasValue && floorIDs.Contains(b.FloorID.Value)).ToList();

                return accessAreas;
            }
        }

        public static DAL.AccessArea GetAccessArea(int id)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.AccessAreas.Where(a => a.AccessAreaID == id).SingleOrDefault();
            }
        }

        /// <summary>
        /// Method to return the access area details based on the RequestMasterID
        /// E.Parker
        /// 29.09.11
        /// </summary>
        /// <param name="RequestMasterID"></param>
        /// <returns></returns>
        public static DAL.AccessArea GetAccessAreaDetailsFromRequestMaster(int RequestMasterID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int? q = (from p in context.AccessRequestMasters 
                        join o in context.AccessRequestPersons on p.RequestMasterID equals o.RequestMasterID
                        join r in context.AccessRequests on o.RequestPersonID equals r.RequestPersonID
                        where p.RequestMasterID == RequestMasterID
                        select r.AccessAreaID).FirstOrDefault();

                //  Added for Zen ticket 2955
                int AccessAreaID = q.HasValue ? q.Value : 0;
                //now get the access area;
                return GetAccessArea(AccessAreaID);
                
            }

        }

        public static List<DAL.AccessArea> GetServiceAccessAreas(int? BuildingId)
        {
            return GetServiceAccessAreas(BuildingId, true);
        }

        public static List<DAL.AccessArea> GetServiceAccessAreas(int? BuildingId, bool EnabledOnly)
        {
            using (var context = new DAL.DBAccessEntities())
            {                
                var q = from i in context.AccessAreas
                    where i.Enabled == EnabledOnly
                    where i.BuildingID == BuildingId
                    where i.AccessAreaTypeID ==  (int)DBAccessEnums.AccessAreaType.Service
                    orderby i.FloorID
                    select i;

                
                
                List<DAL.AccessArea> items = q.ToList();

                return items;
            }
        }
        
        public static List<DAL.vw_AccessArea> GetVwAccessAreasByID(int?[] accessAreaIDs)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from i in context.vw_AccessArea
                        where accessAreaIDs.Contains(i.AccessAreaID)
                        orderby i.AccessAreaName
                        select i;
                

                List<DAL.vw_AccessArea> items = q.ToList();

                return items;

            }

        }

        #endregion


        #region PassOffice
        /// <summary>
        /// method to add/edit a pass office
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="floor"></param>
        /// <returns></returns>
        public static bool CreatePassOffice(DAL.LocationPassOffice passOffice, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.CreateEditPassOffice(passOffice.PassOfficeID, passOffice.Enabled, isNew, passOffice.Name, passOffice.Code, passOffice.Address, passOffice.Email, passOffice.Telephone,passOffice.CountryID, passOffice.CityID, passOffice.EmailEnabled, Result);

                return Convert.ToBoolean(Result.Value);
            }
        }

        #endregion

        #region Landlords
        /// <summary>
        /// Method to create a new or edit existing landlords
        /// E.Parker
        /// 13.05.11
        /// </summary>
        /// <param name="landlord"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public static bool CreateEditLandlords(DAL.ControlLandlord landlord, bool isNew)
        {
            using (var context = new DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.CreateEditLandlords(landlord.LandlordID, landlord.Name, landlord.Address, landlord.Telephone, landlord.Email, landlord.RequiresPhoto, landlord.RequiresMiFare, landlord.RequiresPhoto, isNew, Result);
                return Convert.ToBoolean(Result.Value);
            }
        }
        #endregion

        #region Classifications
        /// <summary>
        /// Method to return all classifications that are enabled
        /// E.Parker 24.05.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.GetClassifications> GetClassifications(int? languageID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
				var q = context.GetClassifications(languageID, true);
						
				List<DAL.GetClassifications> classes = q.ToList();
                return classes;
            }
        }


        /// <summary>
        /// Method to return the classification text based on a classification ID
        /// E.Parker
        /// 13.10.11
        /// </summary>
        /// <param name="classificationID"></param>
        /// <returns></returns>
        public static string GetClassificationText(int classificationID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = (from i in context.HRClassifications
                        where i.HRClassificationID == classificationID
                        orderby i.Classification
                        select i.Classification).FirstOrDefault();
               
                return q.ToString();
            }
        }
        #endregion



		
	}
}
