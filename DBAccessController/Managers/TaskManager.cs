﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    class TaskManager
    {
        public static DAL.Task GetTask(int taskId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                var q = from t in context.Tasks
                        where t.TaskId == taskId
                        select t;

                return q.First();
            }
        }

        public static string GetTaskDetails(int taskId)
        {
            List<string> data;

            using (var context = new DAL.DBAccessEntities())
            {
                data = (List<string>)context.GetTaskDetails(taskId).ToList();
            }

            return data.FirstOrDefault();
        }

        public static int UpdateTaskInfo(int TaskId, string strComment, DateTime? dtEndDate, DateTime? dtStartDate)
        {
            int i = 0;
            using (var context = new DAL.DBAccessEntities())
            {
                DAL.Task tsk = context.Tasks.Single(p => p.TaskId == TaskId);

                DAL.AccessRequest arq = context.AccessRequests.Single(p => p.RequestID == tsk.RequestId);

                tsk.TaskComment = strComment;
                arq.EndDate = dtEndDate;
                arq.StartDate = dtStartDate;
                i = context.SaveChanges();
            }
            return i;
        }

        public static List<DAL.TaskStatusLookup> GetTaskStatusLookup()
        {
            List<DAL.TaskStatusLookup> taskType;
            using (var context = new DAL.DBAccessEntities())
            {
                var items = from t in context.TaskStatusLookups
                            where t.TaskStatusId != 4
                            select t;

                return taskType = items.ToList();
            }
        }

        public static List<DAL.GetTaskStatusLookups> GetTaskStatusLookup(int? languageID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.GetTaskStatusLookups(languageID).ToList();
            }
        }

        public static List<DAL.TaskSearchOption> GetTaskSearchOption()
        {
            List<DAL.TaskSearchOption> taskSearchType;
            using (var context = new DAL.DBAccessEntities())
            {
                var items = from t in context.TaskSearchOptions
                            select t;

                return taskSearchType = items.ToList();
            }
        }

        public static List<DAL.GetTaskSearchOptions> GetTaskSearchOption(int? languageID)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return context.GetTaskSearchOptions(languageID).ToList();
            }
        }

        public static List<DAL.vw_MyTasks> GetMyTasks(int dbPeopleId, int taskstatusId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.vw_MyTasks> data;
                if (taskstatusId > 0)
                {
                    var q = from t in context.vw_MyTasks
                            where t.dbPeopleId == dbPeopleId
                            where t.TaskStatusId == taskstatusId
                            select t;
                    data = q.ToList();
                }
                else
                {
                    var q = from t in context.vw_MyTasks
                            where t.dbPeopleId == dbPeopleId
                            select t;
                    data = q.ToList();
                }

                return data;
            }
        }

        public static List<DAL.vw_MyTasksDetails> GetMyTasksDetails(int dbPeopleId, int taskstatusId, int searchOption, string searchValue)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.vw_MyTasksDetails> data = null;
                return data = context.GetMyTaskDetails(dbPeopleId, taskstatusId, searchValue, searchOption).ToList();
            }
        }

        public static List<DAL.AllDATasks> GetAllDATasks(int dbPeopleId, int taskstatusId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.AllDATasks> data = context.GetAllDATasks(dbPeopleId, taskstatusId).ToList();
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskType"></param>
        /// <param name="userId">ID of the person for whom the task is being created</param>
        /// <param name="createdBy">ID of the person creating the task</param>
        /// <param name="areaId">Business Area Id or Access Area Id</param>
        /// <returns></returns>
        public static int InsertTask(DBAccessEnums.TaskType taskType, int userId, int createdBy, int? divisionId, int? areaId)
        {
            int? businessAreaId = (taskType == DBAccessEnums.TaskType.AcceptBARole) ? areaId : null;
            int? accessAreaId = (taskType == DBAccessEnums.TaskType.AcceptAARole) || (taskType == DBAccessEnums.TaskType.AcceptARECRole) ? areaId : null;

            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int recordAffected = context.InsertTask((int)taskType, userId, createdBy, divisionId, null, businessAreaId, accessAreaId, null, null, Result);
                return (int)Result.Value;
            }
        }

        ///// <summary>
        ///// Called by a user when manging his/her tasks. Fires off an email to the task assigner.
        ///// </summary>
        ///// <param name="taskId"></param>
        ///// <param name="taskStatus"></param>
        ///// <returns></returns>
        //public static int UpdateTaskStatus(int taskId, DBAccessEnums.TaskStatus taskStatus, int userId, string comment)
        //{
        //    using (var context = new DAL.DBAccessEntities())
        //    {
        //        ObjectParameter Result = new ObjectParameter("Result", typeof(int));
        //        int r = context.UpdateTaskStatus(taskId, (int)taskStatus, userId, comment, Result);

        //        return (int)Result.Value;
        //    }
        //}


        /// <summary>
        /// Called by a user when manging his/her tasks. Fires off an email to the task assigner.
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="taskStatus"></param>
        /// <returns></returns>
        public static int UpdateTaskStatusByType(int taskId, DBAccessEnums.TaskStatus taskStatus, int userId, string comment, int taskTypeId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));
                int r = 0;

                switch (taskTypeId)
                {
                    //  CreatePass
                    case 1:
                        r = context.up_UpdateTaskStatus_TaskType1(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  ApproveAccess Or Approve Smart Card Request
                    case 3:
                    case 18:
                        r = context.up_UpdateTaskStatus_TaskType3(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  AcceptBARole
                    case 4:
                        r = context.up_UpdateTaskStatus_TaskType4(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  AcceptAARole
                    //  AcceptARECRole
                    case 5:
                    case 6:
                        r = context.up_UpdateTaskStatus_TaskType5and6(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  ManualAddAccess
                    case 7:
                        r = context.up_UpdateTaskStatus_TaskType7(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  AcceptDARole
                    //  AcceptDORole
                    case 8:
                    case 10:
                        r = context.up_UpdateTaskStatus_TaskType8and10(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  AcceptPORole
                    case 9:
                        r = context.up_UpdateTaskStatus_TaskType9(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  AcceptMSPRole
                    case 11:
                        r = context.up_UpdateTaskStatus_TaskType11(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  DeactivateBadge
                    case 12:
                        r = context.up_UpdateTaskStatus_TaskType12(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  Revoke
                    case 13:
                        r = context.up_UpdateTaskStatus_TaskType13(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  Reactivate
                    case 14:
                        r = context.up_UpdateTaskStatus_TaskType14(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    //  Change Start Date
                    case 15:
                        r = context.up_UpdateTaskStatus_TaskType15(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    case (int)DBAccessEnums.TaskType.SuspendAccess:
                        r = context.up_UpdateTaskStatus_TaskType16(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    case (int)DBAccessEnums.TaskType.UnsuspendAccess:
                        r = context.up_UpdateTaskStatus_TaskType17(taskId, (int)taskStatus, userId, comment, Result);
                        break;
                    default:
                        r = 0;
                        break;
                }

                return (int)Result.Value;
            }
        }

        /// <summary>
        /// Called by a user when manging his/her tasks. Fires off an email to the task assigner.
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="taskStatus"></param>
        /// <returns></returns>
        public static Int32 GetMyTasksCount(int dbPeoplId, int TaskStatusUd)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(int));

                context.GetMyTasksCount(dbPeoplId, TaskStatusUd, Result).FirstOrDefault();

                return Convert.ToInt32(Result.Value);
            }
        }

        public static int GetMiFareNumber(string MiFareNumber)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                int? r = (from b in context.HRPersonBadges
                          where b.MiFareNumber == MiFareNumber
                          select b.MiFareNumber).Count();
                return (int)r;
            }
        }

        public static DAL.HRPerson GetMiFareNumberAssignment(string MiFareNumber)
        {
            DAL.HRPerson theUser = null;

            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.HRPersonBadge> objectList = context.HRPersonBadges.Where(e => e.MiFareNumber == MiFareNumber).ToList();

                if (objectList != null && objectList.Count > 0)
                {
                    int dbPeopleId = objectList[0].dbPeopleID;

                    List<DAL.HRPerson> userList = context.HRPersons.Where(e => e.dbPeopleID == dbPeopleId).ToList();

                    if (userList != null && userList.Count > 0)
                    {
                        theUser = userList[0];
                    }
                }

                if (theUser != null)
                {
                    return theUser;
                }
                else
                {
                    return null;
                }
            }
        }

        public static Dictionary<string, string> GetMiFareNumberAssignmentDate(string MiFareNumber)
        {
            Dictionary<string, string> toReturn = new Dictionary<string, string>();

            using (var context = new DAL.DBAccessEntities())
            {
                List<DAL.vw_BadgeRequest> objectList = context.vw_BadgeRequest.Where(e => e.MiFareNumber == MiFareNumber).ToList();

                if (objectList != null && objectList.Count > 0)
                {
                    int dbPeopleId = objectList[0].CreatedByID.GetValueOrDefault(0);

                    List<DAL.HRPerson> userList = context.HRPersons.Where(e => e.dbPeopleID == dbPeopleId).ToList();

                    if (userList != null && userList.Count > 0)
                    {
                        toReturn.Add(userList[0].Forename + ' ' + userList[0].Surname, objectList[0].Created.ToString("dd-MM-yyyy"));
                    }
                }

                if (toReturn != null)
                {
                    return toReturn;
                }
                else
                {
                    return null;
                }
            }
        }

        public static string GetRequesterEmailAddress(int RequestPersonId)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                ObjectParameter Result = new ObjectParameter("Result", typeof(bool));
                int? nCreatedById = (from t in context.AccessRequestPersons
                                     where t.RequestPersonID == RequestPersonId
                                     select t.CreatedByID).Single();

                string strEmailAddress = (from t in context.HRPersons
                                          where t.dbPeopleID == nCreatedById
                                          select t.EmailAddress).Single();

                return strEmailAddress;
            }
        }

        public static void CreatingPendingTaskUpdate(int? taskId, DBAccessEnums.TaskStatus taskStatus, int _dbPeopleID, string comment)
        {
            using (var context = new DAL.DBAccessEntities())
            {
                context.CreatePendingTaskUpdate((int?)taskId, (int?)taskStatus, (int?)_dbPeopleID, comment);
            }
        }
    }
}
