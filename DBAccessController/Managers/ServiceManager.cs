﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    static class ServiceManager
    {
        public static Guid GetServiceGuid()
        {
            using (var context = new DAL.DBAccessEntities())
            {
                return new Guid(context.AdminValues.Where(a => a.Key == "ServiceGuid").First().Value);
            }
        }
    }
}
