﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Managers
{
    /// <summary>
    /// handles all activity relating to the external control system manager
    /// i.e.
    /// 
    /// get persons access (local copy or live copy)
    /// 
    /// Add Access
    /// Remove Access
    /// Update Access
    /// 
    /// Create Pass
    /// Cancel Pass
    /// 
    /// Update Pass Details
    /// 
    /// </summary>
    static class ControlSystemManager
    {

    }
}
