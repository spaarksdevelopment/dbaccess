﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Resources;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        public bool SubmitFeedback(int iRating, String sSuggestions)
        {
            bool success;

            try
            {
                success = AccessRequestManager.SubmitFeedback(_dbPeopleID, iRating, sSuggestions);
            }
            catch (Exception ex)
            {
                success = false;
                HandleException(ex);
            }

            return success;
        }

        public bool SubmitRecommendation(string sMessage, string sFilePath, string sEmailAddress)
        {
            bool success;

            try
            {
                var hrPersonSubmitter = GetPersonById(_dbPeopleID);

                int languageId = PersonManager.GetPersonLanguage(null, sEmailAddress);

                var emailTemplateRecommendation =
                    EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.Recommendation, languageId);

                emailTemplateRecommendation.Body = emailTemplateRecommendation.Body.Replace("[$$USER_NAME$$]",
                    hrPersonSubmitter.Forename + ' ' + hrPersonSubmitter.Surname);
                emailTemplateRecommendation.Body = emailTemplateRecommendation.Body.Replace("[$$HTTP_ROOT$$]",
                    ConfigurationManager.AppSettings["HTTP_ROOT"]);
                emailTemplateRecommendation.Body = emailTemplateRecommendation.Body.Replace("[$$FILE_PATH$$]",
                    sFilePath);

                int nrval = EmailManager.InsertBatchEmailLog(sEmailAddress, string.Empty, string.Empty,
                    emailTemplateRecommendation.Subject, emailTemplateRecommendation.Body, false, new Guid(),
                    emailTemplateRecommendation.Type);

                success = (nrval > 0);
            }
            catch(Exception ex)
                {
                success = false;
                HandleException(ex);
                }                
            return success;
            }



        public bool SubmitRequestGeneric(int iRequestMasterId, int? vendorId = null)
            {
            bool bSuccess;

            try
            {
                bSuccess = AccessRequestManager.SubmitMasterRequestGeneric(_dbPeopleID, iRequestMasterId, vendorId);
            }
            catch (Exception ex)
            {
                bSuccess = false;
                HandleException(ex);
            }

            return bSuccess;
        }

        public bool SubmitDivisionRoleRequests(int divisionId)
        {
            var success = true;

            try
            {
                if (AccessRequestManager.DoWeHaveRoleRequestsToSubmit(_dbPeopleID, divisionId))
                    return AccessRequestManager.SubmitDivisionRoleRequests(_dbPeopleID, divisionId);
            }
            catch (Exception ex)
            {
                success = false;
                HandleException(ex);
            }

            return success;
        }

        public bool SubmitRequest(int? requestMasterId, string comment, string userEmail, string userForename,
            ref int? errorCode)
        {
            var success = false;

            if (!CheckRequestMaster(requestMasterId))
            {
                FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
                return false;
            }

            try
            {
                //add the comment
                if (!string.IsNullOrWhiteSpace(comment))
                {
                    success = AccessRequestManager.UpdateMasterRequestComment(_dbPeopleID, requestMasterId.Value,
                        comment);
                    if (!success)
                        throw new Exception("Request Master could not be updated");
            }

                //submit the master request
                success = AccessRequestManager.SubmitMasterRequest(_dbPeopleID, requestMasterId);


                //update the badges
                if (success)
                    AccessRequestManager.UpdateAccessRequestBadgeIDs(requestMasterId);
            }
            catch (EntityCommandExecutionException ex)
            {
                success = false;

                if (ex.InnerException != null && ex.InnerException is SqlException)
                {
                    var sqlEx = ex.InnerException as SqlException;

                    HandleException(sqlEx);

                    errorCode = sqlEx.State;
            }
                else
                    HandleException(ex);
        }
            catch (Exception ex)
            {
                success = false;
                HandleException(ex);
            }

            if (success)
        {
            try
            {
                    Send_AccessRequestEmails(requestMasterId, userForename, userEmail);
            }
            catch (Exception ex)
            {
                    HandleException(ex);
                    return false;
            }
        }
            return success;
        }

        private static void Send_AccessRequestEmails(int? requestMasterId, string userForename, string userEmail)
        {
            //Email requestor and applicants in the requestor
            //sending emails to the people in the requet list
            var emailRecipients = GetAccessRequestPersons(Helper.SafeInt(requestMasterId));

            var strReceipient = string.Empty;
            var strApplicantBuild = string.Empty;
            var strAccessAreaBuild = string.Empty;
            var strRequestId = string.Empty;
            var strTaskBuild = string.Empty;


            //Build the details of Applicants on the request 
            EmailManager.BuildApplicantDetails(emailRecipients, ref strRequestId, ref strApplicantBuild,
                ref strReceipient);

            //Build the details of Access Area on the request
            EmailManager.BuildAccessAreaDetails(emailRecipients, ref strAccessAreaBuild);

            //Build the details of tasks on the request
            EmailManager.BuildTaskDetails(emailRecipients, requestMasterId, ref strTaskBuild);

            //send emails to requester and applicants on the request
            EmailManager.SendEmailTo_Requester_Applicants(strReceipient, strRequestId, userForename, strApplicantBuild,
                strAccessAreaBuild, strTaskBuild, userEmail);

            //send the email to the approvers for this request

            //sends emails to Current Approvers
            var emailRequestApprovers = GetAccessRequestApprovers(Helper.SafeInt(requestMasterId));
            EmailManager.SendEmailToApprovers(emailRequestApprovers, userForename);
        }

        public bool CancelRequest(int requestMasterId)
        {
            var success = false;

            try
            {
                success = AccessRequestManager.CancelMasterRequest(_dbPeopleID, requestMasterId);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return success;
        }

        public static string GetLanguageCode(int? currentLanguageId)
        {
            return AccessRequestManager.GetLanguageCode(currentLanguageId);
        }

        public static int? GetLanguageID(int languageId, string currentUserLanguageCode)
        {
            if (languageId > 0)
                return languageId;

            try
            {
                return GetLanguageEntityFromString(currentUserLanguageCode).LanguageId;
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static string GetEnglishVersion(string strStatus, int? languageId)
		{
			try
			{
                return AccessRequestManager.GetEnglishVersion(strStatus, languageId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        #region static get methods

        public static bool CheckRequestMaster(int? requestMasterId)
		{
            bool success;
			try
			{
                var requestMaster = AccessRequestManager.GetAccessRequestMaster(requestMasterId.Value);
                success = (requestMaster != null);
			}
            catch
			{
                //this just means request master was not valid, no need to log
                success = false;
			}

            return success;
		}
		
        public static bool CheckRequestMasterForSubmission(int? requestMasterId)
        {
            var success = false;
            try
            {
                var requestMaster = AccessRequestManager.GetAccessRequestMaster(requestMasterId.Value);

                if (requestMaster != null && requestMaster.RequestTypeID.HasValue)
		{
                    success = true;
                }
            }
            catch (Exception)
            {
                //this just means request master was not valid, no need to log
                success = false;
            }

            return success;
        }

        public static vw_AccessRequest GetLatestAccessRequest(int dbPeopleId, int accessAreaId)
        {
			try
			{
                return AccessRequestManager.GetLatestAccessRequest(dbPeopleId, accessAreaId);
			}
            catch
			{
				return null;
			}
		}

        public static ExternalSystem GetExternalSystem(int externalSystemId)
		{
			try
			{
                return AccessRequestManager.GetExternalSystem(externalSystemId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static List<vw_AccessRequestPerson> GetAccessRequestPersons(int requestMasterId)
		{
			try
			{
                return AccessRequestManager.GetAccessRequestPersons(requestMasterId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
                return null;
			}
		}

        public static List<vw_AccessRequestApprover> GetAccessRequestApprovers(int requestMasterId)
		{
			try
			{
                return AccessRequestManager.GetAccessRequestApprovers(requestMasterId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static List<int> GetRequestIds(int requestPersonId)
        {
            try
            {
                return AccessRequestManager.GetRequestIds(requestPersonId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        private static List<vw_AccessRequestApprover> GetAccessRequestApprovers(int requestMasterId, int requestId)
        {
            try
            {
                return AccessRequestManager.GetAccessRequestApprovers(requestMasterId, requestId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<vw_AccessApproverDetails> GetAccessApproverDetails(int requestPersonId)
        {
            try
            {
                return AccessRequestManager.GetAccessApproverDetails(requestPersonId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static bool CheckMultiApproval(int requestId, int taskId)
        {
            var bval = false;
            try
            {
                bval = AccessRequestManager.CheckMultiApproval(requestId, taskId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            return bval;
        }

        private static List<int[]> GetRequestMasterId(int taskId)
		{
			try
			{
                return AccessRequestManager.GetRequestMasterID(taskId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static int? GetAccessRequestPersonCount(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.GetAccessRequestPersons(requestMasterId).Count;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return null;
            }
        }

        public static int? GetAccessRequestPersonsWithoutBadgeCount(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.GetAccessRequestPersonIDsWithoutBadge(requestMasterId).Count;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        public static List<vw_BadgeRequest> GetBadgeRequests(int requestMasterId,
            DBAccessEnums.BadgeRequestIssueType issueType)
        {
            try
            {
                return AccessRequestManager.GetBadgeRequests(requestMasterId, issueType);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return null;
            }
        }

        public static List<GetAccessRequestJustificationReasons> GetAccessRequestJustificationReasons(int? languageId)
        {
            try
            {
                return AccessRequestManager.AccessRequestJustificationReason(languageId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return null;
            }
        }

        public static List<vw_AccessAreaRequest> GetAccessAreasForRequest(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.GetVwAccessAreasOnRequest(requestMasterId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);                

                return null;
            }
        }

        public static List<vw_MasterAccessRequest> GetMasterAreasForRequest(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.GetMasterAreasForRequest(requestMasterId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int? GetAccessAreasForRequestCount(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.GetVwAccessAreasOnRequest(requestMasterId).Count;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static vw_AccessRequestPerson GetRequestPerson(int requestMasterId, int dbPeopleId)
        {
            try
            {
                return AccessRequestManager.GetRequestPerson(requestMasterId, dbPeopleId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int GetPassRequestMasterId(int dbPeopleId)
        {
            try
            {
                return AccessRequestManager.GetPassRequestMasterID(dbPeopleId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                throw;
            }
        }

        public static bool DoesPendingAccessRequestExist(int dbPeopleId, int accessAreaId)
		{
		    try
		    {
                return AccessRequestManager.DoesPendingAccessRequestExist(dbPeopleId, accessAreaId);
		    }
		    catch (Exception ex)
		    {
		        LogHelper.HandleException(ex);
                throw;
		    }
		}

        public static int DoesOnlyDraftExist(int dbPeopleId, int accessAreaId)
		{
			try
			{
                return AccessRequestManager.DoesOnlyDraftExist(dbPeopleId, accessAreaId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
                throw;
			}
		}

        public static List<NewsFeed> GetNews()
        {
            try
            {
                return AccessRequestManager.GetNews();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                throw;
            }
        }

        #endregion

        #region Create

		public static void SaveNewsHtml(string newHtml)
		{
			try
			{
				AccessRequestManager.SaveNewsHtml(newHtml);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
                throw;
			}
		}
		
        public static bool CheckIfTheUsersExist(string users)
        {
            bool doUsersExist = AccessRequestManager.CheckIfTheUsersExist(users);

            return doUsersExist;
        }

        private int doCreateRequestForPerson(int dbPeopleId, ref int RequestMasterId)
		{
            return doCreateRequestForPerson(dbPeopleId, ref RequestMasterId, false);
		}

        private int doCreateRequestForPerson(int dbPeopleId, ref int RequestMasterId, Boolean bCheckBusinessArea)
        {
            //check if Request master was supplied, and create it if not
            if (RequestMasterId == 0)
                RequestMasterId = AccessRequestManager.CreateRequestMaster(_dbPeopleID);

            //create the request person
            int RequestPersonId = AccessRequestManager.CreateRequestPerson(_dbPeopleID, RequestMasterId, dbPeopleId, bCheckBusinessArea);

            return RequestPersonId;
        }

        public int CreateNewBadgeRequestForPerson(int dbPeopleId, List<int> BadgesToReplace, ref int RequestMasterId)
        {
            int success = 0;

            if (AccessRequestManager.HasPendingPassRequest(dbPeopleId, BadgesToReplace))
            {
                FailureReason = DBAccessEnums.FailureReason.PersonAlreadyOnRequest;
                return success;
            }

            if (RequestMasterId > 0)
            {
                if (!CheckRequestMaster(RequestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
                    return success;
                }
            }

            try
            {
                int RequestPersonId = doCreateRequestForPerson(dbPeopleId, ref RequestMasterId);

				//check if person is disabled
				if (RequestPersonId != -3)
				{

					//create the new badge request(s)
					bool HasBadgesToReplace = (BadgesToReplace == null) ? false : (BadgesToReplace.Count > 0);
					int RequestId = 0;

					//if badges are to be replaced, create a new request for each badge that must be replaced.
					if (HasBadgesToReplace)
					{
						foreach (int badgeId in BadgesToReplace)
						{
                            RequestId = AccessRequestManager.CreateBadgeRequest(_dbPeopleID, RequestPersonId, badgeId);
						}
					}
					else
					{
                        RequestId = AccessRequestManager.CreateBadgeRequest(_dbPeopleID, RequestPersonId, null);

					}

					if (RequestId == 0)
						throw new Exception("The Request could not be created");
                    success = 1;
				}
				else
				{
                    success = -3;
				}
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }

            return success;
        }

        public bool CreateNewBadgeRequestForPerson(int dbPeopleId, List<int> badgesToReplace, ref int requestMasterId,
            int? externalSystemRequestId = null, int? externalSystemId = null)
		{
            var success = false;

            if (AccessRequestManager.HasPendingPassRequest(dbPeopleId, badgesToReplace))
			{
				FailureReason = DBAccessEnums.FailureReason.PersonAlreadyOnRequest;
                return false;
			}

            if (requestMasterId > 0)
			{
                if (!CheckRequestMaster(requestMasterId))
				{
					FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
                    return false;
				}
			}

			try
			{
                var requestPersonId = DoCreateRequestForPerson(dbPeopleId, ref requestMasterId);

				//check if person is disabled
                if (requestPersonId != -3)
				{
					//create the new badge request(s)
                    var hasBadgesToReplace = (badgesToReplace != null) && (badgesToReplace.Count > 0);
                    var requestId = 0;

					//if badges are to be replaced, create a new request for each badge that must be replaced.
                    if (hasBadgesToReplace)
					{
                        foreach (var badgeId in badgesToReplace)
						{
                            requestId = AccessRequestManager.CreateBadgeRequest(_dbPeopleID, requestPersonId, badgeId,
                                externalSystemRequestId, externalSystemId);
						}
					}
					else
					{
                        requestId = AccessRequestManager.CreateBadgeRequest(_dbPeopleID, requestPersonId, null,
                            externalSystemRequestId, externalSystemId);
					}

                    if (requestId == 0)
						throw new Exception("The Request could not be created");

                    success = true;
		}
                }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }

            return success;
        }

        public int CreateNewRequestForDo(int iDbPeopleId, ref int iRequestMasterId, int? iDivisionId)
		{
            var bSuccess = 0;

            if (iRequestMasterId > 0)
            {
                if (!CheckRequestMaster(iRequestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
					return bSuccess;
                }
            }

            try
            {
                var iRequestPersonId = DoCreateRequestForPerson(iDbPeopleId, ref iRequestMasterId);

				//check if person is disabled
				if (iRequestPersonId != -3)
				{
                    var iRequestId = 0;

					//replace this with DO Request
					iRequestId = AccessRequestManager.CreateDORequest(_dbPeopleID, iRequestPersonId, iDivisionId);

					if (0 == iRequestId) throw new Exception("The Request could not be created");

					bSuccess = 1;
				}
				else
				{
					bSuccess = -3;
				}   
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
			return bSuccess;
        }

        public int CreateNewRequestForDivisionAdministrator(int iDbPeopleId, ref int iRequestId, ref int iRequestMasterId,
            int iDivisionId, int iBusinessAreaId)
        {
            var bSuccess = 0;

            if (iRequestMasterId > 0)
            {
                if (!CheckRequestMaster(iRequestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
					return bSuccess;
                }
            }

            try
            {
                var iRequestPersonId = DoCreateRequestForPerson(iDbPeopleId, ref iRequestMasterId);

				//check if person is disabled
				if (iRequestPersonId != -3)
				{
					//replace this with DA Request
                    iRequestId = AccessRequestManager.CreateDARequest(_dbPeopleID, iRequestPersonId, iDivisionId,
                        iBusinessAreaId);

					if (0 == iRequestId) throw new Exception("The Request could not be created");

					bSuccess = 1;
				}
				else
				{
					bSuccess = -3;
				}      
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return bSuccess;
        }

        public int CreateNewRequestForPassOffice(int dbPeopleId, ref int requestMasterId, int passOfficeId)
        {
            var success = 0;
            if (requestMasterId > 0)
            {
                if (!CheckRequestMaster(requestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
					return success;
                }
            }

            try
            {
                var requestPersonId = DoCreateRequestForPerson(dbPeopleId, ref requestMasterId);
				//check if person is disabled
                if (requestPersonId != -3)
				{
                    var requestId = 0;
					//replace this with PO Request
                    requestId = AccessRequestManager.CreatePORequest(_dbPeopleID, requestPersonId, passOfficeId);

                    if (requestId == 0)
						throw new Exception("The Request could not be created");
					success = 1;
				}
				else
				{
					success = -3;
				}   
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
			return success;
        }

        public int CreateNewRequestForAccessApprover(int dbPeopleId, ref int requestId, ref int requestMasterId,
            int iDivisionAccessAreaId, int? classificationId)
        {
            var success = 0;

            if (requestMasterId > 0)
            {
                if (!CheckRequestMaster(requestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
					return success;
                }
            }

            try
            {
                var requestPersonId = DoCreateRequestForPerson(dbPeopleId, ref requestMasterId);
				//check if person is disabled
                if (requestPersonId != -3)
				{
					//replace this with AA Request
                    requestId = AccessRequestManager.CreateAARequest(_dbPeopleID, requestPersonId, iDivisionAccessAreaId,
                        classificationId);

                    if (requestId == 0)
						throw new Exception("The Request could not be created");

					success = 1;
				}
				else
				{
					success = -3;
				}      
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
			return success;
        }

        public int CreateNewRequestForAr(int dbPeopleId, ref int requestId, ref int requestMasterId,
            int iDivisionAccessAreaId)
        {
            var success = 0;

            if (requestMasterId > 0)
            {
                if (!CheckRequestMaster(requestMasterId))
                {
                    FailureReason = DBAccessEnums.FailureReason.InvalidRequestMasterId;
					return success;
                }
            }

            try
            {
                var requestPersonId = DoCreateRequestForPerson(dbPeopleId, ref requestMasterId);
                if (requestPersonId != -3)
				{
					//replace this with AA Request
                    requestId = AccessRequestManager.CreateARRequest(_dbPeopleID, requestPersonId, iDivisionAccessAreaId);

                    if (requestId == 0)
						throw new Exception("The Request could not be created");
					success = 1;
				}
				else
				{
					success = -3;
				}      
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
			return success;
        }

        private void HandleException(Exception ex)
        {
			LoggedExceptionId = LogHelper.HandleException(ex);
			LoggedException = ex;
			FailureReason = DBAccessEnums.FailureReason.Exception;
		}

        private int DoCreateRequestForPerson(int dbPeopleId, ref int requestMasterId)
        {
            return DoCreateRequestForPerson(dbPeopleId, ref requestMasterId, false);
        }

        private int DoCreateRequestForPerson(int dbPeopleId, ref int requestMasterId, bool bCheckBusinessArea)
        {
            //check if Request master was supplied, and create it if not
            if (requestMasterId == 0)
                requestMasterId = AccessRequestManager.CreateRequestMaster(_dbPeopleID);

			//create the request person
            var requestPersonId = AccessRequestManager.CreateRequestPerson(_dbPeopleID, requestMasterId, dbPeopleId,
                bCheckBusinessArea);
			
            return requestPersonId;
        }

        public int? CreateNewRequestForTransferredPersons(int requestMasterId)
        {
            try
            {
                return AccessRequestManager.CreateNewRequestForTransferredPersons(_dbPeopleID, requestMasterId);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        #endregion

        #region Remove

        public bool DeleteAccessAreaRequest(int areaId, int masterId)
        {
            var success = false;

            try
            {
                AccessRequestManager.DeleteAccessAreaRequest(areaId, masterId);
                success = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return success;
        }

        public bool DeleteAccessRequestPerson(int requestPersonId)
        {
            var success = false;

            try
            {
                AccessRequestManager.DeleteAccessRequestPerson(requestPersonId);
                success = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return success;
        }

        public List<AccessArea> GetAccessAreaDetails(int accessAreaId)
        {
            var items = new List<AccessArea>();

            try
            {
                items = AccessRequestManager.GetAccessAreaDetails(accessAreaId);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return items;
        }

        /// <summary>
        ///     AC: removes a single access request. Called from NewPassRequest.aspx
        /// </summary>
        /// <param name="requestId"></param>
        public bool DeleteAccessRequest(int requestId)
        {
            bool success = false;

            try
            {
                success = AccessRequestManager.DeleteAccessRequest(requestId);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return success;
        }

        #endregion

		#region Update Methods

        public bool UpdateAccessJustification(int requestMasterId, int? justificationId)
		{
            var success = false;

            using (var context = new DBAccessEntities())
			{
				try
				{
                    AccessRequestManager.UpdateAccessJustification(requestMasterId, justificationId);
					success = true;
				}
				catch (Exception ex)
				{
					success = false;
					LoggedExceptionId = LogHelper.HandleException(ex);
					LoggedException = ex;
				}
			}
			return success;
		}

        public bool UpdateBadgeJustification(int requestMasterId, int? justificationId)
		{
            var success = false;

            using (var context = new DBAccessEntities())
			{
				try
				{
                    AccessRequestManager.UpdateBadgeJustification(requestMasterId, justificationId);
					success = true;
				}
				catch (Exception ex)
				{
                    LogHelper.HandleException(ex);
            }
        }
					return success;
                }

        #endregion

        #region HelperMethods

		public static ResourceManager GetResourceManager(string resourceName)
		{
			try
			{
                var strLocalizationResource = ConfigurationManager.AppSettings["LocalizationResource"];
                var resourceManager = new ResourceManager(resourceName, Assembly.Load(strLocalizationResource));
				return resourceManager;
			}
			catch (Exception ex)
			{
				 LogHelper.HandleException(ex);
				 return null;
			}	
		}

        public static string GetResourceManager(string resourceName, string resourceIdentifier)
		{
			try
			{
                ResourceManager resourceManager = GetResourceManager(resourceName);
				return resourceManager.GetString(resourceIdentifier);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}	
		}

        public static void UpdateUserLanguage(int languageId, int dbPeopleId, string userEmail)
		{
			try
			{
                AccessRequestManager.UpdateUserLanguage(languageId, dbPeopleId, userEmail);
			}
            catch (Exception ex)
            {
               LogHelper.HandleException(ex);               
            }
		}

        public static Language GetLanguageEntityFromString(string lang)
        {
            try
            {
                return AccessRequestManager.GetLanguageEnumFromString(lang);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static bool IsMultiLanguageOn()
        {
            string isMultiLanguageOn = ConfigurationManager.AppSettings["MultiLanguageOn"];
            if (string.IsNullOrWhiteSpace(isMultiLanguageOn))
                return false;

            return Convert.ToBoolean(isMultiLanguageOn);
        }
        #endregion
	}
}
