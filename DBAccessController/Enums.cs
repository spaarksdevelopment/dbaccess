﻿

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public class DBAccessEnums
    {
        public enum BadgeRequestIssueType
        {
            Any, New, Replacement
        };

        public enum DeliveryOption
        {
            Collect = 1, Contact = 2
        };

        public enum BadgeJustifcationReason
        {
            Lost = 1,
            Stolen = 2,
            Damaged = 3,
            Forgotten = 4
        }

        public enum RequestType
        {
            NewPass = 1,
            Access = 2,
            Service = 3,
            Visitor = 4,
            PassMSP = 5,
            AccessMSP = 6,
            AccessDA = 7,
            AccessAA = 8,
            AccessAREC = 9,
            AccessRA = 10,
            AccessPO = 11,
            AccessDO = 12,
            MSP = 13,
            //[Mahmoud.Ali]
            StopPass = 14,
            //[/Mahmoud.Ali]
            RevokeAccess = 16,
            ReactivateAccess = 17
        };

        public enum AccessAreaType
        {
            Regular = 1, Restricted = 2, General = 3, Service = 4, Public = 5
        };

        public enum FailureReason
        {
            Exception = 1,
            InvalidRequestMasterId = 2,
            PersonAlreadyOnRequest = 3,
            InsufficientData = 4,
            NoRequestApprovers = 5,
            RequestPersonInActive = 6
        };

        public enum EmailType
        {
            None = 0,
            ToRequestee = 1,
            ToRequester = 2,
            BusinessAreaApproverRequest = 3,
            BusinessAreaApproverRequestResponse = 4,
            PendingRoleRecertifications = 5,
            AccessAreaApproverRequest = 6,
            BusinessAreaApproverRemoval = 7,
            AccessAreaApproverRemoval = 8,
            PendingRoleRecertificationsEscalation = 9,
            PendingAccessAreasRecertification = 10,
            AccessRequest = 11,
            AccessAreaApproverRequestResponse = 12,
            AccessAreaRecertifierRequestResponse = 13,
            ApproveAccessResponse = 14,
            ApprovePersonResponse = 15,
            CreatePassResponse = 16,
            AccessAreaRecertifierRemoval = 17,
            AccessAreaRecertifierRequest = 18,
            OutOfOfficeSet = 19,
            OutOfOfficeUnset = 20,
            OutOfOfficeAboutToExpire = 21,
            FlagPassRequestToPO = 22,
            DivisionAdministrator = 23,
            CreatePassRequest = 24,
            PassToRequestee = 25,
            PassOfficeUser = 26,
            DivisionOwner = 27,
            AccessAreaAddition = 28,
            BusinessAreaAddition = 29,
            MSPAssignRole = 30,
            Recommendation = 31,
            RequestApproval = 32,
            AccessApproval = 33,
            SwitzerlandEmail = 35,
            SmartCardRequestRejection = 42
        };

        public enum TaskType
        {
            None = 0,
            CreatePass = 1,
            ApprovePerson = 2,
            ApproveAccess = 3,
            AcceptBARole = 4,
            AcceptAARole = 5,
            AcceptARECRole = 6,
            AcceptDARole = 8,
            AcceptPORole = 9,
            AcceptDORole = 10,
            AcceptMSPRole = 11,
            //[Mahmoud.Ali]
            DeactivateBadge = 12,
            //[/Mahmoud.Ali],
            RevokeAccess = 13,
            ReactivateAccess = 14,
            ChangeStartDate = 15,
            SuspendAccess = 16,
            UnsuspendAccess  =17,
            ApproveSmartCard = 18
        };

        public enum TaskStatus
        {
            Pending = 1, Rejected = 2, Accepted = 3, Redundant = 4
        };

        public enum RoleType
        {
            DivisionOwner = 7,
            DivisionAdministrator = 3,
            BusinessAreaApprover = 4,
            AccessApprover = 5,
            AccessRecertifier = 8,
            PassOfficeOperator = 6
        };

        public enum SortDirection
        {
            Down = -1, Up = 1
        };

        public enum SmartcardStatus
        {
            Active = 1, Inactive = 2, Initial = 3, Unknown = 4
        }

        public enum SmartcardNumberUploadStatus
        {
            Success = 1, MifareExists = 2, dbSmartcardNumberExists = 3, BadgeExists = 4, InvalidCharacters = 5
        }

        public enum SmartcardServiceActionType
        {
            RevokeSmartcard = 1, RequestReplacementBadge = 2
        }

        public enum SmartcardServiceActionStatus
        {
            New = 1, ErrorInvalidArgument = 2, ErrorSmartcardNumberNotFound = 3, CommunicationError = 4, ReachedMaximumRetries = 5, UnknownError = 6, Success = 7
        }

        public enum AccessRequestStatus
        {
            Draft = 1, Submitted = 2, InProgress = 3, Approved = 4, Actioned = 5, Rejected = 10, Cancelled = 11, Failed = 12, MSPExtension = 13
        }

        public enum GlobalResourceEnums
        {
            AwaitingTasks = 1,
            PendingRequests = 2,
            Access = 3,
            NewPass = 4,
            GridViewCommandColumnEdit = 5,
            GridViewDataTextColumnApprove = 6,
            PleaseSelect = 7
        }

        public enum GlobalResourceEnumsRequestStatuses
        {
            AwaitingActionFrom = 1,
            RequestApprovedBy = 2,
            RequestRejectedBy = 3,
            AccessApprovedBy = 4,
            AccessRejectedBy = 5,
            PassRequestApprovedBy = 6,
            PassRequestRejectedBy = 7,
            DivisionAdminRoleApprovedBy = 8,
            DivisionAdminRoleRejectedBy = 9,
            AccessAreaApproverRoleApprovedBy = 10,
            AccessAreaApproverRoleRejectedBy = 11,
            AccessAreaRecertifierRoleApprovedBy = 12,
            AccessAreaRecertifierRoleRejectedBy = 13,
            RequestApproverRoleApprovedBy = 14,
            RequestApproverRoleRejectedBy = 15,
            PassOfficeRoleApprovedBy = 16,
            PassOfficeRoleRejectedBy = 17,
            DivisionOwnerRoleApprovedBy = 18,
            DivisionOwnerRoleRejectedBy = 19
        }

        public enum Language
        {
            English = 1,
            German = 2,
            Italian = 3
        }
    }
}
