﻿
//***********************************Code File Header ******************************************
//Author: 	AC
//Created: 	 
//Version: 	1.0
//Description:	Use this to pass 2 or more parameters during a grid custom callback. 
//Requires the javascript function CallbackParamsToString(params) inluded in Utils.js.
//For sample useage, see my accessareas.aspx.
//**********************************************************************************************/

using System;
using System.Collections.Generic;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public static class DictionaryDeserializer
    {
        private const char Separator = '|';

        public static bool Deserialize(string data, IDictionary<string, string> dictionary)
        {
            dictionary.Clear();
            if (!string.IsNullOrEmpty(data))
            {
                ParseData(data, dictionary);
                return true;
            }
            return false;
        }

        private static void ParseData(string data, IDictionary<string, string> dictionary)
        {
            int startIndex = 0;
            while (ParseNameValuePair(data, ref startIndex, dictionary)) ;
        }
        private static bool ParseNameValuePair(string data, ref int startIndex, IDictionary<string, string> dictionary)
        {
            if (startIndex >= data.Length)
                return false;
            int indexOfFirstSeparator = data.IndexOf(Separator, startIndex);
            string fieldName = data.Substring(startIndex, indexOfFirstSeparator - startIndex);
            startIndex += fieldName.Length + 1;
            int indexOfSecondSeparator = data.IndexOf(Separator, startIndex);
            string fieldValueLengthStr = data.Substring(startIndex, indexOfSecondSeparator - startIndex);
            startIndex += fieldValueLengthStr.Length + 1;
            int fieldValueLength = Int32.Parse(fieldValueLengthStr);
            string fieldValue = data.Substring(startIndex, fieldValueLength);
            startIndex += fieldValueLength;
            dictionary.Add(fieldName, fieldValue);
            return true;
        }
    }
}
