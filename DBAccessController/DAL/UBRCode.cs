//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class UBRCode
    {
        public string UBR_Code { get; set; }
        public int UBR_Level { get; set; }
        public string UBR_Name { get; set; }
        public string Parent { get; set; }
    }
}
