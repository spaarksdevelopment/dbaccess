//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccessRequestStatu
    {
        public AccessRequestStatu()
        {
            this.AccessRequests = new HashSet<AccessRequest>();
            this.AccessRequestMasters = new HashSet<AccessRequestMaster>();
        }
    
        public int RequestStatusID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public string GermanName { get; set; }
        public string ItalyName { get; set; }
    
        public virtual ICollection<AccessRequest> AccessRequests { get; set; }
        public virtual ICollection<AccessRequestMaster> AccessRequestMasters { get; set; }
    }
}
