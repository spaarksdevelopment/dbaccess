//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_ControlSystemCategory
    {
        public int AccessAreaID { get; set; }
        public int ControlSystemID { get; set; }
        public int ControlSystemTypeID { get; set; }
        public string ControlSystemCategoryName { get; set; }
    }
}
