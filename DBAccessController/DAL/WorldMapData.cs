//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    
    public partial class WorldMapData
    {
        public string Name { get; set; }
        public int CountryID { get; set; }
        public string Type { get; set; }
        public int ParentId { get; set; }
        public bool Enabled { get; set; }
    }
}
