//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_MyRequestsDetailReport
    {
        public int requestMasterID { get; set; }
        public Nullable<int> CreatedByID { get; set; }
        public System.DateTime Created { get; set; }
        public int RequestTypeID { get; set; }
        public string RequestType { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public Nullable<int> RequestStatusID { get; set; }
        public string StatusDesc { get; set; }
        public string FloorName { get; set; }
        public string BuildingName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string AccessAreaName { get; set; }
        public Nullable<int> AccessAreaID { get; set; }
        public Nullable<int> AccessAreaTypeID { get; set; }
        public string ApplicantName { get; set; }
        public Nullable<int> LandlordID { get; set; }
        public string VendorName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> SponsorUserID { get; set; }
        public string SponsorName { get; set; }
        public string SponsorEmail { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public Nullable<int> ApprovedByID { get; set; }
        public Nullable<bool> AccessApproved { get; set; }
        public Nullable<System.DateTime> AccessApprovedDate { get; set; }
        public Nullable<int> AccessApprovedByID { get; set; }
        public string ApproverName { get; set; }
        public string AccessApproverName { get; set; }
        public string AwaitingActionFrom { get; set; }
        public Nullable<int> FloorID { get; set; }
        public int RequestID { get; set; }
        public string History { get; set; }
    }
}
