//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    
    public partial class PassOfficeMspRoles
    {
        public int dbPeopleID { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Telephone { get; set; }
        public string Class { get; set; }
        public string UBR { get; set; }
        public string CostCentreName { get; set; }
        public string ManagerName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string Status { get; set; }
        public string VendorName { get; set; }
        public bool Enabled { get; set; }
        public string ExternalType { get; set; }
        public int SecurityGroupId { get; set; }
        public string SecurityGroup { get; set; }
    }
}
