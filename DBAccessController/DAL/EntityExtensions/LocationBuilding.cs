﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    public partial class LocationBuilding : IComparable
    {
        public int CompareTo(object obj)
        {
            return (new AlphaSort()).Compare(this.Name, ((LocationBuilding)obj).Name);
        }
    }
}
