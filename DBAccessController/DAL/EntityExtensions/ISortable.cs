﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
	public interface ISortable
	{
		byte? SortOrder { get; set; }
	}
}
