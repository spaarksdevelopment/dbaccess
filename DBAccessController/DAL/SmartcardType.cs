//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SmartcardType
    {
        public SmartcardType()
        {
            this.SmartcardNumberStagings = new HashSet<SmartcardNumberStaging>();
            this.SmartcardUploadAudits = new HashSet<SmartcardUploadAudit>();
        }
    
        public int ID { get; set; }
        public int SmartCardProviderID { get; set; }
        public string Type { get; set; }
        public string Label { get; set; }
        public string TypeIdentifier { get; set; }
        public bool Enabled { get; set; }
    
        public virtual ICollection<SmartcardNumberStaging> SmartcardNumberStagings { get; set; }
        public virtual SmartcardProvider SmartcardProvider { get; set; }
        public virtual ICollection<SmartcardUploadAudit> SmartcardUploadAudits { get; set; }
    }
}
