//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccessRequestMaster
    {
        public AccessRequestMaster()
        {
            this.AccessRequestPersons = new HashSet<AccessRequestPerson>();
        }
    
        public int RequestMasterID { get; set; }
        public System.DateTime Created { get; set; }
        public string Comment { get; set; }
        public Nullable<int> RequestTypeID { get; set; }
        public Nullable<int> RequestStatusID { get; set; }
        public Nullable<int> CreatedByID { get; set; }
        public Nullable<int> SponsorUserID { get; set; }
    
        public virtual AccessRequestDeliveryDetail AccessRequestDeliveryDetail { get; set; }
        public virtual AccessRequestStatu AccessRequestStatu { get; set; }
        public virtual AccessRequestType AccessRequestType { get; set; }
        public virtual ICollection<AccessRequestPerson> AccessRequestPersons { get; set; }
    }
}
