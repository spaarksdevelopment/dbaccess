//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_MSPPersonDetails
    {
        public int dbPeopleID { get; set; }
        public bool IsExternal { get; set; }
        public string Forename { get; set; }
        public Nullable<int> DBDirID { get; set; }
        public string Surname { get; set; }
        public Nullable<int> VendorId { get; set; }
        public string VendorName { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<int> PersonBadgeID { get; set; }
        public string MiFareNumber { get; set; }
    }
}
