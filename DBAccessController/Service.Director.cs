﻿using System;
using System.Collections.Generic;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region Get methods
        public static Guid? GetServiceGuid()
        {
            try
            {
                return (Guid)Managers.ServiceManager.GetServiceGuid();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }    
        #endregion

        #region Create

        #endregion

        #region Remove

        #endregion

        #region Update

        #endregion
    }
}
