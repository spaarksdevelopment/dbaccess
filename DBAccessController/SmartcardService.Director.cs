﻿using System;
using System.Collections.Generic;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.IO;
using System.Globalization;
using System.Net;
using System.Xml;
using System.Text;
using System.Configuration;
using Spaarks.Common.UtilityManager;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Resources;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region Get methods
		/// <summary>
		/// Returns the dbPeopleID of the smartcard owner or an empty string if no owner
		/// </summary>
		/// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
		public static string GetSmartcardStatus(string smartcardIdentifier, string[] certDetails)
		{
			try
			{
                return Managers.SmartcardServiceManager.GetSmartcardStatus(smartcardIdentifier, certDetails);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return "ERROR";
			}
		}

		/// <summary>
		/// Returns the PUK (Personal Unblocking Key) for the given smartcard or an empty string if smartcard is not known
		/// The PUK is associated with a vendor and will change every few years.
		/// The PUK is stored against the smartcard in the db when the smartcard is issued
		/// </summary>
		/// <param name="smartcardIdentifier">The dbSmartcard card identifier in the form provider_Serial e.g. gem2_4C71A7B5E5C9AECA></param>
		public static string GetKeyIdentifier(string smartcardIdentifier, string[] certDetails)
		{
			try
			{
                return Managers.SmartcardServiceManager.GetKeyIdentifier(smartcardIdentifier, certDetails);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return "ERROR";
			}
		}

        public static List<GetSmartcardProviders_Result> GetSmartCardProviders(int LanguageId, bool includeDisabled) 
		{
			try
			{
                List<GetSmartcardProviders_Result> smartcardProviders = new List<GetSmartcardProviders_Result>();
                smartcardProviders = Managers.SmartcardServiceManager.GetSmartCardProviders(LanguageId,includeDisabled);

				
				return smartcardProviders;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static List<SmartcardProvider> GetSmartCardProviders(bool includeDisabled)
        {
            try
            {
                List<SmartcardProvider> smartcardProviders = new List<SmartcardProvider>();
                smartcardProviders = Managers.SmartcardServiceManager.GetSmartCardProviders(includeDisabled);

                smartcardProviders.Insert(0, new SmartcardProvider { ID = 0, ProviderName = "Select a Provider..." });

                return smartcardProviders;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }


		public static List<vw_SmartcardProviderDetails> GetSmartCardProvidersExtended(bool includeDisabled ,string SelectProvider)
		{
			try
			{
				List<vw_SmartcardProviderDetails> smartcardProviders = new List<vw_SmartcardProviderDetails>();
				smartcardProviders = Managers.SmartcardServiceManager.GetSmartCardProvidersExtended(includeDisabled);

                smartcardProviders.Insert(0, new vw_SmartcardProviderDetails { ID = 0, TypeString = SelectProvider });

				return smartcardProviders;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}
		public static List<vw_SmartcardTypeDetails> GetSmartCardTypes(bool includeDisabled)
		{
			try
			{
				List<vw_SmartcardTypeDetails> smartcardTypesDetails = new List<vw_SmartcardTypeDetails>();
                smartcardTypesDetails = Managers.SmartcardServiceManager.GetSmartCardTypes(includeDisabled);

				smartcardTypesDetails.Insert(0, new vw_SmartcardTypeDetails { ID = 0, ProviderName = "Select a Provider..." });
				return smartcardTypesDetails;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static List<GetSmartCardTypes_Result> GetSmartCardTypes(int languageid, bool includeDisabled)
        {
            try
            {
                List<GetSmartCardTypes_Result> smartcardTypesDetails = new List<GetSmartCardTypes_Result>();
                smartcardTypesDetails = Managers.SmartcardServiceManager.GetSmartCardTypes(languageid,includeDisabled); 

                //smartcardTypesDetails.Insert(0, new vw_SmartcardTypeDetails { ID = 0, ProviderName = "Select a Provider..." });
                return smartcardTypesDetails;
            }

            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		public static List<vw_SmartcardNumberStaging> GetSmartcardNumbersStaging()
		{
			try
			{
				List<vw_SmartcardNumberStaging> smartcardNumbers = new List<vw_SmartcardNumberStaging>();
				smartcardNumbers = Managers.SmartcardServiceManager.GetSmartcardNumbersStaging();

				return smartcardNumbers;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static int GetDuplicatesCount()
		{
			try
			{
				return Managers.SmartcardServiceManager.GetDuplicatesCount();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return 0;
			}
		}

		public static int GetExistingCount()
		{
			try
			{
				return Managers.SmartcardServiceManager.GetExistingCount();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return 0;
			}
		}

		public static int GetInvalidCount()
		{
			try
			{
				return Managers.SmartcardServiceManager.GetInvalidCount();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return 0;
			}
		}

		public static bool GetCanDeleteVendor(int vendorID)
		{
			try
			{
				return Managers.SmartcardServiceManager.GetCanDeleteVendor(vendorID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return false;
			}
		}
		
		public static bool GetCanDeleteSmartcardType(int smartcardTypeId)
		{
			try
			{
				return Managers.SmartcardServiceManager.GetCanDeleteSmartcardType(smartcardTypeId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return false;
			}
		}

        public static bool IsVendorNameUnique(int vendorID, string vendorName)
        {
            try
            {
                return Managers.SmartcardServiceManager.IsVendorNameUnique(vendorID, vendorName);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return false;
            }
        }

        public static bool IsTypeNameUnique(int typeId, string keyIdentifier)
        {
            try
            {
                return Managers.SmartcardServiceManager.IsTypeNameUnique(typeId, keyIdentifier);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return false;
            }
        }

		public static List<vw_SmartcardUploadAudit> GetVendorFiles()
		{
			try
			{
				return Managers.SmartcardServiceManager.GetVendorFiles();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static List<vw_SmartcardNumbers> GetSmartcardNumbers(int auditID)
		{
			try
			{
				return Managers.SmartcardServiceManager.GetSmartcardNumbers(auditID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		//Returns the number of numbers in a vendor upload which have had a badge created with the corresponding mifare
		public static int GetNumberOfMatchingBadges(int auditID)
		{
			try
			{
				return Managers.SmartcardServiceManager.GetNumberOfMatchingBadges(auditID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return -1;
			}
		}

		public static List<vw_SmartcardServiceAction> GetPendingActions()
		{
			try
			{
				return Managers.SmartcardServiceManager.GetPendingActions();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				throw new ApplicationException("Failed to fetch pending actions", ex);
			}
		}
        #endregion

		#region Create
		/// <summary>
		/// Returns Error message, empty if successful
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public static string UploadSmartcardNumbers(StreamReader reader, int providerID)
		{
			try
			{
                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

				//SmartcardProvider provider = Managers.SmartcardServiceManager.GetSmartCardProvider(providerID);

				vw_SmartcardProviderDetails provider = Managers.SmartcardServiceManager.GetSmartCardProvider(providerID);

				bool containsDuplicateSerials = false;

				//NB Clear the staging table, Save them to the staging table (remember to convert from hex) then run a stored procedure to identify duplicates and existing numbers
				List<SmartcardNumberStaging> smartcardNumbers = Director.GetSmartcardNumbersFromFile(reader, provider, ref containsDuplicateSerials);

				if (containsDuplicateSerials)
                    return resourceManager.GetString("dbSmartcardError7"); 

				if (smartcardNumbers.Count == 0)
				{
					Managers.SmartcardServiceManager.ClearSmartcardNumberStaging();
                    return resourceManager.GetString("dbSmartcardError8"); 
				}

				if (provider.UseHexMifare)
					Director.ConvertMifareToDecimal(smartcardNumbers);

				Managers.SmartcardServiceManager.InsertSmartcardNumbersStaging(smartcardNumbers);

				return string.Empty;
			}
			catch (Exception ex)
			{
                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
				LogHelper.HandleException(ex);
                return resourceManager.GetString("dbSmartcardError9");  
			}
		}

		public static string ConfirmSmartcardUpload(int userID)
		{
			//NB Copy the staging data to the smartcard table, ignoring duplicates and existing numbers. Empty the staging table
			try
			{
                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

				int numberOfUploads = Managers.SmartcardServiceManager.ConfirmSmartcardUpload(userID);

                return string.Format("{0} {1} {2} ", numberOfUploads," ", resourceManager.GetString("dbSmartcardSuccess")); 
			}
			catch (Exception ex)
			{
                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
				LogHelper.HandleException(ex);
                return resourceManager.GetString("dbSmartcardError10"); 
			}
		}
		#endregion

		#region Remove
		public int DeleteSmartCardProvider(int id)
		{
            return Managers.SmartcardServiceManager.DeleteSmartCardProvider(id, this._dbPeopleID);
		}

		public int DeleteSmartCardType(int id)
		{
			return Managers.SmartcardServiceManager.DeleteSmartCardType(id, this._dbPeopleID);
		}
		#endregion

		#region Update
        public bool UpdateSmartCardProvider(string vendorName, string keyIdentifier, bool useHexMifare, bool enabled, int ID = 0)
        {
            try
            {
                Managers.SmartcardServiceManager.UpdateSmartCardProvider(this._dbPeopleID, vendorName, keyIdentifier, useHexMifare, enabled, ID);
                return true;
            }
            catch(Exception ex)
            {
                LogHelper.HandleException(ex);
                return false;
            }
        }
        
        public bool UpdateSmartCardType(string smartCardType, string keyIdentifier, bool enabled, int providerId, string label, int ID = 0)
        {
            try
            {
                Managers.SmartcardServiceManager.UpdateSmartCardType(this._dbPeopleID, smartCardType, keyIdentifier, enabled, providerId, label, ID);
                return true;
            }
            catch(Exception ex)
            {
                LogHelper.HandleException(ex);
                return false;
            }
        }

        public static void AddSmartcardServiceAudit(string method, string smartcardIdentifier, string returnValue, string[] certDetails, string errorMessage)
        {
            try
            {
                Managers.SmartcardServiceManager.AddSmartcardServiceAudit(method, smartcardIdentifier, returnValue, certDetails, errorMessage);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
        }

		public static void UpdateRevokeSmartcardAction(int id, int result)
		{
			try
			{
				Managers.SmartcardServiceManager.UpdateRevokeSmartcardAction(id, result);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
		}

		public static int RequestReplacementBadge(int currentUserID, vw_SmartcardServiceAction action)
		{ 
		    try
		    {
                ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");
		        HRPersonBadge badge = Managers.SmartcardServiceManager.GetBadge(action.Mifare);
		        if (badge == null)
                    throw new ApplicationException(string.Format("{0} , actionid={1}, mifare={2}", resourceManager.GetString("dbSmartcardError11"), action.ID, action.Mifare)); 
			
		        int requestMasterID = 0;

		        Director director = new Director(currentUserID);
				List<int> badgesToReplace = new List<int>() { badge.PersonBadgeID };

				if (AccessRequestManager.HasPendingPassRequest(badge.dbPeopleID, badgesToReplace))
				{
					//The badge request is still pending. This means that they have not entered a mifare yet for the badge request
					//so we don't have to create a new badge request, just return 0 for success
					return 0;
				}

				return director.CreateNewBadgeRequestForPerson(badge.dbPeopleID, badgesToReplace, ref requestMasterID);
		    }
		    catch (Exception ex)
		    {
		        LogHelper.HandleException(ex);
		        throw;
		    }
		}
		
		public static void UpdateReplacementBadgeAction(int id, int result)
		{
		    try
		    {
		        Managers.SmartcardServiceManager.UpdateReplacementBadgeAction(id, result);
		    }
		    catch (Exception ex)
		    {
		        LogHelper.HandleException(ex);
		    }
		}

		public static bool RecallFile(int auditID)
		{
			try
			{
				return Managers.SmartcardServiceManager.RecallFile(auditID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return false;
			}
		}

		public static int RevokeSmartcard(string serialNumber)
		{
            ResourceManager resourceManager = Director.GetResourceManager("Resources.CommonResource");

			string host = ConfigurationManager.AppSettings["PKIHost"]; 
			string data = serialNumber;
			string pkiResponse = ServiceCall(host, data);

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(pkiResponse);

			string soapResponse = ParseRevocationResponse(doc);

			int result = -1;
			if (!int.TryParse(soapResponse, out result))
                throw new ApplicationException(string.Format("{0}: {1}", resourceManager.GetString("dbSmartcardError12"), soapResponse));

			return result;
		}
		#endregion

		#region helpers
		//Extract the revocation service return value from xml
		// 0 - Success
		// 1 - invalid argument (incorrect smartcard type) 
		// 2 - couldn't find smartcard identifier
		// 256 - communication error
		private static string ParseRevocationResponse(XmlDocument doc)
		{
			string pkiHost = ConfigurationManager.AppSettings["PKIHost"];
			string pkiMethodPath = ConfigurationManager.AppSettings["PKIMethodPath"];

			XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
			ns.AddNamespace("dns", "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition");
			ns.AddNamespace("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");
			ns.AddNamespace("encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/");
			ns.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
			ns.AddNamespace("PKI", pkiHost + pkiMethodPath);

			//Note the xml element we are looking for is of the form s-gensym3
			//However after DB changed version of the software on their server the name of the element changed to s-gensym6
			//so I have changed the code to use a wildcard instead of an explicit element name
			XmlNode resultNode = doc.SelectSingleNode("/soap:Envelope/soap:Body/PKI:RevokeSmartcardResponse/*", ns);

			if (resultNode == null)
                throw new ApplicationException("Unable to parse a response value from the Revocation service response xml" + doc.InnerXml);

			return resultNode.InnerXml;
		}

		private static string ServiceCall(string host, string data)
		{
			HttpWebRequest request = CreateWebRequest(host);
			XmlDocument soapEnvelopeXml = GetSoapXml(host, data);

			try
			{
				X509Certificate clientCertificate = GetClientCertificate();

				if (clientCertificate != null)
					request.ClientCertificates.Add(clientCertificate);
			}
			catch
			{
				Trace.Write("Failed to add client certificate to the request");
			}

			using (Stream stream = request.GetRequestStream())
			{
				soapEnvelopeXml.Save(stream);
			}

			WebResponse webResponse = request.GetResponse();

			string soapResult;
			using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
			{
				soapResult = rd.ReadToEnd();
			}
			return soapResult;
		}

		private static X509Certificate GetClientCertificate()
		{
			ClientCertificates certs = new ClientCertificates();
			string clientCertificateName = ConfigurationManager.AppSettings["ClientCertificateName"];

			if (certs.Init() != 0 && !string.IsNullOrEmpty(clientCertificateName))
			{
				foreach (var cert in certs.ClientCertificatesList)
				{
					if (cert.Subject.Contains(clientCertificateName))
					{
						Trace.Write(string.Format("Certificate found:{0}", cert.Subject));
						return cert;
					}
				}
			}

			return null;
		}

		private static HttpWebRequest CreateWebRequest(string host)
		{
			string header = ConfigurationManager.AppSettings["PKIHeader"];

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(host + "soap/");
			webRequest.Headers.Add("SOAPAction", host + header);

			webRequest.ContentType = "text/xml;charset=\"utf-8\"";
			webRequest.Accept = "text/xml";

			webRequest.Method = "POST";


			return webRequest;
		}

		private static XmlDocument GetSoapXml(string host, string data)
		{
			string pkiMethodPath = ConfigurationManager.AppSettings["PKIMethodPath"];
			string pkiParameterElementName = ConfigurationManager.AppSettings["PKIParameterElementName"];

			string parameterElementOpen = string.Format("<{0} xsi:type=\"xsd:string\">", pkiParameterElementName);
			string parameterElementClose = string.Format("</{0}>", pkiParameterElementName);

			StringBuilder sb = new StringBuilder();
			sb.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
			sb.Append("<soap:Body>");
			sb.Append("<namesp1:RevokeSmartcard xmlns:namesp1=\"");
			sb.Append(host);
			sb.Append(pkiMethodPath + "\">");
			sb.Append(parameterElementOpen);
			sb.Append(data);
			sb.Append(parameterElementClose);
			sb.Append("</namesp1:RevokeSmartcard>");
			sb.Append("</soap:Body>");
			sb.Append("</soap:Envelope>");

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(sb.ToString());
			return doc;
		}

		private static List<SmartcardNumberStaging> GetSmartcardNumbersFromFile(StreamReader reader, vw_SmartcardProviderDetails provider, ref bool containsDuplicateSerials)
		{ 
			List<SmartcardNumberStaging> smartcardNumbers = new List<SmartcardNumberStaging>();
			
			//This allows a check on whether there are duplicates serial numbers
			List<string> existingSerialNumbers = new List<string>();

			string line;
			while ((line = reader.ReadLine()) != null) 
			{
				string[] values = line.Split('\t');

				if(values.Length!=2)
					continue;

				//Check that this is a hex number or a decimal number
				long dummy = 0;
				bool parsedSuccessfully = long.TryParse(values[0], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out dummy);

				if(parsedSuccessfully)
					parsedSuccessfully = long.TryParse(values[1], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out dummy);

				SmartcardNumberStaging currentEntry = new SmartcardNumberStaging();
				currentEntry.SerialNumber = string.Format("{0}_{1}", provider.TypeIdentifier, values[0]);
				currentEntry.Mifare = values[1];
				currentEntry.UploadStatusID = parsedSuccessfully ? (int)DBAccessEnums.SmartcardNumberUploadStatus.Success : (int)DBAccessEnums.SmartcardNumberUploadStatus.InvalidCharacters;
				currentEntry.SmartcardProviderID = provider.ID;
				currentEntry.SmartcardTypeID = provider.TypeID;
				
				smartcardNumbers.Add(currentEntry);

				if (existingSerialNumbers.Contains(currentEntry.SerialNumber))
				{
					containsDuplicateSerials = true;
					return smartcardNumbers;
				}


				existingSerialNumbers.Add(currentEntry.SerialNumber);
			}

			return smartcardNumbers;
		}

		private static void ConvertMifareToDecimal(List<SmartcardNumberStaging> smartcardNumbers)
		{
			foreach (SmartcardNumberStaging number in smartcardNumbers)
			{
				//Check mifare was parsed successfully before converting to decimal
				if (number.UploadStatusID == (int)DBAccessEnums.SmartcardNumberUploadStatus.Success)
				{
					string mifare = number.Mifare;
					number.Mifare = InvertHexStringAndConvertToDecimal(mifare);
				}
			}
		}

		/// <summary>
		/// Takes a string of hex numbers in chunks of 2 chars and inverts
		/// ie 1F 2E 3D 4C becomes 4C 3D 2E 1F
		/// </summary>
		/// <param name="line"></param>
		/// <returns></returns>
		public static string InvertHexStringAndConvertToDecimal(string line)
		{
			string[] bytes = new string[(line.Length + 1) / 2];

			for (int i = 0; i < (line.Length + 1) / 2; i++)
			{
				int charsToTake = 2;
				if (i * 2 + 2 > line.Length)
					charsToTake = 1;

				string hexByte = line.Substring(i * 2, charsToTake);

				bytes[i] = hexByte;
			}

			Array.Reverse(bytes);

			string result = string.Empty;

			foreach (string hex in bytes)
				result += hex;

			long resultInt = Convert.ToInt64(result, 16);
			return resultInt.ToString();
		}
		#endregion
	}
}
