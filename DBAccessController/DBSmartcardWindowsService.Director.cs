﻿using System;
using System.Collections.Generic;
using Aduvo.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.IO;
using System.Globalization;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region Get methods
		public static List<vw_SmartcardServiceAction> GetPendingActions()
		{
			try
			{
				return Managers.DBSmartcardWindowsServiceManager.GetPendingActions();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				throw new ApplicationException("Failed to fetch pending actions", ex);
			}
		}
        #endregion

		#region Create
		
		#endregion

		#region Remove
		
		#endregion

		#region Update
        
		#endregion

		#region helpers
		
		#endregion
	}
}
