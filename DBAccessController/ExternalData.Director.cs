﻿using System;
using System.Collections.Generic;

using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region Get methods
        public static List<DAL.DBMovesLocation> GetDBMovesLocations(int? dbMovesRegionID)
        {
            try
            {
                return Managers.ExternalDataManager.GetDBMovesLocations(dbMovesRegionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.DBMovesLocationFloor> GetDBMovesFloors(int? dbMovesLocationID)
        {
            try
            {
                return Managers.ExternalDataManager.GetDBMovesFloors(dbMovesLocationID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }
        #endregion

        #region Create

        #endregion

        #region Remove

        #endregion

        #region Update

        #endregion
    }
}
