﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region static get methods

        public static int? GetDBMovesRegionIDByCityID(int cityID)
        {
            try
            {
                return Managers.LocationManager.GetDBMovesRegionIDByCityID(cityID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int? GetDBMovesRegionIDByBuildingID(int buildingID)
        {
            try
            {
                return Managers.LocationManager.GetDBMovesRegionIDByBuildingID(buildingID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int? GetDBMovesLocationIDByBuildingID(int buildingID)
        {
            try
            {
                return Managers.LocationManager.GetDBMovesLocationIDByBuildingID(buildingID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static int? GetDBMovesLocationIDByFloorID(int floorID)
        {
            try
            {
                return Managers.LocationManager.GetDBMovesLocationIDByFloorID(floorID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DBAccessController.DAL.ViewAllTasks> GetViewAllMyTasks(int nuserID)
        {
            try
            {
                return Managers.LocationManager.GetViewAllMyTasks(nuserID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);               
                return null;
            }
        }
		public static List<DBAccessController.DAL.GetViewAllRequests> GetViewAllRequests(int nUserID, DateTime StartDate, DateTime EndDate)
		{
			try
			{
				return Managers.LocationManager.GetViewAllRequests(nUserID, StartDate, EndDate);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        
        public static List<DBAccessController.DAL.GetViewAllAdminRequests> GetViewAllAdminRequests(int nUserID, DateTime StartDate, DateTime EndDate)
        {
            try
            {
				return Managers.LocationManager.GetViewAllAdminRequests(nUserID, StartDate, EndDate);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		public static string GetRequestDetails(int ncurrentUserId, int requestID, string type)
		{
			try
			{
				return Managers.LocationManager.GetRequestDetails(ncurrentUserId, requestID, type);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}


        public static List<DBAccessController.DAL.ViewAllMyRequests> GetViewAllMyRequests(int nUserID)
        {
            try
            {
                return Managers.LocationManager.GetViewAllMyRequests(nUserID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		public static List<DBAccessController.DAL.vw_PassOfficeDetails> GetPassOfficeDetails()
		{
			return GetPassOfficeDetails(0);			// All
		}

		public static List<DBAccessController.DAL.vw_PassOfficeDetails> GetPassOfficeDetails(Int32 iPassOfficeID)
        {
            try
            {
				return Managers.LocationManager.GetPassOfficeDetails(iPassOfficeID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }
		

			 /// <summary>
        /// Get the access areas in AccessAreaLookUp Table
        /// </summary>
        /// <returns></returns>
		public static int GetApproverEsculationFrequency(int nTaskType)
        {
            try
            {
				return Managers.LocationManager.GetApproverEsculationFrequency(nTaskType);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
				return 0;
            }
        }



        /// <summary>
        /// Get the access areas in AccessAreaLookUp Table
        /// the variable language is a dummy
        /// </summary>
        /// <returns></returns>
        public static List<DBAccessController.DAL.GetPassOffice> GetPassOffice(int? LanguageID, string language)
        {
            try
            {
                return Managers.LocationManager.GetPassOffice(LanguageID, language);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Get the access areas in AccessAreaLookUp Table
        /// the variable language is a dummy
        /// </summary>
        /// <returns></returns>
        public static List<DBAccessController.DAL.LocationPassOffice> GetPassOfficesByCountryId(int CountryId)
        {
            try
            {
                return Managers.LocationManager.GetPassOfficeFilteredByCountryId(CountryId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }
        /// <summary>
        /// Method to return a pass office details including display name based on Country
        /// Asanka 21.05.2015
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public static List<GetAllPassOffices> GetEnabledPassOfficeDetailsByCountryID(int? languageID, int CountryId)
        {
            try
            {
                return Managers.LocationManager.GetEnabledPassOfficeDetailsByCountryID(languageID, CountryId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<AccessAreaType> GetAccessAreaTypes()
        {
            try
            {
                return Managers.LocationManager.GetAccessAreaTypes();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<Category> GetControlSystemCategoryName(int areaid)
        {
            try
            {
                return Managers.LocationManager.GetControlSystemCategoryName(areaid);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static bool CheckIfControlSystemExists(int areaId, int controlsystemid)
        {
            bool bval = false;
            try
            {
              bval =   Managers.LocationManager.CheckIfControlSystemExists(areaId, controlsystemid);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
            }
            return bval;
        }

     
        public static List<vw_ControlSystemCategoryName> GetAvailableCategoryName()
        {
            try
            {
                return Managers.LocationManager.GetAvailableCategoryName();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<Category> GetAllCategoryName()
        {
            try
            {
                return Managers.LocationManager.GetAllCategoryName();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }


        public static List<DAL.LocationCountry> GetCountriesByDbPeopleID(int dbPeopleID,int? languageID)
        {
            try
            {
                return Managers.LocationManager.GetCountriesByDbPeopleID(dbPeopleID, languageID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationRegion> GetRegionsAllEnabled()
        {
            try
            {
                return Managers.LocationManager.GetRegions();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationRegion> GetRegions(bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetRegions(EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static DAL.LocationRegion GetRegion(int RegionID)
        {
            try
            {
                return Managers.LocationManager.GetRegion(RegionID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);             
                return null;
            }
        }

        public static List<DAL.LocationCountry> GetCountriesAllEnabled()
        {
            return GetCountriesAllEnabled(false);
        }

        public static bool IsCountryEnabled(int countryID)
        {
            List<LocationCountry> countries = GetCountriesAllEnabled(false);
            LocationCountry thisCountry = countries.SingleOrDefault(a => a.CountryID == countryID);
            return thisCountry != null && thisCountry.Enabled;
        }

        public static List<DAL.LocationCountry> GetCountriesAllEnabled(bool addDefaultValue)
        {
            try
            {
                List<DAL.LocationCountry> countries = new List<LocationCountry>();
                
                if(addDefaultValue)
                    countries.Add(new LocationCountry { CountryID = 0, Name = "Select an Item..." });

                countries.AddRange(Managers.LocationManager.GetCountries());

                return countries;
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationCountry> GetCountriesEnabledForRegion(int RegionId)
        {
            try
            {
                return Managers.LocationManager.GetCountries(RegionId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);              
                return null;
            }
        }

        public static List<DAL.LocationCountry> GetCountriesForRegion(int? RegionId, bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetCountries(RegionId, EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static DAL.LocationCountry GetCountry(int CountryId)
        {
            try
            {
                return Managers.LocationManager.GetCountry(CountryId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.ControlSystem> GetControlSystemsEnabled()
        {
            try
            {
                return Managers.LocationManager.GetControlSystems();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }



        public static List<DAL.LocationCity> GetCitiesAllEnabled()
        {
            try
            {
                return Managers.LocationManager.GetCities();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static DAL.LocationCity GetCity(int cityID)
        {
            try
            {
                return Managers.LocationManager.GetCity(cityID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static DAL.LocationCity GetCityFromAccessArea(int accessAreaID)
        {
            try
            {
                return Managers.LocationManager.GetCityFromAccessArea(accessAreaID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationCity> GetCitiesEnabledForCountry(int CountryId)
        {
            try
            {
                return Managers.LocationManager.GetCities(CountryId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationCity> GetCitiesForCountry(int? CountryId, bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetCities(CountryId, EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationBuilding> GetBuildingsAllEnabled()
        {
            try
            {
                return Managers.LocationManager.GetBuildings();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		public static List<LocationBuilding> GetBuildingsEnabledForCity(int cityId, bool hsApprovers, bool hasAccessAreasOnly = false)
        {
            try
            {
				return Managers.LocationManager.GetBuildings(cityId, true, hsApprovers, hasAccessAreasOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationBuilding> GetServiceBuildingsEnabledForCity(int CityId)
        {
            try
            {
                return Managers.LocationManager.GetBuildings(CityId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationBuilding> GetBuildingsForCity(int? CityId, bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetBuildings(CityId, EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Method to return all pass offices enabled
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<DAL.LocationPassOffice> GetPassOfficesAllEnabled()
        {
            try
            {
				return Managers.LocationManager.GetPassOffices();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public static List<GetAllPassOffices> GetAllPassOfficesEnabled(int? languageID)
		{
			try
			{
				return Managers.LocationManager.GetAllPassOfficesEnabled(languageID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        /// <summary>
        /// Method to return all enabled classifications
        /// E.Parker 
        /// 24.05.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.GetClassifications> GetClassificationsEnabled(int? languageID)
        {
            try
            {
                return Managers.LocationManager.GetClassifications(languageID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Method to get the classification Text 
        /// E.Parker
        /// 13.10.11
        /// </summary>
        /// <param name="classificationID"></param>
        /// <returns></returns>
        public static string GetClassificationText(int classificationID)
        {
            try
            {
                return Managers.LocationManager.GetClassificationText(classificationID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }


        public static List<DAL.LocationPassOffice> GetPassOffices(bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetPassOffices(EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.LocationPassOffice> GetAdminPassOffices()
        {
            try
            {
                return Managers.LocationManager.GetAdminPassOffices();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// method to return the visitor management control systems
        /// E.Parker 
        /// 28.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.ControlSystem> GetVistiorControlSystems()
        {
            try
            {
                return Managers.LocationManager.GetVistiorControlSystems();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);              
                return null;
            }
        }

        /// <summary>
        /// Method to return external landlords to assign to a building
        /// E.Parker
        /// 28.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.ControlLandlord> GetLandlords()
        {
            try
            {
                return Managers.LocationManager.GetLandlords();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);               
                return null;
            }
        }

        /// <summary>
        /// Method to return landlord based on and ID
        /// E.Parker 
        /// 13.05.11
        /// </summary>
        /// <param name="landlordID"></param>
        /// <returns></returns>
        public static DAL.ControlLandlord GetLandLord(int landlordID)
        {
            try
            {
                return Managers.LocationManager.GetLandLord(landlordID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);               
                return null;
            }
        }


        /// <summary>
        /// Method to return a pass office based on an ID
        /// E.Parker
        /// 15.04.11
        /// </summary>
        /// <param name="passOfficeID"></param>
        /// <returns></returns>
        public static DAL.LocationPassOffice GetPassOffice(int passOfficeID)
        {
            try
            {
                return Managers.LocationManager.GetPassOffice(passOfficeID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);              
                return null;
            }
        }

		public static List<LocationFloor> GetFloorsForBuilding(int? buildingID, bool enabledOnly, bool hasApprovers, bool hasAccessAreasOnly = false)
		{
			try
			{
				return Managers.LocationManager.GetFloors(buildingID, enabledOnly, hasApprovers, hasAccessAreasOnly);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);			
				return null;
			}
		}

        /// <summary>
        /// Method to return a specific floor based on ID
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="floorID"></param>
        /// <returns></returns>
        public static DAL.LocationFloor GetFloor(int floorID)
        {
            try
            {
                return Managers.LocationManager.GetFLoor(floorID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);               
                return null;
            }
        }


        /// <summary>
        /// Method to return a specific building based on ID
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="buildingID"></param>
        /// <returns></returns>
        public static DAL.LocationBuilding GetBuilding(int buildingID)
        {
             try
            {
                return Managers.LocationManager.GetBuilding(buildingID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);                
                return null;
            }
        }


        public static string GetLinkedAccessAreas(int buildingId)
        {
            return Managers.LocationManager.GetLinkedAccessAreas(buildingId);
        }


		public static List<string> GetAccessAreaForEdit(int areaId, string areaType, int areaTypeId)
        {
            try
            {
				return Managers.LocationManager.GetAccessAreaForEditing(areaId, areaType, areaTypeId);
          
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// ac: ummm, deletes an access area :)
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public int DeleteAccessArea(int areaId)
        {
            try
            {
                return Managers.LocationManager.DeleteAccessArea(areaId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);                
                return 0;
            }
        }

        /// <summary>
        /// ac: updates an access area.
        /// </summary>
        /// <param name="linkedAreas"></param>
        /// <param name="areaId"></param>
        /// <param name="areaTypeId"></param>
        /// <param name="floorId"></param>
        /// <param name="buildingId"></param>
        /// <param name="name"></param>
        /// <param name="divisionId"></param>
        /// <param name="controlSystemId"></param>
        /// <returns></returns>
		public bool UpdateAccessArea(int areaId, int areaTypeId, int? floorId, int buildingId, string name, int? divisionId, int controlSystemId, int recertPeriod, int categoryid, bool enabled, int corporateDivisionaccessAreaId, int? defaultAccessPeriod, string defaultAccessDuration)
        {
            if (floorId == 0) floorId = null;
            if (divisionId == 0) divisionId = null;

            try
            {
                return Managers.LocationManager.UpdateAccessArea(areaId, areaTypeId, floorId, buildingId, name, divisionId, controlSystemId, recertPeriod, categoryid, enabled, corporateDivisionaccessAreaId, defaultAccessPeriod, defaultAccessDuration);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return false;
            }
        }

        public static DAL.AccessArea GetAccessArea(int id)
        {
            try
            {
                return Managers.LocationManager.GetAccessArea(id);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        /// <summary>
        /// Method to return all details of an accessarea based on the requestmasterid
        /// E.Parker
        /// 29.09.11
        /// </summary>
        /// <param name="RequestMaster"></param>
        /// <returns></returns>
        public static DAL.AccessArea GetAccessAreaDetailsFromRequestMaster(int RequestMaster)
        {
            try {
                return Managers.LocationManager.GetAccessAreaDetailsFromRequestMaster(RequestMaster);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }

        }

        public static List<DAL.AccessArea> GetAccessAreasForEdit(int id, string type)
        {
            try
            {
                return Managers.LocationManager.GetAccessAreasForEditing(id, type);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

		public static List<AccessArea> GetAccessAreasForFloor(Int32? FloorId, Boolean EnabledOnly, Boolean HasApprovers)
		{
			try
			{
				return Managers.LocationManager.GetAccessAreas(FloorId, EnabledOnly, HasApprovers);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				
				return null;
			}
		}

        /// <summary>
        /// Returns any access areas associated with this floor or not associated with any floor 
        /// but in the same building as this floor
        /// </summary>
        /// <param name="FloorId"></param>
        /// <param name="EnabledOnly"></param>
        /// <returns></returns>
        public static List<DAL.AccessArea> GetAccessAreasForFloorAndBuildingGeneralAreas(int? floorId, bool enabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetAccessAreasForFloorAndBuildingGeneralAreas(floorId, enabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        public static List<DAL.AccessArea> GetAccessAreasByDBMovesFloorID(int dbMovesFloorID)
        {
            try
            {
                return Managers.LocationManager.GetAccessAreasByDBMovesFloorID(dbMovesFloorID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<AccessArea> GetAccessAreasForBuilding(int buildingID, bool enabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetAccessAreasForBuilding(buildingID, enabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }

        public static List<DAL.AccessArea> GetServiceAccessAreasForBuilding(int? BuildingId, bool EnabledOnly)
        {
            try
            {
                return Managers.LocationManager.GetServiceAccessAreas(BuildingId, EnabledOnly);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        public static List<DAL.TimeZone> GetAllTimeZones()
        {
            try
            {
                return Managers.LocationManager.GetAllTimeZones();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        /// <summary>
        /// Method to retun the region,country, city for the admin location page
        /// E.Parker
        /// 13.03.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.GetAdminLocationsData> GetAdminLocationsforList(int LanguageId)
        {

            try
            {
                return Managers.LocationManager.GetAdminLocationsforList(LanguageId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        /// <summary>
        /// Method to return the floors for the admin location page
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <returns></returns>
        public static List<DAL.AdminFloors> GetAdminFloors()
        {
            try
            {
                return Managers.LocationManager.GetAdminFloors();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                
                return null;
            }
        }

        public static List<DAL.vw_AccessArea> GetAccessAreasForCountry(int countryID)
        {

            try
            {
                return Managers.LocationManager.GetAccessAreasForCountry(countryID);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return null;
            }
        }

       

        //public static List<DAL.RequestTypesForReportSearching> GetRequestTypesForReportSearching()
        //{
        //   //  RequestTypesForReportSearching
        //    try
        //    {
        //       // return Managers.LocationManager.GetRequestTypesForReportSearching();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.HandleException(ex);

        //        return null;
        //    }
        //}


        #region World Map

        public static List<DAL.WorldMapData> GetMapData(int LanguageID)
        {
            List<DAL.WorldMapData> items = new List<DAL.WorldMapData>();

            items = Managers.LocationManager.GetMapData(LanguageID); 

            return items;
        }

        #endregion

        #endregion

        #region Create
        /// <summary>
        /// Method to create a new city
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <param name="newCity"></param>
        /// <returns></returns>
        public bool CreateCity(DAL.LocationCity newCity, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreateCity(newCity, isNew);
                
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// method to create a new country 
        /// E.Parker
        /// 13.04.11
        /// </summary>
        /// <param name="newCountry"></param>
        /// <returns></returns>
        public bool CreateCountry(DAL.LocationCountry newCountry, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue =  Managers.LocationManager.CreateCountry(newCountry, isNew);
                 
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// Method to create/ edit a floor 
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public bool CreateFloor(DAL.LocationFloor floor, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreateFloor(floor, isNew);
                
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// MEthod to create / edit building 
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="building"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public bool CreateBuilding(DAL.LocationBuilding building, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreateBuilding(building, isNew);
                
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }

        /// <summary>
        /// Method to edit / add region 
        /// E.Parker
        /// 14.04.11
        /// </summary>
        /// <param name="region"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public bool CreateRegion(DAL.LocationRegion region, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreateRegion(region, isNew);
                
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }



        /// <summary>
        /// Method to create/ edit a pass office 
        /// E.Parker 
        /// 14.04.11
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public bool CreatePassOffice(DAL.LocationPassOffice passOffice, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreatePassOffice(passOffice, isNew);

            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue;
        }


        #endregion


        #region Update
        /// <summary>
        /// Method to add / edit a landlord
        /// E.Parker
        /// 13.05.11
        /// </summary>
        /// <param name="landlord"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        public bool CreateEditLandlords(DAL.ControlLandlord landlord, bool isNew)
        {
            bool returnValue;
            try
            {
                returnValue = Managers.LocationManager.CreateEditLandlords(landlord, isNew);
            }
            catch (Exception ex)
            {
                returnValue = false;
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
            }
            return returnValue; 
        }
        #endregion
    }
}
