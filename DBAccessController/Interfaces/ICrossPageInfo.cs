﻿namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public interface ICrossPageInfo
    {
        int DivisionId { get; set; }
        int ItemId { get; set; }
        string Name { get; set; }
        bool EnabledStatus { get; set; }
        int ApprovalTypeId { get; set; }
    }
}
