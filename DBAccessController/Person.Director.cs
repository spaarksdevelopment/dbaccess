﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
	public partial class Director
	{
		#region static get methods

        public static int GetPersonLanguage(int? dbpeopleID, string emailAddress) 
        {
            try
            {
                return Managers.PersonManager.GetPersonLanguage(dbpeopleID, emailAddress);
            }

            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return 0;
            }

        }
		public static DAL.mp_User GetUserByID(int userID)
		{
			try
			{
				return Managers.PersonManager.GetUserByID(userID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

        public static DAL.mp_User GetUserBydbPeopleId(int dbPeopleId)
        {
            try
            {
                return Managers.PersonManager.GetUserBydbPeopleId(dbPeopleId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
                return null;
            }
        }


		public static DAL.HRPerson GetUserBydbPeopleID(int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetUserBydbPeopleID(dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		/// <summary>
		/// Called from MSP manage resources page.
		/// </summary>
		/// <param name="mspId"></param>
		/// <returns></returns>
		public static List<DAL.UserForMspManagement> GetUsersByMspId(int mspId)
		{
			try
			{
				return Managers.PersonManager.GetUsersByMspId(mspId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		/// <summary>
		/// Method to get the MSP details for a specific person
		/// E.Parker
		/// 22.04.11
		/// </summary>
		/// <param name="mspId"></param>
		/// <param name="dbPeopleID"></param>
		/// <returns></returns>
		public static DAL.vw_MSPPersonDetails GetMSPDetailsPerson(int mspId, int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetMSPPersonDetails(mspId, dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		/// <summary>
		/// Method to return a vendorID based on userID
		/// E.Parker 22.04.11
		/// </summary>
		/// <param name="userID"></param>
		/// <returns></returns>
		public static int GetVendorID(int userID)
		{
			try
			{
				return Managers.PersonManager.GetVendorID(userID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return 0;
			}
		}

		/// <summary>
		/// Method to get the vendor based on Id
		/// E.Parker
		/// 23.06.11
		/// </summary>
		/// <param name="?"></param>
		/// <returns></returns>
		public static DAL.HRVendor GetVendorById(int vendorId)
		{
			try
			{
				return Managers.PersonManager.GetVendorById(vendorId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		/// <summary>
		/// method to return the access area details 
		/// E.Parker
		/// 27.04.11
		/// </summary>
		/// <param name="accessArea"></param>
		/// <returns></returns>
		public static DAL.vw_AccessArea GetMSPAccessArea(int accessArea)
		{
			try
			{
				return Managers.PersonManager.GetMSPAccessArea(accessArea);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.HRPerson> GetPerson()
		{
			try
			{
				return Managers.PersonManager.GetPerson();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.HRPerson> GetPerson(int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetPerson(dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static DAL.HRPerson GetPersonById(int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetPersonById(dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.HRPerson> GetPerson(string searchString1)
		{
			try
			{
				return Managers.PersonManager.GetPerson(searchString1);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static string[] GetPersonArray(string searchString1, Boolean bShowTerminated)
		{
			try
			{
				return Managers.PersonManager.GetPersonArray(searchString1, bShowTerminated);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static List<DAL.vw_HRPerson> GetPersonArray(string searchString1, Boolean bShowTerminated, string costCentre = null)
		{
			try
			{
				return Managers.PersonManager.GetPersonArray(searchString1, bShowTerminated, costCentre);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static string[] GetPersonArrayWithValidIds(string searchString1)
		{
			try
			{
				return Managers.PersonManager.GetPersonArrayWithValidIds(searchString1);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static List<DAL.vw_HRPersonBadge> GetPersonBadges(int dbPeopleID, bool? valid)
		{
			try
			{
				return Managers.PersonManager.GetPersonBadges(dbPeopleID, valid);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}


		public static List<DAL.OutOfOffice> GetOutOfOffice(int oooPersonID)
		{
			try
			{

				return Managers.OutOfOfficeManager.GetOutOfOffice(oooPersonID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
				return null;
			}
		}

		public static List<DAL.vw_MyDBAccess> GetMyAccess(int dbPeopleID, int filter)
		{
			try
			{
				return Managers.PersonManager.GetMyDBAccess(dbPeopleID, filter);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}

		}    

		public static List<DAL.LocationPassOffice> GetUserPassOffice(int UserId)
		{
			try
			{
				return Managers.PersonManager.GetUserPassOffice(UserId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.LocationPassOffice> GetPersonPassOffice(int dbPeopleId)
		{
			try
			{
				return Managers.PersonManager.GetPersonPassOffice(dbPeopleId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}


		public static List<vw_MyRequestsDetailReport> GetMyRequestDetailsReport(int dbPeopleID)
		{
			try
			{
				List<vw_MyRequestsDetailReport> myRequestDetails = Managers.PersonManager.GetMyRequestDetailsReport(dbPeopleID);

				return myRequestDetails;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.vw_MyRequestsDetail> GetMyRequestDetails(int masterRequestID)
		{
			try
			{
				List<DAL.vw_MyRequestsDetail> myRequestDetails = Managers.PersonManager.GetMyRequestDetails(masterRequestID);

				return myRequestDetails;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.vw_MyBadges> GetMyBadges(int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetMyBadges(dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.AccessRequestVisitorOptionsLookup> GetAccessRequestVisitorOptionsLookUp()
		{
			try
			{
				return Managers.PersonManager.GetAccessRequestVisitorOptionsLookUp();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}


		public static List<DAL.GetAccessRequestMasters> GetMyMasterRequestsAll(int dbPeopleId, int? languageID)
		{
			try
			{
				return Managers.PersonManager.GetMyMasterRequestsAll(dbPeopleId, languageID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.vw_AccessRequestMaster> GetMyMasterRequests(int dbPeopleId)
		{
			try
			{
				return Managers.PersonManager.GetMyMasterRequests(dbPeopleId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

        public static Int32 GetMyMasterRequestsCount(int dbPeopleId)
        {
            try
            {
                return Managers.PersonManager.GetMyMasterRequestsCount(dbPeopleId);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);

                return 0;
            }
        }

		public static List<DAL.MyRoles_AccessAreas> GetAccessAreaRoles(int dbPeopleId)
		{
			try
			{
				return Managers.PersonManager.GetAccessAreaRoles(dbPeopleId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.MyRoles_Combined> GetMyRolesCombined(int dbPeopleId)
		{
			try
			{
				return Managers.PersonManager.GetMyRolesCombined(dbPeopleId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.MspRoles> GetMspRoles(int nroleid, int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetMspRoles(nroleid, dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.PassOfficeRoles> GetPassOfficeRoles(int nroleid, int dbPeopleID)
		{
			try
			{
				return Managers.PersonManager.GetPassOfficeRoles(nroleid, dbPeopleID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DBAccessController.DAL.GetVisitorRequest_Result> GetVisitorRequest(string users)
		{
			try
			{
				return Managers.PersonManager.GetVisitorRequest(users);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}

		public static List<DAL.MyRoles_DivisionAreas> GetMyDivisionRoles(int dbPeopleId)
		{
			try
			{
				return Managers.PersonManager.GetMyDivisionRoles(dbPeopleId);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}
		#endregion

		#region Create

		/// <summary>
		/// method to create a new out of office
		/// E.Parker 11.03.11
		/// </summary>
		/// <param name="name"></param>
		/// <param name="ubrCode"></param>
		/// <returns></returns>
		public bool CreateNewOutOfOffice(DateTime LeaveDate, DateTime ReturnDate, int oooPersonID, int SubstituteUserID)
		{
			bool res = Managers.OutOfOfficeManager.CreateOutOfOffice(LeaveDate, ReturnDate, SubstituteUserID, oooPersonID);

			// ac: now send an email informing user of result.
			if (res)
			{
                DAL.HRPerson person = PersonManager.GetPersonById(_dbPeopleID);

                int languageID = PersonManager.GetPersonLanguage(person.dbPeopleID, person.EmailAddress); // receipient's language, default to English
                EmailTemplateShort email = EmailManager.GetEmailTemplate(DBAccessEnums.EmailType.OutOfOfficeSet, languageID);

				// Now email the person to confirm ooo being set.


			    Guid guid = Guid.NewGuid();

				email.Body = email.Body.Replace("[$$NAME$$]", person.Forename);

				//EmailManager.SendEmail(person.EmailAddress, string.Empty, email.Subject, email.Body, guid);
				EmailManager.InsertBatchEmailLog(person.EmailAddress, string.Empty, string.Empty, email.Subject, email.Body, false, guid, email.Type);
			 }

			return res;
		}

		/// <summary>
		/// Method to extend the MSP badge for a set period defined in Months
		/// E.Parker
		/// 20.04.11
		/// </summary>
		/// <param name="dbPeopleID"></param>
		/// <param name="userID"></param>
		/// <param name="oldExpiryDate"></param>
		/// <param name="extendMonths"></param>
		/// <returns></returns>
		public bool ExtendMSPPass(int dbPeopleID, int userID, DateTime oldexpiryDate, int renewalPeriod, string forename, string surname, string company, int BadgeID, string miFairNo, int accessAreaID)
		{

			return Managers.PersonManager.ExtendMSP(dbPeopleID, userID, oldexpiryDate, renewalPeriod, forename, surname, company, BadgeID, miFairNo, accessAreaID);
		}

		#endregion

		#region Remove

		/// <summary>
		/// Delete access request from MyDBAccess Page
		/// E.Parker
		/// </summary>
		/// <param name="RequestId"></param>
		public bool DeleteMyDBAccessRequest(int RequestID, bool Approved, int dbPeopleID)
		{
			bool success = false;

			try
			{
				return Managers.AccessRequestManager.DeleteMyDBAccessRequest(RequestID, Approved, dbPeopleID);
			}
			catch (Exception ex)
			{
				LoggedExceptionId = LogHelper.HandleException(ex);
				LoggedException = ex;
				FailureReason = DBAccessEnums.FailureReason.Exception;
			}

			return success;
		}



		/// <summary>
		/// Delete out of office
		/// E.Parker
		/// </summary>
		/// <param name="RequestId"></param>
		public bool DeleteOutOffice(int oooPersonID)
		{
			bool success = false;

			try
			{
				return Managers.OutOfOfficeManager.DeleteOutOfOffice(oooPersonID);
			}
			catch (Exception ex)
			{
				LoggedExceptionId = LogHelper.HandleException(ex);
				LoggedException = ex;
				FailureReason = DBAccessEnums.FailureReason.Exception;
			}

			return success;
		}

		public static bool DeleteDivisionRoleUser(int divisionRoleUserID)
		{
			bool success = false;

			try
			{
				Managers.PersonManager.DeleteDivisionRoleUser(divisionRoleUserID);
				return true;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}

			return success;
		}

		/// <summary>
		/// Method to return all the vendors within the application
		/// E.Parker
		/// 20.06.11
		/// </summary>
		/// <returns></returns>
		public static List<DAL.GetVendors_Result> GetVendors()
		{
			try
			{
				return Managers.PersonManager.GetVendors();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);

				return null;
			}
		}


		public static bool DeletePassOfficeRoleUser(int passOfficeRoleUserID)
		{
			bool success = false;

			try
			{
				Managers.PersonManager.DeletePassOfficeRoleUser(passOfficeRoleUserID);
				return true;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}

			return success;
		}


		public static bool DeleteMspRoleUser(int mspID)
		{
			bool success = false;

			try
			{
				Managers.PersonManager.DeleteMspRoleUser(mspID);
				return true;
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}

			return success;
		}
		#endregion

		#region Update
		#endregion
	}
}
