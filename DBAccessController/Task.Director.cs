﻿using System;
using System.Collections.Generic;
using spaarks.DB.CSBC.DBAccess.DBAccessController.Managers;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public partial class Director
    {
        #region static get methods

        #region Tasks
        
        public static string GetTaskDetails(int taskId)
        {
            string data = string.Empty;

            try
            {
                data = TaskManager.GetTaskDetails(taskId);                
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
              
            }
            
            return data ?? string.Empty;
        }

        public static int GetMiFareNumber(string MiFareNumber)
        {
            int mfNum = -1;
            try
            {
                mfNum = TaskManager.GetMiFareNumber(MiFareNumber);
               
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }
            return mfNum;
        }

        public static HRPerson GetMiFareNumberAssignment(string MiFareNumber)
        {
            HRPerson toReturn = null;

            try
            {
                toReturn = TaskManager.GetMiFareNumberAssignment(MiFareNumber);

            }
            catch (Exception ex)
            {
                toReturn = null;

                LogHelper.HandleException(ex);
            }

            return toReturn;
        }

        public static Dictionary<string, string> GetMiFareNumberAssignmentDateUser(string MiFareNumber)
        {
            Dictionary<string, string> toReturn = null;

            try
            {
                toReturn = TaskManager.GetMiFareNumberAssignmentDate(MiFareNumber);

            }
            catch (Exception ex)
            {
                toReturn = null;

                LogHelper.HandleException(ex);
            }

            return toReturn;
        }        

		public static int UpdateTaskInfo(int TaskId, string strComment, DateTime? dtEndDate, DateTime? dtStartDate)
		{
			int i = 0;
			try
			{
				i = TaskManager.UpdateTaskInfo(TaskId, strComment, dtEndDate, dtStartDate);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
			return i;
		}

		public static List<DAL.TaskStatusLookup> LoadTaskStatus()
		{
			List<DAL.TaskStatusLookup> items = new List<DAL.TaskStatusLookup>();

			try
			{
				items = TaskManager.GetTaskStatusLookup();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);  
			}
			return items;
		}

		public static List<DAL.GetTaskStatusLookups> LoadTaskStatus(int? languageID)
		{
			List<DAL.GetTaskStatusLookups> items = new List<DAL.GetTaskStatusLookups>();

			try
			{
				items = TaskManager.GetTaskStatusLookup(languageID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
			return items;
		}

		public static List<DAL.TaskSearchOption> LoadSearchOption()
		{
			List<DAL.TaskSearchOption> items = new List<DAL.TaskSearchOption>();

			try
			{
				items = TaskManager.GetTaskSearchOption();
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);  
			}
			return items;
		}

		public static List<DAL.GetTaskSearchOptions> LoadSearchOption(int? languageID)
		{
			List<DAL.GetTaskSearchOptions> items = new List<DAL.GetTaskSearchOptions>();

			try
			{
				items = TaskManager.GetTaskSearchOption(languageID);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}
			return items;
		}

        public static Int32 GetMyTasksCount(int userId, int enabled)
        {
            Int32 result = 0;

            try
            {
                result = TaskManager.GetMyTasksCount(userId, enabled);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return result;
        }

        public static List<DAL.vw_MyTasks> GetMyTasks(int userId, int enabled)
        {
            List<DAL.vw_MyTasks> items = new List<DAL.vw_MyTasks>();

            try
            {
                items = TaskManager.GetMyTasks(userId, enabled);
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);              
            }

            return items;
        }

        public static List<DAL.vw_MyTasksDetails> GetMyTasksDetails(int userId, int enabled, int searchOption, string searchValue)
        {
            List<DAL.vw_MyTasksDetails> items = new List<DAL.vw_MyTasksDetails>();

            try
            {
                //  This is erroring when people are searching using ' as in o'brien, ideally we would use a sql parameter but we are constrained by the entity framework
                //  Zen ticket (2959)
                searchValue = searchValue.Replace("'", "''");

				items = TaskManager.GetMyTasksDetails(userId, enabled, searchOption, searchValue);
                items.OrderBy(a => a.RequestMasterID).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.HandleException(ex);
            }

            return items;
        }

		public static List<DAL.AllDATasks> GetAllDATasks(int dbPeopleID, int enabled)
		{
			List<DAL.AllDATasks> items = new List<DAL.AllDATasks>();

			try
			{
				items = TaskManager.GetAllDATasks(dbPeopleID, enabled);
			}
			catch (Exception ex)
			{
				LogHelper.HandleException(ex);
			}

			return items;
		}
            
        #endregion


        #endregion




        #region Update
		public static void CreatingPendingTaskUpdate(int? taskId, DBAccessEnums.TaskStatus taskStatus, int _dbPeopleID, string comment)
		{
			TaskManager.CreatingPendingTaskUpdate(taskId, taskStatus, _dbPeopleID, comment);
		}

        public int UpdateTaskStatus(int taskId, DBAccessEnums.TaskStatus taskStatus, string comment)
        {
            try
            {
                //  Need to get the task to check the type
                Task task = Managers.TaskManager.GetTask(taskId);
                int res = TaskManager.UpdateTaskStatusByType(taskId, taskStatus, _dbPeopleID, comment, task.TaskTypeId);
                

                // Now email the assigner with the task status.
                if (res == 1)
                {
                    DBAccessEnums.EmailType? emailType = null;                          
                    
                    switch ((DBAccessEnums.TaskType)task.TaskTypeId)
                    {
                        case DBAccessEnums.TaskType.AcceptAARole:
                            emailType = DBAccessEnums.EmailType.AccessAreaApproverRequestResponse;
                            break;
                        case DBAccessEnums.TaskType.AcceptARECRole:
                            emailType = DBAccessEnums.EmailType.AccessAreaRecertifierRequestResponse;
                            break;
                        case DBAccessEnums.TaskType.AcceptBARole:
                            emailType = DBAccessEnums.EmailType.BusinessAreaApproverRequestResponse;
                            break;
                        case DBAccessEnums.TaskType.ApproveAccess:                       
                            emailType = DBAccessEnums.EmailType.ApproveAccessResponse;
                            break;
                        case DBAccessEnums.TaskType.ApprovePerson:
                            emailType = DBAccessEnums.EmailType.ApprovePersonResponse;
                            break;
                        case DBAccessEnums.TaskType.CreatePass:
                            emailType = DBAccessEnums.EmailType.CreatePassResponse;
                            break;
                        case DBAccessEnums.TaskType.AcceptDARole:
                            emailType = DBAccessEnums.EmailType.DivisionAdministrator;
                            break;
                        case DBAccessEnums.TaskType.ApproveSmartCard:
                            {
                                if (taskStatus == DBAccessEnums.TaskStatus.Rejected)
                                    emailType = DBAccessEnums.EmailType.SmartCardRequestRejection;
                                break;
                            }
                    }

					//Excluding the emails confirming the requester about the RA and AA
					if (emailType.HasValue && Convert.ToInt32(emailType.Value) != 14 && Convert.ToInt32(emailType.Value) != 15)
                        //EmailManager.SendEmail(emailType.Value, taskId, _dbPeopleID, Guid.NewGuid(), comment);
						EmailManager.InsertTemplatedBatchEmailLog(emailType.Value, taskId, _dbPeopleID, Guid.NewGuid(), null);

					//also check if the request status 2 or 3(during the processing phase)
					//send the email for the next access approver
					//send the email to the approvers for this request
					
					//Get the request MasterID from Task
					List<int[]> details = Director.GetRequestMasterId(taskId);

					if (details.Count == 1)
					{
						int requestMasterId = Convert.ToInt32(details[0][0]);
						int requestID = Convert.ToInt32(details[0][1]);

						// Now email all relevant approvers (ie people who are set as approvers in task users)
						List<vw_AccessRequestApprover> emailRequestApprovers = GetAccessRequestApprovers(Helper.SafeInt(requestMasterId), requestID);

						EmailManager.SendEmail_OnUpdateTaskStatus(emailRequestApprovers, requestMasterId, requestID, _dbPeopleID, taskId);
					}
                }
                return res;
            }
            catch (Exception ex)
            {
                LoggedExceptionId = LogHelper.HandleException(ex);
                LoggedException = ex;
                FailureReason = DBAccessEnums.FailureReason.Exception;
                return -1;
            }
        }

        #endregion
    }
}
