﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using Spaarks.Common.UtilityManager;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.StoredProcs
{
    public static class StoredProcedureManager
    {
        public static List<sp_PerformAdminSearch_Result> PerformSearch(
            DateTime? revokeAccessStartDate,
            DateTime? revokeAccessEndDate, 
            DateTime? taskCreatedStartDate,
            DateTime? taskCreatedEndDate,
            string personNameSearchType,
            string personNameValueStart,
            string personNameValueEnd,
            string personEmpIdSearchType,
            string personEmpIdValueStart,
            string personEmpIdValueEnd,
            string personEmailSearchType,
            string personEmailValueStart,
            string personEmailalueEnd,
            string personDbDirIdSearchType,
            string personDbDirIdValueStart,
            string personDbDirIdValueEnd,
            DateTime? taskCompletedStartDate,
            DateTime? taskCompletedEndDate,
            string passOffice,
            int? requestStatusId,
            int? countryID
            )
        {
            using (var context = new DBAccessEntities())
            {

                context.CommandTimeout = 180;

                ObjectResult<sp_PerformAdminSearch_Result> results = context.sp_PerformAdminSearch(revokeAccessStartDate, revokeAccessEndDate, taskCreatedStartDate, taskCreatedEndDate, personNameSearchType, personNameValueStart, personNameValueEnd,
                    personEmpIdSearchType, personEmpIdValueStart, personEmpIdValueEnd, personEmailSearchType, personEmailValueStart, personEmailalueEnd, personDbDirIdSearchType, personDbDirIdValueStart, personDbDirIdValueEnd, 
                    taskCompletedStartDate, taskCompletedEndDate, passOffice, requestStatusId, countryID);


                return results.ToList();

            }
        }

        public static List<DAL.RequestTypesForReportSearching> GetRequestTypesForReportSearching()
        {
            var types = (new DAL.DBAccessEntities()).GetRequestTypesForReportSearching();
            return types.ToList();

        }

        public static List<sp_PassOfficeReportSearch_Result> PassOfficeReportSearch
        (
           int? LanguageID , DateTime? taskCreatedStartDate, DateTime? taskCreatedEndDate,
           DateTime?  TaskCompletedStartDate,DateTime? TaskCompletedEndDate,
           string  personNameSearchType,string  personNameValueStart,string  personNameValueEnd, string personEmployeeIdSelector,string personEmployeeIdText,string personEmployeeIdTextBetween
           ,string personEmailSelector,string personEmailText,string personEmailTextBetween,string personDbDirSelector,string personDbDireText,string personDbDirTextBetween
           ,string  requestorNameSearchType, string requestorNameValueStart,string requestorNameValueEnd,string requestorEmployeeIdSelector,string requestorEmployeeIdText
            ,string requestorEmployeeIdTextBetween,string requestorEmailSelector,string requestorEmailText,string requestorEmailTextBetween,string requestorDbDirSelector,string requestorDbDireText
           , string requestorDbDirTextBetween, int? countryID, int? cityID, int? buildingID, int? floorID, int? typeID, int? statusId, string passOffices, int UserdbPeopleID, int? requestStatusFullId, int? accessAreaId
         )
        {
            try
            {
                using (var context = new DBAccessEntities())
                {
                    context.CommandTimeout = 180;

                    ObjectResult<sp_PassOfficeReportSearch_Result> results = 
                        context.sp_PassOfficeReportSearch(LanguageID, taskCreatedStartDate, taskCreatedEndDate, TaskCompletedStartDate, TaskCompletedEndDate, personNameSearchType
                                                            , personNameValueStart, personNameValueEnd , personEmployeeIdSelector,personEmployeeIdText ,personEmployeeIdTextBetween
                                                            ,personEmailSelector,personEmailText,personEmailTextBetween,personDbDirSelector,personDbDireText,personDbDirTextBetween 
                                                            ,requestorNameSearchType,requestorNameValueStart,requestorNameValueEnd,requestorEmployeeIdSelector,requestorEmployeeIdText
                                                            ,requestorEmployeeIdTextBetween,requestorEmailSelector,requestorEmailText,requestorEmailTextBetween,requestorDbDirSelector
                                                            , requestorDbDireText, requestorDbDirTextBetween, countryID, cityID, buildingID, floorID, typeID, statusId, passOffices, UserdbPeopleID, requestStatusFullId, accessAreaId);
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                int LoggedExceptionId = LogHelper.HandleException(ex);
            }
          
                return (new List<sp_PassOfficeReportSearch_Result>());
           
        }

        public static List<sp_DAAccessReportSearch_Result> PerformDAAccessSearch(
          int? langaugeId,
          DateTime? taskCreatedStartDate,
          DateTime? taskCreatedEndDate,
          string personNameSearchType,
          string personNameValueStart,
          string personNameValueEnd,
          string personEmpIdSearchType,
          string personEmpIdValueStart,
          string personEmpIdValueEnd,
          string personEmailSearchType,
          string personEmailValueStart,
          string personEmailalueEnd,
          string personDbDirIdSearchType,
          string personDbDirIdValueStart,
          string personDbDirIdValueEnd,
          string requestorNameSearchType,
          string requestorNameValueStart,
          string requestorNameValueEnd,
          string requestorEmpIdSearchType,
          string requestorEmpIdValueStart,
          string requestorEmpIdValueEnd,
          string requestorEmailSearchType,
          string requestorEmailValueStart,
          string requestorEmailalueEnd,
          string requestorDbDirIdSearchType,
          string requestorDbDirIdValueStart,
          string requestorDbDirIdValueEnd,
          int? taskStatusId,
          int? countryId,
          int? cityId,
          int? buildingId,
          int? floorId,
          bool? restricted,
          int currentUserId, 
          int? requestStatusFullId,
          int? accessAreaId,
          int? requestTypeId
          )
        {
            using (var context = new DBAccessEntities())
            {

                context.CommandTimeout = 180;

                ObjectResult<sp_DAAccessReportSearch_Result> results = context.sp_DAAccessReportSearch(langaugeId, taskCreatedStartDate, taskCreatedEndDate,
                    personNameSearchType, personNameValueStart, personNameValueEnd,
                    personEmpIdSearchType, personEmpIdValueStart, personEmpIdValueEnd,
                    personEmailSearchType, personEmailValueStart, personEmailalueEnd,
                    personDbDirIdSearchType, personDbDirIdValueStart, personDbDirIdValueEnd,
                    requestorNameSearchType, requestorNameValueStart, requestorNameValueEnd,
                    requestorEmpIdSearchType, requestorEmpIdValueStart, requestorEmpIdValueEnd,
                    requestorEmailSearchType, requestorEmailValueStart, requestorEmailalueEnd,
                    requestorDbDirIdSearchType, requestorDbDirIdValueStart, requestorDbDirIdValueEnd,
                    taskStatusId,
                    countryId,
                    cityId,
                    buildingId,
                    floorId,
                    restricted, currentUserId, requestStatusFullId, accessAreaId, requestTypeId);

                return results.ToList();

            }
        }

        public static List<sp_SkippedRequestsReportSearch_Result> SkippedRequestsReportSearch(int? LanguageID, DateTime? logCreatedStartDate, DateTime? logCreatedEndDate, string countries, int UserdbPeopleID)
        {
            try
            {
                using (var context = new DBAccessEntities())
                {
                    context.CommandTimeout = 180;

                    ObjectResult<sp_SkippedRequestsReportSearch_Result> results =
                        context.sp_SkippedRequestsReportSearch(LanguageID, logCreatedStartDate, logCreatedEndDate, countries, UserdbPeopleID);
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                int LoggedExceptionId = LogHelper.HandleException(ex);
            }

            return (new List<sp_SkippedRequestsReportSearch_Result>());

        }
    }
}
