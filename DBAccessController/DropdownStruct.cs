﻿

namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    public struct DropdownStruct
    {
        public int id;
        public string name;

        public DropdownStruct(int p1, string p2) 
        {
            id = p1;
            name = p2;    
        }
    }
}
