﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace spaarks.DB.CSBC.DBAccess.DBAccessController.Fake
{
	//Delete this class when implementing the database
	public class SmartcardNumberStaging
	{
		public string Mifare { get; set; }
		public int UploadStatusID { get; set; }
		public string UploadStatus { get; set; }
		//This is in the form "type_identifier" e.g. "gem2_1A01384505062626"
		public string SerialNumber { get; set; }
	}

	public class SmartcardProvider
	{
		public int ID { get; set; }
		public string ProviderName { get; set; }
		public string Type { get; set; }
		public string Label { get; set; }
		public string	 TypeIdentifier { get; set; }
		public bool UseHexMifare { get; set; }
	}

	public class FakeSmartcardDirector 
	{
#region SmartcardNumbers
		private static List<SmartcardNumberStaging> SmartcardNumbers = new List<SmartcardNumberStaging>()
		{ 
			new SmartcardNumberStaging(){Mifare = "6BF9F9DF", SerialNumber = "gem2_B98ECA92A08745E9", UploadStatusID=2, UploadStatus="Already Exists"},
			new SmartcardNumberStaging(){Mifare = "EB98F9DF", SerialNumber = "gem2_07A0EE3B6FCFF36B", UploadStatusID=3, UploadStatus="Duplicate In File"},
			new SmartcardNumberStaging(){Mifare = "EB98F9DF", SerialNumber = "gem2_07A0EE3B6FCFF36B", UploadStatusID=3, UploadStatus="Duplicate In File"},
			new SmartcardNumberStaging(){Mifare = "9BF4F9DF", SerialNumber = "gem2_CA0863E9GDE9A387", UploadStatusID=4, UploadStatus="Invalid Characters"},
			new SmartcardNumberStaging(){Mifare = "9B25F8DF", SerialNumber = "gem2_D01E9472DE952D5E", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1B4EE4F2", SerialNumber = "gem2_62D7A923C9F4BC4F", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "EB0F3FEF", SerialNumber = "gem2_BEE26949FA3DCAFD", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "4BEF84F7", SerialNumber = "gem2_4952F6C5D69B92EC", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1BD885F7", SerialNumber = "gem2_82B445C4ABEB2FFE", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0B8185F7", SerialNumber = "gem2_F6CD1981D0146509", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "8B4084F7", SerialNumber = "gem2_7C57AFD55A5CE2DF", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "EB06E6F2", SerialNumber = "gem2_2819DE0F50FB23E2", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1B212DF5", SerialNumber = "gem2_CADFC504C48FBE8D", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "6B4916EF", SerialNumber = "gem2_D3A7BC7CD0703382", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "FB253DEF", SerialNumber = "gem2_9A4AEED66C142204", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2B01F6DF", SerialNumber = "gem2_8B552108F478DB26", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0B64F9DF", SerialNumber = "gem2_1B04A162075A6EDE", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0B7DF7DF", SerialNumber = "gem2_98D0D2BC99FE7FAE", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "5B09F6DF", SerialNumber = "gem2_6C3A507401B0828F", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "7B27FADF", SerialNumber = "gem2_FECC1D989927D334", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "EB1986F7", SerialNumber = "gem2_DAA8DEAF321B1D91", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "5B9F84F7", SerialNumber = "gem2_C28D6DC9F0A370F0", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "9BDB84F7", SerialNumber = "gem2_CF03B1FE7D975292", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "9B7D86F7", SerialNumber = "gem2_F812CCEEFCF392F3", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "9B8D86F7", SerialNumber = "gem2_5F65B0294FA0F7BF", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1B0EFADF", SerialNumber = "gem2_901EED5B3E349601", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0BE8F6DF", SerialNumber = "gem2_0EE719B183793B53", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "4B67F9DF", SerialNumber = "gem2_57FD9CDA07A6470D", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "5B7EFBDF", SerialNumber = "gem2_621A7DD0A173259C", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "CBF5F3DF", SerialNumber = "gem2_CB0C93014E2E953B", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "5B4EF8DF", SerialNumber = "gem2_64CA87E5D2DEEFB8", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "AB8FFADF", SerialNumber = "gem2_2FD4BF888908AF95", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "CBC1F7DF", SerialNumber = "gem2_364B341A1AC00F37", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1B8EF9DF", SerialNumber = "gem2_A9CC4979AF3B299E", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "DBE884F7", SerialNumber = "gem2_2F03EE1841EE0F9B", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2BD9F6DF", SerialNumber = "gem2_9B3600C7FA86E2B9", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "1B9AF6DF", SerialNumber = "gem2_11159BFB7D2C6270", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "FB0CFBDF", SerialNumber = "gem2_A9A082E76ADC5BA6", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2BB784F7", SerialNumber = "gem2_A68034097CF6DD04", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2B9585F7", SerialNumber = "gem2_7C692A9E2D9EC49C", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "DB6BFBDF", SerialNumber = "gem2_0175AFE03EA337AB", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2BD4F6DF", SerialNumber = "gem2_CA6CC728C7634C5C", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0B02F6DF", SerialNumber = "gem2_C641E08CD75B0687", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "EB3E18EF", SerialNumber = "gem2_BDEF1AB04CFEA69F", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "2B3119EF", SerialNumber = "gem2_3FE5B1374BB8C3FE", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "BB9578F7", SerialNumber = "gem2_918E3C90F983CEE1", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "3B097BF7", SerialNumber = "gem2_AECB179F47DCD12D", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "0BE67AF7", SerialNumber = "gem2_4C06E59EAD290419", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "3BF4F5DF", SerialNumber = "gem2_FF9730E35A77A1AB", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "FBF3F8DF", SerialNumber = "gem2_810825C508891842", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "8BACE5F2", SerialNumber = "gem2_02695D5A656ADF4B", UploadStatus="Success"},
			new SmartcardNumberStaging(){Mifare = "DBC318EF", SerialNumber = "gem2_F135DDD6378F1044", UploadStatus="Success"}
		};
#endregion

		private static List<SmartcardProvider> SmartcardProviders = new List<SmartcardProvider>()
		{
			new SmartcardProvider(){ ID=1, ProviderName = "Gemalto", Type=".NET V2+", Label="CF.NET P11", TypeIdentifier="gem2", UseHexMifare=true},
			new SmartcardProvider(){ ID=3, ProviderName = "G&D", Type="Sm@rtCafé 3.1", Label="GD SmartCafe 3.1", TypeIdentifier="gd31", UseHexMifare=true}
		};

		public static string GetSmartcardOwner(string smartcardIdentifier)
		{
			return "9095809";
		}

		public static string GetSmartcardStatus(string smartcardIdentifier)
		{
			return DBAccessEnums.SmartcardStatus.Active.ToString();
		}

		public static string GetSmartcardKeyID(string smartcardIdentifier)
		{
			return "1234";
		}

		public static List<SmartcardProvider> GetSmartCardProviders()
		{
			if (SmartcardProviders.Where(a => a.ID == 0).Count() == 0)
				SmartcardProviders.Insert(0, new SmartcardProvider { ID = 0, ProviderName = "Select a Provider..." });

			return SmartcardProviders;
		}

		public static List<SmartcardNumberStaging> GetSmartcardNumbersStaging()
		{
			return SmartcardNumbers;
		}

		public static int GetDuplicatesCount()
		{
			return 2;
		}

		public static int GetExistingCount()
		{
			return 1;
		}

		/// <summary>
		/// Returns Error message, empty if successful
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public static string UploadSmartcardNumbers(StreamReader reader, int providerID)
		{
			//NB Clear the staging table, Save them to the staging table then run a stored procedure to identify duplicates and existing numbers
			return string.Empty;
		}

		public static string ConfirmSmartcardUpload()
		{
			//NB Copy the staging data to the smartcard table, ignoring duplicates and existing numbers. Empty the staging table
			return "51 Smartcard Numbers were uploaded successfully";
		}

		public static int GetInvalidCount()
		{
			return 0;
		}
	}
}
