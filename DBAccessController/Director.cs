﻿using System;
using System.Transactions;


namespace spaarks.DB.CSBC.DBAccess.DBAccessController
{
    /// <summary>
    /// The Director is the facade layer through which all calls to the controllers must be directed. 
    /// Business Logic, input validation and error handling should all happen within this class
    ///
    /// Use the appropriate partial class file for your code, unless it does not fit into any of thos categories
    /// </summary>
    public partial class Director
    {
        #region fields
        protected int _dbPeopleID;
        
        public Exception LoggedException;
        public int LoggedExceptionId;
        public DBAccessEnums.FailureReason FailureReason;

        public string ErrorMessage { get; private set; }

        #endregion

        #region Constructors

        public Director()
        {

        }

        public Director(int dbPeopleID)
        {
            _dbPeopleID = dbPeopleID;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Create a transaction with standardised options defined
        /// </summary>
        /// <returns>the transaction scope</returns>
        public TransactionScope startTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = TimeSpan.MaxValue;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);          
        }

        #endregion

        #region GET Methods

            
        #endregion

        #region Create Methods
        
        

        #endregion

        #region Remove Methods
        

       
       
        #endregion

        #region Update Methods

       
        #endregion
    }
}
