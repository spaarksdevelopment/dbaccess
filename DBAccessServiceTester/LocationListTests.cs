﻿using System;
using System.Web;
using System.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBAccessService1;

namespace DBAccessServiceTester
{
    [TestClass]
    public class LocationListTests
    {
        private DBAccessService1.Request _dbAccessServiceRequest;

        [TestInitialize]
        public void DBAccessServiceTester()
        {
            _dbAccessServiceRequest = new Request();
        }

        [TestMethod]
        public void GetRegionsAllEnabled_Valid()
        {
            string[] regions;
            _dbAccessServiceRequest.GetRegionsAllEnabled(out regions);

            Assert.IsTrue(regions.Length > 0);

        }

        [TestMethod]
        public void GetCountriesByRegion_ValidContainsUK()
        {
            string[] countries;
            _dbAccessServiceRequest.GetCountriesByRegion(11, out countries);

            Assert.IsTrue(countries.Length > 0);

            bool sUnitedKingdom = false;
            foreach(string i in countries){
                if (i == "59|United Kingdom")
                    sUnitedKingdom = true;
            }

            Assert.IsTrue(sUnitedKingdom);
        }

        [TestMethod]
        public void GetCountriesByRegion_ValidDeosntContainUK()
        {
            string[] countries;
            _dbAccessServiceRequest.GetCountriesByRegion(12, out countries);

            Assert.IsTrue(countries.Length > 0);

            bool sUnitedKingdom = false;
            foreach (string i in countries)
            {
                if (i == "59|United Kingdom")
                    sUnitedKingdom = true;
            }

            Assert.IsFalse(sUnitedKingdom);
        } 
    }
}
