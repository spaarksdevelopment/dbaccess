﻿namespace HRFeedMessageQueuePusher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_messageType = new System.Windows.Forms.ComboBox();
            this.m_generate = new System.Windows.Forms.Button();
            this.m_textInput = new System.Windows.Forms.TextBox();
            this.m_status = new System.Windows.Forms.Label();
            this.m_currentList = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // m_messageType
            // 
            this.m_messageType.FormattingEnabled = true;
            this.m_messageType.Items.AddRange(new object[] {
            "Trigger",
            "Acknowledgement"});
            this.m_messageType.Location = new System.Drawing.Point(37, 48);
            this.m_messageType.Name = "m_messageType";
            this.m_messageType.Size = new System.Drawing.Size(121, 21);
            this.m_messageType.TabIndex = 0;
            // 
            // m_generate
            // 
            this.m_generate.Location = new System.Drawing.Point(583, 496);
            this.m_generate.Name = "m_generate";
            this.m_generate.Size = new System.Drawing.Size(75, 23);
            this.m_generate.TabIndex = 1;
            this.m_generate.Text = "Generate";
            this.m_generate.UseVisualStyleBackColor = true;
            // 
            // m_textInput
            // 
            this.m_textInput.Location = new System.Drawing.Point(37, 133);
            this.m_textInput.Multiline = true;
            this.m_textInput.Name = "m_textInput";
            this.m_textInput.Size = new System.Drawing.Size(621, 336);
            this.m_textInput.TabIndex = 2;
            // 
            // m_status
            // 
            this.m_status.AutoSize = true;
            this.m_status.Location = new System.Drawing.Point(34, 501);
            this.m_status.Name = "m_status";
            this.m_status.Size = new System.Drawing.Size(0, 13);
            this.m_status.TabIndex = 3;
            // 
            // m_currentList
            // 
            this.m_currentList.Location = new System.Drawing.Point(710, 28);
            this.m_currentList.Name = "m_currentList";
            this.m_currentList.Size = new System.Drawing.Size(510, 561);
            this.m_currentList.TabIndex = 4;
            this.m_currentList.UseCompatibleStateImageBehavior = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 627);
            this.Controls.Add(this.m_currentList);
            this.Controls.Add(this.m_status);
            this.Controls.Add(this.m_textInput);
            this.Controls.Add(this.m_generate);
            this.Controls.Add(this.m_messageType);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_messageType;
        private System.Windows.Forms.Button m_generate;
        private System.Windows.Forms.TextBox m_textInput;
        private System.Windows.Forms.Label m_status;
        private System.Windows.Forms.ListView m_currentList;
    }
}

