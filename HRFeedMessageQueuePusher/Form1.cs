﻿using HRFeedMessageQueuePusher.Helpers;
using SolaceSystems.Solclient.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRFeedMessageQueuePusher
{
    public partial class Form1 : Form
    {

        IContext context = null;
        ISession session = null;
        IBrowser browser = null;
        IQueue queue = null;
        IQueue queueAck = null;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {            
            m_messageType.SelectedIndexChanged += m_messageType_SelectedIndexChanged;
            m_generate.Click += m_generate_Click;

            m_messageType.SelectedIndex = 0;
            m_messageType_SelectedIndexChanged(m_messageType, null);

            string queueName = ConfigurationManager.AppSettings["QueueName"];
            string queueNameAck = ConfigurationManager.AppSettings["QueueNameAck"];

            ContextFactoryProperties cfp = new ContextFactoryProperties();
            // Set log level.
            cfp.SolClientLogLevel = SolLogLevel.Info;
            // Log errors to console.
            cfp.LogToConsoleError();
            // Must init the API before using any of its artifacts.
            ContextFactory.Instance.Init(cfp);

            ContextProperties contextProps = new ContextProperties();
            SessionProperties sessionProps = SolaceUtility.NewSessionPropertiesFromConfig();

            context = ContextFactory.Instance.CreateContext(contextProps, null);
         
            session = context.CreateSession(sessionProps, SolaceUtility.HandleMessageEvent, SolaceUtility.HandleSessionEvent);
            session.Connect();

            queue = ContextFactory.Instance.CreateQueue(queueName);
            queueAck = ContextFactory.Instance.CreateQueue(queueNameAck);

            BrowserProperties browserProps = new BrowserProperties();
            browser = session.CreateBrowser(queue, browserProps);

            RefreshList();
        }


        private void RefreshList()
        {
            IMessage msg = null;

            m_currentList.Items.Clear();

            while ((msg = browser.GetNext()) != null)
            {
                //Convert sbyte[] to byte[]
                byte[] byteData = Array.ConvertAll(msg.XmlContent, (a) => (byte)a);
                System.IO.Stream stream = new System.IO.MemoryStream(byteData);

                System.IO.Compression.GZipStream gstream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress, true);
                byte[] resultBytes = new byte[byteData.Length];

                //fill the resultBytes with decompressed byte array
                int readResult = gstream.Read(resultBytes, 0, resultBytes.Length);

                //you got the actual string here
                string stringFromByteData = Encoding.ASCII.GetString(resultBytes);

                m_currentList.Items.Add(new ListViewItem(stringFromByteData));             
            }

        }

        private void m_generate_Click(object sender, EventArgs e)
        {

            IMessage msg = ContextFactory.Instance.CreateMessage();
            byte[] bytesToSend = Encoding.ASCII.GetBytes(m_textInput.Text);
            sbyte[] byteData = Array.ConvertAll(bytesToSend, (a) => (sbyte)a);
            msg.SetXmlContent(byteData);

            msg.DeliveryMode = MessageDeliveryMode.Persistent;

            if (m_messageType.SelectedIndex == 0)
            {
                msg.Destination = queue;
            }
            else
            {
                msg.Destination = queueAck;
            }

            if (session.Send(msg) == ReturnCode.SOLCLIENT_OK)
            {
                m_status.Text = "Message sent";
            }

            RefreshList();
        }

        private void m_messageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string triggerMessage = @"<?xml version=""1.0""?>
<AccessManagementRequest xmlns=""http://xmlns.oracle.com/AccessManagementPub"" xmlns:ns1=""http://xmlns.oracle.com/AccessManagementPub"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
<ns1:TRANSACTION_ID>106</ns1:TRANSACTION_ID>
<ns1:TRANSACTION_TIMESTAMP>2013-11-20T11:52:00.000+01:00</ns1:TRANSACTION_TIMESTAMP>
<ns1:TRIGGER_TYPE>JOIN</ns1:TRIGGER_TYPE>
<ns1:TRIGGER_ACTION>ADD</ns1:TRIGGER_ACTION>
<ns1:PRIORITY_FLAG>10</ns1:PRIORITY_FLAG>
<ns1:HR_ID>8000067</ns1:HR_ID>
<ns1:FIRST_NAME>Solomon</ns1:FIRST_NAME>
<ns1:LAST_NAME>Quetuket</ns1:LAST_NAME>
<ns1:CREATE_ACCESS_TIMESTAMP>{0}</ns1:CREATE_ACCESS_TIMESTAMP>
<ns1:ORGANIZATIONAL_RELATIONSHIP>EMP</ns1:ORGANIZATIONAL_RELATIONSHIP>
<ns1:COST_CENTER>9731124324</ns1:COST_CENTER>
<ns1:LEGAL_ENTITY>9731</ns1:LEGAL_ENTITY>
<ns1:UBR_CODE>G_7740</ns1:UBR_CODE>
<ns1:LOCATION_ID>USA00020</ns1:LOCATION_ID>
<ns1:LOCATION_COUNTRY>USA</ns1:LOCATION_COUNTRY>
<ns1:LOCATION_CITY>Jacksonville</ns1:LOCATION_CITY>
<ns1:MANAGER_HR_ID>6009178</ns1:MANAGER_HR_ID>
<ns1:REACTIVATION_FLAG>Y</ns1:REACTIVATION_FLAG>
<ns1:REVOKE_ACCESS_TIMESTAMP><{1}</REVOKE_ACCESS_TIMESTAMP>
<ns1:TERMINATION_DATE>{2}</TERMINATION_DATE>
</AccessManagementRequest>";

            string acknowldgementMessage = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<receiveInput_Consume_Message_InputVariable>
<part>
<svcAdminAcknowledgementRequest xmlns=""http://xmlns.oracle.com/svcAdminAcknowledgement"">
<TRANSACTION_ID>516</TRANSACTION_ID>
<ACKNOWLEDGEMENT_TIMESTAMP>{0}</ACKNOWLEDGEMENT_TIMESTAMP>
</svcAdminAcknowledgementRequest>
</part>
</receiveInput_Consume_Message_InputVariable>
";

            TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
            string messageDate = DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + utcOffset.ToString("'+'hh':'mm");

            //2013-11-04T11:56:27+01:00
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                m_textInput.Text = String.Format(triggerMessage, messageDate, messageDate, messageDate);
            }
            else
            {
                m_textInput.Text = String.Format(acknowldgementMessage, messageDate);
            }
                
            
        }
    }
}
