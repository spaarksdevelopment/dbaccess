﻿using System;
using System.Configuration;

namespace DBAccess.Model
{
    //TODO - Promote this to the Utility project
    public static class ConfigurationParameters
    {
        public static string GetStringParameter(string parameterName)
        {
            return ConfigurationManager.AppSettings[parameterName];
        }

        public static int? GetIntParameter(string parameterName)
        {
            string parameterString = GetStringParameter(parameterName);

            int parameterInt;
            bool success = Int32.TryParse(parameterString, out parameterInt);
            if(success)
                return parameterInt;

            return null;
        }
    }
}
