﻿namespace DBAccess.Model.Enums
{
    public enum AccessRequestStatus
    {
        Draft = 1, Submitted = 2, InProgress = 3, Approved = 4, Actioned = 5, Rejected = 10, Cancelled = 11, Failed = 12
    }

    public enum RequestType
    {
        NewPass = 1,
        Access = 2,
        Service = 3,
        Visitor = 4,
        PassMSP = 5,
        AccessMSP = 6,
        DivisionAdministratorRole = 7,
        AccessAreaApproverRole = 8,
        AccessAreaRecertifierRole = 9,
        RequestApproval = 10,
        AccessPO = 11,
        DivisionOwnerRole = 12,
        MSP = 13,
        StopPass = 14,
        Offboarding = 15,
        RevokeAccess = 16,
        ReactivateAccess = 17
    };

    public enum RoleType
    {
        DivisionOwner = 7,
        DivisionAdministrator = 3,
        BusinessAreaApprover = 4,
        AccessApprover = 5,
        AccessRecertifier = 8,
        PassOfficeOperator = 6
    };

    public enum DeliveryOption
    {
        Collect = 1, Contact = 2
    };

    public enum FailureReason
    {
        Exception = 1,
        InvalidRequestMasterID = 2,
        PersonAlreadyOnRequest = 3,
        InsufficientData = 4,
        NotEnoughApproversAvailable = 5,
        RequestPersonInActive = 6,
        InvalidRequestID = 7
    };

    public enum EmailType
    {
        None = 0,
        ToRequestee = 1,
        ToRequester = 2,
        BusinessAreaApproverRequest = 3,
        BusinessAreaApproverRequestResponse = 4,
        PendingRoleRecertifications = 5,
        AccessAreaApproverRequest = 6,
        BusinessAreaApproverRemoval = 7,
        AccessAreaApproverRemoval = 8,
        PendingRoleRecertificationsEscalation = 9,
        PendingAccessAreasRecertification = 10,
        AccessRequest = 11,
        AccessAreaApproverRequestResponse = 12,
        AccessAreaRecertifierRequestResponse = 13,
        ApproveAccessResponse = 14,
        ApprovePersonResponse = 15,
        CreatePassResponse = 16,
        AccessAreaRecertifierRemoval = 17,
        AccessAreaRecertifierRequest = 18,
        OutOfOfficeSet = 19,
        OutOfOfficeUnset = 20,
        OutOfOfficeAboutToExpire = 21,
        FlagPassRequestToPO = 22,
        DivisionAdministrator = 23,
        CreatePassRequest = 24,
        PassToRequestee = 25,
        PassOfficeUser = 26,
        DivisionOwner = 27,
        AccessAreaAddition = 28,
        BusinessAreaAddition = 29,
        MSPAssignRole = 30,
        Recommendation = 31,
        RequestApproval = 32,
        AccessApproval = 33,
        SwitzerlandEmail = 35,
        SmartCardApproval = 41
    };

    public enum DeleteDivisionRoleResult
    {
        Success = 0,
        SuccessRemoveRequestMasterIDDA = 1,
        SuccessRemoveRequestMasterIDDO = 2,
        DivisionAdministratorCouldNotBeDeleted = 3,
        InsufficientDivisionAdministratorsToDelete = 4,
        DivisionOwnerCouldNotBeDeleted = 5
    }

    public enum AddDivisionRoleUserResult
    {
        Success = 0,
        PersonIsDisabled = 1,
        PersonHasNoEmail = 2,
        InvalidDBPeopleIDForDivisionAdministrator = 3,
        PersonAlreadyDivisionAdministratorForThisDivision = 4,
        MaximumNumberOfDivisonAdministratorsReached = 5,
        InvalidDBPeopleIDForDivisionOwner = 6,
        DivisionAlreadyHasADivisionOwner = 7,
        ProblemCreatingRequest = 8
    }
}
