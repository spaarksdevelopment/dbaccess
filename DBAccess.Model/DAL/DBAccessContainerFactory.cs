﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.Model.DAL
{
    public class DBAccessContainerFactory : IDBAccessContainerFactory
    {
        public virtual IDBAccessContainer GetDBAccessContainer()
        {
            return new DBAccessContainer();
        }
    }
}
