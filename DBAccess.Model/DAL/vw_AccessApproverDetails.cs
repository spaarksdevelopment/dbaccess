//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBAccess.Model.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_AccessApproverDetails
    {
        public int RequestPersonID { get; set; }
        public string RequesterName { get; set; }
        public string RequesterEmail { get; set; }
        public string EmailAddress { get; set; }
        public string ApplicantName { get; set; }
        public string ApprovedBy { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string UbrCode { get; set; }
        public string UbrName { get; set; }
        public string CountryName { get; set; }
        public int RequestID { get; set; }
    }
}
