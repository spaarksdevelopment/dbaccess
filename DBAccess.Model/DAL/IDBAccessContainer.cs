﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Objects;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DBAccess.Model.DAL
{
    public interface IDBAccessContainer : IDisposable
    {
        #region DbSets
        IDbSet<vw_AccessRequestPerson> vw_AccessRequestPerson { get; set; }
        IDbSet<AccessRequestMaster> AccessRequestMasters { get; set; }
        IDbSet<vw_MasterAccessRequest> vw_MasterAccessRequest { get; set; }
        IDbSet<CorporateDivisionAccessArea> CorporateDivisionAccessAreas { get; set; }
        IDbSet<HRPerson> HRPersons { get; set; }
        IDbSet<CorporateDivisionAccessAreaApprover> CorporateDivisionAccessAreaApprovers { get; set; }
        IDbSet<HRClassification> HRClassifications { get; set; }
        IDbSet<vw_BadgeRequest> vw_BadgeRequest { get; set; }
        IDbSet<AccessRequest> AccessRequests { get; set; }
        IDbSet<LocationPassOffice> LocationPassOffices { get; set; }
        IDbSet<vw_AccessRequest> vw_AccessRequest { get; set; }
        IDbSet<CorporateDivisionRoleUser> CorporateDivisionRoleUsers { get; set; }
        IDbSet<LocationCountry> LocationCountries { get; set; }
        IDbSet<CorporateDivision> CorporateDivisions { get; set; }
        IDbSet<AccessRequestPerson> AccessRequestPersons { get; set; }
        IDbSet<SmartcardNumber> SmartcardNumbers { get; set; }
        IDbSet<HRPersonBadge> HRPersonBadges { get; set; }
        IDbSet<SmartcardServiceAudit> SmartcardServiceAudits { get; set; }
        IDbSet<vw_AccessAreaRequest> vw_AccessAreaRequest { get; set; }
        IDbSet<XLImportEntity> XLImportEntities { get; set; }
        IDbSet<XLImportFile> XLImportFiles { get; set; }
        IDbSet<XLImportSheetColumnMapping> XLImportSheetColumnMappings { get; set; }
        IDbSet<XLImportStageAccessArea> XLImportStageAccessAreas { get; set; }
        IDbSet<XLImportStageAccessAreaApprover> XLImportStageAccessAreaApprovers { get; set; }
        IDbSet<XLImportStageAccessAreaRecertifier> XLImportStageAccessAreaRecertifiers { get; set; }
        IDbSet<XLImportStageBuilding> XLImportStageBuildings { get; set; }
        IDbSet<XLImportStageCity> XLImportStageCities { get; set; }
        IDbSet<XLImportStageCorporateArea> XLImportStageCorporateAreas { get; set; }
        IDbSet<XLImportStageDivision> XLImportStageDivisions { get; set; }
        IDbSet<XLImportStageDivisionRole> XLImportStageDivisionRoles { get; set; }
        IDbSet<XLImportStagePassOffice> XLImportStagePassOffices { get; set; }
        IDbSet<XLImportStagePassOfficeUser> XLImportStagePassOfficeUsers { get; set; }
        IDbSet<XLImportValidationRule> XLImportValidationRules { get; set; }
        IDbSet<XLImportValidationError> XLImportValidationErrors { get; set; }
        IDbSet<vw_AccessRequestApprover> vw_AccessRequestApprover { get; set; }
        IDbSet<vw_AccessApproverDetails> vw_AccessApproverDetails { get; set; }

        #endregion

        #region Generic DbContext Methods
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet Set(Type entityType);
        int SaveChanges();
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbEntityEntry Entry(object entity);
        #endregion

        #region Sprocs

        int InsertRequest(int? createdByUserID, int? requestTypeID, int? accessAreaID,
            int? requestPersonID, DateTime? startDate, DateTime? endDate,
            int? badgeID, int? divisionID, int? iPassOfficeID, int? businessAreaID,
            ObjectParameter requestID, int? externalSystemRequestID, int? externalSystemID,
            int? classificationID);

        ObjectResult<GetBadgeJustificationReasons_Result> GetBadgeJustificationReasons(int? languageID);
        ObjectResult<LocationPassOffice> GetUserPassOffices(int? dbPeopleID);

        int InsertAccessRequestDeliveryDetail(int? requestMasterID, short? deliveryOptionID,
            int? passOfficeID, string contactEmail, string contactTelephone, ObjectParameter result);

        int SubmitRequestMaster(int? dbPeopleID, int? requestMasterID, ObjectParameter success);

        int InsertRequestMaster(int? createdByID, ObjectParameter requestMasterID);

        int InsertRequestPerson(int? createdByUserID,
            int? requestMasterID, int? dbPeopleID,
            ObjectParameter requestPersonID, bool? bCheckBusinessArea);

        int InsertEmailLog(string to, string from, string cC, string subject, string body, bool? isSent,
            Guid? guid, int? emailTypeId, ObjectParameter result);

        ObjectResult<EmailTemplateShort> GetEmailTemplateForLanguage(int? templateType,
            int? languageID);

        int GetPersonLanguage(int? dbPeopleID, string emailAddress, ObjectParameter languageID);

        int ProcessUploadedUsers(int? dbPeopleID, string users, int? masterId, int? requestType,
            ObjectParameter requestMasterId);

        int IsPersonOutOfOfficeTodayWrapper(int? dbpeopleID, ObjectParameter result);

        ObjectResult<DivisionRoleUser> GetDivisionRoleUsers(int? divisionID);

        int InsertDivisionRoleUser(int? divisionID, int? dBPeopleID, int? securityGroupID, ObjectParameter result);

        int CreateEditDivision(int? divisionID, int? countryID, string name, string uBR,
            bool? enabled, int? recertPeriod, bool? isNew, int? createdBy,
            int? iRequestMasterID_DA, int? iRequestMasterID_DO, bool? isSubdivision,
            int? subdivisionParentDivisionID, ObjectParameter result);

        int XLImportInsertStageTableRecords(string table, string columns, string rows, ObjectParameter success);

        ObjectResult<XLImportGetHistory_Result> XLImportGetHistory();

        int XLImportCopyAllData(Nullable<int> fileID, ObjectParameter errorMessage);

        ObjectResult<XLImportValidateFile_Result> XLImportValidateFile(Nullable<int> fileID);

        int XLImportClearStagingTables();

        ObjectResult<XLImportGetSummary_Result> XLImportGetSummary(Nullable<int> fileID);

        int XLImportRollbackFile(Nullable<int> fileID, ObjectParameter errorMessage);

        #endregion
    }
}
