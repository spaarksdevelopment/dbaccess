//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBAccess.Model.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_MasterAccessRequest
    {
        public int AccessAreaID { get; set; }
        public string AccessAreaTypeName { get; set; }
        public string AccessAreaTypeDescription { get; set; }
        public bool AccessAreaTypeEnabled { get; set; }
        public string AccessAreaName { get; set; }
        public string FloorName { get; set; }
        public string BuildingName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int RequestMasterID { get; set; }
        public Nullable<int> LandlordID { get; set; }
        public string Approvers { get; set; }
    }
}
