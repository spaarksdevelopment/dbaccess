﻿namespace DBAccess.Model.DAL
{
    public interface IDBAccessContainerFactory
    {
        IDBAccessContainer GetDBAccessContainer();
    }
}
