//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBAccess.Model.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_BadgeRequest
    {
        public int RequestMasterID { get; set; }
        public Nullable<int> MasterCreatedByUserID { get; set; }
        public System.DateTime MasterCreated { get; set; }
        public int RequestPersonID { get; set; }
        public Nullable<int> dbPeopleID { get; set; }
        public string FulldbPeopleID { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string CostCentre { get; set; }
        public string CostCentreName { get; set; }
        public string UBR_Code { get; set; }
        public string UBR_Name { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<int> VendorID { get; set; }
        public string VendorName { get; set; }
        public Nullable<int> DivisionID { get; set; }
        public string DivisionName { get; set; }
        public Nullable<int> BusinessAreaID { get; set; }
        public string BusinessAreaName { get; set; }
        public bool IsExternal { get; set; }
        public int RequestID { get; set; }
        public int RequestStatusID { get; set; }
        public Nullable<int> BadgeID { get; set; }
        public string BadgeType { get; set; }
        public string MiFareNumber { get; set; }
        public Nullable<int> LandlordID { get; set; }
        public string LandlordName { get; set; }
        public Nullable<int> BuildingID { get; set; }
        public string BuildingName { get; set; }
        public Nullable<int> PersonCreatedByUserID { get; set; }
        public System.DateTime PersonCreated { get; set; }
        public string Issue { get; set; }
        public string BadgeTag { get; set; }
        public Nullable<int> CreatedByID { get; set; }
        public System.DateTime Created { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string CountryName { get; set; }
        public string Comment { get; set; }
        public Nullable<short> DeliveryOptionID { get; set; }
        public Nullable<int> PassOfficeID { get; set; }
        public string ContactEmail { get; set; }
        public string ContactTelephone { get; set; }
        public string DeliveryOption { get; set; }
        public string PassOfficeName { get; set; }
        public string PassOfficeCode { get; set; }
        public string PassOfficeAddress { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> dbDirID { get; set; }
        public Nullable<int> CountryDefaultPassOffice { get; set; }
    }
}
