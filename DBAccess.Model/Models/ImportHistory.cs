﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.Model.Models
{
    public class ImportHistory
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ImportedBy { get; set; }
        public string ImportedByName { get; set; }
        public DateTime ImportedDate { get; set; }
    }
}
