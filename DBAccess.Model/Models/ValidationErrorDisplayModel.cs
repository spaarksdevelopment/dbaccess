﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.Model.Models
{
    public class ValidationErrorDisplayModel
    {
        public string Entity { get; set; }
        public string SheetName { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }
        public string RowNumbers { get; set; }
    }
}
