﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccess.Model.Models
{
    public class ValidationRuleDisplayModel
    {
        public string SheetName { get; set; }
        public string ValidationType { get; set; }
        public string BusinessRule { get; set; }

    }
}
