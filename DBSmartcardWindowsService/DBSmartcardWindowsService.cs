﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Aduvo.Common.UtilityManager;
using spaarks.DB.CSBC.DBAccess.DBAccessController;
using spaarks.DB.CSBC.DBAccess.DBAccessController.DAL;
using System.Threading;
using System.Configuration;
using System.Net;

namespace DBSmartcardWindowsService
{
	public partial class DBSmartcardWindowsService : ServiceBase
	{
		private const string _AppName = "DBSmartcardWindowsService";
		private int _ActionsProcessed = 0;
		private Timer m_ScheduleTimer;

		public void WriteEventToWindowsLog(string app, string logEntry, EventLogEntryType type)
		{
			if (!System.Diagnostics.EventLog.SourceExists(app))
				System.Diagnostics.EventLog.CreateEventSource(app, "Application");

			EventLog dbSmartcardWindowsServiceEventLog = new EventLog();
			dbSmartcardWindowsServiceEventLog.Source = app;
			dbSmartcardWindowsServiceEventLog.WriteEntry(logEntry, type);
		}

		public DBSmartcardWindowsService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			try
			{
				int interval = 60;
				string serviceInterval = ConfigurationManager.AppSettings["ServiceIntervalMinutes"];

				if (serviceInterval != null)
					interval = Convert.ToInt32(serviceInterval);

				m_ScheduleTimer = new Timer(new TimerCallback(TimerScheduleProc));

				m_ScheduleTimer.Change(0, (interval * 1000 * 60));

				WriteEventToWindowsLog(_AppName, "ServiceInterval=" + interval.ToString(), EventLogEntryType.Information);
			}
			catch(Exception ex)
			{
				WriteEventToWindowsLog(_AppName, ex.Message, EventLogEntryType.Error);
				if(ex.InnerException !=null)
					WriteEventToWindowsLog(_AppName, ex.InnerException.Message, EventLogEntryType.Error);
			}
		}
		
		protected override void OnStop()
		{
			WriteEventToWindowsLog(_AppName, "Service stopped", EventLogEntryType.Information);
		}

		private void TimerScheduleProc(object state)
		{
			ProcessServiceActions();
		}

		private void ProcessServiceActions()
		{
			try
			{
				_ActionsProcessed = 0;

				//Get all the actions
				List<vw_SmartcardServiceAction> pendingActions = Director.GetPendingActions();

				//Process each action 
				foreach (vw_SmartcardServiceAction action in pendingActions)
				{
					switch ((DBAccessEnums.SmartcardServiceActionType)action.ActionType)
					{
						case DBAccessEnums.SmartcardServiceActionType.RevokeSmartcard:
							RevokeSmartcard(action);
							break;
						case DBAccessEnums.SmartcardServiceActionType.RequestReplacementBadge:
							RequestReplacementBadge(action);
							break;
						default:
							break;
					}

					_ActionsProcessed++;
				}

				WriteEventToWindowsLog(_AppName, string.Format("Processed {0} actions", _ActionsProcessed), EventLogEntryType.Information);
			}
			catch (Exception ex)
			{
				string errorMessage = "There was an error processing actions" + ex.Message;
				WriteEventToWindowsLog(_AppName, errorMessage, EventLogEntryType.Error);
			}
		}

		private void RequestReplacementBadge(vw_SmartcardServiceAction action)
		{
		    try
		    {
		        string currentUser = ConfigurationManager.AppSettings["CurrentUserID"];
		        int currentUserID = 0;

		        if (string.IsNullOrEmpty(currentUser) || !int.TryParse(currentUser, out currentUserID))
		            throw new ConfigurationErrorsException("The configuration parameter \"CurrentUserID\" is missing or is not a valid integer, please review the configuration file of the service");

		        int result = Director.RequestReplacementBadge(currentUserID, action);

		        Director.UpdateReplacementBadgeAction(action.ID, result);
		    }
		    catch (Exception ex)
		    {
		        string errorMessage = "There was an error requesting a replacement badge" + ex.Message;
		        WriteEventToWindowsLog(_AppName, errorMessage, EventLogEntryType.Error);
		        Director.UpdateReplacementBadgeAction(action.ID, -1);
		    }
		}

		private void RevokeSmartcard(vw_SmartcardServiceAction action)
		{
			//Only log certificate details for outgoing requests. As we are consuming this service no need to log these details
			string[] certDetails = new string[3];
			try
			{
				int result = Director.RevokeSmartcard(action.SerialNumber);
				Director.UpdateRevokeSmartcardAction(action.ID, result);

				Director.AddSmartcardServiceAudit("RevokeSmartcard", action.SerialNumber, result.ToString(), certDetails, null);
			}
			catch (WebException wex)
			{ 
				System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)wex.Response;
				if(response !=null && (int)response.StatusCode==503)	//if service is unavailable then set status of action to communication error
				{
					int result = 256;
					Director.UpdateRevokeSmartcardAction(action.ID, result);
					Director.AddSmartcardServiceAudit("RevokeSmartcard", action.SerialNumber, result.ToString(), certDetails, null);
					return;
				}

				string errorMessage = "There was an error connecting to the Revocation webservice" + wex.Message;
				Director.AddSmartcardServiceAudit("RevokeSmartcard", action.SerialNumber, null, certDetails, errorMessage);
				Director.UpdateRevokeSmartcardAction(action.ID, -1);
			}
			catch(Exception ex)
			{
				string errorMessage = "There was an error connecting to the Revocation webservice" + ex.Message;
				WriteEventToWindowsLog(_AppName, errorMessage, EventLogEntryType.Error);
				Director.AddSmartcardServiceAudit("RevokeSmartcard", action.SerialNumber, null, certDetails, errorMessage);
				Director.UpdateRevokeSmartcardAction(action.ID, -1);
			}
		}
	}
}
