﻿using HRFeedJMSQueueHandler;
using HRFeedJMSQueueHandler.DAL;
using HRFeedJMSQueueHandler.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ManuallyProcessHRFeedXML
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var context = new ManuallyProcessHRFeedXML.DAL.dbaccesshrfeedEntities())
                {
                    List<string> xmlMessages = context.XMLMessages15092014.Select(a => a.XMLMessage).ToList();

                    foreach (string sourceXML in xmlMessages)
                    {
                        int transactionId = PersistToDatabase(sourceXML);
                    }
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.Print(e.Message);
            }
        }

        public static int PersistToDatabase(string inputXML)
        {
            XmlDocument theDocument = new XmlDocument();
            theDocument.LoadXml(inputXML);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
            nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

            XmlNode rootNode = theDocument.ChildNodes[1];

            HRFeedMessage newMessage = MessageHandlerHelpers.GetHRFeedMessage(inputXML, nsmgr, rootNode);

            IHRFeedRepository repo = new HRFeedRepository();

            repo.AddHRFeedMessage(newMessage);
            return newMessage.TransactionId;
        }
    }
}
