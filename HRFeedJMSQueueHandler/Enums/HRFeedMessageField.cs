﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.Enums
{
    public enum HRFeedMessageField
    {
        TransactionId,
        MessageTimestamp,
        TriggerType,
        TriggerAction,
        PriorityFlag,
        HRID,
        FirstName,
        MiddleName,
        LastName,
        CreateAccessTimestamp,
        Organisational_Relationship,
        CostCenter,
        LegalEntity,
        UBR_CODE,
        LocationId,
        LocationCountry,
        LocationCity,
        ManagerHRID,
        ReactivationFlag,
        RevokeAccessDateTimestamp,
        TerminationDate,
        PreferredFirstName,
        PreferredLastName,
        AMRID,
        EP_Code,
        CorporateTitle,
        PhysicalAccessOnly,
        DBSiteIndicator,
        DBEmailAddress
    }
}
