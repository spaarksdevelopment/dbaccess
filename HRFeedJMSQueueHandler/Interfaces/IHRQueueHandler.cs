﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.Interfaces
{
    public interface IHRQueueHandler
    {        

        #region Logging Methods

        /// <summary>
        /// Logs an information message
        /// </summary>
        /// <param name="message"></param>
        void LogInfoMessage(string message);       


        /// <summary>
        /// Logs an error message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void LogErrorMessage(string message, Exception exception);
        

        /// <summary>
        /// Logs a debug message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void LogDebugMessage(string message, Exception exception);
        

        /// <summary>
        /// Logs a Fatal message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void LogFatalMessage(string message, Exception exception); 

        #endregion
    }
}
