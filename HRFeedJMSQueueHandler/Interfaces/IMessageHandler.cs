﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace HRFeedJMSQueueHandler.Interfaces
{
    public interface IMessageHandler
    {
        void ProcessMessage(string sourceXML);
        int PersistToDatabase(string inputXML);
        void GenerateAndSendAcknowledgementMessage(int transactionId, DateTime acknowledgementDate);
        bool Validate(string inputXML);
        void ReaderSettings_ValidationEventHandler(object sender, ValidationEventArgs args);
    }
}
