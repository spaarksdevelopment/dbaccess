﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HRFeedJMSQueueHandler.Interfaces
{
    public interface IReceiveMessage
    {
        bool ConnnectionToSource();
        void SendMessageToLog(string Message);
        bool ProcessMessage(XmlDocument message);
        //void WriteMessageToLog(

    }
}
