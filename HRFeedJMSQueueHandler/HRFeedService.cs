﻿using HRFeedJMSQueueHandler.CAT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler
{
    public partial class HRFeedService : ServiceBase
    {
        private dbSolaceHandler m_theHandler = null;
        private Timer m_theImportTimer;
        private CATProcessor catProcessor = null;

        public HRFeedService()
        {
            InitializeComponent();           
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                m_theHandler = new dbSolaceHandler();
                catProcessor = new CATProcessor();
                m_theHandler.LogInfoMessage("HRFeed Service Started");
                this.EventLog.WriteEntry("HRFeed Service Started");
                m_theImportTimer = new Timer(new TimerCallback(TimerImportProc));

                m_theImportTimer.Change(0, (Convert.ToInt32(ConfigurationManager.AppSettings["IntervalInMinutes"]) * 1000 * 60));
            }
            catch (Exception ex)
            {
                if (m_theHandler != null)
                {
                    m_theHandler.LogFatalMessage("Error starting HRFeed Service", ex);
                }

                if (m_theImportTimer != null)
                    m_theImportTimer.Dispose();

                this.Stop();
            }
        }

        //  The Tick event
        private void TimerImportProc(object state)
        {
            m_theHandler.PollQueue();

            catProcessor.ProcessTriggers();
        }

        protected override void OnStop()
        {
            if (m_theHandler != null)
            {
                m_theHandler.LogInfoMessage("HRFeed Service Stopped");
            }

            this.EventLog.WriteEntry("HRFeed Service Stopped");

            if (m_theImportTimer != null)
                m_theImportTimer.Dispose();

            m_theHandler = null;
        }
    }
}
