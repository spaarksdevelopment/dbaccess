This directory contains the following Solace Systems Messaging APIs assemblies:

lib/SolaceSystems.Solclient.Messaging.dll
                * The platform target for this assembly is x86
                * At runtime, it requires libsolclient.dll (x86 native dll) to be in the path
                * It has the same strong name as lib/64/SolaceSystems.Solclient.Messaging.dll

lib/64/SolaceSystems.Solclient.Messaging.dll
                * The platform target for this assembly is x64
                * At runtime, it requires libsolclient_64.dll (x64 native dll) to be in the path
                * It has the same strong name as lib/SolaceSystems.Solclient.Messaging.dll
                
lib/SolaceSystems.Solclient.Messaging_64.dll
                * The platform target for this assembly is x64
                * At runtime, it requires libsolclient_64.dll (x64 native dll) to be in the path
                * It has a different strong name than the rest of the bundled assemblies
                * When compiling/building against this assembly:
                                * The Target Platform of the executable must be x64, in this case
                                  the executable will only run on a 64-bit machine
                                * Setting the executable's Target Platform to Any CPU will
                                  produce an executable that can only run on 64 bit machines

Note:
It is possible to build one executable that is able to run as a 32-bit process on a 32-bit machine 
and as a 64-bit process on a 64-bit machine (without WOW64). Here's how:
- Select Any CPU as a Target Platform for the executable
- Reference lib/64/SolaceSystems.Solclient.Messaging.dll or lib/SolaceSystems.Solclient.Messaging.dll  
- When deploying the executable on a 32-bit machine, make sure to bundle with it
  lib/SolaceSystems.Solclient.Messaging.dll + lib/libsolclient.dll
- When deploying the executable on a 64-bit machine, make sure to bundle with it
  lib/64/SolaceSystems.Solclient.Messaging.dll + lib/libsolclient_64.dll

To use SSL connections, the openSSL DDLs will have to be bundled in the same directory as SolaceSystems.Solclient.Messaging.dll
The openSSL DLLs are located :
32-bits : lib\3rdparty\32
64-bits : lib\3rdparty\64
