﻿using HRFeedJMSQueueHandler.DAL;
using HRFeedJMSQueueHandler.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HRFeedJMSQueueHandler.Helpers
{
    public static class MessageHandlerHelpers
    {
        public static HRFeedMessage GetHRFeedMessage(string inputXML, XmlNamespaceManager nsmgr, XmlNode rootNode)
        {
            HRFeedMessage newMessage = new HRFeedMessage();

            SetHRFeedValueString(newMessage, HRFeedMessageField.TransactionId, rootNode, nsmgr);

            if (newMessage.TransactionId > 0)
            {
                SetHRFeedValueString(newMessage, HRFeedMessageField.MessageTimestamp, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.TriggerType, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.TriggerAction, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.PriorityFlag, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.HRID, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.FirstName, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.MiddleName, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.LastName, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.CreateAccessTimestamp, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.Organisational_Relationship, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.CostCenter, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.LegalEntity, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.UBR_CODE, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.LocationId, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.LocationCountry, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.LocationCity, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.ManagerHRID, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.ReactivationFlag, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.RevokeAccessDateTimestamp, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.TerminationDate, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.PreferredFirstName, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.PreferredLastName, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.AMRID, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.PhysicalAccessOnly, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.DBSiteIndicator, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.DBEmailAddress, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.CorporateTitle, rootNode, nsmgr);
                SetHRFeedValueString(newMessage, HRFeedMessageField.EP_Code, rootNode, nsmgr);
                newMessage.Processed = false;
                newMessage.RevokeProcessed = false;
                newMessage.ReactivateProcessed = false;
            }

            return newMessage;
        }

        public static void SetHRFeedValueString(HRFeedMessage newMessage, HRFeedMessageField hrFeedMessageField, XmlNode rootNode, XmlNamespaceManager nsmgr)
        {
            object newValue = GetXMLValue(rootNode, hrFeedMessageField, nsmgr);

            string fieldString = ConvertToString(hrFeedMessageField);
            SetProperty(newMessage, fieldString, newValue);
        }

        public static string GetXMLFieldName(HRFeedMessageField hrFeedMessageField)
        {
            switch (hrFeedMessageField)
            {
                case HRFeedMessageField.TransactionId:
                    return "TRANSACTION_ID";
                case HRFeedMessageField.MessageTimestamp:
                    return "TRANSACTION_TIMESTAMP";
                case HRFeedMessageField.TriggerType:
                    return "TRIGGER_TYPE";
                case HRFeedMessageField.TriggerAction:
                    return "TRIGGER_ACTION";
                case HRFeedMessageField.PriorityFlag:
                    return "PRIORITY_FLAG";
                case HRFeedMessageField.HRID:
                    return "HR_ID";
                case HRFeedMessageField.AMRID:
                    return "AMR_ID";
                case HRFeedMessageField.FirstName:
                    return "LEGAL_FIRST_NAME";
                case HRFeedMessageField.PreferredFirstName:
                    return "PREFERRED_FIRST_NAME";
                case HRFeedMessageField.MiddleName:
                    return "LEGAL_MIDDLE_NAME";
                case HRFeedMessageField.LastName:
                    return "LEGAL_LAST_NAME";
                case HRFeedMessageField.PreferredLastName:
                    return "PREFERRED_LAST_NAME";
                case HRFeedMessageField.CreateAccessTimestamp:
                    return "CREATE_ACCESS_DATE_TIMESTAMP";
                case HRFeedMessageField.Organisational_Relationship:
                    return "ORGANIZATIONAL_RELATIONSHIP";
                case HRFeedMessageField.CorporateTitle:
                    return "CORPORATE_TITLE";
                case HRFeedMessageField.CostCenter:
                    return "COST_CENTER";
                case HRFeedMessageField.LegalEntity:
                    return "LEGAL_ENTITY";
                case HRFeedMessageField.UBR_CODE:
                    return "UBR_CODE";
                case HRFeedMessageField.LocationId:
                    return "LOCATION_ID";
                case HRFeedMessageField.LocationCountry:
                    return "LOCATION_COUNTRY";
                case HRFeedMessageField.LocationCity:
                    return "LOCATION_CITY";
                case HRFeedMessageField.ManagerHRID:
                    return "MANAGER_HR_ID";
                case HRFeedMessageField.ReactivationFlag:
                    return "REACTIVATION_FLAG";
                case HRFeedMessageField.RevokeAccessDateTimestamp:
                    return "REVOKE_ACCESS_TIMESTAMP";
                case HRFeedMessageField.TerminationDate:
                    return "TERMINATION_DATE";
                case HRFeedMessageField.EP_Code:
                    return "EP_CODE";
                case HRFeedMessageField.PhysicalAccessOnly:
                    return "PHYSICAL_ACCESS_ONLY";
                case HRFeedMessageField.DBSiteIndicator:
                    return "DB_SITE_INDICATOR";
                case HRFeedMessageField.DBEmailAddress:
                    return "DB_EMAIL_ADDRESS";
                
                default:
                    throw new ApplicationException("Unhandled HRMessageField");
            }
        }

        public static object GetXMLValue(XmlNode rootNode, HRFeedMessageField hrFeedMessageField, XmlNamespaceManager nsmgr)
        {
            string prefix = ".//ns1:";
            string xmlFieldName = GetXMLFieldName(hrFeedMessageField);
            string path = prefix + xmlFieldName;
            object returnValue = string.Empty;

            try
            {
                XmlNode selectedNode = rootNode.SelectSingleNode(path, nsmgr);
                returnValue = selectedNode == null ? string.Empty : selectedNode.InnerText;
            }
            catch (Exception)
            {
                //  if we dont find it the XML
            }

            return returnValue;
        }

        public static string GenerateAcknowledgementXML(int transactionId, DateTime acknowledgementDate)
        {
            XmlDocument outputDoc = new XmlDocument();

            string messageNamespace = "http://xmlns.oracle.com/svcAdminAcknowledgement";

            XmlNode requestNode = outputDoc.CreateElement("svcAdminAcknowledgementRequest", messageNamespace);
            XmlNode transactionNode = outputDoc.CreateElement("TRANSACTION_ID", messageNamespace);
            transactionNode.InnerText = transactionId.ToString();
            XmlNode timestampNode = outputDoc.CreateElement("ACKNOWLEDGEMENT_TIMESTAMP", messageNamespace);

            TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(acknowledgementDate);
            timestampNode.InnerText = acknowledgementDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + utcOffset.ToString("'+'hh':'mm");

            requestNode.AppendChild(transactionNode);
            requestNode.AppendChild(timestampNode);

            outputDoc.AppendChild(requestNode);

            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + outputDoc.InnerXml;
        }

        #region Generic Methods

        public static void SetProperty(Object target, string fieldString, object newValue)
        {
            if (newValue == string.Empty)
                return;

            PropertyInfo property = target.GetType().GetProperty(fieldString);
            string typeName = (property.PropertyType).FullName;

            Type fieldType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;

            object newValueCorrectType = Convert.ChangeType(newValue, fieldType);
            property.SetValue(target, newValueCorrectType, null);
        }

        public static String ConvertToString(this Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }

        #endregion
    }
}
