﻿using HRFeedJMSQueueHandler.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.CAT
{
    [Flags]
    public enum HRPersonImportValidationFlags
    {
        InvalidHRID = 1,
        InvalidFirstName = 2,
        InvalidLastName = 4,
        InvalidEmail = 8,
        InvalidClass = 16,
        InvalidUBR = 32,
        InvalidCostCentre = 64,
        InvalidCostCentreName = 128,
        InvalidLegalEntity = 256,
        InvalidDivPath = 512,
        InvalidCityName = 1024,
        InvalidCountryName = 2048,
        InvalidStatus = 4096,
        InvalidOfficerID = 8192,
        InvalidPreferredFirstName = 16384,
        InvalidPreferredLastName = 32768
    }

    public class HRPersonValidator
    {
        private ICATRepository _CATRepository;
        private const int MaxLengthDBPeopleID = 7;
        private int _MaxCityNameLength = 50;
        private int _MaxLegalEntityIDLength = 5;

        public HRPersonValidator(ICATRepository repo)
        {
            _CATRepository = repo;            
        }

        public string GetErrorMessage(HRPersonImportValidationFlags errorFlags, string hrID)
        {
            StringBuilder builder = new StringBuilder();

            foreach (HRPersonImportValidationFlags value in Enum.GetValues(errorFlags.GetType()))
                if (errorFlags.HasFlag(value))
                    AppendMessageForFlag(builder, value);

            string message = string.Format("{0} for HR ID:{1}", builder.ToString(), hrID);

            return message;
        }

        public void AppendMessageForFlag(StringBuilder builder, HRPersonImportValidationFlags flag)
        {
            string message = flag.ToString();
            message = message.Replace("Invalid", "Invalid ");
            message += ";";
            builder.Append(message);
        }

        public HRPersonImportValidationFlags ValidateHRFeedMessageHRIDOnly(HRFeedMessage hrFeedMsg)
        {
            HRPersonImportValidationFlags errors = new HRPersonImportValidationFlags();

            if (!IsHRIDValid(hrFeedMsg.HRID))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidHRID);

            return errors;
        }

        public HRPersonImportValidationFlags ValidateHRFeedMessageRow(HRFeedMessage hrFeedMsg)
        {
            HRPersonImportValidationFlags errors = new HRPersonImportValidationFlags();

            if (!IsHRIDValid(hrFeedMsg.HRID))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidHRID);

            if (!IsNameValid(hrFeedMsg.FirstName))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidFirstName);

            if (!IsNameValid(hrFeedMsg.LastName))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidLastName);

            if (!IsNameNullOrValid(hrFeedMsg.PreferredFirstName))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidPreferredFirstName);

            if (!IsNameNullOrValid(hrFeedMsg.PreferredLastName))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidPreferredLastName);

            if (!IsUBRValid(hrFeedMsg.UBR_CODE))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidUBR);

            if (!IsCostCentreValid(hrFeedMsg.CostCenter))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCostCentre);

            if (!IsLegalEntityIDValid(hrFeedMsg.LegalEntity))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidLegalEntity);

            if (!IsCityNameValid(hrFeedMsg.LocationCity))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCityName);

            if (!IsCountryNameValid(hrFeedMsg.LocationCountry))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidCountryName);

            if (!IsOfficerIDValid(hrFeedMsg.CorporateTitle))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidOfficerID);

            if (!IsEmailValid(hrFeedMsg.DBEmailAddress))
                SetErrorMessage(ref errors, HRPersonImportValidationFlags.InvalidEmail);

            return errors;
        }

        public void SetErrorMessage(ref HRPersonImportValidationFlags allErrors, HRPersonImportValidationFlags thisError)
        {
            allErrors = allErrors |= thisError;
        }

        public bool AreThereAnyErrors(HRPersonImportValidationFlags allErrors)
        {
            return (allErrors) != 0;
        }

        public bool IsNameValid(string name)
        {
            return !IsRegexMatch(name, GetRegexFromConfig("RegExValidName"));
        }

        public bool IsNameNullOrValid(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return true;

            return IsNameValid(name);
        }

        public bool IsEmailValid(string emailAddress)
        {
            return string.IsNullOrWhiteSpace(emailAddress) ||
                IsRegexMatch(emailAddress,
                GetRegexFromConfig("RegExValidEmail"));
        }

        public bool IsUBRValid(string ubr)
        {
            return true;
            //if (string.IsNullOrWhiteSpace(ubr))
            //    return false;

            //return HRPersonImportHelpers.IsRegexMatch(ubr, GetRegexFromConfig("RegExValidUBR"));
        }

        public bool IsCostCentreValid(string costCentre)
        {
            return true;
        }

        public bool IsCostCentreNameValid(string costCentreName)
        {
            return true;
        }

        public bool IsLegalEntityIDValid(string legalEntityID)
        {
            if (IsStringNullOrTooLong(legalEntityID, _MaxLegalEntityIDLength))
                return false;

            return true;
        }

        public bool IsCityNameValid(string cityName)
        {
            if (IsStringTooLong(cityName, _MaxCityNameLength))
                return false;

            return true;
        }

        private bool IsStringTooLong(string input, int maxLength)
        {
            if (input == null)
                return false;

            return input.Length > maxLength;
        }

        private bool IsStringNullOrTooLong(string input, int maxLength)
        {
            return string.IsNullOrWhiteSpace(input) || input.Length > maxLength;
        }

        public bool IsCountryNameValid(string isoAlpha3Code)
        {
            return _CATRepository.IsCountryInDBAccess(isoAlpha3Code);
        }
        

        public bool IsOfficerIDValid(string officerID)
        {
            if (string.IsNullOrWhiteSpace(officerID))
                return true;

            return IsRegexMatch(officerID, GetRegexFromConfig("RegExValidOfficerID"));
        }

        public string GetRegexFromConfig(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey].Replace("#8217;", "'");
        }

        public bool IsHRIDValid(string hrID)
        {
            return hrID.Length <= MaxLengthDBPeopleID && IsRegexMatch(hrID, @"^\d{7,}$");
        }

        private bool IsRegexMatch(string input, string regex)
        {
            Match match = Regex.Match(input, regex, RegexOptions.IgnoreCase);
            return match.Success;
        }
    }
}
