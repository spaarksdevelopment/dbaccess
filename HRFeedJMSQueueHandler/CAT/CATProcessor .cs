﻿using HRFeedJMSQueueHandler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.CAT
{
    public class CATProcessor : HRQueueHandlerBase
    {
        ICATRepository _catRepo;
        HRPersonValidator _hrPersonValidator;

        public CATProcessor()
        {
            _catRepo = new CATRepository();
            _hrPersonValidator = new HRPersonValidator(_catRepo);
        }

        public void ProcessTriggers()
        {
            try
            {
                var triggers = _catRepo.GetUnprocessedCreateAccessTriggers();

                foreach (var trigger in triggers)
                {
                    try
                    {
                        switch (trigger.TriggerAction.ToUpper())
                        {
                            case "ADD":
                                {
                                    ProcessAddorUpdateTrigger(trigger);
                                    break;
                                }
                            case "UPD":
                                {
                                    ProcessAddorUpdateTrigger(trigger);
                                    break;
                                }
                            case "DEL":
                                {
                                    ProcessDeleteTrigger(trigger);
                                    break;
                                }
                        }                        
                    }
                    catch (Exception ex) //Catch and log the exception, so that it wont affect to the procesing rest of the records
                    {
                        LogErrorMessage(string.Format("HRFeed CAT Action '{0}' failed. (HRFeedMessageID: {1}, HRID: {2})", trigger.TriggerAction, trigger.HRFeedMessageId, trigger.HRID), ex);
                    }
                    finally
                    {
                        _catRepo.UpdateTriggerAsProcessed(trigger.HRFeedMessageId);
                    }
                }
            }
            catch (Exception ex)
            {
                LogFatalMessage("Error while processing Create Access Triggers", ex);
            }
        }

        private void ProcessAddorUpdateTrigger(HRFeedMessage hrFeedMsg)
        {
            HRPersonImportValidationFlags errorsFlags = _hrPersonValidator.ValidateHRFeedMessageRow(hrFeedMsg);
            if (!_hrPersonValidator.AreThereAnyErrors(errorsFlags))
            {
                int dbPeopleID = int.Parse(hrFeedMsg.HRID);

                var hrPerson = _catRepo.GetHRPerson(dbPeopleID);
                if (hrPerson == null)
                    hrPerson = new HRPerson();

                if (hrPerson != null)
                {
                    FillHRPerson(ref hrPerson, hrFeedMsg);

                    _catRepo.InsertOrUpdateHRPerson(hrPerson);

                    //If there if mp_user record update it as well
                    var mp_user = _catRepo.GetMpUser(dbPeopleID);
                    if (mp_user != null)
                    {
                        FillMP_User(ref mp_user, hrFeedMsg);
                        _catRepo.UpdateMpUser(mp_user);
                    }
                }

                LogSuccess(hrFeedMsg);
            }
            else
            {
                LogValidationError(hrFeedMsg, errorsFlags);
            }
        }

        private void ProcessDeleteTrigger(HRFeedMessage hrFeedMsg)
        {
            HRPersonImportValidationFlags errorsFlags = _hrPersonValidator.ValidateHRFeedMessageHRIDOnly(hrFeedMsg);
            if (!_hrPersonValidator.AreThereAnyErrors(errorsFlags))
            {
                _catRepo.ProcessDeleteTrigger(hrFeedMsg.HRFeedMessageId);
                LogSuccess(hrFeedMsg);
            }
            else
            {
                LogValidationError(hrFeedMsg, errorsFlags);
            }
        }

        private void LogSuccess(HRFeedMessage hrFeedMsg)
        {
            LogInfoMessage(string.Format("HRFeed CAT Action '{0}' processed. (HRFeedMessageID: {1}, HRID: {2})", hrFeedMsg.TriggerAction, hrFeedMsg.HRFeedMessageId, hrFeedMsg.HRID));
        }

        private void LogValidationError(HRFeedMessage hrFeedMsg, HRPersonImportValidationFlags errorFlags)
        {
            string errorMsg = _hrPersonValidator.GetErrorMessage(errorFlags, hrFeedMsg.HRID);
            LogErrorMessage(string.Format("HRFeed CAT Action '{0}' failed - {1}. (HRFeedMessageID: {2})", hrFeedMsg.TriggerAction, errorMsg, hrFeedMsg.HRFeedMessageId), null);
        }

        private void FillHRPerson(ref HRPerson person, HRFeedMessage msg)
        {
            if (person.dbPeopleID == 0)
            {
                person.dbPeopleID = int.Parse(msg.HRID);
                person.DBDirID = 0;
            }
            person.Forename = msg.FirstName;
            person.Surname = msg.LastName;
            person.PreferredFirstName = msg.PreferredFirstName;
            person.PreferredLastName = msg.PreferredLastName;
            person.Class = "A";
            person.Status = "A";
            person.UBR = msg.UBR_CODE;
            person.CostCentre = msg.CostCenter;
            person.LegalEntity = msg.LegalEntity;
            person.CountryName = _catRepo.GetCountryNameFromISO(msg.LocationCountry);
            person.CityName = msg.LocationCity;
            person.OfficerId = msg.CorporateTitle;
            person.Enabled = true;
            person.Revoked = null;

            int managerHRID = 0;
            int.TryParse(msg.ManagerHRID, out managerHRID);
            if (managerHRID > 0)
                person.ManagerName = _catRepo.GetPersonName(managerHRID);

            if (!string.IsNullOrWhiteSpace(msg.DBEmailAddress))
                person.EmailAddress = msg.DBEmailAddress;
        }

        private void FillMP_User(ref mp_User mp_user, HRFeedMessage msg)
        {
            mp_user.Forename = msg.FirstName;
            mp_user.Surname = msg.LastName;
            mp_user.CostCentre = msg.CostCenter;
            mp_user.UBR = msg.UBR_CODE;
            mp_user.Revoked = null;

            if (!string.IsNullOrWhiteSpace(msg.DBEmailAddress))
                mp_user.EmailAddress = msg.DBEmailAddress;
        }

        public override void SendAcknowledgeMessage(string xmlToSend, int transactionId)
        {
            //Empty
        }
    }
}
