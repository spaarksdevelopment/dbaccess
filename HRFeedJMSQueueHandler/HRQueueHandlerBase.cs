﻿using HRFeedJMSQueueHandler.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.Configuration;

namespace HRFeedJMSQueueHandler
{
    public abstract class HRQueueHandlerBase : IHRQueueHandler
    {
        /// <summary>
        /// The log variable.
        /// </summary>
        private ILog _Log = null;

        public ILog Log
        {
            get { return _Log; }
        } 

        private string _EnvironmentName;     
        
        #region Properties

        public string EnvironmentName
        {
            get { return _EnvironmentName; }
        }

        #endregion    

        public HRQueueHandlerBase()
        {
            XmlConfigurator.Configure();
            _Log = LogManager.GetLogger(typeof(IHRQueueHandler));

            LoadConfigParameters();
        }

        /// <summary>
        /// Load all the required parameters from the config file
        /// </summary>
        private void LoadConfigParameters()
        {
            _EnvironmentName = string.Format("{0} - {1}", ConfigurationManager.AppSettings["AppEnv"], ConfigurationManager.AppSettings["AppName"]);
        }

        public abstract void SendAcknowledgeMessage(string xmlToSend, int transactionId);

        #region Logging Methods

        /// <summary>
        /// Logs an information message (Stroed in the rollingAppender and also the HRFeedInfo Table)
        /// </summary>
        /// <param name="message"></param>
        public void LogInfoMessage(string message)
        {
            _Log.Info(message);
        }

        /// <summary>
        /// Logs an error message (Stroed in the rollingAppender and also the HRFeedError Table)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void LogErrorMessage(string message, Exception exception)
        {
            _Log.Error(message, exception);
        }

        /// <summary>
        /// Logs a debug message (Stroed in the rollingAppender)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void LogDebugMessage(string message, Exception exception)
        {
            _Log.Debug(message, exception);
        }

        /// <summary>
        /// Logs a Fatal message (Email sent to specified user(
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void LogFatalMessage(string message, Exception exception)
        {
            _Log.Fatal(message, exception);
        }

        #endregion

    }
}
