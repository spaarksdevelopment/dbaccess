﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace HRFeedJMSQueueHandler
{
    public class FileSystemHandler : HRQueueHandlerBase
    {

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileSystemHandler()
        {
            //  Set up a file watcher event for our folder so that we can process it
            string rootPath = ConfigurationManager.AppSettings["FileLoadPath"];

            FileSystemWatcher watcher = new FileSystemWatcher(rootPath, "*.xml");
            watcher.Created += watcher_Created;

            watcher.EnableRaisingEvents = true;
        }

        public override void SendAcknowledgeMessage(string xmlToSend, int transactionId)
        {
            //  Need to map this to the queue, lets log it for now in a folder with the date and the transaction number
            string outputPath = ConfigurationManager.AppSettings["FileSavePath"];

            File.WriteAllText(outputPath + String.Format("\\Acknowledgement_{0}_{1}.xml", transactionId, DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss")), xmlToSend, Encoding.UTF8);

            LogInfoMessage(String.Format("Acknowledgement file created for transaction id {0}", transactionId));
        }

        /// <summary>
        /// Do the work for the xml file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void watcher_Created(object sender, FileSystemEventArgs e)
        {
            string filePath = e.FullPath;
            string sourceXML = string.Empty;

            IHRFeedRepository repo = new HRFeedRepository(); //DI is overkill for this
            MessageHandler newHandler = new MessageHandler(this, repo);

            try
            {
                sourceXML = File.ReadAllText(filePath);
            }
            catch (Exception ex)
            {
                LogFatalMessage("Error in reading input file", ex);
            }
           
            if (!String.IsNullOrEmpty(sourceXML))
            {
                newHandler.ProcessMessage(sourceXML);
            }
        }

    }
}
