﻿using HRFeedJMSQueueHandler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler
{
    public interface IHRFeedRepository
    {
        void AddHRFeedMessage(HRFeedMessage newMessage);

        void UpdateHRFeedMessagePostProcessing(HRFeedMessage newMessage);
    }
}
