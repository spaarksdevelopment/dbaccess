﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.DAL
{
    public class CATRepository : ICATRepository
    {
        public CATRepository()
        {
            
        }

        public IEnumerable<HRFeedMessage> GetUnprocessedCreateAccessTriggers()
        {
            List<string> triggerActions = new List<string> { "ADD", "UPD", "DEL" };

            using (var context = new DbAccess_ImprovementsEntities())
            {
                return context.HRFeedMessages.Where(m => m.TriggerType.ToUpper() == "JOIN"
                                                        && m.Processed == false
                                                        && triggerActions.Contains(m.TriggerAction)
                                                        && m.ReactivationFlag == "N").OrderBy(o => o.HRFeedMessageId).ToList();
            }
        }

        public void UpdateTriggerAsProcessed(int id)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                var trigger = context.HRFeedMessages.Find(id);
                trigger.Processed = true;
                trigger.ProcessedDate = DateTime.Now;

                context.SaveChanges();
            }
        }

        public HRPerson GetHRPerson(int dbPeopleID)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                return context.HRPersons.Find(dbPeopleID);
            }
        }

        public mp_User GetMpUser(int dbPeopleID)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                return context.mp_User.Where(u => u.dbPeopleID == dbPeopleID).FirstOrDefault();
            }
        }

        public string GetPersonName(int dbPeopleID)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                var person = context.HRPersons.Find(dbPeopleID);
                if (person != null)
                    return string.Format("{0} {1}", person.Forename, person.Surname).Trim();

                return null;
            }
        }

        public void InsertOrUpdateHRPerson(HRPerson hrPerson)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                bool exists = context.HRPersons.Any(a => a.dbPeopleID == hrPerson.dbPeopleID);

                context.Entry(hrPerson).State = exists ? EntityState.Modified : EntityState.Added;

                context.SaveChanges();
            }
        }

        public void UpdateMpUser(mp_User mp_user)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                context.Entry(mp_user).State = EntityState.Modified;
                context.SaveChanges();
            }
        }        

        public void ProcessDeleteTrigger(int hrFeedMsgId)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                context.up_CreateAccessTrigger_DEL(hrFeedMsgId);
            }
        }

        public bool IsCountryInDBAccess(string isoAlpha3Code)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                return DoesCountryExist(isoAlpha3Code, context);
            }
        }

        public bool IsCityInDBAccess(string cityName)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                return context.LocationCities.Any(a => a.Name == cityName);
            }
        }

        private bool DoesCountryExist(string isoAlpha3Code, DbAccess_ImprovementsEntities context)
        {
            bool isoExists = context.ISO_Countries.Any(a => a.ISOALPHA3code == isoAlpha3Code);
            if (!isoExists)
                return false;

            string countryName = context.ISO_Countries.Where(a => a.ISOALPHA3code == isoAlpha3Code).First().Countryorareaname;

            return context.LocationCountries.Any(a => a.Name == countryName);
        }
        

        public string GetCountryNameFromISO(string countryISO)
        {
            using (var context = new DbAccess_ImprovementsEntities())
            {
                if (!DoesCountryExist(countryISO, context))
                    return null;

                string countryName = context.ISO_Countries.Where(a => a.ISOALPHA3code == countryISO).First().Countryorareaname;

                return countryName;
            }
        }
    }
}
