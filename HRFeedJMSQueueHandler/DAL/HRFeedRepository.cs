﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SymmetricEncryption;

namespace HRFeedJMSQueueHandler
{
    public class HRFeedRepository : IHRFeedRepository
    {
        public void AddHRFeedMessage(DAL.HRFeedMessage newMessage)
        {
            using (var context = new DAL.DbAccess_ImprovementsEntities())
            {
                context.HRFeedMessages.Add(newMessage);
                context.SaveChanges();
            }
        }

        public void UpdateHRFeedMessagePostProcessing(DAL.HRFeedMessage newMessage)
        {
            using (var context = new DAL.DbAccess_ImprovementsEntities())
            {
                 context.spr_HRFeedJMSQueueHandler_Post(newMessage.HRID, (int)newMessage.TransactionId, newMessage.TriggerType);
            }
        }
    }
}
