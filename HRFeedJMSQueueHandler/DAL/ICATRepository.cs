﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRFeedJMSQueueHandler.DAL
{
    public interface ICATRepository
    {
        HRPerson GetHRPerson(int dbPeopleID);

        mp_User GetMpUser(int dbPeopleID);

        string GetPersonName(int dbPeopleID);

        IEnumerable<HRFeedMessage> GetUnprocessedCreateAccessTriggers();

        void InsertOrUpdateHRPerson(HRPerson hrPerson);

        void UpdateMpUser(mp_User mp_user);
        
        void UpdateTriggerAsProcessed(int id);

        void ProcessDeleteTrigger(int hrFeedMsgId);

        bool IsCountryInDBAccess(string isoAlpha3Code);

        bool IsCityInDBAccess(string cityName);

        string GetCountryNameFromISO(string countryISO);
    }
}
