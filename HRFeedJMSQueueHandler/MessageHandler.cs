﻿using HRFeedJMSQueueHandler.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using HRFeedJMSQueueHandler.DAL;
using HRFeedJMSQueueHandler.Helpers;

namespace HRFeedJMSQueueHandler
{
    public class MessageHandler : IMessageHandler
    {
        #region Variables

        private HRQueueHandlerBase m_parentObject = null;
        private XmlTextReader m_reader = null;
        private StringBuilder m_xmlErrorLog = new StringBuilder();

        private IHRFeedRepository _HRFeedRepository;
        #endregion

        #region Properties

        public StringBuilder XmlErrorLog
        {
            get { return m_xmlErrorLog; }
            set { m_xmlErrorLog = value; }
        }

        #endregion
        
        #region Constructor

        public MessageHandler(HRQueueHandlerBase handler, IHRFeedRepository repo)
        {
            m_parentObject = handler;
            _HRFeedRepository = repo;
        }

        #endregion

        #region XML Processing

        /// <summary>
        /// Process the message and persist to the database
        /// </summary>
        /// <param name="sourceXML"></param>
        public void ProcessMessage(string sourceXML)
        {

            //   Validate the incoming XML message
            //  Removed cas causing too many issues as we dont have a proper xsd from client!.
            bool isValid = true;//Validate(sourceXML);

            if (isValid)
            {
                //  Insert into the database
                m_parentObject.LogInfoMessage("Processing message: (" + sourceXML + ")");

                int transactionId = PersistToDatabase(sourceXML);

                if (transactionId > 0)
                {
                    //  Send acknowledgement message back to the sender
                    GenerateAndSendAcknowledgementMessage(transactionId, DateTime.Now);
                }
            }
        }

        /// <summary>
        /// Stores the XML message to the database
        /// </summary>
        /// <param name="inputXML"></param>
        public int PersistToDatabase(string inputXML)
        {
            int transactionId = 0;

            try
            {
                XmlDocument theDocument = new XmlDocument();
                theDocument.LoadXml(inputXML);

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(theDocument.NameTable);
                nsmgr.AddNamespace("ns1", "http://xmlns.oracle.com/AccessManagementPub");

                XmlNode rootNode = theDocument.ChildNodes[1];

                HRFeedMessage newMessage = MessageHandlerHelpers.GetHRFeedMessage(inputXML, nsmgr, rootNode);

                if (newMessage.TransactionId > 0)
                {
                    _HRFeedRepository.AddHRFeedMessage(newMessage);

                    _HRFeedRepository.UpdateHRFeedMessagePostProcessing(newMessage);

                    return newMessage.TransactionId;
                }
                else
                    m_parentObject.LogErrorMessage(String.Format("Invalid or missing transaction ID for message ({0})", inputXML), null);
            }
            catch (Exception ex)
            {
                transactionId = 0;
                m_parentObject.LogErrorMessage(String.Format("Failed to process XML message from queue ({0})", inputXML), ex);
            }

            return transactionId;
        }


        #endregion

        #region Acknowledgement

        /// <summary>
        /// Construct the response message and assign to parent
        /// </summary>
        /// <param name="transactionId"></param>
        public void GenerateAndSendAcknowledgementMessage(int transactionId, DateTime acknowledgementDate)
        {
            string outputDoc = MessageHandlerHelpers.GenerateAcknowledgementXML(transactionId, acknowledgementDate);

            m_parentObject.SendAcknowledgeMessage(outputDoc, transactionId);
        }

        #endregion

        #region XML Validation

        /// <summary>
        /// Validates the incoming XML message based on the supplied XSD file
        /// </summary>
        /// <param name="inputXML"></param>
        /// <param name="XSDPath"></param>
        /// <returns></returns>
        public bool Validate(string inputXML)
        {
            bool toReturn = false;
            XmlReader objXmlReader = null;

            try
            {
                MemoryStream inputStream = new MemoryStream((Encoding.UTF8.GetBytes(inputXML)));
                m_reader = new XmlTextReader(inputStream);

                Assembly assembly = Assembly.GetExecutingAssembly();
                StreamReader xsdReaderStream = new StreamReader(assembly.GetManifestResourceStream("HRFeedJMSQueueHandler.XSD.AccessManagementRequest.xsd"));
                XmlSchema Schema = XmlSchema.Read(xsdReaderStream, new ValidationEventHandler(ReaderSettings_ValidationEventHandler));
                xsdReaderStream.Close();

                XmlReaderSettings ReaderSettings = new XmlReaderSettings();
                ReaderSettings.ValidationType = ValidationType.Schema;
                ReaderSettings.Schemas.Add(Schema);

                ReaderSettings.ValidationEventHandler += new ValidationEventHandler(ReaderSettings_ValidationEventHandler);
                objXmlReader = XmlReader.Create(m_reader, ReaderSettings);

                while (objXmlReader.Read()) { }

                objXmlReader.Close();

                toReturn = true;
            }
            catch (Exception ex)
            {
                m_parentObject.LogErrorMessage("Error parsing incoming XML message", ex);
            }
            finally
            {
                if (objXmlReader != null) { objXmlReader.Close(); }
            }

            if (m_xmlErrorLog.Length > 0)
            {
                toReturn = false;
                m_parentObject.LogErrorMessage("XML has failed validation (Message: " + inputXML + ")", null);
                m_parentObject.LogErrorMessage("Error output:" + m_xmlErrorLog.ToString(), null);
            }

            return toReturn;
        }

        /// <summary>
        /// Handle any validation exception messages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ReaderSettings_ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            m_xmlErrorLog.AppendLine("Line: " + m_reader.LineNumber + " - Position: " + m_reader.LinePosition + " - " + args.Message);
        }

        #endregion

    }
}
