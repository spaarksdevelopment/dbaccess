﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Collections;
using SolaceSystems.Solclient.Messaging;
using System.Threading;
using SolaceSystems.Solclient.Messaging.SDT;
using HRFeedJMSQueueHandler.Helpers;

namespace HRFeedJMSQueueHandler
{
    public class dbSolaceHandler : HRQueueHandlerBase
    {
                     
        private string m_EmailAddress;
        private string m_SenderEmailAddress;
        private string m_QueueName;
        private string m_QueueNameAck;
        private bool m_logVerbose;

        IContext context = null;
        ISession session = null;
        IBrowser browser = null;
        IQueue queue = null;
        IQueue queueAck = null;

        public dbSolaceHandler()
        {
            setConfiguration();           
        }

        public void PollQueue()
        {
            if (InitiateListener())
            {
                try
                {
                    ProcessQueue();
                }
                catch (Exception exception)
                {
                    this.LogErrorMessage("Error in ProcessQueue()", exception);
                }
                finally
                {
                    KillConnection();
                }
            }
        }

        public override void SendAcknowledgeMessage(string xmlToSend, int transactionId)
        {                        
            IMessage msg = ContextFactory.Instance.CreateMessage();
            byte[] bytesToSend = Encoding.ASCII.GetBytes(xmlToSend);
            sbyte[] byteData = Array.ConvertAll(bytesToSend, (a) => (sbyte)a);
            msg.SetXmlContent(byteData);
            msg.DeliveryMode = MessageDeliveryMode.Persistent;
            msg.Destination = queueAck;
                    
            if (session.Send(msg) == ReturnCode.SOLCLIENT_OK)
            {
                this.LogInfoMessage(String.Format("SendAcknowledgeMessage - ({0})", transactionId));
            }
        }


        /// <summary>
        /// Get all the messages and process into the database
        /// TODO we need to flag old messages as invalid if new hrid is passed for unprocessed message
        /// </summary>
        public void ProcessQueue()
        {
            IMessage msg = null;
            IHRFeedRepository repo = new HRFeedRepository();
            MessageHandler theHandler = new MessageHandler(this, repo);

            while ((msg = browser.GetNext()) != null)
            {
                if (m_logVerbose)
                {
                    this.LogInfoMessage(msg.Dump());
                }

                //Convert sbyte[] to byte[]
                byte[] byteData = Array.ConvertAll(msg.XmlContent, (a) => (byte)a);
               
                //you got the actual string here
                string stringFromByteData = Encoding.ASCII.GetString(byteData);

                theHandler.ProcessMessage(stringFromByteData);

                // This deletes it from the appliance's message spool.
                browser.Remove(msg);
            }
        }

        protected bool setConfiguration()
        {                       
            this.m_EmailAddress = ConfigurationManager.AppSettings["MessageRecipient"];
            this.m_SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"];
            this.m_QueueName = ConfigurationManager.AppSettings["QueueName"];
            this.m_logVerbose = Boolean.Parse(ConfigurationManager.AppSettings["LogVerbose"]);
            this.m_QueueNameAck = ConfigurationManager.AppSettings["QueueNameAck"];

            return true;
        }

        /// <summary>
        /// Close the listener connection
        /// </summary>
        public void KillConnection()
        {
            try
            {
                session.Disconnect();
            }
            catch (Exception exception)
            {
                this.LogErrorMessage("Error in KillConnection()", exception);
            }
        }
        
        /// <summary>
        /// Initiate the listener connection
        /// </summary>
        public bool InitiateListener()
        {          
            try
            {
                this.LogInfoMessage("HRFeedListener Initiated");

                ContextFactoryProperties cfp = new ContextFactoryProperties();
                // Set log level.
                cfp.SolClientLogLevel = SolLogLevel.Info;
                // Log errors to console.
                cfp.LogToConsoleError();
                // Must init the API before using any of its artifacts.
                ContextFactory.Instance.Init(cfp);

                ContextProperties contextProps = new ContextProperties();
                SessionProperties sessionProps = SolaceUtility.NewSessionPropertiesFromConfig();

                IHRFeedRepository repo = new HRFeedRepository();
                context = ContextFactory.Instance.CreateContext(contextProps, null);

                if (m_logVerbose)
                {
                    this.LogInfoMessage("Context successfully created. ");
                    this.LogInfoMessage("About to create the session ...");
                }

                session = context.CreateSession(sessionProps, SolaceUtility.HandleMessageEvent, SolaceUtility.HandleSessionEvent);
                if (m_logVerbose)
                {
                    this.LogInfoMessage("Session successfully created.");
                    this.LogInfoMessage("About to connect the session ...");
                }

                if (session.Connect() == ReturnCode.SOLCLIENT_OK && m_logVerbose)
                {
                    this.LogInfoMessage("Session successfully connected");
                    this.LogInfoMessage(GetRouterInfo(session));
                }
                if (!session.IsCapable(CapabilityType.SELECTOR))
                {
                    this.LogErrorMessage(string.Format("Capability '{0}' is required to run this sample",
                        CapabilityType.SELECTOR), null);
                    return false;
                }
               
                
                queue = ContextFactory.Instance.CreateQueue(m_QueueName);
                queueAck = ContextFactory.Instance.CreateQueue(m_QueueNameAck);
                
                BrowserProperties browserProps = new BrowserProperties();
                browser = session.CreateBrowser(queue, browserProps);

                if (m_logVerbose)
                {
                    this.LogInfoMessage(string.Format("Browsing queue {0} ", m_QueueName));
                }                                                                  
            }
            catch (Exception exception)
            {
                this.LogErrorMessage("Error in InitiateListener()", exception);
            }

            return true;
        }       

        /// <summary>
        /// Demonstrates how to get router info and capabilities
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public string GetRouterInfo(ISession session)
        {
            CapabilityType[] capabilities =
             new CapabilityType[] {CapabilityType.BROWSER,CapabilityType.COMPRESSION,
                                   CapabilityType.ENDPOINT_MANAGEMENT,CapabilityType.ENDPOINT_MESSAGE_TTL,
                                   CapabilityType.MAX_DIRECT_MSG_SIZE, CapabilityType.MAX_GUARANTEED_MSG_SIZE,
                                   CapabilityType.MESSAGE_ELIDING, CapabilityType.PEER_PORT_SPEED,CapabilityType.PEER_PORT_TYPE,
                                   CapabilityType.PEER_SOFTWARE_DATE, CapabilityType.PUB_GUARANTEED, CapabilityType.QUEUE_SUBSCRIPTIONS,
                                   CapabilityType.SELECTOR, CapabilityType.SUB_FLOW_GUARANTEED, CapabilityType.SUBSCRIPTION_MANAGER,
                                   CapabilityType.SUPPORTS_XPE_SUBSCRIPTIONS, CapabilityType.TEMP_ENDPOINT};
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("Appliance information: \n Appliance Name: {0}\n Platform: {1} \n Version: {2} \n ",
                    session.GetCapability(CapabilityType.PEER_ROUTER_NAME).Value.Value,
                    session.GetCapability(CapabilityType.PEER_PLATFORM).Value.Value,
                    session.GetCapability(CapabilityType.PEER_SOFTWARE_VERSION).Value.Value));
                sb.Append("Appliance Capabilities:\n");
                for (int i = 0; i < capabilities.Length; i++)
                {
                    sb.Append(string.Format("\t{0} : {1}\n", capabilities[i], session.GetCapability(capabilities[i]).Value.Value));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return "Error occurred when getting appliance capabilities, reason: " + ex.Message;
            }
        }

    }
}
